/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.io.File;
import java.io.IOException;

import edu.umd.umiacs.armor.PackageInfo;
import edu.umd.umiacs.armor.gpu.GPUException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.FileIO;
import edu.umd.umiacs.armor.util.ParseOptions;

public class ArmorSas
{
	public static void main(String args[]) throws Exception
  {
  	ParseOptions options = new ParseOptions();
  	options.addOption("pdb", "-pdb", "Name of the PDB file to use for calculations.", "2jy6.pdb");
  	options.addOption("model", "-model", "Model number of the pdb file.", 1);
  	options.addOption("output", "-output", "Name of the profile output file.", "");
  	options.addOption("minQ", "-minQ", "Minimum value of the q range.", .01);
  	options.addOption("maxQ", "-maxQ", "Maxium value of the q range.", 0.5);
  	options.addOption("numQ", "-numQ", "Number of uniformly destributed values in the q range.", 50);
  	options.addOption("electrondensity", "-elec", "Electron density of background solvent in (Angstrom^-1).", 0.334);
  	//options.addIntOption("layer", "-layer", "0) No Layer 1) Solvent Layer 2) Solvent Sphere 3) Solvent Box.", 0);
  	//options.addDoubleOption("thickness", "-thickness", "Thickness of the layer", 8.0);
  	//options.addDoubleOption("eps", "-eps", "Maxium error in the computation of the profile.", 1.0e-3);
  	//options.addIntOption("factors", "-factors", "Type of form factors to use, 1) SAXS with excluded volume 2) SANS", 1);
  	
		System.out.println("ArmorSAS Version: "+PackageInfo.ARMOR_VERSION+", Build Time: "+PackageInfo.BUILD_TIME);

		try 
  	{
  		options.parse(args);
  	}
  	catch (Exception e)
  	{
  		System.out.println(e.getMessage());
  		throw e;
  	}
  	
  	int status = 1;
   	if (options.needsHelp() || options.get("pdb").getString().isEmpty())
   	{
			System.out.println("java -jar armorsas*.jar -pdb <pdbfile> [optional flags]");
			System.out.println("Example: java -jar armorsas-1.0.jar -pdb 2JY6.pdb -output 2jy6_profile.txt");
  		System.out.println(options.helpMenuString());
  		status = 0;
   	}
  	else
  		status = runGeneral(options);
   	
   	if (status==0)
   		System.out.println("Success.");
   	else
   		System.out.println("Failed!");
  }
	
	public static int runGeneral(ParseOptions options) throws GPUException
	{
	  String pdbFile = options.get("pdb").getString();
		String outputFile = options.get("output").getString();

	  System.out.print("Extracting data....");
	  PdbStructure pdb = null;
	  try
	  {
	  	pdb = MoleculeFileIO.readPDB(pdbFile);
	  }
	  catch(Exception e)
	  {
  		System.out.println("Could not parse PDB data.\n"+e.getMessage());
  		return 1;  	
	  }
	  System.out.println("done.");
	  
	  //adjust model number to start with 0
	  int model = options.get("model").getInteger()-1;
	  
	  //check that model number is ok
		if (pdb.getNumberOfModels()<=model)
		{
			System.out.println("Invalid model number: "+model+".");
			return 1;
		}
		
		Molecule molecule;
		try
		{
			molecule = pdb.getModel(model).createBasicMolecule();
		}
		catch (PdbException e)
		{
			System.out.println(e.getMessage());
			return 1;
		}
		
		//create SAXS table
		double qMin = options.get("minQ").getDouble();
		double qMaxs = options.get("maxQ").getDouble();
		int qSize = options.get("numQ").getInteger();
		double electronDensity = options.get("electrondensity").getDouble();
		SaxsFormFactorTable table = new SaxsFormFactorTable(BasicUtils.uniformLinePoints(qMin, qMaxs, qSize), true, electronDensity);
		
		ScatteringProfile profile = null;
		try
		{
			System.out.print("Computing profile...");
			
			//create the brute force algorithm
			ScatteringPredictor gpuPredictor = new BruteGpuScatteringPredictor(table);
			
			//compute the scattering
			profile = gpuPredictor.computeProfile(molecule).getProfile();
			
			System.out.println("done.");
		}
		catch (SasRuntimeException e)
		{
			System.out.println("failed! "+e.getMessage());
			return 1;
		}
		
		if (!outputFile.isEmpty())
		{
			System.out.print("Outputing profile results to "+outputFile+"...");
			
			File output = new File(outputFile);
			try
			{
				FileIO.write(output, profile.toString());
			}
			catch (IOException e)
			{
				System.out.println("\nCould not write to file "+outputFile);
				System.out.println(e.getMessage());
				return 1;
			}
			
			System.out.println("done.");
		}
		else
			System.out.println(profile.toString());
	  
	  return 0;
	}
}
