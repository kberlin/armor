/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLCommandQueue;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLKernel;
import com.jogamp.opencl.CLProgram;

import edu.umd.umiacs.armor.gpu.CLResource;
import edu.umd.umiacs.armor.gpu.GPUException;
import edu.umd.umiacs.armor.gpu.GpuContext;
import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.Molecule;

/**
 * The Class BruteGpuScatteringPredictor.
 */
public class BruteGpuScatteringPredictor extends AbstractScatteringPredictor implements CLResource
{
	/** The context. */
	private final CLContext context;

	/** The device. */
	private final CLDevice device;
	
	/** The form factor table. */
	private FormFactorTable formFactorTable;
	
	/** The kernel. */
	private final CLKernel kernel;

	/** The program. */
	private final CLProgram program;
	
	private static final String debyeCLFileName = "debyeKernel.cl";
	
	/** The Constant NUM_LOCAL_Q. */
	private static final int NUM_LOCAL_Q = 2;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8229970202906762970L;
		
	/**
	 * Instantiates a new brute gpu scattering predictor.
	 * 
	 * @param table
	 *          the table
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 * @throws GPUException 
	 */
	public BruteGpuScatteringPredictor(FormFactorTable table) throws SasRuntimeException, GPUException
	{
		this.formFactorTable = table;
		
		this.context = GpuContext.getCLContext();
		this.device = this.context.getMaxFlopsDevice();
		String compilerOptions = "-cl-mad-enable -cl-fast-relaxed-math -cl-no-signed-zeros -cl-unsafe-math-optimizations -cl-finite-math-only -D NUM_LOCAL_Q="+NUM_LOCAL_Q; 
		String functionName = "debyeFunctionMulti";
		
		try
		{
			InputStream debyeKernelStream = BruteGpuScatteringPredictor.class.getClassLoader().getResourceAsStream("opencl/"+debyeCLFileName);
			
			if (debyeKernelStream==null)
				throw new SasRuntimeException("Could not find OpenCL file "+debyeCLFileName+".");
			
			this.program = this.context.createProgram(debyeKernelStream).build(compilerOptions);
		}
		catch(FileNotFoundException fileException)
		{
			throw new SasRuntimeException("Could not find OpenCL file "+debyeCLFileName+".", fileException);
		}
		catch (NullPointerException nullException)
		{
			throw new SasRuntimeException("Could not find OpenCL file "+debyeCLFileName+".", nullException);
		}
		catch (IOException ioException)
		{
			throw new SasRuntimeException("Could not read OpenCL file "+debyeCLFileName+".", ioException);
		}
		
		this.kernel = this.program.createCLKernel(functionName);
	}
	
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#computeProfile(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public synchronized ScatteringPrediction computeProfile(Molecule molecule) throws SasRuntimeException
	{
    if (getFormFactorTable().sizeQ()==0)
      return null;

    double[] Iq = new double[this.formFactorTable.sizeQ()];
    
    final double[][] atomArray = molecule.getAtomPositionArray();
    //final double[][] fa = table.getFormFactorArray(molecule);
    final double[] qValues = this.formFactorTable.getQArray();
    
    if (molecule.size()==0)
      return new ScatteringPrediction(new ScatteringProfile(qValues, Iq, null), 0.0); 
    
    //get float atomic positions
    float[] floatAtomPositions = getFloatAtomPositions(atomArray);
        
    long startTime;
    long endTime;

    //start timing
    startTime = System.currentTimeMillis();
    
    //store the size of the molecule
    int n = molecule.size();
        
    //figure out the optimal number of cores per work group
    long maxGroupSize = this.kernel.getWorkGroupSize(this.device);
    long totalThreads = maxGroupSize*this.device.getMaxComputeUnits();
    //long totalThreads = maxGroupSize*context.getDeviceInfo().maxComputeUnits;
    
    //make them warp size
    //maxGroupSize = BasicMath.nearestPow2(maxGroupSize);
    //TODO: fix this hack
    
    //compute proper work size
    long[] local_work_size = {Math.min(n, maxGroupSize)};
    
    //nearest block size to n
    long nearestSize = BasicMath.nearestMultiple(n,(int)local_work_size[0]);
    
    //find the best work size
    long[] global_work_size = {nearestSize};
    while(global_work_size[0]<totalThreads*2 && global_work_size[0]*2<n*n)
    	global_work_size[0]*=2;
    
    long numberOfGroups = global_work_size[0]/local_work_size[0];
    
    //number of q per iteration
    //use only half memory, subtract out the location storage, subtract out the need to store at least worker info
    //TODO figure out the correct number
    //int qSize = (int)Math.max(1L,(context.get .maxMemAllocSize/2-NUM_LOCAL_Q*128L*4L*global_work_size[0])/(NUM_LOCAL_Q*4L*4L*global_work_size[0])+1);
    //qSize = Math.min(qSize,10);
    int qSize = 4;
    qSize = Math.min(NUM_LOCAL_Q,qSize);
    
    //System.out.println("MaxGroupSize="+maxGroupSize+" Local Work Size="+local_work_size[0]+"  Q/iter="+qSize+"  Total Threads="+totalThreads);
    
    //declare all the needed buffers
    CLBuffer<FloatBuffer> atomPositionBuffer = null;
    CLBuffer<FloatBuffer> qValuesBuffer = null;
    CLBuffer<FloatBuffer> formFactorsBuffer = null;
    CLBuffer<FloatBuffer> outputBuffer = null;
    CLCommandQueue queue = null;
    
    if (numberOfGroups<1)
  		throw new SasRuntimeException("numberOfGroups must be >=1.");
    if (qSize<1)
  		throw new SasRuntimeException("qSize must be >=1.");
   	
    
    for (int iter=0; iter<floatAtomPositions.length; iter++)
    	if (Float.isNaN(floatAtomPositions[iter]) || Float.isInfinite(floatAtomPositions[iter]))
    		throw new SasRuntimeException("Atom position cannot be infinite or NaN.");
    
    try
    {
	    //create the command queue
  		queue = this.device.createCommandQueue();
	    
    	//atom positions
    	atomPositionBuffer = this.context.createFloatBuffer(n*3, com.jogamp.opencl.CLMemory.Mem.READ_ONLY, com.jogamp.opencl.CLMemory.Mem.COPY_BUFFER);
    	atomPositionBuffer.getBuffer().put(floatAtomPositions).rewind();
    	this.kernel.setArg(0, atomPositionBuffer);
    	queue.putWriteBuffer(atomPositionBuffer, true);
	    //atomPositionBuffer = clCreateBuffer(context.getContext(),CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, Sizeof.cl_float*n*3, Pointer.to(floatAtomPositions), null);
	    //clSetKernelArg(kernelDebye.getKernel(), 0, Sizeof.cl_mem, Pointer.to(atomPositionBuffer));
	
	    //number of atoms
    	this.kernel.setArg(1, n);
	    //clSetKernelArg(kernelDebye.getKernel(), 1, Sizeof.cl_int, Pointer.to(new int[]{n}));
	
	    //the output buffer is reused 
    	outputBuffer = this.context.createFloatBuffer((int)(numberOfGroups*qSize), com.jogamp.opencl.CLMemory.Mem.WRITE_ONLY);
    	//outputBuffer.getBuffer().put(new float[]{numberOfGroups*qSize,2.0f});
    	this.kernel.setArg(5, outputBuffer);
    	//queue.putWriteBuffer(outputBuffer, false);
      //outputBuffer = clCreateBuffer(context.getContext(),CL_MEM_READ_WRITE, Sizeof.cl_float*numberOfGroups*qSize, null, null);
      //clSetKernelArg(kernelDebye.getKernel(), 5, Sizeof.cl_mem, Pointer.to(outputBuffer));	    
	    
	    //local storage of atoms
    	this.kernel.setNullArg(6, (int)(com.jogamp.common.nio.Buffers.SIZEOF_FLOAT*local_work_size[0]*3));
      //clSetKernelArg(kernelDebye.getKernel(), 6, Sizeof.cl_float*local_work_size[0]*3, null);
	
      //iterate by no more than the maxium allowed q values
      for (int qIndex=0; qIndex<getFormFactorTable().sizeQ(); qIndex+=qSize)
	    {
      	//the current qSize
      	int qSizeCurr = Math.min(qSize, getFormFactorTable().sizeQ()-qIndex);
      	
      	//local values to store
        int qSizeLocal = Math.min(NUM_LOCAL_Q, qSizeCurr);
      	
	      float[] floatFormFactorArray = getFloatQFormFactorArray(molecule, qIndex, qSizeCurr);
	      
	      //check for errors
	      for (int newIter=0; newIter<floatFormFactorArray.length; newIter++)
	       	if (Float.isNaN(floatFormFactorArray[newIter]) || Float.isInfinite(floatFormFactorArray[newIter]))
	      		throw new SasRuntimeException("Form factors cannot be infinite or NaN.");
	      
	      //current q values
	      float[] floatQValues = new float[qSizeCurr];
	      for (int qIter=0; qIter<qSizeCurr; qIter++)
	        floatQValues[qIter] = (float)getFormFactorTable().getQ(qIndex+qIter);
	      
	      //check for errors
	      for (int newIter=0; newIter<floatQValues.length; newIter++)
	       	if (Float.isNaN(floatQValues[newIter]) || Float.isInfinite(floatQValues[newIter]))
	      		throw new SasRuntimeException("Q cannot be infinite or NaN.");
	      
	      //store q values
	      qValuesBuffer = this.context.createFloatBuffer(qSizeCurr, com.jogamp.opencl.CLMemory.Mem.READ_ONLY, com.jogamp.opencl.CLMemory.Mem.COPY_BUFFER);
	      qValuesBuffer.getBuffer().put(floatQValues).rewind();
	    	this.kernel.setArg(2, qValuesBuffer);
	    	queue.putWriteBuffer(qValuesBuffer, true);
	      //qValuesBuffer = clCreateBuffer(context.getContext(),CL_MEM_READ_ONLY|CL_MEM_COPY_HOST_PTR, Sizeof.cl_float*qSizeCurr, Pointer.to(floatQValues), null);
		    //clSetKernelArg(kernelDebye.getKernel(), 2, Sizeof.cl_mem, Pointer.to(qValuesBuffer));

	      //current q size
	    	this.kernel.setArg(3, qSizeCurr);
	      //clSetKernelArg(kernelDebye.getKernel(), 3, Sizeof.cl_float, Pointer.to(new int[]{qSizeCurr}));
	      
	      //current form factor values
	    	formFactorsBuffer = this.context.createFloatBuffer(n*qSizeCurr, com.jogamp.opencl.CLMemory.Mem.READ_ONLY, com.jogamp.opencl.CLMemory.Mem.COPY_BUFFER);
	    	formFactorsBuffer.getBuffer().put(floatFormFactorArray).rewind();
	    	this.kernel.setArg(4, formFactorsBuffer);
	    	queue.putWriteBuffer(formFactorsBuffer, true);
	      //formFactorsBuffer = clCreateBuffer(context.getContext(),CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, Sizeof.cl_float*n*qSizeCurr, Pointer.to(floatFormFactorArray), null);
	      //clSetKernelArg(kernelDebye.getKernel(), 4, Sizeof.cl_mem, Pointer.to(formFactorsBuffer));
	
	      //local storage of form factors
	    	this.kernel.setNullArg(7, (int)(com.jogamp.common.nio.Buffers.SIZEOF_FLOAT*local_work_size[0]*qSizeLocal));
	      //clSetKernelArg(kernelDebye.getKernel(), 7, Sizeof.cl_float*local_work_size[0]*qSizeLocal, null);
	    	
	      //local storage of local Iq factors
	    	this.kernel.setNullArg(8, (int)(com.jogamp.common.nio.Buffers.SIZEOF_FLOAT*local_work_size[0]*qSizeLocal));
	      //clSetKernelArg(kernelDebye.getKernel(), 8, Sizeof.cl_float*local_work_size[0]*qSizeLocal, null);

	      //long currStartTime = System.currentTimeMillis();
	    
	      //start the kernel operations with proper number of work size
	      queue.put1DRangeKernel(this.kernel, 0, global_work_size[0], local_work_size[0]);
	      //clEnqueueNDRangeKernel(kernelDebye.getCommandQueue(), kernelDebye.getKernel(), 1, null, global_work_size, local_work_size, 0, null, null);
	      
	      queue.flush();
	      
	      // Wait until all commands have completed
      	queue.finish();

	      //System.out.println("Debye Time = "+(double)(System.currentTimeMillis()-currStartTime)/(double)1000);		

	      try
				{
					reduceBuffer(queue, outputBuffer, (int)numberOfGroups, Iq, qIndex, qSizeCurr);
				}
				catch (GPUException e)
				{
					throw new SasRuntimeException(e);
				}

	      //release the q value buffer
	      qValuesBuffer.release();
	      
	      //release the fa value buffer
	      formFactorsBuffer.release();
	    }
    }
    finally //always clear out the buffers, even if crashed
    {
    	if (qValuesBuffer !=null && !qValuesBuffer.isReleased())
    		qValuesBuffer.release();
    	if (formFactorsBuffer !=null && !formFactorsBuffer.isReleased())
    		formFactorsBuffer.release();
    	
    	if (outputBuffer!=null && !outputBuffer.isReleased())
    		outputBuffer.release();
    	if (atomPositionBuffer!=null && !atomPositionBuffer.isReleased())
    		atomPositionBuffer.release();
    	
    	if (queue!=null && !queue.isReleased())
    		queue.release();
    }
    
    //end timing
    endTime = System.currentTimeMillis();
    double lastProfileComputeTime = (endTime-startTime)/1000.0;
    
    return new ScatteringPrediction(new ScatteringProfile(qValues, Iq, null), lastProfileComputeTime);
	}

	@Override
	public ScatteringPrediction computeProfile(Molecule fromMolecule, Molecule toMolecule) throws SasRuntimeException
  {
  	throw new UnsupportedOperationException("Computation of scatting between to molecules not supproted yet.");
  }
	
	@Override
	public ArrayList<ScatteringPrediction> computeProfiles(List<? extends Molecule> molecules) throws SasRuntimeException
	{
		ArrayList<ScatteringPrediction> profiles = new ArrayList<ScatteringPrediction>(molecules.size());
		
		for (Molecule mol : molecules)
		{
			profiles.add(computeProfile(mol));
		}
		
		return profiles;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable
	{
		try
		{
			release();
		}
		finally
		{
			super.finalize();
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#getErrorBound()
	 */
	@Override
	public double getErrorBound()
	{
		return 0;
	}

	/**
	 * Gets the float atom positions.
	 * 
	 * @param atomPositions
	 *          the atom positions
	 * @return the float atom positions
	 */
	private float[] getFloatAtomPositions(double[][] atomPositions)
  {
		float[] floatAtomPositions = new float[atomPositions.length*3];
		int numberAtoms = atomPositions.length;
		
		for (int atomIter=0; atomIter<numberAtoms; atomIter++)
		{
			floatAtomPositions[0*numberAtoms+atomIter] = (float)atomPositions[atomIter][0];
			floatAtomPositions[1*numberAtoms+atomIter] = (float)atomPositions[atomIter][1];
			floatAtomPositions[2*numberAtoms+atomIter] = (float)atomPositions[atomIter][2];
		}
		
		return floatAtomPositions;
  }

	/**
	 * Gets the float q form factor array.
	 * 
	 * @param molecule
	 *          the molecule
	 * @param qIndex
	 *          the q index
	 * @param numQ
	 *          the num q
	 * @return the float q form factor array
	 */
	private float[] getFloatQFormFactorArray(Molecule molecule, int qIndex, int numQ)
  {
		int molSize = molecule.size();
		float[] formFactorArray = new float[molSize*numQ];
		
		for (int qIter=0; qIter<numQ; qIter++)
			for (int iter=0; iter<molSize; iter++)
				formFactorArray[qIter*molSize+iter] = (float)this.formFactorTable.getFormFactor(molecule, iter, qIndex+qIter);
		
  	return formFactorArray;
  }
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#getFormFactorTable()
	 */
	@Override
	public synchronized FormFactorTable getFormFactorTable()
	{
		return this.formFactorTable;
	}


	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.gpu.CLResource#isReleased()
	 */
	@Override
	public synchronized boolean isReleased()
	{
		return this.kernel.isReleased();
	}


	private void reduceBuffer(CLCommandQueue queue, CLBuffer<FloatBuffer> sharedBuffer, int numberGroups, double[] Iq, int qIndex, int qSize) throws GPUException
  {
    float[] IqBuffer = new float[numberGroups*qSize];
    
    queue.putReadBuffer(sharedBuffer, true);
    queue.finish();    
    sharedBuffer.getBuffer().get(IqBuffer,0,numberGroups*qSize).rewind();
    //clEnqueueReadBuffer(kernelDebye.getCommandQueue(), sharedBuffer, CL_TRUE, 0, Sizeof.cl_float*numberGroups*qSize, Pointer.to(IqBuffer), 0, null, null);
    //clFinish(kernelDebye.getCommandQueue());
    
    for (int qIter=0; qIter<qSize; qIter++)
    {
    	Iq[qIndex+qIter] = 0.0;
    	
    	int offset = qIter*numberGroups;
	    for (int iter=0; iter<numberGroups; iter++)
	    {
	    	Iq[qIndex+qIter] += IqBuffer[offset+iter];
	    }
    }
  }


	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.gpu.CLResource#release()
	 */
	@Override
	public synchronized void release()
	{
		if (!this.kernel.isReleased())
			this.kernel.release();
		if (!this.program.isReleased())
			this.program.release();
	}


	/**
	 * Sets the form factor table.
	 * 
	 * @param formFactorTable
	 *          the new form factor table
	 */
	public synchronized void setFormFactorTable(FormFactorTable formFactorTable)
	{
		this.formFactorTable = formFactorTable;
	}
}
