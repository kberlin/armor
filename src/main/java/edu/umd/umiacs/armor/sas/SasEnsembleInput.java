/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.refinement.AbstractLinearEnsembleInput;

public class SasEnsembleInput extends AbstractLinearEnsembleInput
{
	private final ScatteringProfile experimentalValues;
	private final ScatteringPredictor predictor;
	/**
	 * 
	 */
	private static final long serialVersionUID = 6328844350561136634L;
	
	public SasEnsembleInput(ScatteringProfile experimentalValues, ScatteringPredictor predictor)
	{
		super(false);
		
		this.predictor = predictor;
		this.experimentalValues = experimentalValues;
	}
	
	@Override
	public ScatteringProfile getExperimentalData()
	{
		return this.experimentalValues;
	}

	@Override
	public double[] predict(Molecule mol) throws AtomNotFoundException
	{
		if (this.predictor==null)
			throw new SasRuntimeException("No predictor set.");
		
		return this.predictor.computeProfile(mol).getProfile().values();
	}
	
	public SasEnsembleInput cut(double maxQ)
	{
		int count = 0;
		ArrayList<ScatteringDatum> newList = new ArrayList<ScatteringDatum>();
		for (ScatteringDatum datum : this.experimentalValues)
			if (datum.getQ()<=maxQ)
			{
				newList.add(datum);
				count++;
			}
			else
				break;
		
		if (count==0)
			return null;
		
		//if contains all values, just return the original profile
		if (count==this.experimentalValues.size())
			return this;
		
		try
		{
			//create the new input data
			SasEnsembleInput newInput = new SasEnsembleInput(new ScatteringProfile(newList), this.predictor);
			
			//get current matrix
			RealMatrix A = getPredictionMatrix();
			
			//add the data
			newInput.addPdbModels(getPdbFiles(), this.getPredictionMatrix().getSubMatrix(0, count-1, 0, A.numColumns()-1));

			return newInput;
		}
		catch (PdbException e)
		{
			throw new SasRuntimeException("Unexpected SAS error.", e);
		}	
	}

}