/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.io.Serializable;

public class ScatteringPrediction implements Serializable
{
	private double computeTime;
	private ScatteringProfile profile;
	private double relativeErrorEstimate;
	/**
	 * 
	 */
	private static final long serialVersionUID = -3905051131871358624L;
	
	public ScatteringPrediction(ScatteringPrediction prediction)
	{
		this.profile = prediction.profile;
		this.computeTime = prediction.computeTime;
		this.relativeErrorEstimate = prediction.relativeErrorEstimate;
	}

	public ScatteringPrediction(ScatteringProfile profile)
	{
		this(profile, Double.NaN, Double.NaN);
	}
	

	public ScatteringPrediction(ScatteringProfile profile, double computeTime)
	{
		this(profile, computeTime, Double.NaN);
	}
	
	public ScatteringPrediction(ScatteringProfile profile, double computeTime, double relativeErrorEstimate)
	{
		this.profile = profile;
		this.computeTime = computeTime;
		this.relativeErrorEstimate = relativeErrorEstimate;
	}
	
	public double averageRelativeError(ScatteringPrediction referenceProfile)
	{
		return this.getProfile().averageRelativeError(referenceProfile.getProfile());
	}
	
	public double getComputeTime()
	{
		return this.computeTime;		
	}
	
	public ScatteringProfile getProfile()
	{
		return this.profile;
	}
	
	public double getRelativeErrorEstimate()
	{
		return this.relativeErrorEstimate;
	}

	public double maxRelativeError(ScatteringPrediction referenceProfile)
	{
		//TODO FIX
		return this.getProfile().averageRelativeError(referenceProfile.getProfile());
	}

	public void setRelativeError(double error)
	{
		this.relativeErrorEstimate = error;
	}
}
