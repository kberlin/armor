package edu.umd.umiacs.armor.sas;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BasicUtils;

public class CustomFormFactorTable implements FormFactorTable
{
	private final double[][] formFactorValues;
	private final double[][] formFactorValuesTo;
	private final double[] qValues;

	public CustomFormFactorTable(double[] qValues, double[][] formFactorValues)
	{
		if (qValues==null)
			throw new SasRuntimeException("q values cannot be null.");
		
		if (qValues.length!=formFactorValues[0].length)
			throw new SasRuntimeException("q values length must match form factor size.");
		
		this.qValues = qValues.clone();
		
		this.formFactorValues = BasicUtils.copyArray(formFactorValues);
		this.formFactorValuesTo = null;
	}
	
	public CustomFormFactorTable(double[] qValues, double[][] formFactorValuesFrom, double[][] formFactorValuesTo)
	{
		if (qValues==null)
			throw new SasRuntimeException("q values cannot be null.");
		
		if (qValues.length!=formFactorValuesFrom[0].length)
			throw new SasRuntimeException("q values length must match form factor size.");
		if (qValues.length!=formFactorValuesTo[0].length)
			throw new SasRuntimeException("q values length must match form factor size.");
		
		this.qValues = qValues.clone();
		
		this.formFactorValues = BasicUtils.copyArray(formFactorValuesFrom);
		this.formFactorValuesTo = BasicUtils.copyArray(formFactorValuesTo);
	}

	@Override
	public double getFormFactor(Molecule molecule, int atomIndex, int qIndex)
	{
		if (molecule.size()<=atomIndex)
			throw new SasRuntimeException("Molecule size ("+molecule.size()+") is smaller than atomIndex ("+atomIndex+").");

		if (this.formFactorValues.length<=atomIndex)
			throw new SasRuntimeException("Form factor table size ("+this.formFactorValues.length+") smaller than atomIndex ("+atomIndex+").");

		return this.formFactorValues[atomIndex][qIndex];
	}

	@Override
	public double[][] getFormFactorArray(Molecule molecule)
	{
		if (molecule.size()!=this.formFactorValues.length)
			throw new SasRuntimeException("Form factor table size "+this.formFactorValues.length+" does not match molecule size "+molecule.size()+".");
		
		return BasicUtils.copyArray(this.formFactorValues);
	}

	@Override
	public double[][] getFormFactorArrayFrom(Molecule molecule)
	{
		if (molecule.size()!=this.formFactorValues.length)
			throw new SasRuntimeException("Form factor table size "+this.formFactorValues.length+" does not match from molecule size "+molecule.size()+".");
		
		return BasicUtils.copyArray(this.formFactorValues);
	}

	@Override
	public double[][] getFormFactorArrayTo(Molecule molecule)
	{
		if (molecule.size()!=this.formFactorValuesTo.length)
			throw new SasRuntimeException("Form factor table size "+this.formFactorValuesTo.length+" does not match to molecule size "+molecule.size()+".");
		
		return BasicUtils.copyArray(this.formFactorValuesTo);
	}

	@Override
	public double getFormFactorFrom(Molecule molecule, int atomIndex, int qIndex)
	{
		if (molecule.size()<=atomIndex)
			throw new SasRuntimeException("Molecule size ("+molecule.size()+") is smaller than atomIndex ("+atomIndex+").");

		if (this.formFactorValues.length<=atomIndex)
			throw new SasRuntimeException("Form factor table size ("+this.formFactorValues.length+") smaller than atomIndex ("+atomIndex+").");

		return this.formFactorValues[atomIndex][qIndex];
	}

	@Override
	public double getFormFactorTo(Molecule molecule, int atomIndex, int qIndex)
	{
		if (molecule.size()<=atomIndex)
			throw new SasRuntimeException("Molecule size ("+molecule.size()+") is smaller than atomIndex ("+atomIndex+").");

		if (this.formFactorValuesTo.length<=atomIndex)
			throw new SasRuntimeException("Form factor table size ("+this.formFactorValuesTo.length+") smaller than atomIndex ("+atomIndex+").");

		return this.formFactorValuesTo[atomIndex][qIndex];
	}

	@Override
	public double getMaxQ()
	{
		return BasicMath.max(this.qValues);
	}

	@Override
	public double getMinQ()
	{
		return BasicMath.min(this.qValues);
	}

	@Override
	public double getQ(int index)
	{
		return this.qValues[index];
	}

	@Override
	public double[] getQArray()
	{
		return this.qValues.clone();
	}

	@Override
	public int sizeQ()
	{
		return this.qValues.length;
	}

}
