/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BasicUtils;

/**
 * The Class HierarchicalScatteringPredictor.
 */
public class HierarchicalScatteringPredictor extends AbstractScatteringPredictor implements DifferentiableScatteringPredictor
{
	public enum Method
	{
		DIRECT,
		HIERARCHICAL,
		MIDDLEMAN;
	}
	
	
	private final ProcessBuilder binaryProcess;
	
	private final double eps;
	
	private final Method method;

	private final FormFactorTable table;
	
	private final boolean test;

	public static final int MAX_TREE_LEVEL = 6;
	
	public static final int MIN_TREE_LEVEL = 2;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2799734636206085157L;

	public static int computeHierachicalTreeLevel(int numAtoms, double q, double maxAtomDistance)
	{
		if (numAtoms<10)
			return 1;
		
		double val = Math.floor(0.5*Math.log(4.0*Math.log(2.0)*((double)numAtoms)/(maxAtomDistance*q))/Math.log(2.0))-1.0;
		
		int intVal = (int)val;
    if (intVal>MAX_TREE_LEVEL)
    	intVal = MAX_TREE_LEVEL;
    if (intVal<MIN_TREE_LEVEL)
    	intVal = MIN_TREE_LEVEL;
    
		return intVal;
	}

	public HierarchicalScatteringPredictor(FormFactorTable table, double eps)
	{
		this(table, eps, Method.HIERARCHICAL);
	}
	
	public HierarchicalScatteringPredictor(FormFactorTable table, double eps, boolean middleman, String binaryDirectory)
	{
		this(table, eps, middleman ? Method.MIDDLEMAN : Method.HIERARCHICAL, false, binaryDirectory);			
	}
	
	public HierarchicalScatteringPredictor(FormFactorTable table, double eps, Method method)
	{
		this(table, eps, method, false);	
	}
	
	public HierarchicalScatteringPredictor(FormFactorTable table, double eps, Method method, boolean test)
	{
		this(table, eps, method, test, new File("./bin").getAbsolutePath());	
	}
	
	public HierarchicalScatteringPredictor(FormFactorTable table, double eps, Method method, boolean test, String binaryDirectory)
	{
		this.table = table;
		this.eps = eps;
		this.method = method;
		this.test = test;
		
		if (BasicUtils.isMac())
		{
			binaryDirectory = binaryDirectory+File.separator+"mac64";
			this.binaryProcess = new ProcessBuilder("./fmm_profile");
			
			Map<String, String> env = this.binaryProcess.environment();
			String lib = env.get("DYLD_LIBRARY_PATH");
			if (lib==null)
				lib = ".";			
			env.put("DYLD_LIBRARY_PATH", "."+File.pathSeparator+lib+File.pathSeparator+binaryDirectory);
		}
		else
		if (BasicUtils.isUnix())
		{
			binaryDirectory = binaryDirectory+File.separator+"linux64";
			this.binaryProcess = new ProcessBuilder("./fmm_profile");
			Map<String, String> env = this.binaryProcess.environment();
			String lib = env.get("LD_LIBRARY_PATH");
			if (lib==null)
				lib = ".";			
			env.put("LD_LIBRARY_PATH", "."+File.pathSeparator+lib+File.pathSeparator+binaryDirectory);
		}
		else
			throw new SasRuntimeException("Unsupported platform.");
				
		//set the default directory
		this.binaryProcess.directory(new File(binaryDirectory));
		
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#computeProfile(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public ScatteringPrediction computeProfile(Molecule molecule) throws SasRuntimeException
	{
		return computeProfile(molecule, null, false, null);
	}
	
  @Override
	public ScatteringPrediction computeProfile(Molecule fromMolecule, Molecule toMolecule) throws SasRuntimeException
  {
  	return computeProfile(fromMolecule, toMolecule, false, null);
  }

	private synchronized DifferentiableScatteringPrediction computeProfile(Molecule molecule, Molecule toMolecule, boolean derivatives, double[][] jacobian) throws SasRuntimeException
	{
		if (molecule==null)
			throw new SasRuntimeException("Molecule cannot be null.");
		
		DifferentiableScatteringPrediction prediction;

		try
		{			
			//start and get the input
			Process p = this.binaryProcess.start();
			OutputStream processInput = p.getOutputStream();
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(processInput));
			//BufferedWriter writerout = new BufferedWriter(new FileWriter("try.txt"));
			
			try
			{			
				//generate output
				writeDebyeFile(molecule, toMolecule, derivatives, writer);
				//writeDebyeFile(molecule, toMolecule, derivatives, writerout);
				
				//output the information to the standard output of the processor
				writer.close();
				//writerout.close();
				
				//read the output
				prediction = readDebyeOutput(p, derivatives, jacobian);
				
				//wait for the process to terminate
				p.waitFor();	
				
				//output the error stream
				outputErrorStream(p);

				if (p.exitValue()!=0)
					throw new SasRuntimeException("Execution terminated with error code "+p.exitValue()+".");
			}
			catch (InterruptedException e)
			{
				throw new SasRuntimeException(e);
			}
			finally
			{
				//output the error stream
				outputErrorStream(p);
				
				p.destroy();
			}
		} 
		catch (IOException e)
		{
			throw new SasRuntimeException(e);
		}
		catch (NullPointerException e)
		{
			throw new SasRuntimeException(e.getMessage(), e);
		}
		
		return prediction;
	}
	
	@Override
	public DifferentiableScatteringPrediction computeProfileAndJacobian(Molecule molecule) throws SasRuntimeException
	{
    double[][] jacobian = new double[this.table.sizeQ()][];
  	
  	for (int iter=0; iter<this.table.sizeQ(); iter++)
  		jacobian[iter] = new double[molecule.size()*3];

  	return computeProfile(molecule, null, true, jacobian);
	}
	
	@Override
	public DifferentiableScatteringPrediction computeProfileAndJacobian(Molecule molecule, double[][] jacobian) throws SasRuntimeException
	{
		return computeProfile(molecule, null, true, jacobian);
	}
	
	@Override
	public ArrayList<ScatteringPrediction> computeProfiles(List<? extends Molecule> molecules) throws SasRuntimeException
	{
		ArrayList<ScatteringPrediction> profiles = new ArrayList<ScatteringPrediction>(molecules.size());
		
		for (Molecule mol : molecules)
		{
			profiles.add(computeProfile(mol));
		}
		
		return profiles;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#getErrorBound()
	 */
	@Override
	public double getErrorBound()
	{
		return this.eps;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#getFormFactorTable()
	 */
	@Override
	public FormFactorTable getFormFactorTable()
	{
		return this.table;
	}

	private void outputErrorStream(Process p) throws IOException
	{
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		try
		{
			while (reader.ready())
			{
				String val = reader.readLine();

				//break if end of file
				if (val==null)
					break;
				
				System.err.println(val);
			}
		}
		finally
		{			
			reader.close();
		}
	}
	
	private DifferentiableScatteringPrediction readDebyeOutput(Process p, boolean derivatives, double[][] J) throws SasRuntimeException, IOException, InterruptedException 
	{
		double[] IqValues = new double[this.table.sizeQ()];

		BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));

		Scanner scanner = new Scanner(stdInput);
		scanner.useLocale(Locale.US);

		try
		{
			while(!stdInput.ready())
			{
				try
				{
					if (p.exitValue()!=0)
						throw new SasRuntimeException("Execution terminated with error code "+p.exitValue()+".");
				}
				catch (IllegalThreadStateException e) {}
				
				this.wait(200);
			}
			
		  //read error flag
			int ierr;
			try
			{
				ierr = scanner.nextInt();
			}
			catch (NoSuchElementException e)
			{
				throw new SasRuntimeException("Execution failed with no error code output.", e);
			}

			if (ierr!=0)
			{
				throw new SasRuntimeException("Execution failed with error code "+ierr+".");
			}		
			
			double lastProfileComputeTime;
			@SuppressWarnings("unused")
			double initTime;
			//@SuppressWarnings("unused")
			double computeTime;

			//read timing result
			lastProfileComputeTime = scanner.nextDouble();
	
			//read init time
			initTime = scanner.nextDouble();

			//read compute time
			computeTime = scanner.nextDouble();
			
			//read output result
			for (int iter=0; iter<this.table.sizeQ(); iter++)
				IqValues[iter] = scanner.nextDouble();
			
			//read the jacobian value
			if (derivatives)
			{
				for (int qIter=0; qIter<this.table.sizeQ(); qIter++)
					for (int columnIter=0; columnIter<J[qIter].length; columnIter++)
						J[qIter][columnIter] = scanner.nextDouble();
			}

			//close the reader
			scanner.close();

			//store the actual compute time
			lastProfileComputeTime = computeTime;
			
			return new DifferentiableScatteringPrediction(new ScatteringProfile(this.table.getQArray(), IqValues),lastProfileComputeTime, 0.0,
				 (J==null) ? null : new BlockRealMatrix(J), 0.0);
		}
		finally
		{
			scanner.close();
		}
	}

	private void writeDebyeFile(Molecule molecule, Molecule toMolecule, boolean computeDerivatives, Writer writer) throws IOException 
	{
		double maxAtomDistance = molecule.maxAtomDistance();
		
		int methodValue = 0;
		switch (this.method)
		{
			case DIRECT : methodValue = 1; break;
			case MIDDLEMAN : methodValue = 2; break;
			case HIERARCHICAL : methodValue = 0; break;
			default: break;
		}
		
		//output the size values
		writer.append(""+this.eps+" "+methodValue+" "+(computeDerivatives?1:0)+" "+(this.test?1:0)+" ");
		writer.append(this.table.sizeQ()+" "+1+"\n");
		
		//output the q information
		for (int iter=0; iter<this.table.sizeQ(); iter++)
			writer.append(""+this.table.getQ(iter)+" ");
		writer.append("\n");
		
		//output the L information
		for (int iter=0; iter<this.table.sizeQ(); iter++)
			writer.append(""+computeHierachicalTreeLevel(molecule.size(), this.table.getQ(iter), maxAtomDistance)+" ");
		writer.append("\n");

		//molecule specific information
		if (toMolecule == null)
		{
			writer.append(""+molecule.size()+" "+0+"\n");		
			writeMolecule(molecule, this.table.getFormFactorArray(molecule), writer);
		}
		else
		{
			writer.append(""+molecule.size()+" "+toMolecule.size()+"\n");		

			writeMolecule(molecule, this.table.getFormFactorArrayFrom(molecule), writer);
			writer.append("\n");
			writeMolecule(toMolecule, this.table.getFormFactorArrayTo(toMolecule), writer);
			writer.append("\n");
		}
	}
	
	private String writeMolecule(Molecule molecule, double[][] tableValues, Writer writer) throws IOException
	{
		//output the molecule
		for (int iterAtom=0; iterAtom<molecule.size(); iterAtom++)
		{
			Point3d p = molecule.getAtomPosition(iterAtom);

			writer.append(""+p.x+" "+p.y+" "+p.z+" ");
			for (int iter=0; iter<this.table.sizeQ(); iter++)
			{
				double factor = tableValues[iterAtom][iter];
				writer.append(""+factor+" ");
			}
			writer.append("\n");
		}

		return writer.toString();
	}

}
