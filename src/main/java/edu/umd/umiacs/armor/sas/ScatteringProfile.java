/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.MathRuntimeException;
import edu.umd.umiacs.armor.math.interpolation.CubicSpline;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.linear.SingularValueDecomposition;
import edu.umd.umiacs.armor.math.optim.sparse.MultiOrthogonalMatchingPursuit;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.util.AbstractExperimentalData;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.DatasetComplex;
import edu.umd.umiacs.armor.util.DatasetDouble;
import edu.umd.umiacs.armor.util.SortablePair;

/**
 * The Class ScatteringProfile.
 */
public final class ScatteringProfile extends AbstractExperimentalData<ScatteringDatum>
{
	/** The pr profile cached. */
	private DistanceProfile prProfileCached;

	private static final double MIN_Q_DIFFERENCE = 1.0e-8;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8874750117966313772L;
	
	private static ArrayList<ScatteringDatum> generateDataSet(double[] qValues, double[] IqValues, double[] IqError)
	{
		if (BasicUtils.hasNaN(qValues) || BasicUtils.hasNaN(IqValues) || BasicUtils.hasNaN(IqError))
			throw new SasRuntimeException("Profile values cannot have NaNs.");
		
		if (qValues.length != IqValues.length)
			throw new SasRuntimeException("Number of Iq ("+IqValues.length+") values must match number of q ("+qValues.length+") values.");			
		
		ArrayList<ScatteringDatum> profile = new ArrayList<ScatteringDatum>(qValues.length);
		
		//sort the list
		for (int iter=0; iter<qValues.length; iter++)
		{
			if (IqError!=null)
				profile.add(new ScatteringDatum(qValues[iter], IqValues[iter], IqError[iter]));
			else
				profile.add(new ScatteringDatum(qValues[iter], IqValues[iter]));
		}
		
		//sort the profile
		Collections.sort(profile);

		//check if has same q values
		if (profile.size()>1)
		{
			Iterator<ScatteringDatum> iterator = profile.iterator();
			ScatteringDatum prevValue = iterator.next();
			while (iterator.hasNext())
			{
				ScatteringDatum nextValue = iterator.next();
				if (nextValue.getQ()-prevValue.getQ()<MIN_Q_DIFFERENCE)
					throw new SasRuntimeException("Same profile cannot have multiple Iq values: "+ nextValue.getQ());
			}
		}
			
		return profile;
	}
	
	public static ScatteringProfile readProfile(String fileName) throws IOException, SasRuntimeException
	{
		double[][] data = readProfileFile(fileName);
		
		if (data[0].length<2)
			throw new IOException("Invalid number of columns in file.");
    
    double[] qArray = new double[data.length];
    double[] IqArray = new double[data.length];
    double[] IqErrorArray = null;
    
    //assume third column is error
    if (data[0].length>=3)
    	IqErrorArray = new double[data.length];
    
    for (int iter=0; iter<data.length; iter++)
    {
    	qArray[iter] = data[iter][0];
    	IqArray[iter] = data[iter][1];
    	
    	if (IqErrorArray!=null)
    		IqErrorArray[iter] = data[iter][2];
    }
    
    return new ScatteringProfile(qArray, IqArray, IqErrorArray);
	}

	public static double[][] readProfileFile(String fileName) throws IOException
	{
		FileInputStream fstream = new FileInputStream(fileName);
		DataInputStream in = new DataInputStream(fstream);
	  BufferedReader br = new BufferedReader(new InputStreamReader(in));
	  
	  ArrayList<ArrayList<Double>> list = new ArrayList<ArrayList<Double>>();

    Scanner scanner = new Scanner(br);
    scanner.useLocale(Locale.US);
    
    while(scanner.hasNext())
    {
    	Scanner lineScanner = new Scanner(scanner.nextLine());
    	ArrayList<Double> currList = new ArrayList<Double>();
    	
    	try
    	{
    		while(lineScanner.hasNext())
    		{
    			double curr = lineScanner.nextDouble();
    			currList.add(curr);
    		}

    		if (!currList.isEmpty())
      		list.add(currList);
    	}
    	catch(InputMismatchException e)
    	{
    		//the line does not have a double on it so go to next line
    	}
    	finally
    	{
    		lineScanner.close();
    	}
    }
    
    scanner.close();
    
    if (list.isEmpty())
    	return null;
    
    double[][] data = new double[list.size()][list.get(0).size()];
    for (int rowIter=0; rowIter<list.size(); rowIter++)
    {
    	if (list.get(rowIter).size()!=data[rowIter].length)
    		throw new IOException("Invalid file format.");    		
    		
    	for (int colIter=0; colIter<list.get(rowIter).size(); colIter++)
    		data[rowIter][colIter] = list.get(rowIter).get(colIter).doubleValue();
    }
    
    return data;		
	}
	
	public ScatteringProfile(double[] qValues, double[] IqValues) throws SasRuntimeException
	{
		this(qValues,IqValues, null);
	}
	
	public ScatteringProfile(double[] qValues, double[] IqValues, double[] IqError) throws SasRuntimeException
	{
		super(generateDataSet(qValues, IqValues, IqError), false);
		this.prProfileCached = null;
	}

	public ScatteringProfile(List<ScatteringDatum> data)
	{
		this(data, true);
	}
		
	private ScatteringProfile(List<ScatteringDatum> data, boolean copy)
	{
		super(data, copy);
		this.prProfileCached = null;
	}
		
	public ScatteringProfile(ScatteringProfile profile)
	{
		super(profile.getDataRef());		
		this.prProfileCached = profile.prProfileCached;
	}	

	/**
	 * Abs fft.
	 * 
	 * @return the dataset double
	 */
	public DatasetDouble absFft()
	{
		DatasetComplex result = fft();

		//compute the frequency values
		DatasetDouble absValues = new DatasetDouble();
		for (int iter=0; iter<result.size()/2+1; iter++)
		{
			SortablePair<Double, Complex> val = result.getValue(iter);
			absValues.add(new SortablePair<Double,Double>(val.x, val.y.abs()));
		}
		
		return absValues;
	}
	
	public ScatteringProfile add(double val)
	{
		ArrayList<ScatteringDatum> data = new ArrayList<ScatteringDatum>(size());
		
		for (ScatteringDatum datum : this)
		{
			data.add(datum.add(val));
		}
		
		return new ScatteringProfile(data, false);
	}
	
	public ScatteringProfile add(ScatteringProfile profile)
	{
		if (size()!=profile.size())
			throw new SasRuntimeException("Profile sizes must be equal.");
		
		ArrayList<ScatteringDatum> data = new ArrayList<ScatteringDatum>(size());
		
		for (int iter=0; iter<this.size(); iter++)
		{
			ScatteringDatum datum = get(iter);
			
			if (Math.abs(datum.getQ()-profile.get(iter).getQ())>MIN_Q_DIFFERENCE)
				throw new SasRuntimeException("Profile q values must be equal.");
			
			data.add(new ScatteringDatum(datum.getQ(), datum.add(profile.get(iter).getIq())));
		}
		
		return new ScatteringProfile(data, false);
	}
	
	/*
	public Profile adjustByPredictionAndBackground(Profile prediction, Profile background)
	{
		Profile backgroundInterp = background.interpolate(qValues);
		Profile predictionInterp = prediction.scaledProfile(this).interpolate(qValues);
		
		//store the I_exp = [Iq_pred Iq_back 1]*[x(1) x(2) x(3)]' arrays
		double[][] A = new double[qValues.length][3];
		double[] b = new double[qValues.length];
		for (int iter=0; iter<qValues.length; iter++)
		{
			double currErr = 1.0;
			if (IqError!=null)
				currErr = IqError[iter];
			
			A[iter][0] = predictionInterp.IqValues[iter]/currErr;
			A[iter][1] = backgroundInterp.IqValues[iter]/currErr;
			A[iter][2] = 1.0/currErr;
			b[iter] = IqValues[iter]/currErr;
		}
		
		//compute the optimal solution
		double[] x = BasicMath.solveSVD(A, b, 1.0e-8);
		
		System.out.println(BasicUtils.arrayToString(x));
		
		Profile newProfile = new Profile(this);
		newProfile = newProfile.add(-x[1]);
		
		return newProfile;
	}
	*/
		
	/**
	 * Average relative error.
	 * 
	 * @param referenceProfile
	 *          the profile
	 * @return the double
	 */
	public double averageRelativeError(ScatteringProfile referenceProfile)
  {
		if (size()!=referenceProfile.size())
			throw new SasRuntimeException("Profile sizes must be equal.");

		double errorSum = 0.0;
    for(int iter=0; iter<size(); iter++)
    	errorSum += Math.abs(get(iter).value()-referenceProfile.get(iter).value())/(referenceProfile.get(iter).value());
    
    errorSum = errorSum/(double)size();
    return errorSum;
  }
  
  public double bufferScaling(ScatteringProfile predictedProfile, ScatteringProfile bufferProfile) throws SasRuntimeException
	{
		//figure out the bounds
		double minQ = Math.max(Math.max(minQ(), predictedProfile.minQ()), bufferProfile.minQ());
		double maxQ = Math.min(Math.min(maxQ(), predictedProfile.maxQ()), bufferProfile.maxQ());
		double[] qValues = BasicUtils.uniformLinePoints(minQ, maxQ, this.size());

		//put everythign on the same number line
		ScatteringProfile iterp = this.interpolate(qValues);
		ScatteringProfile interpPredicted = predictedProfile.interpolate(qValues);
		ScatteringProfile interpBuffer = bufferProfile.interpolate(qValues);
				
		//generate the b=A*x arrays
		double[][] A = new double[qValues.length][3];
		double[] b = new double[qValues.length];
		
		//first rescale by the mean value to avoid bad conditioning in SVD
		double scaling = BasicStat.mean(iterp.values())/BasicStat.mean(interpPredicted.values());
		double[] newIqValues = BasicMath.mult(interpPredicted.values(), scaling);
		
		//store the b_i = [Iq_i 1]*[x(0) x(1) x(2)]' arrays
		for (int iter=0; iter<qValues.length; iter++)
		{
			double currErr = 1.0;
			if (predictedProfile.get(iter).hasError())
				currErr = predictedProfile.get(iter).error();
			
			A[iter][0] = newIqValues[iter]/currErr;
			A[iter][1] = 1.0/currErr;
			A[iter][2] = interpBuffer.get(iter).value()/currErr;
			b[iter] = iterp.get(iter).value()/currErr;
		}
		
		//compute the optimal solution
		double[] x = BasicMath.solveSVD(A, b, 1.0e-10);
		
		return -x[2]; 
	}
  

  public double chi2(ScatteringProfile profile) throws SasRuntimeException
  {
  	if (hasErrors())
  		return BasicStat.chi2(values(), profile.values(), errors());
  	else
  		return BasicStat.chi2(values(), profile.values());
  }
  
  public ScatteringProfile computeArtifacts()
	{
		int smooth = 1;
				
		ArrayList<ScatteringDatum> newSet = new ArrayList<ScatteringDatum>();
		for (int iter=smooth; iter<size()-smooth; iter++)
		{
			double val = get(iter).getIq();			
			for (int smoothIter=1; smoothIter<=smooth; smoothIter++)
				val += this.get(iter-smoothIter).value()+this.get(iter+smoothIter).value();
			val = val/(2.0*(double)smooth+1.0);
			
			newSet.add(new ScatteringDatum(this.get(iter).getQ(), val));
		}
		ScatteringProfile interpProfile = new ScatteringProfile(newSet, false);
		
		int n = interpProfile.size();
		double maxFreq = maxFrequency()/4.0;
		double[] r = BasicUtils.uniformSpacedPoints(0.0, 
				maxFreq, 
				0.25*maxFreq/(double)(n-1));

		//allocate space for the inversion matrix
		double[][] A = new double[interpProfile.size()][r.length];
		double[] b = interpProfile.values();
		
		//init A
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = BasicMath.sinc(interpProfile.get(row).getQ()*r[col]);

		//double[] Pr = BasicMath.solveSVD(A, b, 8.0e-3);
		MultiOrthogonalMatchingPursuit pursuit = new MultiOrthogonalMatchingPursuit(1000);
		if (hasErrors())
			pursuit.setRelativeErrorTol(BasicMath.norm(errors())*2.0);
		else
			pursuit.setRelativeErrorTol(BasicMath.norm(values())*1.0e-4);
		
		pursuit.setA(new Array2dRealMatrix(A, false));
		double[] Pr = pursuit.solve(b);
		
		DistanceProfile pr = new DistanceProfile(new DatasetDouble(r, Pr).createNonZero());
		
		return interpProfile.subtract(pr.prModelProfile(interpProfile.getQValues()));
	}
	
	public double computeOptimalChi2(ScatteringProfile profile) throws SasRuntimeException
  {
  	return 0.0;
  }
  
  /**
	 * Compute optimal residuals.
	 * 
	 * @param profile
	 *          the profile
	 * @return the double[]
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
  public double[] computeOptimalResiduals(ScatteringProfile profile) throws SasRuntimeException
  {
  	return null;
  }
  
	public DistanceProfile computePr()
  {
  	return computePrBasisPersuit(BasicUtils.uniformSpacedPoints(0.0, 0.5*maxFrequency(), 0.20*maxFrequency()/(double)(this.size()-1)));
  }
  
  public DistanceProfile computePrBasisPersuit(double[] r)
	{
		ScatteringProfile interpProfile = this;
		
		//allocate space for the inversion matrix
		double[][] A = new double[interpProfile.size()][r.length];
		double[] y = interpProfile.values();
		
		//init A
		for (int row=0; row<A.length; row++)
		{
			if (hasErrors())
				y[row] = y[row]/get(row).error();
			for (int col=0; col<A[row].length; col++)
			{
				A[row][col] = BasicMath.sinc(interpProfile.get(row).getQ()*r[col]);
				if (hasErrors())
					A[row][col] = A[row][col]/(row);
			}
		}
		
		MultiOrthogonalMatchingPursuit pursuit = new MultiOrthogonalMatchingPursuit(1000, true);
		pursuit.setRelativeErrorTol(1.0e-3);
		pursuit.setA(new Array2dRealMatrix(A, false));
		double[] Pr = pursuit.solve(y);
		
		this.prProfileCached = new DistanceProfile(new DatasetDouble(r, Pr));
		
		if (this.prProfileCached.size()==0)
			throw new SasRuntimeException("P(r) transformed return zero value");
		
		return this.prProfileCached;
	}
	
	/**
	 * Compute pr interp.
	 * 
	 * @return the distance profile
	 */
	public DistanceProfile computePrInterp()
	{
		if (this.prProfileCached!=null)
			return this.prProfileCached;
		
		//interpolate onto uniform points
		ScatteringProfile interpProfile = this.interpolate(BasicUtils.uniformLinePoints(this.minQ(), this.maxQ(), this.size()));
		
		int n = interpProfile.size()/2;
		double fs = ((double)(interpProfile.size()-1))/(interpProfile.maxQ()-interpProfile.minQ());
		double freqStep = 1.0*BasicMath.PI*fs/interpProfile.size();
		
		double[] r = new double[n];
		for (int iter=0; iter<n; iter++)
			r[iter] = freqStep*(double)iter;

		//allocate space for the inversion matrix
		double[][] A = new double[interpProfile.size()][n];
		double[] Pr = new double[n];		
		double[] b = interpProfile.values();
		
		//init A
		for (int row=0; row<A.length; row++)
		{
			for (int col=0; col<A[row].length; col++)
			{
				A[row][col] = BasicMath.sinc(interpProfile.get(row).getQ()*r[col]);
			}
		}
		
		//already a well formed matrix
		SingularValueDecomposition svdA = (new BlockRealMatrix(A)).svd();
		System.out.println("cond="+svdA.cond()+" rank="+svdA.rank(1.0e-8)+" columns="+Pr.length);

		//solve for x
		double[] x = (svdA.solve(new BlockRealMatrix(b), 1.0e-8)).getColumn(0);

		//compute the current contribution
	  double[] d = BasicMath.mult(A, x);
	  
	  //form solution
		for (int i=0; i<x.length; i++)
			Pr[i] = x[i];
	
		//compute the residual
		for (int i=0; i<d.length; i++)
			b[i] -= d[i];
		
		this.prProfileCached = new DistanceProfile(new DatasetDouble(r, Pr));
		
		return this.prProfileCached;
	}
			
	public ScatteringProfile cut(double min, double max)
	{
		ArrayList<ScatteringDatum> cutProfile = new ArrayList<ScatteringDatum>();
		for (ScatteringDatum datum : this)
			if (datum.getQ()<min || datum.getQ()>max)
				cutProfile.add(datum);
				
		return new ScatteringProfile(cutProfile, false);
	}
	
	public ScatteringProfile divide(ScatteringProfile profile)
	{
		if (size()!=profile.size())
			throw new SasRuntimeException("Profile sizes must be equal.");
		
		ArrayList<ScatteringDatum> data = new ArrayList<ScatteringDatum>(size());
		
		for (int iter=0; iter<this.size(); iter++)
		{
			ScatteringDatum datum = get(iter);
			
			if (Math.abs(datum.getQ()-profile.get(iter).getQ())>MIN_Q_DIFFERENCE)
				throw new SasRuntimeException("Profile q values must be equal.");
			
			data.add(new ScatteringDatum(datum.getQ(), datum.divide(profile.get(iter).getIq())));
		}
		
		return new ScatteringProfile(data, false);

	}
	    			
	/**
	 * Fft.
	 * 
	 * @return the dataset complex
	 */
	public DatasetComplex fft()
	{
		//create a uniform sample
		ScatteringProfile interpProfile = this.interpolate(BasicUtils.uniformLinePoints(minQ(), maxQ(), size()));
		
		//perform fourier transform
		FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.UNITARY);
		
		double[] powerOfTwoValues = BasicUtils.createArray(BasicMath.closestPowerOfTwo(interpProfile.size()), 0);
		for (int iter=0; iter<interpProfile.size(); iter++)
			powerOfTwoValues[iter] = interpProfile.get(iter).getIq();
		
		//perform the fft
		Complex[] fftValues = fft.transform(powerOfTwoValues, TransformType.FORWARD);

		//compute the frequency values
		double fs = 1.0/(interpProfile.get(1).getQ()-interpProfile.get(0).getQ());
		double freqStep = 2.0*BasicMath.PI*fs/(powerOfTwoValues.length);
		
		DatasetComplex result = new DatasetComplex();
		for (int iter=0; iter<fftValues.length; iter++)
		{
			result.add(iter*freqStep, fftValues[iter]);
		}		
		
		return result;
	}

	public ScatteringProfile filter(double min, double max)
	{
		ArrayList<ScatteringDatum> cutProfile = new ArrayList<ScatteringDatum>();
		for (ScatteringDatum datum : this)
			if (datum.getQ()>=min && datum.getQ()<=max)
				cutProfile.add(datum);
			else
		  if (datum.getQ()>max)
		  	break;
				
		return new ScatteringProfile(cutProfile, false);
	}

	public double[] getIqErrors()
	{
		return this.errors();
	}
	
	public double[] getIqValues()
	{
		return this.values();
	}
	
	public double[] getQValues()
	{
		double[] qArray = new double[size()];
		for (int iter=0; iter<size(); iter++)
			qArray[iter] = get(iter).getQ();
		
		return qArray;
	}
	
	public boolean hasErrors()
	{
		for (ScatteringDatum datum : this)
			if (!datum.hasError())
				return false;
		
		return true;
	}

	public int[] interesectingIndices(double[] qArray)
	{
		return interesectingIndices(qArray, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
	}
		
	/**
	 * Interesecting indices.
	 * 
	 * @param qArray
	 *          the q array
	 * @param lowerBound
	 *          the lower bound
	 * @param upperBound
	 *          the upper bound
	 * @return the int[]
	 */
	public int[] interesectingIndices(double[] qArray, double lowerBound, double upperBound)
	{
		double[] sortedQ =qArray.clone();
		Arrays.sort(sortedQ); 
		
		//find valid range of Q that is inside the current range of values
		double currMinQ = Math.max(lowerBound, Math.max(minQ(), sortedQ[0]));
		double currMaxQ = Math.min(upperBound, Math.min(maxQ(), sortedQ[sortedQ.length-1]));
		
		int numValidQ = 0;
		for (int iter=0; iter<qArray.length; iter++)
			if (qArray[iter]>=currMinQ && qArray[iter]<=currMaxQ)
				numValidQ++;
		
		if (numValidQ==0)
			return null;
		
		//create an array of valid values
		int count = 0;
		int[] validIndex = new int[numValidQ];
		for (int iter=0; iter<qArray.length; iter++)
			if (qArray[iter]>=currMinQ && qArray[iter]<=currMaxQ)
			{
				validIndex[count] = iter;
				count++;
			}
		
		return validIndex;
	}

	public ScatteringProfile interpolate(double[] newQValues) throws SasRuntimeException
	{
		double[] sortedValues = newQValues.clone();
		Arrays.sort(sortedValues);
		
		double[] newIqValues = new double[sortedValues.length];
		double[] newIqErrorValues = null;
		try
		{
			CubicSpline IqSpline = new CubicSpline(getQValues(), values());
			
			CubicSpline errorSpline = null;
			if (hasErrors())
			{
				errorSpline = new CubicSpline(getQValues(), errors());
				newIqErrorValues = new double[sortedValues.length];
			}
			
			for (int iter=0; iter<sortedValues.length; iter++)
			{
				if (sortedValues[iter]>=maxQ() && sortedValues[iter]<=maxQ()+1.0e-8)
				{
					newIqValues[iter] = get(size()-1).value();
					if (hasErrors())
						newIqErrorValues[iter] = get(size()-1).error();					
				}
				else
				if (sortedValues[iter]<=minQ() && sortedValues[iter]>=minQ()-1.0e-8)
				{
					newIqValues[iter] = get(0).value();
					if (hasErrors())
						newIqErrorValues[iter] = get(0).error();			
				}
				else
				{
					newIqValues[iter] = IqSpline.value(sortedValues[iter]);
					if (hasErrors())
						newIqErrorValues[iter] = errorSpline.value(sortedValues[iter]);						
				}
			}
			
		} 
		catch (MathRuntimeException e)
		{
			throw new SasRuntimeException("Cannot interpolate onto the specified grid.", e);
		}
		
		
		return new ScatteringProfile(sortedValues, newIqValues, newIqErrorValues);
	}
	
	public ScatteringProfile interpolate(ScatteringProfile targetProfile)
	{		
	  ScatteringProfile	interpProfile = interpolate(targetProfile.getQValues());
	  
	  return interpProfile;
	}
	
	public ScatteringProfile interpolateAutoRange(ScatteringProfile targetProfile)
	{
		int[] validIndex = interesectingIndices(targetProfile.getQValues());
		
		if (validIndex==null || validIndex.length<3)
			throw new SasRuntimeException("Not enough intersecting points to interpolate the profiles.");
		
		//interpolate the profile onto the target profile's valid range points
		double[] validQValues = new double[validIndex.length];
		for (int iter=0; iter<validIndex.length; iter++)
			validQValues[iter] = targetProfile.get(validIndex[iter]).getQ();		
		
	  ScatteringProfile	interpProfile = interpolate(validQValues);
	  
	  return interpProfile;
	}
	
	/**
	 * Low pass filter.
	 * 
	 * @param maxFrequency
	 *          the max frequency
	 * @return the scattering profile
	 */
	public ScatteringProfile lowPassFilter(double maxFrequency)
	{
		//create a uniform sample
		DatasetComplex fftValues = fft();
		
		//zero the high frequency entries
		Complex[] newFftValues = new Complex[fftValues.size()];
		fftValues.getYList().toArray(newFftValues);
		for (int iter=0; iter<fftValues.size()/2+1; iter++)
		{
			if (fftValues.getX(iter) > maxFrequency)
			{
				newFftValues[iter] = new Complex(0.0, 0.0);
				
				if (iter>0)
					newFftValues[newFftValues.length-iter] = new Complex(0.0, 0.0);
			}
		}
		
		//perform inverse transform
		FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.UNITARY);
		Complex[] fftInverse = fft.transform(newFftValues, TransformType.INVERSE);

		double fs = 1.0/(fftValues.getX(1)-fftValues.getX(0));
		double freqStep = BasicMath.PI*fs*2.0/(fftValues.size());
		
		double[] q = new double[size()];
		double[] val = new double[size()];
		for (int iter=0; iter<val.length; iter++)
		{
			q[iter] = minQ()+freqStep*iter;
			val[iter] = fftInverse[iter].getReal();
		}

		return new ScatteringProfile(q, val);
	}
	
	/**
	 * Max frequency.
	 * 
	 * @return the double
	 */
	public double maxFrequency()
	{
		double maxDiff = Double.MIN_VALUE;
		
		for (int iter=1; iter<size(); iter++)
			maxDiff = Math.max(maxDiff, get(iter).getQ()-get(iter-1).getQ());
		
		return BasicMath.PI/maxDiff;
	}
			
	/**
	 * Max q.
	 * 
	 * @return the double
	 */
	public double maxQ()
	{
		return get(size()-1).getQ();
	}
	
	/**
	 * Max q diff.
	 * 
	 * @return the double
	 */
	public double maxQDiff()
	{
		double maxDiff = 0.0;
		
		for (int iter=1; iter<size(); iter++)
			maxDiff = Math.max(maxDiff, get(iter).getQ()-get(iter-1).getQ());
		
		return maxDiff;		
	}

	/**
	 * Min frequency.
	 * 
	 * @return the double
	 */
	public double minFrequency()
	{
		return BasicMath.PI/(maxQ()-minQ());
	}
	
	/**
	 * Min q.
	 * 
	 * @return the double
	 */
	public double minQ()
	{
		return get(0).getQ();
	}
	
	/**
	 * Min q diff.
	 * 
	 * @return the double
	 */
	public double minQDiff()
	{
		double minDiff = Double.MAX_VALUE;
		
		for (int iter=1; iter<size(); iter++)
			minDiff = Math.min(minDiff, get(iter).getQ()-get(iter-1).getQ());
		
		return minDiff;		
	}

  public ScatteringProfile mult(double val)
	{
		ArrayList<ScatteringDatum> data = new ArrayList<ScatteringDatum>(size());
		
		for (ScatteringDatum datum : this)
		{
			data.add(datum.mult(val));
		}
		
		return new ScatteringProfile(data, false);
	}

	public ScatteringProfile mult(ScatteringProfile profile)
	{
		if (size()!=profile.size())
			throw new SasRuntimeException("Profile sizes must be equal.");
		
		ArrayList<ScatteringDatum> data = new ArrayList<ScatteringDatum>(size());
		
		for (int iter=0; iter<this.size(); iter++)
		{
			ScatteringDatum datum = get(iter);
			
			if (Math.abs(datum.getQ()-profile.get(iter).getQ())>MIN_Q_DIFFERENCE)
				throw new SasRuntimeException("Profile q values must be equal.");
			
			data.add(new ScatteringDatum(datum.getQ(), datum.mult(profile.get(iter).getIq())));
		}
		
		return new ScatteringProfile(data, false);
	}

	/**
	 * Optimize profile scaling.
	 * 
	 * @param targetProfile
	 *          the target profile
	 * @return the scattering profile
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
	public ScatteringProfile optimizeProfileScaling(ScatteringProfile targetProfile) throws SasRuntimeException
	{
		return optimizeProfileScaling(targetProfile, Double.POSITIVE_INFINITY);
	}
	
	/**
	 * Optimize profile scaling.
	 * 
	 * @param targetProfile
	 *          the target profile
	 * @param maxQ
	 *          the max q
	 * @return the scattering profile
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
	public ScatteringProfile optimizeProfileScaling(ScatteringProfile targetProfile, double maxQ) throws SasRuntimeException
	{		
		//put the two profiles on the same q line
	  ScatteringProfile	interpProfile = interpolateAutoRange(targetProfile);
	  ScatteringProfile	interpTargetProfile = targetProfile.interpolateAutoRange(interpProfile);
				
		//generate the b=A*x arrays
		double[][] A = new double[interpProfile.size()][2];
		double[] b = new double[interpProfile.size()];
		
		//first rescale by the mean value to avoid bad conditioning in SVD
		double scaling = BasicStat.mean(targetProfile.values())/BasicStat.mean(interpProfile.values());
		interpProfile = interpProfile.mult(scaling);
		
		//store the b_i = [Iq_i 1]*[x(0) x(1)]' arrays
		for (int iter=0; iter<interpProfile.size(); iter++)
		{
			double currErr = 1.0;
			if (get(iter).hasError())
				currErr = get(iter).error();
			
			A[iter][0] = interpProfile.get(iter).getIq()/currErr;
			A[iter][1] = 1.0/currErr;
			b[iter] = interpTargetProfile.get(iter).getIq()/currErr;
		}
		
		//compute the optimal solution
		double[] x = BasicMath.solveSVD(A, b, 1.0e-10);
		
		ScatteringProfile newProfile = this;
		
		//adjust the new copy of the profile
		newProfile = newProfile.mult(scaling);
		newProfile = newProfile.mult(x[0]);
		newProfile = newProfile.add(x[1]);
		
		return newProfile; 
	}
	
	public double rms(ScatteringProfile profile) throws SasRuntimeException
  {
    return BasicStat.rms(absErrorsByStd(profile.values()));
  }
	
	public ScatteringProfile subtract(ScatteringProfile profile)
	{
		if (size()!=profile.size())
			throw new SasRuntimeException("Profile sizes must be equal.");
		
		ArrayList<ScatteringDatum> data = new ArrayList<ScatteringDatum>(size());
		
		for (int iter=0; iter<this.size(); iter++)
		{
			ScatteringDatum datum = get(iter);
			
			if (Math.abs(datum.getQ()-profile.get(iter).getQ())>MIN_Q_DIFFERENCE)
				throw new SasRuntimeException("Profile q values must be equal.");
			
			data.add(new ScatteringDatum(datum.getQ(), datum.subtract(profile.get(iter).getIq())));
		}
		
		return new ScatteringProfile(data, false);
	}

	public double[][] toArray()
	{
		double[][] a = new double[size()][3];
		
		int counter = 0;
		for (ScatteringDatum datum : this)
		{
			a[counter][0] = datum.getQ();
			a[counter][1] = datum.getIq();
			a[counter][2] = datum.error();
			
			counter++;
		}
		
		return a;
	}
}
