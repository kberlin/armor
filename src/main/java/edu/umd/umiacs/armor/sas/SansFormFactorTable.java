/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.util.HashMap;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.AtomType;
import edu.umd.umiacs.armor.molecule.SolvatedMolecule;

/**
 * The Class SANSFormFactorTable.
 */
public class SansFormFactorTable extends AbstractFormFactorTable
{
	
	/**
	 * The Enum SANSAtomConstants.
	 */
	public enum SansAtomConstants
	{
		
		/** The CARBON. */
		CARBON 			(AtomType.CARBON, 0.6651),
	  
  	/** The DEUTERIUM. */
  	DEUTERIUM 	(AtomType.DEUTERIUM, 0.6674),
	  
  	/** The HYDROGEN. */
  	HYDROGEN 		(AtomType.HYDROGEN, -0.3742),
	  
  	/** The NITROGEN. */
  	NITROGEN 		(AtomType.NITROGEN, 0.940),
	  
  	/** The OXYGEN. */
  	OXYGEN 			(AtomType.OXYGEN, 0.5804),
	  
  	/** The PHOSPHORUS. */
  	PHOSPHORUS 	(AtomType.PHOSPHORUS, 0.517),
	  
  	/** The SUD o_ water. */
  	SUDO_WATER 		(AtomType.SUDO_WATER, -.158),
  	
  	/** The SULFUR. */
  	SULFUR 			(AtomType.SULFUR, 0.2847);
	  
	 
		/** The form factor constant. */
		public double formFactorConstant;
		
		/** The type. */
		public final AtomType type;
		
		private SansAtomConstants(AtomType type, double formFactorConstant)
	  {
			assert this.type.equals(type) : "Atom type constants do not mach. Generic type = "+type+" SANS type = "+this.ordinal()+".";

	    this.type = type;
	    this.formFactorConstant = formFactorConstant;
	  }
	}
	
	/** The solvent decay rate. */
	private final double solventDecayRate;

	/** The solvent scaling. */
	private final double solventScaling;
	
	/** The solvent scattering. */
	private final double solventScattering; 
	
	/** The Constant DEFAULT_DECAY_RATE. */
	public final static double DEFAULT_DECAY_RATE = 1.5; 

	/** The Constant DEFAULT_SCATTERING_OF_D20. */
	public final static double DEFAULT_SCATTERING_OF_D20 = 1.915;
	
	/** The Constant DEFAULT_SCATTERING_OF_H20. */
	public final static double DEFAULT_SCATTERING_OF_H20 = -.158;

	/** The Constant DEFAULT_SOLVENT_SCALE. */
	public final static double DEFAULT_SOLVENT_SCALE = 0.3;	
	
	public final static HashMap<AtomType,SansAtomConstants> SANS_CONSTANTS = makeSansTable();	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7284398177965055139L;
	
	private static HashMap<AtomType, SansAtomConstants> makeSansTable()
	{
		HashMap<AtomType, SansAtomConstants> map = new HashMap<AtomType, SansAtomConstants>();
		for (SansAtomConstants val : SansAtomConstants.values())
			map.put(val.type, val);
			
		return map;
	}
	
	/**
	 * Instantiates a new sANS form factor table.
	 * 
	 * @param qValues
	 *          the q values
	 * @param fractionDeuterated
	 *          the fraction deuterated
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
	public SansFormFactorTable(double[] qValues, double fractionDeuterated) throws SasRuntimeException
	{
		this(qValues, (1.0-fractionDeuterated)*DEFAULT_SCATTERING_OF_H20+fractionDeuterated*DEFAULT_SCATTERING_OF_D20, DEFAULT_SOLVENT_SCALE, DEFAULT_DECAY_RATE);
	}
	
	/**
	 * Instantiates a new sANS form factor table.
	 * 
	 * @param qValues
	 *          the q values
	 * @param solventScattering
	 *          the solvent scattering
	 * @param solventScaling
	 *          the solvent scaling
	 * @param solventDecayRate
	 *          the solvent decay rate
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
	public SansFormFactorTable(double[] qValues, double solventScattering, double solventScaling, double solventDecayRate) throws SasRuntimeException
	{
		super(qValues);
		
		this.solventScattering = solventScattering;
		this.solventScaling = solventScaling;
		this.solventDecayRate = solventDecayRate;
	}
	
	/**
	 * Creates the with new solvent decay.
	 * 
	 * @param solventDecayRate
	 *          the solvent decay rate
	 * @return the sANS form factor table
	 */
	public SansFormFactorTable createWithNewSolventDecay(double solventDecayRate)
	{
		return new SansFormFactorTable(this.qValues, this.solventScattering, this.solventScaling, solventDecayRate);
	}
	
	/**
	 * Creates the with new solvent scaling.
	 * 
	 * @param solventScaling
	 *          the solvent scaling
	 * @return the sANS form factor table
	 */
	public SansFormFactorTable createWithNewSolventScaling(double solventScaling)
	{
		return new SansFormFactorTable(this.qValues, this.solventScattering, solventScaling, this.solventDecayRate);
	}

	/**
	 * Creates the with new solvent scattering.
	 * 
	 * @param scatteringOfSolvent
	 *          the scattering of solvent
	 * @return the sANS form factor table
	 */
	public SansFormFactorTable createWithNewSolventScattering(double scatteringOfSolvent)
	{
		return new SansFormFactorTable(this.qValues, scatteringOfSolvent, this.solventScaling, this.solventDecayRate);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorDisplaced(int)
	 */
	@Override
	public double getFormFactorDisplaced(int qIndex)
	{
		double excludedVolume = 4.0/3.0*BasicMath.PI*BasicMath.cube(SolvatedMolecule.WATER_LAYER_DISTANCE);
		
		return getFormFactorSolvent(excludedVolume);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorMolecule(edu.umd.umiacs.armor.molecule.AtomType, int)
	 */
	@Override
	public double getFormFactorMolecule(AtomType type, int qIndex)
	{
		return SANS_CONSTANTS.get(type).formFactorConstant-getFormFactorSolvent(type.getExcludedWaterVolume());
	}

	/**
	 * Gets the form factor solvent.
	 * 
	 * @param excludedVolume
	 *          the excluded volume
	 * @return the form factor solvent
	 */
	public double getFormFactorSolvent(double excludedVolume)
	{
		return this.solventScattering*excludedVolume;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorSolventLayer(double, int)
	 */
	@Override
	public double getFormFactorSolventLayer(double distanceToCore, int qIndex)
	{
		double scale = this.solventScaling*Math.exp(-this.solventDecayRate*distanceToCore);
		double excludedVolume = 4.0/3.0*BasicMath.PI*BasicMath.cube(SolvatedMolecule.WATER_LAYER_DISTANCE);
		
		return scale*getFormFactorSolvent(excludedVolume);
	}

	/**
	 * Gets the solvent scattering.
	 * 
	 * @return the solvent scattering
	 */
	public double getSolventScattering()
	{
		return this.solventScattering;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#numTypes()
	 */
	@Override
	public int numTypes()
	{
		return SansAtomConstants.values().length;
	}
}
