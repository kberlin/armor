/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.RealMatrix;

public class DifferentiableScatteringPrediction extends ScatteringPrediction
{
	private final RealMatrix jacobian;
	private double jacobianError;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2330648695144916079L;
	
	public DifferentiableScatteringPrediction(ScatteringPrediction prediction, RealMatrix jacobian, double jacobianError)
	{
		super(prediction);
		
		this.jacobian = jacobian;
		this.jacobianError = jacobianError;
	}
	
	public DifferentiableScatteringPrediction(ScatteringProfile profile, double computeTime, double relativeErrorEstimate, RealMatrix jacobian, double jacobianError)
	{
		super(profile, computeTime, relativeErrorEstimate);
		
		this.jacobian = jacobian;
		this.jacobianError = jacobianError;
	}
	
	public RealMatrix getJacobian()
	{
		return this.jacobian;
	}
	
	public double getJacobianError()
	{
		return this.jacobianError;
	}
	
	public double jacobianMaxError(RealMatrix expectedJacobian)
	{
		RealMatrix diff = this.jacobian.subtract(expectedJacobian);
		
		double max = 0.0;
		
		for (int row=0; row<diff.numRows(); row++)
		{
			double value = BasicMath.norm(diff.getRow(row))/BasicMath.norm(expectedJacobian.getRow(row));
			
			max = Math.max(max, value);
		}
		
		return max;
	}

	public void setJacobianError(double error)
	{
		this.jacobianError = error;
	}
}
