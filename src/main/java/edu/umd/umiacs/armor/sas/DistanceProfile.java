/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.util.ArrayList;

import org.apache.commons.math3.distribution.NormalDistribution;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.DatasetDouble;
import edu.umd.umiacs.armor.util.SortType;

/**
 * The Class DistanceProfile.
 */
public final class DistanceProfile extends DatasetDouble
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4803783415895180750L;
	
	public static double[] scalingConstants(double[] r)
	{
		double[] c = new double[r.length];
    for (int col=0; col<r.length; col++)
    {
    	double val;
    	
			//add the scaling factor
    	if (r.length==1)
    		val = 1.0;
    	else
			if (col==0)
				val = 0.5*(r[col+1]-r[col]);
			else
			if (col-1>=0 && col+1<r.length)
				val = 0.5*(r[col+1]-r[col-1]);
			else
				val = 0.5*(r[col]-r[col-1]);			

			c[col] = val;
    }
    
    return c;
	}
	
	public DistanceProfile(DatasetDouble data)
	{
		super(data.sort(SortType.ASCENDING));
	}

	public DistanceProfile(double[] r, double[] pr)
	{
		super(r, pr);
	}
  
  public DistanceProfile gaussianBlur()
  {
  	double[] r = getRArray();
  	
		ArrayList<Double> nonZeroR = new ArrayList<Double>();
		for (int iter=0; iter<r.length; iter++)
			if (Math.abs(getPr(iter))>1.0e-12)
				nonZeroR.add(r[iter]);
		
  	double[] diff = new double[nonZeroR.size()-1];
  	
  	for (int iter=1; iter<nonZeroR.size(); iter++)
  		diff[iter-1] = nonZeroR.get(iter)-nonZeroR.get(iter-1);
  	
  	double meanDiff = BasicStat.mean(diff);
  	double std = 0.5*meanDiff;
  	double range = 4.0*std;
  	
  	double[] extension = BasicUtils.uniformSpacedPoints(r[r.length-1]+meanDiff, r[r.length-1]+meanDiff+range, meanDiff); 
  	double[] rExtended = new double[r.length+extension.length];
  	double[] PrExtended = new double[rExtended.length];
  	
  	//create extension
  	for (int iter=0; iter<r.length; iter++)
  	{
  		rExtended[iter] = getR(iter);
  		PrExtended[iter] = getPr(iter);
  		
  		if (Math.abs(rExtended[iter])<1.0e-2)
  			PrExtended[iter] = 0.0;
  	}
  	for (int iter=0; iter<rExtended.length-r.length; iter++)
  	{
  		rExtended[r.length+iter] = extension[iter];
  		PrExtended[r.length+iter] = 0.0;
  	}
  	
  	NormalDistribution distribution = new NormalDistribution(0.0, std);
  	
  	double[] newPrValues = new double[r.length];
  	for (int rIter=0; rIter<r.length; rIter++)
  	{
  		double currR = r[rIter];
  		ArrayList<Double> kernelValues = new ArrayList<Double>();
  		
  		newPrValues[rIter] = 0.0;
  		for (int iter=0; iter<rExtended.length; iter++)
  		{
    		//do negative
  			double currDiff = currR-(-rExtended[iter]);  	
  			
  			if (Math.abs(rExtended[iter])>=1.0e-2 && Math.abs(currDiff)<range)
  			{
  				double val = distribution.density(currDiff);
  				kernelValues.add(val);
  				
  				newPrValues[rIter]+= 0.5*PrExtended[iter]*val;
  			}
  			
    		//do positive
  			currDiff = currR-rExtended[iter];  			
  			if (Math.abs(currDiff)<range)
  			{
  				double val = distribution.density(currDiff);
  				kernelValues.add(val);
  				
  				if (Math.abs(rExtended[iter])<1.0e-2)
  					newPrValues[rIter]+= 1.0*PrExtended[iter]*val;
  				else
  					newPrValues[rIter]+= 0.5*PrExtended[iter]*val;
  			}

  		}
  		
			//sum up all values and normalize
  		double sum = 0.0;
  		for (double val : kernelValues)
  			sum += val;
  		      	
  		newPrValues[rIter] = newPrValues[rIter]/sum;

  		
  	}
  	
  	return new DistanceProfile(r, newPrValues);
  }
	
	public double getPr(int index)
  {
  	return getY(index);
  }

  public double[] getPrArray()
	{
		return getYArray();
	}

  public double getR(int index)
  {
  	return getX(index);
  }

  public double[] getRArray()
	{
		return getXArray();
	}

	public ScatteringProfile prModelProfile(double[] qValues)
  {
  	return prModelProfile(qValues, Double.POSITIVE_INFINITY);
  }
  
  public ScatteringProfile prModelProfile(double[] qValues, double Dmax)
  {
  	double[] r = getRArray();
  	double[] pr = getPrArray();
  	
  	double[][] A = new double[qValues.length][r.length];
  	double[] c = scalingConstants(r);
		for (int row=0; row<A.length; row++)
		{
			for (int col=0; col<A[row].length; col++)
			{
				A[row][col] = c[col]*BasicMath.sinc(qValues[row]*r[col]);
			}
		}

		double[] prediction = BasicMath.mult(A, pr);
		
		return new ScatteringProfile(qValues, prediction);
  }
}
