/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.io.IOException;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.refinement.AbstractSparseEnsembleSelection;
import edu.umd.umiacs.armor.refinement.RefinementRuntimeException;
import edu.umd.umiacs.armor.util.ParseOptions;

public class SasSparseEnsembleSelection extends AbstractSparseEnsembleSelection<SasEnsembleInput>
{
	public static void main(String[] args) throws IOException, PdbException, AtomNotFoundException, ClassNotFoundException, RuntimeException
	{
  	ParseOptions options = new ParseOptions();
  	options.addRequiredOption("sas", "sas", "Scattering profile file name.", String.class);
		options.addRequiredOption("matrix", "matrix", "File name of the prediction matrix file.", String.class);
  	options.addOption("maxQ", "maxQ", "Maximum q value to use in the computation.", Double.POSITIVE_INFINITY);

		//create the solver
  	SasSparseEnsembleSelection selection = new SasSparseEnsembleSelection(options, args);
		
		selection.run("sas");
	}

	public SasSparseEnsembleSelection(ParseOptions options, String[] args)
	{
		super(options, args);
		setDefaultSolver();
	}
	
	@Override
	public SasEnsembleInput createInput()
	{
	 	try
	 	{
			ScatteringProfile profile = ScatteringProfile.readProfile(getOptions().get("sas").getString());
	   	
			//create input
		 	SasEnsembleInput ensembleInput = new SasEnsembleInput(profile, null);
		 	
			//cut the maximum q value
			ensembleInput = ensembleInput.cut(getOptions().get("maxQ").getDouble());

			return ensembleInput;
	 	}
	 	catch (IOException e)
	 	{
	 		throw new RefinementRuntimeException(e);
	 	}
		
	}
}
