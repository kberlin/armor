/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.ExperimentalDatum;

public class ScatteringDatum extends BaseExperimentalDatum implements Comparable<ScatteringDatum>
{
	private final double q;
	/**
	 * 
	 */
	private static final long serialVersionUID = 4972078982996346259L;
	
	public ScatteringDatum(double q, double value)
	{
		super(value);
		this.q = checkQ(q);
	}
	
	public ScatteringDatum(double q, double value, double error)
	{
		super(value, error);
		this.q = checkQ(q);
	}

	public ScatteringDatum(double q, ExperimentalDatum datum)
	{
		super(datum);
		this.q = checkQ(q);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#add(double)
	 */
	@Override
	public ScatteringDatum add(double value)
	{
		return new ScatteringDatum(this.q, super.add(value));
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#add(edu.umd.umiacs.armor.util.ExperimentalDatum)
	 */
	@Override
	public ScatteringDatum add(ExperimentalDatum value)
	{
		return new ScatteringDatum(this.q, super.add(value));
	}
	
	private double checkQ(double q)
	{
		if (q<0.0)
			throw new SasRuntimeException("Q value must be greater than 0.");
		
		return q;
	}

	@Override
	public int compareTo(ScatteringDatum d)
	{
		return Double.compare(this.q, d.q); 
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#divide(double)
	 */
	@Override
	public ScatteringDatum divide(double value)
	{
		return new ScatteringDatum(this.q, super.divide(value));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#divide(edu.umd.umiacs.armor.util.ExperimentalDatum)
	 */
	@Override
	public ScatteringDatum divide(ExperimentalDatum value)
	{
		return new ScatteringDatum(this.q, super.divide(value));
	}

	public double getIq()
	{
		return value();
	}

	public double getQ()
	{
		return this.q;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#mult(double)
	 */
	@Override
	public ScatteringDatum mult(double value)
	{
		return new ScatteringDatum(this.q, super.mult(value));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#subtract(double)
	 */
	@Override
	public ScatteringDatum subtract(double value)
	{
		return new ScatteringDatum(this.q, super.subtract(value));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#subtract(edu.umd.umiacs.armor.util.ExperimentalDatum)
	 */
	@Override
	public ScatteringDatum subtract(ExperimentalDatum value)
	{
		return new ScatteringDatum(this.q, super.subtract(value));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#valueInfoString()
	 */
	@Override
	public String valueInfoString()
	{
		return String.format("%7.4f", this.q);
	}

}
