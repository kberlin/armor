/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.util.HashMap;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.AtomType;
import edu.umd.umiacs.armor.molecule.SolvatedMolecule;

public class SaxsFormFactorTable extends AbstractFormFactorTable
{
	
	protected enum SaxsAtomConstants
	{
		//    International Tables for Crystallography
		//      Volume C
		//      Mathematical, Physical and Chemical Tables
		//      Edited by A.J.C. Wilson
		//      Kluwer Academic Publishers
		//      Dordrecht/Boston/London
		//      1992
		//
		//    Table 6.1.1.4 (pp. 500-502)
		//      Coefficients for analytical approximation to the scattering factors
		//      of Tables 6.1.1.1 and 6.1.1.3
		//
		//    [ Table 6.1.1.4 is a reprint of Table 2.2B, pp. 99-101,
		//      International Tables for X-ray Crystallography, Volume IV,
		//      The Kynoch Press: Birmingham, England, 1974.
		//      There is just one difference, see "Tl3+".
		//    ] 
		//             f0[k] = c + [SUM a_i*EXP(-b_i*(k^2)) ]				     
		//                i=1,5
		//
		//         a1         a2         a3         a4         a5     c           b1        b2          b3        b4          b5    excl_vol
		//H        0.493002   0.322912   0.140191   0.040810   0.0     0.003038   10.5109   26.1257     3.14236   57.7997     1.0      5.15
		//HE       0.489918   0.262003   0.196767   0.049879   0.0     0.001305   20.6593    7.74039   49.5519     2.20159    1.0      5.15
		//C        2.31000    1.02000    1.58860    0.865000   0.0     0.215600   20.8439   10.2075     0.568700  51.6512     1.0      16.44
		//N       12.2126     3.13220    2.01250    1.16630    0.0   -11.529       0.005700  9.89330   28.9975     0.582600   1.0      2.49
		//O        3.04850    2.28680    1.54630    0.867000   0.0     0.250800   13.2771    5.70110    0.323900  32.9089     1.0      9.13
		//NE       3.95530    3.11250    1.45460    1.12510    0.0     0.351500    8.40420   3.42620    0.230600  21.7184     1.0      9.0
		//NA       4.76260    3.17360    1.26740    1.11280    0.0     0.676000    3.28500   8.84220    0.313600 129.424      1.0      9.0
		//MG       5.42040    2.17350    1.22690    2.30730    0.0     0.858400    2.82750  79.2611     0.380800   7.19370    1.0      9.0
		//P        6.43450    4.17910    1.78000    1.49080    0.0     1.11490     1.90670  27.1570     0.526000  68.1645     1.0      5.73
		//S        6.90530    5.20340    1.43790    1.58630    0.0     0.866900    1.46790  22.2151     0.253600  56.1720     1.0      19.86
		//CA      15.6348     7.95180    8.43720    0.853700   0.0   -14.875      -0.00740   0.608900  10.3116    25.9905     1.0      9.0
		//FE      11.0424     7.37400    4.13460    0.439900   0.0     1.00970     4.65380   0.305300  12.0546    31.2809     1.0      9.0
		//ZN      11.9719     7.38620    6.46680    1.39400    0.0     0.780700    2.99460   0.203100   7.08260   18.0995     1.0      9.0
		//SE      17.0006     5.81960    3.97310    4.35430    0.0     2.84090     2.40980   0.272600   15.2372   43.8163     1.0      9.0
		//AU      16.8819    18.5913    25.5582     5.86000    0.0     12.0658     0.461100  8.62160     1.48260  36.3956     1.0      19.86
	
		
		/** The CARBON. */
		CARBON 			(AtomType.CARBON, new double[]{2.31000,1.02000,1.58860,0.865000,0.0,0.215600,20.8439,10.2075,0.568700,51.6512,1.0}),
	  
  	/** The DEUTERIUM. */
  	DEUTERIUM 	(AtomType.DEUTERIUM, new double[]{0.493002,0.322912,0.140191,0.040810,0.0,0.003038,10.5109,26.1257,3.14236,57.7997,1.0}),
	  
  	/** The HYDROGEN. */
  	HYDROGEN 		(AtomType.HYDROGEN, new double[]{0.493002,0.322912,0.140191,0.040810,0.0,0.003038,10.5109,26.1257,3.14236,57.7997,1.0}),
	  
  	/** The NITROGEN. */
  	NITROGEN 		(AtomType.NITROGEN, new double[]{12.2126,3.13220,2.01250,1.16630,0.0,-11.529,0.005700,9.89330,28.9975,0.582600,1.0}),
	  
  	/** The OXYGEN. */
  	OXYGEN 			(AtomType.OXYGEN, new double[]{3.04850,2.28680,1.54630,0.867000,0.0,0.250800,13.2771,5.70110,0.323900,32.9089,1.0}),
	  
  	/** The PHOSPHORUS. */
  	PHOSPHORUS 	(AtomType.PHOSPHORUS, new double[]{6.43450,4.17910,1.78000,1.49080,0.0,1.11490,1.90670,27.1570,0.526000,68.1645,1.0}),
	  
  	/** The SUDO_Water. */
  	SUDO_WATER 		(AtomType.SUDO_WATER, new double[]{1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0}),
	  
  	/** The SULFUR. */
  	SULFUR 			(AtomType.SULFUR, new double[]{6.90530,5.20340,1.43790,1.58630,0.0,0.866900,1.46790,22.2151,0.253600,56.1720,1.0});	 
		/** The form factor constants. */
		public final double[] formFactorConstants;
	
		/** The type. */
		public final AtomType type;
		
		/**
		 * Instantiates a new sAXS atom constants.
		 * 
		 * @param type
		 *          the type
		 * @param formFactorConstants
		 *          the form factor constants
		 */
		private SaxsAtomConstants(AtomType type, double[] formFactorConstants)
	  {
	    this.type = type;
	    this.formFactorConstants = formFactorConstants;
	  }
	}
	
	/** The electron density of solvent. */
	private final double electronDensityOfSolvent;

	/** The implicit solvent. */
	private final boolean implicitSolvent;
	
	/** The solvent contrast scale. */
	private final double solventContrastScale;
	
	/** The solvent decay rate. */
	private final double solventDecayRate;

	/** The volume correction. */
	private final double volumeCorrection;
	
	/** The Constant DEFAULT_ELECTRON_DENSITY_OF_SOLVENT. */
	public final static double DEFAULT_ELECTRON_DENSITY_OF_SOLVENT = 0.334; // e/A^3 (H20), 0.334
	
	/** The Constant DEFAULT_SOLVENT_CONTRAST_SCALE. */
	public final static double DEFAULT_SOLVENT_CONTRAST_SCALE = 0.1;
	
	/** The Constant DEFAULT_SOLVENT_DECAY_RATE. */
	public final static double DEFAULT_SOLVENT_DECAY_RATE = 1.6;

	/** The Constant DEFAULT_VOLUME_CORRECTION. */
	public final static double DEFAULT_VOLUME_CORRECTION = 1.0;
	
	public final static HashMap<AtomType, SaxsAtomConstants> SAXS_CONSTANTS = makeSaxsTable();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7129428212532721482L;
		
	/**
	 * Gets the form factor.
	 * 
	 * @param type
	 *          the type
	 * @param qValue
	 *          the q value
	 * @return the form factor
	 */
	public static double getFormFactor(AtomType type, double qValue)
	{
		SaxsAtomConstants saxsConstants = SAXS_CONSTANTS.get(type);
		double[] formFactorConstants = saxsConstants.formFactorConstants;

		//initial offset
		double fa = formFactorConstants[5];
		
		double sValue = qValue/(4.0*BasicMath.PI);
		for (int iter=0; iter<5; iter++)
		{
			fa += formFactorConstants[iter]*Math.exp(-formFactorConstants[iter+6]*sValue*sValue);
		}
		
		return fa;
	}
	
	/**
	 * Gets the volume form factor.
	 * 
	 * @param excludedVolume
	 *          the excluded volume
	 * @param qValue
	 *          the q value
	 * @param electronDensity
	 *          the electron density
	 * @return the volume form factor
	 */
	public static double getVolumeFormFactor(double excludedVolume, double qValue, double electronDensity)
	{
		double fa = electronDensity*excludedVolume*Math.exp(-1.0/(4.0*BasicMath.PI)*qValue*qValue*Math.pow(excludedVolume, 2.0/3.0));
	 	
		return fa;
	}
	
	private static HashMap<AtomType, SaxsAtomConstants> makeSaxsTable()
	{
		HashMap<AtomType, SaxsAtomConstants> map = new HashMap<AtomType, SaxsAtomConstants>();
		for (SaxsAtomConstants val : SaxsAtomConstants.values())
			map.put(val.type, val);
			
		return map;
	}
	
	/**
	 * Instantiates a new sAXS form factor table.
	 * 
	 * @param qValues
	 *          the q values
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
	public SaxsFormFactorTable(double[] qValues) throws SasRuntimeException
	{
		this(qValues, true);
	}
	
	/**
	 * Instantiates a new sAXS form factor table.
	 * 
	 * @param qValues
	 *          the q values
	 * @param implicitSolvent
	 *          the implicit solvent
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
	public SaxsFormFactorTable(double[] qValues, boolean implicitSolvent) throws SasRuntimeException
	{
		this(qValues, implicitSolvent, DEFAULT_ELECTRON_DENSITY_OF_SOLVENT, DEFAULT_SOLVENT_CONTRAST_SCALE, 
				DEFAULT_SOLVENT_DECAY_RATE, DEFAULT_VOLUME_CORRECTION);
	}
	
	public SaxsFormFactorTable(double[] qValues, boolean implicitSolvent, double electronDensity) throws SasRuntimeException
	{
		this(qValues, implicitSolvent, electronDensity, DEFAULT_SOLVENT_CONTRAST_SCALE, 
				DEFAULT_SOLVENT_DECAY_RATE, DEFAULT_VOLUME_CORRECTION);
	}

	/**
	 * Instantiates a new sAXS form factor table.
	 * 
	 * @param qValues
	 *          the q values
	 * @param implicitSolvent
	 *          the implicit solvent
	 * @param electronDensityOfSolvent
	 *          the electron density of solvent
	 * @param solventContrastScale
	 *          the solvent contrast scale
	 * @param solventDecayRate
	 *          the solvent decay rate
	 * @param volumeCorrection
	 *          the volume correction
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
	public SaxsFormFactorTable(double[] qValues, boolean implicitSolvent, double electronDensityOfSolvent, 
			double solventContrastScale, double solventDecayRate, double volumeCorrection) throws SasRuntimeException
	{
		super(qValues);
		
		this.implicitSolvent = implicitSolvent;
		this.electronDensityOfSolvent = electronDensityOfSolvent;
		this.solventContrastScale = solventContrastScale;
		this.solventDecayRate = solventDecayRate;
		this.volumeCorrection = volumeCorrection;
	}

	/**
	 * Creates the with new electron density.
	 * 
	 * @param electronDensityOfSolvent
	 *          the electron density of solvent
	 * @return the sAXS form factor table
	 */
	public SaxsFormFactorTable createWithNewElectronDensity(double electronDensityOfSolvent)
	{
		return new SaxsFormFactorTable(this.qValues, this.implicitSolvent, electronDensityOfSolvent, this.solventContrastScale, this.solventDecayRate, this.volumeCorrection);
	}
	
	/**
	 * Creates the with new q values.
	 * 
	 * @param qValues
	 *          the q values
	 * @return the sAXS form factor table
	 */
	public SaxsFormFactorTable createWithNewQValues(double[] qValues)
	{
		return new SaxsFormFactorTable(qValues, this.implicitSolvent, this.electronDensityOfSolvent, this.solventContrastScale, this.solventDecayRate, this.volumeCorrection);
	}
	
	/**
	 * Creates the with new solvent contrast scale.
	 * 
	 * @param solventContrastScale
	 *          the solvent contrast scale
	 * @return the sAXS form factor table
	 */
	public SaxsFormFactorTable createWithNewSolventContrastScale(double solventContrastScale)
	{
		return new SaxsFormFactorTable(this.qValues, this.implicitSolvent, this.electronDensityOfSolvent, solventContrastScale, this.solventDecayRate, this.volumeCorrection);
	}

	/**
	 * Creates the with new solvent decay rate.
	 * 
	 * @param solventDecayRate
	 *          the solvent decay rate
	 * @return the sAXS form factor table
	 */
	public SaxsFormFactorTable createWithNewSolventDecayRate(double solventDecayRate)
	{
		return new SaxsFormFactorTable(this.qValues, this.implicitSolvent, this.electronDensityOfSolvent, this.solventContrastScale, solventDecayRate, this.volumeCorrection);
	}
	
	/**
	 * Creates the with new volume correction.
	 * 
	 * @param volumeCorrection
	 *          the volume correction
	 * @return the sAXS form factor table
	 */
	public SaxsFormFactorTable createWithNewVolumeCorrection(double volumeCorrection)
	{
		return new SaxsFormFactorTable(this.qValues, this.implicitSolvent, this.electronDensityOfSolvent, this.solventContrastScale, this.solventDecayRate, volumeCorrection);
	}
	
	/**
	 * Gets the electron density of solvent.
	 * 
	 * @return the electron density of solvent
	 */
	public double getElectronDensityOfSolvent()
	{
		return this.electronDensityOfSolvent;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorDisplaced(int)
	 */
	@Override
	public double getFormFactorDisplaced(int qIndex)
	{
		//double excludedVolume = 4.0/3.0*BasicMath.PI*BasicMath.cube(SolvatedMolecule.SUDO_WATER_RADIUS);
		double excludedVolume = Math.pow(BasicMath.PI, 3.0/2.0)*BasicMath.cube(SolvatedMolecule.WATER_LAYER_DISTANCE);
		return getVolumeFormFactor(excludedVolume, getQ(qIndex), this.electronDensityOfSolvent);
	}
	
	/**
	 * Gets the form factor minus solvent.
	 * 
	 * @param type
	 *          the type
	 * @param qValue
	 *          the q value
	 * @return the form factor minus solvent
	 */
	public double getFormFactorMinusSolvent(AtomType type, double qValue)
	{
		double atomFormFactor = getFormFactor(type, qValue);
	 	double solventFormFactor = getVolumeFormFactor(type.getExcludedWaterVolume()*this.volumeCorrection, qValue, this.electronDensityOfSolvent);
	 	
		return atomFormFactor-solventFormFactor;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorMolecule(edu.umd.umiacs.armor.molecule.AtomType, int)
	 */
	@Override
	public double getFormFactorMolecule(AtomType type, int qIndex)
	{
		if (this.implicitSolvent)
			return getFormFactorMinusSolvent(type, getQ(qIndex));
		
		return getFormFactor(type, getQ(qIndex));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorSolventLayer(double, int)
	 */
	@Override
	public double getFormFactorSolventLayer(double distanceToCore, int qIndex)
	{
		//double scale = 1.0/BasicMath.cube(1.0+solventDecayRate*distanceToCore);
		
		double scale = this.solventContrastScale*Math.exp(-this.solventDecayRate*distanceToCore);
		//double excludedVolume = 4.0/3.0*BasicMath.PI*BasicMath.cube(SolvatedMolecule.SUDO_WATER_RADIUS);
		//double excludedVolume = volumeCorrection*Math.pow(BasicMath.PI, 3.0/2.0)*BasicMath.cube(SolvatedMolecule.SUDO_WATER_RADIUS);
		
		//return scale*getVolumeFormFactor(excludedVolume, getQ(qIndex), electronDensityOfSolvent);
		double q = getQ(qIndex);
		double r = SolvatedMolecule.WATER_LAYER_DISTANCE*this.volumeCorrection;
		
		if (q==0)
			q = 1.0e-8;
		
		double formFactor = DEFAULT_ELECTRON_DENSITY_OF_SOLVENT*BasicMath.square(3.0*(BasicMath.sin(q*r)-q*r*BasicMath.cos(q*r))/BasicMath.cube(q*r));
		
		return scale*formFactor;
	}

	/**
	 * Gets the solvent contrast scale.
	 * 
	 * @return the solvent contrast scale
	 */
	public double getSolventContrastScale()
	{
		return this.solventContrastScale;
	}

	/**
	 * Gets the solvent decay rate.
	 * 
	 * @return the solvent decay rate
	 */
	public double getSolventDecayRate()
	{
		return this.solventDecayRate;
	}
	
	/**
	 * Gets the volume correction.
	 * 
	 * @return the volume correction
	 */
	public double getVolumeCorrection()
	{
		return this.volumeCorrection;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#numTypes()
	 */
	@Override
	public int numTypes()
	{
		return SaxsAtomConstants.values().length;
	}
}
