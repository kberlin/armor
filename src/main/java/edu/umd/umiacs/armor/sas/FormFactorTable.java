package edu.umd.umiacs.armor.sas;

import edu.umd.umiacs.armor.molecule.Molecule;

public interface FormFactorTable
{

	public double getFormFactor(Molecule molecule, int atomIndex, int qIndex);

	public double[][] getFormFactorArray(Molecule molecule);

	public double[][] getFormFactorArrayFrom(Molecule molecule);

	public double[][] getFormFactorArrayTo(Molecule molecule);

	public double getFormFactorFrom(Molecule molecule, int atomIndex, int qIndex);

	public double getFormFactorTo(Molecule molecule, int atomIndex, int qIndex);

	public double getMaxQ();

	public double getMinQ();

	public double getQ(int index);

	public double[] getQArray();

	public int sizeQ();

}