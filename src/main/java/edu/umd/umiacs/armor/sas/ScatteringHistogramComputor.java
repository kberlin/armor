/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.util.Arrays;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BasicUtils;

/**
 * The Class ScatteringHistogramComputor.
 */
public final class ScatteringHistogramComputor
{
	private final int grain;
	
	/** The table. */
	private final FormFactorTable table;
	
	public static final int DEFAULT_GRAIN = 1;
	
	public static final double DEFAULT_RESOLUTION = 1.0; //ANGSTROM
	
	private static double[] scaleValues(double[] r, double[] histogram)
	{
		double[] c = DistanceProfile.scalingConstants(r);
    for (int col=0; col<r.length; col++)
    {
			histogram[col] = histogram[col]/c[col];
    }

    return histogram;
	}

	public ScatteringHistogramComputor(FormFactorTable table)
	{
		this.table = table;
		this.grain = DEFAULT_GRAIN;
	}
	
	public ScatteringHistogramComputor(FormFactorTable table, int grain)
	{
		this.table = table;
		this.grain = grain;
	}	
	
	public DistanceProfile computeHistogram(Molecule molecule)
	{
		//figure out the number of points
		int numPoints = (int)Math.ceil(molecule.maxAtomDistance());
		
    return computeHistogram(molecule, BasicUtils.uniformLinePoints(0.0, molecule.maxAtomDistance(), numPoints));
	}
	
	public DistanceProfile computeHistogram(Molecule molecule, double[] rArray) throws SasRuntimeException
	{
    int qIndex = 0;
    
    //make a defensive copy
    rArray = rArray.clone();
    Arrays.sort(rArray);
    
    final double[] atomArray = molecule.getAtomPositionSingleArray();
    final double[] fa = new double[molecule.size()];
    
    for (int iter=0; iter<fa.length; iter++)
    	fa[iter] = this.table.getFormFactor(molecule, iter, qIndex);

    double numAtoms = molecule.size();
    double faSource;
    double faSink;
    
    //create the histogram
    double[] histogram = BasicUtils.createArray(rArray.length, 0.0);
    
    //loop through all the upper triangle atoms
    for (int iterSource=0; iterSource<numAtoms; iterSource+=this.grain)
    {
      //get the form factor array for the first atom
      faSource = fa[iterSource];
      double x = atomArray[3*iterSource+0];
      double y = atomArray[3*iterSource+1];
      double z = atomArray[3*iterSource+2];
      
      for (int iterSink=iterSource+1; iterSink<numAtoms; iterSink+=this.grain)
	    {
	    	//compute distance between atoms
      	double dx = x-atomArray[3*iterSink+0];
      	double dy = y-atomArray[3*iterSink+1];
      	double dz = z-atomArray[3*iterSink+2];
      	double r = BasicMath.sqrt(dx*dx+dy*dy+dz*dz);

	      //get the form factor array for the second atom
	      faSink = fa[iterSink];
	      
	      int histogramIndex = findIndex(r, rArray);
	      
	      histogram[histogramIndex]+= 2.0*faSource*faSink;
	    }

      histogram[0]+= faSource*faSource;
    }
    
    return new DistanceProfile(rArray, scaleValues(rArray, histogram));
	}
	
	private int findIndex(double r, double[] rArray)
	{
		int nearest = -1;
		double bestDistance = Double.MAX_VALUE;
		
		for (int i = 0; i < rArray.length; i++) {
		  if (rArray[i] == r) 
		  {
		    return i;		    
		  }
		  else
		  {
		    double d = Math.abs(r - rArray[i]);
		    if (d < bestDistance) 
		    {
		      nearest = i;
		      bestDistance = d;
		    }
		  }
		}
		
		return nearest;
	}
}
