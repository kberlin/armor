/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BasicUtils;

/**
 * The Class BruteScatteringPredictor.
 */
public class BruteScatteringPredictor extends AbstractScatteringPredictor implements DifferentiableScatteringPredictor
{
	
	private class BruteJacobianThread implements Runnable
	{
	  private final double[] atomArray;
		private final double[][] fa;
		private final double[][] J;
		private final int numThreads;
		private final double[] qValues;
		private final int threadNumber;

		private BruteJacobianThread(double[][] J, double[] atomArray, double[][] fa, double[] qValues, int threadNumber, int numThreads)
	  {
	  	this.J = J;
	  	this.atomArray = atomArray;
	  	this.fa = fa;
	  	this.qValues = qValues;
	  	this.threadNumber = threadNumber;
	  	this.numThreads = numThreads;
	  }

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public final void run()
		{
	    //allocate memory for reusable storage
	    double[] xpos = new double[3];
	    double[] dcenter = new double[3];
	   	
	    //storage for differences
	    double[] IqDerivative = BasicUtils.createArray(this.qValues.length*this.fa.length, 0.0);

	    //for each atom compute the individual 3 (x,y,z) columns of the Jacobian
	    for (int iterSource=this.threadNumber; iterSource<this.fa.length; iterSource+=this.numThreads)
	    {
	      double[] faSource = this.fa[iterSource];

	      //go through the x,y,z
	      for (int x=0; x<3; x++)
	    	{
	      	//store the original values
	      	for (int iter=0; iter<IqDerivative.length; iter++)
	          IqDerivative[iter] = 0.0;

		      xpos[0] = this.atomArray[3*iterSource+0];
		      xpos[1] = this.atomArray[3*iterSource+1];
		      xpos[2] = this.atomArray[3*iterSource+2];

		      //add the difference to the total sum
		      for (int iterSink=0; iterSink<this.fa.length; iterSink++)
			    {
		      	if (iterSink==iterSource)
		      		continue;
		      	
			    	//compute distance between atoms
		      	dcenter[0] = xpos[0]-this.atomArray[3*iterSink+0];
		      	dcenter[1] = xpos[1]-this.atomArray[3*iterSink+1];
		      	dcenter[2] = xpos[2]-this.atomArray[3*iterSink+2];
		      	double r2 = dcenter[0]*dcenter[0]+dcenter[1]*dcenter[1]+dcenter[2]*dcenter[2];
		      	double r = BasicMath.sqrt(r2);
		      	
			      //get the form factor array for the second atom
			      double[] faSink = this.fa[iterSink];

			      //loop through each q value, compute cross terms
			      for (int iterQ=0; iterQ<this.qValues.length; iterQ++)
			      {
			      	double faValue = 2.0*faSource[iterQ]*faSink[iterQ];

			      	double qr = this.qValues[iterQ]*r;
			      	double sincDerivative;
			      	if (qr==0 || Math.abs(qr)<1.0e-12)
			      		sincDerivative = 0.0;
			      	else
			      	  sincDerivative = dcenter[x]*(BasicMath.cos(qr)/r2-BasicMath.sin(qr)/(qr*r2));
			      	
			      	IqDerivative[iterQ]+= faValue*sincDerivative;
			      }
			    }
		      
		      //store the derivative
		      for (int iterQ=0; iterQ<this.qValues.length; iterQ++)
		      	this.J[iterQ][3*iterSource+x] = IqDerivative[iterQ];
	    	}
	    }
		}
	}
	
	/**
	 * The Class RowSumBruteProfile.
	 */
	private final class BruteSumThread implements Runnable
  {
		
		/** The atom array. */
		final private double[] atomArray;
		
		/** The fa. */
		final private double[][] fa; 
		
		/** The Iq. */
  	final private double[] Iq;
	  
  	/** The local iq. */
  	final private double[] localIq;
	  
  	/** The locks. */
  	final private ReentrantLock[] locks;
	  
  	/** The num atoms. */
  	final private int numAtoms;
	  
  	/** The num threads. */
  	final private int numThreads;
	  
  	/** The q values. */
		final private double[] qValues;
	  
  	/** The thread number. */
  	final private int threadNumber;
  	
	  private BruteSumThread(double[] Iq, double[] atomArray, double[][] fa, double[] qValues, ReentrantLock[] locks, int threadNumber, int numThreads)
  	{
  		this.atomArray = atomArray;
  		this.fa = fa;
  		this.qValues = qValues;
  		this.numAtoms = atomArray.length/3;
  		
  		this.Iq = Iq;
  		this.locks = locks;
  		this.threadNumber = threadNumber;
  		this.numThreads = numThreads;
  		
		  this.localIq = new double[Iq.length];
      Arrays.fill(this.localIq,0.0);
  	}
  	
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public final void run()
		{
			double[] faSource, faSink;
			
	    //loop through all the upper triangle atoms
	    for (int iterSource=this.threadNumber; iterSource<this.numAtoms; iterSource+=this.numThreads)
	    {
	      //get the form factor array for the first atom
	      faSource = this.fa[iterSource];
	      double x = this.atomArray[3*iterSource+0];
	      double y = this.atomArray[3*iterSource+1];
	      double z = this.atomArray[3*iterSource+2];

	      for (int iterSink=iterSource+1; iterSink<this.numAtoms; iterSink++)
		    {
		    	//compute distance between atoms
		    	//double r = BasicMath.euclideanDistance(x, y, z, atomArray[iter2][0], atomArray[iter2][1], atomArray[iter2][2]);
	      	double dx = x-this.atomArray[3*iterSink+0];
	      	double dy = y-this.atomArray[3*iterSink+1];
	      	double dz = z-this.atomArray[3*iterSink+2];
	      	double r = BasicMath.sqrt(dx*dx+dy*dy+dz*dz);

		      //get the form factor array for the second atom
		      faSink = this.fa[iterSink];

		      //loop through each q value, compute cross terms
		      for (int iterQ=0; iterQ<this.qValues.length; iterQ++)
		      {
		      	this.localIq[iterQ]+= 2.0*faSource[iterQ]*faSink[iterQ]*BasicMath.sinc(r*this.qValues[iterQ]);
		      }
		    }

		    //compute diagnal terms
		    for (int iterQ=0; iterQ<this.qValues.length; iterQ++)
		    {		     
		      //add the diagnal term and the previous value
		      this.localIq[iterQ] += faSource[iterQ]*faSource[iterQ];
		    }  	
	    }
	    
	    //add the current sum to the global total
	    if (this.numThreads>1)
	    {
		    for (int iterQ=0; iterQ<this.qValues.length; iterQ++)
		    {
		      this.locks[iterQ].lock();
		      try
		      {
		      	this.Iq[iterQ]+= this.localIq[iterQ];
		      }
		      finally
		      {
		      	this.locks[iterQ].unlock();
		      }
		    }
	    }
	    else
	    {
		    for (int iterQ=0; iterQ<this.qValues.length; iterQ++)
		      this.Iq[iterQ]+= this.localIq[iterQ];
	    }
	  }
  }

	/** The number of threads. */
	private final int numberOfThreads;
	
	/** The table. */
	private final FormFactorTable table;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7304916754588681830L;	
	
	/**
	 * Creates the multi threaded.
	 * 
	 * @param table
	 *          the table
	 * @return the brute scattering predictor
	 */
	public static BruteScatteringPredictor createMultiThreaded(FormFactorTable table)
	{
		return new BruteScatteringPredictor(table, Runtime.getRuntime().availableProcessors());
	}
	
	/**
	 * Creates the single threaded.
	 * 
	 * @param table
	 *          the table
	 * @return the brute scattering predictor
	 */
	public static BruteScatteringPredictor createSingleThreaded(FormFactorTable table)
	{
		return new BruteScatteringPredictor(table, 1);
	}
	
	/**
	 * Instantiates a new brute scattering predictor.
	 * 
	 * @param table
	 *          the table
	 */
	public BruteScatteringPredictor(FormFactorTable table)
	{
		this.table = table;
		this.numberOfThreads = Runtime.getRuntime().availableProcessors();
	}

	public BruteScatteringPredictor(FormFactorTable table, int numberOfThreads)
	{
		this.table = table;
		this.numberOfThreads = numberOfThreads;
	}
	
	public void computeJacobian(Molecule molecule, double[][] jacobian)
	{		
    //store atoms in an array for quick access
    final double[] atomArray = molecule.getAtomPositionSingleArray();
    final double[][] fa = this.table.getFormFactorArray(molecule);
    final double[] qValues = this.table.getQArray();
        
    //long startTime;
    //long endTime;
    
    //make sure that enough work exists in each thread
   	int numThreads = Math.max(1,Math.min(this.numberOfThreads, molecule.size()/100));
    
    //start timing
    //startTime = System.currentTimeMillis();
    
    //create the output profile
    double[] Iq = new double[qValues.length];
    Arrays.fill(Iq,0.0);
    
    if (numThreads==1)
    {
    	//do direct evaluations
    	BruteJacobianThread rowObj = new BruteJacobianThread(jacobian, atomArray, fa, qValues, 0, numThreads);  	
    	rowObj.run();
    }
    else
    {
      //create a thread pool
      //ExecutorService execSvc = Executors.newCachedThreadPool();
      ExecutorService execSvc = Executors.newFixedThreadPool(numThreads);

      //create locks for concurrancy
      ReentrantLock[] locks = new ReentrantLock[qValues.length];
      for (int iter=0; iter<locks.length; iter++)
        locks[iter] = new ReentrantLock();

      //start each thread with own offset
	    for (int iter=0; iter<numThreads; iter++)
	    {
	    	BruteJacobianThread rowObj = new BruteJacobianThread(jacobian, atomArray, fa, qValues, iter, numThreads);  	
	
	    	//rowObj.run();
	    	execSvc.execute(rowObj);
	    }

	    //shutdown the service
	    execSvc.shutdown();
	    try
			{
				execSvc.awaitTermination(60L, TimeUnit.DAYS);
			} 
	    catch (InterruptedException e) 
	    {
	    	execSvc.shutdownNow();
	    	throw new AssertionError("Unable to finish all tasks.");
	    }
  	}	        

    //end timing
    //endTime = System.currentTimeMillis();
    //double lastProfileComputeTime = (endTime-startTime)/1000.0;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#computeProfile(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public ScatteringPrediction computeProfile(Molecule molecule) throws SasRuntimeException
	{
    long startTime;
    long endTime;
    
    final double[] atomArray = molecule.getAtomPositionSingleArray();
    final double[][] fa = this.table.getFormFactorArray(molecule);
    final double[] qValues = this.table.getQArray();

    //make sure that enough work exists in each thread
   	int numThreads = Math.max(1,Math.min(this.numberOfThreads, molecule.size()/100));
    
    //start timing
    startTime = System.currentTimeMillis();
    
    //create the output profile
    double[] Iq = new double[qValues.length];
    Arrays.fill(Iq,0.0);
    
    if (numThreads==1)
    {
    	//do direct evaluations
    	BruteSumThread rowObj = new BruteSumThread(Iq, atomArray, fa, qValues, null, 0, numThreads);  	
    	rowObj.run();
    }
    else
    {
      //create a thread pool
      //ExecutorService execSvc = Executors.newCachedThreadPool();
      ExecutorService execSvc = Executors.newFixedThreadPool(numThreads);

      //create locks for concurrancy
      ReentrantLock[] locks = new ReentrantLock[qValues.length];
      for (int iter=0; iter<locks.length; iter++)
        locks[iter] = new ReentrantLock();

      //start each thread with own offset
	    for (int iter=0; iter<numThreads; iter++)
	    {
	    	BruteSumThread rowObj = new BruteSumThread(Iq, atomArray, fa, qValues, locks, iter, numThreads);
	
	    	//rowObj.run();
	    	execSvc.execute(rowObj);
	    }

	    //shutdown the service
	    execSvc.shutdown();
	    try
			{
				execSvc.awaitTermination(60L, TimeUnit.DAYS);
			} 
	    catch (InterruptedException e) 
	    {
	    	execSvc.shutdownNow();
	    	throw new AssertionError("Unable to finish all tasks.");
	    }
  	}	
        
    //end timing
    endTime = System.currentTimeMillis();
    double lastProfileComputeTime = (endTime-startTime)/1000.0;
        
    return new ScatteringPrediction(new ScatteringProfile(qValues, Iq, null), lastProfileComputeTime, 0.0);  	
	}
	
  @Override
	public ScatteringPrediction computeProfile(Molecule fromMolecule, Molecule toMolecule) throws SasRuntimeException
  {
  	throw new UnsupportedOperationException("Computation of scatting between to molecules not supproted yet.");
  }
	
	@Override
	public DifferentiableScatteringPrediction computeProfileAndJacobian(Molecule molecule) throws SasRuntimeException
	{
    long startTime = System.currentTimeMillis();
    
    double[][] jacobian = new double[this.table.sizeQ()][];
  	
  	for (int iter=0; iter<this.table.sizeQ(); iter++)
  		jacobian[iter] = new double[molecule.size()*3];
    
		computeJacobian(molecule, jacobian);
		ScatteringPrediction prediction = computeProfile(molecule);
		
    //end timing
    long endTime = System.currentTimeMillis();
    double lastProfileComputeTime = (endTime-startTime)/1000.0;
		    
		return new DifferentiableScatteringPrediction(prediction.getProfile(), lastProfileComputeTime, prediction.getRelativeErrorEstimate(), new Array2dRealMatrix(jacobian, false), 0.0);
	}
	
	@Override
	public DifferentiableScatteringPrediction computeProfileAndJacobian(Molecule molecule, double[][] jacobian) throws SasRuntimeException
	{
    long startTime = System.currentTimeMillis();
    
		computeJacobian(molecule, jacobian);
		ScatteringPrediction prediction = computeProfile(molecule);
		
    //end timing
    long endTime = System.currentTimeMillis();
    double lastProfileComputeTime = (endTime-startTime)/1000.0;
		    
		return new DifferentiableScatteringPrediction(prediction.getProfile(), lastProfileComputeTime, prediction.getRelativeErrorEstimate(), new Array2dRealMatrix(jacobian, false), 0.0);
	}
	
	@Override
	public ArrayList<ScatteringPrediction> computeProfiles(List<? extends Molecule> molecules) throws SasRuntimeException
	{
		ArrayList<ScatteringPrediction> profiles = new ArrayList<ScatteringPrediction>(molecules.size());
		
		for (Molecule mol : molecules)
		{
			profiles.add(computeProfile(mol));
		}
		
		return profiles;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#getErrorBound()
	 */
	@Override
	public double getErrorBound()
	{
		return 0;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.ScatteringPredictor#getFormFactorTable()
	 */
	@Override
	public FormFactorTable getFormFactorTable()
	{
		return this.table;
	}

}
