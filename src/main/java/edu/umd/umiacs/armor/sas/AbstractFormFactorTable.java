/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.sas;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.AtomType;
import edu.umd.umiacs.armor.molecule.Molecule;

/**
 * The Class FormFactorTable.
 */
public abstract class AbstractFormFactorTable implements Serializable, FormFactorTable
{	
	
	/** The q values. */
	protected final double[] qValues;
	/**
	 * 
	 */
	private static final long serialVersionUID = -1103263547354125826L;
		
	/**
	 * Instantiates a new form factor table.
	 * 
	 * @param qValues
	 *          the q values
	 * @throws SasRuntimeException
	 *           the sAS runtime exception
	 */
	public AbstractFormFactorTable(double[] qValues) throws SasRuntimeException
	{
		for (int iter=0; iter<qValues.length; iter++)
			if (qValues[iter]<0.0 || Double.isNaN(qValues[iter]) || Double.isInfinite(qValues[iter]))
				throw new SasRuntimeException("Invalid q value of "+qValues[iter]+" at index "+iter+".");
		
		this.qValues = qValues;		

	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactor(edu.umd.umiacs.armor.molecule.Molecule, int, int)
	 */
	@Override
	public double getFormFactor(Molecule molecule, int atomIndex, int qIndex)
	{
		double distanceToCore = molecule.distanceToCore(atomIndex);
		
		double formFactor;
		
		if (molecule.isDisplacedAtom(atomIndex))
			formFactor = getFormFactorDisplaced(qIndex);
		else
		if (molecule.isSolventAtom(atomIndex))
			formFactor = getFormFactorSolventLayer(distanceToCore, qIndex);
	  else
	  	formFactor = getFormFactorMolecule(molecule.getAtomType(atomIndex), qIndex);
		
		//output error message
		if (Double.isNaN(formFactor) || Double.isInfinite(formFactor))
		{
			String type;
			
			if (molecule.isDisplacedAtom(atomIndex))
				type = "displaced";
			else
			if (molecule.isSolventAtom(atomIndex))
				type = "solvent";
		  else
		  	type = "molecule";
			
			throw new SasRuntimeException("Form factor of "+type+" atom cannot have value of "+formFactor);
		}
		
		return formFactor;
	}
		
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#createFormFactorArray(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public double[][] getFormFactorArray(Molecule molecule)
	{
		double[][] formFactorArray = new double[molecule.size()][sizeQ()];
		
		for (int atomIndex=0; atomIndex<molecule.size(); atomIndex++)
			for (int qIndex=0; qIndex<sizeQ(); qIndex++)
				formFactorArray[atomIndex][qIndex] = getFormFactor(molecule, atomIndex, qIndex);
		
  	return formFactorArray;
 	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorArrayFrom(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public double[][] getFormFactorArrayFrom(Molecule molecule)
	{
		return getFormFactorArray(molecule);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorArrayTo(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public double[][] getFormFactorArrayTo(Molecule molecule)
	{
		return getFormFactorArray(molecule);
	}
	
	/**
	 * Gets the form factor displaced.
	 * 
	 * @param qIndex
	 *          the q index
	 * @return the form factor displaced
	 */
	public abstract double getFormFactorDisplaced(int qIndex);
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorFrom(edu.umd.umiacs.armor.molecule.Molecule, int, int)
	 */
	@Override
	public double getFormFactorFrom(Molecule molecule, int atomIndex, int qIndex)
	{
		return getFormFactor(molecule, atomIndex, qIndex);
	}
	
	/**
	 * Gets the form factor molecule.
	 * 
	 * @param type
	 *          the type
	 * @param qIndex
	 *          the q index
	 * @return the form factor molecule
	 */
	public abstract double getFormFactorMolecule(AtomType type, int qIndex);

	/**
	 * Gets the form factor solvent layer.
	 * 
	 * @param distanceToCore
	 *          the distance to core
	 * @param qIndex
	 *          the q index
	 * @return the form factor solvent layer
	 */
	public abstract double getFormFactorSolventLayer(double distanceToCore, int qIndex);

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getFormFactorTo(edu.umd.umiacs.armor.molecule.Molecule, int, int)
	 */
	@Override
	public double getFormFactorTo(Molecule molecule, int atomIndex, int qIndex)
	{
		return getFormFactor(molecule, atomIndex, qIndex);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getMaxQ()
	 */
	@Override
	public double getMaxQ()
	{
		return BasicMath.max(this.qValues);
	}
  
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getMinQ()
	 */
	@Override
	public double getMinQ()
	{
		return BasicMath.min(this.qValues);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getQ(int)
	 */
	@Override
	public double getQ(int index)
	{
		return this.qValues[index];
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#getQArray()
	 */
	@Override
	public double[] getQArray()
	{
		double[] qArray = new double[this.qValues.length];
		for (int iter=0; iter<this.qValues.length; iter++)
			qArray[iter] = this.qValues[iter];
		
		return qArray;
	}

	/**
	 * Num types.
	 * 
	 * @return the int
	 */
	public abstract int numTypes();

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.sas.FormFactorTable#sizeQ()
	 */
	@Override
	public int sizeQ()
	{
		return this.qValues.length;
	}
}
