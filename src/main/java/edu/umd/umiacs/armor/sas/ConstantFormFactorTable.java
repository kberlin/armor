package edu.umd.umiacs.armor.sas;

import edu.umd.umiacs.armor.molecule.AtomType;

public class ConstantFormFactorTable extends AbstractFormFactorTable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8029185612002595114L;
	private final double c;

	public ConstantFormFactorTable(double[] qValues, double c) throws SasRuntimeException
	{
		super(qValues);
		this.c = c;
	}

	@Override
	public double getFormFactorDisplaced(int qIndex)
	{
		return this.c;
	}

	@Override
	public double getFormFactorMolecule(AtomType type, int qIndex)
	{
		// TODO Auto-generated method stub
		return this.c;
	}

	@Override
	public double getFormFactorSolventLayer(double distanceToCore, int qIndex)
	{
		return this.c;
	}

	@Override
	public int numTypes()
	{
		return 1;
	}

}
