/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.SimpleBounds;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;

import edu.umd.umiacs.armor.math.func.MultivariateFunction;

public class BOBYQAOptimizer extends AbstractIterativeOptimizer<MultivariateFunction> implements BoundedOptimizer<MultivariateFunction>
{
	private final double initialTrustRegionRadius;
	private double[] lowerBounds;
	private final int numberOfInterpolationPoints;
	private double[] upperBounds;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6827131080505692473L;
	
	public BOBYQAOptimizer(int numberOfInterpolationPoints,  double initialTrustRegionRadius)
	{
		this.numberOfInterpolationPoints = numberOfInterpolationPoints;
		this.initialTrustRegionRadius = initialTrustRegionRadius;
		this.lowerBounds = null;
		this.upperBounds = null;
	}

	
	private org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction getCommonsFunction()
	{
		final MultivariateFunction func = getFunction();
		org.apache.commons.math3.analysis.MultivariateFunction returnFunction = new org.apache.commons.math3.analysis.MultivariateFunction()
		{		
			@Override
			public double value(double[] x)
			{
				return func.value(x);
			}
		};
		
	  return new org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction(returnFunction);
	}
	
	@Override
	public OptimizationResultImpl optimize(double[] x0)
	{
		//checks for the infinity norm
		org.apache.commons.math3.optim.nonlinear.scalar.noderiv.BOBYQAOptimizer optimizer = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.BOBYQAOptimizer(
				this.numberOfInterpolationPoints,
				this.initialTrustRegionRadius,
			  getAbsPointDifference());

		SimpleBounds bounds;
		if (this.lowerBounds==null && this.upperBounds==null)
			bounds = SimpleBounds.unbounded(x0.length);
		else
			bounds = new SimpleBounds(this.lowerBounds, this.upperBounds);

		try
		{
			PointValuePair val = optimizer.optimize(getCommonsFunction(), GoalType.MINIMIZE, 
					new InitialGuess(x0), bounds,  new MaxEval(getNumberEvaluations()+1));
			
			return new OptimizationResultImpl(val.getPoint(), val.getValue(), optimizer.getEvaluations());
		}
		catch (TooManyEvaluationsException e)
		{
			throw new OptimizationRuntimeException(e.getMessage());
		}	
	}
	
	@Override
	public void setBounds(double[] lowerBound, double[] upperBound)
	{
		this.lowerBounds = lowerBound;
		this.upperBounds = upperBound;
	}
}
