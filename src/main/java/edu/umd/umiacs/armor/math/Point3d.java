/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math;

import java.io.Serializable;
import java.util.Collection;

import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.HashCodeUtil;

/**
 * The Class Point3d.
 */
public final class Point3d implements Serializable
{
	
	/** The x. */
  public final double x;

	/** The y. */
  public final double y;;
	
  /** The z. */
  public final double z;
  
  /** The origin. */
	private static Point3d origin = new Point3d(0.0,0.0,0.0);
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 7178856136130661889L;
  
  /**
	 * Conver to position array.
	 * 
	 * @param list
	 *          the list
	 * @return the double[][]
	 */
  public static double[][] converToPositionArray(Collection<Point3d> list)
  {
  	double[][] array = new double[list.size()][3];
  	
  	int count=0;
  	for (Point3d p : list)
  	{
  		array[count][0] = p.x;
  		array[count][1] = p.y;
  		array[count][2] = p.z;
  		count++;
  	}
  	
  	return array;
  }
  
  /**
	 * Gets the origin.
	 * 
	 * @return the origin
	 */
  public static Point3d getOrigin()
  {
  	return origin;
  }
  
  /**
	 * Mean position.
	 * 
	 * @param list
	 *          the list
	 * @return the point3d
	 */
  public static Point3d meanPosition(Collection<Point3d> list)
  {
  	double x = 0.0;
  	double y = 0.0;
  	double z = 0.0;
  	
  	int count=0;
  	for (Point3d p : list)
  	{
  		x += p.x;
  		y += p.y;
  		z += p.z;
  		count++;
  	}
  	
  	return new Point3d(x/count, y/count, z/count);
  }
  
  /**
	 * Instantiates a new point3d.
	 * 
	 * @param x
	 *          the x
	 * @param y
	 *          the y
	 * @param z
	 *          the z
	 */
  public Point3d(double x, double y, double z)
  {
  	this.x = x;
  	this.y = y;
  	this.z = z;
  }

  public Point3d(double[] x)
  {
  	if (x==null || x.length!=3)
  		throw new MathRuntimeException("x must have dimension of 3.");
  	
  	this.x = x[0];
  	this.y = x[1];
  	this.z = x[2];
  }
  
  /**
	 * Adds the.
	 * 
	 * @param val
	 *          the val
	 * @return the point3d
	 */
  public Point3d add(double val)
  {
  	return new Point3d(this.x+val, this.y+val, this.z+val);
  }
  
  /**
	 * Adds the.
	 * 
	 * @param p
	 *          the p
	 * @return the point3d
	 */
  public Point3d add(Point3d p)
  {
  	return new Point3d(this.x+p.x, this.y+p.y, this.z+p.z);
  }
  
  /**
	 * Distance.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
  public double distance(Point3d p)
  {
    return BasicMath.sqrt(distanceSquared(p));
  }
  
  /**
	 * Distance squared.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
  public double distanceSquared(Point3d p)
  {
    double dx = this.x-p.x;
    double dy = this.y-p.y;
    double dz = this.z-p.z;
    
    return dx*dx+dy*dy+dz*dz;
  }

  /**
	 * Divide.
	 * 
	 * @param val
	 *          the val
	 * @return the point3d
	 */
  public Point3d divide(double val)
  {
  	return mult(1.0/val);
  }
  
  /* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point3d other = (Point3d) obj;
		if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(this.y) != Double.doubleToLongBits(other.y))
			return false;
		if (Double.doubleToLongBits(this.z) != Double.doubleToLongBits(other.z))
			return false;
		return true;
	}
  
  /**
	 * Equals.
	 * 
	 * @param p
	 *          the p
	 * @return true, if successful
	 */
	public boolean equals(Point3d p)
	{
		if (this == p)
			return true;
		if (p == null)
			return false;
		
		return (p.x == this.x && p.y==this.y && p.z == this.z);
	}
  
  /* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
    int result = HashCodeUtil.SEED;
    
    //collect the contributions of various fields
    result = HashCodeUtil.hash(result, this.x);
    result = HashCodeUtil.hash(result, this.y);
    result = HashCodeUtil.hash(result, this.z);
    
    return result;
	}
  
  public boolean hasNaN()
	{
		return Double.isNaN(this.x) || Double.isNaN(this.y) || Double.isNaN(this.z);
	}
  
  /**
	 * Inverse rotate.
	 * 
	 * @param R
	 *          the r
	 * @return the point3d
	 */
  public Point3d inverseRotate(Rotation R)
  {
  	return R.multInv(this);
  }
  
  /**
	 * Checks if is origin.
	 * 
	 * @return true, if is origin
	 */
  public boolean isOrigin()
  {
  	if (this.x == 0.0 && this.y == 0.0 && this.z == 0.0)
  		return true;
  	
  	return false;
  }
  
  /**
	 * Length.
	 * 
	 * @return the double
	 */
  public double length()
  {
    return BasicMath.sqrt(this.x*this.x+this.y*this.y+this.z*this.z);
  }
  
  /**
	 * Mult.
	 * 
	 * @param val
	 *          the val
	 * @return the point3d
	 */
  public Point3d mult(double val)
  {
  	return new Point3d(this.x*val,this.y*val,this.z*val);
  }
  
  /**
	 * Normalized.
	 * 
	 * @return the point3d
	 */
  public Point3d normalized()
  {
  	double r = length();
  	return new Point3d(this.x/r,this.y/r,this.z/r);
  }
  
  /**
	 * Rotate.
	 * 
	 * @param R
	 *          the r
	 * @return the point3d
	 */
  public Point3d rotate(Rotation R)
  {
  	return R.mult(this);
  }

	/**
	 * Element square.
	 * 
	 * @return the point3d
	 */
  public Point3d squared()
  {
  	return new Point3d(this.x*this.x,this.y*this.y,this.z*this.z);
  }

	/**
	 * Subtract.
	 * 
	 * @param val
	 *          the val
	 * @return the point3d
	 */
  public Point3d subtract(double val)
  {
  	return add(-val);
  }
	
	/**
	 * Subtract.
	 * 
	 * @param p
	 *          the p
	 * @return the point3d
	 */
  public Point3d subtract(Point3d p)
  {
  	return new Point3d(this.x-p.x, this.y-p.y, this.z-p.z);
  }

	/**
	 * To array.
	 * 
	 * @return the double[]
	 */
  public double[] toArray()
  {
  	return new double[]{this.x,this.y,this.z};
  }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return BasicUtils.toString(this.toArray());
	}
}
