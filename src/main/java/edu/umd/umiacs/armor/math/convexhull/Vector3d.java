/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.convexhull;

/*
 * Copyright John E. Lloyd, 2003. All rights reserved. Permission
 * to use, copy, and modify, without fee, is granted for non-commercial 
 * and research purposes, provided that this copyright notice appears 
 * in all copies.
 *
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 */

import java.util.Random;

/**
 * The Class Vector3d.
 */
public class Vector3d
{
	
	/** The x. */
	public double x;

	/** The y. */
	public double y;

	/** The z. */
	public double z;

	/** The Constant DOUBLE_PREC. */
	static private final double DOUBLE_PREC = 2.2204460492503131e-16;

	/**
	 * Instantiates a new vector3d.
	 */
	public Vector3d()
	{
	}

	/**
	 * Instantiates a new vector3d.
	 * 
	 * @param x
	 *          the x
	 * @param y
	 *          the y
	 * @param z
	 *          the z
	 */
	public Vector3d(double x, double y, double z)
	{
		set(x, y, z);
	}

	/**
	 * Instantiates a new vector3d.
	 * 
	 * @param v
	 *          the v
	 */
	public Vector3d(Vector3d v)
	{
		set(v);
	}

	/**
	 * Adds the.
	 * 
	 * @param v1
	 *          the v1
	 */
	public void add(Vector3d v1)
	{
		this.x += v1.x;
		this.y += v1.y;
		this.z += v1.z;
	}

	/**
	 * Adds the.
	 * 
	 * @param v1
	 *          the v1
	 * @param v2
	 *          the v2
	 */
	public void add(Vector3d v1, Vector3d v2)
	{
		this.x = v1.x + v2.x;
		this.y = v1.y + v2.y;
		this.z = v1.z + v2.z;
	}

	/**
	 * Cross.
	 * 
	 * @param v1
	 *          the v1
	 * @param v2
	 *          the v2
	 */
	public void cross(Vector3d v1, Vector3d v2)
	{
		double tmpx = v1.y * v2.z - v1.z * v2.y;
		double tmpy = v1.z * v2.x - v1.x * v2.z;
		double tmpz = v1.x * v2.y - v1.y * v2.x;

		this.x = tmpx;
		this.y = tmpy;
		this.z = tmpz;
	}

	/**
	 * Distance.
	 * 
	 * @param v
	 *          the v
	 * @return the double
	 */
	public double distance(Vector3d v)
	{
		double dx = this.x - v.x;
		double dy = this.y - v.y;
		double dz = this.z - v.z;

		return Math.sqrt(dx * dx + dy * dy + dz * dz);
	}

	/**
	 * Distance squared.
	 * 
	 * @param v
	 *          the v
	 * @return the double
	 */
	public double distanceSquared(Vector3d v)
	{
		double dx = this.x - v.x;
		double dy = this.y - v.y;
		double dz = this.z - v.z;

		return (dx * dx + dy * dy + dz * dz);
	}

	/**
	 * Dot.
	 * 
	 * @param v1
	 *          the v1
	 * @return the double
	 */
	public double dot(Vector3d v1)
	{
		return this.x * v1.x + this.y * v1.y + this.z * v1.z;
	}

	/**
	 * Gets the.
	 * 
	 * @param i
	 *          the i
	 * @return the double
	 */
	public double get(int i)
	{
		switch (i)
		{
			case 0:
			{
				return this.x;
			}
			case 1:
			{
				return this.y;
			}
			case 2:
			{
				return this.z;
			}
			default:
			{
				throw new ArrayIndexOutOfBoundsException(i);
			}
		}
	}

	/**
	 * Norm.
	 * 
	 * @return the double
	 */
	public double norm()
	{
		return Math.sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
	}

	/**
	 * Normalize.
	 */
	public void normalize()
	{
		double lenSqr = this.x * this.x + this.y * this.y + this.z * this.z;
		double err = lenSqr - 1;
		if (err > (2 * DOUBLE_PREC) || err < -(2 * DOUBLE_PREC))
		{
			double len = Math.sqrt(lenSqr);
			this.x /= len;
			this.y /= len;
			this.z /= len;
		}
	}

	/**
	 * Norm squared.
	 * 
	 * @return the double
	 */
	public double normSquared()
	{
		return this.x * this.x + this.y * this.y + this.z * this.z;
	}

	/**
	 * Scale.
	 * 
	 * @param s
	 *          the s
	 */
	public void scale(double s)
	{
		this.x = s * this.x;
		this.y = s * this.y;
		this.z = s * this.z;
	}

	/**
	 * Scale.
	 * 
	 * @param s
	 *          the s
	 * @param v1
	 *          the v1
	 */
	public void scale(double s, Vector3d v1)
	{
		this.x = s * v1.x;
		this.y = s * v1.y;
		this.z = s * v1.z;
	}

	/**
	 * Sets the.
	 * 
	 * @param x
	 *          the x
	 * @param y
	 *          the y
	 * @param z
	 *          the z
	 */
	public void set(double x, double y, double z)
	{
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Sets the.
	 * 
	 * @param i
	 *          the i
	 * @param value
	 *          the value
	 */
	public void set(int i, double value)
	{
		switch (i)
		{
			case 0:
			{
				this.x = value;
				break;
			}
			case 1:
			{
				this.y = value;
				break;
			}
			case 2:
			{
				this.z = value;
				break;
			}
			default:
			{
				throw new ArrayIndexOutOfBoundsException(i);
			}
		}
	}

	/**
	 * Sets the.
	 * 
	 * @param v1
	 *          the v1
	 */
	public void set(Vector3d v1)
	{
		this.x = v1.x;
		this.y = v1.y;
		this.z = v1.z;
	}

	/**
	 * Sets the random.
	 * 
	 * @param lower
	 *          the lower
	 * @param upper
	 *          the upper
	 * @param generator
	 *          the generator
	 */
	protected void setRandom(double lower, double upper, Random generator)
	{
		double range = upper - lower;

		this.x = generator.nextDouble() * range + lower;
		this.y = generator.nextDouble() * range + lower;
		this.z = generator.nextDouble() * range + lower;
	}

	/**
	 * Sets the zero.
	 */
	public void setZero()
	{
		this.x = 0;
		this.y = 0;
		this.z = 0;
	}

	/**
	 * Sub.
	 * 
	 * @param v1
	 *          the v1
	 */
	public void sub(Vector3d v1)
	{
		this.x -= v1.x;
		this.y -= v1.y;
		this.z -= v1.z;
	}

	/**
	 * Sub.
	 * 
	 * @param v1
	 *          the v1
	 * @param v2
	 *          the v2
	 */
	public void sub(Vector3d v1, Vector3d v2)
	{
		this.x = v1.x - v2.x;
		this.y = v1.y - v2.y;
		this.z = v1.z - v2.z;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return this.x + " " + this.y + " " + this.z;
	}
}
