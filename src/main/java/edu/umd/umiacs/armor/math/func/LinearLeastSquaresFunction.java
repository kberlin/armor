/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.func;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;

public class LinearLeastSquaresFunction implements DifferentiableMultivariateVectorFunction
{
	private final RealMatrix A;
	private final double[] y;
	
	public LinearLeastSquaresFunction(RealMatrix A)
	{
		this.A = A;
		this.y = null;
	}
	
	public LinearLeastSquaresFunction(RealMatrix A, double[] y)
	{
		this.A = A;
		this.y = y;
	}
	
	public LinearLeastSquaresFunction(double[][] A, double[] y)
	{
		this(new Array2dRealMatrix(A, false), y);
	}
	
	public RealMatrix getA()
	{
		return this.A;
	}
	
	public double[] multJacobian(double[] v)
	{
		return this.A.transpose().mult(this.A.mult(v));
	}

	@Override
	public double[] value(double[] x)
	{
		double[] val = this.A.mult(x);
		if (this.y==null)
			return val;
		
		return BasicMath.subtract(this.A.mult(x), this.y);
	}

	@Override
	public RealMatrix jacobian(double[] x)
	{
   return this.A;
	}
}
