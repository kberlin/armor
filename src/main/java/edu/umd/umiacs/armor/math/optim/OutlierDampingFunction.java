/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;

public final class OutlierDampingFunction implements MultivariateVectorFunction
{
	public enum DampingType
	{	
		/** The LOG. */
		LOG,
		
		/** The NONE. */
		NONE;
	}
	
	private final double cutoff;
	
	private final DampingType dampingType;

	private final double[] error;
	
	private final MultivariateVectorFunction func;
	
	private final double[] observations;
	
	public OutlierDampingFunction(MultivariateVectorFunction func, double[] observations, double[] error, double cutoff)
	{
		this(func, observations, error, cutoff, DampingType.LOG);
	}
	
	public OutlierDampingFunction(MultivariateVectorFunction func, double[] observations, double[] error, double cutoff, DampingType type)
	{
		this.func= func;
		this.cutoff = cutoff;
		this.observations = observations.clone();	
		this.error = error.clone();
		
		this.dampingType = type;
	}

	public final double logDampingFunction(double val, double observation, double error, double cutoff)
	{
		double normDiff = (val-observation)/error;
		double absNormDiff = Math.abs(normDiff);
		
		//if needs to be damped
		if (absNormDiff>cutoff)
		{
			val = observation+Math.signum(normDiff)*(cutoff+Math.log(absNormDiff-cutoff+1.0))*error;
		}

		return val;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.math3.analysis.MultivariateVectorFunction#value(double[])
	 */
	@Override
	public final double[] value(double[] x) throws IllegalArgumentException
	{
		double[] val = this.func.value(x);
		
		for (int iter=0; iter<val.length; iter++)
		{
			switch (this.dampingType)
			{
				case NONE : break;
				case LOG : val[iter] = logDampingFunction(val[iter], this.observations[iter], this.error[iter], this.cutoff); break;
			}
		}
		
		
		return val;
	}
}