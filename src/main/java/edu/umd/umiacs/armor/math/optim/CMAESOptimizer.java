/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.SimpleBounds;
import org.apache.commons.math3.optim.SimplePointChecker;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.random.MersenneTwister;

import edu.umd.umiacs.armor.math.func.MultivariateFunction;

public final class CMAESOptimizer extends AbstractIterativeOptimizer<MultivariateFunction> implements BoundedOptimizer<MultivariateFunction>
{
	
	private final double[] initialStandardDeviation;
	private double[] lowerBounds;
	private final int samplePopulationSize;
	private double[] upperBounds;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8668926084605357827L;
	
	public CMAESOptimizer(int samplePopulationSize, double[] initialStandardDeviation)
	{
		this.initialStandardDeviation = initialStandardDeviation;
		this.samplePopulationSize = samplePopulationSize;
		this.lowerBounds = null;
		this.upperBounds = null;
	}
	
	private org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction getCommonsFunction()
	{
		final MultivariateFunction func = getFunction();
		org.apache.commons.math3.analysis.MultivariateFunction returnFunction = new org.apache.commons.math3.analysis.MultivariateFunction()
		{		
			@Override
			public double value(double[] x)
			{
				return func.value(x);
			}
		};
		
	  return new org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction(returnFunction);
	}
	
	@Override
	public OptimizationResultImpl optimize(double[] x0)
	{
		//checks for the infinity norm
		org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer optimizer = new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer(
				getNumberEvaluations()+1, 0, true, 0, 1, new MersenneTwister(0), false, new SimplePointChecker<PointValuePair>(0.0, getAbsPointDifference(), getNumberEvaluations()));
		
		SimpleBounds bounds;
		if (this.lowerBounds==null && this.upperBounds==null)
			bounds = SimpleBounds.unbounded(x0.length);
		else
			bounds = new SimpleBounds(this.lowerBounds, this.upperBounds);
		
		try
		{
			PointValuePair val = optimizer.optimize(getCommonsFunction(),  GoalType.MINIMIZE, 
						new InitialGuess(x0), bounds,
						new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer.Sigma(this.initialStandardDeviation),
						new org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer.PopulationSize(this.samplePopulationSize),
						new MaxEval(getNumberEvaluations()+1));
			
			return new OptimizationResultImpl(val.getPoint(), val.getValue(), optimizer.getEvaluations());
		}
		catch (TooManyEvaluationsException e)
		{
			throw new OptimizationRuntimeException(e.getMessage());
		}	
	}

	@Override
	public void setBounds(double[] lowerBound, double[] upperBound)
	{
		this.lowerBounds = lowerBound;
		this.upperBounds = upperBound;
	}
}
