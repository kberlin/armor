/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import edu.umd.umiacs.armor.math.MathRuntimeException;

/**
 * The Class EigenDecompositionImpl.
 */
public final class EigenDecompositionImpl implements EigenDecomposition
{
  
  /** The diag. */
  private final double[] diag;

	/** The V. */
  private final OrthonormalMatrix V;
  
  /** The VT. */
  private final OrthonormalMatrix VT;
  
  /**
	 * 
	 */
	private static final long serialVersionUID = -2511379335001784762L;
	
  /**
	 * Instantiates a new eigen decomposition impl.
	 * 
	 * @param diag
	 *          the diag
	 * @param V
	 *          the v
	 */
  public EigenDecompositionImpl(double[] diag, OrthonormalMatrix V)
  {
  	if (diag.length != V.numColumns())
  		throw new MathRuntimeException("Diagnal length incompatable with size of V.");
  	
  	this.V = V;
  	this.VT = V.orthoInverse();
  	this.diag = diag.clone();
  }
  
  /**
	 * Instantiates a new eigen decomposition impl.
	 * 
	 * @param A
	 *          the a
	 */
  protected EigenDecompositionImpl(org.apache.commons.math3.linear.RealMatrix A)
  {
  	org.apache.commons.math3.linear.EigenDecomposition eig = new org.apache.commons.math3.linear.EigenDecomposition(A);
  	this.V = new OrthonormalMatrixImpl(eig.getV());
  	this.VT = new OrthonormalMatrixImpl(eig.getVT());
  	this.diag = eig.getRealEigenvalues();
  }
  
  /*
	public EigenDecompositionImpl(RealMatrix a)
	{
		this(a.getRealMatrixImpl().A);
	}
	*/
	
  /**
	 * Instantiates a new eigen decomposition impl.
	 * 
	 * @param A
	 *          the a
	 */
  public EigenDecompositionImpl(SymmetricMatrix A)
	{
		this(A.getApacheCommonsMatrix());
	}
  
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.EigenDecomposition#formMatrix()
	 */
	@Override
	public SymmetricMatrix formMatrix()
	{
		SymmetricMatrix diagnalMatrix = getD();
		SymmetricMatrixImpl newMatrix = new SymmetricMatrixImpl(this.V.mult(diagnalMatrix).mult(this.VT).getApacheCommonsMatrix());
		newMatrix.eig = this;
		
		return newMatrix;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.math.EigenDecompositionIn#getD()
	 */
	@Override
	public SymmetricMatrix getD()
	{
		return new SymmetricMatrixImpl(this.diag);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.math.EigenDecompositionIn#getEigenvalue(int)
	 */
	@Override
	public double getEigenvalue(int index)
	{
		return this.diag[index];
	}
		
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.math.EigenDecompositionIn#getEigenvalues()
	 */
	@Override
	public double[] getEigenvalues()
	{
		return this.diag.clone();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.math.EigenDecompositionIn#getV()
	 */
	@Override
	public OrthonormalMatrix getV()
	{
		return this.V;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.EigenDecomposition#getVT()
	 */
	@Override
	public OrthonormalMatrix getVT()
	{
		return this.VT;
	}

}
