package edu.umd.umiacs.armor.math.optim;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.ConjugateGradient;
import org.apache.commons.math3.linear.RealLinearOperator;
import org.apache.commons.math3.linear.RealVector;

public final class ConjugateGradientSolver
{
	private final ConjugateGradient solver;

	public ConjugateGradientSolver(int maxIterations, double relativeError)
	{
		this.solver = new ConjugateGradient(maxIterations, relativeError, false);
	}
	
	public double[] solve(double[][] A, double[] y)
	{
		double[] x = new double[A.length];
		this.solver.solveInPlace(new Array2DRowRealMatrix(A, false), new ArrayRealVector(y, false), new ArrayRealVector(x, false));
		
		return x;
	}
	
	protected double[] solve(RealLinearOperator A, RealVector y, double[] x0)
	{
		double[] x = x0.clone();
		this.solver.solveInPlace(A, y, new ArrayRealVector(x, false));
		
		return x;
	}
	
	public double[] solve(double[][] A, double[] y, double[] x0)
	{
		double[] x = x0.clone();
		this.solver.solveInPlace(new Array2DRowRealMatrix(A, false), new ArrayRealVector(y, false), new ArrayRealVector(x, false));
		
		return x;
	}

	public double[] solve(double[][] A, double[][] P, double[] y, double[] x0)
	{
		double[] x = x0.clone();
		this.solver.solveInPlace(new Array2DRowRealMatrix(A, false), new Array2DRowRealMatrix(P, false), new ArrayRealVector(y, false), new ArrayRealVector(x, false));
		
		return x;
	}

}
