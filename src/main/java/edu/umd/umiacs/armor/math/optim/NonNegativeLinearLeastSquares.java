/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.linear.QRDecomposition;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.util.BasicUtils;

/**
 * Adapted from
 * 
 * Copyright 2008 Josh Vermaas, except he's nice and instead prefers
 * this to be licensed under the LGPL. Since the license itself is longer
 * than the code, if this truly worries you, you can look up the text at
 * http://www.gnu.org/licenses/
 * Lawson and Hanson, "Solving Least Squares Problems", Prentice-Hall, 1974, Chapter 23, p. 161.
 */
public class NonNegativeLinearLeastSquares implements LinearLeastSquaresOptimizer
{
	private RealMatrix A;
	private RealMatrix At;
	private final double eps;
	private final int numberIterations;
	
	private static boolean isAllNegativeOrZero(double[] x)
	{
		for (double value : x)
			if (value>1.0e-15)
				return false;
		
		return true;
	}
	
	private static boolean isAllPositive(double[] x)
	{
		for (double xvalue : x)
			if (xvalue<=0)
				return false;
		
		return true;
	}
	
	private static int[] listIndicies(List<Integer> activeList)
	{
		//generate the index of active columns
		int[] columnIndicies = new int[activeList.size()];
		int count = 0;
		for (int index : activeList)
		{
			columnIndicies[count] = index;
			count++;
		}

		return columnIndicies;
	}
	
	private static double[] minusInPlace(double[] a, double[] b)
	{
  	for (int iter=0; iter<a.length; iter++)
  		a[iter] -= b[iter];
  	
  	return a;
	}
	
	public NonNegativeLinearLeastSquares(double eps, int numberIterations)
	{
		this.eps = eps;
		this.numberIterations = numberIterations;
	}

	@Override
	public int maxThreads()
	{
		return Integer.MAX_VALUE;
	}
	
	private double residualNorm(double[] x, double[] y)
	{
		return BasicMath.norm(minusInPlace(this.A.mult(x),y));
	}
	
	public void setA(double[][] A)
	{
		setA(new BlockRealMatrix(A));
	}

	@Override
	public void setA(RealMatrix A)
	{
		this.A = A;
		this.At = A.transpose();
	}

	@Override
	public double[] solve(double[] y)
	{
		if (this.A==null)
			throw new OptimizationRuntimeException("A matrix never set.");
		
		//get dimension size
		int xn = this.A.numColumns();

		ArrayList<Integer> activeList = new ArrayList<Integer>(xn);
		ArrayList<Integer> remainingList = new ArrayList<Integer>(xn);

		//generate the remaining list
		for (int index=0; index<xn; index++)
			remainingList.add(index);
		
		//generate row index
		int[] rowIndicies = new int[y.length];
		for (int index=0; index<y.length; index++)
			rowIndicies[index] = index;
				
		//evaluate the first solution
		double[] x =  BasicUtils.createArray(xn, 0.0);
		double fval = residualNorm(x, y);

		//init other variables
		double fvalPrev = Double.MAX_VALUE;
		int outsideIterations = 0;

		while(outsideIterations<this.numberIterations+xn && fval>this.eps && (fvalPrev-fval)>this.eps)
		{
			double[] w = this.At.mult(BasicMath.subtract(y, this.A.mult(x)));

			//check if there is a descent direction
			if(remainingList.size() == 0 || isAllNegativeOrZero(w))
			{
				break;
			}
			
			//find the best column to select next
			int bestIndex = -1;
			double max = Double.NEGATIVE_INFINITY;
			int count = 0;
			for (int activeListIndex : remainingList)
			{
				if (w[activeListIndex]>max)
				{
					bestIndex = count;
					max = w[activeListIndex];
				}
				count++;
			}
			
			//add the column to the active list
			activeList.add(remainingList.get(bestIndex));
			remainingList.remove(bestIndex);
		
			boolean allPositive = false;
			while(!allPositive)
			{
				//compute the submatrix
				RealMatrix Asub = this.A.getSubMatrix(rowIndicies, listIndicies(activeList));
				
				//solve the linear system
				QRDecomposition qr = new QRDecomposition(Asub);
				double[] xSub = qr.solve(y);
				//SingularValueDecomposition qr = new SingularValueDecomposition(Asub);
				//double[] xSub = qr.solve(y, 1.0e-15);
				
				//generate the full x solution
				double[] xNew = new double[xn];
				count = 0;
				for (int index : activeList)
				{
					xNew[index] = xSub[count];
					count++;
				}
				
				//check if the solution is nonnegative
				allPositive = isAllPositive(xSub);
				
				//if all positive accept as new solution
				if (allPositive)
				{
					x = xNew;
				}
				else
				{
					double alpha = Double.MAX_VALUE;
					for (int index : activeList)
					{
						if (xNew[index] <= 0)
						{
							double currAlpha = x[index]/(x[index]-xNew[index]);
							if (currAlpha < alpha)
								alpha = currAlpha;
						}
					}
					
					//threshold x so that no values is below 0
					x = BasicMath.add(x, BasicMath.mult(BasicMath.subtract(xNew,x),alpha));

					//remove almost zero values, and put back into general list
					for (int iter = activeList.size()-1; iter >= 0; iter--)
						if (Math.abs(x[activeList.get(iter)]) < 1.0e-15)
							remainingList.add(activeList.remove(iter));
				}
			}
			
			//update the iteration values
			fval = residualNorm(x, y);
			outsideIterations++;
		}
		
		return x;
	}
}
