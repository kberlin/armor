/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.DifferentiableMultivariateVectorFunction;
import edu.umd.umiacs.armor.math.linear.DiagnalMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.Pair;

public class ActiveSetLeastSquares extends AbstractActiveSetOptimizer<DifferentiableMultivariateVectorFunction> implements IterativeLeastSquaresOptimizer<DifferentiableMultivariateVectorFunction>
{
	final IterativeLeastSquaresOptimizer<DifferentiableMultivariateVectorFunction> optimizer;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2727794740254839104L;
	
	public static ActiveSetLeastSquares getLevenbergMarquardtOptimizer()
	{
		return new ActiveSetLeastSquares(new LevenbergMarquardtOptimizer());
	}
	
	public ActiveSetLeastSquares(IterativeLeastSquaresOptimizer<DifferentiableMultivariateVectorFunction> optimizer)
	{
		super(1.0e-10);
		this.optimizer = optimizer;
	}

	private double[] computeGradient(double[] x)
	{
		RealMatrix J = this.optimizer.getFunction().jacobian(x);
		double[] d = BasicMath.subtract(this.optimizer.getFunction().value(x), this.optimizer.getObservations());
		
		//multiply by the weight
		d = BasicMath.mult(d, this.optimizer.getWeights());
		
		double[] gradient = BasicMath.mult(J.transpose().mult(d), 2.0);
				
		return gradient;
	}
	
	@Override
	public double getAbsPointDifference()
	{
		return this.optimizer.getAbsPointDifference();
	}

	@Override
	public double[] getErrors()
	{
		return this.optimizer.getErrors();
	}
		
	@Override
	public DifferentiableMultivariateVectorFunction getFunction()
	{
		return this.optimizer.getFunction();
	}

	@Override
	public int getNumberEvaluations()
	{
		return this.optimizer.getNumberEvaluations();
	}

	@Override
	public double[] getObservations()
	{
		return this.optimizer.getObservations();
	}

	@Override
	public double[] getWeights()
	{
		return this.optimizer.getWeights();
	}
	
	@Override
	public void setAbsPointDifference(double absPointDifference)
	{
		this.optimizer.setAbsPointDifference(absPointDifference);
	}

	@Override
	public void setErrors(double[] errors)
	{
		this.optimizer.setErrors(errors);
	}
	
	@Override
	public void setNumberEvaluations(int numberEvaluations)
	{
		this.optimizer.setNumberEvaluations(numberEvaluations);
	}

	@Override
	public void setObservations(double[] observations)
	{
		this.optimizer.setObservations(observations);
	}

	@Override
	public void setWeights(double[] weights)
	{
		this.optimizer.setWeights(weights);
	}
	
	private LeastSquaresOptimizationResult generateResult(double[] x)
	{
		double[] values = getFunction().value(x);
		double chi2 = BasicStat.chi2(values, this.optimizer.getObservations(), this.optimizer.getErrors());
		return new LeastSquaresResultImpl(x, chi2, 0, values);		
	}

	private LeastSquaresOptimizationResult solveSubProblem(final double[] x0, LinearConstraints equalityConstraints)
	{
		final RealMatrix nullSpace;
		if (equalityConstraints.isEmpty())
		{
			nullSpace = new DiagnalMatrix(BasicUtils.createArray(x0.length, 1.0));
		}
		else
		{
			nullSpace = equalityConstraints.getNullSpace();
		}
		
		//there is no degree of freedom, so return feasible solution
		if (nullSpace==null)
			return generateResult(x0);

		//set the function
	  final DifferentiableMultivariateVectorFunction funOriginal = this.optimizer.getFunction();
		this.optimizer.setFunction(new DifferentiableMultivariateVectorFunction()
			{			
				@Override
				public final RealMatrix jacobian(double[] z)
				{
					RealMatrix J = funOriginal.jacobian(translate(z));
					J = J.mult(nullSpace);
					
					return J;
				}
				
				public final double[] translate(double[] z)
				{
					double[] x = BasicMath.add(x0, nullSpace.mult(z));
					
					//check if x violates range constraints
					if (violatesConstraints(x))
						throw new ViolatedConstraintsException(x);
					
					return x;
				}
				
				@Override
				public final double[] value(double[] z)
				{
					double[] y = funOriginal.value(translate(z));

					if (BasicUtils.hasNaN(y) || BasicUtils.hasInfinite(y))
						throw new OptimizationRuntimeException("Function value is NaN or Infinity for input x: "+translate(z));
					
					return y;
				}
			});
			
		//compute the initial guess
		double[] z0 = BasicUtils.createArray(nullSpace.numColumns(), 0.0);
		
		double[] x;
		try
		{
			LeastSquaresOptimizationResult resultSub = this.optimizer.optimize(z0);

			//move the solution back into original space
			double[] z = resultSub.getPoint();
		  x = BasicMath.add(x0, nullSpace.mult(z));
		  
			if (violatesConstraints(x))
				throw new ViolatedConstraintsException(x);
		  
		}
		catch(ViolatedConstraintsException e)
		{
			//find none violating point
			x = validValueSearch(x0, e.getX(), getConstraintAbsErrorTol());
		}
			
		//set the original function back
		this.optimizer.setFunction(funOriginal);

		return generateResult(x);
	}
	
	@Override
	public void setFunction(DifferentiableMultivariateVectorFunction function)
	{
		this.optimizer.setFunction(function);
	}

	@Override
	public LeastSquaresOptimizationResult optimize(double[] x0)
	{
		double[] x = x0;
		double[] xprev;
		
		LinearConstraints activeConstraints = new LinearConstraints();	
		ArrayList<Pair<LinearConstraints,Integer>> currActiveSet = new ArrayList<Pair<LinearConstraints,Integer>>();		
		ArrayList<Pair<LinearConstraints, Integer>> prevActiveSet = new ArrayList<Pair<LinearConstraints,Integer>>();
		
		if (violatesConstraints(x0))
			throw new OptimizationRuntimeException("Initial solution not feasible.");
		
		//check if the current solution is feasible
		LeastSquaresOptimizationResult bestResult = generateResult(x0);
		
		while (true)
		{
			//solve the problem with the current active set
			xprev = x;
			LeastSquaresOptimizationResult result = solveSubProblem(x, this.equalityConstraints.merge(activeConstraints));
			x = result.getPoint();
			
			//compute the gradient
			double[] g = computeGradient(x);
			
			//store previous result
			prevActiveSet = currActiveSet;

			//find inactive constraints
			ArrayList<Pair<LinearConstraints, Integer>> currInactiveSet = getInactiveConstrains(x, g, activeConstraints, prevActiveSet);			

			//find which constraints are activily violated
			currActiveSet = getViolatedConstraints(x);			
			currActiveSet.removeAll(currInactiveSet);
						
			//if violations have not changed from previous result, exit from loop
			if (currInactiveSet.isEmpty() && currActiveSet.size()==prevActiveSet.size())
			{				
				if (bestResult==null || bestResult.getChiSquared()>result.getChiSquared())
					bestResult = result;
				
				//only stop if at a real minimum and not just binary search
				if (BasicMath.norm(BasicMath.subtract(x, xprev))<getAbsPointDifference())
					break;
			}
			
			activeConstraints = generateActiveConstraints(currActiveSet);					
		}
		
		return bestResult;
	}
}
