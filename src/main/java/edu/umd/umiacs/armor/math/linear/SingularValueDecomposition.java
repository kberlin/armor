/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;


/**
 * The Class SingularValueDecomposition.
 */
public final class SingularValueDecomposition
{
	
	/** The svd. */
	protected final org.apache.commons.math3.linear.SingularValueDecomposition svd;
	
	/**
	 * Instantiates a new singular value decomposition.
	 * 
	 * @param A
	 *          the a
	 */
	public SingularValueDecomposition(RealMatrix A)
	{
		this.svd = new org.apache.commons.math3.linear.SingularValueDecomposition(A.getApacheCommonsMatrix());
	}
	
	/**
	 * Cond.
	 * 
	 * @return the double
	 */
	public double cond()
	{
		return this.svd.getConditionNumber();
	}
	
	/**
	 * Gets the s.
	 * 
	 * @return the s
	 */
	public SymmetricMatrix getS()
	{
	  return new SymmetricMatrixImpl(this.svd.getS());
	}
	
	/**
	 * Gets the singular values.
	 * 
	 * @return the singular values
	 */
	public double[] getSingularValues()
	{
		return this.svd.getSingularValues();
	}
	
	/**
	 * Gets the u.
	 * 
	 * @return the u
	 */
	public OrthonormalMatrix getU()
	{
		return new OrthonormalMatrixImpl(this.svd.getU());
	}
	
	/**
	 * Gets the uT.
	 * 
	 * @return the uT
	 */
	public OrthonormalMatrix getUT()
	{
		return new OrthonormalMatrixImpl(this.svd.getUT());
	}
	
	/**
	 * Gets the v.
	 * 
	 * @return the v
	 */
	public OrthonormalMatrix getV()
	{
		return new OrthonormalMatrixImpl(this.svd.getV());
	}

	/**
	 * Gets the vT.
	 * 
	 * @return the vT
	 */
	public OrthonormalMatrix getVT()
	{
		return new OrthonormalMatrixImpl(this.svd.getVT());
	}
		
	/**
	 * Pseudo inverse.
	 * 
	 * @param eps
	 *          the eps
	 * @return the real matrix
	 */
	public RealMatrix pseudoInverse(double eps)
	{
		OrthonormalMatrix V = getV();
		ApacheCommonsRealDynamicMatrixWrapper<org.apache.commons.math3.linear.RealMatrix> S = 
				new ApacheCommonsRealDynamicMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(getS().getApacheCommonsMatrix().copy());
		OrthonormalMatrix Ut = getUT();
				
		for (int iter=0; iter<Math.min(S.numRows(),S.numColumns()); iter++)
		{
			if (S.get(iter, iter)<eps)
				S.set(iter, iter, 0.0);
			else
				S.set(iter, iter, 1.0/S.get(iter, iter));
		}

		return V.mult(S).mult(Ut);
	}

	/**
	 * Pseudo inverse relative.
	 * 
	 * @param relEps
	 *          the rel eps
	 * @return the real matrix
	 */
	public RealMatrix pseudoInverseRelative(double relEps)
	{
		double[] s = getSingularValues();

		return pseudoInverse(relEps*s[0]);
	}
	
	/**
	 * Rank.
	 * 
	 * @param eps
	 *          the eps
	 * @return the int
	 */
	public int rank(double eps)
	{
		double[] s = getSingularValues();

		int count = 0;
		for (int iter=0; iter<s.length; iter++)
			if (s[iter] >= eps)
				count++;
		
		return count;
	}

	/**
	 * Rank relative.
	 * 
	 * @param relEps
	 *          the rel eps
	 * @return the int
	 */
	public int rankRelative(double relEps)
	{
		double[] s = getSingularValues();

		return rank(s[0]*relEps);
	}

	/**
	 * Solve.
	 * 
	 * @param B
	 *          the b
	 * @param eps
	 *          the eps
	 * @return the real matrix
	 */
	public RealMatrix solve(RealMatrix B, double eps)
	{
		RealMatrix Ainv = pseudoInverse(eps);
		
		return Ainv.mult(B);
	}
	
	public double[] solve(double[] y, double eps)
	{
		RealMatrix Ainv = pseudoInverse(eps);
		
		return Ainv.mult(y);
	}

}
