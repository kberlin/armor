/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.convexhull;

/*
 * Copyright John E. Lloyd, 2003. All rights reserved. Permission
 * to use, copy, and modify, without fee, is granted for non-commercial 
 * and research purposes, provided that this copyright notice appears 
 * in all copies.
 *
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 */

/**
 * The Class SimpleExample.
 */
public class SimpleExample
{
	
	/**
	 * The main method.
	 * 
	 * @param args
	 *          the arguments
	 */
	public static void main(String[] args)
	{
		// x y z coordinates of 6 points
		ConvexHullPoint3d[] points = new ConvexHullPoint3d[]
		{ new ConvexHullPoint3d(0.0, 0.0, 0.0), new ConvexHullPoint3d(1.0, 0.5, 0.0), new ConvexHullPoint3d(2.0, 0.0, 0.0), new ConvexHullPoint3d(0.5, 0.5, 0.5),
				new ConvexHullPoint3d(0.0, 0.0, 2.0), new ConvexHullPoint3d(0.1, 0.2, 0.3), new ConvexHullPoint3d(0.0, 2.0, 0.0), };

		QuickHull3D hull = new QuickHull3D();
		hull.build(points);

		System.out.println("Vertices:");
		ConvexHullPoint3d[] vertices = hull.getVertices();
		for (int i = 0; i < vertices.length; i++)
		{
			ConvexHullPoint3d pnt = vertices[i];
			System.out.println(pnt.x + " " + pnt.y + " " + pnt.z);
		}

		System.out.println("Faces:");
		int[][] faceIndices = hull.getFaces();
		for (int i = 0; i < vertices.length; i++)
		{
			for (int k = 0; k < faceIndices[i].length; k++)
			{
				System.out.print(faceIndices[i][k] + " ");
			}
			System.out.println("");
		}
	}
}
