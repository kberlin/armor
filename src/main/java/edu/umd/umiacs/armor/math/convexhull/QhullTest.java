/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.convexhull;

/*
 * Copyright John E. Lloyd, 2003. All rights reserved. Permission
 * to use, copy, and modify, without fee, is granted for non-commercial 
 * and research purposes, provided that this copyright notice appears 
 * in all copies.
 *
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 */

/**
 * The Class QhullTest.
 */
class QhullTest
{
	
	/** The coords. */
	static double[] coords = new double[]
	{};

	/** The faces. */
	static int[][] faces = new int[][]
	{};

	/**
	 * The main method.
	 * 
	 * @param args
	 *          the arguments
	 */
	public static void main(String[] args)
	{
		QuickHull3D hull = new QuickHull3D();
		QuickHull3DTest tester = new QuickHull3DTest();

		hull = new QuickHull3D();

		for (int i = 0; i < 100; i++)
		{
			double[] pnts = tester.randomCubedPoints(100, 1.0, 0.5);

			hull.setFromQhull(pnts, pnts.length / 3, /* triangulated= */false);

			pnts = tester.addDegeneracy(QuickHull3DTest.VERTEX_DEGENERACY, pnts, hull);

			// hull = new QuickHull3D ();
			hull.setFromQhull(pnts, pnts.length / 3, /* triangulated= */true);

			if (!hull.check(System.out))
			{
				System.out.println("failed for qhull triangulated");
			}

			// hull = new QuickHull3D ();
			hull.setFromQhull(pnts, pnts.length / 3, /* triangulated= */false);

			if (!hull.check(System.out))
			{
				System.out.println("failed for qhull regular");
			}

			// hull = new QuickHull3D ();
			hull.build(pnts, pnts.length / 3);
			hull.triangulate();

			if (!hull.check(System.out))
			{
				System.out.println("failed for QuickHull3D triangulated");
			}

			// hull = new QuickHull3D ();
			hull.build(pnts, pnts.length / 3);

			if (!hull.check(System.out))
			{
				System.out.println("failed for QuickHull3D regular");
			}
		}
	}
}
