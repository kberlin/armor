package edu.umd.umiacs.armor.math.optim;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.util.Pair;

public abstract class AbstractActiveSetOptimizer<F> implements LinearlyConstrainedOptimizer<F>
{
	private final double constraintAbsError;
	protected LinearConstraints equalityConstraints;
	protected LinearConstraints inequalityConstraints;
	protected LinearConstraints lowerBound;
	protected LinearConstraints upperBound;
	/**
	 * 
	 */
	private static final long serialVersionUID = -7991437583663060027L;

	public AbstractActiveSetOptimizer(double constraintAbsErrorTol)
	{
		this.equalityConstraints = new LinearConstraints();
		this.inequalityConstraints = new LinearConstraints();
		this.upperBound = new LinearConstraints();
		this.lowerBound = new LinearConstraints();
		this.constraintAbsError = constraintAbsErrorTol;
	}

	protected LinearConstraints generateActiveConstraints(ArrayList<Pair<LinearConstraints,Integer>> activeSet)
	{
		//create the new active set
		double[][] A = new double[activeSet.size()][];
		double[] b = new double[activeSet.size()];
		int iter=0;
		for (Pair<LinearConstraints, Integer> c : activeSet)
		{
			A[iter] = c.x.getConstraint(c.y);
			b[iter] = c.x.getConstraintBound(c.y);
			iter++;
		}
		LinearConstraints activeConstraints = new LinearConstraints(A, b);
	
		return activeConstraints;
	}
	
	public double getConstraintAbsErrorTol()
	{
		return this.constraintAbsError;
	}
	
	protected ArrayList<Pair<LinearConstraints,Integer>> getInactiveConstrains(double[] x, double[] g, LinearConstraints activeConstraints, ArrayList<Pair<LinearConstraints,Integer>> activeSet)
	{
		ArrayList<Integer> inactiveIndicies = activeConstraints.inactiveConstraints(g, getConstraintAbsErrorTol());
	
		ArrayList<Pair<LinearConstraints,Integer>> currInactiveSet = new ArrayList<Pair<LinearConstraints,Integer>>(inactiveIndicies.size());		
		for (int index : inactiveIndicies)
		{
			currInactiveSet.add(activeSet.get(index));
		}
		
		return currInactiveSet;
	}
		
	public ArrayList<Pair<LinearConstraints, Integer>> getViolatedConstraints(double[] x)
	{
		ArrayList<Pair<LinearConstraints, Integer>> currActiveSet = new ArrayList<Pair<LinearConstraints,Integer>>();		
		currActiveSet.addAll(getViolatedInequalityConstrains(x, this.inequalityConstraints));
		currActiveSet.addAll(getViolatedInequalityConstrains(x, this.lowerBound));
		currActiveSet.addAll(getViolatedInequalityConstrains(x, this.upperBound));
		
		return currActiveSet;
	}


	protected ArrayList<Pair<LinearConstraints,Integer>> getViolatedEqualityConstrains(double[] x, LinearConstraints c)
	{
		//find which constraints are newly violated
		ArrayList<Integer> currSetInequality = c.violatedEqualityConstraints(x, getConstraintAbsErrorTol());
		
		ArrayList<Pair<LinearConstraints,Integer>> currActiveSet = new ArrayList<Pair<LinearConstraints,Integer>>(currSetInequality.size());		
		for (Integer index : currSetInequality)
			currActiveSet.add(new Pair<LinearConstraints, Integer>(c, index));
		
		return currActiveSet;
	}

	protected ArrayList<Pair<LinearConstraints,Integer>> getViolatedInequalityConstrains(double[] x, LinearConstraints c)
	{
		//find which constraints are newly violated
		ArrayList<Integer> currSetInequality = c.violatedInequalityConstraints(x, getConstraintAbsErrorTol());
		
		ArrayList<Pair<LinearConstraints,Integer>> currActiveSet = new ArrayList<Pair<LinearConstraints,Integer>>(currSetInequality.size());		
		for (Integer index : currSetInequality)
			currActiveSet.add(new Pair<LinearConstraints, Integer>(c, index));
		
		return currActiveSet;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.BoundedOptimizer#setBounds(double[], double[])
	 */
	@Override
	public void setBounds(double[] lowerBound, double[] upperBound)
	{
		setLowerBound(lowerBound);
		setUpperBound(upperBound);
	}

	@Override
	public void setLinearEqualityConstraint(double[][] C, double[] d)
	{
		setLinearEqualityConstraint(new LinearConstraints(C, d));
	}


	@Override
	public void setLinearEqualityConstraint(LinearConstraints ec)
	{
		this.equalityConstraints = ec;
	}

	@Override
	public void setLinearEqualityConstraint(RealMatrix C, double[] d)
	{
		setLinearEqualityConstraint(new LinearConstraints(C.toArray(), d));
	}

	@Override
	public void setLinearInequalityConstraint(LinearConstraints ic)
	{
		this.inequalityConstraints = ic;
	}

	@Override
	public void setLinearInequalityConstraint(RealMatrix E, double[] f)
	{
		setLinearInequalityConstraint(new LinearConstraints(E.toArray(), f));
	}

	@Override
	public void setLowerBound(double[] lowerBound)
	{
		this.lowerBound = LinearConstraints.createLowerBound(lowerBound);
	}

	@Override
	public void setUpperBound(double[] upperBound)
	{
		this.upperBound = LinearConstraints.createUpperBound(upperBound);
	}

	protected double[] validValueSearch(double[] start, double[] end, double eps)
	{
		//ideally should be replaced by a cutoff value based on the maxim violated constraint
		
		if (!violatesConstraints(end))
			return end;
		
		double[] v = BasicMath.subtract(end, start);
		double h = BasicMath.norm(v);
		
		double[] p = start;
		double[] bestNonViolator = start;
		while(h>getConstraintAbsErrorTol()*1.0e-3)
		{			
			h = h/2.0;
			if (violatesConstraints(p))
				p = BasicMath.subtract(p, BasicMath.mult(v, h));
			else
			{
				bestNonViolator = p;
				p = BasicMath.add(p, BasicMath.mult(v, h));
			}
		}
		
		return bestNonViolator;
	}

	public boolean violatesConstraints(double[] x)
	{
		if (!this.lowerBound.isLessEqualsThan(x))
			return true;
		
		if (!this.upperBound.isLessEqualsThan(x))
			return true;
		
		return (!this.equalityConstraints.isFeasible(x) || !this.inequalityConstraints.isLessEqualsThan(x));
	}
}