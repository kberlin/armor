/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.stat;

import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.correlation.Covariance;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.MathRuntimeException;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3dImpl;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrixImpl;

/**
 * The Class Statistics.
 */
public final class BasicStat
{
	private static PearsonsCorrelation correlation = new PearsonsCorrelation();
	public static double NORMAL_CONFIDENCE_INTERVAL_95 = 1.959963984540;
	public static double NORMAL_CONFIDENCE_INTERVAL_99 = 2.575829303549;
	public static double NORMAL_CONFIDENCE_INTERVAL_99_5 = 2.807033768344;
	
	public static double NORMAL_CONFIDENCE_INTERVAL_99_9 = 3.290526731492;
	
	public static double akaikeInformationCriterion(double chi2, int k)
	{
		return chi2+2.0*(double)k;
	}
	
	public static double akaikeInformationCriterionCorrected(double chi2, int k, int n)
	{
		if (n-k-1<=0)
			throw new MathRuntimeException("AICc cannot be performed: n-k<1");
		
		return akaikeInformationCriterion(chi2,k)+(double)(2*k*(k+1))/(double)(n-k-1);
	}
	
	public static double chi(double[] a, double[] b)
	{
		return chi(a, b, null);
	}
	
	public static double chi(double[] a, double[] b, double[] std)
	{
		return BasicMath.sqrt(chi2(a, b, std));
	}
	
	public static double chi2(double[] a, double[] b)
	{
		return chi2(a, b, null);
	}

	public static double chi2(double[] a, double[] b, double[] std)
	{
		if (a.length != b.length)
			throw new MathRuntimeException("Sizes of vectors are different");
		
		if (std==null)
			return  BasicMath.normSquared(BasicMath.subtract(a,b));
		
		return BasicMath.normSquared(BasicMath.divide(BasicMath.subtract(a,b), std));
	}
		
	public static double cloreQualityFactor(double[] a, double[] aActual)
	{
		/*Clore, G. Marius, and Daniel S. Garrett. 
		 * "R-factor, Free R, and Complete Cross-validation for Dipolar Coupling Refinement of NMR Structures." 
		 * J. Am. Chem. Soc. 121, no. 39 (1999): 
		 * 9008-9012. 
		 * doi:10.1021/ja991789k.
		 * 
		 */
		
		if (a.length != aActual.length)
			throw new MathRuntimeException("Sizes of vectors are different");
		
		double[] diff = BasicMath.square(BasicMath.subtract(a, aActual));
		double[] actual = BasicMath.square(aActual);
		double meanSquared = BasicStat.mean(diff)/(2.0*BasicStat.mean(actual));
		
		return BasicMath.sqrt(meanSquared);
	}
	
	/**
	 * Covariance matrix.
	 * 
	 * @param list
	 *          the list
	 * @return the symmetric matrix3d
	 */
	public static SymmetricMatrix3d covarianceMatrix(Collection<Point3d> list)
	{
		if (list==null || list.isEmpty())
			throw new MathRuntimeException("List cannot be empty.");
		
		SymmetricMatrix cov = covMatrix(Point3d.converToPositionArray(list));
		
		return new SymmetricMatrix3dImpl(cov);
	}
	
	/**
	 * Cov matrix.
	 * 
	 * @param data
	 *          the data
	 * @return the symmetric matrix
	 */
	public static SymmetricMatrix covMatrix(double[][] data)
	{
		Covariance cov = new Covariance(data);
		return SymmetricMatrixImpl.createFromArray(cov.getCovarianceMatrix().getData());
	}
	
	/**
	 * Exponential distribution sample.
	 * 
	 * @param mean
	 *          the mean
	 * @return the double
	 */
	public static double exponentialDistributionSample(double mean)
	{
		ExponentialDistribution distribution = new ExponentialDistribution(mean);
		
		return distribution.sample();
	}
	
	public static double max(Collection<Double> a)
	{
		double max = Double.NEGATIVE_INFINITY;
		for (double val : a)
			if (max<val)
				max = val;
			
		return max;
	}
	
	public static double max(double[] a)
	{
		double max = Double.NEGATIVE_INFINITY;
		for (double val : a)
			if (max<val)
				max = val;
			
		return max;
	}	
	
	public static double mean(Collection<Double> a)
	{
		double val = 0.0;
		for (Double elem : a)
		{
			if (elem!=null)
				val += elem;
		}
			
		return val/a.size();  	
	}
	
	/**
	 * Mean.
	 * 
	 * @param a
	 *          the a
	 * @return the double
	 */
	public static double mean(double[] a)
	{
		double val = 0.0;
		for (double elem : a)
			val += elem;
			
		return val/a.length;  	
	}	

	/**
	 * Mean columns.
	 * 
	 * @param A
	 *          the a
	 * @return the double[]
	 */
	public static double[] meanColumns(double[][] A)
	{
		double[] val = new double[A[0].length];
		for (int iterColumn=0; iterColumn<val.length; iterColumn++)
			val[iterColumn] = 0.0;
		
		for (int iterRow=0; iterRow<A.length; iterRow++)
	  	for (int iterColumn=0; iterColumn<A[iterRow].length; iterColumn++)
	  		val[iterColumn] += A[iterRow][iterColumn];  		
		
		for (int iterColumn=0; iterColumn<val.length; iterColumn++)
			val[iterColumn] = val[iterColumn]/A.length;
	
		return val;  	
	}
	
	public static double median(Collection<Double> a)
	{
		double[] vals = new double[a.size()];
		int counter = 0;
		for (double val : a)
		{
			vals[counter] = val;
			counter++;
		}
		
		return StatUtils.percentile(vals, 50.0);
	}
	
	public static double median(double[] a)
	{
		return StatUtils.percentile(a, 50.0);
	}
	
	public static double min(Collection<Double> a)
	{
		double min = Double.POSITIVE_INFINITY;
		for (double val : a)
			if (min>val)
				min = val;
			
		return min;
	}
	

	public static double min(double[] a)
	{
		double min = Double.POSITIVE_INFINITY;
		for (double val : a)
			if (min>val)
				min = val;
			
		return min;
	}
	
	public static double normalDistributionSample(double mean, double std)
	{
		if (Double.isNaN(std))
			return Double.NaN;
		
		NormalDistribution normalDistribution = new NormalDistribution(0.0,1.0);

		return mean+normalDistribution.sample()*std;
	}
	
	public static double normalDistributionSamplePositive(double mean, double std)
	{
		if (Double.isNaN(mean))
			return Double.NaN;
		
		return Math.max(0.0, normalDistributionSample(mean, std));
	}


	/**
	 * Optimal scaling.
	 * 
	 * @param ofArray
	 *          the of array
	 * @param toArray
	 *          the to array
	 * @return the double
	 */
	public static double optimalScaling(double[] ofArray, double[] toArray)
	{
		if (ofArray.length != toArray.length)
			throw new MathRuntimeException("Sizes of vectors are different");
		
		double x = BasicMath.dotProduct(ofArray, toArray)/BasicMath.normSquared(ofArray);

		return x;
	}

	public static SymmetricMatrix3d orientationalSamplingMeasure(List<Point3d> normalVectors)
	{
		double[][] omega = new double[3][3];
		
		double xx = 0.0;
		double yy = 0.0;
		double zz = 0.0;
		double xy = 0.0;
		double xz = 0.0;
		double yz = 0.0;
		for (Point3d p : normalVectors)
		{
			p = p.normalized();
			xx += p.x*p.x;
			yy += p.y*p.y;
			zz += p.z*p.z;
			xy += p.x*p.y;
			xz += p.x*p.z;
			yz += p.y*p.z;
		}
		xx = xx/normalVectors.size();
		yy = yy/normalVectors.size();
		zz = zz/normalVectors.size();
		xy = xy/normalVectors.size();
		xz = xz/normalVectors.size();
		yz = yz/normalVectors.size();
		
		omega[0][0] = (3.0*xx-1.0)/2.0;
		omega[1][1] = (3.0*yy-1.0)/2.0;
		omega[2][2] = (3.0*zz-1.0)/2.0;
		omega[0][1] = (3.0*xy)/2.0;
		omega[0][2] = (3.0*xz)/2.0;
		omega[1][2] = (3.0*yz)/2.0;
		omega[1][0] = omega[0][1];
		omega[2][0] = omega[0][2];
		omega[2][1] = omega[1][2];
		
		return new SymmetricMatrix3dImpl(omega, false);
	}
		
	/**
	 * Pareto distribution sample.
	 * 
	 * @param alpha
	 *          the alpha
	 * @param max
	 *          the max
	 * @return the double
	 */
	public static double paretoDistributionSample(double alpha, double max)
	{
		double u = Math.random();
		
		return Math.pow(Math.pow(max, alpha+1.0)*u, 1.0/(alpha+1));
	}

	/**
	 * Pearsons correlation.
	 * 
	 * @param a
	 *          the a
	 * @param b
	 *          the b
	 * @return the double
	 */
	public static double pearsonsCorrelation(double[] a, double[] b)
	{
		return correlation.correlation(a, b);
	}
	
	/**
	 * Quality factor.
	 * 
	 * @param a
	 *          the a
	 * @param actual
	 *          the actual
	 * @return the double
	 */
	public static double qualityFactor(double[] a, double[] actual)
	{
		if (a.length != actual.length)
			throw new MathRuntimeException("Sizes of vectors are different");
		
		return BasicMath.norm(BasicMath.subtract(a,actual))/BasicMath.norm(actual);
	}
	
	public static double[] residuals(double[] predicted, double[] observed, double[] std)
	{
		return BasicMath.divide(BasicMath.subtract(predicted,observed), std);
	}
	
	public static double rms(double[] a)
	{
		return BasicMath.sqrt(BasicMath.normSquared(a)/(double)a.length);
	}
	
	public static double rmsOfSquares(double[] aSquared)
	{
		if (aSquared==null)
			return 0.0;
		
		double sum = 0.0;
		for (double val : aSquared)
		{
			if (val<0.0)
				throw new MathRuntimeException("Values cannot be negative.");
			
			sum+= val;
		}

		return BasicMath.sqrt(sum/(double)aSquared.length);
	}
	
	/**
	 * Rmsd.
	 * 
	 * @param a
	 *          the a
	 * @param b
	 *          the b
	 * @return the double
	 */
	public static double rmsd(double[] a, double[] b)
	{
		if (a.length != b.length)
			throw new MathRuntimeException("Sizes of vectors are different");
		
		return rms(BasicMath.subtract(a,b));
	}

	public static double rotdifQualityFactor(double[] a, double[] aActual)
	{
		if (a.length != aActual.length)
			throw new MathRuntimeException("Sizes of vectors are different");
		
		double[] diff = BasicMath.subtract(a, aActual);
		double meanActual = BasicStat.mean(aActual);
		double meanAdjusted = 2.0*BasicStat.mean(BasicMath.square(BasicMath.subtract(aActual, meanActual)));
		double meanSquared = BasicStat.mean(BasicMath.square(diff));
		
		return BasicMath.sqrt(meanSquared/meanAdjusted);
	}
	
	/**
	 * Scaled quality factor.
	 * 
	 * @param a
	 *          the a
	 * @param aActual
	 *          the a actual
	 * @return the double
	 */
	public static double scaledQualityFactor(double[] a, double[] aActual)
	{
		if (a.length != aActual.length)
			throw new MathRuntimeException("Sizes of vectors are different");
		
		double scale = optimalScaling(a, aActual);
		double[] aScaled = BasicMath.mult(a, scale);
		
		return BasicMath.norm(BasicMath.subtract(aScaled,aActual))/BasicMath.norm(aActual);
	}
	
	public static double std(Collection<Double> a)
	{
		return variance(a);
	}
	
	public static double std(double[] a)
	{
		return BasicMath.sqrt(variance(a));
	}
	
	public static double std(double[] a, double mean)
	{
		return BasicMath.sqrt(StatUtils.variance(a, mean));
	}

	
	/**
	 * Uniform distribution sample.
	 * 
	 * @param min
	 *          the min
	 * @param max
	 *          the max
	 * @return the double
	 */
	public static double uniformDistributionSample(double min, double max)
	{
		if (min>max)
			throw new MathRuntimeException("Minimum value must be smaller than maximum value.");
		
		return Math.random()*(max-min)+min;
	}
	
	public static double variance(Collection<Double> a)
	{
		double u = mean(a);
		
		double sum = 0.0;
		for (double val : a)
		{
			sum += BasicMath.square(val-u);
		}
		
		return sum/(double)(a.size()-1);
	}
	

	public static double variance(double[] a)
	{
		return StatUtils.variance(a);
	}
}
