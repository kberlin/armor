/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.interpolation;

import edu.umd.umiacs.armor.math.func.TwoVariableFunction;

/**
 * The Class BicubicSplineInterpolator.
 */
public class BicubicSplineInterpolator
{
	
	/**
	 * The Class SplineFunctionWrapper.
	 */
	private final class SplineFunctionWrapper implements TwoVariableFunction
	{
		/** The func. */
		private final org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolatingFunction func;
		
		/**
		 * Instantiates a new spline function wrapper.
		 * 
		 * @param func
		 *          the func
		 */
		public SplineFunctionWrapper(org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolatingFunction func)
		{
			this.func = func;
		}

		/* (non-Javadoc)
		 * @see edu.umd.umiacs.armor.math.func.TwoVariableFunction#value(double, double)
		 */
		@Override
		public double value(double a, double b)
		{
			return this.func.value(a, b);
		}
	}
	
	
	/** The interpolator. */
	org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolator interpolator;
	
	/**
	 * Instantiates a new bicubic spline interpolator.
	 */
	public BicubicSplineInterpolator()
	{
		this.interpolator = new org.apache.commons.math3.analysis.interpolation.BicubicSplineInterpolator();
	}
	
	/**
	 * Interpolate.
	 * 
	 * @param arg0
	 *          the arg0
	 * @param arg1
	 *          the arg1
	 * @param arg2
	 *          the arg2
	 * @return the two variable function
	 */
	public TwoVariableFunction interpolate(double[] arg0, double[] arg1, double[][] arg2)
	{
		return new SplineFunctionWrapper(this.interpolator.interpolate(arg0, arg1, arg2));
	}

}
