/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import edu.umd.umiacs.armor.math.BasicMath;

/**
 * The Class OrthonormalMatrixImpl.
 */
public class OrthonormalMatrixImpl extends ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix> implements OrthonormalMatrix
{
	private static final OrthonormalMatrixImpl i1 = createIdentityMatrixHelper(1);
	private static final OrthonormalMatrixImpl i2 = createIdentityMatrixHelper(2);
	private static final OrthonormalMatrixImpl i3 = createIdentityMatrixHelper(3);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -579306045110908154L;

	public static OrthonormalMatrixImpl createIdentityMatrix(int size)
	{
		switch (size)
		{
			case 3 : return i3;
			case 2 : return i2;
			case 1 : return i1;
			default : return createIdentityMatrixHelper(size);
		}
	}
	
	private static OrthonormalMatrixImpl createIdentityMatrixHelper(int size)
	{
		return new OrthonormalMatrixImpl(new BlockRealMatrix(BasicMath.createIdentityMatrix(size, size)));
	}
	
	protected OrthonormalMatrixImpl(org.apache.commons.math3.linear.RealMatrix A)
	{
		super(A);
	}

	protected OrthonormalMatrixImpl(RealMatrix A)
	{
		super(A.getApacheCommonsMatrix());
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.RealMatrixImpl#conditionEstimate()
	 */
	@Override
	public double conditionEstimate()
	{
		return 1.0;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.OrthonormalMatrix#orthoInverse()
	 */
	@Override
	public OrthonormalMatrix orthoInverse()
	{
		return new OrthonormalMatrixImpl(this.A.transpose());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.RealMatrixImpl#pseudoInverse()
	 */
	@Override
	public RealMatrix pseudoInverse()
	{
		return transpose();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.RealMatrixImpl#pseudoInverse(double)
	 */
	@Override
	public RealMatrix pseudoInverse(double eps)
	{
		return transpose();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.RealMatrixImpl#pseudoInverseRelative(double)
	 */
	@Override
	public RealMatrix pseudoInverseRelative(double relEps)
	{
		return transpose();
	}
}
