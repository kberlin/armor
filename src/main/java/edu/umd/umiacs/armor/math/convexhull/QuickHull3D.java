/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.convexhull;

/*
 * Copyright John E. Lloyd, 2003. All rights reserved. Permission
 * to use, copy, and modify, without fee, is granted for non-commercial 
 * and research purposes, provided that this copyright notice appears 
 * in all copies.
 *
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 */

import java.util.*;
import java.io.*;

/**
 * The Class QuickHull3D.
 */
public final class QuickHull3D
{
	
	// estimated size of the point set
	/** The char length. */
	protected double charLength;

	/** The claimed. */
	private VertexList claimed = new VertexList();

	/** The debug. */
	protected boolean debug = false;

	/** The discarded faces. */
	private Face[] discardedFaces = new Face[3];

	/** The explicit tolerance. */
	protected double explicitTolerance = AUTOMATIC_TOLERANCE;

	/** The faces. */
	protected ArrayList<Face> faces = new ArrayList<Face>(16);

	/** The find index. */
	protected int findIndex = -1;

	/** The horizon. */
	protected ArrayList<HalfEdge> horizon = new ArrayList<HalfEdge>(16);

	/** The max vtxs. */
	private Vertex[] maxVtxs = new Vertex[3];
	
	/** The min vtxs. */
	private Vertex[] minVtxs = new Vertex[3];
	
	/** The new faces. */
	private FaceList newFaces = new FaceList();

	/** The num faces. */
	protected int numFaces;
	
	/** The num points. */
	protected int numPoints;

	/** The num vertices. */
	protected int numVertices;
	
	/** The point buffer. */
	protected Vertex[] pointBuffer = new Vertex[0];

	/** The tolerance. */
	protected double tolerance;
	
	/** The unclaimed. */
	private VertexList unclaimed = new VertexList();
	
	/** The vertex point indices. */
	protected int[] vertexPointIndices = new int[0];

	/** The Constant AUTOMATIC_TOLERANCE. */
	public static final double AUTOMATIC_TOLERANCE = -1;
	
	/** The Constant CLOCKWISE. */
	public static final int CLOCKWISE = 0x1;
	
	/** The Constant DOUBLE_PREC. */
	static private final double DOUBLE_PREC = 2.2204460492503131e-16;

	/** The Constant INDEXED_FROM_ONE. */
	public static final int INDEXED_FROM_ONE = 0x2;
	
	/** The Constant INDEXED_FROM_ZERO. */
	public static final int INDEXED_FROM_ZERO = 0x4;

	/** The Constant NONCONVEX. */
	private static final int NONCONVEX = 2;

	/** The Constant NONCONVEX_WRT_LARGER_FACE. */
	private static final int NONCONVEX_WRT_LARGER_FACE = 1;

	/** The Constant POINT_RELATIVE. */
	public static final int POINT_RELATIVE = 0x8;

	/**
	 * Instantiates a new quick hull3 d.
	 */
	public QuickHull3D()
	{
	}

	/**
	 * Instantiates a new quick hull3 d.
	 * 
	 * @param points
	 *          the points
	 * @throws IllegalArgumentException
	 *           the illegal argument exception
	 */
	public QuickHull3D(ConvexHullPoint3d[] points) throws IllegalArgumentException
	{
		build(points, points.length);
	}

	/**
	 * Instantiates a new quick hull3 d.
	 * 
	 * @param coords
	 *          the coords
	 * @throws IllegalArgumentException
	 *           the illegal argument exception
	 */
	public QuickHull3D(double[] coords) throws IllegalArgumentException
	{
		build(coords, coords.length / 3);
	}

	/**
	 * Adds the adjoining face.
	 * 
	 * @param eyeVtx
	 *          the eye vtx
	 * @param he
	 *          the he
	 * @return the half edge
	 */
	private HalfEdge addAdjoiningFace(Vertex eyeVtx, HalfEdge he)
	{
		Face face = Face.createTriangle(eyeVtx, he.tail(), he.head());
		this.faces.add(face);
		face.getEdge(-1).setOpposite(he.getOpposite());
		return face.getEdge(0);
	}

	/**
	 * Adds the new faces.
	 * 
	 * @param newFaces
	 *          the new faces
	 * @param eyeVtx
	 *          the eye vtx
	 * @param horizon2
	 *          the horizon2
	 */
	protected void addNewFaces(FaceList newFaces, Vertex eyeVtx, ArrayList<HalfEdge> horizon2)
	{
		newFaces.clear();

		HalfEdge hedgeSidePrev = null;
		HalfEdge hedgeSideBegin = null;

		for (Iterator<HalfEdge> it = horizon2.iterator(); it.hasNext();)
		{
			HalfEdge horizonHe = it.next();
			HalfEdge hedgeSide = addAdjoiningFace(eyeVtx, horizonHe);
			if (this.debug)
			{
				System.out.println("new face: " + hedgeSide.face.getVertexString());
			}
			if (hedgeSidePrev != null)
			{
				hedgeSide.next.setOpposite(hedgeSidePrev);
			} else
			{
				hedgeSideBegin = hedgeSide;
			}
			newFaces.add(hedgeSide.getFace());
			hedgeSidePrev = hedgeSide;
		}
		hedgeSideBegin.next.setOpposite(hedgeSidePrev);
	}

	/**
	 * Adds the point to face.
	 * 
	 * @param vtx
	 *          the vtx
	 * @param face
	 *          the face
	 */
	private void addPointToFace(Vertex vtx, Face face)
	{
		vtx.face = face;

		if (face.outside == null)
		{
			this.claimed.add(vtx);
		} else
		{
			this.claimed.insertBefore(vtx, face.outside);
		}
		face.outside = vtx;
	}

	/**
	 * Adds the point to hull.
	 * 
	 * @param eyeVtx
	 *          the eye vtx
	 */
	protected void addPointToHull(Vertex eyeVtx)
	{
		this.horizon.clear();
		this.unclaimed.clear();

		if (this.debug)
		{
			System.out.println("Adding point: " + eyeVtx.index);
			System.out.println(" which is " + eyeVtx.face.distanceToPlane(eyeVtx.pnt) + " above face "
					+ eyeVtx.face.getVertexString());
		}
		removePointFromFace(eyeVtx, eyeVtx.face);
		calculateHorizon(eyeVtx.pnt, null, eyeVtx.face, this.horizon);
		this.newFaces.clear();
		addNewFaces(this.newFaces, eyeVtx, this.horizon);

		// first merge pass ... merge faces which are non-convex
		// as determined by the larger face

		for (Face face = this.newFaces.first(); face != null; face = face.next)
		{
			if (face.mark == Face.VISIBLE)
			{
				while (doAdjacentMerge(face, NONCONVEX_WRT_LARGER_FACE))
					;
			}
		}
		// second merge pass ... merge faces which are non-convex
		// wrt either face
		for (Face face = this.newFaces.first(); face != null; face = face.next)
		{
			if (face.mark == Face.NON_CONVEX)
			{
				face.mark = Face.VISIBLE;
				while (doAdjacentMerge(face, NONCONVEX))
					;
			}
		}
		resolveUnclaimedPoints(this.newFaces);
	}

	/**
	 * Builds the.
	 * 
	 * @param points
	 *          the points
	 * @throws IllegalArgumentException
	 *           the illegal argument exception
	 */
	public void build(ConvexHullPoint3d[] points) throws IllegalArgumentException
	{
		build(points, points.length);
	}

	/**
	 * Builds the.
	 * 
	 * @param points
	 *          the points
	 * @param nump
	 *          the nump
	 * @throws IllegalArgumentException
	 *           the illegal argument exception
	 */
	public void build(ConvexHullPoint3d[] points, int nump) throws IllegalArgumentException
	{
		if (nump < 4)
		{
			throw new IllegalArgumentException("Less than four input points specified");
		}
		if (points.length < nump)
		{
			throw new IllegalArgumentException("Point array too small for specified number of points");
		}
		initBuffers(nump);
		setPoints(points, nump);
		buildHull();
	}

	/**
	 * Builds the.
	 * 
	 * @param coords
	 *          the coords
	 * @throws IllegalArgumentException
	 *           the illegal argument exception
	 */
	public void build(double[] coords) throws IllegalArgumentException
	{
		build(coords, coords.length / 3);
	}

	/**
	 * Builds the.
	 * 
	 * @param coords
	 *          the coords
	 * @param nump
	 *          the nump
	 * @throws IllegalArgumentException
	 *           the illegal argument exception
	 */
	public void build(double[] coords, int nump) throws IllegalArgumentException
	{
		if (nump < 4)
		{
			throw new IllegalArgumentException("Less than four input points specified");
		}
		if (coords.length / 3 < nump)
		{
			throw new IllegalArgumentException("Coordinate array too small for specified number of points");
		}
		initBuffers(nump);
		setPoints(coords, nump);
		buildHull();
	}

	/**
	 * Builds the hull.
	 */
	protected void buildHull()
	{
		int cnt = 0;
		Vertex eyeVtx;

		computeMaxAndMin();
		createInitialSimplex();
		while ((eyeVtx = nextPointToAdd()) != null)
		{
			addPointToHull(eyeVtx);
			cnt++;
			if (this.debug)
			{
				System.out.println("iteration " + cnt + " done");
			}
		}
		reindexFacesAndVertices();
		if (this.debug)
		{
			System.out.println("hull done");
		}
	}

	/**
	 * Calculate horizon.
	 * 
	 * @param eyePnt
	 *          the eye pnt
	 * @param edge0
	 *          the edge0
	 * @param face
	 *          the face
	 * @param horizon2
	 *          the horizon2
	 */
	protected void calculateHorizon(ConvexHullPoint3d eyePnt, HalfEdge edge0, Face face, ArrayList<HalfEdge> horizon2)
	{
		// oldFaces.add (face);
		deleteFacePoints(face, null);
		face.mark = Face.DELETED;
		if (this.debug)
		{
			System.out.println("  visiting face " + face.getVertexString());
		}
		HalfEdge edge;
		if (edge0 == null)
		{
			edge0 = face.getEdge(0);
			edge = edge0;
		} else
		{
			edge = edge0.getNext();
		}
		do
		{
			Face oppFace = edge.oppositeFace();
			if (oppFace.mark == Face.VISIBLE)
			{
				if (oppFace.distanceToPlane(eyePnt) > this.tolerance)
				{
					calculateHorizon(eyePnt, edge.getOpposite(), oppFace, horizon2);
				} else
				{
					horizon2.add(edge);
					if (this.debug)
					{
						System.out.println("  adding horizon edge " + edge.getVertexString());
					}
				}
			}
			edge = edge.getNext();
		} while (edge != edge0);
	}

	// private void printPoints(PrintStream ps)
	// {
	// for (int i = 0; i < numPoints; i++)
	// {
	// Point3d pnt = pointBuffer[i].pnt;
	// ps.println(pnt.x + ", " + pnt.y + ", " + pnt.z + ",");
	// }
	// }

	/**
	 * Check.
	 * 
	 * @param ps
	 *          the ps
	 * @return true, if successful
	 */
	public boolean check(PrintStream ps)
	{
		return check(ps, getDistanceTolerance());
	}

	/**
	 * Check.
	 * 
	 * @param ps
	 *          the ps
	 * @param tol
	 *          the tol
	 * @return true, if successful
	 */
	public boolean check(PrintStream ps, double tol)

	{
		// check to make sure all edges are fully connected
		// and that the edges are convex
		double dist;
		double pointTol = 10 * tol;

		if (!checkFaces(this.tolerance, ps))
		{
			return false;
		}

		// check point inclusion

		for (int i = 0; i < this.numPoints; i++)
		{
			ConvexHullPoint3d pnt = this.pointBuffer[i].pnt;
			for (Iterator<Face> it = this.faces.iterator(); it.hasNext();)
			{
				Face face = it.next();
				if (face.mark == Face.VISIBLE)
				{
					dist = face.distanceToPlane(pnt);
					if (dist > pointTol)
					{
						if (ps != null)
						{
							ps.println("Point " + i + " " + dist + " above face " + face.getVertexString());
						}
						return false;
					}
				}
			}
		}
		return true;
	}

	/**
	 * Check face convexity.
	 * 
	 * @param face
	 *          the face
	 * @param tol
	 *          the tol
	 * @param ps
	 *          the ps
	 * @return true, if successful
	 */
	protected boolean checkFaceConvexity(Face face, double tol, PrintStream ps)
	{
		double dist;
		HalfEdge he = face.he0;
		do
		{
			face.checkConsistency();
			// make sure edge is convex
			dist = oppFaceDistance(he);
			if (dist > tol)
			{
				if (ps != null)
				{
					ps.println("Edge " + he.getVertexString() + " non-convex by " + dist);
				}
				return false;
			}
			dist = oppFaceDistance(he.opposite);
			if (dist > tol)
			{
				if (ps != null)
				{
					ps.println("Opposite edge " + he.opposite.getVertexString() + " non-convex by " + dist);
				}
				return false;
			}
			if (he.next.oppositeFace() == he.oppositeFace())
			{
				if (ps != null)
				{
					ps.println("Redundant vertex " + he.head().index + " in face " + face.getVertexString());
				}
				return false;
			}
			he = he.next;
		} while (he != face.he0);
		return true;
	}

	/**
	 * Check faces.
	 * 
	 * @param tol
	 *          the tol
	 * @param ps
	 *          the ps
	 * @return true, if successful
	 */
	protected boolean checkFaces(double tol, PrintStream ps)
	{
		// check edge convexity
		boolean convex = true;
		for (Iterator<Face> it = this.faces.iterator(); it.hasNext();)
		{
			Face face = it.next();
			if (face.mark == Face.VISIBLE)
			{
				if (!checkFaceConvexity(face, tol, ps))
				{
					convex = false;
				}
			}
		}
		return convex;
	}

	/**
	 * Compute max and min.
	 */
	protected void computeMaxAndMin()
	{
		Vector3d max = new Vector3d();
		Vector3d min = new Vector3d();

		for (int i = 0; i < 3; i++)
		{
			this.maxVtxs[i] = this.minVtxs[i] = this.pointBuffer[0];
		}
		max.set(this.pointBuffer[0].pnt);
		min.set(this.pointBuffer[0].pnt);

		for (int i = 1; i < this.numPoints; i++)
		{
			ConvexHullPoint3d pnt = this.pointBuffer[i].pnt;
			if (pnt.x > max.x)
			{
				max.x = pnt.x;
				this.maxVtxs[0] = this.pointBuffer[i];
			} else if (pnt.x < min.x)
			{
				min.x = pnt.x;
				this.minVtxs[0] = this.pointBuffer[i];
			}
			if (pnt.y > max.y)
			{
				max.y = pnt.y;
				this.maxVtxs[1] = this.pointBuffer[i];
			} else if (pnt.y < min.y)
			{
				min.y = pnt.y;
				this.minVtxs[1] = this.pointBuffer[i];
			}
			if (pnt.z > max.z)
			{
				max.z = pnt.z;
				this.maxVtxs[2] = this.pointBuffer[i];
			} else if (pnt.z < min.z)
			{
				min.z = pnt.z;
				this.maxVtxs[2] = this.pointBuffer[i];
			}
		}

		// this epsilon formula comes from QuickHull, and I'm
		// not about to quibble.
		this.charLength = Math.max(max.x - min.x, max.y - min.y);
		this.charLength = Math.max(max.z - min.z, this.charLength);
		if (this.explicitTolerance == AUTOMATIC_TOLERANCE)
		{
			this.tolerance = 3
					* DOUBLE_PREC
					* (Math.max(Math.abs(max.x), Math.abs(min.x)) + Math.max(Math.abs(max.y), Math.abs(min.y)) + Math.max(
							Math.abs(max.z), Math.abs(min.z)));
		} else
		{
			this.tolerance = this.explicitTolerance;
		}
	}

	// private void splitFace (Face face)
	// {
	// Face newFace = face.split();
	// if (newFace != null)
	// { newFaces.add (newFace);
	// splitFace (newFace);
	// splitFace (face);
	// }
	// }

	/**
	 * Creates the initial simplex.
	 * 
	 * @throws IllegalArgumentException
	 *           the illegal argument exception
	 */
	protected void createInitialSimplex() throws IllegalArgumentException
	{
		double max = 0;
		int imax = 0;

		for (int i = 0; i < 3; i++)
		{
			double diff = this.maxVtxs[i].pnt.get(i) - this.minVtxs[i].pnt.get(i);
			if (diff > max)
			{
				max = diff;
				imax = i;
			}
		}

		if (max <= this.tolerance)
		{
			throw new IllegalArgumentException("Input points appear to be coincident");
		}
		Vertex[] vtx = new Vertex[4];
		// set first two vertices to be those with the greatest
		// one dimensional separation

		vtx[0] = this.maxVtxs[imax];
		vtx[1] = this.minVtxs[imax];

		// set third vertex to be the vertex farthest from
		// the line between vtx0 and vtx1
		Vector3d u01 = new Vector3d();
		Vector3d diff02 = new Vector3d();
		Vector3d nrml = new Vector3d();
		Vector3d xprod = new Vector3d();
		double maxSqr = 0;
		u01.sub(vtx[1].pnt, vtx[0].pnt);
		u01.normalize();
		for (int i = 0; i < this.numPoints; i++)
		{
			diff02.sub(this.pointBuffer[i].pnt, vtx[0].pnt);
			xprod.cross(u01, diff02);
			double lenSqr = xprod.normSquared();
			if (lenSqr > maxSqr && this.pointBuffer[i] != vtx[0] && // paranoid
					this.pointBuffer[i] != vtx[1])
			{
				maxSqr = lenSqr;
				vtx[2] = this.pointBuffer[i];
				nrml.set(xprod);
			}
		}
		if (Math.sqrt(maxSqr) <= 100 * this.tolerance)
		{
			throw new IllegalArgumentException("Input points appear to be colinear");
		}
		nrml.normalize();

		double maxDist = 0;
		double d0 = vtx[2].pnt.dot(nrml);
		for (int i = 0; i < this.numPoints; i++)
		{
			double dist = Math.abs(this.pointBuffer[i].pnt.dot(nrml) - d0);
			if (dist > maxDist && this.pointBuffer[i] != vtx[0] && // paranoid
					this.pointBuffer[i] != vtx[1] && this.pointBuffer[i] != vtx[2])
			{
				maxDist = dist;
				vtx[3] = this.pointBuffer[i];
			}
		}
		if (Math.abs(maxDist) <= 100 * this.tolerance)
		{
			throw new IllegalArgumentException("Input points appear to be coplanar");
		}

		if (this.debug)
		{
			System.out.println("initial vertices:");
			System.out.println(vtx[0].index + ": " + vtx[0].pnt);
			System.out.println(vtx[1].index + ": " + vtx[1].pnt);
			System.out.println(vtx[2].index + ": " + vtx[2].pnt);
			System.out.println(vtx[3].index + ": " + vtx[3].pnt);
		}

		Face[] tris = new Face[4];

		if (vtx[3].pnt.dot(nrml) - d0 < 0)
		{
			tris[0] = Face.createTriangle(vtx[0], vtx[1], vtx[2]);
			tris[1] = Face.createTriangle(vtx[3], vtx[1], vtx[0]);
			tris[2] = Face.createTriangle(vtx[3], vtx[2], vtx[1]);
			tris[3] = Face.createTriangle(vtx[3], vtx[0], vtx[2]);

			for (int i = 0; i < 3; i++)
			{
				int k = (i + 1) % 3;
				tris[i + 1].getEdge(1).setOpposite(tris[k + 1].getEdge(0));
				tris[i + 1].getEdge(2).setOpposite(tris[0].getEdge(k));
			}
		} else
		{
			tris[0] = Face.createTriangle(vtx[0], vtx[2], vtx[1]);
			tris[1] = Face.createTriangle(vtx[3], vtx[0], vtx[1]);
			tris[2] = Face.createTriangle(vtx[3], vtx[1], vtx[2]);
			tris[3] = Face.createTriangle(vtx[3], vtx[2], vtx[0]);

			for (int i = 0; i < 3; i++)
			{
				int k = (i + 1) % 3;
				tris[i + 1].getEdge(0).setOpposite(tris[k + 1].getEdge(1));
				tris[i + 1].getEdge(2).setOpposite(tris[0].getEdge((3 - i) % 3));
			}
		}

		for (int i = 0; i < 4; i++)
		{
			this.faces.add(tris[i]);
		}

		for (int i = 0; i < this.numPoints; i++)
		{
			Vertex v = this.pointBuffer[i];

			if (v == vtx[0] || v == vtx[1] || v == vtx[2] || v == vtx[3])
			{
				continue;
			}

			maxDist = this.tolerance;
			Face maxFace = null;
			for (int k = 0; k < 4; k++)
			{
				double dist = tris[k].distanceToPlane(v.pnt);
				if (dist > maxDist)
				{
					maxFace = tris[k];
					maxDist = dist;
				}
			}
			if (maxFace != null)
			{
				addPointToFace(v, maxFace);
			}
		}
	}

	/**
	 * Delete face points.
	 * 
	 * @param face
	 *          the face
	 * @param absorbingFace
	 *          the absorbing face
	 */
	protected void deleteFacePoints(Face face, Face absorbingFace)
	{
		Vertex faceVtxs = removeAllPointsFromFace(face);
		if (faceVtxs != null)
		{
			if (absorbingFace == null)
			{
				this.unclaimed.addAll(faceVtxs);
			} else
			{
				Vertex vtxNext = faceVtxs;
				for (Vertex vtx = vtxNext; vtx != null; vtx = vtxNext)
				{
					vtxNext = vtx.next;
					double dist = absorbingFace.distanceToPlane(vtx.pnt);
					if (dist > this.tolerance)
					{
						addPointToFace(vtx, absorbingFace);
					} else
					{
						this.unclaimed.add(vtx);
					}
				}
			}
		}
	}

	/**
	 * Do adjacent merge.
	 * 
	 * @param face
	 *          the face
	 * @param mergeType
	 *          the merge type
	 * @return true, if successful
	 */
	private boolean doAdjacentMerge(Face face, int mergeType)
	{
		HalfEdge hedge = face.he0;

		boolean convex = true;
		do
		{
			Face oppFace = hedge.oppositeFace();
			boolean merge = false;
			@SuppressWarnings("unused")
			double dist1, dist2;

			if (mergeType == NONCONVEX)
			{ // then merge faces if they are definitively non-convex
				if (oppFaceDistance(hedge) > -this.tolerance || oppFaceDistance(hedge.opposite) > -this.tolerance)
				{
					merge = true;
				}
			} else
			// mergeType == NONCONVEX_WRT_LARGER_FACE
			{ // merge faces if they are parallel or non-convex
				// wrt to the larger face; otherwise, just mark
				// the face non-convex for the second pass.
				if (face.area > oppFace.area)
				{
					if ((dist1 = oppFaceDistance(hedge)) > -this.tolerance)
					{
						merge = true;
					} else if (oppFaceDistance(hedge.opposite) > -this.tolerance)
					{
						convex = false;
					}
				} else
				{
					if (oppFaceDistance(hedge.opposite) > -this.tolerance)
					{
						merge = true;
					} else if (oppFaceDistance(hedge) > -this.tolerance)
					{
						convex = false;
					}
				}
			}

			if (merge)
			{
				if (this.debug)
				{
					System.out.println("  merging " + face.getVertexString() + "  and  " + oppFace.getVertexString());
				}

				int numd = face.mergeAdjacentFace(hedge, this.discardedFaces);
				for (int i = 0; i < numd; i++)
				{
					deleteFacePoints(this.discardedFaces[i], face);
				}
				if (this.debug)
				{
					System.out.println("  result: " + face.getVertexString());
				}
				return true;
			}
			hedge = hedge.next;
		} while (hedge != face.he0);
		if (!convex)
		{
			face.mark = Face.NON_CONVEX;
		}
		return false;
	}

	/**
	 * Find half edge.
	 * 
	 * @param tail
	 *          the tail
	 * @param head
	 *          the head
	 * @return the half edge
	 */
	private HalfEdge findHalfEdge(Vertex tail, Vertex head)
	{
		// brute force ... OK, since setHull is not used much
		for (Iterator<Face> it = this.faces.iterator(); it.hasNext();)
		{
			HalfEdge he = (it.next()).findEdge(tail, head);
			if (he != null)
			{
				return he;
			}
		}
		return null;
	}

	/**
	 * Gets the debug.
	 * 
	 * @return the debug
	 */
	public boolean getDebug()
	{
		return this.debug;
	}

	/**
	 * Gets the distance tolerance.
	 * 
	 * @return the distance tolerance
	 */
	public double getDistanceTolerance()
	{
		return this.tolerance;
	}

	/**
	 * Gets the explicit distance tolerance.
	 * 
	 * @return the explicit distance tolerance
	 */
	public double getExplicitDistanceTolerance()
	{
		return this.explicitTolerance;
	}

	/**
	 * Gets the face indices.
	 * 
	 * @param indices
	 *          the indices
	 * @param face
	 *          the face
	 * @param flags
	 *          the flags
	 * @return the face indices
	 */
	private void getFaceIndices(int[] indices, Face face, int flags)
	{
		boolean ccw = ((flags & CLOCKWISE) == 0);
		boolean indexedFromOne = ((flags & INDEXED_FROM_ONE) != 0);
		boolean pointRelative = ((flags & POINT_RELATIVE) != 0);

		HalfEdge hedge = face.he0;
		int k = 0;
		do
		{
			int idx = hedge.head().index;
			if (pointRelative)
			{
				idx = this.vertexPointIndices[idx];
			}
			if (indexedFromOne)
			{
				idx++;
			}
			indices[k++] = idx;
			hedge = (ccw ? hedge.next : hedge.prev);
		} while (hedge != face.he0);
	}

	/**
	 * Gets the faces.
	 * 
	 * @return the faces
	 */
	public int[][] getFaces()
	{
		return getFaces(0);
	}

	/**
	 * Gets the faces.
	 * 
	 * @param indexFlags
	 *          the index flags
	 * @return the faces
	 */
	public int[][] getFaces(int indexFlags)
	{
		int[][] allFaces = new int[this.faces.size()][];
		int k = 0;
		for (Iterator<Face> it = this.faces.iterator(); it.hasNext();)
		{
			Face face = it.next();
			allFaces[k] = new int[face.numVertices()];
			getFaceIndices(allFaces[k], face, indexFlags);
			k++;
		}
		return allFaces;
	}

	/**
	 * Gets the num faces.
	 * 
	 * @return the num faces
	 */
	public int getNumFaces()
	{
		return this.faces.size();
	}

	/**
	 * Gets the num vertices.
	 * 
	 * @return the num vertices
	 */
	public int getNumVertices()
	{
		return this.numVertices;
	}

	/**
	 * Gets the vertex point indices.
	 * 
	 * @return the vertex point indices
	 */
	public int[] getVertexPointIndices()
	{
		int[] indices = new int[this.numVertices];
		for (int i = 0; i < this.numVertices; i++)
		{
			indices[i] = this.vertexPointIndices[i];
		}
		return indices;
	}

	/**
	 * Gets the vertices.
	 * 
	 * @return the vertices
	 */
	public ConvexHullPoint3d[] getVertices()
	{
		ConvexHullPoint3d[] vtxs = new ConvexHullPoint3d[this.numVertices];
		for (int i = 0; i < this.numVertices; i++)
		{
			vtxs[i] = this.pointBuffer[this.vertexPointIndices[i]].pnt;
		}
		return vtxs;
	}

	/**
	 * Gets the vertices.
	 * 
	 * @param coords
	 *          the coords
	 * @return the vertices
	 */
	public int getVertices(double[] coords)
	{
		for (int i = 0; i < this.numVertices; i++)
		{
			ConvexHullPoint3d pnt = this.pointBuffer[this.vertexPointIndices[i]].pnt;
			coords[i * 3 + 0] = pnt.x;
			coords[i * 3 + 1] = pnt.y;
			coords[i * 3 + 2] = pnt.z;
		}
		return this.numVertices;
	}

	/**
	 * Inits the buffers.
	 * 
	 * @param nump
	 *          the nump
	 */
	protected void initBuffers(int nump)
	{
		if (this.pointBuffer.length < nump)
		{
			Vertex[] newBuffer = new Vertex[nump];
			this.vertexPointIndices = new int[nump];
			for (int i = 0; i < this.pointBuffer.length; i++)
			{
				newBuffer[i] = this.pointBuffer[i];
			}
			for (int i = this.pointBuffer.length; i < nump; i++)
			{
				newBuffer[i] = new Vertex();
			}
			this.pointBuffer = newBuffer;
		}
		this.faces.clear();
		this.claimed.clear();
		this.numFaces = 0;
		this.numPoints = nump;
	}

	/**
	 * Mark face vertices.
	 * 
	 * @param face
	 *          the face
	 * @param mark
	 *          the mark
	 */
	private void markFaceVertices(Face face, int mark)
	{
		HalfEdge he0 = face.getFirstEdge();
		HalfEdge he = he0;
		do
		{
			he.head().index = mark;
			he = he.next;
		} while (he != he0);
	}

	/**
	 * Next point to add.
	 * 
	 * @return the vertex
	 */
	protected Vertex nextPointToAdd()
	{
		if (!this.claimed.isEmpty())
		{
			Face eyeFace = this.claimed.first().face;
			Vertex eyeVtx = null;
			double maxDist = 0;
			for (Vertex vtx = eyeFace.outside; vtx != null && vtx.face == eyeFace; vtx = vtx.next)
			{
				double dist = eyeFace.distanceToPlane(vtx.pnt);
				if (dist > maxDist)
				{
					maxDist = dist;
					eyeVtx = vtx;
				}
			}
			return eyeVtx;
		} else
		{
			return null;
		}
	}
	
	/**
	 * Opp face distance.
	 * 
	 * @param he
	 *          the he
	 * @return the double
	 */
	protected double oppFaceDistance(HalfEdge he)
	{
		return he.face.distanceToPlane(he.opposite.face.getCentroid());
	}

	/**
	 * Prints the.
	 * 
	 * @param ps
	 *          the ps
	 */
	public void print(PrintStream ps)
	{
		print(ps, 0);
	}

	/**
	 * Prints the.
	 * 
	 * @param ps
	 *          the ps
	 * @param indexFlags
	 *          the index flags
	 */
	public void print(PrintStream ps, int indexFlags)
	{
		if ((indexFlags & INDEXED_FROM_ZERO) == 0)
		{
			indexFlags |= INDEXED_FROM_ONE;
		}
		for (int i = 0; i < this.numVertices; i++)
		{
			ConvexHullPoint3d pnt = this.pointBuffer[this.vertexPointIndices[i]].pnt;
			ps.println("v " + pnt.x + " " + pnt.y + " " + pnt.z);
		}
		for (Iterator<Face> fi = this.faces.iterator(); fi.hasNext();)
		{
			Face face = fi.next();
			int[] indices = new int[face.numVertices()];
			getFaceIndices(indices, face, indexFlags);

			ps.print("f");
			for (int k = 0; k < indices.length; k++)
			{
				ps.print(" " + indices[k]);
			}
			ps.println("");
		}
	}

	/**
	 * Prints the qhull errors.
	 * 
	 * @param proc
	 *          the proc
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	private void printQhullErrors(Process proc) throws IOException
	{
		boolean wrote = false;
		InputStream es = proc.getErrorStream();
		while (es.available() > 0)
		{
			System.out.write(es.read());
			wrote = true;
		}
		if (wrote)
		{
			System.out.println("");
		}
	}

	/**
	 * Reindex faces and vertices.
	 */
	protected void reindexFacesAndVertices()
	{
		for (int i = 0; i < this.numPoints; i++)
		{
			this.pointBuffer[i].index = -1;
		}
		// remove inactive faces and mark active vertices
		this.numFaces = 0;
		for (Iterator<Face> it = this.faces.iterator(); it.hasNext();)
		{
			Face face = it.next();
			if (face.mark != Face.VISIBLE)
			{
				it.remove();
			} else
			{
				markFaceVertices(face, 0);
				this.numFaces++;
			}
		}
		// reindex vertices
		this.numVertices = 0;
		for (int i = 0; i < this.numPoints; i++)
		{
			Vertex vtx = this.pointBuffer[i];
			if (vtx.index == 0)
			{
				this.vertexPointIndices[this.numVertices] = i;
				vtx.index = this.numVertices++;
			}
		}
	}

	/**
	 * Removes the all points from face.
	 * 
	 * @param face
	 *          the face
	 * @return the vertex
	 */
	private Vertex removeAllPointsFromFace(Face face)
	{
		if (face.outside != null)
		{
			Vertex end = face.outside;
			while (end.next != null && end.next.face == face)
			{
				end = end.next;
			}
			this.claimed.delete(face.outside, end);
			end.next = null;
			return face.outside;
		} else
		{
			return null;
		}
	}

	/**
	 * Removes the point from face.
	 * 
	 * @param vtx
	 *          the vtx
	 * @param face
	 *          the face
	 */
	private void removePointFromFace(Vertex vtx, Face face)
	{
		if (vtx == face.outside)
		{
			if (vtx.next != null && vtx.next.face == face)
			{
				face.outside = vtx.next;
			} else
			{
				face.outside = null;
			}
		}
		this.claimed.delete(vtx);
	}

	/**
	 * Resolve unclaimed points.
	 * 
	 * @param newFaces
	 *          the new faces
	 */
	protected void resolveUnclaimedPoints(FaceList newFaces)
	{
		Vertex vtxNext = this.unclaimed.first();
		for (Vertex vtx = vtxNext; vtx != null; vtx = vtxNext)
		{
			vtxNext = vtx.next;

			double maxDist = this.tolerance;
			Face maxFace = null;
			for (Face newFace = newFaces.first(); newFace != null; newFace = newFace.next)
			{
				if (newFace.mark == Face.VISIBLE)
				{
					double dist = newFace.distanceToPlane(vtx.pnt);
					if (dist > maxDist)
					{
						maxDist = dist;
						maxFace = newFace;
					}
					if (maxDist > 1000 * this.tolerance)
					{
						break;
					}
				}
			}
			if (maxFace != null)
			{
				addPointToFace(vtx, maxFace);
				if (this.debug && vtx.index == this.findIndex)
				{
					System.out.println(this.findIndex + " CLAIMED BY " + maxFace.getVertexString());
				}
			} else
			{
				if (this.debug && vtx.index == this.findIndex)
				{
					System.out.println(this.findIndex + " DISCARDED");
				}
			}
		}
	}

	/**
	 * Sets the debug.
	 * 
	 * @param enable
	 *          the new debug
	 */
	public void setDebug(boolean enable)
	{
		this.debug = enable;
	}

	/**
	 * Sets the explicit distance tolerance.
	 * 
	 * @param tol
	 *          the new explicit distance tolerance
	 */
	public void setExplicitDistanceTolerance(double tol)
	{
		this.explicitTolerance = tol;
	}

	/**
	 * Sets the from qhull.
	 * 
	 * @param coords
	 *          the coords
	 * @param nump
	 *          the nump
	 * @param triangulate
	 *          the triangulate
	 */
	protected void setFromQhull(double[] coords, int nump, boolean triangulate)
	{
		String commandStr = "./qhull i";
		if (triangulate)
		{
			commandStr += " -Qt";
		}
		try
		{
			Process proc = Runtime.getRuntime().exec(commandStr);
			PrintStream ps = new PrintStream(proc.getOutputStream());
			StreamTokenizer stok = new StreamTokenizer(new InputStreamReader(proc.getInputStream()));

			ps.println("3 " + nump);
			for (int i = 0; i < nump; i++)
			{
				ps.println(coords[i * 3 + 0] + " " + coords[i * 3 + 1] + " " + coords[i * 3 + 2]);
			}
			ps.flush();
			ps.close();
			ArrayList<Integer> indexList = new ArrayList<Integer>(3);
			stok.eolIsSignificant(true);
			printQhullErrors(proc);

			do
			{
				stok.nextToken();
			} while (stok.sval == null || !stok.sval.startsWith("MERGEexact"));
			for (int i = 0; i < 4; i++)
			{
				stok.nextToken();
			}
			if (stok.ttype != StreamTokenizer.TT_NUMBER)
			{
				System.out.println("Expecting number of faces");
				System.exit(1);
			}
			int numf = (int) stok.nval;
			stok.nextToken(); // clear EOL
			int[][] faceIndices = new int[numf][];
			for (int i = 0; i < numf; i++)
			{
				indexList.clear();
				while (stok.nextToken() != StreamTokenizer.TT_EOL)
				{
					if (stok.ttype != StreamTokenizer.TT_NUMBER)
					{
						System.out.println("Expecting face index");
						System.exit(1);
					}
					indexList.add(0, new Integer((int) stok.nval));
				}
				faceIndices[i] = new int[indexList.size()];
				int k = 0;
				for (Iterator<Integer> it = indexList.iterator(); it.hasNext();)
				{
					faceIndices[i][k++] = (it.next()).intValue();
				}
			}
			setHull(coords, nump, faceIndices, numf);
		} catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * Sets the hull.
	 * 
	 * @param coords
	 *          the coords
	 * @param nump
	 *          the nump
	 * @param faceIndices
	 *          the face indices
	 * @param numf
	 *          the numf
	 */
	protected void setHull(double[] coords, int nump, int[][] faceIndices, int numf)
	{
		initBuffers(nump);
		setPoints(coords, nump);
		computeMaxAndMin();
		for (int i = 0; i < numf; i++)
		{
			Face face = Face.create(this.pointBuffer, faceIndices[i]);
			HalfEdge he = face.he0;
			do
			{
				HalfEdge heOpp = findHalfEdge(he.head(), he.tail());
				if (heOpp != null)
				{
					he.setOpposite(heOpp);
				}
				he = he.next;
			} while (he != face.he0);
			this.faces.add(face);
		}
	}

	/**
	 * Sets the points.
	 * 
	 * @param pnts
	 *          the pnts
	 * @param nump
	 *          the nump
	 */
	protected void setPoints(ConvexHullPoint3d[] pnts, int nump)
	{
		for (int i = 0; i < nump; i++)
		{
			Vertex vtx = this.pointBuffer[i];
			vtx.pnt.set(pnts[i]);
			vtx.index = i;
		}
	}

	/**
	 * Sets the points.
	 * 
	 * @param coords
	 *          the coords
	 * @param nump
	 *          the nump
	 */
	protected void setPoints(double[] coords, int nump)
	{
		for (int i = 0; i < nump; i++)
		{
			Vertex vtx = this.pointBuffer[i];
			vtx.pnt.set(coords[i * 3 + 0], coords[i * 3 + 1], coords[i * 3 + 2]);
			vtx.index = i;
		}
	}

	/**
	 * Triangulate.
	 */
	public void triangulate()
	{
		double minArea = 1000 * this.charLength * DOUBLE_PREC;
		this.newFaces.clear();
		for (Iterator<Face> it = this.faces.iterator(); it.hasNext();)
		{
			Face face = it.next();
			if (face.mark == Face.VISIBLE)
			{
				face.triangulate(this.newFaces, minArea);
				// splitFace (face);
			}
		}
		for (Face face = this.newFaces.first(); face != null; face = face.next)
		{
			this.faces.add(face);
		}
	}
}
