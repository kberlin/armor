/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import edu.umd.umiacs.armor.math.MathRuntimeException;
import edu.umd.umiacs.armor.math.Rotation;

/**
 * The Class SymmetricMatrix3dImpl.
 */
public class SymmetricMatrix3dImpl extends ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix> implements SymmetricMatrix3d
{
	/** The eig. */
	protected EigenDecomposition3d eig;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2273903952901127525L;
		
	public SymmetricMatrix3dImpl(double[][] A, boolean copy)
	{
		this(new Array2dRealMatrix(A, copy));
	}
	
	public SymmetricMatrix3dImpl(double[][] A)
	{
		this(new Array2dRealMatrix(A, true));
	}
	
	protected SymmetricMatrix3dImpl(SymmetricMatrix3d A)
	{
		super(A.getApacheCommonsMatrix());
		this.eig = null;
	}

	public SymmetricMatrix3dImpl(RealMatrix A)
	{
		super(A.getApacheCommonsMatrix());
		this.eig = null;
		
		if (A==null || A.numColumns() != 3 || A.numRows()!=3)
			throw new MathRuntimeException("Matrix size must be 3x3.");
	}

	public SymmetricMatrix3dImpl(double x, double y, double z)
	{
		this(new EigenDecomposition3d(x, y, z, Rotation.identityRotation()));
	}
	
	public SymmetricMatrix3dImpl(double x, double y, double z, Rotation R)
	{
		this(new EigenDecomposition3d(x, y, z, R));
	}
	
	public SymmetricMatrix3dImpl(EigenDecomposition3d eig)
	{
		this(eig.formMatrix());
		this.eig = eig;
	}
	
	public SymmetricMatrix3dImpl(double[] eigenvalues)
	{
		this(new SymmetricMatrixImpl(eigenvalues));
	}

	@Override
	public synchronized EigenDecomposition3d getEigenDecomposition()
	{
		if (this.eig == null)
		{
			this.eig = new EigenDecomposition3d(this);
		}

		return this.eig;
	}

	@Override
	public int size()
	{
		return numColumns();
	}
}
