/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math;

import java.io.Serializable;

import org.apache.commons.math3.special.Beta;

/**
 * The Class Ftest.
 */
public final class Ftest implements Serializable
{ 
	
	/** The fval. */
	private final double fval;

	/** The P. */
	private final double P;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 635002088322540250L;
	
	public Ftest(int degreeOfFreedom1, double chiSquared1, int degreeOfFreedom2, double chiSquared2)
	{
		double df1;
		double df2;
		double chi1;
		double chi2;

		//The following is exact copy from the old ROTDIF
		//code is not clear
		
		chiSquared1 = chiSquared1/degreeOfFreedom1;
		chiSquared2 = chiSquared2/degreeOfFreedom2;
		
		if (chiSquared1*(double)degreeOfFreedom1>chiSquared2*(double)degreeOfFreedom2)
		{
			df1 = (double)degreeOfFreedom1;
			df2 = (double)degreeOfFreedom2;
			chi1 = chiSquared1;
			chi2 = chiSquared2;			
		}
		else
		{
			df1 = (double)degreeOfFreedom2;
			df2 = (double)degreeOfFreedom1;
			chi1 = chiSquared2;
			chi2 = chiSquared1;			
		}
		
		//this.fval = ((chi1-chi2)/(df1-df2))/(chi2/df2);
		this.fval = (chi1*df1-chi2*df2)/(df1-df2)/(chi2);
				
		//FDistribution Ffunc = new FDistribution(df2,df1-df2);
		//FDistribution Ffunc = new FDistribution(df1-df2, df2);
		//double Pcurr = 2.0*Ffunc.cumulativeProbability(this.fval);
		//if (Pcurr>1.0)
		//	this.P = 2.0-Pcurr;
		//else
		//	this.P = Pcurr;
		
		//DF test why?
		double var1 = (chi1*df1-chi2*df2)/(df1-df2);
		double var2 = chi2;
		double n1 = (df1-df2);
		double n2 = df2;
		double F= var1/var2;
		if (var2>var1)
		{
			double temp = n1;
			n1 = n2;
			n2 = temp;
			F = var2/var1;
		}
		double x= n2/(n2+n1*F);
				
		double P = Beta.regularizedBeta(x,n2/2.0,n1/2.0);
		if (this.fval < 1.0)
			P = 1.0 - P;
		
		this.P = P;
	}
	
	/**
	 * F value.
	 * 
	 * @return the double
	 */
	public double fValue()
	{
		return this.fval;
	}
	
	/**
	 * Gets the probability.
	 * 
	 * @return the probability
	 */
	public double getProbability()
	{
		return this.P;
	}
	
	/**
	 * Checks if is null hypothesis.
	 * 
	 * @param alpha
	 *          the alpha
	 * @return true, if is null hypothesis
	 */
	public boolean isNullHypothesis(double alpha)
	{
		return (this.P<=alpha);
	}
}
