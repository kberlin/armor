/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Scanner;

import edu.umd.umiacs.armor.math.MathRuntimeException;

public final class LinearMathFileIO
{
	public static Array2dRealMatrix read(File file) throws FileNotFoundException
	{
		try
		{
			return readBinary(file);
		}
		catch(Exception e)
		{
			return readText(file);
		}
	}

	public static Array2dRealMatrix readBinary(File file) throws IOException
	{
		FileInputStream fstream = new FileInputStream(file);
		BufferedInputStream in = new BufferedInputStream(fstream);
		DataInputStream dataInput = new DataInputStream(in);
		
		double[][] A = null;
		try
		{
			int numRows = dataInput.readInt();
			int numColumns = dataInput.readInt();
			
			if (numRows<1)
	    	throw new MathRuntimeException("Number of rows cannot be less than one in the file.");
			if (numColumns<1)
	    	throw new MathRuntimeException("Number of columns cannot be less than one in the file.");
			
			//read all the values into an array
			ArrayList<Double> list = new ArrayList<Double>();
			
			//read all the values
			for (int iter=0; iter<numRows*numColumns; iter++)
				list.add(dataInput.readDouble());
			
			if (in.read()!=-1)
	    	throw new MathRuntimeException("EOF not reached after fully reading all matrix values.");							

			//generate the matrix since everything is ok
			A = new double[numRows][];
			
			//store the values into an array
			Iterator<Double> iterator = list.iterator();
			for (int row=0; row<numRows; row++)
			{
				A[row] = new double[numColumns];
				for (int col=0; col<numColumns; col++)
					A[row][col] = iterator.next();
			}
		}
		catch(EOFException e)
		{
    	throw new MathRuntimeException("EOF reached before all values read.",e);							
		}
		finally
		{		
			dataInput.close();
		}
		
		return new Array2dRealMatrix(A, false);
	}
	
	public static Array2dRealMatrix readText(File file) throws FileNotFoundException 
	{
		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
	  BufferedReader br = new BufferedReader(new InputStreamReader(in));
	  
	  Scanner scanner = new Scanner(br);
	  scanner.useLocale(Locale.US);
	  
	  try
	  {
	    //if no data, return null
	    if (!scanner.hasNext())
	    	return null;
	    
	    //read the first line
	    Scanner firstLineScanner = new Scanner(scanner.nextLine());
	    scanner.useLocale(Locale.US);
	    
	    if (!firstLineScanner.hasNextDouble())
	    	return null;
	    
	    //parse first line
	    ArrayList<Double> doubleList = new ArrayList<Double>();
	    while (firstLineScanner.hasNextDouble())
	    	doubleList.add(firstLineScanner.nextDouble());
	    
	    //close the reader
	    firstLineScanner.close();
	    
	    //create the list for the array
	    ArrayList<double[]> doubleArray = new ArrayList<double[]>();
	
	    //now that we know the number of columns copy back the original values
	    double[] currentRow = new double[doubleList.size()];
	    for (int iter=0; iter<doubleList.size(); iter++)
	    	currentRow[iter] = doubleList.get(iter);
	    
	    //add the first row to the list
			doubleArray.add(currentRow);
	    int numberColumns = currentRow.length;
			
			//allocate the memory again
			currentRow = new double[numberColumns];
	    	    
	    int currentColumn = 0;
	    while(scanner.hasNextDouble())
	    {
	    	//read the next value
	    	double nextDouble = scanner.nextDouble();
	    		    		    	
	    	//add the element
	    	currentRow[currentColumn] = nextDouble;
	    	
	    	//increment the columns
	    	currentColumn = (currentColumn+1)%numberColumns;
	    	
	    	//store the current row and allocate memory
	    	if (currentColumn==0)
	    	{
	    		//add the current row
	    		doubleArray.add(currentRow);
	    		
	    		//allocate the memory again
	    		currentRow = new double[numberColumns];
	    	}
	    }
	    
			if (currentColumn!=0)
	    	throw new MathRuntimeException("Incorrect number of columns in the matrix file.");
	
	    //create the actual array
	    double[][] A = new double[doubleArray.size()][];
	    for (int row=0; row<A.length; row++)
	    	A[row] = doubleArray.get(row);
	    
	    return new Array2dRealMatrix(A, false);
	  }
	  finally
	  {      
	    //close the file
	    scanner.close();    	
	  }
	  
	}
	
	public static void write(File file, double[][] A) throws IOException
	{
		write(file, new Array2dRealMatrix(A, false));
	}


	public static void write(File file, RealMatrix A) throws IOException
	{
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
		
		for (int i=0; i<A.numRows(); i++)
		{
			for (int j=0; j<A.numColumns(); j++)
			{
				if (j<A.numColumns()-1)
					out.printf(Locale.US, "%f\t", A.get(i, j));
				else
					out.printf(Locale.US, "%f\n", A.get(i, j));
			}
		}
		
		out.close();
	}

}
