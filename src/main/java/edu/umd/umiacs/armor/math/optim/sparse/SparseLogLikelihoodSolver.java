/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim.sparse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.linear.DiagnalMatrix;
import edu.umd.umiacs.armor.math.linear.LeftSingularValueDecomposition;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.optim.LinearConstraintedLinearLeastSquares;
import edu.umd.umiacs.armor.math.optim.LinearConstraints;
import edu.umd.umiacs.armor.math.optim.LinearSolution;
import edu.umd.umiacs.armor.math.optim.LinearSolutions;
import edu.umd.umiacs.armor.math.optim.OptimizationRuntimeException;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.ProgressObserver;

public class SparseLogLikelihoodSolver implements MultipleSolutionSparseLinearLeastSquaresOptimizer
{
	public enum Preconditioning	
	{
		COMPRESSION,
		NONE,
		PUFFER;
	}
	
	private RealMatrix A;
	private final Preconditioning compress;
	private final ArrayList<ProgressObserver> observers;
	private final MultipleSolutionSparseLinearLeastSquaresOptimizer optimizer;
	private final double relSvdCutoff;
	
	public static LinearSystem compressLinearSystem(RealMatrix A, double[] y, double relTol)
	{
		//perform svd
		//SingularValueDecomposition svdDecomp = A.svd();
		LeftSingularValueDecomposition svdDecomp = new LeftSingularValueDecomposition(A);
		
		//get the new rank
		double[] s = svdDecomp.getSingularValues();
		int newRank = 1;
		while(newRank<s.length)
		{
			if (s[newRank]/s[0]<relTol)
				break;
			newRank++;
		}
		
		RealMatrix ut = svdDecomp.getU().transpose();
		
		//transpose y
		y = ut.mult(y);
		y = Arrays.copyOf(y, newRank);
		
		//update A, such that only the relavant rows remain
		A = (ut.mult(A)).getSubMatrix(0, newRank-1, 0, A.numColumns()-1);
		
		LinearSystem ls = new LinearSystem(A,y);
		
		return ls;
	}
	
	public static LinearSystem pufferLinearSystem(RealMatrix A, double[] y, double relTol)
	{
		//perform svd
		//SingularValueDecomposition svdDecomp = A.svd();
		LeftSingularValueDecomposition svdDecomp = new LeftSingularValueDecomposition(A);
		
		//get the new rank
		double[] s = svdDecomp.getSingularValues();
		int newRank = 1;
		while(newRank<s.length)
		{
			if (s[newRank]/s[0]<relTol)
				break;
			newRank++;
		}
		
		RealMatrix ut = svdDecomp.getU().transpose();
		
		//transpose y
		y = ut.mult(y);
		y = Arrays.copyOf(y, newRank);
		
		//update A, such that only the relavant rows remain
		A = (ut.mult(A)).getSubMatrix(0, newRank-1, 0, A.numColumns()-1);
		
		//epsilon puffer cutoff
		double eps = 1.0e-1;
		
		//invert the diagnal
		double[] sinv = new double[newRank];
		for (int iter=0; iter<newRank; iter++)
		{
			if (s[iter]/s[0]>eps)
				sinv[iter] = eps*s[0]/s[iter];
			else
				sinv[iter] = 1.0;
		}
		DiagnalMatrix D = new DiagnalMatrix(sinv);
		
		//create the updated system
		LinearSystem ls = new LinearSystem(D.mult(A), D.mult(y));
		
		return ls;
	}

	public SparseLogLikelihoodSolver(MultipleSolutionSparseLinearLeastSquaresOptimizer optimizer)
	{
		this(optimizer, Preconditioning.NONE);
	}

	public SparseLogLikelihoodSolver(MultipleSolutionSparseLinearLeastSquaresOptimizer optimizer, Preconditioning type)
	{
		this(optimizer, type, optimizer.getRelativeErrorTol()*1.0e-1);
	}

	public SparseLogLikelihoodSolver(MultipleSolutionSparseLinearLeastSquaresOptimizer optimizer, Preconditioning type, double relSvdTol)
	{
		this.optimizer = optimizer;
		this.compress = type;
		this.relSvdCutoff = relSvdTol;
		this.observers = new ArrayList<ProgressObserver>();
		
		this.optimizer.setA(null);
	}
	
	@Override
	public void addProgressObserver(ProgressObserver o)
	{
		this.optimizer.addProgressObserver(o);
		this.observers.add(o);
	}
	
	@Override
	public double getRelativeErrorTol()
	{
		return this.optimizer.getRelativeErrorTol();
	}

	@Override
	public boolean isNonNegativeOnly()
	{
		return this.optimizer.isNonNegativeOnly();
	}
	
	@Override
	public double maxSum()
	{
		return this.optimizer.maxSum();
	}

	@Override
	public int maxThreads()
	{
		return this.optimizer.maxThreads();
	}

	private Collection<SparseLinearSolution> removeDiscontinuousRegions(Collection<SparseLinearSolution> solutions)
	{
		int maxL0 = 0;
		for (SparseLinearSolution solution : solutions)
			maxL0 = Math.max(maxL0, solution.l0Norm());
		
		boolean[] exists = new boolean[maxL0+1];
		exists[0] = true;
		
		for (SparseLinearSolution solution : solutions)
			exists[solution.l0Norm()] = true;

		//find maximum soluion
		maxL0 = 0;
		for (boolean exist : exists)
		{
			if (!exist)
				break;
			
			maxL0++;
		}
		
		//must remove tail end of the region
		if (maxL0<exists.length-1)
		{
			ArrayList<SparseLinearSolution> updatedSolutions = new ArrayList<SparseLinearSolution>(solutions.size());
			for (SparseLinearSolution solution : solutions)
				if (solution.l0Norm()<=maxL0)
					updatedSolutions.add(solution);
			
			solutions = updatedSolutions;
		}
		
		return solutions;
	}
	
	@Override
	public void removeProgressObserver(ProgressObserver o)
	{
		this.optimizer.removeProgressObserver(o);
		this.observers.remove(o);
	}

	@Override
	public void setA(RealMatrix A)
	{
		this.A = A;
	}

	@Override
	public void setRelativeErrorTol(double errTol)
	{
		this.optimizer.setRelativeErrorTol(errTol);
	}
	
	@Override
	public double[] solve(double[] y)
	{
		return solveAlt(y).getBest().getX();
	}
	
	public double[] solve(double[] y, int maxL0)
	{
		return solveAlt(y, null, maxL0).getBest().getX();
	}
	
	@Override
	public LinearSolutions solveAlt(double[] y)
	{
		return solveAlt(y, null, Integer.MAX_VALUE);
	}
	
	public LinearSolutions solveAlt(double[] y, double[] errors, int maxL0)
	{
		if (this.A == null)
			throw new OptimizationRuntimeException("The matrix A was never set.");
		
		if (this.A.numRows()!=y.length)
			throw new OptimizationRuntimeException("Number of rows in A ("+this.A.numRows()+") does not match length of y ("+y.length+").");

		RealMatrix matrixCurr = this.A;
		RealMatrix matrixErrorScaled = this.A;
		double[] yCurr = y.clone();
		double[] yErrorScaled = y;
		
		boolean computeError = false;
		if (errors!=null)
		{
			for (double val : errors)
				if (Math.abs(val-1.0)>1.0e-12)
				{
					computeError = true;
					break;
				}
		}
		
		if (computeError)
		{
			//change to max likelyhood
			BlockRealMatrix predictionMatrix = new BlockRealMatrix(this.A.numRows(), this.A.numColumns());
			for (int row=0; row<this.A.numRows(); row++)
			{
				double error = errors[row];
				
				if (Double.isInfinite(error) || error==0.0)
					throw new OptimizationRuntimeException("Invalid value for error: "+error);
				
				if (Double.isNaN(error))
					error = 1.0;
				
				yCurr[row] = yCurr[row]/error;
	
				for (int col=0; col<this.A.numColumns(); col++)
					predictionMatrix.set(row, col, this.A.get(row, col)/error);
			}
			
			//assign the matrix back
			matrixErrorScaled = predictionMatrix;
			matrixCurr = predictionMatrix;			
			yErrorScaled = yCurr.clone();
			yCurr = yErrorScaled;
		}
				
		//send update of status
		updateProgress(0);
		
		switch (this.compress)
		{		
			case COMPRESSION :
			{
				LinearSystem ls = compressLinearSystem(matrixCurr, y, this.relSvdCutoff);
				matrixCurr = ls.A;
				yCurr = ls.y;
				break;
			}
			case PUFFER:
			{
				LinearSystem ls = pufferLinearSystem(matrixCurr, y, this.relSvdCutoff);
				matrixCurr = ls.A;
				yCurr = ls.y;
				break;
			}
			default:
				break;
		}

		//setup the matrix
		this.optimizer.setA(matrixCurr);
				
		//compute the solution
		LinearSolutions solutions = this.optimizer.solveAlt(yCurr, maxL0);
		
		//if compressed the linear system, recompute the error relative to original
		final List<SparseLinearSolution> newSolutions = Collections.synchronizedList(new ArrayList<SparseLinearSolution>(solutions.size()));
		if (this.compress!=Preconditioning.NONE)
		{
			final boolean isNonNegOnly = this.optimizer.isNonNegativeOnly();
			final RealMatrix matrixErrorScaledThread = matrixErrorScaled;
			final double[] yErrorScaledThread = yErrorScaled;
			final LinearSolutions originalSolutions = solutions;
			
			//figure out number of threads
	  	int cores = Runtime.getRuntime().availableProcessors();
	  	final int numberOfThreads = Math.min(maxThreads(), Math.min(cores*2, solutions.size()));

	  	//create the threading service
			ExecutorService execSvc = Executors.newFixedThreadPool(numberOfThreads);
			
			for (int thread=0; thread<numberOfThreads; thread++)
			{
				//set your thread number
				final int myThread = thread;
				
				Runnable oneThreadWork = new Runnable()
				{
					@Override
					public void run()
					{
						Iterator<LinearSolution> iterator = originalSolutions.iterator();
						
						//get your starting point equal to your thread number
						for (int iter=0; iter<myThread; iter++)
							if (iterator.hasNext())
								iterator.next();
							else
								return;
						
						while(iterator.hasNext())
						{
							LinearSolution sol = iterator.next();
							
							int[] colList = sol.getActiveColumns();
							
							//get the submatrix
							RealMatrix subA = matrixErrorScaledThread.getColumns(colList);
							
							LinearConstraintedLinearLeastSquares solver = new LinearConstraintedLinearLeastSquares();
							solver.setA(subA);
							
							//compute the QR decomposition and solve
							double[] x = solver.solve(yErrorScaledThread, sol.getActiveWeights());
							
							//make sure the result is ok
							if (BasicMath.hasNaN(x))
								throw new OptimizationRuntimeException("Sparse weights cannot be NaN.");

							//make sure does not violate the constraints, but only constrain using maxsum constraint
							if (!Double.isInfinite(maxSum()) && (BasicMath.sum(x)>maxSum() || (isNonNegOnly && !BasicMath.isNonNegative(x))))
							{
								solver.setLinearEqualityConstraint(LinearConstraints.createMaxSum(x.length, maxSum()));
								x = solver.solve(yErrorScaledThread, BasicUtils.createArray(x.length, maxSum()/x.length));

								//make sure the result is ok
								if (BasicMath.hasNaN(x))
									throw new OptimizationRuntimeException("Sparse weights cannot be NaN.");
							}
								
							//make sure does not violate negative constraint, otherwise drop solution
							if (!isNonNegOnly || BasicMath.isNonNegative(x))
							{
								double err = BasicStat.chi(yErrorScaledThread, subA.mult(x));
								SparseLinearSolution updatedSol = new SparseLinearSolution(err, x, colList, matrixErrorScaledThread.numColumns());
								
								//add the solution the list, the list in synchronized
								newSolutions.add(updatedSol);
							}
							
							//if does not have more elements quit
							for (int iter=0; iter<numberOfThreads-1; iter++)
								if (iterator.hasNext())
									iterator.next();
								else
									return;
						}
					}
				};
				
				execSvc.execute(oneThreadWork);
			}
			
		  //shutdown the service
		  execSvc.shutdown();
		  try
			{
				execSvc.awaitTermination(365L, TimeUnit.DAYS);
			} 
		  catch (InterruptedException e) 
		  {
		  	execSvc.shutdownNow();
		  	throw new OptimizationRuntimeException("Unable to finish all tasks.", e);
		  }
			
			//remove regions
			solutions = new LinearSolutions(removeDiscontinuousRegions(newSolutions));
		}
		
		return solutions;
	}

	@Override
	public LinearSolutions solveAlt(double[] y, int maxL0)
	{
		return solveAlt(y, null, maxL0);
	}

	private synchronized void updateProgress(int l0)
	{
		for (ProgressObserver o : this.observers)
			o.update((int)l0);
	}
}
