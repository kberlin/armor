/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.util.BasicUtils;

public abstract class AbstractIterativeLeastSquaresOptimizer<F extends MultivariateVectorFunction> extends AbstractIterativeOptimizer<F> implements IterativeLeastSquaresOptimizer<F> 
{
	private double[] observations = null;
	private double[] weights = null;

	/**
	 * 
	 */
	private static final long serialVersionUID = 709329420357209937L;

	@Override
	public double[] getErrors()
	{
		double[] errors = getWeights();
		
		for (int iter=0; iter<errors.length; iter++)
			errors[iter] = BasicMath.sqrt(1.0/errors[iter]);
		
		return errors;
	}

	@Override
	public double[] getObservations()
	{
		if (this.observations==null)
			throw new OptimizationRuntimeException("Observations not set.");
		
		return this.observations;
	}
	
	@Override
	public double[] getWeights()
	{
		if (this.weights==null)
			return BasicUtils.createArray(getObservations().length, 1.0);
		
		return this.weights.clone();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.IterativeLeastSquaresOptimizer#setErrors(double[])
	 */
	@Override
	public void setErrors(double[] errors)
	{
		this.weights = new double[errors.length];
		for (int iter=0; iter<errors.length; iter++)
			this.weights[iter] = 1.0/(errors[iter]*errors[iter]);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.IterativeLeastSquaresOptimizer#setTarget(double[])
	 */
	@Override
	public void setObservations(double[] observations)
	{
		this.observations = observations.clone();
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.IterativeLeastSquaresOptimizer#setWeights(double[])
	 */
	@Override
	public void setWeights(double[] weights)
	{
		this.weights = weights.clone();
	}
}
