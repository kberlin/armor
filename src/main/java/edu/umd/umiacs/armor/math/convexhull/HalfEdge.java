/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.convexhull;

/*
 * Copyright John E. Lloyd, 2003. All rights reserved. Permission
 * to use, copy, and modify, without fee, is granted for non-commercial 
 * and research purposes, provided that this copyright notice appears 
 * in all copies.
 *
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 */

/**
 * The Class HalfEdge.
 */
class HalfEdge
{
	
	/** The face. */
	Face face;

	/** The next. */
	HalfEdge next;

	/** The opposite. */
	HalfEdge opposite;

	/** The prev. */
	HalfEdge prev;

	/** The vertex. */
	Vertex vertex;

	/**
	 * Instantiates a new half edge.
	 */
	public HalfEdge()
	{
	}

	/**
	 * Instantiates a new half edge.
	 * 
	 * @param v
	 *          the v
	 * @param f
	 *          the f
	 */
	public HalfEdge(Vertex v, Face f)
	{
		this.vertex = v;
		this.face = f;
	}

	/**
	 * Gets the face.
	 * 
	 * @return the face
	 */
	public Face getFace()
	{
		return this.face;
	}

	/**
	 * Gets the next.
	 * 
	 * @return the next
	 */
	public HalfEdge getNext()
	{
		return this.next;
	}

	/**
	 * Gets the opposite.
	 * 
	 * @return the opposite
	 */
	public HalfEdge getOpposite()
	{
		return this.opposite;
	}

	/**
	 * Gets the prev.
	 * 
	 * @return the prev
	 */
	public HalfEdge getPrev()
	{
		return this.prev;
	}

	/**
	 * Gets the vertex string.
	 * 
	 * @return the vertex string
	 */
	public String getVertexString()
	{
		if (tail() != null)
		{
			return "" + tail().index + "-" + head().index;
		} else
		{
			return "?-" + head().index;
		}
	}

	/**
	 * Head.
	 * 
	 * @return the vertex
	 */
	public Vertex head()
	{
		return this.vertex;
	}

	/**
	 * Length.
	 * 
	 * @return the double
	 */
	public double length()
	{
		if (tail() != null)
		{
			return head().pnt.distance(tail().pnt);
		} else
		{
			return -1;
		}
	}

	/**
	 * Length squared.
	 * 
	 * @return the double
	 */
	public double lengthSquared()
	{
		if (tail() != null)
		{
			return head().pnt.distanceSquared(tail().pnt);
		} else
		{
			return -1;
		}
	}

	/**
	 * Opposite face.
	 * 
	 * @return the face
	 */
	public Face oppositeFace()
	{
		return this.opposite != null ? this.opposite.face : null;
	}

	/**
	 * Sets the next.
	 * 
	 * @param edge
	 *          the new next
	 */
	public void setNext(HalfEdge edge)
	{
		this.next = edge;
	}

	/**
	 * Sets the opposite.
	 * 
	 * @param edge
	 *          the new opposite
	 */
	public void setOpposite(HalfEdge edge)
	{
		this.opposite = edge;
		edge.opposite = this;
	}

	/**
	 * Sets the prev.
	 * 
	 * @param edge
	 *          the new prev
	 */
	public void setPrev(HalfEdge edge)
	{
		this.prev = edge;
	}

	/**
	 * Tail.
	 * 
	 * @return the vertex
	 */
	public Vertex tail()
	{
		return this.prev != null ? this.prev.vertex : null;
	}

	// /**
	// * Computes nrml . (del0 X del1), where del0 and del1
	// * are the direction vectors along this halfEdge, and the
	// * halfEdge he1.
	// *
	// * A product > 0 indicates a left turn WRT the normal
	// */
	// public double turnProduct (HalfEdge he1, Vector3d nrml)
	// {
	// Point3d pnt0 = tail().pnt;
	// Point3d pnt1 = head().pnt;
	// Point3d pnt2 = he1.head().pnt;

	// double del0x = pnt1.x - pnt0.x;
	// double del0y = pnt1.y - pnt0.y;
	// double del0z = pnt1.z - pnt0.z;

	// double del1x = pnt2.x - pnt1.x;
	// double del1y = pnt2.y - pnt1.y;
	// double del1z = pnt2.z - pnt1.z;

	// return (nrml.x*(del0y*del1z - del0z*del1y) +
	// nrml.y*(del0z*del1x - del0x*del1z) +
	// nrml.z*(del0x*del1y - del0y*del1x));
	// }
}
