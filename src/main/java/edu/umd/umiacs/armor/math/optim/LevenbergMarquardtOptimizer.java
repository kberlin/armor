/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import org.apache.commons.math3.analysis.MultivariateMatrixFunction;
import org.apache.commons.math3.exception.TooManyEvaluationsException;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.SimpleVectorValueChecker;
import org.apache.commons.math3.optim.PointVectorValuePair;
import org.apache.commons.math3.optim.nonlinear.vector.Target;
import org.apache.commons.math3.optim.nonlinear.vector.Weight;

import edu.umd.umiacs.armor.math.func.DifferentiableMultivariateVectorFunction;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.util.BasicUtils;

@SuppressWarnings("deprecation")
public final class LevenbergMarquardtOptimizer extends AbstractIterativeLeastSquaresOptimizer<DifferentiableMultivariateVectorFunction> 
{
	private MultivariateVectorFunction nonDiffFunc;
	private final static double FINITE_DIFFERENCE_DELTA = 1.0e-2;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4620924381908601732L;
	
	public LevenbergMarquardtOptimizer()
	{
		this.nonDiffFunc = null;
	}

	private org.apache.commons.math3.optim.nonlinear.vector.ModelFunction getCommonsFunction()
	{
		final DifferentiableMultivariateVectorFunction func = getFunction();
						
		org.apache.commons.math3.analysis.MultivariateVectorFunction returnFunc = new org.apache.commons.math3.analysis.MultivariateVectorFunction()
		{		
				@Override
			public double[] value(double[] x)
			{
				return func.value(x);
			}
		};
		
		return new org.apache.commons.math3.optim.nonlinear.vector.ModelFunction(returnFunc);
	}
	
	private org.apache.commons.math3.optim.nonlinear.vector.ModelFunctionJacobian getCommonsJacobianFunction()
	{
		final DifferentiableMultivariateVectorFunction func = getFunction();
		
		final MultivariateMatrixFunction jacobianFunc = new MultivariateMatrixFunction()
		{
			@Override
			public double[][] value(double[] x) throws IllegalArgumentException
			{
				return func.jacobian(x).toArray();
			}
		};
		
		return new org.apache.commons.math3.optim.nonlinear.vector.ModelFunctionJacobian(jacobianFunc);
	}
	
	@Override
	public LeastSquaresResultImpl optimize(double[] x0)
	{
		org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer optimizer = new org.apache.commons.math3.optim.nonlinear.vector.jacobian.LevenbergMarquardtOptimizer(
				new SimpleVectorValueChecker(-1.0, getAbsPointDifference(), getNumberEvaluations()));
		
		try
		{
			PointVectorValuePair val = optimizer.optimize(
					getCommonsFunction(), getCommonsJacobianFunction(), new Target(getObservations()), new Weight(getWeights()), new InitialGuess(x0), new MaxEval(getNumberEvaluations()*3));
			
			return new LeastSquaresResultImpl(val.getPoint(), optimizer.getChiSquare(), optimizer.getEvaluations(), val.getValue());
		}
		catch (TooManyEvaluationsException e)
		{
			throw new OptimizationRuntimeException(e.getMessage(), e);
		}
		catch (org.apache.commons.math3.exception.ConvergenceException e)
		{
			throw new OptimizationRuntimeException("Internal optimization exception for initial value: "+BasicUtils.toString(x0), e);			
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.AbstractIterativeOptimizer#setAbsPointDifference(double)
	 */
	@Override
	public void setAbsPointDifference(double absPointDifference)
	{
		super.setAbsPointDifference(absPointDifference);
		if (this.nonDiffFunc != null)
			super.setFunction(new FiniteDifferenceDMVFWrapper(this.nonDiffFunc, getAbsPointDifference()*FINITE_DIFFERENCE_DELTA));			
	}

	public void setFiniteDifferenceFunction(MultivariateVectorFunction function)
	{
		this.nonDiffFunc = function;
		super.setFunction(new FiniteDifferenceDMVFWrapper(this.nonDiffFunc, getAbsPointDifference()*FINITE_DIFFERENCE_DELTA));
	}

	@Override
	public void setFunction(DifferentiableMultivariateVectorFunction function)
	{
		super.setFunction(function);
		this.nonDiffFunc = null;
	}
}
