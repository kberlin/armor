/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import org.apache.commons.math3.exception.DimensionMismatchException;

import edu.umd.umiacs.armor.math.func.MultivariateFunction;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;

public final class LeastSquaresConverter implements MultivariateFunction
{
	private final MultivariateVectorFunction function;
	private final double[] observations;
	private final double[] weights;
	private final boolean display;
	private int iter;

	public LeastSquaresConverter(MultivariateVectorFunction f, double[] observations, double[] weights)
	{
		this.function = f;
		this.observations = observations.clone();
		this.weights = weights.clone();
		this.display = false;
		this.iter = 0;
	}
	
	public LeastSquaresConverter(MultivariateVectorFunction f, double[] observations, double[] weights, boolean display)
	{
		this.function = f;
		this.observations = observations.clone();
		this.weights = weights.clone();
		this.display = display;
		this.iter = 0;
	}

	public double chi2(double[] point)
	{
		return value(point);
	}
	
	@Override
	public double value(double[] point)
	{
		//adopted from apache math commons
		// compute residuals
		final double[] residuals = this.function.value(point);
		if (residuals.length != this.observations.length)
		{
			throw new DimensionMismatchException(residuals.length, this.observations.length);
		}
		for (int i = 0; i < residuals.length; ++i)
		{
			residuals[i] -= this.observations[i];
		}

		// compute sum of squares
		double sumSquares = 0;
		if (this.weights != null)
		{
			for (int i = 0; i < residuals.length; ++i)
			{
				final double ri = residuals[i];
				sumSquares += this.weights[i]*ri*ri;
			}

		}
		else
		{
			for (final double ri : residuals)
			{
				sumSquares += ri*ri;
			}
		}
		
		if (this.display)
			System.out.format("Iteration %d: value = %f\n", this.iter++, sumSquares);
		
		return sumSquares;
	}

}
