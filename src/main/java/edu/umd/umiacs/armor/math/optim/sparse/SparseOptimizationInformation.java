package edu.umd.umiacs.armor.math.optim.sparse;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.linear.QRDecomposition;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.util.BasicUtils;

public final class SparseOptimizationInformation
{
	private double[] Anorms;
	private double[][] AtNormalized;
	private BlockRealMatrix AtNormalizedScaledMatrix;
	private double columnsNorm;
	private double[][] currSumNullSpace;
	private int currSumNullSpaceSize;
	private final double maxSum;
	private final boolean positiveOnly;
	private double[][] currSumNullSpaceT;

	protected SparseOptimizationInformation(double maxSum, boolean positiveOnly)
	{
		this.maxSum = maxSum;
		this.positiveOnly = positiveOnly;
		this.AtNormalized = null;
		this.AtNormalizedScaledMatrix = null;
		this.Anorms = null;
		
		this.currSumNullSpace = new double[1][1];
		this.currSumNullSpaceT = new double[1][1];
		this.currSumNullSpaceSize = 0;
	}
	
	public final double[] computeNormalizedEstimate(final double[] y)
	{
		final double[] z = this.AtNormalizedScaledMatrix.mult(y);		
		
		return z;
	}
	
	public double getColumnsNorm()
	{
		return this.columnsNorm;
	}
	
	/**
	 * @return the maxSum
	 */
	public final double getMaxSum()
	{
		return this.maxSum;
	}
	
	public final double[] getNormalizedColumn(int index)
	{
		return this.AtNormalized[index];
	}
	
	public final double[][] getNormalizedMatrix()
	{
		return this.AtNormalized;
	}
	
	public final double getNormalizedMaxSum()
	{
		return this.maxSum*this.columnsNorm;
	}
	
	public final double[][] getNormalizedSubAt(final int[] columns)
	{
		double[][] At = new double[columns.length][];
		
		for (int iter=0; iter<columns.length; iter++)
			At[iter] = this.getNormalizedColumn(columns[iter]);
		
		return At;
	}
	
	private final void cacheNullSpace(final int size)
	{
		synchronized (this.currSumNullSpace)
		{
			//compute the null space if already not stored
			if (this.currSumNullSpaceSize!=size)
			{
				double[][] At = new double[size][1];
				for (double[] col : At)
					col[0] = 1.0;
				
				QRDecomposition qr = new QRDecomposition(new Array2dRealMatrix(At, false));
				int rank = qr.getRank(1.0e-12);
				
				RealMatrix Q = qr.getQ();
				
				RealMatrix nullSpace = null;
				if (rank<Q.numRows())
					nullSpace = Q.getSubMatrix(0, Q.numRows()-1, rank, Q.numColumns()-1);
				
				this.currSumNullSpaceSize = size;
				this.currSumNullSpace = nullSpace.toArray();
				this.currSumNullSpaceT = nullSpace.transpose().toArray();
			}
		}
	}

	public final double[][] getSumNullSpace(final int size)
	{
		cacheNullSpace(size);
		
		return this.currSumNullSpace;
	}
	
	public final double[][] getSumNullSpaceTransposed(final int size)
	{
		cacheNullSpace(size);
		
		return this.currSumNullSpaceT;
	}
	
	/**
	 * @return the positiveOnly
	 */
	public final boolean isPositiveOnly()
	{
		return this.positiveOnly;
	}

	public final boolean isSumConstrained()
	{
		return !Double.isInfinite(this.maxSum) && !Double.isNaN(this.maxSum);
	}
	
	public boolean matrixIsNull()
	{
		return this.AtNormalized==null;
	}

	public final int numColumns()
	{
		return this.AtNormalized.length;
	}
	
	public final int numRows()
	{
		return this.AtNormalized[0].length;
	}

	public void setA(final RealMatrix A)
	{
		if (A==null)
		{
			this.AtNormalized = null;
			this.AtNormalizedScaledMatrix = null;
			this.Anorms = null;
			return;
		}			
		
		//store A in transpose mode in order to be able to access columns quickly
		this.AtNormalized = A.transpose().toArray();
		
		this.Anorms = new double[this.AtNormalized.length];
		for (int col=0; col<this.AtNormalized.length; col++)
		{
			this.Anorms[col] = BasicMath.norm(this.AtNormalized[col]);
		}
		
		this.columnsNorm = BasicMath.max(this.Anorms);

	  //normalize A
		for (int col=0; col<this.AtNormalized.length; col++)
		{
			this.Anorms[col] = this.Anorms[col]/this.columnsNorm;
			for (int row=0; row<this.AtNormalized[col].length; row++)
				this.AtNormalized[col][row] = this.AtNormalized[col][row]/this.columnsNorm;
		}
		
		double[][] AtNormalizedScaled = BasicUtils.copyArray(this.AtNormalized);
		for (int col=0; col<AtNormalizedScaled.length; col++)
		{
			double norm = BasicMath.norm(AtNormalizedScaled[col]);
			double norm2 = norm*norm;
			
			for (int row=0; row<AtNormalizedScaled[col].length; row++)
				AtNormalizedScaled[col][row] = AtNormalizedScaled[col][row]/(norm2);
		}
		
		this.AtNormalizedScaledMatrix = new BlockRealMatrix(AtNormalizedScaled); 
	}

}
