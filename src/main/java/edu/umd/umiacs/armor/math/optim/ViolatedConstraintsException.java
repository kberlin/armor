package edu.umd.umiacs.armor.math.optim;

public class ViolatedConstraintsException extends OptimizationRuntimeException
{

	private final double[] x;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7205247028816243325L;

	public ViolatedConstraintsException(double[] x)
	{
		super();
		this.x = x;
	}
	
	public ViolatedConstraintsException(String arg0, double[] x)
	{
		super(arg0);
		this.x = x;
	}
	
	public double[] getX()
	{
		return this.x;
	}
}
