/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.umd.umiacs.armor.math.MathRuntimeException;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.stat.BasicStat;

public class OptimizationResultImpl implements OptimizationResult
{
	private final int numEvals;

	private final double[] point;
	
	protected final double value;

	/**
	 * 
	 */
	private static final long serialVersionUID = 4000336686409127785L;

	public static ArrayList<Rotation> filterRotations(List<OptimizationResult> results, List<Rotation> rotations, double angleRMSD)
	{
		if (results.size()!=rotations.size())
			throw new OptimizationRuntimeException("Lists must match size.");
			  
	  //add the best solution
	  ArrayList<OptimizationResult> filteredResults = new ArrayList<OptimizationResult>();
	  ArrayList<Rotation> filteredRotations = new ArrayList<Rotation>();
	  
	  Iterator<OptimizationResult> resultsIter = results.iterator();
	  Iterator<Rotation> rotationsIter = rotations.iterator();
	  while(resultsIter.hasNext())
	  {
	  	OptimizationResult result = resultsIter.next();
	  	Rotation rotation = rotationsIter.next();
	  	
			boolean exists = false;
			int count = 0;
	  	for (Rotation current : filteredRotations)
	  	{
	  		if (current.distance(rotation) < angleRMSD)
	  		{
	  			exists = true;
	  			break;
	  		}
	  		count++;
	  	}
	  	
	  	if (exists)
	  	{
	  		//if the solution is actually better than the closest found solution
	  		if (result.getValue()<filteredResults.get(count).getValue())
	  		{
	  			filteredResults.set(count, result);
	  			filteredRotations.set(count, rotation);
	  		}
	  	}
	  	else
	  	{
				filteredResults.add(result);
				filteredRotations.add(rotation);
	  	}
	  }
	  
	  return filteredRotations;
	}
	
	public static <T extends OptimizationResult> ArrayList<T> filterSolutions(List<T> results, double relativeRMS, double pointRMSD)
	{
	  //get the best solution
	  double bestVal = Double.POSITIVE_INFINITY;
	  for (T result : results)
	  {
	  	if (result.getValue()>1.0e-10 && bestVal>result.getValue())
	  		bestVal = result.getValue();
	  }

	  //add the best solution
	  ArrayList<T> filteredResults = new ArrayList<T>();
	  for (T result : results)
	  {
  		double[] currSol = result.getPoint();

			boolean exists = false;
			int count = 0;
	  	for (T current : filteredResults)
	  	{
	  		if (BasicStat.rmsd(currSol, current.getPoint()) < pointRMSD)
	  		{
	  			exists = true;
	  			break;
	  		}
	  		count++;
	  	}
	  	
	  	if (exists)
	  	{
	  		//if the solution is actually better than the closest found solution
	  		if (filteredResults.get(count).getValue()>result.getValue())
	  		{
	  			filteredResults.set(count, result);
	  		}
	  	}
	  	else
	  	if (result.getValue()<bestVal*(1.0+relativeRMS))
	  	{
				filteredResults.add(result);
	  	}
	  }
	  
	  return filteredResults;
	}
	
	public static <T extends OptimizationResult> T getBestSolution(ArrayList<T> sol)
	{
		if (sol.size()<1)
			throw new MathRuntimeException("No solutions exist.");
		
	  //get the best solution
	  double bestVal = Double.POSITIVE_INFINITY;
	  T bestSolution = null;
	  for (T result : sol)
	  {
	  	if (result.getValue()<bestVal)
	  	{
		  	bestVal = Math.min(bestVal,result.getValue());
		  	bestSolution = result;
	  	}
	  }

	  return bestSolution;
	}
	
	public OptimizationResultImpl(double[] point, double value, int numEvals)
	{
		this.point = point.clone();
		this.value = value;
		this.numEvals = numEvals;
	}
	
	@Override
	public int compareTo(OptimizationResult o)
	{
		return Double.compare(getValue(), o.getValue());
	}
	
	public int getNumEvals()
	{
		return this.numEvals;
	}
	
	@Override
	public double[] getPoint()
	{
		return this.point.clone();
	}

	@Override
	public double getValue()
	{
		return this.value;
	}
	
}
