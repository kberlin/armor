/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import edu.umd.umiacs.armor.math.MathRuntimeException;

/**
 * The Class SymmetricMatrixImpl.
 */
public final class SymmetricMatrixImpl extends ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix> implements SymmetricMatrix
{
	/** The eig. */
	protected EigenDecomposition eig;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6993885011518373740L;
	
	public static SymmetricMatrixImpl createFromArray(double[][] A)
	{		
		SymmetricMatrixImpl m = new SymmetricMatrixImpl(new org.apache.commons.math3.linear.BlockRealMatrix(A));
		
		if (A.length != A[0].length)
			throw new MathRuntimeException("Generating matrix must by a square.");

		return m;
	}
	
	public static SymmetricMatrixImpl createFromMatrix(RealMatrix A)
	{
		SymmetricMatrixImpl m = new SymmetricMatrixImpl(A.getApacheCommonsMatrix().copy());
		
		if (A.numColumns() != A.numRows())
			throw new MathRuntimeException("Generating matrix must by a square.");

		return m;
	}
	
	public SymmetricMatrixImpl(double[] diag)
	{
		this(new org.apache.commons.math3.linear.DiagonalMatrix(diag));		
	}
		
	public SymmetricMatrixImpl(EigenDecomposition eig)
	{
		this(eig.formMatrix().getApacheCommonsMatrix());
		this.eig = eig;
	}

	protected SymmetricMatrixImpl(org.apache.commons.math3.linear.RealMatrix A)
	{
		super(A);
		this.eig = null;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.SymmetricMatrix#getEigenDecomposition()
	 */
	@Override
	public synchronized EigenDecomposition getEigenDecomposition()
	{
		if (this.eig==null)
			this.eig = new EigenDecompositionImpl(this);
		
		return this.eig;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.SymmetricMatrix#size()
	 */
	@Override
	public int size()
	{
		return numColumns();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.SymmetricMatrix#trace()
	 */
	@Override
	public double trace()
	{
		return this.A.getTrace();
	}
}
