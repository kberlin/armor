/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math;

import org.apache.commons.math3.geometry.euclidean.threed.CardanEulerSingularityException;
import org.apache.commons.math3.geometry.euclidean.threed.RotationOrder;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import edu.umd.umiacs.armor.math.linear.OrthonormalMatrixImpl;
import edu.umd.umiacs.armor.math.linear.RealMatrix;

/**
 * The Class Rotation.
 */
public final class Rotation extends OrthonormalMatrixImpl
{
	/** The rotation. */
	private final org.apache.commons.math3.geometry.euclidean.threed.Rotation rotation;
	
	/** The Constant identity. */
  private static final Rotation identity = new Rotation(new org.apache.commons.math3.geometry.euclidean.threed.Rotation(1, 0, 0, 0, false));
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6182394970688639097L;

	private static Rotation create(RotationOrder order, double alpha, double beta, double gamma)
	{
		org.apache.commons.math3.geometry.euclidean.threed.Rotation newRotation = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(
				order, alpha, beta, gamma);

		return new Rotation(newRotation);
	}
	
	public static Rotation createRandom()
	{
		org.apache.commons.math3.geometry.euclidean.threed.Rotation rotation = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(Math.random()-.5, Math.random()-.5, Math.random()-.5, Math.random()-.5, true);
		return new Rotation(rotation);
	}

	public static Rotation createZYZ(double alpha, double beta, double gamma)
	{
		return create(RotationOrder.ZYZ, alpha, beta, gamma);
	}

	public static Rotation identityRotation()
	{
		return identity;
	}

	public Rotation(double[][] R)
	{
		super(new org.apache.commons.math3.linear.Array2DRowRealMatrix(R, true));
		this.rotation = new org.apache.commons.math3.geometry.euclidean.threed.Rotation(R, 1.0e-10);
	}

	protected Rotation(org.apache.commons.math3.geometry.euclidean.threed.Rotation rotation)
	{
		super(new org.apache.commons.math3.linear.Array2DRowRealMatrix(rotation.getMatrix(), true));
		this.rotation = rotation;
	}

	public Rotation(RealMatrix R)
	{
		this(R.toArray());
	}
	
	public double distance(Rotation R)
	{
		return org.apache.commons.math3.geometry.euclidean.threed.Rotation.distance(this.rotation, R.rotation);
	}
	
	public double[] getAnglesZYZ()
	{
		double[] angles;
		
		try 
		{
			angles = this.rotation.getAngles(RotationOrder.ZYZ);
		}
		catch (CardanEulerSingularityException e)
		{
			angles = new double[]{0.0,0.0,0.0};
		}
		
		return angles;
	}

	public Rotation inverse()
	{
		return new Rotation(this.rotation.revert());
	}
	
	public boolean isIdentity()
	{
		return this.rotation.getAngle() < 1.0e-8;
	}
	
	public Point3d mult(Point3d p)
	{
		Vector3D newU = this.rotation.applyTo(new Vector3D(p.x, p.y, p.z));

		return new Point3d(newU.getX(), newU.getY(), newU.getZ());
	}
	
	public Rotation mult(Rotation R)
	{
		return new Rotation(this.rotation.applyTo(R.rotation));
	}
	
	public Point3d multInv(Point3d p)
	{
		Vector3D newU = this.rotation.applyInverseTo(new Vector3D(p.x, p.y, p.z));

		return new Point3d(newU.getX(), newU.getY(), newU.getZ());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.linear.ApacheCommonsRealMatrixWrapper#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		
		double[][] M = this.rotation.getMatrix();
		for (int row=0; row<M.length; row++)
		{
			for (int col=0; col<M[row].length; col++)
				str.append(String.format("%.4f\t", M[row][col]));
			if (row<M.length-1)
				str.append("\n");
		}
		
		return str.toString();
	}

}
