/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

public abstract class AbstractIterativeOptimizer<F> extends AbstractOptimizer<F> implements IterativeOptimizer<F>
{
	private double absPointDifference = DEFAULT_ABS_POINT_DIFFERENCE;
	
	private int numberEvaluations = DEFAULT_NUMBER_EVALUATIONS;
	public static final double DEFAULT_ABS_POINT_DIFFERENCE = 1.0e-8;
	
	public static final int DEFAULT_NUMBER_EVALUATIONS = 2000;
	/**
	 * 
	 */
	private static final long serialVersionUID = 8124220637931404935L;

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.IterativeOptimizer#getAbsPointDifference()
	 */
	@Override
	public double getAbsPointDifference()
	{
		return this.absPointDifference;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.IterativeOptimizer#getNumberEvaluations()
	 */
	@Override
	public int getNumberEvaluations()
	{
		return this.numberEvaluations;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.IterativeOptimizer#setAbsPointDifference()
	 */
	@Override
	public void setAbsPointDifference(double absPointDifference)
	{
		this.absPointDifference = absPointDifference;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.IterativeOptimizer#setNumberEvaluations()
	 */
	@Override
	public void setNumberEvaluations(int numberEvaluations)
	{
		this.numberEvaluations = numberEvaluations; 
	}
}
