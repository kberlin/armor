/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.integration;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.TwoVariableFunction;
import edu.umd.umiacs.armor.math.func.UnivariateFunction;

/**
 * The Class GuassLegendreIntegration2D.
 */
public class GuassLegendreIntegration2D implements UnivariateIntegrator, DoubleIntegrator
{	
	
	/* Computing of abscissas and weights for Gauss-Legendre quadrature for any(reasonable) order n
		[in] n   - order of quadrature
		[in] eps - required precision (must be eps>=macheps(double), usually eps = 1e-10 is ok)
		[out]x   - abscisass, size = (n+1)>>1
		[out]w   - weights, size = (n+1)>>1 
	*/
	/**
	 * Gauss legendre table.
	 * 
	 * @param n
	 *          the n
	 * @param eps
	 *          the eps
	 * @return the double[][]
	 */
	private static double[][] gaussLegendreTable(int n, double eps)
	{
		double x0,  x1,  dx;	/* Abscissas */
		double w0=0.0,  w1,  dw;	/* Weights */
		double P0, P_1, P_2;	/* Legendre polynomial values */
		double dpdx;			/* Legendre polynomial derivative */
		int i, j, k, m;			/* Iterators */
		double t0, t1, t2, t3;
		double[] x;
		double[] w;
		double[][] returnVal;
	
		m = (n+1)>>1;
				
		x = new double[m];
		w = new double[m];
		returnVal = new double[2][];
		returnVal[0] = x;
		returnVal[1] = w;
		
		t0 = (1.0-(1.0-1.0/n)/(8.0*n*n));
		t1 = 1.0/(4.0*n+2.0);
	
		for (i = 1; i <= m; i++)
		{
			/* Find i-th root of Legendre polynomial */
	
			/* Initial guess */
			x0 = BasicMath.cos(BasicMath.PI*((i<<2)-1)*t1)*t0;
	
			/* Newton iterations, at least one */
			j = 0;
			dx = dw = Double.MAX_VALUE;
			do 
			{
				/* Compute Legendre polynomial value at x0 */
				P_1 = 1.0;
				P0  = x0;

				/* Simple, not optimized version */
				for (k = 2; k <= n; k++)
				{
					P_2 = P_1;
					P_1 = P0;
					t2 = x0*P_1;
					t3 = (double)(k-1)/(double)k;
	
					P0 = t2 + t3*(t2 - P_2);
				}
				
				/* Compute Legendre polynomial derivative at x0 */
				dpdx = ((x0*P0-P_1)*n)/(x0*x0-1.0);
	
				/* Newton step */
				x1 = x0-P0/dpdx;
	
				/* Weight computing */
				w1 = 2.0/((1.0-x1*x1)*dpdx*dpdx);
	
				/* Compute weight w0 on first iteration, needed for dw */
				if (j==0) w0 = 2.0/((1.0-x0*x0)*dpdx*dpdx);
	
				dx = x0-x1;
				dw = w0-w1;
	
				x0 = x1;
				w0 = w1;
				j++;
				
			} while((Math.abs(dx)>eps || Math.abs(dw)>eps) && j<200);
			
			x[(m-1)-(i-1)] = x1;
			w[(m-1)-(i-1)] = w1;
		}
	
		return returnVal;
	}
	
	/* 
	Gauss-Legendre n-points quadrature, exact for polynomial of degree <=2n-1
	
	1. n - even:
	
	int(f(t),t=a..b) = A*sum(w[i]*f(A*x[i]+B),i=0..n-1) 
			 = A*sum(w[k]*[f(B+A*x[k])+f(B-A*x[k])],k=0..n/2-1)
		A = (b-a)/2, 
		B = (a+b)/2
	
		
	2. n - odd:
	
	int(f(t),t=a..b) = A*sum(w[i]*f(A*x[i]+B),i=0..n-1) 
			 = A*w[0]*f(B)+A*sum(w[k]*[f(B+A*x[k])+f(B-A*x[k])],k=1..(n-1)/2)
		A = (b-a)/2, 
		B = (a+b)/2
	*/
	
	/**
	 * Instantiates a new guass legendre integration2 d.
	 */
	public GuassLegendreIntegration2D()
	{
	}
	
	
	/* 2D Numerical computation of int(f(x,y),x=a..b,y=c..d) by Gauss-Legendre n-th order high precision quadrature 
			[in]n     - quadrature order
			[in]f     - integrand
			[in]data  - pointer on user-defined data which will 
						be passed to f every time it called (as third parameter).
			[in][a,b] - interval of integration
	
		return:
				-computed integral value or -1.0 if n order quadrature is not supported
	
		1. n - even:
	
		int(f(t,p),t=a..b,p=c..d) = C*A*sum(w[i]*w[j]*f(A*x[i]+B,C*y[j]+D),i=0..n-1,j=0..n-1) 
				 = C*A*sum(w[k]*w[l]*[f(B+A*x[k],C*y[l]+D)+f(B-A*x[k],C*y[l]+D)],k=0..n/2-1,l=0..n-1)
				 = C*A*sum(w[k]*w[l]*[f(B+A*x[k],C*y[l]+D)+f(B+A*x[k],D-C*y[l])+
									 +f(B-A*x[k],C*y[l]+D)+f(B-A*x[k],D-C*y[l])],k=0..n/2-1,l=0..n/2-1)
			A = (b-a)/2 
			B = (a+b)/2
			C = (d-c)/2
			D = (d+c)/2
	
			
		2. n - odd:
	
		int(f(t,p),t=a..b,p=c..d) = C*A*sum(w[i]*w[j]*f(A*x[i]+B,C*y[j]+D),i=0..n-1,j=0..n-1) 
				 = C*A*[w[0]*w[0]*f(B,D)+sum(w[0]*w[j]*(f(B,C*y[j]+D)+f(B,D-C*y[j])),j=1..(n-1)/2)+
				                        +sum(w[i]*w[0]*(f(B+A*x[i],D)+f(B-A*x[i],D)),i=1..(n-1)/2)+
										+sum(w[k]*w[l]*[f(B+A*x[k],C*y[l]+D)+f(B+A*x[k],D-C*y[l])+
											  +f(B-A*x[k],C*y[l]+D)+f(B-A*x[k],D-C*y[l])],k=1..(n-1)/2,l=1..(n-1)/2)]
			
			A = (b-a)/2, 
			B = (a+b)/2
			C = (d-c)/2
			D = (d+c)/2
	
	*/
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.integration.DoubleIntegrator#integrate(edu.umd.umiacs.armor.math.func.TwoVariableFunction, double, double, double, double, int)
	 */
	@Override
	public double integrate(TwoVariableFunction fun, double a, double b, double c, double d, int n)
	{
		double[] x = null;
		double[] w = null;
		double A,B,C,D,Ax,Cy,s,t;
		int i,j, m;
	
		m = (n+1)>>1;
	
		/* Generate new if non-predefined table is required */
		/* with precision of 1e-10 */
		if(x==null)
		{
			double[][] xwArray = gaussLegendreTable(n,1e-14);
			x = xwArray[0];
			w = xwArray[1];
		}
	
	
		A = 0.5*(b-a);
		B = 0.5*(b+a);
		C = 0.5*(d-c);
		D = 0.5*(d+c);
	
		if((n&1)==1) /* n - odd */
		{
	
			s = w[0]*w[0]*fun.value(B,D);
			
			for (j=1,t=0.0;j<m;j++)
			{
				Cy = C*x[j];
				t += w[j]*(fun.value(B,D+Cy)+fun.value(B,D-Cy));
			}
			s += w[0]*t;
	
			for (i=1,t=0.0;i<m;i++)
			{
				Ax = A*x[i];
				t += w[i]*(fun.value(B+Ax,D)+fun.value(B-Ax,D));
			}
			s += w[0]*t;
	
			for (i=1;i<m;i++)
			{
				Ax = A*x[i];
				for (j=1;j<m;j++)
				{
					Cy = C*x[j];
					s += w[i]*w[j]*( fun.value(B+Ax,D+Cy)+fun.value(Ax+B,D-Cy)+fun.value(B-Ax,D+Cy)+fun.value(B-Ax,D-Cy));
				}
			}
	
		}
		else
		{ /* n - even */
	
			s = 0.0;
			for (i=0;i<m;i++)
			{
				Ax = A*x[i];
				for (j=0;j<m;j++)
				{
					Cy = C*x[j];
					s += w[i]*w[j]*( fun.value(B+Ax,D+Cy)+fun.value(Ax+B,D-Cy)+fun.value(B-Ax,D+Cy)+fun.value(B-Ax,D-Cy));
				}
			}
		}
	
		return C*A*s;
	}
	
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.integration.UnivariateIntegrator#integrate(edu.umd.umiacs.armor.math.func.UnivariateFunction, double, double, int)
	 */
	@Override
	public double integrate(UnivariateFunction fun, double a, double b, int maxEvals)
	{
		double[] x = null;
		double[] w = null;
		double A,B,Ax,s;
		int i, m;
		
		int n = maxEvals;
	
		m = (n+1)>>1;
			
		/* Generate new if non-predefined table is required */
		/* with precision of 1e-10 */
		if(x==null)
		{
			double[][] xwArray = gaussLegendreTable(n,1e-14);
			x = xwArray[0];
			w = xwArray[1];
		}
	
	
		A = 0.5*(b-a);
		B = 0.5*(b+a);
	
		if((n&1)==1) /* n - odd */
		{
			s = w[0]*(fun.value(B));
			for (i=1;i<m;i++)
			{
				Ax = A*x[i];
				s += w[i]*(fun.value(B+Ax)+fun.value(B-Ax));
			}
	
		}
		else
		{ /* n - even */
			
			s = 0.0;
			for (i=0;i<m;i++)
			{
				Ax = A*x[i];
				s += w[i]*(fun.value(B+Ax)+fun.value(B-Ax));			
			}
		}
	
		return A*s;
	}
}
