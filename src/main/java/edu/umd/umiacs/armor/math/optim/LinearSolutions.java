/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import edu.umd.umiacs.armor.util.Indicies;


public class LinearSolutions implements Iterable<LinearSolution>, Serializable
{
	private final ArrayList<LinearSolution> solutions;
	/**
	 * 
	 */
	private static final long serialVersionUID = -8272858065951467324L;
	
	public LinearSolutions(Collection<? extends LinearSolution> solutions)
	{
		this.solutions = new ArrayList<LinearSolution>(solutions);
		Collections.sort(this.solutions);
	}
	
	public LinearSolution get(int l0, int index)
	{
		return getSolutions(l0).get(index);
	}
	
	public LinearSolution getBest()
	{
		return getBest(maxL0());
	}
	
	public LinearSolution getBest(int l0)
	{
		List<LinearSolution> bestSolutions = getSolutions(l0);
		
		if (bestSolutions.isEmpty())
			return null;
		
		return bestSolutions.get(0);
	}
	
	public List<LinearSolution> getSolutions(int l0)
	{
		ArrayList<LinearSolution> solutions = new ArrayList<LinearSolution>();
		for (LinearSolution solution : this.solutions)
		{
			if (solution.l0Norm()==l0)
			{
				solutions.add(solution);
			}
		}
		
		return solutions;
	}
	
	public boolean isEmpty()
	{
		return size()==0;
	}
	
	@Override
	public Iterator<LinearSolution> iterator()
	{
		return this.solutions.iterator();
	}

	public int maxL0()
	{
		int maxValue = 0;
		for (LinearSolution sol : this.solutions)
		{
			maxValue = Math.max(maxValue, sol.l0Norm());
		}
		
		return maxValue;
	}
	
	public LinearSolutions merge(LinearSolutions sol)
	{
		if (sol.size()==0)
			return this;
		if (this.size()==0)
			return sol;
		
		if (this.solutions.get(0).dimension()!=sol.solutions.get(0).dimension())
			throw new OptimizationRuntimeException("Merged solutions must have the same dimension.");
		
		int maxL0 = Math.max(this.maxL0(), sol.maxL0());
		
		ArrayList<LinearSolution> combinedSolutions = new ArrayList<LinearSolution>(this.size());
		for (int l0=1; l0<=maxL0; l0++)
		{
			List<LinearSolution> currSolution = new ArrayList<LinearSolution>();
			
			//allocate list
			List<LinearSolution> currList1 = getSolutions(l0);
			List<LinearSolution> currList2 = sol.getSolutions(l0);
			
			//create a hashmap
			HashMap<Indicies, LinearSolution> solutionMap = new HashMap<Indicies, LinearSolution>();
			
			//hash all the solution
			for (LinearSolution currSol : currList1)
			{
				Indicies indicies = new Indicies(currSol.getActiveColumns(), false);
				LinearSolution hashedSol = solutionMap.get(indicies);
				
				//add only if better
				if (hashedSol==null || hashedSol.absoluteError()>currSol.absoluteError())
					solutionMap.put(indicies, currSol);
			}
			for (LinearSolution currSol : currList2)
			{
				Indicies indicies = new Indicies(currSol.getActiveColumns(), false);
				LinearSolution hashedSol = solutionMap.get(indicies);
				
				//add only if better
				if (hashedSol==null || hashedSol.absoluteError()>currSol.absoluteError())
					solutionMap.put(indicies, currSol);
			}
			
			//add the values in hash list to all the solution
			currSolution.addAll(solutionMap.values());
			
			//sort
			Collections.sort(currSolution);
			
			//cut the list
			currSolution = currSolution.subList(0, Math.min(currSolution.size(), Math.max(currList1.size(), currList2.size())));
			
			//add to the combined list
			combinedSolutions.addAll(currSolution);
		}
		
		return new LinearSolutions(combinedSolutions);
	}
	
	public void save(File textFile, int maxSize, double yNorm) throws IOException
	{
		PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(textFile)));
		
		out.print("%%<size><name><chi2><columns><weights>\n");
		out.println(String.format(Locale.US,"0 null %f",yNorm*yNorm));
		for (int l0=1; l0<=maxL0(); l0++)
		{
			List<LinearSolution> bestSolutions = getSolutions(l0);
			
			int counter = 0;
			for (LinearSolution sol : bestSolutions)
			{
				out.print(sol.toFileString());
				out.print("\n");
				
				//increase counter
				counter++;
				
				//make sure not to many elements are outputed
				if (counter>=maxSize)
					break;
			}
		}
		
		out.close();
	}

	public int size()
	{
		return this.solutions.size();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder s = new StringBuilder();
		
		s.append("Best Solutions:\n");
		
		int maxL0 = maxL0();
		for (int iter=1; iter<=maxL0; iter++)
		{
			s.append(""+getBest(iter)+"\n");
		}
		
		return s.toString();
	}

}
