/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import java.util.ArrayList;
import java.util.HashSet;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.QRDecomposition;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.util.BasicUtils;

public class LinearConstraints
{
	private final double[][] A;
	private final double[] b;
	
	public static LinearConstraints createEmpty()
	{
		return new LinearConstraints(null, null);
	}
	
	public static LinearConstraints createLowerBound(double[] lowerBound)
	{
		double[][] A = new double[lowerBound.length][lowerBound.length];
		double[] b = new double[lowerBound.length];
		for (int iter=0; iter<lowerBound.length; iter++)
		{
			A[iter][iter] = -1.0;
			b[iter] = -lowerBound[iter];
		}
		
		return new LinearConstraints(A, b);
	}
	
	public static LinearConstraints createMaxSum(int size, double value)
	{
		double[][] A = new double[1][size];
		for (int iter=0; iter<size; iter++)
			A[0][iter] = 1.0;
		
		double[] b = new double[]{value};
		
		return new LinearConstraints(A, b);
	}

	public static LinearConstraints createMinSum(int size, double value)
	{
		double[][] A = new double[1][size];
		for (int iter=0; iter<size; iter++)
			A[0][iter] = -1.0;
		
		double[] b = new double[]{-value};
		
		return new LinearConstraints(A, b);
	}
	
	public static LinearConstraints createUpperBound(double[] upperBound)
	{
		double[][] A = new double[upperBound.length][upperBound.length];
		double[] b = new double[upperBound.length];
		for (int iter=0; iter<upperBound.length; iter++)
		{
			A[iter][iter] = 1.0;
			b[iter] = upperBound[iter];
		}
		
		return new LinearConstraints(A, b);
	}
	
	public LinearConstraints()
	{
		this(null, null);
	}

	public LinearConstraints(double[][] A, double[] b)
	{
		if (A!=null)
		{
			for (double[] row : A)
				if (BasicUtils.hasNaN(row) || BasicUtils.hasInfinite(row))
					throw new OptimizationRuntimeException("A matrix cannot have NaN or Infinite values.");
			
			if (A.length>0)
				this.A = A;
			else
				this.A = null;
		}
		else
			this.A = null;

		if (b!=null)
		{
			if (BasicUtils.hasNaN(b) || BasicUtils.hasInfinite(b))
				throw new OptimizationRuntimeException("Vector b cannot have NaN or Infinite values.");

			if (b.length>0)
				this.b = b;
			else
				this.b = null;
		}
		else
			this.b = null;
	}
	
	public ArrayList<Integer> activeConstraints(double[] g, double eps)
	{
		ArrayList<Integer> active = new ArrayList<Integer>();
		
		//check for empty
		if (this.A == null)
			return active;
		
		//compute lagrange multipliers
		double[] lambda = computeLagrangeMultiplier(g);
		
		for (int index=0; index<this.b.length; index++)
		{
			if (lambda[index]<eps)
				active.add(index);
		}
		
		return active;
	}
	
	public ArrayList<Integer> additionalViolatedConstraints(double[] x, ArrayList<Integer> selectedIndicies, double eps)
	{
		ArrayList<Integer> active = new ArrayList<Integer>();

		//check for empty
		if (isEmpty())
			return active;
		
		HashSet<Integer> indexSet = new HashSet<Integer>(selectedIndicies);
		for (int row=0; row<this.A.length; row++)
		{
			if (!indexSet.contains(row) && BasicMath.dotProduct(this.A[row],x)>this.b[row]+eps)
				active.add(row);
		}
		
		return active;
	}
	
	private double[] computeLagrangeMultiplier(double[] g)
	{
		RealMatrix M = new Array2dRealMatrix(this.A, false).transpose();
		
		QRDecomposition qr = new QRDecomposition(M);
		double[] lambda = qr.solve(g);
		
		return lambda;
	}
	
	public double[] getConstraint(int number)
	{
		return this.A[number].clone();
	}
	
	public double getConstraintBound(int number)
	{
		return this.b[number];
	}
	
	public double[] getFeasiblePoint()
	{
		if (isEmpty())
			return null;
		
		RealMatrix M = new Array2dRealMatrix(this.A, false);
		double[] x = new QRDecomposition(M).solve(this.b);
		
		if (!isFeasible(x))
			throw new OptimizationRuntimeException("No feasible solution possible.");
		if (BasicUtils.hasNaN(x) || BasicUtils.hasInfinite(x))
			throw new OptimizationRuntimeException("Feasible point is infinite or NaN.");
		
		return x;
	}
	
	public RealMatrix getNullSpace()
	{
		if (isEmpty())
			return null;
		
		QRDecomposition qr = new QRDecomposition(new Array2dRealMatrix(this.A, false).transpose());
		int rank = qr.getRank(1.0e-10);
		
		RealMatrix Q = qr.getQ();
		
		RealMatrix nullSpace = null;
		if (rank<Q.numColumns())
			nullSpace = Q.getSubMatrix(0, Q.numRows()-1, rank, Q.numColumns()-1);

		return nullSpace;
	}
	
	public ArrayList<Integer> inactiveConstraints(double[] g, double eps)
	{
		ArrayList<Integer> inactive = new ArrayList<Integer>();
		
		//check for empty
		if (this.A == null)
			return inactive;
		
		//compute lagrange multipliers
		double[] lambda = computeLagrangeMultiplier(g);
		
		for (int index=0; index<this.b.length; index++)
		{
			if (lambda[index]>eps)
				inactive.add(index);
		}
		
		return inactive;
	}
	
	public boolean isEmpty()
	{
		if (this.A==null)
			return true;
		
		return false;
	}
	
	public boolean isFeasible(double[] x)
	{
		if (isEmpty())
			return true;
		
		double error = BasicMath.norm(BasicMath.subtract(BasicMath.mult(this.A, x), this.b));
		if (error>1.0e-10)
			return false;
		
		return true;
	}
	
	public boolean isLessEqualsThan(double[] x)
	{
		if (isEmpty())
			return true;
		
		double[] residuals = BasicMath.subtract(BasicMath.mult(this.A, x), this.b);
		for (double val : residuals)
			if (val>1.0e-10)
				return false;
		
		return true;
	}
	
	public LinearConstraints merge(LinearConstraints b)
	{
		if (b.isEmpty())
			return this;
		
		int mySize = size();
		
		double[][] Anew = new double[mySize+b.size()][];
		double[] bnew = new double[mySize+b.size()];
		
		for (int iter=0; iter<mySize; iter++)
		{
			Anew[iter] = this.A[iter];
			bnew[iter] = this.b[iter];
		}
		
		for (int iter=0; iter<b.size(); iter++)
		{
			Anew[iter+mySize] = b.A[iter];
			bnew[iter+mySize] = b.b[iter];
		}
		
		return new LinearConstraints(Anew, bnew);			
	}
	
	public LinearConstraints merge(LinearConstraints b, ArrayList<Integer> indicesB)
	{
		if (b.isEmpty() || indicesB.isEmpty())
			return this;
		
		int mySize = size();
		
		double[][] Anew = new double[mySize+indicesB.size()][];
		double[] bnew = new double[mySize+indicesB.size()];
		
		for (int iter=0; iter<mySize; iter++)
		{
			Anew[iter] = this.A[iter];
			bnew[iter] = this.b[iter];
		}
		for (int iter=0; iter<indicesB.size(); iter++)
		{
			Anew[iter+mySize] = b.A[indicesB.get(iter)];
			bnew[iter+mySize] = b.b[indicesB.get(iter)];
		}
		
		return new LinearConstraints(Anew, bnew);			
	}

	public int size()
	{
		if (isEmpty())
			return 0;			

		return this.b.length;
	}

	public ArrayList<Integer> violatedInequalityConstraints(double[] x, double eps)
	{
		ArrayList<Integer> active = new ArrayList<Integer>();

		//check for empty
		if (isEmpty())
			return active;
		
		for (int row=0; row<this.A.length; row++)
		{
			if (BasicMath.dotProduct(this.A[row],x)>this.b[row]+eps)
				active.add(row);
		}
		
		return active;	
	}
	
	public ArrayList<Integer> violatedEqualityConstraints(double[] x, double eps)
	{
		ArrayList<Integer> active = new ArrayList<Integer>();

		//check for empty
		if (isEmpty())
			return active;
		
		for (int row=0; row<this.A.length; row++)
		{
			if (Math.abs(BasicMath.dotProduct(this.A[row],x)-this.b[row])<eps)
				active.add(row);
		}
		
		return active;	
	}
}