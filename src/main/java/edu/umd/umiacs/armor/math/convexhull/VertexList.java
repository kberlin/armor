/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.convexhull;

/*
 * Copyright John E. Lloyd, 2003. All rights reserved. Permission
 * to use, copy, and modify, without fee, is granted for non-commercial 
 * and research purposes, provided that this copyright notice appears 
 * in all copies.
 *
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 */

/**
 * The Class VertexList.
 */
class VertexList
{
	
	/** The head. */
	private Vertex head;
	
	/** The tail. */
	private Vertex tail;

	/**
	 * Adds the.
	 * 
	 * @param vtx
	 *          the vtx
	 */
	public void add(Vertex vtx)
	{
		if (this.head == null)
		{
			this.head = vtx;
		} else
		{
			this.tail.next = vtx;
		}
		vtx.prev = this.tail;
		vtx.next = null;
		this.tail = vtx;
	}

	/**
	 * Adds the all.
	 * 
	 * @param vtx
	 *          the vtx
	 */
	public void addAll(Vertex vtx)
	{
		if (this.head == null)
		{
			this.head = vtx;
		} else
		{
			this.tail.next = vtx;
		}
		vtx.prev = this.tail;
		while (vtx.next != null)
		{
			vtx = vtx.next;
		}
		this.tail = vtx;
	}

	/**
	 * Clear.
	 */
	public void clear()
	{
		this.head = this.tail = null;
	}

	/**
	 * Delete.
	 * 
	 * @param vtx
	 *          the vtx
	 */
	public void delete(Vertex vtx)
	{
		if (vtx.prev == null)
		{
			this.head = vtx.next;
		} else
		{
			vtx.prev.next = vtx.next;
		}
		if (vtx.next == null)
		{
			this.tail = vtx.prev;
		} else
		{
			vtx.next.prev = vtx.prev;
		}
	}

	/**
	 * Delete.
	 * 
	 * @param vtx1
	 *          the vtx1
	 * @param vtx2
	 *          the vtx2
	 */
	public void delete(Vertex vtx1, Vertex vtx2)
	{
		if (vtx1.prev == null)
		{
			this.head = vtx2.next;
		} else
		{
			vtx1.prev.next = vtx2.next;
		}
		if (vtx2.next == null)
		{
			this.tail = vtx1.prev;
		} else
		{
			vtx2.next.prev = vtx1.prev;
		}
	}

	/**
	 * First.
	 * 
	 * @return the vertex
	 */
	public Vertex first()
	{
		return this.head;
	}

	/**
	 * Insert before.
	 * 
	 * @param vtx
	 *          the vtx
	 * @param next
	 *          the next
	 */
	public void insertBefore(Vertex vtx, Vertex next)
	{
		vtx.prev = next.prev;
		if (next.prev == null)
		{
			this.head = vtx;
		} else
		{
			next.prev.next = vtx;
		}
		vtx.next = next;
		next.prev = vtx;
	}

	/**
	 * Checks if is empty.
	 * 
	 * @return true, if is empty
	 */
	public boolean isEmpty()
	{
		return this.head == null;
	}
}
