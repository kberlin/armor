/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.interpolation;

import org.apache.commons.math3.analysis.interpolation.SplineInterpolator;
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction;
import org.apache.commons.math3.exception.OutOfRangeException;

import edu.umd.umiacs.armor.math.MathRuntimeException;

/**
 * The Class CubicSpline.
 */
public final class CubicSpline
{
	
	/** The poly function. */
	PolynomialSplineFunction polyFunction;

  /**
	 * Instantiates a new cubic spline.
	 * 
	 * @param x
	 *          the x
	 * @param y
	 *          the y
	 * @throws MathRuntimeException
	 *           the math exception
	 */
  public CubicSpline(double x[], double y[]) throws MathRuntimeException
  {
  	this.polyFunction = new SplineInterpolator().interpolate(x, y);
  }
  
  /**
	 * Value.
	 * 
	 * @param x
	 *          the x
	 * @return the double
	 */
  public double value(double x)
  {
  	try
  	{
  		return this.polyFunction.value(x);
  	}
  	catch (OutOfRangeException e)
  	{
  		throw new MathRuntimeException(e.getMessage(), e);
  	}
  }
}