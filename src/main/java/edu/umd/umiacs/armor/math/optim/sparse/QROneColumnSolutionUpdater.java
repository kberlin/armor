/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim.sparse;

import java.util.Arrays;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.QRDecomposition;
import edu.umd.umiacs.armor.math.linear.QRDecompositionOverdetermined;

public final class QROneColumnSolutionUpdater extends OneColumnSolutionUpdater 
{
	private final QRDecompositionOverdetermined qr;
	
	protected QROneColumnSolutionUpdater(double[] y, SparseOptimizationInformation properties)
	{
  	super(y, properties);
		
		this.qr = null;
	}
	
	protected QROneColumnSolutionUpdater(int[] activeColumns, double[] x, double[] residual, SparseOptimizationInformation properties, QRDecompositionOverdetermined qr)
	{
		super(activeColumns, x, residual, properties);
		this.qr = qr;
	}

	@Override
	public QROneColumnSolutionUpdater oneColumnUpdate(int column, double[] yNormed, double yNorm, double relError)
	{
		//merge old columns with the new
		int[] updatedColumnIndicies;
		if (this.activeColumns==null)
			updatedColumnIndicies = new int[1];
		else
			updatedColumnIndicies = Arrays.copyOf(this.activeColumns, this.activeColumns.length+1);
		updatedColumnIndicies[updatedColumnIndicies.length-1] = column;
		
		//store the current column value
		double[] columnValue = this.properties.getNormalizedColumn(column);
		
		//solve for x
		QRDecompositionOverdetermined qrUpdated = null;
		if (this.qr==null)
			//note that the input is a row vector of a transposed matrix, which is silently transposed in the constructor
			qrUpdated = new QRDecompositionOverdetermined(new QRDecomposition(new Array2dRealMatrix(columnValue)));
		else
		{
			//update the QR decomposition
			qrUpdated = this.qr.columnUpdate(columnValue); 
		}
		
		//solve using the QR decomposition
		double[] xUpdated = qrUpdated.solve(new Array2dRealMatrix(yNormed)).getColumn(0);
		double[] yPred = qrUpdated.mult(new Array2dRealMatrix(xUpdated)).getColumn(0);

		//compute the residual
	  double[] rUpdated = BasicMath.subtract(yNormed,yPred);
	  
		QROneColumnSolutionUpdater newSolution = null;
		if ((!this.properties.isSumConstrained() || isXSumOk(xUpdated, yNorm, relError))
				&& (!this.properties.isPositiveOnly() || BasicMath.min(xUpdated)>0.0))
		{
		  newSolution = new QROneColumnSolutionUpdater(updatedColumnIndicies, xUpdated, rUpdated, this.properties, qrUpdated);
		}
		else 
		if (!this.properties.isSumConstrained())
		{
		  newSolution = null;
		}
		else
		if (updatedColumnIndicies.length==1)
		{
			//nothing to do since only one column
			if (xUpdated[0]<=0.0)
				return null;
			
			//update the residual
			xUpdated[0] = this.getNormalizedMaxSum(yNorm);			
			yPred = BasicMath.multTranspose(this.properties.getNormalizedSubAt(updatedColumnIndicies), xUpdated);
		  rUpdated = BasicMath.subtract(yNormed,yPred);

			return new QROneColumnSolutionUpdater(updatedColumnIndicies, xUpdated, rUpdated, this.properties, qrUpdated);
		}
		else
		{
			//this code will use the active set method to apply the max sum constraint
			
			//get the matrix and the null space for the max sum
			double[][] AtSubMatrix = this.properties.getNormalizedSubAt(updatedColumnIndicies);
			double[][] constraintNullSpace = this.properties.getSumNullSpace(updatedColumnIndicies.length);
			
			//get feasible point
			double[] x0 = Arrays.copyOf(this.x, this.activeColumns.length+1);
			x0[x0.length-1] = this.getNormalizedMaxSum(yNorm)-BasicMath.sum(this.x);

			//compute the null space problem setup
			double[][] Am = BasicMath.multTranspose(AtSubMatrix, constraintNullSpace);
			double[] ym = BasicMath.subtract(yNormed, BasicMath.multTranspose(AtSubMatrix, x0));
			
			//solve the null space problem
			double[] z = new QRDecomposition(new Array2dRealMatrix(Am, false)).solve(ym);

			//move the solution back into the original space
		  xUpdated = BasicMath.add(x0, BasicMath.mult(constraintNullSpace, z));

			//update solution
			yPred = BasicMath.multTranspose(AtSubMatrix, xUpdated);
		  rUpdated = BasicMath.subtract(yNormed,yPred);

		  //store solution
			if (!this.properties.isPositiveOnly() || BasicMath.min(xUpdated)>0.0)
				newSolution = new QROneColumnSolutionUpdater(updatedColumnIndicies, xUpdated, rUpdated, this.properties, qrUpdated);
			else				
				newSolution = null;
		}
		  						
		return newSolution;
	}
}