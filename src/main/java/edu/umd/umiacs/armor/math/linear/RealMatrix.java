/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import java.io.Serializable;

public interface RealMatrix extends Serializable
{
	public RealMatrix add(RealMatrix B);

	public RealMatrix concatMatrixColumns(RealMatrix B);

	public double conditionEstimate();

	public double[] diag();
	
	public double get(int i, int j);

	public org.apache.commons.math3.linear.RealMatrix getApacheCommonsMatrix();
	
	public double[] getColumn(int index);

	public RealMatrix getColumns(int[] selectedColumns);
	
	public double[] getRow(int index);

	public RealMatrix getSubMatrix(int startRow, int endRow, int startColumn, int endColumn);

	public RealMatrix getSubMatrix(int[] selectedRows, int[] selectedColumns);

	public RealMatrix mult(double b);

	public double[] mult(double[] b);

	public double[] multTranspose(double[] b);

	public RealMatrix mult(RealMatrix B);

	public double mutualCoherence();

	public double normFrobenius();

	public int numColumns();

	public int numRows();

	public RealMatrix pseudoInverse();

	public RealMatrix pseudoInverse(double eps);
	
	public RealMatrix pseudoInverseRelative(double relEps);

	public RealMatrix subtract(RealMatrix B);
	
	public SingularValueDecomposition svd();
	
	public double[][] toArray();
	
	public String toMatlab();
	
	String toPlainText();

	public double trace();

	public RealMatrix transpose();

}