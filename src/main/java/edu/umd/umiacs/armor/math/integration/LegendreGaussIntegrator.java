/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.integration;

import edu.umd.umiacs.armor.math.func.UnivariateFunction;

/**
 * The Class LegendreGaussIntegrator.
 */
public class LegendreGaussIntegrator
{
	
	/** The integrator. */
	org.apache.commons.math3.analysis.integration.IterativeLegendreGaussIntegrator integrator;
	
	/**
	 * Instantiates a new legendre gauss integrator.
	 * 
	 * @param n
	 *          the n
	 * @param relativeAccuracy
	 *          the relative accuracy
	 * @param absoluteAccuracy
	 *          the absolute accuracy
	 */
	public LegendreGaussIntegrator(int n, double relativeAccuracy, double absoluteAccuracy)
	{
		this.integrator = new org.apache.commons.math3.analysis.integration.IterativeLegendreGaussIntegrator(n, relativeAccuracy, absoluteAccuracy);
	}
	
	/**
	 * Instantiates a new legendre gauss integrator.
	 * 
	 * @param n
	 *          the n
	 * @param relativeAccuracy
	 *          the relative accuracy
	 * @param absoluteAccuracy
	 *          the absolute accuracy
	 * @param minimalIterationCount
	 *          the minimal iteration count
	 * @param maximalIterationCount
	 *          the maximal iteration count
	 */
	public LegendreGaussIntegrator(int n, double relativeAccuracy, double absoluteAccuracy, int minimalIterationCount, int maximalIterationCount)
	{
		this.integrator = new org.apache.commons.math3.analysis.integration.IterativeLegendreGaussIntegrator(n, relativeAccuracy, absoluteAccuracy, minimalIterationCount, maximalIterationCount);		
	}
	
	/**
	 * Integrate.
	 * 
	 * @param f
	 *          the f
	 * @param lower
	 *          the lower
	 * @param upper
	 *          the upper
	 * @param maxEval
	 *          the max eval
	 * @return the double
	 */
	public double integrate(final UnivariateFunction f, double lower, double upper, int maxEval)
	{
		org.apache.commons.math3.analysis.UnivariateFunction func = new org.apache.commons.math3.analysis.UnivariateFunction()
		{		
			@Override
			public double value(double x)
			{
				return f.value(x);
			}
		};
		
		return this.integrator.integrate(maxEval, func, lower, upper);
	}
}
