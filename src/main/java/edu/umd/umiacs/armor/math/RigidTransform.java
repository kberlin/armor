/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * The Class RigidTransform.
 */
public final class RigidTransform implements Serializable
{
	
	/** The is identity. */
	private boolean isIdentity;

	/** The is origin. */
	private boolean isOrigin;
	
	/** The R. */
	private final Rotation R;
	
	/** The t. */
	private final Point3d t;
	
	/** The identity. */
	private static RigidTransform identity = null;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3241249475260736224L;
	
	/**
	 * Identity transform.
	 * 
	 * @return the rigid transform
	 */
	public static RigidTransform identityTransform()
	{
		if (identity == null)
			identity = new RigidTransform(null,null);
		
		return identity;
	}
	
	/**
	 * Instantiates a new rigid transform.
	 * 
	 * @param R
	 *          the r
	 * @param t
	 *          the t
	 */
	public RigidTransform(Rotation R, Point3d t)
	{
		if (R==null)
			R = Rotation.identityRotation();
		
		if (t ==null)
			t = new Point3d(0.0,0.0,0.0);
		
		this.isIdentity = R.isIdentity();		
		this.isOrigin = t.isOrigin();
		
		this.R = R;
		this.t = t;
	}
	
	/**
	 * Gets the rotation.
	 * 
	 * @return the rotation
	 */
	public Rotation getRotation()
	{
		return this.R;
	}

	/**
	 * Gets the translation.
	 * 
	 * @return the translation
	 */
	public Point3d getTranslation()
	{
		return this.t;
	}
	
	/**
	 * Inverse transform.
	 * 
	 * @param p
	 *          the p
	 * @return the point3d
	 */
  public Point3d inverseTransform(Point3d p)
  {
  	if (this.isOrigin)
  	{
  		if (this.isIdentity)
  			return p;
  		
  		return p.inverseRotate(this.R);
  	}
  	
  	if (this.isIdentity)
  		return p.subtract(this.t);
  	
    return p.subtract(this.t).inverseRotate(this.R);
  }
	
	/**
	 * Checks if is identity transform.
	 * 
	 * @return true, if is identity transform
	 */
	public boolean isIdentityTransform()
	{
		return this.isIdentity;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder s = new StringBuilder();

		s.append("R = \n["+this.R);
		s.append("]\n");
		s.append("t = "+this.t+"\n");
		
		return s.toString();
	}
	
  /**
	 * Transform.
	 * 
	 * @param points
	 *          the points
	 * @return the array list
	 */
	public ArrayList<Point3d> transform(Collection<Point3d> points)
	{
		ArrayList<Point3d> newPoints = new ArrayList<Point3d>(points.size());
		for (Point3d p : points)
		{
			newPoints.add(transform(p));
		}
		
		return newPoints;
	}
	
  /**
	 * Transform.
	 * 
	 * @param x
	 *          the x
	 * @param y
	 *          the y
	 * @param z
	 *          the z
	 * @return the point3d
	 */
  public Point3d transform(double x, double y, double z)
  {
  	return transform(new Point3d(x,y,z));
  }
  
  /**
	 * Transform.
	 * 
	 * @param p
	 *          the p
	 * @return the point3d
	 */
  public Point3d transform(Point3d p)
  {
  	if (this.isIdentity)
  	{
  		if (this.isOrigin)
  			return p;
  		
  		return p.add(this.t);
  	}

  	if (this.isOrigin)
			return p.rotate(this.R);
  	
  	return p.rotate(this.R).add(this.t);
  }

	/**
	 * Union.
	 * 
	 * @param transform
	 *          the transform
	 * @return the rigid transform
	 */
	public RigidTransform union(RigidTransform transform)
	{
		Rotation newR = transform.R.mult(this.R);
		Point3d newT = this.t.rotate(transform.R).add(transform.t);
		
		return new RigidTransform(newR, newT);
	}
  
  
}
