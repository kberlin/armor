/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.QRDecomposition;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.util.Pair;

public final class LinearConstraintedLinearLeastSquares extends AbstractActiveSetOptimizer<RealMatrix> implements LinearLeastSquaresOptimizer
{
	private RealMatrix A;
	/**
	 * 
	 */
	private static final long serialVersionUID = 802077058813155693L;
	
	public LinearConstraintedLinearLeastSquares()
	{
		super(1.0e-10);
		this.A = null;
	}
	
	private double[] computeGradient(double[] x, double[] y)
	{
		double[] d = BasicMath.subtract(this.A.mult(x), y);
		
		return this.A.transpose().mult(d);
	}
	
	@Override
	public RealMatrix getFunction()
	{
		return this.A;
	}

	@Override
	public int maxThreads()
	{
		return Integer.MAX_VALUE;
	}
	
	@Override
	public void setA(RealMatrix A)
	{
		this.A = A;
	}

	@Override
	public void setFunction(RealMatrix A)
	{
		setA(A);
	}

	@Override
	public double[] solve(double[] y)
	{
		throw new OptimizationRuntimeException("Method not implemented yet.");
	}

	public double[] solve(double[] y, double[] x0)
	{
		double[] x = x0;
		double[] xprev = x0;
		
		LinearConstraints activeConstraints = new LinearConstraints();	
		ArrayList<Pair<LinearConstraints,Integer>> currActiveSet = new ArrayList<Pair<LinearConstraints,Integer>>();		
		ArrayList<Pair<LinearConstraints, Integer>> prevActiveSet = new ArrayList<Pair<LinearConstraints,Integer>>();
		
		if (violatesConstraints(x0))
			throw new OptimizationRuntimeException("Initial solution not feasible.");

		while (true)
		{
			//solve the problem with the current active set
			xprev = x;
			x = solveSubProblem(y, xprev, this.equalityConstraints.merge(activeConstraints));
			
			//find none violating point
			x = validValueSearch(xprev, x, getConstraintAbsErrorTol());
			
			//compute the gradient
			double[] g = computeGradient(x, y);
			
			//store previous result
			prevActiveSet = currActiveSet;

			//find inactive constraints
			ArrayList<Pair<LinearConstraints, Integer>> currInactiveSet = getInactiveConstrains(x, g, activeConstraints, prevActiveSet);			

			//find which constraints are activily violated
			currActiveSet = getViolatedConstraints(x);			
			currActiveSet.removeAll(currInactiveSet);
									
			//if violations have not changed from previous result, exit from loop
			if (currInactiveSet.isEmpty() && currActiveSet.size()==prevActiveSet.size())
			{
				break;
			}

			activeConstraints = generateActiveConstraints(currActiveSet);
		}
		
		return x;
	}

	public double[] solveSubProblem(double[] y, double[] x0, LinearConstraints equalityConstraints)
	{
		if (equalityConstraints.isEmpty())
		{
			QRDecomposition qr = new QRDecomposition(this.A);
			double[] x = qr.solve(y);
			
			return x;
		}
		
		//get null space info
		RealMatrix nullSpace = equalityConstraints.getNullSpace();
				
		//no degrees of freedom, return feasible solution
		if (nullSpace==null)
			return x0;

		//compute the null space problem setup
		RealMatrix Am = this.A.mult(nullSpace);			
		double[] ym = BasicMath.subtract(y, this.A.mult(x0));
		
		//solve the null space problem
		double[] z = new QRDecomposition(Am).solve(ym);

		//move the solution back into original space
	  double[] x = BasicMath.add(x0, nullSpace.mult(z));
	  
	  return x;
	}

}
