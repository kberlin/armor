/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import edu.umd.umiacs.armor.math.func.DifferentiableMultivariateVectorFunction;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.util.BasicUtils;

public final class FiniteDifferenceDMVFWrapper implements DifferentiableMultivariateVectorFunction
{
	private final boolean centralDifference;
	private final MultivariateVectorFunction f;
	private final double h;
	
	public FiniteDifferenceDMVFWrapper(MultivariateVectorFunction f, double h)
	{
		this(f,h,true);
	}
	
	public FiniteDifferenceDMVFWrapper(MultivariateVectorFunction f, double h, boolean centralDifference)
	{
		this.f = f;
		this.h = h;
		this.centralDifference = centralDifference;
	}
	
	@Override
	public RealMatrix jacobian(double[] x)
	{
		return new Array2dRealMatrix(jacobianArray(x), false);
	}
	
	public double[][] jacobianArray(double[] x)
	{
		for (double val : x)
			if (Double.isNaN(val))
				throw new IllegalArgumentException("Function input value is NaN for array "+BasicUtils.toString(x)+".");

		double[] f0 = null;
		double diff;
		if (!this.centralDifference)
		{
			f0 = this.f.value(x);
		
			for (double val : f0)
				if (Double.isNaN(val))
					throw new IllegalArgumentException("Function returned NaN for input "+BasicUtils.toString(x)+".");
			
			diff = this.h;
		}
		else
			diff = 2.0*this.h;
		
		double[] argTemp = x.clone();
		double[][] J = null;
		
		for (int xIter=0; xIter<argTemp.length; xIter++)
		{
			argTemp[xIter] += this.h;

			double[] fh = this.f.value(argTemp);
			for (double val : fh)
				if (Double.isNaN(val))
					throw new IllegalArgumentException("Function returned NaN for input "+BasicUtils.toString(argTemp)+".");

			//if first time in loop, init J now that you know the size
			if (J==null)
				J = new double[fh.length][x.length];
			
			double[] fprev;
			if (!this.centralDifference)
			{
				fprev = f0;
				argTemp[xIter] -= this.h;
			}
			else
			{
				argTemp[xIter] -= 2.0*this.h;
				
				fprev = this.f.value(argTemp);
				for (double val : fprev)
					if (Double.isNaN(val))
						throw new IllegalArgumentException("Function returned NaN for input "+BasicUtils.toString(argTemp)+".");
				
				//reset back to where it was
				argTemp[xIter] += this.h;
			}

			for (int yIter=0; yIter<J.length; yIter++)
			{
				J[yIter][xIter] = (fh[yIter]-fprev[yIter])/diff;
			}
			
		}
		
		return J;
	}


	/* (non-Javadoc)
	 * @see org.apache.commons.math3.analysis.MultivariateVectorFunction#value(double[])
	 */
	@Override
	public double[] value(double[] x)
	{
		return this.f.value(x);
	}

}
