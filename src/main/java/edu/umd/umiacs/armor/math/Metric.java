package edu.umd.umiacs.armor.math;

public interface Metric<T>
{
	double distance(T a, T b);
	double distanceSquard(T a, T b);
	T requiredSubObject(T a);
}
