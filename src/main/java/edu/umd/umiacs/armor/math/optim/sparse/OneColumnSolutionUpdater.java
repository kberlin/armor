package edu.umd.umiacs.armor.math.optim.sparse;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.util.BasicUtils;

public abstract class OneColumnSolutionUpdater implements Comparable<OneColumnSolutionUpdater>
{
	protected final SparseOptimizationInformation properties;
	protected final int[] activeColumns;
	protected final double[] residual;
	protected final double residualNorm;
	protected final double[] x;

	public OneColumnSolutionUpdater(double[] y, SparseOptimizationInformation properties)
	{
	 	this.activeColumns = null;
		
		this.x = null;
		this.residual = y;
		this.residualNorm = BasicMath.norm(y);
		this.properties = properties;
	}	
	
	public OneColumnSolutionUpdater(int[] activeColumns, double[] x, double[] residual, SparseOptimizationInformation properties)
	{
		this.activeColumns = activeColumns;
		this.x = x;
		this.residual = residual;
		this.residualNorm = BasicMath.norm(residual);
		this.properties = properties;
	}

	@Override
	public final int compareTo(final OneColumnSolutionUpdater s)
	{
		return Double.compare(this.getResidualNorm(), s.getResidualNorm());
	}

	private boolean containsColumn(int column)
	{
		if (this.activeColumns==null)
			return false;
		
		for (int index : this.activeColumns)
			if (index == column)
				return true;
		
		return false;
	}
		
	public final double[] bestColumnsResidualNorms(final double[] y, final double yNorm)
	{
		final double[] residualNorms = new double[this.properties.numColumns()];
		final double[] localResidual = new double[this.residual.length];
		
		final double[] z = this.properties.computeNormalizedEstimate(this.residual);
		
		final double[][] At = this.properties.getNormalizedMatrix();
		
		//double sumX = 0.0;
		//if (this.properties.isPositiveOnly())
		//	sumX = BasicMath.sum(this.x);
		
		//go though each column and compute the residual
		for (int col=0; col<z.length; col++)
		{
			if (containsColumn(col))
			{
				residualNorms[col] = Double.NaN;
				continue;
			}
			
			//see if small contribution makes a difference
			if (this.properties.isPositiveOnly() && z[col]<0.0)
		  	z[col] = -z[col]*1.0e-4;			  
			
			//make sure z cannot exceed max sum
	  	//if (this.properties.isSumConstrained())
	  	//	z[col] = Math.min(z[col], getNormalizedMaxSum(yNorm)-sumX);			  

			double[] column = At[col];
			for (int row=0; row<column.length; row++)
				localResidual[row] = this.residual[row]-column[row]*z[col];
				
			residualNorms[col] = BasicMath.norm(localResidual);
		}
		
		return residualNorms;
	}

	public int[] getColumnsRef()
	{
		return this.activeColumns;
	}
	
	public double[] getUnscaledX(double yNorm)
	{
		double[] x = this.x.clone();
		double scale = yNorm/this.properties.getColumnsNorm();
		for (int iter=0; iter<this.x.length; iter++)
			x[iter] = scale*x[iter];
		
		return x;
	}

	public double[] getUnscaledFullX(double yNorm)
	{
		double[] x = BasicUtils.createArray(this.properties.numColumns(), 0.0);
		double scale = yNorm/this.properties.getColumnsNorm();
		for (int iter=0; iter<this.x.length; iter++)
		{
			int col = this.activeColumns[iter];
			x[col] = scale*this.x[iter];
		}
		
		return x;
	}

	public double getResidualNorm()
	{
		return this.residualNorm;
	}

	public double[] getResidualRef()
	{
		return this.residual;
	}

	public boolean isEmpty()
	{
		return this.activeColumns==null;
	}
	
	public double getNormalizedMaxSum(double yNorm)
	{
		return this.properties.getNormalizedMaxSum()/yNorm;
	}

	public boolean isXSumOk(double[] x, double yNorm, double eps)
	{
		if (!this.properties.isSumConstrained())
			return true;
		
		return BasicMath.sum(x)<=getNormalizedMaxSum(yNorm)+eps;
	}
	
	public abstract OneColumnSolutionUpdater oneColumnUpdate(int column, double[] y, double yNorm, double relError);

}