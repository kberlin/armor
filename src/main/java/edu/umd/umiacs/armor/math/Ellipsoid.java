/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math;

import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3dImpl;

/**
 * The Class Ellipsoid.
 */
public final class Ellipsoid extends SymmetricMatrix3dImpl
{

	private static final long serialVersionUID = 2674179512043930699L;
	
	public static Ellipsoid createFromCovariance(SymmetricMatrix3d covMatrix)
	{
		EigenDecomposition3d eig = covMatrix.getEigenDecomposition();
		
		double[] diag = new double[covMatrix.size()];
		
		for (int iterRow = 0; iterRow<diag.length; iterRow++)
		{
			double val = 1.0/(3.0*eig.getEigenvalue(iterRow));			
			if (val<0.0)
				throw new MathRuntimeException("Covariance Matrix must have positive diagnal values.");

			diag[iterRow] = val;
		}
		
		return new Ellipsoid(new SymmetricMatrix3dImpl(diag[0], diag[1], diag[2], eig.getRotation()));
	}
  
  private Ellipsoid(SymmetricMatrix3d E)
  {
  	super(E);
  }
	
	/**
	 * Gets the lengths.
	 * 
	 * @return the lengths
	 */
	public double[] getLengths()
	{
		double[] val = getEigenDecomposition().getEigenvalues();
		
		val[0] = BasicMath.sqrt(1.0/val[0]);
		val[1] = BasicMath.sqrt(1.0/val[1]);
		val[2] = BasicMath.sqrt(1.0/val[2]);
		
		return val;
	}
}
