/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;


public class Array2dRealMatrix extends ApacheCommonsRealDynamicMatrixWrapper<org.apache.commons.math3.linear.Array2DRowRealMatrix>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7681971170615396951L;
	
	protected Array2dRealMatrix(Array2dRealMatrix A)
	{
		super(new org.apache.commons.math3.linear.Array2DRowRealMatrix(A.getDataRef(), true));
	}
	
	public Array2dRealMatrix(double[] b)
	{
		super(new org.apache.commons.math3.linear.Array2DRowRealMatrix(b.length, 1));
		this.A.setColumn(0, b);
	}

	public Array2dRealMatrix(double[][] A)
	{
		this(A, true);
	}
	
	public Array2dRealMatrix(double[][] A, boolean copy)
	{
		super(new org.apache.commons.math3.linear.Array2DRowRealMatrix(A, copy));
	}
	
	public Array2dRealMatrix(int numRows, int numColumns)
	{
		super(new org.apache.commons.math3.linear.Array2DRowRealMatrix(numRows, numColumns));
	}
	
	protected Array2dRealMatrix(org.apache.commons.math3.linear.Array2DRowRealMatrix A)
	{
		super(A);
	}
	
	public double[] getColumnRef(int index)
	{
		return this.A.getDataRef()[index];
	}
	
	public final double[][] getDataRef()
	{
		return this.A.getDataRef();
	}
}
