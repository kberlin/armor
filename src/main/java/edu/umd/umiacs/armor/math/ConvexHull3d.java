/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math;

import edu.umd.umiacs.armor.math.convexhull.ConvexHullPoint3d;
import edu.umd.umiacs.armor.math.convexhull.QuickHull3D;

/**
 * The Class ConvexHull3d.
 */
public final class ConvexHull3d
{
	
	/** The hull. */
	private final QuickHull3D hull;

	/**
	 * Instantiates a new convex hull3d.
	 */
	public ConvexHull3d()
	{
		this.hull = new QuickHull3D();		
	}
	
	/**
	 * Compute hull.
	 * 
	 * @param pointArray
	 *          the point array
	 */
	private void computeHull(double[][] pointArray)
	{
		ConvexHullPoint3d[] points = new ConvexHullPoint3d[pointArray.length];
		
		for (int iter=0; iter<pointArray.length; iter++)
			points[iter] = new ConvexHullPoint3d(pointArray[iter][0],pointArray[iter][1],pointArray[iter][2]);

		//generate the convex hull
		this.hull.build(points);		
	}
	
	/**
	 * Gets the hull point array.
	 * 
	 * @param pointArray
	 *          the point array
	 * @return the hull point array
	 */
	public double[][] getHullPointArray(double[][] pointArray)
	{
		//compute the hull
		computeHull(pointArray);
		
		//extract the veritices
		int[] vertexIndex = this.hull.getVertexPointIndices();

		//get the hull points
		double[][] hullVertices = new double[vertexIndex.length][3];
		for (int iter=0; iter<vertexIndex.length; iter++)
		{
			hullVertices[iter][0] = pointArray[vertexIndex[iter]][0];
			hullVertices[iter][1] = pointArray[vertexIndex[iter]][1];
			hullVertices[iter][2] = pointArray[vertexIndex[iter]][2];
		}
		
		return hullVertices;
	}

		
	/**
	 * Gets the hull vertex indices.
	 * 
	 * @param pointArray
	 *          the point array
	 * @return the hull vertex indices
	 */
	public int[] getHullVertexIndices(double[][] pointArray)
	{
		computeHull(pointArray);
		
		return this.hull.getVertexPointIndices();
	}
}
