package edu.umd.umiacs.armor.math.optim;

import java.io.Serializable;

public interface LinearSolution extends Serializable, Comparable<LinearSolution>
{
	public double absoluteError();
	public double chiSquared();
	public int dimension();
	public int[] getActiveColumns();	
	public double[] getActiveWeights();	
	public int[] getOrderedActiveColumns();
	public double[] getOrderedActiveWeights();
	public double[] getOrderedNormalizedActiveWeights();		
	public double[] getX();
	public int l0Norm();
	
	public String toFileString();
	
	@Override
	public String toString();
	
	public String toString(double yNorm);
}
