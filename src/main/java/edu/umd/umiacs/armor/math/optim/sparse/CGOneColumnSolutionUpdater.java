/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim.sparse;

import java.util.Arrays;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.optim.ConjugantGradientLinearLeastSquares;

public final class CGOneColumnSolutionUpdater extends OneColumnSolutionUpdater 
{
	protected CGOneColumnSolutionUpdater(double[] y, SparseOptimizationInformation properties)
	{
  	super(y, properties);
	}
	
	protected CGOneColumnSolutionUpdater(int[] activeColumns, double[] x, double[] residual, SparseOptimizationInformation properties)
	{
		super(activeColumns, x, residual, properties);
	}

	@Override
	public CGOneColumnSolutionUpdater oneColumnUpdate(int column, double[] yNormed, double yNorm, double relError)
	{
		//merge old columns with the new
		int[] updatedColumnIndicies;
		double[] x0;
		if (this.activeColumns==null)
		{
			updatedColumnIndicies = new int[1];
			x0 = new double[1];
		}
		else
		{
			updatedColumnIndicies = Arrays.copyOf(this.activeColumns, this.activeColumns.length+1);
			x0 = Arrays.copyOf(this.x, this.activeColumns.length+1);
		}
		updatedColumnIndicies[updatedColumnIndicies.length-1] = column;
		x0[x0.length-1] = 0;
		
		double[][] AtSubMatrix = this.properties.getNormalizedSubAt(updatedColumnIndicies);

		ConjugantGradientLinearLeastSquares solver = new ConjugantGradientLinearLeastSquares(this.properties.numColumns()*100, relError);
		solver.setATranposed(AtSubMatrix);
		
		//solve using the QR decomposition
		double[] xUpdated = solver.optimize(yNormed, x0);
		double[] yPred = BasicMath.multTranspose(AtSubMatrix, xUpdated);
		
		//compute the residual
	  double[] rUpdated = BasicMath.subtract(yNormed,yPred);
	  
		if ((!this.properties.isSumConstrained() || isXSumOk(xUpdated, yNorm, relError))
				&& (!this.properties.isPositiveOnly() || BasicMath.min(xUpdated)>0.0))
		{
		  return new CGOneColumnSolutionUpdater(updatedColumnIndicies, xUpdated, rUpdated, this.properties);
		}
		else 
		if (!this.properties.isSumConstrained())
		{
		  return null;
		}
		else
		if (updatedColumnIndicies.length==1)
		{
			//nothing to do since only one column
			if (xUpdated[0]<=0.0)
				return null;
			
			//update the residual
			xUpdated[0] = this.getNormalizedMaxSum(yNorm);			
			yPred = BasicMath.multTranspose(AtSubMatrix, xUpdated);
		  rUpdated = BasicMath.subtract(yNormed,yPred);

			return new CGOneColumnSolutionUpdater(updatedColumnIndicies, xUpdated, rUpdated, this.properties);
		}
		else
		{		
			//this code will use the active set method to apply the max sum constraint
			
			//get the matrix and the null space for the max sum
			double[][] constraintNullSpaceT = this.properties.getSumNullSpaceTransposed(updatedColumnIndicies.length);
			
			//get feasible point
			x0[x0.length-1] = this.getNormalizedMaxSum(yNorm)-BasicMath.sum(this.x);
			
			//compute the null space problem setup
			double[] ym = BasicMath.subtract(yNormed, BasicMath.multTranspose(AtSubMatrix, x0));
			
			//solve the null space problem
			solver.setATranposed(constraintNullSpaceT, AtSubMatrix);
			double[] z = solver.optimize(ym);

			//move the solution back into the original space
		  xUpdated = BasicMath.add(x0, BasicMath.multTranspose(constraintNullSpaceT, z));

			//update solution
			yPred = BasicMath.multTranspose(AtSubMatrix, xUpdated);
		  rUpdated = BasicMath.subtract(yNormed,yPred);
		  
		  //store solution
			if (!this.properties.isPositiveOnly() || BasicMath.min(xUpdated)>0.0)
				return new CGOneColumnSolutionUpdater(updatedColumnIndicies, xUpdated, rUpdated, this.properties);
			else				
				return null;
		}
	}
}