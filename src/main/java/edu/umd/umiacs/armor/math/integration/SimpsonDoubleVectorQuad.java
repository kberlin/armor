/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.integration;

import edu.umd.umiacs.armor.math.MathRuntimeException;
import edu.umd.umiacs.armor.math.func.TwoVariableVectorFunction;
import edu.umd.umiacs.armor.math.func.UnivariateVectorFunction;

/**
 * The Class SimpsonDoubleVectorQuad.
 */
public class SimpsonDoubleVectorQuad implements DoubleVectorIntegrator
{
	
	/**
	 * The Class XFunction.
	 */
	private class XFunction implements UnivariateVectorFunction
	{
		/** The a. */
		private double a;
		
		/** The b. */
		private double b;
		
		/** The f. */
		private YFunction f;
		
		/** The integrator y. */
		SimpsonVectorQuad integratorY;
		
		/** The max evals. */
		private int maxEvals;
		
		/**
		 * Instantiates a new x function.
		 * 
		 * @param f
		 *          the f
		 * @param integratorY
		 *          the integrator y
		 * @param a
		 *          the a
		 * @param b
		 *          the b
		 * @param realTolerance
		 *          the real tolerance
		 * @param maxEvals
		 *          the max evals
		 */
		public XFunction(YFunction f, SimpsonVectorQuad integratorY, double a, double b, double realTolerance, int maxEvals)
		{
			this.f = f;
			this.a = a;
			this.b = b;
			this.maxEvals = maxEvals;
			this.integratorY = integratorY;
		}
		
		/* (non-Javadoc)
		 * @see edu.umd.umiacs.armor.math.func.UnivariateVectorFunction#value(double)
		 */
		@Override
		public double[] value(double x)
		{
			this.f.setX(x);
			
			return this.integratorY.integrate(this.f, this.a, this.b, this.maxEvals);
		}
	}
	
	/**
	 * The Class YFunction.
	 */
	private class YFunction implements UnivariateVectorFunction
	{
		/** The f. */
		private TwoVariableVectorFunction f;
		
		/** The x. */
		private double x;
		
		/**
		 * Instantiates a new y function.
		 * 
		 * @param f
		 *          the f
		 * @param x
		 *          the x
		 */
		public YFunction(TwoVariableVectorFunction f, double x)
		{
			this.f = f;
			this.x = x;
		}
		
		/**
		 * Sets the x.
		 * 
		 * @param x
		 *          the new x
		 */
		public void setX(double x)
		{
			this.x = x;
		}

		/* (non-Javadoc)
		 * @see edu.umd.umiacs.armor.math.func.UnivariateVectorFunction#value(double)
		 */
		@Override
		public double[] value(double y)
		{
			return this.f.value(this.x,y);
		}
	}
	
	/** The integrator x. */
	final SimpsonVectorQuad integratorX;
	
	/** The integrator y. */
	final SimpsonVectorQuad integratorY;
	
	/** The real tolerance. */
	double realTolerance;

	
	/**
	 * Instantiates a new simpson double vector quad.
	 * 
	 * @param realTolerance
	 *          the real tolerance
	 * @param absPointTolerance
	 *          the abs point tolerance
	 */
	public SimpsonDoubleVectorQuad(double realTolerance, double absPointTolerance)
	{
		this.realTolerance = realTolerance;
		this.integratorX = new SimpsonVectorQuad(realTolerance, absPointTolerance);
		this.integratorY = new SimpsonVectorQuad(realTolerance, absPointTolerance);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.integration.DoubleVectorIntegrator#integrate(edu.umd.umiacs.armor.math.func.TwoVariableVectorFunction, double, double, double, double, int)
	 */
	@Override
	public double[] integrate(TwoVariableVectorFunction f, double x1, double x2, double y1, double y2, int maxEvals) throws MathRuntimeException
	{
		YFunction yfunc = new YFunction(f, 0.0);
		XFunction xfunc = new XFunction(yfunc, this.integratorY, y1, y2, this.realTolerance, maxEvals);

		double[] val = this.integratorX.integrate(xfunc, x1, x2, maxEvals);
		
		return val;
	}
}
