/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealLinearOperator;
import org.apache.commons.math3.linear.RealVector;

import edu.umd.umiacs.armor.util.BasicUtils;


public final class ConjugantGradientLinearLeastSquares
{
	private final class MultiRealLinearOperator extends RealLinearOperator
	{
		private final boolean isTranposed;
		private final Array2DRowRealMatrix[] matricies;
		
		public MultiRealLinearOperator(boolean isTranposed, double[][]...As)
		{
			this.isTranposed = isTranposed;

			this.matricies = new Array2DRowRealMatrix[As.length];
			for (int iter=0; iter<As.length; iter++)
			{
				if (As[iter]==null)
					throw new OptimizationRuntimeException("A matrix cannot be null.");
				
				this.matricies[iter] = new Array2DRowRealMatrix(As[iter], false);
			}
		}
		

		@Override
		public final int getColumnDimension()
		{
			if (this.isTranposed)
				return this.matricies[0].getRowDimension();
				
			return this.matricies[this.matricies.length-1].getColumnDimension();
		}

		@Override
		public final int getRowDimension()
		{
			if (this.isTranposed)
				return this.matricies[this.matricies.length-1].getColumnDimension();

			return this.matricies[0].getRowDimension();
		}
		
		@Override
		public final RealVector operate(RealVector x) throws DimensionMismatchException
		{
			if (this.isTranposed)
				return operateTransposeHelper(x);
			
			return operateHelper(x);
		}
		
		public final RealVector operateHelper(RealVector x) throws DimensionMismatchException
		{
			for (int iter=this.matricies.length-1; iter>=0; iter--)
			{
				x = this.matricies[iter].operate(x);
			}

			return x;
		}

		@Override
		public final RealVector operateTranspose(RealVector x) throws DimensionMismatchException
		{
			if (this.isTranposed)
				return operateHelper(x);
			
			return operateTransposeHelper(x);
		}
		
		public final RealVector operateTransposeHelper(RealVector x) throws DimensionMismatchException
		{
			for (int iter=0; iter<this.matricies.length; iter++)
			{
				x = this.matricies[iter].preMultiply(x);
			}

			return x;
		}

	}
	
	private final class TranposeRealLinearOperator extends RealLinearOperator
	{
		private final MultiRealLinearOperator m;
		
		public TranposeRealLinearOperator(MultiRealLinearOperator m)
		{
			this.m = m;
		}		

		@Override
		public final int getColumnDimension()
		{
			return this.m.getColumnDimension();
		}

		@Override
		public final int getRowDimension()
		{
			return this.m.getColumnDimension();
		}
		
		@Override
		public final RealVector operate(RealVector x) throws DimensionMismatchException
		{
			return this.m.operateTranspose(this.m.operate(x));
		}
		
		@Override
		public final RealVector operateTranspose(RealVector x) throws DimensionMismatchException
		{
			return this.m.operate(this.m.operateTranspose(x));
		}
	}

	private final ConjugateGradientSolver cgSolver;
	private MultiRealLinearOperator M;
	
	public ConjugantGradientLinearLeastSquares(int maxIterations, double relativeError)
	{
		this.cgSolver = new ConjugateGradientSolver(maxIterations, relativeError);
		this.M = null;
	}
	
	public double[] optimize(final double[] y)
	{
		return optimize(y, null);
	}
	
	public double[] optimize(final double[] y, double[] x0)
	{
		if (this.M==null)
			throw new OptimizationRuntimeException("A matrix must be set.");
		
		if (y==null)
			throw new OptimizationRuntimeException("y cannot be null.");
		
		if (x0==null)
			x0 = BasicUtils.createArray(this.M.getColumnDimension(), 0.0);
		
		RealVector Aty = this.M.operateTranspose(new ArrayRealVector(y, false));
		TranposeRealLinearOperator T = new TranposeRealLinearOperator(this.M);
		
		return this.cgSolver.solve(T, Aty, x0);
	}

	public void setA(boolean aTransposed, final double[][]... A)
	{
		this.M = new MultiRealLinearOperator(aTransposed, A);		
	}
	
	public void setA(final double[][]... A)
	{
		this.M = new MultiRealLinearOperator(false, A);		
	}

	public void setATranposed(final double[][]... At)
	{
		this.M = new MultiRealLinearOperator(true, At);		
	}
}
