/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim.sparse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.optim.LinearSolution;
import edu.umd.umiacs.armor.math.optim.OptimizationRuntimeException;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.Pair;
import edu.umd.umiacs.armor.util.SortablePair;

public final class SparseLinearSolution implements LinearSolution
{
	private final int dimension;
	private double error;
	private final ArrayList<Pair<Integer,Double>> indexValuePair;
	private final int[] order;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3427354715033085631L;
	
	private static int[] computeOrder(ArrayList<Pair<Integer, Double>> valuePairs)
	{
		//store the sparse vector
		ArrayList<SortablePair<Double, Integer>> pairArray = new ArrayList<SortablePair<Double, Integer>>(valuePairs.size());
		int counter = 0;
		for (Pair<Integer, Double> pair : valuePairs)
		{
			pairArray.add(new SortablePair<Double, Integer>(Math.abs(pair.y), counter));
			counter++;
		}
		
		//sort the values in the descending order of importance
		Collections.sort(pairArray, Collections.reverseOrder());
		
		int[] order = new int[pairArray.size()];
		for (int iter=0; iter<pairArray.size(); iter++)
			order[iter] = pairArray.get(iter).y;
		
		return order;
	}
	
	public SparseLinearSolution(double error, double[] x, double zeroThreshold)
	{
		this.error = error;
		this.dimension = x.length;
		
		//store the sparse vector
		this.indexValuePair = new ArrayList<Pair<Integer, Double>>();
		for (int iter = 0; iter < x.length; iter++)
		{
			if (Math.abs(x[iter])>=zeroThreshold)
			{
				this.indexValuePair.add(new Pair<Integer,Double>(iter,x[iter]));
			}
		}
		
		this.order = computeOrder(this.indexValuePair);
	}
		
	public SparseLinearSolution(double error, double[] x, int[] cols, int dimension)
	{
		this.error = error;
		this.dimension = dimension;
		
		//store the sparse vector
		this.indexValuePair = new ArrayList<Pair<Integer, Double>>();
		for (int iter = 0; iter < x.length; iter++)
		{
			if (cols[iter]>=dimension)
				throw new OptimizationRuntimeException("Input column "+cols[iter]+" bigger than dimension size "+dimension+".");
			
			this.indexValuePair.add(new Pair<Integer,Double>(cols[iter],x[iter]));
		}
		
		this.order = computeOrder(this.indexValuePair);
	}
	
	private SparseLinearSolution(SparseLinearSolution prevSolution, double error)
	{
		this.error = error;
		this.dimension = prevSolution.dimension;
		this.indexValuePair = prevSolution.indexValuePair;
		this.order = prevSolution.order;
	}
	
	@Override
	public double absoluteError()
	{
		return this.error;
	}
	
	@Override
	public double chiSquared()
	{
		return this.error*this.error;
	}
	
	@Override
	public int compareTo(LinearSolution sol)
	{
		int compare = Double.compare(this.error, sol.absoluteError());
		
		if (compare!=0)
			return compare;
		
		return Integer.valueOf(l0Norm()).compareTo(Integer.valueOf(sol.l0Norm()));
	}
	
	public SparseLinearSolution createWithError(double error)
	{
		if (this.error==error)
			return this;
		
		return new SparseLinearSolution(this, error);
	}
	
	@Override
	public int dimension()
	{
		return this.dimension;
	}

	@Override
	public int[] getActiveColumns()
	{
		int[] columns = new int[this.indexValuePair.size()];
		
		int count = 0;
		for (Pair<Integer,Double> pair : this.indexValuePair)
		{
			columns[count] = pair.x;
			count++;
		}
		
		return columns;
	}
		
	@Override
	public double[] getActiveWeights()
	{
		double[] weights = new double[this.indexValuePair.size()];
		
		int count = 0;
		for (Pair<Integer,Double> pair : this.indexValuePair)
		{
			weights[count] = pair.y;
			count++;
		}
		
		return weights;
	}

	@Override
	public int[] getOrderedActiveColumns()
	{
		int[] columnList = new int[this.order.length];
		for (int iter=0; iter<this.order.length; iter++)
			columnList[iter] = this.indexValuePair.get(this.order[iter]).x;
		
		return columnList;
	}
	
	@Override
	public double[] getOrderedActiveWeights()
	{
		double[] weights = new double[this.indexValuePair.size()];
		for (int iter=0; iter<this.order.length; iter++)
			weights[iter] = this.indexValuePair.get(this.order[iter]).y;
		
		return weights;
	}
	
	@Override
	public double[] getOrderedNormalizedActiveWeights()
	{
		double[] weights = getOrderedActiveWeights();
		double sum = BasicMath.sum(weights);
		weights = BasicMath.divide(weights, sum);
		
		return weights;
	}
	
	@Override
	public double[] getX()
	{
		double[] x = BasicUtils.createArray(this.dimension, 0.0);
		for (Pair<Integer,Double> pair : this.indexValuePair)
			x[pair.x] = pair.y;
		
		return x;
	}

	@Override
	public int l0Norm()
	{
		return this.indexValuePair.size();
	}	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		
		str.append("Chi^2="+chiSquared()+", Columns=[");
		int[] list = getOrderedActiveColumns();
		for (int num : list)
			str.append(""+(num+1)+" ");
		str.append("]");
		
		double[] weights = getOrderedActiveWeights();
		str.append(", Weights=[");
		for (double num : weights)
			str.append(""+num+" ");
		str.append("]");

		return str.toString();
	}

	@Override
	public String toString(double yNorm)
	{
		StringBuilder str = new StringBuilder();
		
		str.append("Relative Error="+absoluteError()/yNorm+", Columns=[");
		int[] list = getOrderedActiveColumns();
		for (int num : list)
			str.append(""+(num+1)+" ");
		str.append("]");
		
		double[] weights = getOrderedActiveWeights();
		str.append(", Weights=[");
		for (double num : weights)
			str.append(""+num+" ");
		str.append("]");

		return str.toString();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.LinearSolution#toFileString()
	 */
	@Override
	public String toFileString()
	{
		StringBuilder s = new StringBuilder();
		
		//print norm
		s.append(String.format(Locale.US,"%d ", l0Norm()));
		
		//print chi^2
		s.append(String.format(Locale.US,"%.8g ", chiSquared()));
		
		for (int col : getOrderedActiveColumns())
			s.append(String.format(Locale.US,"%d\t", col+1));
		for (double weight : getOrderedActiveWeights())
			s.append(String.format(Locale.US,"%.8g\t", weight));				

		return s.toString();
	}	
}