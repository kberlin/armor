/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim.sparse;

import java.io.File;
import java.io.FileNotFoundException;

import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.LinearMathFileIO;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.optim.LinearSolutions;
import edu.umd.umiacs.armor.refinement.AbstractSparseEnsembleSelection;
import edu.umd.umiacs.armor.util.ParseOptions;
import edu.umd.umiacs.armor.util.ProgressObserver;


public class SparseLinearRefinement implements ProgressObserver
{
	private int l0 = -1;
	private SparseLogLikelihoodSolver solver;

	/**
	 * @param args
	 * @throws RuntimeException 
	 * @throws FileNotFoundException 
	 */
	public static void main(String[] args) throws FileNotFoundException, RuntimeException
	{
  	ParseOptions options = new ParseOptions();
  	options.addRequiredOption("pred", "pred", "Name of prediction matrix file.", String.class);
  	options.addRequiredOption("data", "data", "Name of data file.", String.class);
  	options.addOption("eps", "eps", "Relative error.", AbstractSparseEnsembleSelection.DEFAULT_RELATIVE_ERROR_TOL);
  	options.addOption("K", "K", "Number top solutions.", MultiOrthogonalMatchingPursuit.DEFAULT_TOP_SOLUTION_SIZE);

  	try 
  	{
  		options.parse(args);
  		if (options.needsHelp())
  		{
  			System.out.println(options.helpMenuString());
  			System.exit(0);
  		}
  		
  		options.checkParameters();
  	}
  	catch (Exception e)
  	{
  		System.err.println(e.getMessage());
  		System.err.println(options.helpMenuString());
  		System.exit(1);
  	}
  	
  	//read the A matrix
  	Array2dRealMatrix A = LinearMathFileIO.read(new File(options.get("pred").getString()));

  	//read the A matrix
  	Array2dRealMatrix y = LinearMathFileIO.read(new File(options.get("data").getString()));
  	
  	SparseLinearRefinement refinement = new SparseLinearRefinement(
  			options.get("K").getInteger(),
  			options.get("eps").getDouble(),
  			0.005);
  	
  	LinearSolutions sol = refinement.solve(A,y);
  	
  	System.out.println(sol);

	}
	
	public SparseLinearRefinement(int numTopSolutions, double relEps, double storageCutoff)
	{
  	this.solver = new SparseLogLikelihoodSolver(
  			new MultiOrthogonalMatchingPursuit(numTopSolutions, true, storageCutoff));
  	this.solver.setRelativeErrorTol(relEps);
		this.solver.addProgressObserver(this);
	}

	public LinearSolutions solve(RealMatrix A, RealMatrix y)
	{
		this.solver.setA(A);
		return this.solver.solveAlt(y.getColumn(0), y.getColumn(1), Integer.MAX_VALUE);
	}
	
	public LinearSolutions solve(RealMatrix A, double[] y)
	{
		this.solver.setA(A);
		return this.solver.solveAlt(y);
	}

	@Override
	public void update(double progressPercentage)
	{
		int numIterations = (int)progressPercentage;
		if (progressPercentage>this.l0 )
		{
			this.l0 = numIterations;
			System.out.print(""+numIterations+"...");
		}
	}
	
}
