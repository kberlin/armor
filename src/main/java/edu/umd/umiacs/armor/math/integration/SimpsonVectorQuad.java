/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.integration;

import edu.umd.umiacs.armor.math.MathRuntimeException;
import edu.umd.umiacs.armor.math.func.UnivariateVectorFunction;

/**
 * The Class SimpsonVectorQuad.
 */
public final class SimpsonVectorQuad implements UnivariateVectorIntegrator
{
	
	/** The abs point tolerance. */
	final double absPointTolerance;
	
	/** The real tolerance. */
	final double realTolerance;
	
	/**
	 * Instantiates a new simpson vector quad.
	 * 
	 * @param realTolerance
	 *          the real tolerance
	 */
	public SimpsonVectorQuad(double realTolerance)
	{
		this(realTolerance,1.0e-8);
	}

	/**
	 * Instantiates a new simpson vector quad.
	 * 
	 * @param realTolerance
	 *          the real tolerance
	 * @param absPointTolerance
	 *          the abs point tolerance
	 */
	public SimpsonVectorQuad(double realTolerance, double absPointTolerance)
	{
		this.realTolerance = realTolerance;
		this.absPointTolerance = absPointTolerance;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.integration.UnivariateVectorIntegrator#integrate(edu.umd.umiacs.armor.math.func.UnivariateVectorFunction, double, double, int)
	 */
	@Override
	public double[] integrate(UnivariateVectorFunction f, double a, double b, int maxEvals)
	{
		IntegratorProperties property = new IntegratorProperties();
		property.evalCount = 0;
		
		double h = 0.13579*(b-a);
		double[] x = {a, a+h, a+2.0*h, (a+b)*0.5, b-2.0*h, b-h, b};
		
		double[][] y = new double[7][];
		for (int iter=0; iter<x.length; iter++)
		{
		  y[iter] = f.value(x[iter]);
		  
		  for (int nanLoop=0; nanLoop<y[iter].length; nanLoop++)
			  if (Double.isNaN(y[iter][nanLoop]))
			  	throw new MathRuntimeException("Function evaluation return NaN.");
		}
		property.evalCount += 7;
		
		double[] Q1 = new double[y[0].length];
		double[] Q2 = new double[y[0].length];
		double[] Q3 = new double[y[0].length];
		
		int depth = 0;
		quadstep(f,Q1,x[0],x[2],y[0],y[1],y[2],this.realTolerance/3.0,property,maxEvals,depth+1);
		quadstep(f,Q2,x[2],x[4],y[2],y[3],y[4],this.realTolerance/3.0,property,maxEvals,depth+1);
		quadstep(f,Q3,x[4],x[6],y[4],y[5],y[6],this.realTolerance/3.0,property,maxEvals,depth+1);
		
		//sum up the results
		for (int iter=0; iter<Q1.length; iter++)
			Q1[iter] = Q1[iter]+Q2[iter]+Q3[iter];
		
		return Q1;
	}
	
	/**
	 * Quadstep.
	 * 
	 * @param f
	 *          the f
	 * @param Q
	 *          the q
	 * @param a
	 *          the a
	 * @param b
	 *          the b
	 * @param fa
	 *          the fa
	 * @param fc
	 *          the fc
	 * @param fb
	 *          the fb
	 * @param tol
	 *          the tol
	 * @param property
	 *          the property
	 * @param maxEvals
	 *          the max evals
	 * @param depth
	 *          the depth
	 */
	private void quadstep(UnivariateVectorFunction f, double[] Q, double a, double b, 
												double[] fa, double[] fc, double[] fb, 
												double tol, IntegratorProperties property, double maxEvals, int depth)
	{		
		// Evaluate integrant twice in interior of subinterval [a,b].
		double h = b - a;
		double c = (a + b)*0.5;
		double d = (a + c)*0.5;
		double e = (c + b)*0.5;
		
		double[] fd = f.value(d);
	  for (int nanLoop=0; nanLoop<fd.length; nanLoop++)
		  if (Double.isNaN(fd[nanLoop]))
		  	throw new ArithmeticException("Function evaluation returned NaN.");

		
		double[] fe = f.value(e);
	  for (int nanLoop=0; nanLoop<fe.length; nanLoop++)
		  if (Double.isNaN(fe[nanLoop]))
		  	throw new ArithmeticException("Function evaluation returned NaN.");

		property.evalCount += 2;

		double[] Q1 = new double[fd.length];
		double[] Q2 = new double[fd.length];
		
		for (int iter=0; iter<Q.length; iter++)
		{
			// Three point Simpson's rule.
			Q1[iter] = (h/6.0)*(fa[iter] + 4.0*fc[iter] + fb[iter]);
		
			// Five point double Simpson's rule.
			Q2[iter] = (h/12.0)*(fa[iter] + 4.0*fd[iter] + 2.0*fc[iter] + 4.0*fe[iter] + fb[iter]);
		
			// One step of Romberg extrapolation.
			Q[iter] = Q2[iter] + (Q2[iter] - Q1[iter])/15.0;
		}
		
		//found the right value
		boolean allBelow = true;
		for (int iter=0; iter<Q.length; iter++)
			if (Math.abs(Q2[iter] - Q[iter]) > tol)
			{
			  allBelow = false;
			}
		
		if (allBelow)
			return;
	
	  // Maximum function count exceeded; singularity likely.
		if (property.evalCount > maxEvals)
			//return;
			throw new MathRuntimeException("Integrator exceeded maximum number of function evaluations. Singularity likely.");
		
	  // Maximum depth count exceeded; singularity likely.
		if (h<this.absPointTolerance)
		{
			return;
		}		

		// Subdivide into two subintervals.
		quadstep(f,Q1,a,c,fa,fd,fc,tol*0.5,property,maxEvals,depth+1);
		quadstep(f,Q2,c,b,fc,fe,fb,tol*0.5,property,maxEvals,depth+1);
		
		for (int iter=0; iter<Q.length; iter++)		
			Q[iter] = Q1[iter] + Q2[iter];
		
		return;
	}
}
