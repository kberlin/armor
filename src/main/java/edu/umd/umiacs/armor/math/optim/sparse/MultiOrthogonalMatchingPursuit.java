/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim.sparse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.MathRuntimeException;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.optim.LinearSolution;
import edu.umd.umiacs.armor.math.optim.LinearSolutions;
import edu.umd.umiacs.armor.math.optim.OptimizationRuntimeException;
import edu.umd.umiacs.armor.util.Indicies;
import edu.umd.umiacs.armor.util.HashCodeUtil;
import edu.umd.umiacs.armor.util.LimitedSizeCollection;
import edu.umd.umiacs.armor.util.ProgressObserver;

public final class MultiOrthogonalMatchingPursuit implements MultipleSolutionSparseLinearLeastSquaresOptimizer
{
	private final class ColumnGuess implements Comparable<ColumnGuess>
	{
		public final int column;
		public final int prevSolutionIndex;
		public final double residual;
		
		public ColumnGuess(final int uid, final int column, final double residual)
		{
			this.prevSolutionIndex = uid;
			this.column = column;
			this.residual = residual;
		}

		@Override
		public final int compareTo(final ColumnGuess s)
		{
			//compare reverse order
			int value = Double.compare(this.residual, s.residual);
			
			if (value!=0)
				return value;
			
			value = this.column-s.column;
			if (value!=0)
				return value;
			
			return this.prevSolutionIndex-s.prevSolutionIndex;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			
			ColumnGuess other = (ColumnGuess) obj;
			
			return (this.compareTo(other)==0);
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode()
		{
	    int result = HashCodeUtil.SEED;
	    
	    result = HashCodeUtil.hash(result, this.prevSolutionIndex);
	    result = HashCodeUtil.hash(result, this.column);
	    result = HashCodeUtil.hash(result, this.residual);

	    return result;		
		}		
	}
	
	private double epsilon;

	private final int maxThreads;
	private final int numTopSolutions;
	private final ArrayList<ProgressObserver> observers;
	protected final SparseOptimizationInformation properties;
	private final double solutionStorageRelativeCutoff;

	private final boolean useCG;
	
	public final static double DEFAULT_MAX_SUM = Double.POSITIVE_INFINITY;
	public final static double DEFAULT_RELATIVE_ERROR = 5.0e-4;
	public final static double DEFAULT_SOLUTION_STORAGE_RELATIVE_CUTOFF = 0.005;
	public final static int DEFAULT_TOP_SOLUTION_SIZE = 100000;
	public final static int MAX_SOLUTION_STORED = 200000;
	public final static double ZERO_VALUE_CUTOFF = 1.0e-10;
	
	public MultiOrthogonalMatchingPursuit()
	{
		this(false, DEFAULT_TOP_SOLUTION_SIZE);
	}
	
	public MultiOrthogonalMatchingPursuit(boolean useCG, int numTopSolutions)
	{
		this(useCG, numTopSolutions, false);
	}
	
	public MultiOrthogonalMatchingPursuit(boolean useCG, int numTopSolutions, boolean positiveOnly)
	{
		this(useCG, numTopSolutions, positiveOnly, DEFAULT_MAX_SUM);
	}
	
	public MultiOrthogonalMatchingPursuit(boolean useCG, int numTopSolutions, boolean positiveOnly, double maxSum)
	{
		this(useCG, numTopSolutions, positiveOnly, maxSum, DEFAULT_SOLUTION_STORAGE_RELATIVE_CUTOFF);
	}
		
	public MultiOrthogonalMatchingPursuit(boolean useCG, int numTopSolutions, boolean positiveOnly, double maxSum, double solutionStorageRelativeCutoff)
	{
		this(useCG, numTopSolutions, positiveOnly, maxSum, solutionStorageRelativeCutoff, Integer.MAX_VALUE);
	}
	
	public MultiOrthogonalMatchingPursuit(boolean useCG, int numTopSolutions, boolean positiveOnly, double maxSum, double solutionStorageRelativeCutoff, int maxThreads)
	{
		this.useCG = useCG;
		this.numTopSolutions = numTopSolutions;
		
		//since the matrix and y will be normalized, this becomes relative error
		this.epsilon = DEFAULT_RELATIVE_ERROR;

		this.observers = new ArrayList<ProgressObserver>();
		
		this.solutionStorageRelativeCutoff = solutionStorageRelativeCutoff;
		
		//this.rho = rho;
		this.maxThreads = maxThreads;
		
		this.properties = new SparseOptimizationInformation(maxSum, positiveOnly);
	}

	public MultiOrthogonalMatchingPursuit(int numTopSolutions)
	{
		this(false, numTopSolutions, false);
	}
	
	public MultiOrthogonalMatchingPursuit(int numTopSolutions, boolean positiveOnly)
	{
		this(numTopSolutions, positiveOnly, DEFAULT_MAX_SUM);
	}
	
	public MultiOrthogonalMatchingPursuit(int numTopSolutions, boolean positiveOnly, double maxSum)
	{
		this(numTopSolutions, positiveOnly, maxSum, DEFAULT_SOLUTION_STORAGE_RELATIVE_CUTOFF);
	}
	
	public MultiOrthogonalMatchingPursuit(int numTopSolutions, boolean positiveOnly, double maxSum, double solutionStorageRelativeCutoff)
	{
		this(numTopSolutions, positiveOnly, maxSum, solutionStorageRelativeCutoff, Integer.MAX_VALUE);
	}

	public MultiOrthogonalMatchingPursuit(int numTopSolutions, boolean positiveOnly, double maxSum, double solutionStorageRelativeCutoff, int maxThreads)
	{
		this(false, numTopSolutions, positiveOnly, maxSum, solutionStorageRelativeCutoff, maxThreads);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ProgressObservable#addProgressObserver(edu.umd.umiacs.armor.util.ProgressObserver)
	 */
	@Override
	public void addProgressObserver(ProgressObserver o)
	{
		this.observers.add(o);
	}
	
	public Collection<ColumnGuess> bestSolutionGuesses(final double[] y, final double yNorm, final ArrayList<OneColumnSolutionUpdater> prevIterSolutionList, final int maxTopSolutions)
	{
	 	//find best possible reduction directions
  	final LimitedSizeCollection<ColumnGuess> bestResults = new LimitedSizeCollection<ColumnGuess>(maxTopSolutions);  			

  	//get number of cores
  	final int cores = Runtime.getRuntime().availableProcessors();
  	final int numberOfThreads = Math.min(this.maxThreads, Math.min(cores*2, prevIterSolutionList.size()));
  	
  	//create the threading service
		ExecutorService execSvc = Executors.newFixedThreadPool(numberOfThreads);

		for (int thread=0; thread<numberOfThreads; thread++)
		{
			final int threadNumber = thread;
			
			Runnable oneThreadWork = new Runnable()
			{
				@Override
				public void run()
				{
					double largestNorm = Double.POSITIVE_INFINITY;
			  	for (int solutionIndex=threadNumber; solutionIndex<prevIterSolutionList.size(); solutionIndex+=numberOfThreads)
					{
						final OneColumnSolutionUpdater prevSolution = prevIterSolutionList.get(solutionIndex);
						
						final double[] residualNorms = prevSolution.bestColumnsResidualNorms(y,yNorm);
						
				    //update the top solutions, by combining all the residuals
						for (int iter=0; iter<residualNorms.length; iter++)
						{
							//generate the guess
							final double norm = residualNorms[iter];
						  final ColumnGuess guess = new ColumnGuess(solutionIndex, iter, norm);
							
							//add to the queue
						  if (Double.isNaN(norm) || Double.isInfinite(norm))
						  {
						  	//do nothing
						  }
						  else
							if (norm<largestNorm)
							{
								synchronized(bestResults)
								{
									//add the element and update residual
									bestResults.add(guess);
									if (bestResults.isFull() && !bestResults.isEmpty())
										largestNorm = bestResults.getWorst().residual;									
								}
							}
						}
					}
				}
			};
			
			if (numberOfThreads==1)
				oneThreadWork.run();
			else
				execSvc.execute(oneThreadWork);
		}
  			
    //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(365L, TimeUnit.DAYS);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new OptimizationRuntimeException("Unable to finish all tasks.", e);
    }
    
    //combine the positive and negative queues
    ArrayList<ColumnGuess> combinedList = new ArrayList<ColumnGuess>(maxTopSolutions);
    if (bestResults!=null)
    	combinedList.addAll(bestResults);
    combinedList.addAll(bestResults);
    
    return combinedList;
	}
	
	private Set<OneColumnSolutionUpdater> computeColumnUpdate(final double[] y, final double yNorm, ArrayList<OneColumnSolutionUpdater> neededPrevSolutionList, Collection<ColumnGuess> bestGuesses)
	{
  	//get number of cores
  	int cores = Runtime.getRuntime().availableProcessors();
  	final int numberOfThreads = Math.min(this.maxThreads, Math.min(cores*2, bestGuesses.size()));
  	final double relError = this.epsilon;
  	
  	//create the threading service
		ExecutorService execSvc = Executors.newFixedThreadPool(numberOfThreads);
		
		//init the lists, depending on number of threads
		final Set<OneColumnSolutionUpdater> bestSolutionHashList;
		if (numberOfThreads>=1)
			bestSolutionHashList= new HashSet<OneColumnSolutionUpdater>(bestGuesses.size());
		else
			bestSolutionHashList= Collections.synchronizedSet(new HashSet<OneColumnSolutionUpdater>(bestGuesses.size()));
		
		HashSet<Indicies> existingColumns = new HashSet<Indicies>(bestGuesses.size());
		Iterator<OneColumnSolutionUpdater> qrIterator = neededPrevSolutionList.iterator();
		
		for (final ColumnGuess result : bestGuesses)
		{
			//get the previous solution
			final OneColumnSolutionUpdater prevSolution = qrIterator.next();
			
			//generate an updated column list, to see if it already exists
			Indicies newColumns = new Indicies(prevSolution.getColumnsRef(), result.column);

			//add the column
			boolean added = existingColumns.add(newColumns);
			
			//if the column list does not exist perform a rank one update of x
			if (added)
			{
				if (numberOfThreads>=1)
				{
					OneColumnSolutionUpdater newSolution = prevSolution.oneColumnUpdate(result.column, y, yNorm, relError*.1);

					if (newSolution!=null)
						bestSolutionHashList.add(newSolution);					
				}
				else
				{
					Runnable oneThreadWork = new Runnable()
					{
						@Override
						public void run()
						{
							OneColumnSolutionUpdater newSolution = prevSolution.oneColumnUpdate(result.column, y, yNorm, relError*.1);

							//if has constrained solution add it list
							if (newSolution!=null)
								bestSolutionHashList.add(newSolution);
						}
					};
					
					execSvc.execute(oneThreadWork);
				}
			}
		}
			
	  //shutdown the service
	  execSvc.shutdown();
	  try
		{
			execSvc.awaitTermination(365L, TimeUnit.DAYS);
		} 
	  catch (InterruptedException e) 
	  {
	  	execSvc.shutdownNow();
	  	throw new OptimizationRuntimeException("Unable to finish all tasks.", e);
	  }
		
		return bestSolutionHashList;
	}
	
	private SparseLinearSolution formSolution(OneColumnSolutionUpdater solution, double yNorm)
	{
		//store the absolute error
		double error = BasicMath.norm(solution.getResidualRef())*yNorm;
			
		SparseLinearSolution sparseSolution = new SparseLinearSolution(
				error, solution.getUnscaledX(yNorm), solution.getColumnsRef(), solution.properties.numColumns());
		
		return sparseSolution;
	}
	
	@Override
	public double getRelativeErrorTol()
	{
		return this.epsilon;
	}
	
	@Override
	public boolean isNonNegativeOnly()
	{
		return this.properties.isPositiveOnly();
	}
	
	@Override
	public double maxSum()
	{
		return this.properties.getMaxSum();
	}
	
	@Override
	public int maxThreads()
	{
		return this.maxThreads;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ProgressObservable#removeProgressObserver(edu.umd.umiacs.armor.util.ProgressObserver)
	 */
	@Override
	public void removeProgressObserver(ProgressObserver o)
	{
		this.observers.remove(o);
	}
		
	@Override
	public void setA(RealMatrix A)
	{
		this.properties.setA(A);		
	}
	
	@Override
	public void setRelativeErrorTol(double errTol)
	{
		this.epsilon = errTol;
	}

	@Override
	public double[] solve(double[] y)
	{
		LinearSolutions solutions = solveAlt(y, Integer.MAX_VALUE, 1.0e-16);
						
		return solutions.getBest().getX();
	}
	
	public double[] solve(double[] y, int maxL0)
	{
		LinearSolutions solutions = solveAlt(y, maxL0, 1.0e-16);
		
		if (solutions.isEmpty())
			return null;
		
		return solutions.getBest().getX();
	}

	@Override
	public LinearSolutions solveAlt(double[] y)
	{
		return solveAlt(y, Integer.MAX_VALUE);
	}
	
	@Override
	public LinearSolutions solveAlt(double[] y, int maxL0)
	{
		return solveAlt(y, maxL0, this.solutionStorageRelativeCutoff);		
	}

	private LinearSolutions solveAlt(double[] y, int maxL0, double topStorageCutoff)
	{
		if (this.properties.matrixIsNull())
			throw new MathRuntimeException("Matrix A must be set.");
		
		if (y.length!=this.properties.numRows())
			throw new MathRuntimeException("Length of y must match number or rows in A.");
		
		//set the maximum number of iterations
		maxL0 = Math.min(maxL0, Math.min(this.properties.numRows(), this.properties.numColumns()));
		
		//normalize y
		double[] yNormalized = y.clone();
		double yNorm = BasicMath.norm(yNormalized);
		for (int row=0; row<yNormalized.length; row++)
			yNormalized[row] = yNormalized[row]/yNorm;
								
		//recording of solution
		ArrayList<LimitedSizeCollection<LinearSolution>> l0Solutions = new ArrayList<LimitedSizeCollection<LinearSolution>>();		
		
		//create an empty solution list, l0=0
		ArrayList<OneColumnSolutionUpdater> possibleSolutionList = new ArrayList<OneColumnSolutionUpdater>();
		
		//select the type of updater that you want
		if (this.useCG)
			possibleSolutionList.add(new CGOneColumnSolutionUpdater(yNormalized, this.properties));
		else
			possibleSolutionList.add(new QROneColumnSolutionUpdater(yNormalized, this.properties));

		//init the variables
		int maxTopSolutions = this.numTopSolutions;
		double previousNorm = Double.MAX_VALUE;
		double currentNorm = yNorm;
		int l0 = 1;

		//all loop exits are relative values
		while (l0<=maxL0 &&
				(currentNorm/yNorm)>this.epsilon && 
				((previousNorm-currentNorm)/yNorm)>this.epsilon && 
				!possibleSolutionList.isEmpty())
		{
			//notify the observers
			updateProgress(l0);
			
			//compute max solution storage size
			int maxStorageSize = Math.max(1,MAX_SOLUTION_STORED/l0);
						
			//update the other queue sizes
			for (LimitedSizeCollection<LinearSolution> l0TopList : l0Solutions)
				l0TopList.setSize(maxStorageSize);

			//allocate storage for this norm
			l0Solutions.add(new LimitedSizeCollection<LinearSolution>(maxStorageSize));
			
			//solve for the solutions
			//all computations are done on normalized y column
			boolean memAdjusted = true;
			while(memAdjusted)
			{
				try
				{
					//attempt to run the software, if fail try to shrink list
					possibleSolutionList = solveHelperIteration(yNormalized, yNorm, possibleSolutionList, maxTopSolutions);
					memAdjusted = false;
				}
				catch (OutOfMemoryError e)
				{
					//cut the number of storage by two
					if (maxTopSolutions>=100)
					{
						maxTopSolutions = maxTopSolutions/2;
						
						System.err.println("Out of memory. Trying to shrink the number of solutions per iteration to "+maxTopSolutions+".");
						
						//remove from the tail end
						for (int iter=possibleSolutionList.size()-1; iter>=maxTopSolutions; iter--)
							possibleSolutionList.remove(iter);
							
						memAdjusted = true;
					}
					else
						throw e;
				}
			}
			
			//if no longer can find any good vectors
			if (possibleSolutionList==null || possibleSolutionList.isEmpty())
				break;
			
			//add the solution to total solution list
			for (OneColumnSolutionUpdater solution : possibleSolutionList)
			{
				//generate the solution
				SparseLinearSolution topSparseSolution = formSolution(solution, yNorm);
				
				//compute the current l0-norm
				int l0Value = topSparseSolution.l0Norm();
				if (l0Value<=0)
					continue;
				
				//get the sorted queue for the solutions
				LimitedSizeCollection<LinearSolution> l0priorityQueue = l0Solutions.get(l0Value-1);
				
				//add the element needs to be added
				if (l0priorityQueue.isEmpty() || topSparseSolution.absoluteError()<(l0priorityQueue.getBest().absoluteError()+yNorm*topStorageCutoff))
					l0priorityQueue.add(topSparseSolution);
			}
			
			//update the current progress norm
			previousNorm = currentNorm;
			currentNorm = l0Solutions.get(l0-1).getBest().absoluteError();
			
			//update the iteration count
			l0++;
		}
		
		//create the final solution list, by performing final filter of solutions
		ArrayList<LinearSolution> solutions = new ArrayList<LinearSolution>();
		for (LimitedSizeCollection<LinearSolution> l0TopList : l0Solutions)
		{
			if (!l0TopList.isEmpty())
			{
				double maxError = l0TopList.getBest().absoluteError()+yNorm*topStorageCutoff;
				for (LinearSolution sol : l0TopList)
					if (sol.absoluteError()<maxError)
						solutions.add(sol);
			}
		}
		
		return new LinearSolutions(solutions);
	}

	private ArrayList<OneColumnSolutionUpdater> solveHelperIteration(final double[] y, double yNorm, ArrayList<OneColumnSolutionUpdater> prevIterSolutionList, int maxTopSolutions)
	{  	
		Collection<ColumnGuess> bestGuesses = bestSolutionGuesses(y, yNorm, prevIterSolutionList, maxTopSolutions);
		
		//check if no good values found
		if (bestGuesses.isEmpty())
		  return null;
		
		//store only the needed previous iterations
		ArrayList<OneColumnSolutionUpdater> neededPrevSolutionList = new ArrayList<OneColumnSolutionUpdater>(bestGuesses.size());
		for (ColumnGuess result : bestGuesses)
			neededPrevSolutionList.add(prevIterSolutionList.get(result.prevSolutionIndex));
		
		//perform a least-squares update to all the possible solutions
		Set<OneColumnSolutionUpdater> bestSolutionHashList = computeColumnUpdate(y, yNorm, neededPrevSolutionList, bestGuesses);
		
		//move the hashtable list to an ArrayList structure
		ArrayList<OneColumnSolutionUpdater> bestSolutions = new ArrayList<OneColumnSolutionUpdater>(bestSolutionHashList);
		
		//sort the solutions in terms of residual norm
		Collections.sort(bestSolutions);

		return bestSolutions;
	}

	private void updateProgress(int l0)
	{
		for (ProgressObserver o : this.observers)
			o.update((int)l0);
	}
}
