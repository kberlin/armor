/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.linear.SingularValueDecomposition;

import edu.umd.umiacs.armor.math.BasicMath;

public final class QRDecompositionOverdetermined
{
	private final RealMatrix Q1;
	private final RealMatrix R1;
	
	private static RealMatrix solve(RealMatrix Q1, RealMatrix R1, RealMatrix b)
	{
		org.apache.commons.math3.linear.RealMatrix r1 = R1.getApacheCommonsMatrix();
		org.apache.commons.math3.linear.RealMatrix q1t = Q1.getApacheCommonsMatrix().transpose();
		
		RealVector c1 = q1t.multiply(b.getApacheCommonsMatrix()).getColumnVector(0);
		
		try
		{
			MatrixUtils.solveUpperTriangularSystem(r1, c1);
		}
		catch (org.apache.commons.math3.exception.MathArithmeticException e)
		{
			//numerically unstable, so compute SVD solution
			c1 = new SingularValueDecomposition(r1).getSolver().solve(c1);
		}
		
		return new BlockRealMatrix(c1.toArray());
	}
	
	public QRDecompositionOverdetermined(QRDecomposition qrDecomposition)
	{
		this(qrDecomposition.getQ(), qrDecomposition.getR());
	}
			
	public QRDecompositionOverdetermined(RealMatrix Q, RealMatrix R)
	{
		org.apache.commons.math3.linear.RealMatrix r = R.getApacheCommonsMatrix();
		org.apache.commons.math3.linear.RealMatrix r1 = r.getSubMatrix(0, r.getColumnDimension()-1, 0, r.getColumnDimension()-1);
		
		org.apache.commons.math3.linear.RealMatrix q = Q.getApacheCommonsMatrix();
		org.apache.commons.math3.linear.RealMatrix q1 = q.getSubMatrix(0, q.getRowDimension()-1, 0, r.getColumnDimension()-1);
		
		this.Q1 = new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(q1);
		this.R1 = new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(r1);
	}
		
	private QRDecompositionOverdetermined(RealMatrix Q1, RealMatrix R1, boolean byReference)
	{
		this.Q1 = Q1;
		this.R1 = R1;
	}
	
	public QRDecompositionOverdetermined columnUpdate(double[] column)
	{
		org.apache.commons.math3.linear.RealMatrix Q1impl = this.Q1.getApacheCommonsMatrix();
			
		//compute dot product
		double[] qDotA = new double[this.Q1.numColumns()+1];
		qDotA[qDotA.length-1] = 0.0;
		for (int n=0; n<this.Q1.numColumns(); n++)
		{
			qDotA[n] = 0.0;
			for (int m=0; m<column.length; m++)
				qDotA[n] += Q1impl.getEntry(m, n)*column[m];
		}
		
		double[] qnew = new double[column.length];
		for (int m=0; m<column.length; m++)
		{
			qnew[m] = column[m];
			for (int n=0; n<this.Q1.numColumns(); n++)
				qnew[m] -= Q1impl.getEntry(m, n)*qDotA[n];
		}
		
		double qNewNorm = BasicMath.norm(qnew);		
		for (int m=0; m<column.length; m++)
			qnew[m] = qnew[m]/qNewNorm;
		
		RealMatrix Q1new = this.Q1.concatMatrixColumns(new BlockRealMatrix(qnew));
		
		//create the R matrix
		org.apache.commons.math3.linear.Array2DRowRealMatrix R1impl = new org.apache.commons.math3.linear.Array2DRowRealMatrix(this.R1.numRows()+1, this.R1.numColumns()+1);
		
		//copy over the previous matrix
		for (int row=0; row<this.R1.numRows(); row++)
			for (int col=0; col<this.R1.numColumns(); col++)
				R1impl.setEntry(row, col, this.R1.get(row, col));
		
		//set last row to 0
		for (int col=0; col<this.R1.numColumns(); col++)
			R1impl.setEntry(this.R1.numRows(), col, 0.0);
		
		//set the last column
		for (int row=0; row<this.R1.numRows(); row++)
			R1impl.setEntry(row, this.R1.numColumns(), qDotA[row]);
		
		//compute last entry in the new R1
		R1impl.setEntry(this.R1.numRows(), this.R1.numColumns(), qNewNorm);
				
		return new QRDecompositionOverdetermined(Q1new, new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(R1impl), true);
	}
	
	public RealMatrix getQ1()
	{
		return this.Q1;
	}
	
	public RealMatrix getR1()
	{
		return this.R1;
	}
		
	public RealMatrix mult(RealMatrix B)
	{
		RealMatrix C = this.R1.mult(B);
		return this.Q1.mult(C);
	}
	
	public RealMatrix solve(RealMatrix b)
	{
		return solve(this.Q1, this.R1, b);	
	}
}
