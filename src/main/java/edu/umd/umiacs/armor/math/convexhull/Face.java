/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.convexhull;

/*
 * Copyright John E. Lloyd, 2003. All rights reserved. Permission
 * to use, copy, and modify, without fee, is granted for non-commercial 
 * and research purposes, provided that this copyright notice appears 
 * in all copies.
 *
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 */

/**
 * The Class Face.
 */
class Face
{
	
	/** The area. */
	double area;
	
	/** The centroid. */
	private ConvexHullPoint3d centroid;
	
	/** The he0. */
	HalfEdge he0;
	
	/** The index. */
	int index;
	
	/** The mark. */
	int mark = VISIBLE;
	
	/** The next. */
	Face next;
	
	/** The normal. */
	private Vector3d normal;

	/** The num verts. */
	int numVerts;

	/** The outside. */
	Vertex outside;
	
	/** The plane offset. */
	double planeOffset;
	
	/** The Constant DELETED. */
	static final int DELETED = 3;

	/** The Constant NON_CONVEX. */
	static final int NON_CONVEX = 2;

	/** The Constant VISIBLE. */
	static final int VISIBLE = 1;

	/**
	 * Creates the.
	 * 
	 * @param vtxArray
	 *          the vtx array
	 * @param indices
	 *          the indices
	 * @return the face
	 */
	public static Face create(Vertex[] vtxArray, int[] indices)
	{
		Face face = new Face();
		HalfEdge hePrev = null;
		for (int i = 0; i < indices.length; i++)
		{
			HalfEdge he = new HalfEdge(vtxArray[indices[i]], face);
			if (hePrev != null)
			{
				he.setPrev(hePrev);
				hePrev.setNext(he);
			} else
			{
				face.he0 = he;
			}
			hePrev = he;
		}
		face.he0.setPrev(hePrev);
		hePrev.setNext(face.he0);

		// compute the normal and offset
		face.computeNormalAndCentroid();
		return face;
	}

	/**
	 * Creates the triangle.
	 * 
	 * @param v0
	 *          the v0
	 * @param v1
	 *          the v1
	 * @param v2
	 *          the v2
	 * @return the face
	 */
	public static Face createTriangle(Vertex v0, Vertex v1, Vertex v2)
	{
		return createTriangle(v0, v1, v2, 0);
	}

	/**
	 * Creates the triangle.
	 * 
	 * @param v0
	 *          the v0
	 * @param v1
	 *          the v1
	 * @param v2
	 *          the v2
	 * @param minArea
	 *          the min area
	 * @return the face
	 */
	public static Face createTriangle(Vertex v0, Vertex v1, Vertex v2, double minArea)
	{
		Face face = new Face();
		HalfEdge he0 = new HalfEdge(v0, face);
		HalfEdge he1 = new HalfEdge(v1, face);
		HalfEdge he2 = new HalfEdge(v2, face);

		he0.prev = he2;
		he0.next = he1;
		he1.prev = he0;
		he1.next = he2;
		he2.prev = he1;
		he2.next = he0;

		face.he0 = he0;

		// compute the normal and offset
		face.computeNormalAndCentroid(minArea);
		return face;
	}

	/**
	 * Instantiates a new face.
	 */
	public Face()
	{
		this.normal = new Vector3d();
		this.centroid = new ConvexHullPoint3d();
		this.mark = VISIBLE;
	}

	/**
	 * Area squared.
	 * 
	 * @param hedge0
	 *          the hedge0
	 * @param hedge1
	 *          the hedge1
	 * @return the double
	 */
	@SuppressWarnings("unused")
	private double areaSquared(HalfEdge hedge0, HalfEdge hedge1)
	{
		// return the squared area of the triangle defined
		// by the half edge hedge0 and the point at the
		// head of hedge1.

		ConvexHullPoint3d p0 = hedge0.tail().pnt;
		ConvexHullPoint3d p1 = hedge0.head().pnt;
		ConvexHullPoint3d p2 = hedge1.head().pnt;

		double dx1 = p1.x - p0.x;
		double dy1 = p1.y - p0.y;
		double dz1 = p1.z - p0.z;

		double dx2 = p2.x - p0.x;
		double dy2 = p2.y - p0.y;
		double dz2 = p2.z - p0.z;

		double x = dy1 * dz2 - dz1 * dy2;
		double y = dz1 * dx2 - dx1 * dz2;
		double z = dx1 * dy2 - dy1 * dx2;

		return x * x + y * y + z * z;
	}

	/**
	 * Check consistency.
	 */
	void checkConsistency()
	{
		// do a sanity check on the face
		HalfEdge hedge = this.he0;
		double maxd = 0;
		int numv = 0;

		if (this.numVerts < 3)
		{
			throw new InternalErrorException("degenerate face: " + getVertexString());
		}
		do
		{
			HalfEdge hedgeOpp = hedge.getOpposite();
			if (hedgeOpp == null)
			{
				throw new InternalErrorException("face " + getVertexString() + ": " + "unreflected half edge "
						+ hedge.getVertexString());
			} else if (hedgeOpp.getOpposite() != hedge)
			{
				throw new InternalErrorException("face " + getVertexString() + ": " + "opposite half edge "
						+ hedgeOpp.getVertexString() + " has opposite " + hedgeOpp.getOpposite().getVertexString());
			}
			if (hedgeOpp.head() != hedge.tail() || hedge.head() != hedgeOpp.tail())
			{
				throw new InternalErrorException("face " + getVertexString() + ": " + "half edge " + hedge.getVertexString()
						+ " reflected by " + hedgeOpp.getVertexString());
			}
			Face oppFace = hedgeOpp.face;
			if (oppFace == null)
			{
				throw new InternalErrorException("face " + getVertexString() + ": " + "no face on half edge "
						+ hedgeOpp.getVertexString());
			} else if (oppFace.mark == DELETED)
			{
				throw new InternalErrorException("face " + getVertexString() + ": " + "opposite face "
						+ oppFace.getVertexString() + " not on hull");
			}
			double d = Math.abs(distanceToPlane(hedge.head().pnt));
			if (d > maxd)
			{
				maxd = d;
			}
			numv++;
			hedge = hedge.next;
		} while (hedge != this.he0);

		if (numv != this.numVerts)
		{
			throw new InternalErrorException("face " + getVertexString() + " numVerts=" + this.numVerts + " should be " + numv);
		}

	}

	/**
	 * Compute centroid.
	 * 
	 * @param centroid
	 *          the centroid
	 */
	public void computeCentroid(ConvexHullPoint3d centroid)
	{
		centroid.setZero();
		HalfEdge he = this.he0;
		do
		{
			centroid.add(he.head().pnt);
			he = he.next;
		} while (he != this.he0);
		centroid.scale(1 / (double) this.numVerts);
	}

	/**
	 * Compute normal.
	 * 
	 * @param normal
	 *          the normal
	 */
	public void computeNormal(Vector3d normal)
	{
		HalfEdge he1 = this.he0.next;
		HalfEdge he2 = he1.next;

		ConvexHullPoint3d p0 = this.he0.head().pnt;
		ConvexHullPoint3d p2 = he1.head().pnt;

		double d2x = p2.x - p0.x;
		double d2y = p2.y - p0.y;
		double d2z = p2.z - p0.z;

		normal.setZero();

		this.numVerts = 2;

		while (he2 != this.he0)
		{
			double d1x = d2x;
			double d1y = d2y;
			double d1z = d2z;

			p2 = he2.head().pnt;
			d2x = p2.x - p0.x;
			d2y = p2.y - p0.y;
			d2z = p2.z - p0.z;

			normal.x += d1y * d2z - d1z * d2y;
			normal.y += d1z * d2x - d1x * d2z;
			normal.z += d1x * d2y - d1y * d2x;

			he1 = he2;
			he2 = he2.next;
			this.numVerts++;
		}
		this.area = normal.norm();
		normal.scale(1 / this.area);
	}

	/**
	 * Compute normal.
	 * 
	 * @param normal
	 *          the normal
	 * @param minArea
	 *          the min area
	 */
	public void computeNormal(Vector3d normal, double minArea)
	{
		computeNormal(normal);

		if (this.area < minArea)
		{
			//System.out.println("area=" + this.area);
			// make the normal more robust by removing
			// components parallel to the longest edge

			HalfEdge hedgeMax = null;
			double lenSqrMax = 0;
			HalfEdge hedge = this.he0;
			do
			{
				double lenSqr = hedge.lengthSquared();
				if (lenSqr > lenSqrMax)
				{
					hedgeMax = hedge;
					lenSqrMax = lenSqr;
				}
				hedge = hedge.next;
			} while (hedge != this.he0);

			ConvexHullPoint3d p2 = hedgeMax.head().pnt;
			ConvexHullPoint3d p1 = hedgeMax.tail().pnt;
			double lenMax = Math.sqrt(lenSqrMax);
			double ux = (p2.x - p1.x) / lenMax;
			double uy = (p2.y - p1.y) / lenMax;
			double uz = (p2.z - p1.z) / lenMax;
			double dot = normal.x * ux + normal.y * uy + normal.z * uz;
			normal.x -= dot * ux;
			normal.y -= dot * uy;
			normal.z -= dot * uz;

			normal.normalize();
		}
	}

	/**
	 * Compute normal and centroid.
	 */
	private void computeNormalAndCentroid()
	{
		computeNormal(this.normal);
		computeCentroid(this.centroid);
		this.planeOffset = this.normal.dot(this.centroid);
		int numv = 0;
		HalfEdge he = this.he0;
		do
		{
			numv++;
			he = he.next;
		} while (he != this.he0);
		if (numv != this.numVerts)
		{
			throw new InternalErrorException("face " + getVertexString() + " numVerts=" + this.numVerts + " should be " + numv);
		}
	}

	/**
	 * Compute normal and centroid.
	 * 
	 * @param minArea
	 *          the min area
	 */
	private void computeNormalAndCentroid(double minArea)
	{
		computeNormal(this.normal, minArea);
		computeCentroid(this.centroid);
		this.planeOffset = this.normal.dot(this.centroid);
	}

	/**
	 * Connect half edges.
	 * 
	 * @param hedgePrev
	 *          the hedge prev
	 * @param hedge
	 *          the hedge
	 * @return the face
	 */
	private Face connectHalfEdges(HalfEdge hedgePrev, HalfEdge hedge)
	{
		Face discardedFace = null;

		if (hedgePrev.oppositeFace() == hedge.oppositeFace())
		{ // then there is a redundant edge that we can get rid off

			Face oppFace = hedge.oppositeFace();
			HalfEdge hedgeOpp;

			if (hedgePrev == this.he0)
			{
				this.he0 = hedge;
			}
			if (oppFace.numVertices() == 3)
			{ // then we can get rid of the opposite face altogether
				hedgeOpp = hedge.getOpposite().prev.getOpposite();

				oppFace.mark = DELETED;
				discardedFace = oppFace;
			} else
			{
				hedgeOpp = hedge.getOpposite().next;

				if (oppFace.he0 == hedgeOpp.prev)
				{
					oppFace.he0 = hedgeOpp;
				}
				hedgeOpp.prev = hedgeOpp.prev.prev;
				hedgeOpp.prev.next = hedgeOpp;
			}
			hedge.prev = hedgePrev.prev;
			hedge.prev.next = hedge;

			hedge.opposite = hedgeOpp;
			hedgeOpp.opposite = hedge;

			// oppFace was modified, so need to recompute
			oppFace.computeNormalAndCentroid();
		} else
		{
			hedgePrev.next = hedge;
			hedge.prev = hedgePrev;
		}
		return discardedFace;
	}

	/**
	 * Distance to plane.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
	public double distanceToPlane(ConvexHullPoint3d p)
	{
		return this.normal.x * p.x + this.normal.y * p.y + this.normal.z * p.z - this.planeOffset;
	}

	/**
	 * Find edge.
	 * 
	 * @param vt
	 *          the vt
	 * @param vh
	 *          the vh
	 * @return the half edge
	 */
	public HalfEdge findEdge(Vertex vt, Vertex vh)
	{
		HalfEdge he = this.he0;
		do
		{
			if (he.head() == vh && he.tail() == vt)
			{
				return he;
			}
			he = he.next;
		} while (he != this.he0);
		return null;
	}

	/**
	 * Gets the centroid.
	 * 
	 * @return the centroid
	 */
	public ConvexHullPoint3d getCentroid()
	{
		return this.centroid;
	}

	/**
	 * Gets the edge.
	 * 
	 * @param i
	 *          the i
	 * @return the edge
	 */
	public HalfEdge getEdge(int i)
	{
		HalfEdge he = this.he0;
		while (i > 0)
		{
			he = he.next;
			i--;
		}
		while (i < 0)
		{
			he = he.prev;
			i++;
		}
		return he;
	}

	/**
	 * Gets the first edge.
	 * 
	 * @return the first edge
	 */
	public HalfEdge getFirstEdge()
	{
		return this.he0;
	}

	/**
	 * Gets the normal.
	 * 
	 * @return the normal
	 */
	public Vector3d getNormal()
	{
		return this.normal;
	}

	/**
	 * Gets the vertex indices.
	 * 
	 * @param idxs
	 *          the idxs
	 * @return the vertex indices
	 */
	public void getVertexIndices(int[] idxs)
	{
		HalfEdge he = this.he0;
		int i = 0;
		do
		{
			idxs[i++] = he.head().index;
			he = he.next;
		} while (he != this.he0);
	}

	/**
	 * Gets the vertex string.
	 * 
	 * @return the vertex string
	 */
	public String getVertexString()
	{
		String s = null;
		HalfEdge he = this.he0;
		do
		{
			if (s == null)
			{
				s = "" + he.head().index;
			} else
			{
				s += " " + he.head().index;
			}
			he = he.next;
		} while (he != this.he0);
		return s;
	}

	/**
	 * Merge adjacent face.
	 * 
	 * @param hedgeAdj
	 *          the hedge adj
	 * @param discarded
	 *          the discarded
	 * @return the int
	 */
	public int mergeAdjacentFace(HalfEdge hedgeAdj, Face[] discarded)
	{
		Face oppFace = hedgeAdj.oppositeFace();
		int numDiscarded = 0;

		discarded[numDiscarded++] = oppFace;
		oppFace.mark = DELETED;

		HalfEdge hedgeOpp = hedgeAdj.getOpposite();

		HalfEdge hedgeAdjPrev = hedgeAdj.prev;
		HalfEdge hedgeAdjNext = hedgeAdj.next;
		HalfEdge hedgeOppPrev = hedgeOpp.prev;
		HalfEdge hedgeOppNext = hedgeOpp.next;

		while (hedgeAdjPrev.oppositeFace() == oppFace)
		{
			hedgeAdjPrev = hedgeAdjPrev.prev;
			hedgeOppNext = hedgeOppNext.next;
		}

		while (hedgeAdjNext.oppositeFace() == oppFace)
		{
			hedgeOppPrev = hedgeOppPrev.prev;
			hedgeAdjNext = hedgeAdjNext.next;
		}

		HalfEdge hedge;

		for (hedge = hedgeOppNext; hedge != hedgeOppPrev.next; hedge = hedge.next)
		{
			hedge.face = this;
		}

		if (hedgeAdj == this.he0)
		{
			this.he0 = hedgeAdjNext;
		}

		// handle the half edges at the head
		Face discardedFace;

		discardedFace = connectHalfEdges(hedgeOppPrev, hedgeAdjNext);
		if (discardedFace != null)
		{
			discarded[numDiscarded++] = discardedFace;
		}

		// handle the half edges at the tail
		discardedFace = connectHalfEdges(hedgeAdjPrev, hedgeOppNext);
		if (discardedFace != null)
		{
			discarded[numDiscarded++] = discardedFace;
		}

		computeNormalAndCentroid();
		checkConsistency();

		return numDiscarded;
	}

	/**
	 * Num vertices.
	 * 
	 * @return the int
	 */
	public int numVertices()
	{
		return this.numVerts;
	}

	/**
	 * Triangulate.
	 * 
	 * @param newFaces
	 *          the new faces
	 * @param minArea
	 *          the min area
	 */
	public void triangulate(FaceList newFaces, double minArea)
	{
		HalfEdge hedge;

		if (numVertices() < 4)
		{
			return;
		}

		Vertex v0 = this.he0.head();
		// Face prevFace = null;

		hedge = this.he0.next;
		HalfEdge oppPrev = hedge.opposite;
		Face face0 = null;

		for (hedge = hedge.next; hedge != this.he0.prev; hedge = hedge.next)
		{
			Face face = createTriangle(v0, hedge.prev.head(), hedge.head(), minArea);
			face.he0.next.setOpposite(oppPrev);
			face.he0.prev.setOpposite(hedge.opposite);
			oppPrev = face.he0;
			newFaces.add(face);
			if (face0 == null)
			{
				face0 = face;
			}
		}
		hedge = new HalfEdge(this.he0.prev.prev.head(), this);
		hedge.setOpposite(oppPrev);

		hedge.prev = this.he0;
		hedge.prev.next = hedge;

		hedge.next = this.he0.prev;
		hedge.next.prev = hedge;

		computeNormalAndCentroid(minArea);
		checkConsistency();

		for (Face face = face0; face != null; face = face.next)
		{
			face.checkConsistency();
		}

	}
}
