/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import java.util.ArrayList;
import java.util.Collections;

import org.apache.commons.math3.geometry.euclidean.threed.NotARotationMatrixException;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.MathRuntimeException;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.util.SortType;
import edu.umd.umiacs.armor.util.SortablePair;

/**
 * The Class EigenDecomposition3d.
 */
public final class EigenDecomposition3d implements EigenDecomposition
{
	
	/** The R. */
	private final Rotation R;

	/** The z. */
	private double x, y, z;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9080585228136927202L;

	public EigenDecomposition3d(double x, double y, double z, Rotation R)
	{
		this.x = x;
		this.y = y;
		this.z = z;
		this.R = R;
	}

	public EigenDecomposition3d(SymmetricMatrix3d A)
	{
		EigenDecomposition eig = new EigenDecompositionImpl(A);

		OrthonormalMatrix V = eig.getV();
		Rotation tempR;
		try
		{
			// try the corrent orthogonal matrix
			tempR = new Rotation(V);
		} catch (NotARotationMatrixException e)
		{
			// if the orthogonal matrix is left-handed (aka det=-1)
			// negate one of the columns to make det=1
			ApacheCommonsRealDynamicMatrixWrapper<org.apache.commons.math3.linear.RealMatrix> newVWrapper = new ApacheCommonsRealDynamicMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(V.getApacheCommonsMatrix().copy());
			newVWrapper.negateColumn(2);
			tempR = new Rotation(newVWrapper);
		}
		this.R = tempR;

		this.x = eig.getEigenvalue(0);
		this.y = eig.getEigenvalue(1);
		this.z = eig.getEigenvalue(2);
	}
	
	/**
	 * Compute closest by oriention eigen decomposition.
	 * 
	 * @param eig
	 *          the eig
	 * @return the eigen decomposition3d
	 */
	public EigenDecomposition3d computeClosestOrientionEigenDecomposition(EigenDecomposition3d eig)
	{
		ArrayList<EigenDecomposition3d> eigList = this.generateEquivalentEigenDecompositions();
		
		double bestDistance = Double.POSITIVE_INFINITY; 
		
		int bestIndex = 0;
		int iter =0;
		for (EigenDecomposition3d currEig : eigList)
		{
			double currDist = eig.R.distance(currEig.R);
			if (currDist<bestDistance)
			{
				bestIndex = iter;
				bestDistance = currDist;
			}
			
			iter++;
		}
		
		return eigList.get(bestIndex);
	}
	
	/**
	 * Compute eigenvalues std.
	 * 
	 * @param eigList
	 *          the eig list
	 * @return the double[]
	 */
	public double[] computeEigenvaluesStd(ArrayList<EigenDecomposition3d> eigList)
	{
		double[] eigValuesStd = new double[3];
		eigValuesStd[0] = 0.0;
		eigValuesStd[1] = 0.0;
		eigValuesStd[2] = 0.0;
		for (EigenDecomposition3d eig : eigList)
		{
			eigValuesStd[0] += BasicMath.square(this.x-eig.x);
			eigValuesStd[1] += BasicMath.square(this.y-eig.y);
			eigValuesStd[2] += BasicMath.square(this.z-eig.z);
		}

		eigValuesStd[0] = BasicMath.sqrt(eigValuesStd[0]/(double)(eigList.size()));
		eigValuesStd[1] = BasicMath.sqrt(eigValuesStd[1]/(double)(eigList.size()));
		eigValuesStd[2] = BasicMath.sqrt(eigValuesStd[2]/(double)(eigList.size()));
		
		return eigValuesStd;
	}
	
	/**
	 * Compute eigenvalue std.
	 * 
	 * @param index
	 *          the index
	 * @param eigList
	 *          the eig list
	 * @return the double
	 */
	public double computeEigenvalueStd(int index, ArrayList<EigenDecomposition3d> eigList)
	{
		if (eigList==null || eigList.isEmpty())
			return Double.NaN;
		
		double eigValueStd = 0.0;
		for (EigenDecomposition3d eig : eigList)
			eigValueStd += BasicMath.square(getEigenvalue(index)-eig.getEigenvalue(index));

		eigValueStd = BasicMath.sqrt(eigValueStd/(double)(eigList.size()));
		
		return eigValueStd;
	}
	
	/**
	 * Compute orientational std.
	 * 
	 * @param eigList
	 *          the eig list
	 * @return the double
	 */
	public double computeOrientationalStd(ArrayList<EigenDecomposition3d> eigList)
	{
		if (eigList==null || eigList.isEmpty())
			return Double.NaN;

		double eigValueStd = 0.0;
		for (EigenDecomposition3d eig : eigList)
			eigValueStd += BasicMath.square(orientationalDistance(eig));

		eigValueStd = BasicMath.sqrt(eigValueStd/(double)(eigList.size()));
		
		return eigValueStd;
	}

	/**
	 * Compute zyz angles std.
	 * 
	 * @param eigList
	 *          the eig list
	 * @return the double[]
	 */
	public double[] computeZYZAnglesStd(ArrayList<EigenDecomposition3d> eigList)
	{
		if (eigList==null || eigList.isEmpty())
			return new double[]{Double.NaN, Double.NaN, Double.NaN};
		
		double[] angles = this.R.getAnglesZYZ();
		double[] anglesStd = new double[3];
		anglesStd[0] = 0.0;
		anglesStd[1] = 0.0;
		anglesStd[2] = 0.0;
		for (EigenDecomposition3d eig : eigList)
		{
			double[] bestAngles = eig.computeClosestOrientionEigenDecomposition(this).R.getAnglesZYZ();
			
			anglesStd[0] += BasicMath.square(angles[0]-bestAngles[0]);
			anglesStd[1] += BasicMath.square(angles[1]-bestAngles[1]);
			anglesStd[2] += BasicMath.square(angles[2]-bestAngles[2]);
		}

		anglesStd[0] = BasicMath.sqrt(anglesStd[0]/(double)(eigList.size()));
		anglesStd[1] = BasicMath.sqrt(anglesStd[1]/(double)(eigList.size()));
		anglesStd[2] = BasicMath.sqrt(anglesStd[2]/(double)(eigList.size()));
		
		return anglesStd;
	}

	public EigenDecomposition3d createPermutedEigenDecomposition(int[] permutation)
	{
		if (permutation.length!=3)
			throw new MathRuntimeException("Permuation size must be 3.");
		
		// get the old values
		OrthonormalMatrix originalV = getV();
		double[] oldEigenValues = {this.x,this.y,this.z};

		// sort the rotation matrix and the eigenvalues
		double[] eigValues = new double[oldEigenValues.length];
		RealMatrixDynamic newVWrapper = new ApacheCommonsRealDynamicMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(originalV.getApacheCommonsMatrix().copy());
		for (int iter = 0; iter < eigValues.length; iter++)
		{
			if (permutation[iter]<0 || permutation[iter]>=3)
				throw new MathRuntimeException("Invalid permuation value "+permutation[iter]+".");
			
			int oldIndex = permutation[iter];

			//permute the columns and the eigenvaleus
			eigValues[iter] = oldEigenValues[oldIndex];

			//exchange the columns in the rotation
			if (iter != oldIndex)
			{
				double[] currColumn = originalV.getColumn(oldIndex);
				newVWrapper.setColumn(iter, currColumn);
			}
			
		}

		//make sure the results are always positive in zyz angles
		if (newVWrapper.get(1, 2)<0.0)
		  newVWrapper.negateColumn(2);
		if (newVWrapper.get(2, 1)<0.0)
		  newVWrapper.negateColumn(1);
	
		//make sure the determinate is positive
		double[][] ort = newVWrapper.toArray();
    double det = BasicMath.det(ort);
 
    //flip the unmodified column if it is a left-handed rotation
    if (det<0.0)
		  newVWrapper.negateColumn(0);

    //form the rotation
		Rotation tempR = new Rotation(newVWrapper);
		
		return new EigenDecomposition3d(eigValues[0], eigValues[1], eigValues[2], tempR);
	}
	
	/**
	 * Creates the sorted eigen decomposition.
	 * 
	 * @param sortType
	 *          the sort type
	 * @return the eigen decomposition3d
	 */
	public EigenDecomposition3d createSortedEigenDecomposition(SortType sortType)
	{
		double[] oldEigenValues = getEigenvalues();

		ArrayList<SortablePair<Double, Integer>> pairArray = new ArrayList<SortablePair<Double, Integer>>();
		
		//check if already sorted
		boolean isSorted = true;
		for (int iter = 0; iter < oldEigenValues.length-1; iter++)
		{
			switch (sortType)
			{
				case ASCENDING :
				{
					if (oldEigenValues[iter]>oldEigenValues[iter+1])
						isSorted = false;
					break;
				}
				case DESCENDING :
				{
					if (-oldEigenValues[iter]>-oldEigenValues[iter+1])
						isSorted = false;
					break;
				}
				case ABSOLUTE_ASCENDING :
				{
					if (Math.abs(oldEigenValues[iter])>Math.abs(oldEigenValues[iter+1]))
						isSorted = false;
					break;
				}
				case ABSOLUTE_DESCENDING :
				{
					if (-Math.abs(oldEigenValues[iter])>-Math.abs(oldEigenValues[iter+1]))
						isSorted = false;
					break;
				}
				default :	throw new MathRuntimeException("This sort type is not supported.");
			}
			
			if (!isSorted)
				break;
		}
		
		//if sorted still must check for proper tensor value quadrant
		if (isSorted)
			return createPermutedEigenDecomposition(new int[]{0,1,2});
		
		for (int iter = 0; iter < oldEigenValues.length; iter++)
		{
			if (sortType == SortType.ASCENDING)
				pairArray.add(new SortablePair<Double, Integer>(oldEigenValues[iter], iter));
			else if (sortType == SortType.DESCENDING)
				pairArray.add(new SortablePair<Double, Integer>(-oldEigenValues[iter], iter));
			else if (sortType == SortType.ABSOLUTE_ASCENDING)
				pairArray.add(new SortablePair<Double, Integer>(Math.abs(oldEigenValues[iter]), iter));
			else if (sortType == SortType.ABSOLUTE_DESCENDING)
				pairArray.add(new SortablePair<Double, Integer>(-Math.abs(oldEigenValues[iter]), iter));
			else
				throw new MathRuntimeException("Type of sort for eigenvalues not implemented");
		}

		// sort the values in the right order
		Collections.sort(pairArray);

		return createPermutedEigenDecomposition(new int[]{pairArray.get(0).y,pairArray.get(1).y,pairArray.get(2).y});
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.EigenDecomposition#formMatrix()
	 */
	@Override
	public SymmetricMatrix3d formMatrix()
	{
		SymmetricMatrix3dImpl newMatrix = new SymmetricMatrix3dImpl((this.R.mult(getD()).mult(this.R.inverse())));
		newMatrix.eig = this;

		return newMatrix;
	}

	/**
	 * Generate equivalent eigen decompositions.
	 * 
	 * @return the array list
	 */
	public ArrayList<EigenDecomposition3d> generateEquivalentEigenDecompositions()
	{
		ArrayList<EigenDecomposition3d> list = new ArrayList<EigenDecomposition3d>();

		// get the rotation matrix into a dynamic wrapper
		ApacheCommonsRealDynamicMatrixWrapper<org.apache.commons.math3.linear.RealMatrix> matrixR = new ApacheCommonsRealDynamicMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(this.R.A.copy());

		//the original
		list.add(new EigenDecomposition3d(this.x, this.y, this.z, this.R));

		// add the [0 -1 -1]
		matrixR.negateColumn(1);
		matrixR.negateColumn(2);
		list.add(new EigenDecomposition3d(this.x, this.y, this.z, new Rotation(matrixR)));

		// add the [-1 0 -1]
		matrixR.negateColumn(0);
		matrixR.negateColumn(1);
		list.add(new EigenDecomposition3d(this.x, this.y, this.z, new Rotation(matrixR)));

		// add the [-1 -1 0]
		matrixR.negateColumn(1);
		matrixR.negateColumn(2);
		list.add(new EigenDecomposition3d(this.x, this.y, this.z, new Rotation(matrixR)));

		return list;
	}
	
	/**
	 * Generate equivalent eigen decompositions.
	 * 
	 * @param sortType
	 *          the sort type
	 * @return the array list
	 */
	public ArrayList<EigenDecomposition3d> generateEquivalentEigenDecompositions(SortType sortType)
	{
		ArrayList<EigenDecomposition3d> list = new ArrayList<EigenDecomposition3d>();		
		EigenDecomposition3d eig = createSortedEigenDecomposition(sortType);
		
		list.addAll(eig.generateEquivalentEigenDecompositions());
		
		return list;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.EigenDecomposition#getD()
	 */
	@Override
	public SymmetricMatrix3d getD()
	{
		return new SymmetricMatrix3dImpl(getEigenvalues());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.EigenDecomposition#getEigenvalue(int)
	 */
	@Override
	public double getEigenvalue(int index)
	{
		switch (index)
		{
			case 0:
				return this.x;
			case 1:
				return this.y;
			case 2:
				return this.z;
		}

		return 0;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.EigenDecomposition#getEigenvalues()
	 */
	@Override
	public double[] getEigenvalues()
	{
		return new double[]
		{ this.x, this.y, this.z };
	}

	/**
	 * Gets the rotation.
	 * 
	 * @return the rotation
	 */
	public Rotation getRotation()
	{
		return this.R;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.EigenDecomposition#getV()
	 */
	@Override
	public OrthonormalMatrix getV()
	{
		return this.R;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.EigenDecomposition#getVT()
	 */
	@Override
	public OrthonormalMatrix getVT()
	{
		return this.R.inverse();
	}

	/**
	 * Orientational distance.
	 * 
	 * @param eig
	 *          the eig
	 * @return the double
	 */
	public double orientationalDistance(EigenDecomposition3d eig)
	{
		EigenDecomposition3d bestEig = computeClosestOrientionEigenDecomposition(eig);
		
		return eig.R.distance(bestEig.R);
	}
}
