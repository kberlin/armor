/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.linear;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.MathRuntimeException;

public class ApacheCommonsRealMatrixWrapper<Matrix extends org.apache.commons.math3.linear.RealMatrix> implements RealMatrix
{
	protected final Matrix A;
	/**
	 * 
	 */
	private static final long serialVersionUID = 7676787213938466471L;
	
	protected ApacheCommonsRealMatrixWrapper(ApacheCommonsRealMatrixWrapper<Matrix> A)
	{
		this.A = A.A;
	}

	protected ApacheCommonsRealMatrixWrapper(Matrix A)
	{
		this.A = A;
	}
	
	@Override
	public RealMatrix add(RealMatrix B)
	{
		return new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(this.A.add(B.getApacheCommonsMatrix()));
	}

	@Override
	public RealMatrix concatMatrixColumns(RealMatrix B)
	{
		if (this.numRows()!= B.numRows())
			throw new MathRuntimeException("Number of rows in A ("+this.numRows()+") does not equal number of rows in B ("+B.numRows()+").");
		
		int numColumns = this.A.getColumnDimension();
		org.apache.commons.math3.linear.RealMatrix M = B.getApacheCommonsMatrix();
		
		//create the new matrix
		org.apache.commons.math3.linear.BlockRealMatrix C = new org.apache.commons.math3.linear.BlockRealMatrix(this.A.getRowDimension(), this.A.getColumnDimension()+M.getColumnDimension());
		
		for (int row=0; row<this.A.getRowDimension(); row++)
		{
			//copy over the first matrix columns
			for (int col=0; col<numColumns; col++)
			{
				C.setEntry(row, col, this.A.getEntry(row, col));
			}
			
			//copy over the second matrix
			for (int col=0; col<M.getColumnDimension(); col++)
			{
				C.setEntry(row, numColumns+col, M.getEntry(row, col));
			}
		}
		
		return new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(C);
	}

	@Override
	public double conditionEstimate()
	{
		double sMax = Double.MIN_VALUE;
		double sMin = Double.MAX_VALUE;
		
		for (int i=0; i<Math.min(numColumns(),numRows()); i++)
		{
			sMax = Math.max(sMax,Math.abs(this.A.getEntry(i, i)));
			sMin = Math.min(sMin,Math.abs(this.A.getEntry(i, i)));
		}
		
		if (sMin<0.0)
			return Double.MAX_VALUE;
		
		return sMax/sMin;
	}

	@Override
	public final double[] diag()
	{
		int size = Math.min(numColumns(), numRows());
		
		double[] diag = new double[size];
		
		for (int iter=0; iter<size; iter++)
			diag[iter] = get(iter,iter);
		
		return diag;
	}

	@Override
	public final double get(int i, int j)
	{
		return this.A.getEntry(i, j);
	}

	@Override
	public org.apache.commons.math3.linear.RealMatrix getApacheCommonsMatrix()
	{
		return this.A;
	}

	@Override
	public final double[] getColumn(int index)
	{
		double[] x = new double[numRows()];
		for (int iter=0; iter<numRows(); iter++)
			x[iter] = get(iter, index);
		
		return x;
	}

	@Override
	public RealMatrix getColumns(int[] selectedColumns)
	{
		int[] allrows = new int[this.numRows()];
		for (int iter=0; iter<allrows.length; iter++)
			allrows[iter] = iter;
		
		return getSubMatrix(allrows, selectedColumns);
	}

	@Override
	public final double[] getRow(int index)
	{
		double[] x = new double[numColumns()];
		for (int iter=0; iter<numColumns(); iter++)
			x[iter] = get(index, iter);
		
		return x;
	}

	@Override
	public RealMatrix getSubMatrix(int startRow, int endRow, int startColumn, int endColumn)
	{
		return new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(this.A.getSubMatrix(startRow, endRow, startColumn, endColumn));
	}

	@Override
	public RealMatrix getSubMatrix(int[] selectedRows, int[] selectedColumns)
	{
		return new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(this.A.getSubMatrix(selectedRows, selectedColumns));
	}

	@Override
	public final RealMatrix mult(double b)
	{
		return new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(this.A.scalarMultiply(b));
	}

	@Override
	public double[] mult(double[] b)
	{
		return this.A.operate(b);
	}

	@Override
	public final RealMatrix mult(RealMatrix B)
	{
		return new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(this.A.multiply(B.getApacheCommonsMatrix()));
	}

	@Override
	public double[] multTranspose(double[] b)
	{
		return this.A.preMultiply(b);
	}

	@Override
	public double mutualCoherence()
	{
		double[][] At = transpose().toArray();
		
		double max = 0.0;
		for (int k=0; k<At.length; k++)
			for (int j=k+1; j<At.length; j++)
			{
				double val = Math.abs(BasicMath.dotProduct(At[k], At[j]))/(BasicMath.norm(At[k])*BasicMath.norm(At[j]));
				//double val = (BasicMath.dotProduct(At[k], At[j]))/(BasicMath.norm(At[k])*BasicMath.norm(At[j]));
				
				if (val>max)
					max = val;
			}
		
		return max;
	}

	@Override
	public final double normFrobenius()
	{
		return this.A.getFrobeniusNorm();
	}

	@Override
	public final int numColumns()
	{
		return this.A.getColumnDimension();
	}

	@Override
	public final int numRows()
	{
		return this.A.getRowDimension();
	}

	@Override
	public RealMatrix pseudoInverse()
	{
		return pseudoInverse(1.0e-8);
	}

	@Override
	public RealMatrix pseudoInverse(double eps)
	{
		SingularValueDecomposition svd = svd();
		
		return svd.pseudoInverse(eps);
	}

	@Override
	public RealMatrix pseudoInverseRelative(double relEps)
	{
		SingularValueDecomposition svd = svd();
		
		return svd.pseudoInverse(relEps);
		
	}

	@Override
	public final RealMatrix subtract(RealMatrix B)
	{
		return new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(this.A.subtract(B.getApacheCommonsMatrix()));
	}

	@Override
	public SingularValueDecomposition svd()
	{
		return new SingularValueDecomposition(this);
	}
	
	@Override
	public double[][] toArray()
	{
		return this.A.getData();
	}

	@Override
	public String toMatlab()
	{
		StringBuffer s = new StringBuffer();
		
		s.append("[");
		for (int i=0; i<numRows(); i++)
		{
			for (int j=0; j<numColumns()-1; j++)
			{
				s.append(get(i,j));
				s.append(", ");
			}
			s.append(get(i,numColumns()-1));
			s.append(";");
			if (i<numRows()-1)
				s.append("\n");
		}
		s.append("]");
		
		return s.toString();
	}

	@Override
	public String toPlainText()
	{
		StringBuffer s = new StringBuffer();
		
		for (int i=0; i<numRows(); i++)
		{
			for (int j=0; j<numColumns()-1; j++)
			{
				s.append(get(i,j));
				s.append(" ");
			}
			s.append(get(i,numColumns()-1));
			if (i<numRows()-1)
				s.append("\n");
		}
		
		
		return s.toString();
	}

	@Override
	public String toString()
	{
		StringBuffer s = new StringBuffer();
		
		for (int i=0; i<numRows(); i++)
		{
			s.append("[");
			for (int j=0; j<numColumns()-1; j++)
			{
				s.append(get(i,j));
				s.append(", ");
			}
			s.append(get(i,numColumns()-1));
			s.append("]");
			if (i<numRows()-1)
				s.append("\n");
		}
		
		
		return s.toString();
	}

	@Override
	public double trace()
	{
		return this.A.getTrace();
	}

	@Override
	public RealMatrix transpose()
	{
		return new ApacheCommonsRealMatrixWrapper<org.apache.commons.math3.linear.RealMatrix>(this.A.transpose());
	}
}