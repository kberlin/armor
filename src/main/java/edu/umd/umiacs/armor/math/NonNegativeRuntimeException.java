package edu.umd.umiacs.armor.math;

public class NonNegativeRuntimeException extends MathRuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 399173237798838256L;

	public NonNegativeRuntimeException()
	{
		super();
	}

	public NonNegativeRuntimeException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public NonNegativeRuntimeException(String arg0)
	{
		super(arg0);
	}

	public NonNegativeRuntimeException(Throwable arg0)
	{
		super(arg0);
	}

}
