/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.gpu;

import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLPlatform;

public class GpuContext
{
	private final CLContext context;
	
	private static GpuContext programContext = null;
	
	public static CLContext getCLContext() throws GPUException
	{
		if (programContext == null)
			programContext = new GpuContext();
		
		return programContext.context;
	}
	
	private GpuContext() throws GPUException
	{
		CLPlatform platform = CLPlatform.getDefault();
		
		if (platform.listCLDevices(CLDevice.Type.GPU).length <= 0)
			throw new GPUException("No GPU device found.");
		
		this.context = CLContext.create(platform.getMaxFlopsDevice(CLDevice.Type.GPU));
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable
	{
		try
		{
			this.context.release();
		}
		finally
		{
			super.finalize();
		}
	}
	
//	private static GpuContext programContext = null;
//  /** The context. */
//  protected cl_context context = null;
//  
//  /** The device gpu. */
//  protected cl_device_id deviceGPU = null;
//  
//  private final OpenCLDeviceInfo info;
//  
//  /**
//   * The Class OpenCLDeviceInfo.
//   */
//  public class OpenCLDeviceInfo
//  {   
//    /** The address bits. */
//    final public int addressBits;
//    
//    /** The error correction support. */
//    final public int errorCorrectionSupport;
//    
//    /** The global mem size. */
//    final public long globalMemSize;
//    
//    /** The local mem size. */
//    final public long localMemSize;
//    
//    /** The local mem type. */
//    final public int localMemType;
//    
//    /** The max clock frequency. */
//    final public long maxClockFrequency;
//    
//    /** The max compute units. */
//    final public int maxComputeUnits;
//    
//    /** The max constant buffer size. */
//    final public long maxConstantBufferSize;
//    
//    /** The max mem alloc size. */
//    final public long maxMemAllocSize;
//    
//    /** The max work group size. */
//    final public long maxWorkGroupSize;
//    
//    /** The max work item dimensions. */
//    final public long maxWorkItemDimensions;
//    
//    /** The max work item sizes. */
//    final public long[] maxWorkItemSizes;
//		
//		/** The device name. */
//    final public String deviceName;
//		
//		/** The device vendor. */
//    final public String deviceVendor;
//		
//		/** The driver version. */
//    final public String driverVersion;
//		
//		/** The device type. */
//    final public long deviceType;
//		
//		public OpenCLDeviceInfo(GpuContext context)
//		{
//	    // CL_DEVICE_NAME
//	    deviceName = getString(context.deviceGPU, CL_DEVICE_NAME);
//	
//	    // CL_DEVICE_VENDOR
//	    deviceVendor = getString(context.deviceGPU, CL_DEVICE_VENDOR);
//	
//	    // CL_DRIVER_VERSION
//	    driverVersion = getString(context.deviceGPU, CL_DRIVER_VERSION);
//	
//	    // CL_DEVICE_TYPE
//	    deviceType = getLong(context.deviceGPU, CL_DEVICE_TYPE);
//	    
//	    // CL_DEVICE_MAX_COMPUTE_UNITS
//	    maxComputeUnits = getInt(context.deviceGPU, CL_DEVICE_MAX_COMPUTE_UNITS);
//	    //System.out.printf("CL_DEVICE_MAX_COMPUTE_UNITS:\t\t%d\n", info.maxComputeUnits);
//	
//	    // CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS
//	    maxWorkItemDimensions = getLong(context.deviceGPU, CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS);
//	    //System.out.printf("CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:\t%d\n", info.maxWorkItemDimensions);
//	
//	    // CL_DEVICE_MAX_WORK_ITEM_SIZES
//	    maxWorkItemSizes = getLongs(context.deviceGPU, CL_DEVICE_MAX_WORK_ITEM_SIZES, 3);
//	    //System.out.printf("CL_DEVICE_MAX_WORK_ITEM_SIZES:\t\t%d / %d / %d \n",info.maxWorkItemSizes[0], info.maxWorkItemSizes[1], info.maxWorkItemSizes[2]);
//	
//	    // CL_DEVICE_MAX_WORK_GROUP_SIZE
//	    maxWorkGroupSize = getLong(context.deviceGPU, CL_DEVICE_MAX_WORK_GROUP_SIZE);
//	    //System.out.printf("CL_DEVICE_MAX_WORK_GROUP_SIZE:\t\t%d\n", info.maxWorkGroupSize);
//	
//	    // CL_DEVICE_MAX_CLOCK_FREQUENCY
//	    maxClockFrequency = getLong(context.deviceGPU, CL_DEVICE_MAX_CLOCK_FREQUENCY);
//	    //System.out.printf("CL_DEVICE_MAX_CLOCK_FREQUENCY:\t\t%d MHz\n", info.maxClockFrequency);
//	
//	    // CL_DEVICE_ADDRESS_BITS
//	    addressBits = getInt(context.deviceGPU, CL_DEVICE_ADDRESS_BITS);
//	    //System.out.printf("CL_DEVICE_ADDRESS_BITS:\t\t\t%d\n", info.addressBits);
//	
//	    // CL_DEVICE_MAX_MEM_ALLOC_SIZE
//	    maxMemAllocSize = getLong(context.deviceGPU, CL_DEVICE_MAX_MEM_ALLOC_SIZE);
//	    //System.out.printf("CL_DEVICE_MAX_MEM_ALLOC_SIZE:\t\t%d MByte\n", (int)(info.maxMemAllocSize / (1024 * 1024)));
//	
//	    // CL_DEVICE_GLOBAL_MEM_SIZE
//	    globalMemSize = getLong(context.deviceGPU, CL_DEVICE_GLOBAL_MEM_SIZE);
//	    //System.out.printf("CL_DEVICE_GLOBAL_MEM_SIZE:\t\t%d MByte\n", (int)(info.globalMemSize / (1024 * 1024)));
//	
//	    // CL_DEVICE_ERROR_CORRECTION_SUPPORT
//	    errorCorrectionSupport = getInt(context.deviceGPU, CL_DEVICE_ERROR_CORRECTION_SUPPORT);
//	    //System.out.printf("CL_DEVICE_ERROR_CORRECTION_SUPPORT:\t%s\n", info.errorCorrectionSupport != 0 ? "yes" : "no");
//	
//	    // CL_DEVICE_LOCAL_MEM_TYPE
//	    localMemType = getInt(context.deviceGPU, CL_DEVICE_LOCAL_MEM_TYPE);
//	    //System.out.printf("CL_DEVICE_LOCAL_MEM_TYPE:\t\t%s\n", info.localMemType == 1 ? "local" : "global");
//	
//	    // CL_DEVICE_LOCAL_MEM_SIZE
//	    localMemSize = getLong(context.deviceGPU, CL_DEVICE_LOCAL_MEM_SIZE);
//	    //System.out.printf("CL_DEVICE_LOCAL_MEM_SIZE:\t\t%d KByte\n", (int)(info.localMemSize / 1024));
//	
//	    // CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE
//	    maxConstantBufferSize = getLong(context.deviceGPU, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE);
//	    //System.out.printf("CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE:\t%d KByte\n", (int)(info.maxConstantBufferSize / 1024));
//		}
//  }
//	
//  
//  
//  public synchronized static GpuContext getGPUContext()
//  {
//  	if (programContext==null)
//  		programContext = new GpuContext();
//  	
//  	return programContext;
//  }
//  
//	private GpuContext()
//	{
//	  final int platformIndex = 0;
//	  final long deviceType = CL_DEVICE_TYPE_GPU;
//	  final int deviceIndex = 0;
//	
//	  try
//	  {
//	    // Enable exceptions and subsequently omit error checks in this sample
//	    CL.setExceptionsEnabled(true);
//	    //CL.setLogLevel(CL.LogLevel.LOG_TRACE);
//	
//	    // Obtain the number of platforms
//	    int numPlatformsArray[] = new int[1];
//	    clGetPlatformIDs(0, null, numPlatformsArray);
//	    int numPlatforms = numPlatformsArray[0];
//	
//	    // Obtain a platform ID
//	    cl_platform_id platforms[] = new cl_platform_id[numPlatforms];
//	    clGetPlatformIDs(platforms.length, platforms, null);
//	    cl_platform_id platform = platforms[platformIndex];
//	
//	    // Initialize the context properties
//	    cl_context_properties contextProperties = new cl_context_properties();
//	    contextProperties.addProperty(CL_CONTEXT_PLATFORM, platform);
//	    
//	    // Obtain the number of devices for the platform
//	    int numDevicesArray[] = new int[1];
//	    clGetDeviceIDs(platform, deviceType, 0, null, numDevicesArray);
//	    int numDevices = numDevicesArray[0];
//	    
//	    // Obtain a device ID 
//	    cl_device_id devices[] = new cl_device_id[numDevices];
//	    clGetDeviceIDs(platform, deviceType, numDevices, devices, null);
//	    deviceGPU = devices[deviceIndex];
//	
//	    // Create a context for the selected device
//	    context = clCreateContext(contextProperties, 1, new cl_device_id[]{deviceGPU},null, null, null);
//	    
//	    info = new OpenCLDeviceInfo(this);
//	  } 
//	  catch (GPUException e) 
//	  { 
//	  	cleanupGPU(); 
//	  	throw e;
//	  }
//	}
//  
//  /**
//   * Returns the value of the device info parameter with the given name.
//   *
//   * @param device The device
//   * @param paramName The parameter name
//   * @return The value
//   */
//  private static int getInt(cl_device_id device, int paramName)
//  {
//      return getInts(device, paramName, 1)[0];
//  }
//  
//  /**
//   * Returns the values of the device info parameter with the given name.
//   *
//   * @param device The device
//   * @param paramName The parameter name
//   * @param numValues The number of values
//   * @return The value
//   */
//  private static int[] getInts(cl_device_id device, int paramName, int numValues)
//  {
//      int values[] = new int[numValues];
//      clGetDeviceInfo(device, paramName, Sizeof.cl_int * numValues, Pointer.to(values), null);
//      return values;
//  }
//  
//  /**
//   * Returns the value of the device info parameter with the given name.
//   *
//   * @param device The device
//   * @param paramName The parameter name
//   * @return The value
//   */
//  private static long getLong(cl_device_id device, int paramName)
//  {
//      return getLongs(device, paramName, 1)[0];
//  }
//  
//  /**
//   * Returns the values of the device info parameter with the given name.
//   *
//   * @param device The device
//   * @param paramName The parameter name
//   * @param numValues The number of values
//   * @return The value
//   */
//  private static long[] getLongs(cl_device_id device, int paramName, int numValues)
//  {
//      long values[] = new long[numValues];
//      clGetDeviceInfo(device, paramName, Sizeof.cl_long * numValues, Pointer.to(values), null);
//      return values;
//  }
//  
//  /**
//   * Returns the value of the device info parameter with the given name.
//   *
//   * @param device The device
//   * @param paramName The parameter name
//   * @return The value
//   */
//  private static String getString(cl_device_id device, int paramName)
//  {
//      // Obtain the length of the string that will be queried
//      long size[] = new long[1];
//      clGetDeviceInfo(device, paramName, 0, null, size);
//
//      // Create a buffer of the appropriate size and fill it with the info
//      byte buffer[] = new byte[(int)size[0]];
//      clGetDeviceInfo(device, paramName, buffer.length, Pointer.to(buffer), null);
//
//      // Create a string from the buffer (excluding the trailing \0 byte)
//      return new String(buffer, 0, buffer.length-1);
//  }
//	
//  private void cleanupGPU()
//  {
//    if (context!=null)
//    {
//      clReleaseContext(context);
//      context = null;
//    }
//  }
//  
//  public cl_context getContext()
//  {
//  	return context;
//  }
//  
//  public OpenCLDeviceInfo getDeviceInfo()
//  {
//  	return info;
//  }
//	
//	public GpuKernel createKernel(File programFile, String compilerOptions, String functionName)
//	{
//    return new GpuKernel(this, programFile, compilerOptions, functionName);
//	}
//	
//  @Override
//	protected void finalize() throws Throwable
//  {
//    //make sure to cleanup the GPU stuff it exists
//    try
//    {
//      cleanupGPU();
//    }
//    finally
//    {
//      super.finalize();
//    }
//  }
}
