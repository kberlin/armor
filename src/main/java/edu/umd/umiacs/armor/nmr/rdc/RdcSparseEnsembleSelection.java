/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import edu.umd.umiacs.armor.math.linear.LinearMathFileIO;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.Model;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.filters.ResidueStructureFilter;
import edu.umd.umiacs.armor.refinement.AbstractSparseEnsembleSelection;
import edu.umd.umiacs.armor.refinement.RefinementRuntimeException;
import edu.umd.umiacs.armor.util.ParseOptions;

public final class RdcSparseEnsembleSelection extends AbstractSparseEnsembleSelection<RdcEnsembleInput>
{
	public static void main(String[] args) throws IOException, PdbException, AtomNotFoundException, ClassNotFoundException, RuntimeException, RdcDataException
	{
  	ParseOptions options = new ParseOptions();
  	options.addOption("bicelle", "bicelle", "Bicelle concentration for RDCs.", 0.05);
  	options.addOption("maxsum", "maxsum", "Maximum sum of the column weights.", 5.0);
  	options.addOption("rdc", "rdc", "Pati formatted RDC filename.", "");
  	options.addOption("vector", "vector", "Output vectors as Lx3N A matrix file. The matrix is scaled such that D=A*S, "
  			+ "where S is the alignment tensor, and all the constants are included in A.", false);
  	options.addOption("rdcOverlay", "overlay", "Overlay file when computing RDCs. Rarely used, two domain molecule only. Set to empty for none.", "");
  	
		//create the solver
		RdcSparseEnsembleSelection selection = new RdcSparseEnsembleSelection(options, args);
		
		selection.run("rdc");
	}
	
	public RdcSparseEnsembleSelection(ParseOptions options, String[] args)
	{
		super(options, args);
		setDefaultSolver();
	}
	
	@Override
	protected RdcEnsembleInput createInput()
	{
   	if (getOptions().get("rdc").getString().isEmpty())
  	{
  		System.err.println("RDC filename name cannot be empty.");
  		System.err.println(getOptions().helpMenuString());
  		System.exit(1);
  	}

		String rdcFile = getOptions().get("rdc").getString();
		String overlayPdb  =  getOptions().get("rdcOverlay").getString();
		
	 	try
		{
			//create data list
		  ExperimentalRdcData experimentalData = ExperimentalRdcData.readPati(new File(rdcFile));
			
			//create predictor
		 	PatiPredictor rdcPredictor = PatiPredictor.createBicellePredictor(getOptions().get("bicelle").getDouble());
	
		 	//create filter
		 	ResidueStructureFilter structFilter = null;
		 		 	
		 	//open the overlay structure
		 	ArrayList<BasicMolecule> molList = null;
		 	if (!overlayPdb.isEmpty())
		 	{
			 	Model model = MoleculeFileIO.readPDB(overlayPdb).getModel(0);
			 	
			 	ArrayList<String> chains = model.getModelChains();
			 	molList = new ArrayList<BasicMolecule>(chains.size());
			 	
			 	for (String chain : chains)
			 		molList.add(model.createChain(chain).createBasicMolecule());
		 	}
		 	
		 	//create the input
		 	RdcEnsembleInput ensembleInput = new RdcEnsembleInput(experimentalData, rdcPredictor, molList, structFilter);
		 	
		 	return ensembleInput;
		}
		catch (PdbException e)
		{
			throw new RefinementRuntimeException(e);
		}	 		
		catch (IOException e)
		{
			throw new RefinementRuntimeException(e);
		}	 		
	}
	
	@Override
	protected RdcEnsembleInput postprocessInput(RdcEnsembleInput input)
	{
		if (getOptions().get("vector").getBoolean())
		{
		 	System.out.print("Computing the scaled vector matrix...");
		 	RealMatrix A = input.getScaledVectorMatrix();
		 	System.out.println("done.");
		 	
		 	String name = getOptions().get("output").getString()+File.separator+"rdc_vector_matrix.dat";
		 	
		 	System.out.print("Outputing the scaled vector file to "+name+"...");
		 	try
			{
				LinearMathFileIO.write(new File(name), A);
			 	System.out.println("done.");

			}
			catch (Exception e)
			{
				System.out.println("failed.");
				e.printStackTrace();
			}
		}
		
		return input;
	}

}
