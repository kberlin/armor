/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.util.AbstractSolution;

public final class RdcSolution extends AbstractSolution<ExperimentalRdcData, RdcDatum>
{
	private final ArrayList<RdcPredictor> predictors;
	/**
	 * 
	 */
	private static final long serialVersionUID = 7154202924751313545L;

	public RdcSolution(List<? extends RdcPredictor> predictors, ExperimentalRdcData data)
	{
		super(data);
		this.predictors = new ArrayList<RdcPredictor>(predictors);
	}

	public RdcSolution createRigid()
	{
		return createSubList(getExperimentalData().createRigid());
	}
	
	private RdcSolution createSubList(ExperimentalRdcData newList)
	{
		if (newList==getExperimentalData())
			return this;
		
		ArrayList<Integer> indicies = getAssociatedIndices(newList);
		ArrayList<RdcPredictor> newPredictors = new ArrayList<RdcPredictor>(indicies.size());
		for (int index : indicies)
		{
			newPredictors.add(this.predictors.get(index));
		}
		
		return new RdcSolution(newPredictors, newList);
	}
	
	private ArrayList<RdcSolution> createSubLists(ArrayList<ExperimentalRdcData> experimentalList)
	{
		ArrayList<RdcSolution> newLists = new ArrayList<RdcSolution>(experimentalList.size());
		for (ExperimentalRdcData currData : experimentalList)
			newLists.add(createSubList(currData));
		
		return newLists;
	}
	
	private final ArrayList<Integer> getAssociatedIndices(ExperimentalRdcData list)
	{
		ArrayList<Integer> indices = new ArrayList<Integer>(list.size());
		ArrayList<Bond> bonds = getExperimentalData().getBonds();
		for (RdcDatum bondRelax : list.getDataRef())
		{			
			int index = bonds.indexOf(bondRelax.getBond());
			indices.add(index);
		}
		
		return indices;
	}
	
	public RdcPredictor getPredictor(int index)
	{
		return this.predictors.get(index);
	}
	
	public AlignmentTensor getTensor()
	{
		return this.predictors.get(0).getAlignmentTensor();
	}

	@Override
	public double[] predict()
	{
		double[] pred = new double[this.predictors.size()];
		
		int count=0;
		for (RdcPredictor predictor : this.predictors)
		{
			pred[count] = predictor.predict();
			count++;
		}
		
		return pred;
	}

	public double predict(int index)
	{
		return this.predictors.get(index).predict();
	}
	
	public double qualityFactor()
	{
		return BasicStat.cloreQualityFactor(predict(), getExperimentalData().values());
	}
	
	public ArrayList<RdcSolution> seperateByBondType()
	{
		return createSubLists(getExperimentalData().seperateByBondType());
	}
	
	public ArrayList<RdcSolution> seperateByChain()
	{
		return createSubLists(getExperimentalData().seperateByChain());
	}
}
