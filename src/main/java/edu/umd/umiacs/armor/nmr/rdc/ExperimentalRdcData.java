/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.BondType;
import edu.umd.umiacs.armor.molecule.Model;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.AtomImpl;
import edu.umd.umiacs.armor.molecule.PrimaryStructure;
import edu.umd.umiacs.armor.molecule.SecondaryStructure;
import edu.umd.umiacs.armor.util.AbstractExperimentalData;
import edu.umd.umiacs.armor.util.BasicUtils;

/**
 * The Class ExperimentalCouplingData.
 */
public final class ExperimentalRdcData extends AbstractExperimentalData<RdcDatum>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6090380364021901054L;
	
	/**
	 * Read cns.
	 * 
	 * @param file
	 *          the file
	 * @return the experimental coupling data
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 * @throws RelaxFileException
	 *           the rdc file exception
	 */
	public static ExperimentalRdcData readCNS(File file) throws IOException, RdcFileException
	{
    String fileText = BasicUtils.readFileToString(file);
    
    String key = "(?:[\\w]+)";
    String value = "\"?(?:[\\w]+)\"?";
    
    String set = key+"[\\s=]+"+value;
    String fullset = set+"(?:[\\s]+and[\\s]+"+set+")*";
    
    String keyG = "([\\w]+)";
    String valueG = "\"?([\\w]+)\"?";
    String setG = keyG+"[\\s=]+"+valueG+"(?:[\\s]+and)*";
    //String fullsetG = "[\\s]*"+setG+"(?:[\\s]+and[\\s]+"+setG+")*";
    
    String res1 = "(\\([\\s]*"+fullset+"[\\s]*\\))";
    String res2 = "(\\([\\s]*"+fullset+"[\\s]*\\))[\\s]+(-?\\d*\\.*\\d+)[\\s]+(-?\\d*\\.*\\d+)(?:[\\s]+-?\\d*\\.*\\d+)?";

    String exp = ""+res1+"\\s*"+""+res2+"";
    
    Pattern patternGeneral = Pattern.compile(exp,Pattern.CASE_INSENSITIVE);
    Pattern patternFullSet = Pattern.compile(setG,Pattern.CASE_INSENSITIVE);
    
    ArrayList<RdcDatum> newList = new ArrayList<RdcDatum>();
    
    try
    {
    	Matcher matcher = patternGeneral.matcher(fileText);
	    while(matcher.find()) 
	    { 
	    	double rdcValue = Double.parseDouble(matcher.group(3));
	    	double rdcError = rdcValue*.05;
	    	if (matcher.groupCount()>=4)
	    		rdcError = Double.parseDouble(matcher.group(4));
	
	    	Matcher matcherRes1 = patternFullSet.matcher(matcher.group(1));
	     	Matcher matcherRes2 = patternFullSet.matcher(matcher.group(2));
	
	      String type1 = "";
	     	String type2 = "";
	     	int residue1 = 0;
	     	int residue2 = 0;
	     	String chainID1 = "A";
	     	String chainID2 = "A";
	     	
	      while (matcherRes1.find())
	      {
	      	if (matcherRes1.group(1).equalsIgnoreCase("resid") 
	      			|| matcherRes1.group(1).equalsIgnoreCase("resi"))
	      		residue1 = Integer.parseInt(matcherRes1.group(2));
	      	else
	      	if (matcherRes1.group(1).equalsIgnoreCase("name") )
	      		type1 = matcherRes1.group(2);      		
	      	else
	       	if (matcherRes1.group(1).equalsIgnoreCase("segid") )
	       		chainID1 = matcherRes1.group(2);
	      }
	      while (matcherRes2.find())
	      {
	      	if (matcherRes2.group(1).compareToIgnoreCase("resid")==0 
	      			|| matcherRes2.group(1).compareToIgnoreCase("resi")==0)
	      		residue2 = Integer.parseInt(matcherRes2.group(2));
	      	else
	      	if (matcherRes2.group(1).compareToIgnoreCase("name")==0)
	      	{
	      		type2 = matcherRes2.group(2);
	      	}
	      	else
	       	if (matcherRes2.group(1).compareToIgnoreCase("segid")==0)
	       		chainID2 = matcherRes2.group(2);
	      }
	      
	      PrimaryStructure primary1 = new PrimaryStructure(residue1, type1, chainID1, "UKW", 0.0);
	      PrimaryStructure primary2 = new PrimaryStructure(residue2, type2, chainID2, "UKW", 0.0);
	      SecondaryStructure secondary1 = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
	      SecondaryStructure secondary2 = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
	      
	      AtomImpl atom1 = new AtomImpl(new Point3d(0,0,0), primary1, secondary1);
	      AtomImpl atom2 = new AtomImpl(new Point3d(1,0,0), primary2, secondary2);
	      
	      if (atom1.isHydrogen() && !atom2.isHydrogen())
	      {
	      	AtomImpl tempAtom = atom2;
	      	atom2 = atom1;
	      	atom1 = tempAtom;
	      }
	      
	      Bond bond = new Bond(atom1, atom2);
	      
	      newList.add(new RdcDatum(rdcValue, rdcError==0.0 ? 1.0 : rdcError, bond, true));
	    }
    }
    catch (Exception e)
    {
    	throw new RdcFileException("Could not parse CNS file.", e);
    }
    
    if (newList.isEmpty())
    	throw new RdcFileException("Could not find any CNS rdc data in file.");
    
    return new ExperimentalRdcData(newList);
	}
	
	/**
	 * Read pati.
	 * 
	 * @param file
	 *          the file
	 * @return the experimental coupling data
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 * @throws RelaxFileException
	 *           the rdc file exception
	 */
	public static ExperimentalRdcData readPati(File file) throws IOException, RdcFileException
	{
		Scanner scanner = BasicUtils.getFileScanner(file);
		
		try
		{
	    ArrayList<RdcDatum> dataList = new ArrayList<RdcDatum>();
	    
	    Pattern word = Pattern.compile("[\\w']+");
			
			int counter = 0;
			while (scanner.hasNextLine())
			{
				//mark the line number
				counter++;
				
				//get the next line
				String nextLine = scanner.nextLine();
				Scanner lineScanner = new Scanner(nextLine);
				
				//get rid of potential trash due to invalid conversion
				try
				{
					boolean isInactive = false;

					//is empty
					if (!lineScanner.hasNext())
						continue;
					
					String currLine = lineScanner.nextLine().trim();
					
					//is a comment line
					if (currLine.startsWith("%"))
						continue;

					//is flexible
					if (currLine.startsWith("*"))
					{
						isInactive = true;
						lineScanner.close();
						lineScanner = new Scanner(currLine.substring(1));
					}
					else
					{
						lineScanner.close();
						lineScanner = new Scanner(currLine);
					}

					lineScanner.useLocale(Locale.US);
					
					int residueNumber1;
					String chainID1;
					String nameP;
					int residueNumber2;
					String chainID2;
					String nameQ;
		      double value;
		      double error;
					try
					{
						residueNumber1 = (int)lineScanner.nextDouble();
						chainID1 = lineScanner.next(word);
						nameP = lineScanner.next(word);
						residueNumber2 = (int)lineScanner.nextDouble();
						chainID2 = lineScanner.next(word);
						nameQ = lineScanner.next(word);
			      value = lineScanner.nextDouble();
			      error = lineScanner.nextDouble();
					}
					catch(InputMismatchException e)
					{
						//try the no chainID
						lineScanner.close();
						lineScanner = new Scanner(currLine);						
						lineScanner.useLocale(Locale.US);
						
						residueNumber1 = (int)lineScanner.nextDouble();
						chainID1 = "A";
						nameP = lineScanner.next(word);
						residueNumber2 = (int)lineScanner.nextDouble();
						chainID2 = "A";
						nameQ = lineScanner.next(word);
			      value = lineScanner.nextDouble();
			      error = lineScanner.nextDouble();
					}
					
					//read in the bond lengths
					
		      PrimaryStructure primary1 = new PrimaryStructure(residueNumber1, nameP, chainID1, "UKW", 0.0);
		      PrimaryStructure primary2 = new PrimaryStructure(residueNumber2, nameQ, chainID2, "UKW", 0.0);
		      SecondaryStructure secondary1 = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
		      SecondaryStructure secondary2 = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
		      
		      AtomImpl a1 = new AtomImpl(new Point3d(0,0,0), primary1, secondary1);
		      AtomImpl a2 = new AtomImpl(new Point3d(1,0,0), primary2, secondary2);

		      Bond bond = new Bond(a1,a2);
		      
		      RdcDatum rdc = new RdcDatum(value, error, bond, !isInactive);
		      dataList.add(rdc);
				}
				catch (Exception e)
				{
					throw new RdcFileException("Could not parse relaxation input file, line "+counter+":\n  \""+nextLine+"\"", e);
				}
				finally
				{
					lineScanner.close();
				}
			}
			
			return new ExperimentalRdcData(dataList);
		}
		finally
		{
			scanner.close();
		}
	}
	
	public ExperimentalRdcData(List<RdcDatum> couplingList)
	{
		super(couplingList);
	}

	/**
	 * Creates the from chain.
	 * 
	 * @param chainID
	 *          the chain id
	 * @return the experimental coupling data
	 */
	public ExperimentalRdcData createFromChain(String chainID)
	{
		ArrayList<RdcDatum> bondList = new ArrayList<RdcDatum>();

		for (RdcDatum coupling : getDataRef())
		{
			if (coupling.getBond().getFromAtom().getPrimaryInfo().getChainID().equalsIgnoreCase(chainID)
					&& 	(coupling.getBond().getToAtom().getPrimaryInfo().getChainID().equalsIgnoreCase(chainID)))
				
				bondList.add(coupling);
		}
		
		return new ExperimentalRdcData(bondList);
	}
	
	/**
	 * Creates the from existing model atoms.
	 * 
	 * @param model
	 *          the model
	 * @return the experimental coupling data
	 */

	public ExperimentalRdcData createFromExistingAtoms(Molecule molecule)
	{
    ArrayList<RdcDatum> newList = new ArrayList<RdcDatum>(size());
    
		for (int iter=0; iter<size(); iter++)
		{
			try
			{
				newList.add(get(iter).createFromMolecule(molecule));
			}
			catch (AtomNotFoundException e) {}
		}
		
		return new ExperimentalRdcData(newList);
	}

	/**
	 * Creates the from type.
	 * 
	 * @param type
	 *          the type
	 * @return the experimental coupling data
	 */
	public ExperimentalRdcData createFromType(BondType type)
	{
		ArrayList<RdcDatum> bondList = new ArrayList<RdcDatum>();

		for (RdcDatum coupling : getDataRef())
		{
			if (coupling.getBond().getBondType().equals(type))
				bondList.add(coupling);
		}
		
		return new ExperimentalRdcData(bondList);
	}

	/**
	 * Creates the from vector list.
	 * 
	 * @param vectorList
	 *          the vector list
	 * @return the experimental coupling data
	 */
	public ExperimentalRdcData createFromVectorList(ArrayList<Point3d> vectorList)
	{
    ArrayList<RdcDatum> newList = new ArrayList<RdcDatum>(size());
		for (int iter=0; iter<size(); iter++)
			newList.add(get(iter).createFromVector(vectorList.get(iter)));
		
		return new ExperimentalRdcData(newList);
	}

	/**
	 * Creates the monte carlo.
	 * 
	 * @return the experimental coupling data
	 */
	public ExperimentalRdcData createMonteCarlo()
	{
    ArrayList<RdcDatum> newList = new ArrayList<RdcDatum>(size());
		for (int iter=0; iter<size(); iter++)
			newList.add(get(iter).createMonteCarlo());
		
		return new ExperimentalRdcData(newList);
	}
	
	/**
	 * Creates the rigid.
	 * 
	 * @return the experimental coupling data
	 */
	public ExperimentalRdcData createRigid()
	{
    ArrayList<RdcDatum> newList = new ArrayList<RdcDatum>(size());
    
		for (int iter=0; iter<size(); iter++)
		{
			if (get(iter).isRigid())
				newList.add(get(iter));
		}
		
		return new ExperimentalRdcData(newList);
	}
	
	/**
	 * Creates the rotated.
	 * 
	 * @param R
	 *          the r
	 * @return the experimental coupling data
	 */
	public ExperimentalRdcData createRotated(Rotation R)
	{
    ArrayList<RdcDatum> newList = new ArrayList<RdcDatum>(size());
		for (int iter=0; iter<size(); iter++)
			newList.add(get(iter).createFromVector(R.mult(get(iter).getBond().getVector())));
		
		return new ExperimentalRdcData(newList);
	}
	
	/**
	 * Creates the secondary only.
	 * 
	 * @param model
	 *          the model
	 * @return the experimental coupling data
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public ExperimentalRdcData createSecondaryOnly(Model model) throws AtomNotFoundException
	{
    ArrayList<RdcDatum> newList = new ArrayList<RdcDatum>(size());
    
		for (int iter=0; iter<size(); iter++)
			newList.add(get(iter).createSecondaryOnly(model));
		
		return new ExperimentalRdcData(newList);
	}
	
	public ExperimentalRdcData createWithMatchingBonds(ExperimentalRdcData matchingData)
	{
    ArrayList<RdcDatum> newList = new ArrayList<RdcDatum>(size());
    
    ArrayList<Bond> matchingDataBonds = matchingData.getBonds();
		for (RdcDatum rdc : getDataRef())
		{
			if (matchingDataBonds.contains(rdc.getBond()))
				newList.add(rdc);
		}
		
		return new ExperimentalRdcData(newList);
	}
	
	/**
	 * Gets the bonds.
	 * 
	 * @return the bonds
	 */
	public ArrayList<Bond> getBonds()
	{
		ArrayList<Bond> bondList = new ArrayList<Bond>(size());
		
		for (RdcDatum rdc : getDataRef())
			bondList.add(rdc.getBond());
		
		return bondList;
	}

	
	/**
	 * Gets the bond types.
	 * 
	 * @return the bond types
	 */
	public ArrayList<BondType> getBondTypes()
	{
		ArrayList<BondType> bonds = new ArrayList<BondType>();
		
		for (RdcDatum c : getDataRef())
		{
			int index = bonds.indexOf(c.getBond().getBondType());
			if (index<0)
				bonds.add(c.getBond().getBondType());
		}
		
		//sort the result
		Collections.sort(bonds);
		
		return bonds;
	}
	
	public ArrayList<String> getChains()
	{
		ArrayList<String> bonds = new ArrayList<String>();
		
		for (RdcDatum coupling : getDataRef())
		{
			int index = bonds.indexOf(coupling.getBond().getFromAtom().getPrimaryInfo().getChainID());
			if (index<0)
				bonds.add(coupling.getBond().getFromAtom().getPrimaryInfo().getChainID());
		}
		
		//sort the result
		Collections.sort(bonds);
		
		return bonds;
	}
	
	
	/**
	 * Gets the norm bond vectors.
	 * 
	 * @return the norm bond vectors
	 */
	public ArrayList<Point3d> getNormBondVectors()
	{
		ArrayList<Point3d> bonds = new ArrayList<Point3d>(size());
		
		for (int iter=0; iter<size(); iter++)
			bonds.add(get(iter).getBond().getNormVector());
		
		return bonds;
	}

	/**
	 * Seperate by bond type.
	 * 
	 * @return the array list
	 */
	public ArrayList<ExperimentalRdcData> seperateByBondType()
	{
		ArrayList<BondType> types = getBondTypes();
		
		ArrayList<ExperimentalRdcData> couplingList = new ArrayList<ExperimentalRdcData>();
		for (BondType type : types)
		{
			couplingList.add(createFromType(type));
		}
		
		return couplingList;
	}

	public ArrayList<ExperimentalRdcData> seperateByChain()
	{
		ArrayList<String> types = getChains();
		
		ArrayList<ExperimentalRdcData> relaxLists = new ArrayList<ExperimentalRdcData>();
		for (String type : types)
		{
			relaxLists.add(createFromChain(type));
		}
		
		return relaxLists;
	}
	
	@Override
	protected List<RdcDatum> getDataRef()
	{
		return super.getDataRef();
	}
}
