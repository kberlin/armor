/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Model;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.SecondaryStructure;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;

/**
 * The Class Coupling.
 */
public final class RdcDatum extends BaseExperimentalDatum
{
	
	/** The bond. */
	final private Bond bond;

	/** The rigid. */
	final private boolean rigid;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6497851035071117719L;
	
	public RdcDatum(double value, double error, Bond bond, boolean rigid)
	{
		super(value, error);
		this.bond = bond;
		this.rigid = rigid;
	}

	public RdcDatum(ExperimentalDatum value, Bond bond, boolean rigid)
	{
		super(value);
		
		if (Double.isNaN(value.value()))
			throw new RdcRuntimeException("RDC experimental value cannot be NaN.");
		if (Double.isNaN(value.error()))
			throw new RdcRuntimeException("RDC experimental error cannot be NaN.");
		
		this.bond = bond;
		this.rigid = rigid;
	}
	
	private RdcDatum(RdcDatum prevValue, Bond bond)
	{
		super(prevValue);
		this.bond = bond;
		this.rigid = prevValue.rigid;
	}

	/**
	 * Creates the from bond.
	 * 
	 * @param bond
	 *          the bond
	 * @return the coupling
	 */
	public RdcDatum createFromBond(Bond bond)
	{
		RdcDatum rdc = new RdcDatum(this, bond);
		
		return rdc;
	}
	
	/**
	 * Creates the from model.
	 * 
	 * @param model
	 *          the model
	 * @return the coupling
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public RdcDatum createFromModel(Model model) throws AtomNotFoundException
	{
		return createFromBond(model.getModelBond(this.bond));
	}
	
	/**
	 * Creates the from molecule.
	 * 
	 * @param mol
	 *          the mol
	 * @return the coupling
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public RdcDatum createFromMolecule(Molecule mol) throws AtomNotFoundException
	{
		Point3d a1 = mol.getAtomPosition(this.bond.getFromAtom());
		Point3d a2 = mol.getAtomPosition(this.bond.getToAtom());
		
		Bond newBond = new Bond(this.bond.getFromAtom().createWithNewPosition(a1), this.bond.getToAtom().createWithNewPosition(a2));
		
		RdcDatum rdc = new RdcDatum(this, newBond);
		
		return rdc;
	}
	
	/**
	 * Creates the from vector.
	 * 
	 * @param p
	 *          the p
	 * @return the coupling
	 */
	public RdcDatum createFromVector(Point3d p)
	{
		Point3d newP = this.bond.getFromAtom().getPosition().add(p);		
		Bond newBond = new Bond(this.bond.getFromAtom(), this.bond.getToAtom().createWithNewPosition(newP));
		
		RdcDatum rdc = new RdcDatum(this, newBond);
		
		return rdc;
	}

	/**
	 * Creates the monte carlo.
	 * 
	 * @return the coupling
	 */
	public RdcDatum createMonteCarlo()
	{
		return new RdcDatum(BasicStat.normalDistributionSample(value(), error()), error(), this.bond, this.rigid);
	}
	
	/**
	 * Creates the rigid.
	 * 
	 * @param isRigid
	 *          the is rigid
	 * @return the coupling
	 */
	public RdcDatum createRigid(boolean rigid)
	{
		return new RdcDatum(this, this.bond, rigid);
	}
	
	/**
	 * Creates the secondary only.
	 * 
	 * @param model
	 *          the model
	 * @return the coupling
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public RdcDatum createSecondaryOnly(Model model) throws AtomNotFoundException
	{
		Bond modelBond = model.getModelBond(this.bond);
		
		boolean isRigid = modelBond.getFromAtom().getSecondaryInfo().getType() == SecondaryStructure.Type.HELIX ||
				modelBond.getFromAtom().getSecondaryInfo().getType() == SecondaryStructure.Type.SHEET;
		
		return new RdcDatum(this, modelBond, isRigid);
	}
	
	/**
	 * Gets the bond.
	 * 
	 * @return the bond
	 */
	public Bond getBond()
	{
		return this.bond;
	}

	/**
	 * Checks if is rigid.
	 * 
	 * @return true, if is rigid
	 */
	public boolean isRigid()
	{
		return this.rigid;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "Value: "+value()+" Error: "+error()+", Rigid: "+ this.rigid +" From:"+this.bond.getFromAtom()+" To: "+this.bond.getToAtom();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.BaseExperimentalDatum#structureInfoString()
	 */
	@Override
	public String valueInfoString()
	{
		return String.format("%4d %2s %2s %2s", getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
				getBond().getFromAtom().getPrimaryInfo().getChainID(),
				getBond().getFromAtom().getPrimaryInfo().getAtomName(),
				getBond().getToAtom().getPrimaryInfo().getAtomName());

	}
}