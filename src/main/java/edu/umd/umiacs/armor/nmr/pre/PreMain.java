/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import edu.umd.umiacs.armor.PackageInfo;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.nmr.pre.gui.PreFitPlot;
import edu.umd.umiacs.armor.util.FileIO;
import edu.umd.umiacs.armor.util.ParseOptions;

public class PreMain
{
	public static void main(String[] args) throws PdbException, IOException, AtomNotFoundException
	{
  	ParseOptions options = new ParseOptions();
  	options.addRequiredOption("pdb", "-pdb", "Name of the PDB file to use for calculations.", String.class);
  	options.addOption("model", "-model", "Model number of the pdb file.", 1);
  	options.addRequiredOption("pre", "-pre", "Name of the PRE file to use for calculations. ARMOR format.", String.class);
  	options.addOption("ball", "-ball", "Name of the PyMOL script file for the PRE location.", "");
  	options.addOption("dockPdb", "-dock", "Name of the output PDB file used for PRE docking.", "");
  	options.addOption("draw", "-draw", "Draw the fit plots.", false);
  	options.addOption("nostat", "-nostat", "Do not compute statistics (speed up computation).", false);
  	options.addOption("rescale", "-rescale", "Optimally rescale the PRE values.", false);
  	options.addOption("robust", "-robust", "Perform robust least-squares regression (outlier suppression) were possible.", false);
  	
  	try 
  	{
  		options.parse(args);
  		if (options.needsHelp())
  		{
  			System.out.println(options.helpMenuString());
  			System.exit(0);
  		}
  		
  		options.checkParameters();
  	}
  	catch (Exception e)
  	{
  		System.err.println(e.getMessage());
  		e.printStackTrace();
  		System.err.println(options.helpMenuString());
  		System.exit(1);
  	}
  	
 		int status = runProgram(options);
 		
 		if (status!=0)
 		{
 			System.out.println("Execution failed!");
 		}
	}
	
	private static int runProgram(ParseOptions options) throws PdbException, IOException, AtomNotFoundException 
	{
		System.out.println("ARMOR Version: "+PackageInfo.ARMOR_VERSION+", Build Time: "+PackageInfo.BUILD_TIME);

		if (options.get("help").getBoolean() || !options.get("pdb").isSet())
		{
			System.out.println("java -jar armorpre*.jar -pdb <pdbfile> [-rdc <rdcfile>] [optional flags]");
			System.out.println("Example: java -jar armorpre.jar -pdb 2JY6.pdb -pre 2jy6_pre.txt");
  		System.out.println(options.helpMenuString());
  		return 0;
		}
		
		String preFile = options.get("pre").getString();
	  String pdbFile = options.get("pdb").getString();
	  boolean robust = options.get("robust").getBoolean();

	  //read experimental data
	  System.out.print("Extracting data....");
	  HashMap<Integer, ExperimentalPreData> experimentalData = null;
	  if (!preFile.isEmpty())
	  {
	  	try
	  	{
	  		experimentalData = ExperimentalPreData.readDataFile(new File(preFile));
	  	}
	  	catch (Exception e)
	  	{
	  		System.out.println("Could not parse PRE data.\n"+e.getMessage());
	  		e.printStackTrace();
	  		return 1;
	  	}
	  }

	  //adjust model number to start with 0
	  int model = options.get("model").getInteger()-1;
	  
	  //read the pdb file
	  PdbStructure pdb = null;
	  try
	  {
	  	pdb = MoleculeFileIO.readPDB(pdbFile);
	  }
	  catch(Exception e)
	  {
  		System.out.println("Could not parse PDB data.\n"+e.getMessage());
  		return 1;  	
	  }
		
	  //check that model number is ok
		if (pdb.getNumberOfModels()<=model)
		{
			System.out.println("Invalid model number: "+model+". Failed!");
			return 1;
		}
		
		Molecule molecule = pdb.getModel(model).createBasicMolecule();
		
		System.out.println("done.");
 		
		//compute the results
	  System.out.print("Computing results....");
	  ArrayList<PreResults> results = new ArrayList<PreResults>();
  	ArrayList<Integer> keys = new ArrayList<Integer>(experimentalData.keySet());
  	Collections.sort(keys);
	  try
	  {
	  	for (Integer key : keys)
	  		results.add(new PreResults(experimentalData.get(key), molecule, robust, options.get("rescale").getBoolean()));
	  }
	  catch (AtomNotFoundException e)
	  {
	  	System.out.println(e.getMessage());
	  	return 1;
	  }
		System.out.println("done.");
		
		//add listener
		//results.addProgressObserver(new PreMain());
		
		//display fit
		if (options.get("draw").getBoolean())
		{
		  System.out.println("Displaying fit...");
		  int count = 0;
		  for (PreResults result : results)
		  {
		  	new PreFitPlot(result.getSolution(), ""+keys.get(count)).repaint();
		  	count++;
		  }
		}
		
		if (!options.get("dockPdb").getString().isEmpty())
		{
			//ArrayList<ExperimentalRdcData> chainList = experimentalData.seperateByChain();
			
			if (pdb.getModel(0).getModelChains().size()!=2 )
			{
				System.out.println("Docking must be performed on a two domain file only.");
				return 1;
			}
		}
		
		//compute the statistic
		if (!options.get("nostat").getBoolean())
		{
		  System.out.print("Computing statistics..");
		  for (PreResults result : results)
		  	result.computeStatistic();
		  System.out.println("done.");
		  
		  if (!options.get("dockPdb").getString().isEmpty())
		  {
			  System.out.print("Computing alignment uncertainty..");
		  	//computeAlignmentError(experimentalData, pdb.getModel(model), 150);
		  }			  
		}
		
	  //show results
	  int count = 0;
	  for (PreResults result : results)
	  {
	  	System.out.println("\n=====PRE Results for PRE Label "+keys.get(count)+"======\n"+result);
	  	count++;
	  }
				
		if (!options.get("ball").getString().isEmpty())
		{
			String fileName = options.get("axes").getString();
			
			File axesFile = FileIO.ensureExtension(new File(fileName), "", "py");
		  System.out.print("Saving tensor axes to \""+axesFile+"\"...");
		  //PdbFileIO.writePymolAxesScript(axesFile, pdb.getModel(model).createBasicMolecule(), results.getSolution().getTensor().getProperEigenDecomposition3d());
		  System.out.println("done.\n");
		}

		
		//if docking version turned on
		if (!options.get("dockPdb").getString().isEmpty())
		{
			String dockPdbName = options.get("dockPdb").getString();
			
			System.out.print("Computing docking results...");
			//dockResults(dockPdbName, experimentalData, pdb, model, volumeFraction, robust);
			System.out.println("");
			
			if (!options.get("axes").getString().isEmpty())
			{
				String dockAxesFile = FileIO.removeExtension(options.get("axes").getString());
				
			  System.out.print("Saving docking tensor axes to \""+dockAxesFile+"*.py\"...");
			  
			  //read the pdb file
			  PdbStructure dockedPdb = MoleculeFileIO.readPDB(new File(dockPdbName));
			  
			  for (int iter=0; iter<dockedPdb.getNumberOfModels(); iter++)
			  {
			  	//String name1 = dockedPdb.getModel(iter).getModelChains().get(0);
			  	//String name2 = dockedPdb.getModel(iter).getModelChains().get(1);
			  	//Molecule mol1 = dockedPdb.getModel(iter).createChain(0).createBasicMolecule();
			  	//Molecule mol2 = dockedPdb.getModel(iter).createChain(1).createBasicMolecule();
			  	
		  	  //ExperimentalRdcData data1 = experimentalData.createFromExistingAtoms(mol1);	  
		  	  //ExperimentalRdcData data2 = experimentalData.createFromExistingAtoms(mol2);
		  	  
		    	//AlignmentComputor d1Computor = new AlignmentComputor(data1, results.getComputor().isRobust(), results.getComputor().getOutlierThreshold());
		    	//AlignmentComputor d2Computor = new AlignmentComputor(data2, results.getComputor().isRobust(), results.getComputor().getOutlierThreshold());

		    	//AlignmentTensor tensor1 = d1Computor.computeTensor(mol1);
		    	//AlignmentTensor tensor2 = d2Computor.computeTensor(mol2);
			  	
				  //PdbFileIO.writePymolAxesScript(new File(dockAxesFile.concat("_"+name1+"_"+(iter+1)+".py")), mol1, tensor1.getProperEigenDecomposition3d());
				  //PdbFileIO.writePymolAxesScript(new File(dockAxesFile.concat("_"+name2+"_"+(iter+1)+".py")), mol2, tensor2.getProperEigenDecomposition3d());
			  }
			  
			  System.out.println("done.");
			}

		}
		
		
		System.out.println("\n\nComputation complete!");
		return 0;
	}
}
