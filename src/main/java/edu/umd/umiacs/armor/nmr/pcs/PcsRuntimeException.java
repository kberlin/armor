package edu.umd.umiacs.armor.nmr.pcs;

public class PcsRuntimeException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7118376138521797093L;

	public PcsRuntimeException()
	{
		super();
	}

	public PcsRuntimeException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public PcsRuntimeException(String message)
	{
		super(message);
	}

	public PcsRuntimeException(Throwable cause)
	{
		super(cause);
	}

}
