/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.AtomImpl;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.BondType;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PrimaryStructure;
import edu.umd.umiacs.armor.molecule.SecondaryStructure;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.NonNegativeExperimentalDatum;

/**
 * The Class ExperimentalRelaxation.
 */
public class ExperimentalRelaxationData implements Serializable
{
	/** The bond relaxation list. */
	private final ArrayList<BondRelaxation> bondRelaxationList;

	private final boolean rigid;
	
	/** The total size. */
	private final int totalSize;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5143864667344175236L;

	/**
	 * Creates the with list.
	 * 
	 * @param bondList
	 *          the bond list
	 * @return the experimental relaxation
	 */
	protected static ExperimentalRelaxationData createWithList(ArrayList<BondRelaxation> bondList)
	{
		return new ExperimentalRelaxationData(bondList, true);
	}
	
	/**
	 * Read data file.
	 * 
	 * @param file
	 *          the file
	 * @return the experimental relaxation
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 * @throws RelaxationFileException
	 *           the relaxation file exception
	 */
	public static ExperimentalRelaxationData readDataFile(File file) throws IOException, RelaxationFileException
	{
		ArrayList<BondRelaxation> newList = new ArrayList<BondRelaxation>();
		
		Scanner scanner = BasicUtils.getFileScanner(file);
		
		try
		{
			Pattern atomName = Pattern.compile("[\\w']+");
			
			int counter = 0;
			while (scanner.hasNextLine())
			{
				//mark the line number
				counter++;
				
				//get the next line
				String nextLine = scanner.nextLine();
				Scanner lineScanner = new Scanner(nextLine);
				
				//get rid of potential trash due to invalid conversion
				try
				{
					boolean isInactive = false;

					//is empty
					if (!lineScanner.hasNext())
						continue;
					
					String currLine = lineScanner.nextLine().trim();
					
					//is a comment line
					if (currLine.startsWith("%"))
						continue;

					//is flexible
					if (currLine.startsWith("*"))
					{
						isInactive = true;
						lineScanner.close();
						lineScanner = new Scanner(currLine.substring(1));
					}
					else
					{
						lineScanner.close();
						lineScanner = new Scanner(currLine);
					}

					lineScanner.useLocale(Locale.US);
					
					int residueNumber = (int)lineScanner.nextDouble();
					String chainID = lineScanner.next(atomName);
					String nameP = lineScanner.next(atomName);
					String nameQ = lineScanner.next(atomName);
				  double freq = lineScanner.nextDouble();
		      double R1 = lineScanner.nextDouble();
		      double R1err = lineScanner.nextDouble();
		      double R2 = lineScanner.nextDouble();
		      double R2err = lineScanner.nextDouble();
		      
		      double NOE = Double.NaN;
		      double NOEerr = Double.NaN;
			    double eta_xy = Double.NaN;
			    double eta_xyErr = Double.NaN;
			    double eta_z = Double.NaN;
			    double eta_zErr = Double.NaN;

		      
		      if (lineScanner.hasNextDouble())
		      	NOE = lineScanner.nextDouble();
		      if (lineScanner.hasNextDouble())
		      	NOEerr = lineScanner.nextDouble();

		      double CSA = Double.NaN;
		      if (lineScanner.hasNextDouble())
		      	CSA = lineScanner.nextDouble();
					
		      PrimaryStructure primary1 = new PrimaryStructure(residueNumber, nameP, chainID, "UKW", 0.0);
		      PrimaryStructure primary2 = new PrimaryStructure(residueNumber, nameQ, chainID, "UKW", 0.0);
		      SecondaryStructure secondary1 = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
		      SecondaryStructure secondary2 = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
		      
		      AtomImpl a1 = new AtomImpl(new Point3d(0,0,0), primary1, secondary1);
		      AtomImpl a2 = new AtomImpl(new Point3d(1,0,0), primary2, secondary2);

			    NonNegativeExperimentalDatum r1Datum = new NonNegativeExperimentalDatum(R1, R1err);
			    NonNegativeExperimentalDatum r2Datum = new NonNegativeExperimentalDatum(R2, R2err);
			    BaseExperimentalDatum noeDatum = new BaseExperimentalDatum(NOE, NOEerr);
			    NonNegativeExperimentalDatum etaXyDatum = new NonNegativeExperimentalDatum(eta_xy, eta_xyErr);
			    NonNegativeExperimentalDatum etaZDatum = new NonNegativeExperimentalDatum(eta_z, eta_zErr);
			    
			    //the default values
			    Bond bond = new Bond(a1,a2);
			    
			    ModelFreeBondConstraints constraints;
			    if (Double.isNaN(CSA))
			    	constraints = ModelFreeBondConstraints.getBondConstants(bond.getBondType());
			    else
			    	constraints = new ModelFreeBondConstraints(bond.getBondType(), CSA, CSA, CSA);
			    
			    RelaxationDatum data = new RelaxationDatum(bond, constraints, r1Datum, r2Datum, noeDatum, etaXyDatum, etaZDatum, freq, !isInactive);
		      ArrayList<RelaxationDatum> dataList = new ArrayList<RelaxationDatum>();
		      dataList.add(data);
		      
					newList.add(new BondRelaxation(bond, dataList));
				}
				catch (NoSuchElementException e)
				{
					throw new RelaxationFileException("Could not parse relaxation input file, line "+counter+":\n  \""+nextLine+"\"", e);
				}
				finally
				{
					lineScanner.close();
				}
			}
		}
		finally
		{
			scanner.close();
		}
		
		return new ExperimentalRelaxationData(newList);
	}
	
	/**
	 * Read data file.
	 * 
	 * @param fileName
	 *          the file name
	 * @return the experimental relaxation
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 * @throws RelaxationFileException
	 *           the relaxation file exception
	 */
	public static ExperimentalRelaxationData readDataFile(String fileName) throws IOException, RelaxationFileException
	{
		File file = new File(fileName);
		return readDataFile(file);
	}

	
	/**
	 * Instantiates a new experimental relaxation.
	 * 
	 * @param list
	 *          the list
	 */
	public ExperimentalRelaxationData(ArrayList<BondRelaxation> list)
	{
		this(list, false);
	}
	
	/**
	 * Instantiates a new experimental relaxation.
	 * 
	 * @param list
	 *          the list
	 * @param noMerging
	 *          the no merging
	 */
	protected ExperimentalRelaxationData(ArrayList<BondRelaxation> list, boolean noMerging)
	{
		if (list.isEmpty())
			throw new RelaxationRuntimeException("Relaxation data cannot be empty.");
		
		if (noMerging)
		{
			this.bondRelaxationList = list;
		}
		else
		{
			//same bonds must be merged into one group
			this.bondRelaxationList = new ArrayList<BondRelaxation>();
			
			for (BondRelaxation bondRelax : list)
			{
				BondRelaxation existingBondRelax = null;
				int existingIndex = -1;
				
				int index = 0;
				for (BondRelaxation currBondRelax : this.bondRelaxationList)
				{
					if (currBondRelax.getBond().equals(bondRelax.getBond()))
					{
						existingBondRelax = currBondRelax;
						existingIndex = index;
						break;
					}
					index++;
				}
				
				//if did not find
				if (existingBondRelax==null)
					this.bondRelaxationList.add(bondRelax);
				else
				{
					//if found same bonds already, merge with previous list
					ArrayList<RelaxationDatum> data = existingBondRelax.getRelaxationListRef();					
					data.addAll(bondRelax.getRelaxationListRef());
					
					//create a new bond
					this.bondRelaxationList.set(existingIndex, new BondRelaxation(existingBondRelax.getBond(), data));
				}
			}
		}
		
		//compute the total sizes
		int currSize = 0;
		boolean rigid = true;
		for (BondRelaxation b : this.bondRelaxationList)
		{
			currSize+= b.size();
			if (!b.isRigid())
				rigid = false;
		}
		
		this.rigid = rigid;		
		this.totalSize = currSize;
	}

	/**
	 * Creates the from chain.
	 * 
	 * @param chainID
	 *          the chain id
	 * @return the experimental relaxation
	 */
	public ExperimentalRelaxationData createFromChain(String chainID)
	{
		ArrayList<BondRelaxation> bondList = new ArrayList<BondRelaxation>();

		for (BondRelaxation b: this.bondRelaxationList)
		{
			if (b.getBond().getFromAtom().getPrimaryInfo().getChainID().equalsIgnoreCase(chainID))
				bondList.add(b);
		}
		
		return new ExperimentalRelaxationData(bondList, true);
	}
	
	/**
	 * Creates the from frequency.
	 * 
	 * @param frequency
	 *          the frequency
	 * @return the experimental relaxation
	 */
	public ExperimentalRelaxationData createFromFrequency(double frequency)
	{
		ArrayList<BondRelaxation> bondList = new ArrayList<BondRelaxation>();

		for (BondRelaxation b: this.bondRelaxationList)
		{
			for (RelaxationDatum data : b.getRelaxationListRef())
			{
				if (data.getFrequency() == frequency)
				{
		      ArrayList<RelaxationDatum> dataList = new ArrayList<RelaxationDatum>();
		      dataList.add(data);
		      
					//create bond of one frequency
		      BondRelaxation bondRelax = new BondRelaxation(b.getBond(), dataList);
		      
		      //add the new predictor into the list
					bondList.add(bondRelax);
				}
			}
		}
		
		return new ExperimentalRelaxationData(bondList, true);
	}
	
	/**
	 * Creates the from type.
	 * 
	 * @param type
	 *          the type
	 * @return the experimental relaxation
	 */
	public ExperimentalRelaxationData createFromType(BondType type)
	{
		ArrayList<BondRelaxation> bondList = new ArrayList<BondRelaxation>();

		for (BondRelaxation b: this.bondRelaxationList)
		{
			if (b.getBond().getBondType().equals(type))
				bondList.add(b);
		}
		
		return new ExperimentalRelaxationData(bondList, true);
	}

	/**
	 * Creates the monte carlo list.
	 * 
	 * @return the experimental relaxation
	 */
	public ExperimentalRelaxationData createMonteCarlo()
	{
		ArrayList<BondRelaxation> bondList = new ArrayList<BondRelaxation>(this.bondRelaxationList.size());
		for (int iter=0; iter<this.bondRelaxationList.size(); iter++)
			bondList.add(this.bondRelaxationList.get(iter).createMonteCarlo());
			
		return new ExperimentalRelaxationData(bondList, true);		
	}
	
	/**
	 * Creates the rigid.
	 * 
	 * @return the experimental relaxation
	 */
	public ExperimentalRelaxationData createRigid()
	{
		if (isRigid())
			return this;
		
		ArrayList<BondRelaxation> bondList = new ArrayList<BondRelaxation>();

		for (BondRelaxation b: this.bondRelaxationList)
		{
			if (b.isRigid())
			{
	      //add the new predictor into the list
				bondList.add(b);
			}
		}
		
		return new ExperimentalRelaxationData(bondList, true);
	}
	
	/**
	 * Creates the from molecule.
	 * 
	 * @param mol
	 *          the mol
	 * @return the experimental relaxation
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public ExperimentalRelaxationData createFromExistingAtoms(Molecule mol)
	{
		ArrayList<BondRelaxation> bondList = new ArrayList<BondRelaxation>();
		for (BondRelaxation bondRelax : this.bondRelaxationList)
		{
			if (mol.exists(bondRelax.getBond().getFromAtom()) && mol.exists(bondRelax.getBond().getToAtom()))
			{
				bondList.add(bondRelax);
			}
		}
		
		return new ExperimentalRelaxationData(bondList, true);
	}
	
	public ExperimentalRelaxationData createWithUpdatedRhos(List<? extends ModelFreePredictor> predictors)
	{
		if (predictors.size()!=size())
			throw new RelaxationRuntimeException("Number of predictors must match number of bonds.");
		
		ArrayList<BondRelaxation> newBondList = new ArrayList<BondRelaxation>(this.bondRelaxationList.size());
		Iterator<? extends ModelFreePredictor> predictorIter = predictors.iterator();
		for (BondRelaxation b: this.bondRelaxationList)
		{
			ModelFreePredictor predictor = predictorIter.next();
			ArrayList<RelaxationDatum> updatedData = new ArrayList<RelaxationDatum>(b.size());
			for (RelaxationDatum datum : b.getRelaxationListRef())
			{
				RelaxationDatum newDatum = datum.createWithRho(RelaxationDatum.computeRho(datum, predictor));
				
				updatedData.add(newDatum);
			}

			newBondList.add(new BondRelaxation(b, updatedData));
		}
		
		return new ExperimentalRelaxationData(newBondList, true);
	}

	
	public Atom firstMissingAtom(Molecule mol)
	{
		for (BondRelaxation bondRelax : this.bondRelaxationList)
		{
			if (!mol.exists(bondRelax.getBond().getFromAtom()))
				return bondRelax.getBond().getFromAtom();
			if (!mol.exists(bondRelax.getBond().getToAtom()))
				return bondRelax.getBond().getToAtom();
		}
		
		return null;
	}

	/**
	 * Gets the bond relaxation.
	 * 
	 * @param index
	 *          the index
	 * @return the bond relaxation
	 */
	public BondRelaxation getBondRelaxation(int index)
	{
		return this.bondRelaxationList.get(index);
	}
	
	/**
	 * Gets the bond relaxation list.
	 * 
	 * @return the bond relaxation list
	 */
	public ArrayList<BondRelaxation> getBondRelaxationData()
	{
		return new ArrayList<BondRelaxation>(this.bondRelaxationList);
	}
	
	/**
	 * Gets the bond relaxation list ref.
	 * 
	 * @return the bond relaxation list ref
	 */
	protected ArrayList<BondRelaxation> getBondRelaxationDataRef()
	{
		return this.bondRelaxationList;
	}
	
	/**
	 * Gets the bonds.
	 * 
	 * @return the bonds
	 */
	public ArrayList<Bond> getBonds()
	{
		ArrayList<Bond> vectorList = new ArrayList<Bond>();
		for (BondRelaxation b : this.bondRelaxationList)
				vectorList.add(b.getBond());
		
		return vectorList;	
	}
	
	/**
	 * Gets the bond types.
	 * 
	 * @return the bond types
	 */
	public ArrayList<BondType> getBondTypes()
	{
		ArrayList<BondType> bonds = new ArrayList<BondType>();
		
		for (BondRelaxation b : this.bondRelaxationList)
		{
			int index = bonds.indexOf(b.getBond().getBondType());
			if (index<0)
				bonds.add(b.getBond().getBondType());
		}
		
		//sort the result
		Collections.sort(bonds);
		
		return bonds;
	}

	/**
	 * Gets the chains.
	 * 
	 * @return the chains
	 */
	public ArrayList<String> getChains()
	{
		ArrayList<String> bonds = new ArrayList<String>();
		
		for (BondRelaxation b : this.bondRelaxationList)
		{
			int index = bonds.indexOf(b.getBond().getFromAtom().getPrimaryInfo().getChainID());
			if (index<0)
				bonds.add(b.getBond().getFromAtom().getPrimaryInfo().getChainID());
		}
		
		//sort the result
		Collections.sort(bonds);
		
		return bonds;
	}
	

	/**
	 * Gets the frequency types.
	 * 
	 * @return the frequency types
	 */
	public ArrayList<Double> getFrequencyTypes()
	{
		ArrayList<Double> freqList = new ArrayList<Double>();
		
		for (BondRelaxation b : this.bondRelaxationList)
		{
			for (RelaxationDatum data : b.getRelaxationListRef())
			{
				int index = freqList.indexOf(data.getFrequency());
				if (index<0)
					freqList.add(data.getFrequency());				
			}
		}
		
		//sort the result
		Collections.sort(freqList);
		
		return freqList;
	}
	
	public BaseExperimentalData getNoeData()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(sizeRho());
		for (BondRelaxation relax : this.bondRelaxationList)
			data.addAll(relax.getNoeData());
		
		return new BaseExperimentalData(data);		
	}
	
	public BaseExperimentalData getObservedData()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(this.totalSize*3);
		for (BondRelaxation relax : this.bondRelaxationList)
			for (BaseExperimentalDatum datum : relax.getObservedData())
				data.add(datum);
		
		return  new BaseExperimentalData(data);
	}
		
	public BaseExperimentalData getR1Data()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(sizeRho());
		for (BondRelaxation relax : this.bondRelaxationList)
			data.addAll(relax.getR1Data());
		
		return new BaseExperimentalData(data);		
	}
	
	public BaseExperimentalData getR2Data()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(sizeRho());
		for (BondRelaxation relax : this.bondRelaxationList)
			data.addAll(relax.getR2Data());
		
		return new BaseExperimentalData(data);		
	}
	
	public BaseExperimentalData getRatioData()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(sizeRho());
		for (BondRelaxation relax : this.bondRelaxationList)
			for (BaseExperimentalDatum datum : relax.getRatioData())
				data.add(datum);
		
		return new BaseExperimentalData(data);
	}
	
	public BaseExperimentalData getRhoData()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(sizeRho());
		for (BondRelaxation relax : this.bondRelaxationList)
			data.addAll(relax.getRhoData());
		
		return new BaseExperimentalData(data);		
	}
	
	public boolean isRigid()
	{
		return this.rigid;
	}

	public ArrayList<ExperimentalRelaxationData> seperateByBondType()
	{
		ArrayList<BondType> types = getBondTypes();
		
		ArrayList<ExperimentalRelaxationData> relaxLists = new ArrayList<ExperimentalRelaxationData>(types.size());
		for (BondType type : types)
		{
			relaxLists.add(createFromType(type));
		}
		
		return relaxLists;
	}
	
	/**
	 * Seperate by bond type and frequency.
	 * 
	 * @return the array list
	 */
	public ArrayList<ExperimentalRelaxationData> seperateByBondTypeAndFrequency()
	{
		ArrayList<ExperimentalRelaxationData> relaxLists = new ArrayList<ExperimentalRelaxationData>();
		
		ArrayList<ExperimentalRelaxationData> relaxBondTypes = seperateByBondType();
		for (ExperimentalRelaxationData list : relaxBondTypes)
		{
			ArrayList<ExperimentalRelaxationData> relaxFreq = list.seperateByFrequency();
			for (ExperimentalRelaxationData freqList : relaxFreq)
				relaxLists.add(freqList);
		}
		
		return relaxLists;
	}
	
	/**
	 * Seperate by chain.
	 * 
	 * @return the array list
	 */
	public ArrayList<ExperimentalRelaxationData> seperateByChain()
	{
		ArrayList<String> types = getChains();
		
		ArrayList<ExperimentalRelaxationData> relaxLists = new ArrayList<ExperimentalRelaxationData>(types.size());
		for (String type : types)
		{
			relaxLists.add(createFromChain(type));
		}
		
		return relaxLists;
	}

	/**
	 * Seperate by frequency.
	 * 
	 * @return the array list
	 */
	public ArrayList<ExperimentalRelaxationData> seperateByFrequency()
	{
		ArrayList<Double> types = getFrequencyTypes();
		
		ArrayList<ExperimentalRelaxationData> relaxLists = new ArrayList<ExperimentalRelaxationData>(types.size());
		for (double freq : types)
		{
			relaxLists.add(createFromFrequency(freq));
		}
		
		return relaxLists;
	}

	/**
	 * Size.
	 * 
	 * @return the int
	 */
	public int size()
	{
		return this.bondRelaxationList.size();
	}

	/**
	 * Size data.
	 * 
	 * @return the int
	 */
	public int sizeRho()
	{
		return this.totalSize;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		
		for (int iter=0; iter<this.bondRelaxationList.size(); iter++)
			str.append("  "+this.bondRelaxationList.get(iter)+"\n");
		
		return str.toString();	
	}
}
