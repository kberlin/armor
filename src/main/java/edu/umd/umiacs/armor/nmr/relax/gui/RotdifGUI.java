/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax.gui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.border.TitledBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Color;

import javax.swing.JRadioButton;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.PackageInfo;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Model;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.nmr.relax.AbstractModelFreeComputor;
import edu.umd.umiacs.armor.nmr.relax.AnisotropicRelaxationComputor;
import edu.umd.umiacs.armor.nmr.relax.ElmPredictor;
import edu.umd.umiacs.armor.nmr.relax.ExperimentalRelaxationData;
import edu.umd.umiacs.armor.nmr.relax.MoleculeRelaxationPredictor;
import edu.umd.umiacs.armor.nmr.relax.RelaxationRuntimeException;
import edu.umd.umiacs.armor.nmr.relax.RotationalDiffusionTensor;
import edu.umd.umiacs.armor.nmr.relax.RotdifResults;
import edu.umd.umiacs.armor.nmr.relax.TwoDomainRelaxationAligner;
import edu.umd.umiacs.armor.nmr.relax.gui.Chi2Plot;
import edu.umd.umiacs.armor.nmr.relax.gui.DiffusionTensorFitPlot;
import edu.umd.umiacs.armor.nmr.relax.gui.ExperimentalRatioPlot;
import edu.umd.umiacs.armor.nmr.relax.gui.OrderParameterPlot;
import edu.umd.umiacs.armor.nmr.relax.gui.ThreeDimensionPlot;
import edu.umd.umiacs.armor.nmr.relax.gui.TwoDimensionPrediction;
import edu.umd.umiacs.armor.nmr.relax.gui.VectorPlot;
import edu.umd.umiacs.armor.refinement.ConvexAlignmentTranslationDocker;
import edu.umd.umiacs.armor.refinement.DockInfo;
import edu.umd.umiacs.armor.util.FileEditor;
import edu.umd.umiacs.armor.util.FileIO;
import edu.umd.umiacs.armor.util.ProgressObserver;

import javax.swing.JProgressBar;
import javax.swing.ButtonGroup;
import javax.swing.JSpinner;

import java.awt.Cursor;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JFormattedTextField;

import java.awt.Dimension;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JCheckBox;

public class RotdifGUI extends JFrame
{	
	/**
	 * The Class ElmDockTask.
	 */
	private class ElmDockTask extends SwingWorker<Void, Void> implements ProgressObserver
	{
		
		/** The background exception. */
		private Exception backgroundException = null;
		
		/** The elm dock. */
		private ConvexAlignmentTranslationDocker elmDock;
		
		/** The main frame. */
		private final JFrame mainFrame;
		
		/**
		 * Instantiates a new elm dock task.
		 * 
		 * @param frame
		 *          the frame
		 */
		public ElmDockTask(JFrame frame)
		{
			this.mainFrame = frame;
		}
		
   /*
     * Main task. Executed in background thread.
     */
    /* (non-Javadoc)
    * @see javax.swing.SwingWorker#doInBackground()
    */
   @Override
    public Void doInBackground() 
    {
    	try
    	{	
	  	  Model model1 = RotdifGUI.this.pdbModel.createChain(0);
	  	  Model model2 = RotdifGUI.this.pdbModel.createChain(1);
	  	  
	  	  BasicMolecule m1 = model1.createBasicMolecule();
	  	  BasicMolecule m2 = model2.createBasicMolecule();

	    	TwoDomainRelaxationAligner computor = new TwoDomainRelaxationAligner(RotdifGUI.this.experimentalData, RotdifGUI.this.robustLeastSquaresButton.isSelected());

	    	//generate the predictors
	    	double temperature = Double.valueOf(RotdifGUI.this.temperatureTextField.getText());
	    	ElmPredictor elmPredictor = new ElmPredictor(temperature);
	    	MoleculeRelaxationPredictor relaxPredictor = new MoleculeRelaxationPredictor(computor.getComputor(), elmPredictor);

	    	//create the docker
	    	this.elmDock = new ConvexAlignmentTranslationDocker(computor, relaxPredictor, 0.5);
	    	this.elmDock.addProgressObserver(this);
	    		      
	    	//compute the result
	    	List<DockInfo> dockResults = this.elmDock.dock(m1, m2);
	    		    	
	    	RotdifGUI.this.elmdockResults = RotdifGUI.this.pdbStruct.createFromMoleculeList(new MultiDomainComplex(m1, m2).createSecondDomainTranslatedDockList(dockResults));
    	}
     	catch (Exception e)
    	{
     		this.backgroundException = e;
    	}
     
      return null;
    }

    /*
     * Executed in event dispatching thread
     */
    /* (non-Javadoc)
     * @see javax.swing.SwingWorker#done()
     */
    @Override
    public void done() 
    {
      if (this.backgroundException!=null)
      {
      	this.backgroundException.printStackTrace();
    		JOptionPane.showMessageDialog(this.mainFrame,
    				this.backgroundException.getMessage(),
    		    "ELMDOCK Error",
    		    JOptionPane.ERROR_MESSAGE);
      }
      else
    	{
      	RotdifGUI.this.elmdockSaveButton.setEnabled(true);
    	} 

  		RotdifGUI.this.elmdockButton.setEnabled(true);
    }

		/* (non-Javadoc)
		 * @see edu.umd.umiacs.armor.util.ProgressObserver#update(double)
		 */
		@Override
		public void update(double progressPercentage)
		{
			RotdifGUI.this.elmdockProgressBar.setIndeterminate(false);
			setProgress((int)Math.round(progressPercentage*100.0));
		}
	}

	/**
	 * The Class RunTask.
	 */
	private class RunTask extends SwingWorker<Void, Double> implements ProgressObserver
	{
		
		/** The background exception. */
		private volatile Exception backgroundException = null;
		
		/** The main frame. */
		private final JFrame mainFrame;
		
		/**
		 * Instantiates a new run task.
		 * 
		 * @param frame
		 *          the frame
		 */
		public RunTask(JFrame frame)
		{
			this.mainFrame = frame;
		}
		
   /*
     * Main task. Executed in background thread.
     */
    /* (non-Javadoc)
    * @see javax.swing.SwingWorker#doInBackground()
    */
   @Override
    public Void doInBackground() 
    {
    	try
    	{	
	  		RotdifGUI.this.runProgressBar.setIndeterminate(false);
	    	setProgress(0);
	    	
				//init the solution class
	    	ArrayList<ProgressObserver> initialObservers = new ArrayList<ProgressObserver>(1);
	    	initialObservers.add(this);
	    	
	    	RotdifGUI.this.results = new RotdifResults(RotdifGUI.this.experimentalData, RotdifGUI.this.pdbMolecule, RotdifGUI.this.chckbxFullStatistics.isSelected(), true, RotdifGUI.this.robustLeastSquaresButton.isSelected(), initialObservers);      
	      setGraphButtons(true);
	      
	      //compute the statistic
	      RotdifGUI.this.results.computeStatistic();
	      
    	}
     	catch (Exception e)
    	{
     		this.backgroundException = e;
    	}
     
      return null;
    }

    /*
     * Executed in event dispatching thread
     */
    /* (non-Javadoc)
     * @see javax.swing.SwingWorker#done()
     */
    @Override
    public void done() 
    {
      if (this.backgroundException!=null)
      {
      	this.backgroundException.printStackTrace();
    		JOptionPane.showMessageDialog(this.mainFrame,
    				this.backgroundException.getMessage(),
    		    "ROTDIF Error",
    		    JOptionPane.ERROR_MESSAGE);
      }
      else
    	{
      	RotdifGUI.this.detailedResultsButton.setEnabled(true);
    	} 

      RotdifGUI.this.runButton.setEnabled(true);
    }

		@Override
		protected void process(List<Double> progressPercentages)
		{
			double progressPercentage = progressPercentages.get(progressPercentages.size()-1);
			setProgress((int)Math.round(progressPercentage*100.0));			
		}

		/* (non-Javadoc)
		 * @see edu.umd.umiacs.armor.util.ProgressObserver#update(double)
		 */
		@Override
		public void update(double progressPercentage)
		{
			publish(new Double(progressPercentage));
		}
	}
	
	/** The active bond button. */
	private JButton activeBondButton;
	
	/** The ani fit button. */
	private JButton aniFitButton;
	
	/** The ani fit graph. */
	private DiffusionTensorFitPlot aniFitGraph = null;
	
	/** The averaged model button. */
	private JRadioButton averagedModelButton;
	
	private JFileChooser axesSaveFileChooser;
	
	/** The axial fit button. */
	private JButton axialFitButton;	
	
	/** The axial fit graph. */
	private DiffusionTensorFitPlot axialFitGraph = null;	
	
	/** The center panel. */
	private JPanel centerPanel = new JPanel();
	
	/** The chckbx full statistics. */
	private JCheckBox chckbxFullStatistics;	
	
	/** The chi2 button. */
	private JButton chi2Button;
	
	/** The chi2 plot. */
	private Chi2Plot chi2Plot = null;
	
	/** The detailed editor. */
	private FileEditor detailedEditor = null;
	
	/** The detailed results button. */
	private JButton detailedResultsButton;
	
	private JButton dynamicsButton;

	private OrderParameterPlot dynamicsPlot = null;
	
	/** The elmdock button. */
	private JButton elmdockButton;
	
	/** The elmdock progress bar. */
	private JProgressBar elmdockProgressBar; 
	
	/** The elmdock results. */
	private PdbStructure elmdockResults = null;
	
	/** The elmdock save button. */
	private JButton elmdockSaveButton;
	
	/** The elmdock save file chooser. */
	private JFileChooser elmdockSaveFileChooser;
	
	/** The elm prediction button. */
	private JButton elmPredictionButton;
	
	/** The elm prediction editor. */
	private FileEditor elmPredictionEditor = null;
	
	/** The experimental data. */
	private ExperimentalRelaxationData experimentalData = null;
	
	/** The exp rho button. */
	private JButton expRhoButton;
	
	/** The help button. */
	private JButton helpButton;
	
	/** The help editor. */
	private FileEditor helpEditor = null;
	
	private JButton isoFitButton;
	
	private DiffusionTensorFitPlot isoFitGraph = null; 
	
	/** The least squares button. */
	private JRadioButton leastSquaresButton;
	
	/** The model button group. */
	private final ButtonGroup modelButtonGroup = new ButtonGroup();
	
	/** The model text field. */
	private JSpinner modelTextField;
	
	/** The pdb file. */
	private File pdbFile = null;
	
	/** The pdb file chooser. */
	private JFileChooser pdbFileChooser;
	
	/** The pdb file status label. */
	private JLabel pdbFileStatusLabel;
	
	/** The pdb file text field. */
	private JTextField pdbFileTextField;
	
	/** The pdb model. */
	private Model pdbModel;
	
	/** The pdb molecule. */
	private BasicMolecule pdbMolecule = null;
	
	/** The pdb struct. */
	private PdbStructure pdbStruct= null;
	
	/** The ratio plot. */
	private ExperimentalRatioPlot ratioPlot = null;
	
	/** The regression button group. */
	private final ButtonGroup regressionButtonGroup = new ButtonGroup();
	
	/** The relaxation file status label. */
	private JLabel relaxationFileStatusLabel;
	
	/** The relaxation file text. */
	private JTextField relaxationFileText;
	
	/** The relax file. */
	private File relaxFile = null;
	
	/** The relax file chooser. */
	private JFileChooser relaxFileChooser;
	
	/** The reload button. */
	private JButton reloadButton;
	
	/** The results. */
	private RotdifResults results = null;
	
	/** The robust least squares button. */
	private JRadioButton robustLeastSquaresButton;
	
	/** The run button. */
	private JButton runButton;
	
	/** The run progress bar. */
	private JProgressBar runProgressBar;
	
	private JButton saveTensorAxesButton;

	/** The selected model button. */
	private JRadioButton selectedModelButton;

	/** The temperature text field. */
	private JFormattedTextField temperatureTextField;
	
	/** The three dimension fit button. */
	private JButton threeDimensionFitButton;
	
	/** The three dim plot. */
	private ThreeDimensionPlot threeDimPlot = null;
	
	/** The two dimension fit button. */
	private JButton twoDimensionFitButton;
	
	/** The two dim fit graph. */
	private TwoDimensionPrediction twoDimFitGraph = null;
	
	/** The vector plot. */
	private VectorPlot vectorPlot = null;
	
	/** The version label. */
	private JLabel versionLabel;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4297710166970457273L;

	private final static String VERSION = "3.1";

	public static void mainGUI()
	{
		try {
      // Set System L&F
		  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} 
		catch (UnsupportedLookAndFeelException e) {
		 // handle exception
		}
		catch (ClassNotFoundException e) {
		 // handle exception
		}
		catch (InstantiationException e) {
		 // handle exception
		}
		catch (IllegalAccessException e) {
		 // handle exception
		}
		
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					//check if the library is missing
					RotdifGUI window = new RotdifGUI();
					
					window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					window.setVisible(true);
					
					try
					{
						Class.forName("org.apache.commons.math3.linear.ArrayRealVector");
					}
					catch (ClassNotFoundException e)
					{
						JOptionPane.showMessageDialog(window,
						    "Library directory not found.",
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
						
					}
						
				} catch (Exception e)
				{
					
					e.printStackTrace();
				}
			}
		});		
	}
	
	/**
	 * Instantiates a new rotdif gui.
	 */
	public RotdifGUI()
	{
		setTitle("Rotdif "+VERSION);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		ToolTipManager.sharedInstance().setInitialDelay(500);
		
		getContentPane().add(this.centerPanel);
		this.centerPanel.setLayout(new BoxLayout(this.centerPanel, BoxLayout.Y_AXIS));
		
		JPanel settingsPanel = new JPanel();
		this.centerPanel.add(settingsPanel);
		settingsPanel.setBorder(new TitledBorder(null, "Settings", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JPanel dataOptionsPanel = new JPanel();
		dataOptionsPanel.setToolTipText("Relaxation input data.");
		dataOptionsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Data", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		dataOptionsPanel.setLayout(new BoxLayout(dataOptionsPanel, BoxLayout.Y_AXIS));
		
		JPanel relaxationFilePanel = new JPanel();
		relaxationFilePanel.setBorder(null);
		dataOptionsPanel.add(relaxationFilePanel);
		GridBagLayout gbl_relaxationFilePanel = new GridBagLayout();
		gbl_relaxationFilePanel.columnWidths = new int[]{106, 434, 0, 88};
		gbl_relaxationFilePanel.rowHeights = new int[]{30, 24};
		gbl_relaxationFilePanel.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		gbl_relaxationFilePanel.rowWeights = new double[]{0.0,Double.MIN_VALUE};
		relaxationFilePanel.setLayout(gbl_relaxationFilePanel);
		
		this.relaxFileChooser = new JFileChooser(new File(".").getAbsolutePath());
		this.relaxFileChooser.setFileFilter(new FileNameExtensionFilter("Relaxation Files","txt","dat"));
		
		JLabel relaxationFileLabel = new JLabel("Relaxation File");
		relaxationFileLabel.setToolTipText("Relaxation input data file.");
		GridBagConstraints gbc_relaxationFileLabel = new GridBagConstraints();
		gbc_relaxationFileLabel.fill = GridBagConstraints.HORIZONTAL;
		gbc_relaxationFileLabel.insets = new Insets(0, 0, 5, 5);
		gbc_relaxationFileLabel.gridx = 0;
		gbc_relaxationFileLabel.gridy = 0;
		relaxationFilePanel.add(relaxationFileLabel, gbc_relaxationFileLabel);
		
		this.relaxationFileText = new JTextField();
		this.relaxationFileText.setToolTipText("Path of the current relaxation input file.");
		this.relaxationFileText.setEditable(false);
		this.relaxationFileText.setColumns(40);
		GridBagConstraints gbc_relaxationFileText = new GridBagConstraints();
		gbc_relaxationFileText.fill = GridBagConstraints.HORIZONTAL;
		gbc_relaxationFileText.anchor = GridBagConstraints.NORTH;
		gbc_relaxationFileText.insets = new Insets(0, 0, 5, 5);
		gbc_relaxationFileText.gridx = 1;
		gbc_relaxationFileText.gridy = 0;
		relaxationFilePanel.add(this.relaxationFileText, gbc_relaxationFileText);
				
		this.reloadButton = new JButton("Reload");
		GridBagConstraints gbc_reloadButton = new GridBagConstraints();
		gbc_reloadButton.insets = new Insets(0, 0, 5, 5);
		gbc_reloadButton.gridx = 2;
		gbc_reloadButton.gridy = 0;
		relaxationFilePanel.add(this.reloadButton, gbc_reloadButton);
		this.reloadButton.setEnabled(false);
		
		this.reloadButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				readDataFile();
			}
		});

		final JButton selectRelaxationFileButton = new JButton("Browse");
		selectRelaxationFileButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				int returnVal = RotdifGUI.this.relaxFileChooser.showOpenDialog(selectRelaxationFileButton);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					RotdifGUI.this.relaxFile = RotdifGUI.this.relaxFileChooser.getSelectedFile();
					RotdifGUI.this.relaxationFileText.setText(RotdifGUI.this.relaxFileChooser.getSelectedFile().getName());
					RotdifGUI.this.reloadButton.setEnabled(true);
					readDataFile();						
				}
			}
		});
		GridBagConstraints gbc_selectRelaxationFileButton = new GridBagConstraints();
		gbc_selectRelaxationFileButton.anchor = GridBagConstraints.SOUTH;
		gbc_selectRelaxationFileButton.insets = new Insets(0, 0, 5, 0);
		gbc_selectRelaxationFileButton.gridx = 3;
		gbc_selectRelaxationFileButton.gridy = 0;
		relaxationFilePanel.add(selectRelaxationFileButton, gbc_selectRelaxationFileButton);

		this.relaxationFileStatusLabel = new JLabel("status: empty");
		this.relaxationFileStatusLabel.setToolTipText("Current status of the relaxation input file. Must be \"success\" in order to continue.");
		this.relaxationFileStatusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_relaxationFileStatusLabel = new GridBagConstraints();
		gbc_relaxationFileStatusLabel.gridwidth = 4;
		gbc_relaxationFileStatusLabel.anchor = GridBagConstraints.NORTHWEST;
		gbc_relaxationFileStatusLabel.gridx = 0;
		gbc_relaxationFileStatusLabel.gridy = 1;
		relaxationFilePanel.add(this.relaxationFileStatusLabel, gbc_relaxationFileStatusLabel);
		
		JPanel pdbFilePanel = new JPanel();
		dataOptionsPanel.add(pdbFilePanel);
		GridBagLayout gbl_pdbFilePanel = new GridBagLayout();
		gbl_pdbFilePanel.columnWidths = new int[]{50, 162, 303, 88, 0};
		gbl_pdbFilePanel.rowHeights = new int[]{29, 38, 0};
		gbl_pdbFilePanel.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_pdbFilePanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		pdbFilePanel.setLayout(gbl_pdbFilePanel);
		
		JLabel pdbFileLabel = new JLabel("PDB File");
		pdbFileLabel.setToolTipText("The PDB input file for processing.");
		GridBagConstraints gbc_pdbFileLabel = new GridBagConstraints();
		gbc_pdbFileLabel.anchor = GridBagConstraints.WEST;
		gbc_pdbFileLabel.insets = new Insets(0, 0, 5, 5);
		gbc_pdbFileLabel.gridx = 0;
		gbc_pdbFileLabel.gridy = 0;
		pdbFilePanel.add(pdbFileLabel, gbc_pdbFileLabel);
		
		this.pdbFileTextField = new JTextField();
		this.pdbFileTextField.setToolTipText("Path of the current PDB input file.");
		this.pdbFileTextField.setEditable(false);
		this.pdbFileTextField.setColumns(38);
		GridBagConstraints gbc_pdbFileTextField = new GridBagConstraints();
		gbc_pdbFileTextField.gridwidth = 2;
		gbc_pdbFileTextField.fill = GridBagConstraints.HORIZONTAL;
		gbc_pdbFileTextField.anchor = GridBagConstraints.NORTH;
		gbc_pdbFileTextField.insets = new Insets(0, 0, 5, 5);
		gbc_pdbFileTextField.gridx = 1;
		gbc_pdbFileTextField.gridy = 0;
		pdbFilePanel.add(this.pdbFileTextField, gbc_pdbFileTextField);
		
		this.pdbFileChooser = new JFileChooser(new File(".").getAbsolutePath());
		this.pdbFileChooser.setFileFilter(new FileNameExtensionFilter("PDB Files","pdb"));
		
		final JButton pdbBrowseButton = new JButton("Browse");
		pdbBrowseButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{		
				int returnVal = RotdifGUI.this.pdbFileChooser.showOpenDialog(pdbBrowseButton);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
					try
					{
						RotdifGUI.this.pdbFile = RotdifGUI.this.pdbFileChooser.getSelectedFile();
						RotdifGUI.this.pdbFileTextField.setText(RotdifGUI.this.pdbFileChooser.getSelectedFile().getName());

						RotdifGUI.this.pdbStruct = MoleculeFileIO.readPDB(RotdifGUI.this.pdbFile);

						RotdifGUI.this.pdbFileStatusLabel.setText("status: success");
						RotdifGUI.this.pdbFileStatusLabel.setForeground(Color.GREEN);
						
						RotdifGUI.this.modelTextField.setModel(new SpinnerNumberModel(1, 1, RotdifGUI.this.pdbStruct.getNumberOfModels(), 1));
						
					} 
					catch (Exception pdbException)
					{
						RotdifGUI.this.pdbFileStatusLabel.setText("status: failed....Could not parse file "+RotdifGUI.this.pdbFileChooser.getSelectedFile().getName());
						RotdifGUI.this.pdbFileStatusLabel.setForeground(Color.RED);
						RotdifGUI.this.pdbFileTextField.setText("*Invalid*");
					}

					expDataChangedAction();
				}
			}
		});
		GridBagConstraints gbc_pdbBrowseButton = new GridBagConstraints();
		gbc_pdbBrowseButton.anchor = GridBagConstraints.NORTH;
		gbc_pdbBrowseButton.insets = new Insets(0, 0, 5, 0);
		gbc_pdbBrowseButton.gridx = 3;
		gbc_pdbBrowseButton.gridy = 0;
		pdbFilePanel.add(pdbBrowseButton, gbc_pdbBrowseButton);
		
		JPanel modelSelectionPanel = new JPanel();
		modelSelectionPanel.setBorder(null);
		GridBagConstraints gbc_modelSelectionPanel = new GridBagConstraints();
		gbc_modelSelectionPanel.gridwidth = 4;
		gbc_modelSelectionPanel.anchor = GridBagConstraints.NORTHWEST;
		gbc_modelSelectionPanel.gridx = 0;
		gbc_modelSelectionPanel.gridy = 1;
		pdbFilePanel.add(modelSelectionPanel, gbc_modelSelectionPanel);
		
		this.averagedModelButton = new JRadioButton("Averaged");
		this.averagedModelButton.setToolTipText("Use Averaged Model");
		this.modelButtonGroup.add(this.averagedModelButton);
		modelSelectionPanel.add(this.averagedModelButton);
		
		this.selectedModelButton = new JRadioButton("Model");
		this.selectedModelButton.setToolTipText("Select Single Model");
		this.modelButtonGroup.add(this.selectedModelButton);
		this.selectedModelButton.setSelected(true);
		modelSelectionPanel.add(this.selectedModelButton);
		this.modelTextField = new JSpinner(new SpinnerNumberModel(1, 1, 1, 1));
		modelSelectionPanel.add(this.modelTextField);
		
		this.pdbFileStatusLabel = new JLabel("status: empty");
		modelSelectionPanel.add(this.pdbFileStatusLabel);
		
		JPanel optimizationPanel = new JPanel();
		optimizationPanel.setToolTipText("Run Options");
		optimizationPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Run Menu", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		optimizationPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Optimization Method", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		optimizationPanel.add(panel, BorderLayout.WEST);
		
		this.leastSquaresButton = new JRadioButton("Least-Squares");
		this.leastSquaresButton.setToolTipText("Performs standard chi2 minimization.");
		panel.add(this.leastSquaresButton);
		this.regressionButtonGroup.add(this.leastSquaresButton);
		this.leastSquaresButton.setSelected(true);
		
		this.robustLeastSquaresButton = new JRadioButton(String.format("Robust Least-Squares (%.1f\u03C3)",AbstractModelFreeComputor.OUTLIER_THRESHOLD));
		this.robustLeastSquaresButton.setToolTipText(String.format("Damps the contribution of outliers (residuals > %.1f\u03C3), in all regression methods. ",AbstractModelFreeComputor.OUTLIER_THRESHOLD));
		panel.add(this.robustLeastSquaresButton);
		this.regressionButtonGroup.add(this.robustLeastSquaresButton);
		settingsPanel.setLayout(new BorderLayout(0, 0));
		settingsPanel.add(dataOptionsPanel);
		
		settingsPanel.add(optimizationPanel, BorderLayout.SOUTH);
		
		JPanel rotdifRunPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) rotdifRunPanel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		rotdifRunPanel.setBorder(new TitledBorder(null, "Rotdif Execution", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		optimizationPanel.add(rotdifRunPanel, BorderLayout.EAST);
		
		this.chckbxFullStatistics = new JCheckBox("Full Statistics");
		this.chckbxFullStatistics.setHorizontalAlignment(SwingConstants.LEFT);
		this.chckbxFullStatistics.setToolTipText("Perform robust error analaysis, more accurate (required for publication) but slow. Turn off to increase speed.");
		this.chckbxFullStatistics.setSelected(false);
		rotdifRunPanel.add(this.chckbxFullStatistics);
		
		this.runButton = new JButton("Run");
		this.runButton.setToolTipText("Compute the results.");
		rotdifRunPanel.add(this.runButton);
		this.runButton.setEnabled(false);
		
		this.runProgressBar = new JProgressBar();
		this.runProgressBar.setPreferredSize(new Dimension(200, 20));
		rotdifRunPanel.add(this.runProgressBar);
		this.runButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				runButtonAction();
			}
		});
		
		JPanel graphsPanel = new JPanel();
		this.centerPanel.add(graphsPanel);
		graphsPanel.setBorder(new TitledBorder(null, "Graphs", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		graphsPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel expPlotsPanel = new JPanel();
		expPlotsPanel.setBorder(new TitledBorder(null, "Experimental Data Plots", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		graphsPanel.add(expPlotsPanel, BorderLayout.WEST);
		expPlotsPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		this.activeBondButton = new JButton("Bond Orientation");
		this.activeBondButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (RotdifGUI.this.vectorPlot!=null)
				{
					RotdifGUI.this.vectorPlot.setVisible(true);
					RotdifGUI.this.vectorPlot.toFront();
				}
				else
				{
					ExperimentalRelaxationData relaxData = RotdifGUI.this.experimentalData.createRigid();
					try
					{
						ArrayList<Bond> bondList = new ArrayList<Bond>(RotdifGUI.this.experimentalData.size());
						for (Bond bond : relaxData.getBonds())
							bondList.add(bond.createFromMolecule(RotdifGUI.this.pdbMolecule));

						RotdifGUI.this.vectorPlot = new VectorPlot(bondList);
					}
					catch (AtomNotFoundException e)
					{
						throw new RelaxationRuntimeException("Unexpected error.", e);
					}
				}
			
			}
		});
		
		this.expRhoButton = new JButton("Experimental Data");
		this.expRhoButton.setEnabled(false);
		this.expRhoButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if (RotdifGUI.this.ratioPlot!=null)
				{
					RotdifGUI.this.ratioPlot.setVisible(true);
					RotdifGUI.this.ratioPlot.toFront();
				}
				else
					RotdifGUI.this.ratioPlot = new ExperimentalRatioPlot(RotdifGUI.this.experimentalData);
			}
		});
		expPlotsPanel.add(this.expRhoButton);
		this.activeBondButton.setEnabled(false);
		expPlotsPanel.add(this.activeBondButton);
		
		JPanel resultPlotsPanel = new JPanel();
		resultPlotsPanel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Results", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		graphsPanel.add(resultPlotsPanel, BorderLayout.CENTER);
		resultPlotsPanel.setLayout(new GridLayout(0, 3, 0, 0));
		
		this.chi2Button = new JButton("\u03C7\u00B2");
		this.chi2Button.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (RotdifGUI.this.chi2Plot!=null)
				{
					RotdifGUI.this.chi2Plot.setVisible(true);
					RotdifGUI.this.chi2Plot.toFront();
				}
				else
					RotdifGUI.this.chi2Plot = new Chi2Plot(RotdifGUI.this.results.getAxiSolution());
			}
		});
		resultPlotsPanel.add(this.chi2Button);
		
		this.twoDimensionFitButton = new JButton("2D Model Fit");
		this.twoDimensionFitButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if (RotdifGUI.this.twoDimFitGraph!=null)
				{
					RotdifGUI.this.twoDimFitGraph.setVisible(true);
					RotdifGUI.this.twoDimFitGraph.toFront();
				}
				else
					RotdifGUI.this.twoDimFitGraph = new TwoDimensionPrediction(RotdifGUI.this.results.getAniSolution());
			}
		});
		resultPlotsPanel.add(this.twoDimensionFitButton);
		
		this.axialFitButton = new JButton("Axially Symmetric Fit");
		this.axialFitButton.setToolTipText("Correlation Plot of Axially Symmetric Fit");
		this.axialFitButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (RotdifGUI.this.axialFitGraph!=null)
				{
					RotdifGUI.this.axialFitGraph.setVisible(true);
					RotdifGUI.this.axialFitGraph.toFront();
				}
				else
					RotdifGUI.this.axialFitGraph = new DiffusionTensorFitPlot(RotdifGUI.this.results.getAxiSolution(), "Axially Symmetric Fit");
			}
		});
		
		this.threeDimensionFitButton = new JButton("3D Model Fit");
		this.threeDimensionFitButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (RotdifGUI.this.threeDimPlot!=null)
				{
					RotdifGUI.this.threeDimPlot.setVisible(true);
					RotdifGUI.this.threeDimPlot.toFront();
				}
				else
					RotdifGUI.this.threeDimPlot = new ThreeDimensionPlot(RotdifGUI.this.results.getAniSolution());
			}
		});
		resultPlotsPanel.add(this.threeDimensionFitButton);
		
		this.isoFitButton = new JButton("Isotropic Fit");
		this.isoFitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (RotdifGUI.this.isoFitGraph!=null)
				{
					RotdifGUI.this.isoFitGraph.setVisible(true);
					RotdifGUI.this.isoFitGraph.toFront();
				}
				else
					RotdifGUI.this.isoFitGraph = new DiffusionTensorFitPlot(RotdifGUI.this.results.getIsoSolution(), "Isotropic Fit");
			}
		});
		this.isoFitButton.setEnabled(false);
		resultPlotsPanel.add(this.isoFitButton);
		resultPlotsPanel.add(this.axialFitButton);
		
		this.aniFitButton = new JButton("Anisotropic Fit");
		this.aniFitButton.setToolTipText("Correlation Plot of Anisotropic Fit");
		this.aniFitButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				if (RotdifGUI.this.aniFitGraph!=null)
				{
					RotdifGUI.this.aniFitGraph.setVisible(true);
					RotdifGUI.this.aniFitGraph.toFront();
				}
				else
					RotdifGUI.this.aniFitGraph = new DiffusionTensorFitPlot(RotdifGUI.this.results.getAniSolution(), "Anisotropic Fit");
			}
		});
		resultPlotsPanel.add(this.aniFitButton);
		
		this.dynamicsButton = new JButton("Dynamics");
		this.dynamicsButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (RotdifGUI.this.dynamicsPlot!=null)
				{
					RotdifGUI.this.dynamicsPlot.setVisible(true);
					RotdifGUI.this.dynamicsPlot.toFront();
				}
				else
					RotdifGUI.this.dynamicsPlot = new OrderParameterPlot(RotdifGUI.this.results.getBestDynamicsSolution(),RotdifGUI.this.results.getBestSolutionType());
			}
		});
		this.dynamicsButton.setEnabled(false);
		resultPlotsPanel.add(this.dynamicsButton);
		
		this.axesSaveFileChooser = new JFileChooser(new File(".").getAbsolutePath());
		this.axesSaveFileChooser.setFileFilter(new FileNameExtensionFilter("Python Script Files","py"));
		this.saveTensorAxesButton = new JButton("Save PyMOL Axes");
		this.saveTensorAxesButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				int returnVal = RotdifGUI.this.axesSaveFileChooser.showSaveDialog(selectRelaxationFileButton);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
				  try
					{
				  	File saveFile = RotdifGUI.this.axesSaveFileChooser.getSelectedFile();				  	
				  	
				  	//generate the 3 files
				  	File saveAniFile = FileIO.ensureExtension(saveFile, "_ani", "py");
						File saveAxiFile = FileIO.ensureExtension(saveFile, "_axial", "py");
						File predictedSaveFile = FileIO.ensureExtension(saveFile, "_predicted", "py");

						double lengthAddition = 5.0;
						
						if (RotdifGUI.this.results.getAniTensor()!=null)
							MoleculeFileIO.writePymolAxesScript(saveAniFile, RotdifGUI.this.pdbMolecule, RotdifGUI.this.results.getAniTensor().getProperEigenDecomposition(), lengthAddition);
						if (RotdifGUI.this.results.getAxiTensor()!=null)
							MoleculeFileIO.writePymolAxesScript(saveAxiFile, RotdifGUI.this.pdbMolecule, RotdifGUI.this.results.getAxiTensor().getProperEigenDecomposition(), lengthAddition);
				  	MoleculeFileIO.writePymolAxesScript(predictedSaveFile, RotdifGUI.this.pdbMolecule, RotdifGUI.this.results.getUnscaledPredictedTensor().getProperEigenDecomposition(), lengthAddition);
					}
					catch (IOException e)
					{
		    		JOptionPane.showMessageDialog(RotdifGUI.this.centerPanel,
		    				e.getMessage(),
		    		    "File Write Error",
		    		    JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		this.saveTensorAxesButton.setEnabled(false);
		this.saveTensorAxesButton.setToolTipText("Create a three script files with the anisotropic ([file]_ani.py), axially-symmetric ([file]_axial.py), and ELM predicted ([file]_predicted.py) tensor axes.");
		resultPlotsPanel.add(this.saveTensorAxesButton);
		
		this.detailedResultsButton = new JButton("Detailed Results");
		resultPlotsPanel.add(this.detailedResultsButton);
		this.detailedResultsButton.setEnabled(false);
		this.detailedResultsButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (RotdifGUI.this.detailedEditor!=null)
				{
					RotdifGUI.this.detailedEditor.setVisible(true);
					RotdifGUI.this.detailedEditor.toFront();					
				}
				else
				{
					RotdifGUI.this.detailedEditor = new FileEditor(RotdifGUI.this.results.generateOutputString());
					RotdifGUI.this.detailedEditor.setTitle("Detailed ROTDIF Results");
					RotdifGUI.this.detailedEditor.setEditable(false);
				}
			}
		});
				
		JPanel elmdockPanel = new JPanel();
		elmdockPanel.setBorder(new TitledBorder(null, "ELMDOCK", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		this.centerPanel.add(elmdockPanel);
		elmdockPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel temperaturePanel = new JPanel();
		elmdockPanel.add(temperaturePanel, BorderLayout.WEST);
		
		JLabel temperatureLabel = new JLabel("Temperature (\u00B0K)");
		temperaturePanel.add(temperatureLabel);
		
		this.temperatureTextField = new JFormattedTextField(new DecimalFormat());
		this.temperatureTextField.setToolTipText("Temperature (in Kelvin) at which the experiment is to be simulation.");
		this.temperatureTextField.setEnabled(false);
		temperaturePanel.add(this.temperatureTextField);
		this.temperatureTextField.setText("298.0");
		this.temperatureTextField.setColumns(5);
		
		JPanel runElmdockPanel = new JPanel();
		elmdockPanel.add(runElmdockPanel, BorderLayout.CENTER);
		runElmdockPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		this.elmPredictionButton = new JButton("ELM Prediction");
		this.elmPredictionButton.setEnabled(false);
		this.elmPredictionButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (RotdifGUI.this.elmPredictionEditor!=null)
				{
					RotdifGUI.this.elmPredictionEditor.dispose();
					RotdifGUI.this.elmPredictionEditor = null;					
				}

				{
					RotdifGUI.this.centerPanel.setCursor(new Cursor(Cursor.WAIT_CURSOR));

					RotdifGUI.this.elmPredictionEditor = new FileEditor("Please Wait...");
					RotdifGUI.this.elmPredictionEditor.setTitle("ELM Prediction");
					RotdifGUI.this.elmPredictionEditor.setEditable(false);
					
					RotationalDiffusionTensor tensor = predictTensor();
					RotdifGUI.this.elmPredictionEditor.setMainText(tensor.toLongString());
					RotdifGUI.this.centerPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
				}
			}
		});
		this.elmPredictionButton.setHorizontalAlignment(SwingConstants.LEFT);
		runElmdockPanel.add(this.elmPredictionButton);
		
		this.elmdockButton = new JButton("Run ELMDOCK");
		this.elmdockButton.setEnabled(false);
		runElmdockPanel.add(this.elmdockButton);
		
		this.elmdockProgressBar = new JProgressBar();
		this.elmdockProgressBar.setPreferredSize(new Dimension(100, 20));
		runElmdockPanel.add(this.elmdockProgressBar);
		
		this.elmdockSaveButton = new JButton("Save Result");
		this.elmdockSaveButton.setEnabled(false);
		runElmdockPanel.add(this.elmdockSaveButton);
		this.elmdockSaveFileChooser = new JFileChooser(new File(".").getAbsolutePath());
		this.elmdockSaveFileChooser.setFileFilter(new FileNameExtensionFilter("PDB Files","pdb"));
		this.elmdockSaveButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				int returnVal = RotdifGUI.this.elmdockSaveFileChooser.showSaveDialog(RotdifGUI.this.elmdockSaveButton);
				if (returnVal == JFileChooser.APPROVE_OPTION)
				{
				  try
					{
				  	File dockedResultsFile = RotdifGUI.this.elmdockSaveFileChooser.getSelectedFile();
				  	dockedResultsFile = FileIO.ensureExtension(dockedResultsFile, "", "pdb"); 
				  	
				  	File dockedResultsBaseFile = new File(FileIO.removeExtension(dockedResultsFile.getAbsolutePath()));

				  	//write the results to file
						MoleculeFileIO.writePDB(dockedResultsFile, RotdifGUI.this.elmdockResults.toPDB());
						
					  //read the pdb file
					  PdbStructure dockedPdb = MoleculeFileIO.readPDB(dockedResultsFile);

						//compute and save the rotational diffusion tensor for all models
					  for (int iter=0; iter<dockedPdb.getNumberOfModels(); iter++)
					  {
					  	String name1 = dockedPdb.getModel(iter).getModelChains().get(0);
					  	String name2 = dockedPdb.getModel(iter).getModelChains().get(1);
					  	Molecule mol1 = dockedPdb.getModel(iter).createChain(0).createBasicMolecule();
					  	Molecule mol2 = dockedPdb.getModel(iter).createChain(1).createBasicMolecule();
					  	
				  	  ExperimentalRelaxationData relaxData1 = RotdifGUI.this.experimentalData.createFromExistingAtoms(mol1);	  
				  	  ExperimentalRelaxationData relaxData2 = RotdifGUI.this.experimentalData.createFromExistingAtoms(mol2);
				  	  
				    	AnisotropicRelaxationComputor d1Computor = new AnisotropicRelaxationComputor(relaxData1, RotdifGUI.this.robustLeastSquaresButton.isSelected());
				    	AnisotropicRelaxationComputor d2Computor = new AnisotropicRelaxationComputor(relaxData2, RotdifGUI.this.robustLeastSquaresButton.isSelected());

				    	RotationalDiffusionTensor tensor1 = d1Computor.computeTensor(mol1);
				    	RotationalDiffusionTensor tensor2 = d2Computor.computeTensor(mol2);
				    	
					  	File axes1 = FileIO.ensureExtension(dockedResultsBaseFile, "_axes_"+name1+"_"+(iter+1), "py");
					  	File axes2 = FileIO.ensureExtension(dockedResultsBaseFile, "_axes_"+name2+"_"+(iter+1), "py");
					  	
					  	double lengthAddition = 5.0;
					  	
						  MoleculeFileIO.writePymolAxesScript(axes1, mol1, tensor1.getProperEigenDecomposition(), lengthAddition);
						  MoleculeFileIO.writePymolAxesScript(axes2, mol2, tensor2.getProperEigenDecomposition(), lengthAddition);
					  }
					}
					catch (IOException e)
					{
		    		JOptionPane.showMessageDialog(RotdifGUI.this.centerPanel,
		    				e.getMessage(),
		    		    "ELMDOCK Error",
		    		    JOptionPane.ERROR_MESSAGE);
					}
					catch (PdbException e)
					{
		    		JOptionPane.showMessageDialog(RotdifGUI.this.centerPanel,
		    				e.getMessage(),
		    		    "File Error",
		    		    JOptionPane.ERROR_MESSAGE);
					}
					catch (AtomNotFoundException e)
					{
		    		JOptionPane.showMessageDialog(RotdifGUI.this.centerPanel,
		    				e.getMessage(),
		    		    "PDB Error",
		    		    JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		
		this.averagedModelButton.addItemListener(new ItemListener()
		{
			@Override
			public void itemStateChanged(ItemEvent arg0)
			{
				if (RotdifGUI.this.averagedModelButton.isSelected())
					expDataChangedAction();
			}
		});
		this.modelTextField.addChangeListener(new ChangeListener() 
		{
			@Override
			public void stateChanged(ChangeEvent arg0)
			{
				if (RotdifGUI.this.selectedModelButton.isSelected())
					expDataChangedAction();
			}
		});
		this.selectedModelButton.addItemListener(new ItemListener() 
		{
			@Override
			public void itemStateChanged(ItemEvent arg0)
			{
				if (RotdifGUI.this.selectedModelButton.isSelected())
					expDataChangedAction();
			}
		});
		
		this.elmdockButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				dockButtonAction();
			}
		});
		
		JPanel finalOptionsPanel = new JPanel();
		getContentPane().add(finalOptionsPanel, BorderLayout.SOUTH);
		finalOptionsPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel helpPanel = new JPanel();
		finalOptionsPanel.add(helpPanel, BorderLayout.WEST);
		
		JButton quitButton = new JButton("Quit");
		quitButton.setToolTipText("Quit Rotdif.");
		helpPanel.add(quitButton);
		
		this.helpButton = new JButton("Help");
		this.helpButton.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				if (RotdifGUI.this.helpEditor!=null)
				{
					RotdifGUI.this.helpEditor.setVisible(true);
					RotdifGUI.this.helpEditor.toFront();					
				}
				else
				{
					try
					{
						URL fileName = RotdifGUI.class.getClassLoader().getResource("edu/umd/umiacs/armor/nmr/relax/RotdifREADME.txt");
						
						if (fileName==null)
							throw new IOException("Help file not found.");
						
						RotdifGUI.this.helpEditor = new FileEditor(fileName.openStream());
						RotdifGUI.this.helpEditor.setTitle("ROTDIF Help");
						RotdifGUI.this.helpEditor.setEditable(false);
					}
					catch (IOException e)
					{
		    		JOptionPane.showMessageDialog(RotdifGUI.this.centerPanel,
		    				e.getMessage(),
		    		    "Rotdif Error",
		    		    JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		});
		this.helpButton.setToolTipText("Get help on how to use Rotdif.");
		helpPanel.add(this.helpButton);
		this.helpButton.setHorizontalAlignment(SwingConstants.LEFT);
		
		this.versionLabel = new JLabel("armor version: "+PackageInfo.ARMOR_VERSION);
		this.versionLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		finalOptionsPanel.add(this.versionLabel, BorderLayout.EAST);
		quitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		JLabel titleLabel = new JLabel("Rotdif "+VERSION);
		getContentPane().add(titleLabel, BorderLayout.NORTH);
		titleLabel.setVerticalAlignment(SwingConstants.TOP);
		titleLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 17));
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		setGraphButtons(false);
	}
	
	/**
	 * Close graph windows.
	 */
	private void closeGraphWindows()
	{
		if (this.ratioPlot!=null)
			this.ratioPlot.dispose();
		this.ratioPlot = null;

		if (this.vectorPlot!=null)
			this.vectorPlot.dispose();
		this.vectorPlot = null;		
		
		if (this.isoFitGraph!=null)
			this.isoFitGraph.dispose();
		this.isoFitGraph = null;
		
		if (this.axialFitGraph!=null)
			this.axialFitGraph.dispose();
		this.axialFitGraph = null;

		if (this.aniFitGraph!=null)
			this.aniFitGraph.dispose();
		this.aniFitGraph = null;

		if (this.detailedEditor!=null)
			this.detailedEditor.dispose();
		this.detailedEditor = null;

		if (this.twoDimFitGraph!=null)
			this.twoDimFitGraph.dispose();
		this.twoDimFitGraph = null;

		if (this.dynamicsPlot!=null)
			this.dynamicsPlot.dispose();
		this.dynamicsPlot = null;

		if (this.threeDimPlot!=null)
			this.threeDimPlot.dispose();
		this.threeDimPlot = null;
		
		if (this.elmPredictionEditor!=null)
			this.elmPredictionEditor.dispose();
		this.elmPredictionEditor = null;
	}
	
	/**
	 * Combine experimental data.
	 */
	private void combineExperimentalData()
	{
		if (!this.relaxationFileStatusLabel.getText().matches(".*success.*")|| !this.pdbFileStatusLabel.getText().matches(".*success.*"))
			return;
		
		try
		{
			this.pdbMolecule  = this.pdbModel.createBasicMolecule();
			Atom missingAtom = this.experimentalData.firstMissingAtom(this.pdbMolecule);
			if (missingAtom!=null)
				throw new PdbException("Could not find atom "+missingAtom.getPrimaryInfo()+" in the PDB file.");
		    
		   ArrayList<ExperimentalRelaxationData> relaxLists = this.experimentalData.seperateByChain(); 
		   if (relaxLists.size()==2)
		   	setElmdock(true);
		   else
		   	setElmdock(false);
		    
				this.activeBondButton.setEnabled(true);
				this.runButton.setEnabled(true);
		}
		catch (Exception e)
		{
  		e.printStackTrace();
  		JOptionPane.showMessageDialog(this.centerPanel,
  				e.getMessage(),
  		    "Rotdif Error",
  		    JOptionPane.ERROR_MESSAGE);

  		this.runButton.setEnabled(false);
			this.activeBondButton.setEnabled(false);
			setElmdock(false);
		}
	}
	
	/**
	 * Dock button action.
	 */
	private void dockButtonAction()
	{
		this.elmdockButton.setEnabled(false);
		
		this.elmdockProgressBar.setMaximum(100);
		this.elmdockProgressBar.setIndeterminate(true);
		
		ElmDockTask task = new ElmDockTask(this);
    task.addPropertyChangeListener(new PropertyChangeListener()
		{
			
			@Override
			public void propertyChange(PropertyChangeEvent evt)
			{
				if ("progress".equals(evt.getPropertyName())) 
				{
		      int progress = (Integer) evt.getNewValue();
		      RotdifGUI.this.elmdockProgressBar.setValue(progress);
				}
			}
		});
   	task.execute();		
	}
	
	/**
	 * Exp data changed action.
	 * @throws PdbException 
	 */
	private void expDataChangedAction()
	{	
		closeGraphWindows();
		setGraphButtons(false);
		this.runProgressBar.setValue(0);
		
		this.detailedResultsButton.setEnabled(false);			
		
		try
		{
			if (this.pdbFileStatusLabel.getText().matches(".*success.*"))
			{
				if (this.selectedModelButton.isSelected())
		  	{
		  		SpinnerNumberModel model = (SpinnerNumberModel)this.modelTextField.getModel();	  		
					this.pdbModel = this.pdbStruct.getModel(model.getNumber().intValue()-1);
					this.pdbMolecule = this.pdbModel.createBasicMolecule();
		  	}
		  	else
		  	{
		  		this.pdbModel = this.pdbStruct.getAverageModel();
					this.pdbMolecule = this.pdbModel.createBasicMolecule();
		  	}
				
				this.temperatureTextField.setEnabled(true);
				this.elmPredictionButton.setEnabled(true);
	
				combineExperimentalData();
			}
			else
			{			
				this.temperatureTextField.setEnabled(false);
				this.elmPredictionButton.setEnabled(false);
			}
		}
		catch (PdbException e)
		{
			throw new RelaxationRuntimeException(e.getMessage(), e);
		}
	}
	
	/**
	 * Predict tensor.
	 * 
	 * @return the rotational diffusion tensor
	 */
	private RotationalDiffusionTensor predictTensor()
	{
		BasicMolecule mol = this.pdbMolecule;
		
		ElmPredictor predictor = new ElmPredictor(Double.valueOf(this.temperatureTextField.getText()));
		return predictor.predictTensor(mol);
	}
	
	/**
	 * Read data file.
	 */
	private void readDataFile()
	{
		try
		{
			this.experimentalData = ExperimentalRelaxationData.readDataFile(this.relaxFile);

			this.relaxationFileStatusLabel.setText("status: success");
			this.relaxationFileStatusLabel.setForeground(Color.GREEN);
			this.expRhoButton.setEnabled(true);
		} 
		catch (IOException ioException)
		{
			//JOptionPane.showMessageDialog(mainPanel,"Could not open file.","File error", JOptionPane.ERROR_MESSAGE);
			this.relaxationFileStatusLabel.setText("status: failed....Could not open file "+this.relaxFileChooser.getSelectedFile().getName());
			this.relaxationFileStatusLabel.setForeground(Color.RED);
			ioException.printStackTrace(System.err);
			this.expRhoButton.setEnabled(false);
		} 
		catch (Exception exception)
		{
			//JOptionPane.showMessageDialog(mainPanel,relaxException.getMessage(),"File parsing error", JOptionPane.ERROR_MESSAGE);
			String message = exception.getMessage();
			this.relaxationFileStatusLabel.setText("status: failed..."+message.substring(0, Math.min(message.length()-1, 80))+"...");
			this.relaxationFileStatusLabel.setForeground(Color.RED);
			exception.printStackTrace(System.err);
			this.expRhoButton.setEnabled(false);
		}
		finally
		{
			expDataChangedAction();
		}
	}
		
	/**
	 * Run button action.
	 */
	private void runButtonAction()
	{
		this.runButton.setEnabled(false);
		
		closeGraphWindows();		
		setGraphButtons(false);
		
		this.detailedResultsButton.setEnabled(false);
		this.runProgressBar.setMaximum(100);
		this.runProgressBar.setIndeterminate(true);
		
		RunTask task = new RunTask(this);
    task.addPropertyChangeListener(new PropertyChangeListener()
		{
			
			@Override
			public void propertyChange(PropertyChangeEvent evt)
			{
				if ("progress".equals(evt.getPropertyName())) 
				{
		      int progress = (Integer) evt.getNewValue();
		      RotdifGUI.this.runProgressBar.setValue(progress);
				}
			}
		});
    
   	task.execute();
	}
	
	/**
	 * Sets the elmdock.
	 * 
	 * @param value
	 *          the new elmdock
	 */
	private void setElmdock(boolean value)
	{
		this.elmdockButton.setEnabled(value);
		this.elmdockProgressBar.setEnabled(value);
		this.elmdockSaveButton.setEnabled(false);
	}
	
	/**
	 * Sets the graph buttons.
	 * 
	 * @param value
	 *          the new graph buttons
	 */
	private void setGraphButtons(boolean value)
	{
		if (value==false)
		{
			this.chi2Button.setEnabled(value);
			this.twoDimensionFitButton.setEnabled(value);
			this.isoFitButton.setEnabled(value);	
			this.axialFitButton.setEnabled(value);	
			this.threeDimensionFitButton.setEnabled(value);	
			this.aniFitButton.setEnabled(value);
			this.dynamicsButton.setEnabled(value);
			this.saveTensorAxesButton.setEnabled(value);
		}
		else
		{
			this.isoFitButton.setEnabled(value);	
			this.dynamicsButton.setEnabled(value);
			if (this.results.getAxiTensor()!=null)
			{
				this.chi2Button.setEnabled(value);
				this.axialFitButton.setEnabled(value);	
			}
			if (this.results.getAniTensor()!=null)
			{
				this.twoDimensionFitButton.setEnabled(value);
				this.threeDimensionFitButton.setEnabled(value);	
				this.aniFitButton.setEnabled(value);
				this.saveTensorAxesButton.setEnabled(value);				
			}
		}
	}
}

