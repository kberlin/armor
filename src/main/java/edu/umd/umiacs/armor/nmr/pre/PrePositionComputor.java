/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.util.ArrayList;
import java.util.Iterator;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OutlierDampingFunction;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.ExperimentComputor;

public final class PrePositionComputor implements ExperimentComputor<PreSolution>
{
	private final ExperimentalPreData data;
	private final double outlierThreshold;
	private final boolean rescale;
	private final boolean robust;
	public static final double ABSOLUTE_POSITION_ERROR = 1.0e-5;
	public static final double OUTLIER_THRESHOLD = 3.0;
	
	public PrePositionComputor(ExperimentalPreData data, boolean robust)
	{
		this(data, robust, false);
	}
	
	public PrePositionComputor(ExperimentalPreData data, boolean robust, boolean rescale)
	{
		this(data, robust,rescale, OUTLIER_THRESHOLD);
	}
	
	public PrePositionComputor(ExperimentalPreData data,	boolean robust, boolean rescale, double outlerThreshold)
	{
		if (data==null)
			throw new NullPointerException("Pre data cannot be null.");
		if (data.size()==4)
			throw new PreRuntimeException("Pre Computor needs at least 4 PRE datapoints to compute position.");

		this.data = data;
		this.robust = robust;
		this.rescale = rescale;
		this.outlierThreshold = outlerThreshold;
	}
	
	private Point3d computePosition(final ArrayList<Point3d> atomPostions, boolean robust, final boolean rescale, double outlierThreshold)
	{
		//generate the target and the associated weights
	  final double[] targetValues = this.data.values();
	  final double[] errors = this.data.errors();
	  
	  //create the optimizers
	  MultivariateVectorFunction prePredictionFunction = new MultivariateVectorFunction()
		{
			@Override
			public final double[] value(double[] pos) throws IllegalArgumentException
			{
				double[] predictedValues;
				if (rescale)
					predictedValues = predict(atomPostions, new Point3d(pos[0],pos[1],pos[2]), pos[3]);
				else
					predictedValues = predict(atomPostions, new Point3d(pos[0],pos[1],pos[2]));
							  
				return predictedValues;
			}
		};
	  
	  //create initial guess
		Point3d initPostion = computePositionQuick(atomPostions);		
	  double[] x0 = initPostion.toArray();
	  if (rescale)
	  	x0 = new double[]{x0[0], x0[1], x0[2], this.data.get(0).getTauC()};

	  LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(2000);
		optimizer.setAbsPointDifference(ABSOLUTE_POSITION_ERROR);
		optimizer.setObservations(targetValues);
		optimizer.setErrors(errors);
		
	  if (robust)
	  	prePredictionFunction = new OutlierDampingFunction(prePredictionFunction, targetValues, errors, outlierThreshold);
	  
	  optimizer.setFiniteDifferenceFunction(prePredictionFunction);
	  
	  //compute the result
	  OptimizationResultImpl result = optimizer.optimize(x0);
	  double[] pos = result.getPoint();
	  
		return new Point3d(pos[0], pos[1], pos[2]);
	}
	
	private Point3d computePositionQuick(ArrayList<Point3d> atomPositions)
	{
		ArrayList<PreDatum> goodPres = new ArrayList<PreDatum>(5);
		for (PreDatum preDatum : this.data)
		{
			if (preDatum.hasActiveDistance())
			{
				goodPres.add(preDatum);
			}
		}
		
		//if did not find good data, use zero position
		if (goodPres.isEmpty())
		{
			return Point3d.getOrigin();
		}
			
		int maxIndex = Math.min(goodPres.size(), 5);
		double[][] sol = new double[3][maxIndex];
		for (int iter=0; iter<sol[0].length; iter++)
		{
			Point3d p = computePositionQuickHelper(atomPositions, goodPres, iter);
			
			sol[0][iter] = p.x;
			sol[1][iter] = p.y;
			sol[2][iter] = p.z;
		}
			
		//create the median point
		Point3d avePoint = new Point3d(BasicStat.median(sol[0]), BasicStat.median(sol[1]), BasicStat.median(sol[2]));
		
		if (avePoint.hasNaN())
			throw new PreRuntimeException("Could not compute initial position.");
					
		return avePoint;
	}
	
	private Point3d computePositionQuickHelper(ArrayList<Point3d> atomPositions, ArrayList<PreDatum> validDistances, int startingPoint)
	{
		//multilateration
		
		//select the centered around point
		Point3d firstPoint = atomPositions.get(startingPoint);
		double firstPointDistSquared = BasicMath.square(validDistances.get(startingPoint).getDistance());
		
		//form b and A matricies
		int counter = 0;
		double[] b = new double[validDistances.size()-1];
		double[][] A = new double[validDistances.size()-1][3];
		for (int iter=0; iter<validDistances.size(); iter++)
		{
			if (iter==startingPoint)
				continue;
				
			PreDatum datum = validDistances.get(iter);			
			Point3d currPoint = atomPositions.get(iter).subtract(firstPoint);
			
			b[counter] = BasicMath.square(datum.getDistance())
					-firstPointDistSquared
					-BasicMath.square(currPoint.length());
			
			A[counter][0] = -2.0*currPoint.x;
			A[counter][1] = -2.0*currPoint.y;
			A[counter][2] = -2.0*currPoint.z;

			counter++;
		}
		
		//solve for initial position
		double[] x = BasicMath.solveSVD(A, b, 1.0e-8);
		
		return new Point3d(x).add(firstPoint); 
	}
	
	public PrePositionComputor createRigid()
	{
		return new PrePositionComputor(this.data.createRigid(), this.robust, this.rescale, this.outlierThreshold);
	}

	public ArrayList<Point3d> extractPositions(Molecule mol) throws AtomNotFoundException
	{
		ArrayList<Point3d> positionList = new ArrayList<Point3d>(this.data.size());
		
		for (PreDatum datum : this.data)
			positionList.add(mol.getAtomPosition(datum.getAtom()));
		
		return positionList;
	}

	@Override
	public ExperimentalPreData getData()
	{
		return this.data;
	}
	
	private double[] predict(ArrayList<Point3d> atomPositions, Point3d prePosition)
	{
		double[] prediction = new double[this.data.size()];
		
		int counter=0;
		Iterator<Point3d> posIter = atomPositions.iterator();
		for (PreDatum datum : this.data)
		{
			Point3d p = posIter.next();
			prediction[counter] = datum.predict(prePosition, p);
			counter++;
		}
		
		return prediction;		
	}
		
	protected double[] predict(ArrayList<Point3d> atomPositions, Point3d prePosition, double tauC)
	{
		double[] prediction = new double[this.data.size()];
		
		int counter=0;
		Iterator<Point3d> posIter = atomPositions.iterator();
		for (PreDatum datum : this.data)
		{
			Point3d p = posIter.next();
			prediction[counter] = datum.predict(prePosition, p, tauC);
			counter++;
		}
		
		return prediction;		
	}
	
	public double[] predict(Molecule mol, Point3d prePosition) throws AtomNotFoundException
	{
		return predict(extractPositions(mol), prePosition);
	}
	
	private double[] predictDeltaR2(ArrayList<Point3d> atomPositions, Point3d prePosition, boolean cutoff)
	{
		double[] prediction = new double[this.data.size()];
		
		int counter=0;
		Iterator<Point3d> posIter = atomPositions.iterator();
		for (PreDatum datum : this.data)
		{
			Point3d p = posIter.next();
			prediction[counter] = datum.predictDeltaR2(prePosition, p);
			
			if (cutoff)
				prediction[counter] = Math.min(prediction[counter], datum.getMaxR2Delta());
			
			counter++;
		}
		
		return prediction;		
	}
	
	
	public double[] predictDeltaR2(Molecule mol, Point3d prePosition) throws AtomNotFoundException
	{
		return predictDeltaR2(extractPositions(mol), prePosition, false);
	}
	
	public double[] predictDeltaR2Cutoff(Molecule mol, Point3d prePosition) throws AtomNotFoundException
	{
		return predictDeltaR2(extractPositions(mol), prePosition, true);
	}

	
	public int size()
	{
		return this.data.size();
	}
	
	public PreSolution solve(ArrayList<Point3d> atomPositions)
	{
		if (!this.data.isRigid())
		{
			ArrayList<Point3d> rigidPositions = new ArrayList<Point3d>(atomPositions.size());
			for (int iter=0; iter<this.data.size(); iter++)
				if (this.data.get(iter).isRigid())
					rigidPositions.add(atomPositions.get(iter));
			
			return createRigid().solve(rigidPositions);
		}
		
		Point3d prePosition = computePosition(atomPositions, this.robust, this.rescale, this.outlierThreshold);

		return new PreSolution(prePosition, atomPositions, this.data);
	}
	
	@Override
	public PreSolution solve(Molecule mol) throws AtomNotFoundException
	{
		if (!this.data.isRigid())
			return createRigid().solve(mol);
		
		ArrayList<Point3d> atomPositions = extractPositions(mol);
		
		Point3d prePosition = solvePosition(mol);

		return new PreSolution(prePosition, atomPositions, this.data);
	}

	public Point3d solvePosition(Molecule mol) throws AtomNotFoundException
	{
		if (!this.data.isRigid())
			return createRigid().solvePosition(mol);
		
		return computePosition(extractPositions(mol), this.robust, this.rescale, this.outlierThreshold);
	}
}
