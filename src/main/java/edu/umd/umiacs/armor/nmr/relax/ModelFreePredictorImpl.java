/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.physics.BasicPhysics;

/**
 * The Class ModelFreePredictorImpl.
 * 
 * @param <M,J>
 *          the generic type
 */
class ModelFreePredictorImpl<J extends MFSpectralDensityFunction<J>> implements ModelFreePredictor
{
	/** The adj csa. */
	protected final double adjCSA;

	protected final Bond bond;
	
	protected final ModelFreeBondConstraints constraint;
	
	protected final double csaAngle;
	
	/** The d. */
	protected final double d;
	
	/** The gamma p. */
	protected final double gammaP;
	
	/** The gamma q. */
	protected final double gammaQ;
	
	/** The J. */
	protected final J j;
	
	protected final int model;
	
	/** The radius. */
	protected final double radius;
	
	/** The Rex. */
	protected final double Rex;
	
	public final static double REX_BASE_FREQUENCY  = 600.13;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4233610017679729112L;
	
	/**
	 * Rex absolute.
	 * 
	 * @param rex
	 *          the rex
	 * @param frequency
	 *          the frequency
	 * @return the double
	 */
	public static double rexAbsolute(double rex, double frequency)
	{
		return rex*((frequency*frequency)/(REX_BASE_FREQUENCY*REX_BASE_FREQUENCY));		
	}
	
	/**
	 * Rex relative.
	 * 
	 * @param rexAbsolute
	 *          the rex absolute
	 * @param frequency
	 *          the frequency
	 * @return the double
	 */
	public static double rexRelative(double rexAbsolute, double frequency)
	{
		return rexAbsolute*((REX_BASE_FREQUENCY*REX_BASE_FREQUENCY)/(frequency*frequency));		
	}
	
	/**
	 * Instantiates a new model free predictor impl.
	 * 
	 * @param J
	 *          the j
	 * @param bond
	 *          the bond
	 * @param CSA
	 *          the cSA
	 * @param radius
	 *          the radius
	 * @param Rex
	 *          the rex
	 */
	protected ModelFreePredictorImpl(J j, Bond bond, double CSA, double radius, double csaAngle, double Rex, int model)
	{
		this.j = j.createWithVector(bond.getVector());
		this.Rex = Rex;
		this.radius = radius;
		this.constraint = ModelFreeBondConstraints.getBondConstants(bond.getBondType());
		
		//convert to ?
		this.adjCSA = CSA*1.0e-6;
		
		this.csaAngle = csaAngle;
		
		//convert everything to SI units
	  double r = radius*1.0e-10; //in m
	  
	  this.bond = bond;
	  this.model = model;
	  
	  //define constants
	  double mu0 = BasicPhysics.mu0; //Tesla*m/A
	  double h = BasicPhysics.h;

	  //compute the larmor frequencies	  
	  this.gammaP = bond.getBondType().getFromAtomType().getGyromagneticRatio();
	  this.gammaQ = bond.getBondType().getToAtomType().getGyromagneticRatio();
	  
	  //contribution constants for dipolar (d)
	  this.d = -mu0*this.gammaP*this.gammaQ*h/(16.0*BasicMath.PI*BasicMath.PI*r*r*r);
	}
	
	protected ModelFreePredictorImpl(ModelFreePredictorImpl<J> predictor)
	{
		this.adjCSA = predictor.adjCSA;
		this.d = predictor.d;
		this.gammaP = predictor.gammaP;
		this.gammaQ = predictor.gammaQ;
		this.Rex = predictor.Rex;
		this.radius = predictor.radius;
		this.bond = predictor.bond;
		this.constraint = predictor.constraint;
		this.j = predictor.j;
		this.csaAngle = predictor.csaAngle;
		this.model = predictor.model;
	}
	
	protected ModelFreePredictorImpl(ModelFreePredictorImpl<J> predictor, double CSA)
	{
		//convert to ?
		this.adjCSA = CSA*1.0e-6;
		
		this.d = predictor.d;
		this.gammaP = predictor.gammaP;
		this.gammaQ = predictor.gammaQ;
		this.Rex = predictor.Rex;
		this.radius = predictor.radius;
		this.bond = predictor.bond;
		this.constraint = predictor.constraint;
		this.j = predictor.j;
		this.csaAngle = predictor.csaAngle;
		this.model = predictor.model;
	}
	
	private ModelFreePredictorImpl(ModelFreePredictorImpl<J> predictor, double rex, double csaAngle)
	{
		this.j = predictor.j;
		this.bond = predictor.bond;
		this.radius = predictor.radius;
		this.adjCSA = predictor.adjCSA;
		this.d = predictor.d;
		this.gammaP = predictor.gammaP;
		this.gammaQ = predictor.gammaQ;
		this.constraint = predictor.constraint;
		this.csaAngle = csaAngle;
		this.Rex = rex;
		this.model = 0;
	}
	
	protected ModelFreePredictorImpl(ModelFreePredictorImpl<J> predictor, J j, int model)
	{
		this.radius = predictor.radius;
		this.adjCSA = predictor.adjCSA;
		this.d = predictor.d;
		this.gammaP = predictor.gammaP;
		this.gammaQ = predictor.gammaQ;
		this.Rex = predictor.Rex;		
		this.j = j;
		this.bond = predictor.bond;
		this.csaAngle = predictor.csaAngle;
		this.constraint = predictor.constraint;
		this.model = model;
	}
	
	protected ModelFreePredictorImpl(ModelFreePredictorImpl<J> predictor, Point3d bondVector)
	{
		this.bond = predictor.bond.createNewVector(bondVector);
		this.j = predictor.j.createWithVector(bondVector);
		this.adjCSA = predictor.adjCSA;
		this.d = predictor.d;
		this.gammaP = predictor.gammaP;
		this.gammaQ = predictor.gammaQ;
		this.Rex = predictor.Rex;
		this.radius = predictor.radius;
		this.constraint = predictor.constraint;
		this.csaAngle = predictor.csaAngle;
		this.model = predictor.model;
	}
	
	@Override
	public final ModelFreePredictorImpl<J> createFromMolecule(Molecule mol) throws AtomNotFoundException
	{
		Point3d p1 = mol.getAtomPosition(this.bond.getFromAtom());
		Point3d p2 = mol.getAtomPosition(this.bond.getToAtom());
	
		return createWithVector(p2.subtract(p1));
	}
			
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#createWithNewCSA(double)
	 */
	@Override
	public final ModelFreePredictorImpl<J> createWithCSA(double CSA)
	{
		return new ModelFreePredictorImpl<J>(this, CSA);
	}
		
	@Override
	public ModelFreePredictorImpl<J> createWithCsaAngle(double csaAngle)
	{
		return new ModelFreePredictorImpl<J>(this, this.Rex, csaAngle);
	}
	
	@Override
	public final ModelFreePredictorImpl<J> createWithLocal(double S2, double tauLocal, double S2Fast, double tauLocalFast)
	{
		return new ModelFreePredictorImpl<J>(this, this.j.createWithLocal(S2, tauLocal, S2Fast, tauLocalFast), 0);
	}

	
	@Override
	public ModelFreePredictorImpl<J> createWithModel(int model)
	{
		return new ModelFreePredictorImpl<J>(this, this.j, model);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#createWithParameters(double, double, double)
	 */
	@Override
	public final ModelFreePredictorImpl<J> createWithParameters(double CSA, double r, double csaAngle, double Rex)
	{
		return new ModelFreePredictorImpl<J>(this.j, this.bond, CSA, r, csaAngle, Rex, 0);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor#createWithParametersFrom(edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor)
	 */
	@Override
	public final <T extends ModelFreePredictor> ModelFreePredictorImpl<J> createWithParametersFrom(T predictor)
	{
		return new ModelFreePredictorImpl<J>(
				this.j.createWithLocal(predictor.getS2Slow(), predictor.getTauLocal(), predictor.getS2Fast(), predictor.getTauFast()).createWithTensor(predictor.getTensor()),
				this.bond, predictor.getCSA(), predictor.getRadius(), predictor.getCsaAngle(), predictor.getRex(), predictor.getModel());
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#createWithNewRadius(double)
	 */
	@Override
	public final ModelFreePredictorImpl<J> createWithRadius(double r)
	{
		return new ModelFreePredictorImpl<J>(this.j, this.bond, getCSA(), r, this.csaAngle, getRex(), getModel());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor#createWithNewRex(double)
	 */
	@Override
	public final ModelFreePredictorImpl<J> createWithRex(double rex)
	{
		ModelFreePredictorImpl<J> predictor = new ModelFreePredictorImpl<J>(this, rex, this.csaAngle);
		
		return predictor;

	}

	@Override
	public final ModelFreePredictorImpl<J> createWithS2Fast(double S2Fast)
	{
		return new ModelFreePredictorImpl<J>(this, this.j.createWithS2Fast(S2Fast), 0);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor#createWithNewS2(double)
	 */
	@Override
	public final ModelFreePredictorImpl<J> createWithS2Slow(double S2Slow)
	{
		return new ModelFreePredictorImpl<J>(this, this.j.createWithS2Slow(S2Slow), 0);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor#createWithNewTauLocal(double)
	 */
	@Override
	public final ModelFreePredictorImpl<J> createWithTauLocal(double tauLocal)
	{
		return new ModelFreePredictorImpl<J>(this, this.j.createWithTauLocal(tauLocal), 0);
	}

	@Override
	public final ModelFreePredictorImpl<J> createWithTauLocalFast(double tauLocalFast)
	{
		return new ModelFreePredictorImpl<J>(this, this.j.createWithTauFast(tauLocalFast), 0);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#createWithNewRotationalDiffusionTensor(edu.umd.umiacs.armor.nmr.relax.RotationalDiffusionTensor)
	 */
	@Override
	public final ModelFreePredictorImpl<J> createWithTensor(RotationalDiffusionTensor tensor)
	{
		return new ModelFreePredictorImpl<J>(this, this.j.createWithTensor(tensor), 0);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#createWithNewVector(edu.umd.umiacs.armor.math.Point3d)
	 */
	@Override
	public final ModelFreePredictorImpl<J> createWithVector(Point3d bondVector)
	{
		return new ModelFreePredictorImpl<J>(this, bondVector);
	}

	public final RelaxationDatum derivativeRex(double frequency)
	{
		double[] derivative = new double[5];

		derivative[0] = 0.0;
		derivative[1] = rexAbsolute(1.0, frequency);
		derivative[2] = 0.0;
		derivative[3] = 0.0;
		derivative[4] = 0.0;
		
		return new RelaxationDatum(this.bond, this.constraint, derivative[0], derivative[1], derivative[2], derivative[3], derivative[4], frequency, 0.0);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor#getBond()
	 */
	@Override
	public final Bond getBond()
	{
		return this.bond;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#getCSA()
	 */
	@Override
	public final double getCSA()
	{
		return this.adjCSA*1.0e6;
	}

	@Override
	public double getCsaAngle()
	{
		return this.csaAngle;
	}

	/**
	 * Gets the dipolar constant.
	 * 
	 * @return the dipolar constant
	 */
	@Override
	public final double getDipolarConstant()
	{
		return this.d;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#getGammaFromAtom()
	 */
	@Override
	public final double getGammaFromAtom()
	{
		return this.gammaP;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#getGammaToAtom()
	 */
	@Override
	public final double getGammaToAtom()
	{
		return this.gammaQ;
	}

	@Override
	public int getModel()
	{
		return this.model;
	}

	@Override
	public ModelFreeBondConstraints getModelFreeBondConstraint()
	{
		return this.constraint;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#getRadius()
	 */
	@Override
	public final double getRadius()
	{
		return this.radius;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#getRex()
	 */
	@Override
	public final double getRex()
	{
		return this.Rex;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor#getS2()
	 */
	@Override
	public double getS2()
	{
		return this.j.getS2();
	}

	@Override
	public double getS2Fast()
	{
		return this.j.getS2Fast();
	}

	@Override
	public double getS2Slow()
	{
		return this.j.getS2Slow();
	}

	/**
	 * Gets the spectral density function.
	 * 
	 * @return the spectral density function
	 */
	@Override
	public J getSpectralDensityFunction()
	{
		return this.j;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#getTauC()
	 */
	@Override
	public double getTauC()
	{
		return this.j.getTauC();
	}

	@Override
	public double getTauFast()
	{
		return this.j.getTauFast();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor#getTauLocal()
	 */
	@Override
	public double getTauLocal()
	{
		return this.j.getTauLocal();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#getRotationalDiffusionTensor()
	 */
	@Override
	public RotationalDiffusionTensor getTensor()
	{
		return this.j.getTensor();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#predict(double)
	 */
	@Override
	public final RelaxationDatum predict(double frequency)
	{
		double omegaP = BasicPhysics.larmorFrequency(frequency, this.gammaP);
		double omegaQ = BasicPhysics.larmorFrequency(frequency, this.gammaQ);
	  
	  //contribution constants for CSA interactions (c)
	  double c = -omegaP*this.adjCSA/3.0;
		
		double c2 = c*c;
		double d2 = this.d*this.d;
		
		double J0 = this.j.value(0.0);
		double Jp = this.j.value(omegaP);
		double Jq = this.j.value(omegaQ);
		double Jq_plus_p = this.j.value(omegaP+omegaQ);
		double Jq_minus_p = this.j.value(omegaQ-omegaP);
		
	  //T1 relaxation rate
	  double r1 = 3.0*(d2+c2)*Jp+d2*(Jq_minus_p+6.0*Jq_plus_p);
	  
	  //T2 relaxation rate
	  double r2 = 0.5*(d2+c2)*(4.0*J0+3.0*Jp)+0.5*d2*(Jq_minus_p+6*Jq+6.0*Jq_plus_p)+rexAbsolute(frequency);
	  	  
	  //NOE
	  double noe = 1.0+((this.gammaQ/this.gammaP)*d2*(6.0*Jq_plus_p-Jq_minus_p))/r1;
	  
	  //eta_xy
	  double eta_xy = this.d*c*(4.0*J0+3.0*Jp)*(3.0*BasicMath.cos(this.csaAngle)-1.0)/2.0;
	  
	  //eta_z
	  double eta_z = this.d*c*(3.0*Jp)*(3.0*BasicMath.cos(this.csaAngle)-1.0)/2.0;
	  	  
	  return new RelaxationDatum(this.bond, this.constraint, r1, r2, noe, eta_xy, eta_z, frequency, (4.0/3.0)*(J0/Jp));
	}
	
	@Override
	public final double[] predict(double frequency, RelaxationDatum datum)
	{
		RelaxationDatum prediction = predict(frequency);
		
		double[] values = new double[datum.size()];
		
		int count = 0;
		for (int iter=0; iter<datum.numValues(); iter++)
		{
			if (datum.hasValue(iter))
			{
				values[count] = prediction.getValue(iter).value();				
				count++;
			}
		}
		
		return values;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#predictEtaXy(double)
	 */
	@Override
	public double predictEtaXy(double frequency)
	{
		RelaxationDatum relaxationDatum = predict(frequency);
		return relaxationDatum.getEtaXy().value();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#predictEtaZ(double)
	 */
	@Override
	public double predictEtaZ(double frequency)
	{
		RelaxationDatum relaxationDatum = predict(frequency);
		return relaxationDatum.getEtaZ().value();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#predictNOE(double)
	 */
	@Override
	public final double predictNOE(double frequency)
	{
		RelaxationDatum relaxationDatum = predict(frequency);
		return relaxationDatum.getNoe().value();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#predictR1(double)
	 */
	@Override
	public final double predictR1(double frequency)
	{
		RelaxationDatum relaxationDatum = predict(frequency);
		return relaxationDatum.getR1().value();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#predictR2(double)
	 */
	@Override
	public final double predictR2(double frequency)
	{
		RelaxationDatum relaxationDatum = predict(frequency);
		return relaxationDatum.getR2().value();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#predictRho(double)
	 */
	@Override
	public final double predictRho(double frequency)
	{
		double omegaP = BasicPhysics.larmorFrequency(frequency, this.gammaP);

		//double J0 = J.value(0.0);
		//double Jp = J.value(omegaP);
		double J0 = this.j.valueOrdered(0.0);
		double Jp = this.j.valueOrdered(omegaP);
		
		//return 0.75*(Jp/J0);
		return (4.0/3.0)*(J0/Jp);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor#predictRhoEta(double)
	 */
	@Override
	public double predictRhoEta(double frequency)
	{
		double omegaP = BasicPhysics.larmorFrequency(frequency, this.gammaP);

		//double J0 = J.value(0.0);
		//double Jp = J.value(omegaP);
		double J0 = this.j.valueOrdered(0.0);
		double Jp = this.j.valueOrdered(omegaP);
		
		//return 0.75*(Jp/J0);
		return (4.0*J0+3.0*Jp)/(3.0*Jp);
	}

	/**
	 * Rex absolute.
	 * 
	 * @param frequency
	 *          the frequency
	 * @return the double
	 */
	public final double rexAbsolute(double frequency)
	{
		return rexAbsolute(this.Rex, frequency);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "[CSA=" + getCSA() + 
				", Rex=" + getRex() + 
				", S2=" + getS2() + 
				", tauLoc=" + getTauLocal() + 
				", S2Fast=" + getS2Fast() + 
				", tauLocFast=" + getTauFast()+"]";
	}
	
	
}
