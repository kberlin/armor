/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.regex.Pattern;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.AtomImpl;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PrimaryStructure;
import edu.umd.umiacs.armor.molecule.SecondaryStructure;
import edu.umd.umiacs.armor.nmr.pre.PreDatum.ExperimentType;
import edu.umd.umiacs.armor.util.AbstractExperimentalData;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.NonNegativeExperimentalDatum;

public final class ExperimentalPreData extends AbstractExperimentalData<PreDatum>
{
	private final boolean rigid;

	/**
	 * 
	 */
	private static final long serialVersionUID = -823211201115826524L;
	
	public static HashMap<Integer,ExperimentalPreData> readDataFile(File file) throws IOException, PreFileException
	{
		HashMap<Integer,ExperimentalPreData> datasets = new HashMap<Integer, ExperimentalPreData>();
		
		Scanner scanner = BasicUtils.getFileScanner(file);
		
		try
		{
			Pattern atomName = Pattern.compile("[\\w']+");
			Pattern typePattern = Pattern.compile("[\\w']+");
			
			int counter = 0;
			while (scanner.hasNextLine())
			{
				//mark the line number
				counter++;
				
				//get the next line
				String nextLine = scanner.nextLine();
				Scanner lineScanner = new Scanner(nextLine);
				
				//get rid of potential trash due to invalid conversion
				try
				{
					boolean isInactive = false;

					//is empty
					if (!lineScanner.hasNext())
						continue;
					
					String currLine = lineScanner.nextLine().trim();
					
					//is a comment line
					if (currLine.startsWith("%"))
						continue;

					//is flexible
					if (currLine.startsWith("*"))
					{
						isInactive = true;
						lineScanner.close();
						lineScanner = new Scanner(currLine.substring(1));
					}
					else
					{
						lineScanner.close();
						lineScanner = new Scanner(currLine);
					}

					lineScanner.useLocale(Locale.US);
					
					int indexNumber = (int)lineScanner.nextDouble();
					int residueNumber = (int)lineScanner.nextDouble();
					String chainID = lineScanner.next(atomName);
					String nameP = lineScanner.next(atomName);
					String type = lineScanner.next(typePattern);
				  double freq = lineScanner.nextDouble();
		      double tauC = lineScanner.nextDouble();
		      double time = lineScanner.nextDouble();
		      double t2Dia = lineScanner.nextDouble();
		      double iOx = lineScanner.nextDouble();
		      double iOxError = lineScanner.nextDouble();
		      double iRed = lineScanner.nextDouble();
		      double iRedError = lineScanner.nextDouble();
		      					
		      PrimaryStructure primary = new PrimaryStructure(residueNumber, nameP, chainID, "UKW", 0.0);
		      SecondaryStructure secondary = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
		      
		      AtomImpl a1 = new AtomImpl(new Point3d(0,0,0), primary, secondary);
		      
		      if (iOxError<1.0e-12)
		      	iOxError = iOx*0.03;
		      if (iRedError<1.0e-12)
		      	iRedError = iRed*0.03;

		      PreDatum datum = new PreDatum(new NonNegativeExperimentalDatum(iOx,iOxError), new NonNegativeExperimentalDatum(iRed, iRedError), 
	      		tauC, freq, time, t2Dia, a1, PreType.valueOf(type), !isInactive);
		      
		      if (datasets.containsKey(indexNumber))
		      {
		      	datasets.get(indexNumber).add(datum);
		      }
		      else
		      {
		      	ExperimentalPreData expDataset = new ExperimentalPreData();
		      	expDataset.add(datum);
		      	datasets.put(indexNumber, expDataset);
		      }
				}
				catch (NoSuchElementException e)
				{
					throw new PreFileException("Could not parse relaxation input file, line "+counter+":\n  \""+nextLine+"\"", e);
				}
				catch (PreRuntimeException e)
				{
					throw new PreFileException("Could not parse relaxation input file, line "+counter+":\n  \""+nextLine+"\"", e);					
				}
				finally
				{
					lineScanner.close();
				}
			}
		}
		finally
		{
			scanner.close();
		}
		
		return datasets;
	}
	
	public static ExperimentalPreData readDataFile(File file, int index) throws IOException, PreFileException
	{
		return readDataFile(file).get(index);
	}

	public static HashMap<Integer,ExperimentalPreData> readDataFile(String file) throws IOException, PreFileException
	{
		return readDataFile(new File(file));
	}

	public static ExperimentalPreData readDataFile(String file, int index) throws IOException, PreFileException
	{
		return readDataFile(new File(file), index);
	}
	
	public static ExperimentalPreData readDataFileFirst(File file) throws IOException, PreFileException
	{
		return readDataFile(file).values().iterator().next();
	}

	public static ExperimentalPreData readDataFileFirst(String file) throws IOException, PreFileException
	{
		return readDataFileFirst(new File(file));
	}
	
	private ExperimentalPreData()
	{
		super();
		this.rigid = false;
	}
	
	public ExperimentalPreData(List<PreDatum> data)
	{
		super(data);
		
		boolean rigid = true;
		for (PreDatum datum : data)
		{
			if (!datum.isRigid())
				rigid = false;
		}
		
		this.rigid = rigid;
	}
	
	public ExperimentalPreData createFromChain(String chainID) throws PreDataException
	{
		ArrayList<PreDatum> newList = new ArrayList<PreDatum>(size());
		for (PreDatum datum : this)
		{
			if (datum.getAtom().getPrimaryInfo().getChainID().equals(chainID))
				newList.add(datum);				
		}		
		
		if (newList.isEmpty())
			throw new PreDataException("Cannot find data for chain "+chainID);
		
		return new ExperimentalPreData(newList);
	}

	public ExperimentalPreData createFromExistingAtoms(Molecule molecule)
	{
	   ArrayList<PreDatum> newList = new ArrayList<PreDatum>(size());
	    
			for (PreDatum datum : this)
			{				
				try
				{
					newList.add(datum.createFromMolecule(molecule));
				}
				catch (AtomNotFoundException e) {}
		}
			
		return new ExperimentalPreData(newList);
	}

	public ExperimentalPreData createRigid()
	{
		if (this.rigid)
			return this;
		
	  ArrayList<PreDatum> newList = new ArrayList<PreDatum>(size());
	    
		for (PreDatum datum : this)
		{
			if (datum.isRigid())
				newList.add(datum);
		}
		
		return new ExperimentalPreData(newList);
	}
	
	public ArrayList<Atom> getAtoms()
	{
		ArrayList<Atom> atoms = new ArrayList<Atom>(size());
		for (PreDatum datum : this)
			atoms.add(datum.getAtom());
			
		return atoms;
	}
	
	public ArrayList<String> getChains()
	{
		ArrayList<String> bonds = new ArrayList<String>();
		
		for (PreDatum datum : this)
		{
			int index = bonds.indexOf(datum.getAtom().getPrimaryInfo().getChainID());
			if (index<0)
				bonds.add(datum.getAtom().getPrimaryInfo().getChainID());
		}
		
		//sort the result
		Collections.sort(bonds);
		
		return bonds;
	}
	
	public BaseExperimentalData getDeltaR2Data()
	{
		ArrayList<BaseExperimentalDatum> expData = new ArrayList<BaseExperimentalDatum>(size());
		for (PreDatum datum : this)
			expData.add(datum.getDeltaR2());
		
		return new BaseExperimentalData(expData);
	}
	
	public ExperimentType getExperimentType()
	{
		return this.get(0).getExperimentType();
	}

	public ExperimentalPreData getValid()
	{
		ArrayList<PreDatum> data = new ArrayList<PreDatum>();
		
		for (PreDatum datum : this)
		{
			if (!datum.isNaNOrInfinite())
				data.add(datum);
		}
		
		return new ExperimentalPreData(data);
	}

	public boolean isRigid()
	{
		return this.rigid;
	}

	public ArrayList<ExperimentalPreData> seperateByChain()
	{
		ArrayList<String> chains = getChains();
		ArrayList<ExperimentalPreData> newList = new ArrayList<ExperimentalPreData>(chains.size());
		
		for (String chain : chains)
		{
			try
			{
				newList.add(createFromChain(chain));
			}
			catch (PreDataException e)
			{
				throw new PreRuntimeException("Unexpected error.", e);
			}
		}
				
		return newList;
	}
}
