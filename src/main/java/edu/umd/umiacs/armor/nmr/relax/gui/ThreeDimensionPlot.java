/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax.gui;

import java.awt.BorderLayout;
import java.awt.Paint;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JFrame;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.TwoVariableFunction;
import edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor;
import edu.umd.umiacs.armor.nmr.relax.RelaxationSolution;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.plot.Limit;
import edu.umd.umiacs.armor.util.plot.Surface;
import edu.umd.umiacs.armor.util.plot.SurfacePlot;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jzy3d.colors.Color;

/**
 * The Class ThreeDimensionPlot.
 */
public class ThreeDimensionPlot extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4632606767035814687L;

	/**
	 * Spherical to cartesian.
	 * 
	 * @param theta
	 *          the theta
	 * @param phi
	 *          the phi
	 * @return the point3d
	 */
	private static Point3d sphericalToCartesian(double theta, double phi, double radius)
	{
	  return new Point3d(radius*BasicMath.sin(theta)*BasicMath.cos(phi), 
	  		radius*BasicMath.sin(theta)*BasicMath.sin(phi), 
	  		radius*BasicMath.cos(theta));
	}
	
	private static Point3d cartesianToSpherical(Point3d p)
	{
		double r = p.length();
		double thetaExp = Math.acos(p.z/r);
		double phiExp = Math.atan2(p.y,p.x);
		
		return new Point3d(thetaExp, phiExp,r);
	}

	
	/**
	 * Instantiates a new three dimension plot.
	 * 
	 * @param solution
	 *          the solution
	 */
	public ThreeDimensionPlot(RelaxationSolution<? extends RelaxationRatePredictor> solution) 
	{
		getContentPane().setBackground(java.awt.Color.WHITE);
		
		setTitle("3D Fit Plot");
		setBounds(100, 100, 400, 400);
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		//don't resize until 3d plot graphs are fixed
		this.setResizable(false);
		
		JLabel lblChiPlot = new JLabel("3D Fit Plot");
		lblChiPlot.setBackground(java.awt.Color.WHITE);
		lblChiPlot.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(lblChiPlot, BorderLayout.NORTH);
		
		SurfacePlot plot = new SurfacePlot();
		
		final RelaxationSolution<? extends RelaxationRatePredictor> rigidSolution = solution.createRigid();

		//create tensor with coordinate from orienation
		final Rotation tensorRotation = solution.getTensor().getProperEigenDecomposition().getRotation();
		
		ArrayList<? extends RelaxationSolution<? extends RelaxationRatePredictor>> solutionList = rigidSolution.seperateByBondTypeAndFrequency();
		
		//store the paint methods for all the units
		ArrayList<Paint> paintList = new ArrayList<Paint>(solutionList.size());
		DefaultDrawingSupplier supplier = new DefaultDrawingSupplier();
		for (int index=0; index<solutionList.size(); index++)
			paintList.add(supplier.getNextPaint());	
		
		for (final RelaxationSolution<? extends RelaxationRatePredictor> currentSolution : solutionList)
		{
			//create the "ideal" predictor
			final RelaxationRatePredictor currPredictor = currentSolution.getPredictor(0)
					.createWithLocal(1.0, 0.0, 1.0, 0.0).createWithRex(0.0);
			final double vectorLength = currPredictor.getBond().getVector().length();
			
			//get the current freq
			final double currFreq = currentSolution.getRelaxationData().getBondRelaxation(0).getRelaxationDatum(0).getFrequency();
			
			// Define a function to plot
			TwoVariableFunction rhoFunc = new TwoVariableFunction()
			{
				@Override
				public double value(double a, double b)
				{
					//rotation the point relative to the diffusion tensor since we want to plot in its frame
					Point3d p = tensorRotation.mult(sphericalToCartesian(a*BasicMath.PI/180.0,b*BasicMath.PI/180.0, vectorLength));
					double rho = currPredictor.createWithVector(p).predictRho(currFreq);
					
					return rho;
				}
			};

			//create limits
			Limit rangeTheta = new Limit(0.0, 180.0);
			Limit rangePhi = new Limit(-180, 180);
			
			Surface surface = new Surface(rhoFunc, rangeTheta, 40, rangePhi, 40);
			surface.setWireframeDisplayed(false);
			surface.setGrayColor(new Color(1, 1, 1, .5f));
			//surface.setColor(new Color(1, 1, 1, .2f));
			
			plot.addSurface(surface);
			
			//add the solution
			ArrayList<Point3d> pointList = new ArrayList<Point3d>();
			Iterator<BaseExperimentalDatum> rhoIterator = currentSolution.getRelaxationData().getRhoData().iterator();
			for (RelaxationRatePredictor predictor : currentSolution.getPredictors())
			{
				//get the x-y coordinate
				Point3d rotatedBondVector = tensorRotation.multInv(predictor.getBond().getVector());
				Point3d sphericalCoord = cartesianToSpherical(rotatedBondVector);
				double thetaExp = sphericalCoord.x*180.0/BasicMath.PI;
				double phiExp = sphericalCoord.y*180.0/BasicMath.PI;
				
				//get the experimental value
				double rhoExp = rhoIterator.next().value();

				//get the predictor rho value
				double rhoPred = currPredictor.createWithVector(predictor.getBond().getVector()).predictRho(currFreq);

				//add the point
				pointList.add(new Point3d(thetaExp, phiExp, rhoExp));
				
				//put a line to the surface from the point
				Point3d p = new Point3d(thetaExp, phiExp, Math.min(rhoExp, rhoPred));
				plot.addCylinder(p, Math.abs(rhoExp-rhoPred), 0.3, Color.BLUE);
			}			
			
			plot.addScatter(pointList, 5.0, Color.BLUE);
		}
		
		plot.setXLabel("theta");
		plot.setYLabel("phi");
		plot.setZLabel("rho");
		//plot.setColorBar(false);
		
		plot.addLight(new Point3d(100,100,100));
		
		//add the plot
		getContentPane().add(plot, BorderLayout.CENTER);
		
		setVisible(true);
	}
}
