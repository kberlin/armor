/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.nmr.pre.PreDatum;
import edu.umd.umiacs.armor.nmr.pre.PreSolution;
import edu.umd.umiacs.armor.util.plot.Limit;
import edu.umd.umiacs.armor.util.plot.Line;
import edu.umd.umiacs.armor.util.plot.Plot2d;

import java.awt.GridLayout;

/**
 * The Class RdcFitPlot.
 */
public class PreFitPlot extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4863678518947357686L;

	/**
	 * Instantiates a new rdc fit plot.
	 * 
	 * @param computor
	 *          the computor
	 */
	public PreFitPlot(PreSolution solution, String preName)
	{
		getContentPane().setBackground(java.awt.Color.WHITE);

		setTitle("Fit Plot for PRE Label: "+preName);
		setBounds(100, 100, 500, 600);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		// create the double layout
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
	
		// prediction scatter plot
		double[] residuals = solution.residuals();
		//double[] experimentalValues = solution.values();
		//double[] predictedValues = solution.predict();
		double[] experimentalValues = solution.valuesDeltaR2();
		double[] predictedValues = solution.predictDeltaR2();

		int counter = 0;
		ArrayList<String> toolTips = new ArrayList<String>();
		for (PreDatum pre : solution.getExperimentalData())
		{
			toolTips.add(String.format(
					"<html>Residue: %d<br>Chain: %s<br>Distance: %.3f<br>Residual: %.3f</html>", 
					pre.getAtom().getPrimaryInfo().getResidueNumber(), 
					pre.getAtom().getPrimaryInfo().getChainID(),
					pre.getDistance(), 
					residuals[counter]));

				counter++;
		}

		Line scatterLine = new Line(experimentalValues, predictedValues, "Inliers", toolTips);
		scatterLine.setDrawLine(false);
		scatterLine.setDrawShape(true);
		scatterLine.setPaint(Color.BLUE);
		scatterLine.setShape(new Ellipse2D.Double(-2.5, -2.5, 5, 5));

		double minValue = Math.min(BasicMath.min(predictedValues), BasicMath.min(experimentalValues));
		double maxValue = Math.max(BasicMath.max(predictedValues), BasicMath.max(experimentalValues));
		double diff = maxValue-minValue;
		Limit limit = new Limit(minValue - diff * .1, maxValue + diff * .1);

		Line regressionLine = new Line(new double[] {limit.getMin(), limit.getMax()}, new double[] {limit.getMin(), limit.getMax()}, "y=x");
		regressionLine.setDrawLine(true);
		regressionLine.setDrawShape(false);
		regressionLine.setPaint(Color.BLACK);

		Plot2d plot = new Plot2d();
		plot.addLine(regressionLine);
		plot.addLine(scatterLine);
		plot.setXLabel("DeltaR2 Exp.");
		plot.setYLabel("DeltaR2 Comp.");
		plot.setTitle(
				String.format("%s (Q=%.3f, R=%.3f)", "Delta R2 Fit", 
						BasicStat.rotdifQualityFactor(experimentalValues, predictedValues), 
						BasicStat.pearsonsCorrelation(experimentalValues, predictedValues)),
				new Font("Ariel", Font.PLAIN, 14));

		// set the axes limits
		plot.setXAxisLimit(limit);
		plot.setYAxisLimit(limit);

		// add the plot
		getContentPane().add(plot);
		
		// generate a bar plot for each chain
		ArrayList<PreSolution> computorListOfChains = solution.seperateByChain();
		for (PreSolution chainSolution : computorListOfChains)
		{
			Plot2d barPlot = new Plot2d();
			barPlot.setTitle(String.format("Residuals for Chain %s", 
							chainSolution.getExperimentalData().get(0).getAtom().getPrimaryInfo().getChainID()),
							new Font("Ariel", Font.PLAIN, 12));
			barPlot.setIntegerTicks(true);
			barPlot.setXLabel("Residue/Nucleotide #");
			barPlot.setYLabel("DeltaR2 Residuals");
			getContentPane().add(barPlot);
			// barPlot.removeLegend();

			PreSolution typeSolution = chainSolution;

			double[] residues = new double[typeSolution.getExperimentalData().size()];
			toolTips = new ArrayList<String>();

			counter = 0;
			residuals = typeSolution.residualsDeltaR2();
			for (PreDatum datum : typeSolution.getExperimentalData())
			{
				residues[counter] = datum.getAtom().getPrimaryInfo().getResidueNumber();
				toolTips.add(String.format(
						"<html>Residue: %d<br>Chain: %s<br>Residual: %.3f</html>", 
						datum.getAtom().getPrimaryInfo().getResidueNumber(), 
						datum.getAtom().getPrimaryInfo().getChainID(),
						residuals[counter]));

				counter++;
			}

			// label of the current dataset
			String label = String.format("%s, %s", 
					typeSolution.getExperimentalData().get(0).getAtom().getPrimaryInfo().getChainID(),
					preName);
			
			Line residualLine = new Line(residues, residuals, label, toolTips);
			// residualLine.setPaint(new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F,
			// 0.0F, new Color(0, 0, 64)));

			// add to the bar list
			barPlot.addBar(residualLine);
		}
		
		// generate a bar fit plot for each chain
		for (PreSolution chainSolution : computorListOfChains)
		{
			Plot2d barPlot = new Plot2d();
			barPlot.setTitle(String.format("Fit for Chain %s", 
							chainSolution.getExperimentalData().get(0).getAtom().getPrimaryInfo().getChainID()),
							new Font("Ariel", Font.PLAIN, 12));
			barPlot.setIntegerTicks(true);
			barPlot.setXLabel("Residue/Nucleotide #");
			barPlot.setYLabel("Ratio");
			getContentPane().add(barPlot);
			// barPlot.removeLegend();

			PreSolution typeSolution = chainSolution;

			double[] residues = new double[typeSolution.getExperimentalData().size()];
			toolTips = new ArrayList<String>();

			counter = 0;
			double[] valuesExp = typeSolution.getExperimentalData().values();
			for (PreDatum datum : typeSolution.getExperimentalData())
			{
				residues[counter] = datum.getAtom().getPrimaryInfo().getResidueNumber();
				toolTips.add(String.format(
						"<html>Residue: %d<br>Chain: %s<br>Value: %.3f</html>", 
						datum.getAtom().getPrimaryInfo().getResidueNumber(), 
						datum.getAtom().getPrimaryInfo().getChainID(),
						valuesExp[counter]));

				counter++;
			}
			
			counter = 0;
			double[] valuesPred = typeSolution.predict();
			for (PreDatum datum : typeSolution.getExperimentalData())
			{
				residues[counter] = datum.getAtom().getPrimaryInfo().getResidueNumber();
				toolTips.add(String.format(
						"<html>Residue: %d<br>Chain: %s<br>Value: %.3f</html>", 
						datum.getAtom().getPrimaryInfo().getResidueNumber(), 
						datum.getAtom().getPrimaryInfo().getChainID(),
						valuesPred[counter]));

				counter++;
			}

			// label of the current dataset
			String labelExp = String.format("%s, %s", 
					typeSolution.getExperimentalData().get(0).getAtom().getPrimaryInfo().getChainID(),
					""+preName+" Exp.");			
			Line expLine = new Line(residues, valuesExp, labelExp, toolTips);

			String labelPred = String.format("%s, %s", 
					typeSolution.getExperimentalData().get(0).getAtom().getPrimaryInfo().getChainID(),
					""+preName+" Pred.");			
			Line predLine = new Line(residues, valuesPred, labelPred, toolTips);
	
			// add to the bar list
			barPlot.addBar(expLine);
			barPlot.addLine(predLine);
		}
		
		// generate a bar plot for each chain
		computorListOfChains = solution.seperateByChain();
		for (PreSolution chainSolution : computorListOfChains)
		{
			Plot2d barPlot = new Plot2d();
			barPlot.setTitle(String.format("Residuals for Chain %s", 
							chainSolution.getExperimentalData().get(0).getAtom().getPrimaryInfo().getChainID()),
							new Font("Ariel", Font.PLAIN, 12));
			barPlot.setIntegerTicks(true);
			barPlot.setXLabel("Residue/Nucleotide #");
			barPlot.setYLabel("Ratio Residuals");
			getContentPane().add(barPlot);
			// barPlot.removeLegend();

			PreSolution typeSolution = chainSolution;

			double[] residues = new double[typeSolution.getExperimentalData().size()];
			toolTips = new ArrayList<String>();

			counter = 0;
			residuals = typeSolution.residuals();
			for (PreDatum datum : typeSolution.getExperimentalData())
			{
				residues[counter] = datum.getAtom().getPrimaryInfo().getResidueNumber();
				toolTips.add(String.format(
						"<html>Residue: %d<br>Chain: %s<br>Residual: %.3f</html>", 
						datum.getAtom().getPrimaryInfo().getResidueNumber(), 
						datum.getAtom().getPrimaryInfo().getChainID(),
						residuals[counter]));

				counter++;
			}

			// label of the current dataset
			String label = String.format("%s, %s", 
					typeSolution.getExperimentalData().get(0).getAtom().getPrimaryInfo().getChainID(),
					preName);
			
			Line residualLine = new Line(residues, residuals, label, toolTips);
			// residualLine.setPaint(new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F,
			// 0.0F, new Color(0, 0, 64)));

			// add to the bar list
			barPlot.addBar(residualLine);
		}

		setVisible(true);
	}
}
