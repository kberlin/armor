/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.Model;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.nmr.relax.gui.RotdifGUI;
import edu.umd.umiacs.armor.refinement.ConvexAlignmentTranslationDocker;
import edu.umd.umiacs.armor.refinement.DockInfo;
import edu.umd.umiacs.armor.util.FileIO;
import edu.umd.umiacs.armor.util.ParseOptions;
import edu.umd.umiacs.armor.util.ProgressObserver;

public class RotdifMain
{	
	public static List<DockInfo> dockResults(File outputPdbFile, ExperimentalRelaxationData experimentalData, PdbStructure pdb, int modelNumber, double temperature, boolean robust) throws AtomNotFoundException, IOException, PdbException, RelaxationDataException
	{
		experimentalData = experimentalData.createRigid();
		ArrayList<ExperimentalRelaxationData> chainList = experimentalData.seperateByChain();
		
		if (chainList.size()!=2)
			throw new RelaxationRuntimeException("Docking must be performed on a two domain file only.");
		
		Model model = pdb.getModel(modelNumber);
	  Model model1 = model.createChain(0);
	  Model model2 = model.createChain(1);
		
		BasicMolecule mol1 = model1.createBasicMolecule();
		BasicMolecule mol2 = model2.createBasicMolecule();
		
		TwoDomainRelaxationAligner twoDomainComputor = new TwoDomainRelaxationAligner(experimentalData, robust);
  	ElmPredictor elmPredictor = new ElmPredictor(temperature);
  	MoleculeRelaxationPredictor relaxPredictor = new MoleculeRelaxationPredictor(twoDomainComputor.getComputor(), elmPredictor);
  	
  	//predict both tensors
  	RotationalDiffusionTensor tensor1 = twoDomainComputor.predictMoleculeTensor1(mol1);
  	RotationalDiffusionTensor tensor2 = twoDomainComputor.predictMoleculeTensor2(mol2);

  	//create the docker
  	ConvexAlignmentTranslationDocker elmDock = new ConvexAlignmentTranslationDocker(twoDomainComputor, relaxPredictor, 1.5);

  	List<DockInfo> dockResults = elmDock.dock(mol1, mol2);
  	//List<DockInfo> dockResults = elmDock.dockWithoutAlignment(mol1, mol2);
  	
  	System.out.println("done.");
  	
  	System.out.println("====ELM Domain 1 Tensor====");
  	System.out.println(tensor1.toLongString());
  	System.out.println("====ELM Domain 2 Tensor====");
  	System.out.println(tensor2.toLongString());
  	
	  System.out.println("====Docking Results====");
	  System.out.println("Docking transformations of second domain ([x_t;y_t;z_t] = R*[x;y;z]+t):");
	  int count = 1;
	  for (DockInfo t : dockResults)
	  {
		  System.out.print("Solution "+count+":\n");
	  	System.out.println("Chi^2 = "+t.getOptimizationResult().getValue());
	  	System.out.print("R = [");
	  	System.out.print(t.getRigidTransform().getRotation()+"]\n");
	  	System.out.print("t = ");
	  	System.out.format("[%.3f %.3f %.3f]\n", t.getRigidTransform().getTranslation().x, t.getRigidTransform().getTranslation().y, t.getRigidTransform().getTranslation().z);
	  	System.out.println();
	  	
	  	count++;
	  }
	  System.out.println("Total number of solutions = "+dockResults.size());
	  
	  if (outputPdbFile!=null)
	  {
		  System.out.print("Outputing results to: "+outputPdbFile+"...");
		 	PdbStructure patiResults = pdb.createFromMoleculeList(new MultiDomainComplex(mol1, mol2).createSecondDomainTranslatedDockList(dockResults));
			MoleculeFileIO.writePDB(outputPdbFile, patiResults.toPDB());
		  System.out.print("done.");
	  }
	  
	  return dockResults;
	}
	
	public static void main(String[] args) throws AtomNotFoundException, PdbException, RelaxationDataException, IOException
	{
  	ParseOptions options = new ParseOptions();
  	options.addOption("pdb", "-pdb", "Name of the PDB file to use for calculations.", "");
  	options.addOption("model", "-model", "Model number of the pdb file.", 1);
  	options.addOption("relax", "-relax", "Name of the relaxation file to use, ROTDIF format.", "");
  	options.addOption("axes", "-axes", "Name of the PyMOL script file for tensor axes. Not generated if set to empty.", false);
  	options.addOption("axeslength", "-axesl", "Length of PyMOL axes extension beyond the molecule bounds (in Angstroms).", 5.0);
  	options.addOption("temperature", "-temp", "Temperature for relaxation in Kelvin.", 298.0);
  	options.addOption("dock", "-dock", "Perform docking of a two domain system and output rotation and translation.", false);
  	options.addOption("out", "-out", "Root string for the names of output PDB files and axes, if requested. Not generated if set to empty.", "");
  	options.addOption("nogui", "-nogui", "Use commandline, instead of GUI, interface.", false);
  	options.addOption("nostat", "-nostat", "Do not compute statistics (speeds up computation).", false);
  	options.addOption("nodynamics", "-nodynamics", "Do not compute dynamics, only tensor (speeds up computation).", false);
  	options.addOption("elm", "-elm", "Run ELM on the pdb file to predict data.", false);
  	options.addOption("robust", "-robust", "Perform robust least-squares regression (outlier suppression) were possible.", false);

  	try 
  	{
  		options.parse(args);
  		if (options.needsHelp())
  		{
  			System.out.println(options.helpMenuString());
  			System.exit(0);
  		}
  		
  		options.checkParameters();
  	}
  	catch (Exception e)
  	{
  		e.printStackTrace(System.err);
  		System.err.println(options.helpMenuString());
  		System.exit(1);
  	}
  	
  	if (options.get("nogui").getBoolean())
  		mainCommand(options);
  	else
  		RotdifGUI.mainGUI();
	}

	public static void mainCommand(ParseOptions options) throws AtomNotFoundException, PdbException, RelaxationDataException, IOException
	{
		if (options.get("help").getBoolean() || !options.get("pdb").isSet())
		{
			System.err.println("The pdb option was not set.");
  		System.err.println(options.helpMenuString());
  		System.exit(0);;
		}
		
		System.out.println("Options:");
		System.out.println(options);
		
		String relaxFile = options.get("relax").getString();
	  String pdbFile = options.get("pdb").getString();
	  boolean robust = options.get("robust").getBoolean();

	  //read experimental data
	  ExperimentalRelaxationData experimentalData = null;
	  if (!relaxFile.isEmpty())
	  {
		  System.out.print("Extracting experimental data....");
	  	try
	  	{
	  		experimentalData = ExperimentalRelaxationData.readDataFile(new File(relaxFile));
		  	System.out.println("done.");
	  	}
	  	catch (Exception e2)
	  	{
	  		System.err.println("Could not parse relaxation data.\n");
	  		e2.printStackTrace(System.err);
	  		System.exit(1);
	  	}
	  }

	  //adjust model number to start with 0
	  int model = options.get("model").getInteger()-1;
	  
	  //read the pdb file
	  PdbStructure pdb = null;
	  try
	  {
		  System.out.print("Parsing pdb file....");
	  	pdb = MoleculeFileIO.readPDB(pdbFile);
			System.out.println("done.");
	  }
	  catch(Exception e)
	  {
  		System.err.println("Could not parse PDB data.\n");
  		e.printStackTrace(System.err);
  		System.exit(1);
	  }
		
	  //check that model number is ok
		if (pdb.getNumberOfModels()<=model)
		{
			System.out.println("Invalid model number: "+model+". Failed!");
  		System.exit(1);
		}
		
		Molecule molecule = pdb.getModel(model).createBasicMolecule();
		double tempK = options.get("temperature").getDouble();
		
 		
		if (!relaxFile.isEmpty())
		{
			//compute the results
		  System.out.print("Computing results....");
		  RotdifResults results = null;
		  try
		  {
		  	ProgressObserver obs = new ProgressObserver()
				{
		  		private double max = 0.0;
		  		
					@Override
					public synchronized void update(double progressPercentage)
					{
						if (progressPercentage>this.max+.1)
						{
							System.out.format("%.1f%%...",progressPercentage*100);
							this.max = progressPercentage;
						}
					}
				};
				
				ArrayList<ProgressObserver> observers = new ArrayList<ProgressObserver>();
				observers.add(obs);
		  	
		  	results = new RotdifResults(experimentalData, molecule, !options.get("nostat").getBoolean(), !options.get("nodynamics").getBoolean(), robust, observers);
		  }
		  catch (AtomNotFoundException e)
		  {
		  	e.printStackTrace(System.err);
	  		System.exit(1);
		  }
									
			if (!options.get("dock").isBoolean())
			{
				ArrayList<ExperimentalRelaxationData> chainList = experimentalData.seperateByChain();
				
				if (chainList.size()!=2 )
				{
					System.out.println("Docking must be performed on a two domain file only.");
		  		System.exit(1);
				}
			}
			
			//compute the statistic
			if (!options.get("nostat").getBoolean())
			{
				results.computeStatistic();
			}
		  System.out.println("done.");
			
		  //show backcalculatation results
			System.out.println(results.generateOutputString());
			
			//show prediction results
			System.out.println(results.toString());
			
		  double lengthAddition = options.get("axeslength").getDouble();
		  
		  //get the output name
			String outPdbName = options.get("out").getString();
			File outFile = null;
			if (!outPdbName.isEmpty())
				outFile = new File(outPdbName);
			
			if (options.get("axes").getBoolean())
			{
		  	//generate the 3 files
		  	File saveAniFile = FileIO.ensureExtension(outFile, "_ani", "py");
				File saveAxiFile = FileIO.ensureExtension(outFile, "_axial", "py");
								  	
			  System.out.print("Saving anisotropic tensor axes, if computed, to \""+saveAniFile+"\" and\n");
			  System.out.print("\taxially symmetric tensor, if computed, axes to \""+saveAxiFile+"\"...");

			  if (results.getAniTensor()!=null)
					MoleculeFileIO.writePymolAxesScript(saveAniFile, molecule, results.getAniTensor().getProperEigenDecomposition(), lengthAddition);
				if (results.getAxiTensor()!=null)
					MoleculeFileIO.writePymolAxesScript(saveAxiFile, molecule, results.getAxiTensor().getProperEigenDecomposition(), lengthAddition);

				System.out.println("done.");
			}
			
			if (options.get("elm").getBoolean())
			{
				System.out.print("Computing the ELM tensor prediction...");
				
			  //now save the predicted axes
			  ElmPredictor predictor = new ElmPredictor(tempK);
				RotationalDiffusionTensor tensor = predictor.predictTensor(molecule);

				System.out.println("done");
				
				System.out.println(results.generatePredictionString(molecule, tensor));
				
				if (!options.get("axes").getBoolean())
				{
					File axesFile = FileIO.ensureExtension(outFile, "_predicted", "py");
				  System.out.print("Saving predicted tensor axes to \""+axesFile+"\"...");
				  MoleculeFileIO.writePymolAxesScript(axesFile, pdb.getModel(model).createBasicMolecule(), 
				  		tensor.getProperEigenDecomposition(), lengthAddition);
				  System.out.println("done.\n");
				}				
			}
			
			//if docking version turned on
			if (options.get("dock").getBoolean())
			{
				//compute the statistic
				if (!options.get("nostat").getBoolean())
				{
				  if (options.get("dock").getBoolean())
				  {
					  System.out.print("Computing alignment uncertainty (computationally intensive)...");
				  	computeAlignmentError(experimentalData, pdb.getModel(model), 150, robust);
				  }			  
				}
				
				System.out.print("Computing docking results...");
				File dockPdb = FileIO.ensureExtension(outFile, "_dock", "pdb");
				dockResults(dockPdb, experimentalData, pdb, model, tempK, robust);
				System.out.println("");
				
				if (options.get("axes").getBoolean() && outFile!=null)
				{
				  System.out.print("Saving docking tensor axes to \""+outFile+"*.py\"...");
				  
				  //read the pdb file
				  PdbStructure dockedPdb = MoleculeFileIO.readPDB(dockPdb);
				  
				  for (int iter=0; iter<dockedPdb.getNumberOfModels(); iter++)
				  {
				  	String name1 = dockedPdb.getModel(iter).getModelChains().get(0);
				  	String name2 = dockedPdb.getModel(iter).getModelChains().get(1);
				  	Molecule mol1 = dockedPdb.getModel(iter).createChain(0).createBasicMolecule();
				  	Molecule mol2 = dockedPdb.getModel(iter).createChain(1).createBasicMolecule();
				  	
			  	  ExperimentalRelaxationData data1 = experimentalData.createFromExistingAtoms(mol1);	  
			  	  ExperimentalRelaxationData data2 = experimentalData.createFromExistingAtoms(mol2);
			  	  
			    	AnisotropicRelaxationComputor d1Computor = new AnisotropicRelaxationComputor(data1, options.get("robust").getBoolean());
			    	AnisotropicRelaxationComputor d2Computor = new AnisotropicRelaxationComputor(data2, options.get("robust").getBoolean());

			    	RotationalDiffusionTensor tensor1 = d1Computor.computeTensor(mol1);
			    	RotationalDiffusionTensor tensor2 = d2Computor.computeTensor(mol2);

				  	MoleculeFileIO.writePymolAxesScript(new File(outFile.getName().concat("_"+name1+"_"+(iter+1)+".py")), mol1, tensor1.getProperEigenDecomposition(), lengthAddition);
					  MoleculeFileIO.writePymolAxesScript(new File(outFile.getName().concat("_"+name2+"_"+(iter+1)+".py")), mol2, tensor2.getProperEigenDecomposition(), lengthAddition);
				  }
				  
				  System.out.println("done.");
				}

			}
		}
		else
		{
			System.out.print("Predicting tensor...");
			ElmPredictor predictor = new ElmPredictor(tempK);
			RotationalDiffusionTensor tensor = predictor.predictTensor(molecule);
			System.out.println("done.\n");
			System.out.println(tensor.toLongString());
		}
	}
	
	private static void computeAlignmentError(ExperimentalRelaxationData experimentalData, Model model, final int number, final boolean robust) throws AtomNotFoundException, PdbException, RelaxationDataException
	{
		Model model1 = model.createChain(0);
		Model model2 = model.createChain(1);
		
		final Molecule mol1 = model1.createBasicMolecule();
		final Molecule mol2 = model2.createBasicMolecule();
		
	  final Collection<ArrayList<Rotation>> alignmentMonteCarlo = Collections.synchronizedCollection(new ArrayList<ArrayList<Rotation>>());
	  
		final ExperimentalRelaxationData experimentalDataFinal  = experimentalData.createRigid();
		final TwoDomainRelaxationAligner computor = new TwoDomainRelaxationAligner(experimentalData, robust);	  
	  ArrayList<Rotation> alignmentSolution = computor.align(mol1, mol2);
	  	  
	  final ArrayList<Exception> exceptionErrors = new ArrayList<Exception>();
		//ExecutorService execSvc = Executors.newCachedThreadPool();
		ExecutorService execSvc = Executors.newFixedThreadPool(Math.max(1, Runtime.getRuntime().availableProcessors()));
		
    Runnable sampleTask = new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					ExperimentalRelaxationData randomizedList = experimentalDataFinal.createMonteCarlo();

				  TwoDomainRelaxationAligner computor = new TwoDomainRelaxationAligner(randomizedList, robust);	  
				  alignmentMonteCarlo.add(computor.align(mol1, mol2));

				  System.out.print(".");
				}
				catch (Exception e)
				{
					exceptionErrors.add(new RelaxationRuntimeException(e.getMessage(), e));
				}
			}
		};
    
		for (int iter=0; iter<number; iter++)
		{
			execSvc.execute(sampleTask);
		}
		
    //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(120L, TimeUnit.MINUTES);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new RelaxationRuntimeException("Unable to finish all tasks.", e);
    }
    
    if (!exceptionErrors.isEmpty())
    	throw new RelaxationRuntimeException(exceptionErrors.get(0));
	    		
		//find the closest orientation
		double largestError = 0.0;
		for (Rotation currSolution : alignmentSolution)
		{
			double errorSum = 0.0;
			for (ArrayList<Rotation> monteCarloSolution  : alignmentMonteCarlo)
			{
				double minError = Double.POSITIVE_INFINITY;				
				for (Rotation currRotation : monteCarloSolution)
					minError = Math.min(minError, currRotation.distance(currSolution));
				
				errorSum += minError;
			}
			
			largestError = Math.max(largestError, errorSum/(double)alignmentMonteCarlo.size());
		}
		
		
		System.out.println("\n========Uncertainty in Two Domain Alignment=========");
	  System.out.format("Alignment Orientational Uncertainty = %.1f deg\n\n", largestError*180.0/BasicMath.PI);
	}
}

