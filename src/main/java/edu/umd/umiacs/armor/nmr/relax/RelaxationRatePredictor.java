/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;

/**
 * The Interface RelaxationRatePredictor.
 * 
 * @param <E>
 *          the element type
 */
public interface RelaxationRatePredictor extends Serializable
{
	
	public RelaxationRatePredictor createFromMolecule(Molecule mol) throws AtomNotFoundException;
	
	/**
	 * Creates the with new csa.
	 * 
	 * @param CSA
	 *          the cSA
	 * @return the e
	 */
	public RelaxationRatePredictor createWithCSA(double CSA);
	
	public RelaxationRatePredictor createWithParameters(double CSA, double r, double csaAngle, double Rex);
	
	public RelaxationRatePredictor createWithRadius(double r);
	
	public RelaxationRatePredictor createWithRex(double Rex);
	
	public RelaxationRatePredictor createWithCsaAngle(double csaAngle);
	
	/**
	 * Creates the with new rotational diffusion tensor.
	 * 
	 * @param tensor
	 *          the tensor
	 * @return the e
	 */
	public RelaxationRatePredictor createWithTensor(RotationalDiffusionTensor tensor);
	
	/**
	 * Creates the with new vector.
	 * 
	 * @param bondVector
	 *          the bond vector
	 * @return the e
	 */
	public RelaxationRatePredictor createWithVector(Point3d bondVector);
	
	/**
	 * Gets the bond.
	 * 
	 * @return the bond
	 */
	public Bond getBond();
	
	/**
	 * Gets the cSA.
	 * 
	 * @return the cSA
	 */
	public double getCSA();
	
	/**
	 * Gets the gamma from atom.
	 * 
	 * @return the gamma from atom
	 */
	public double getGammaFromAtom();
	
	/**
	 * Gets the gamma to atom.
	 * 
	 * @return the gamma to atom
	 */
	public double getGammaToAtom();
	
	public double getCsaAngle();

	/**
	 * Gets the radius.
	 * 
	 * @return the radius
	 */
	public double getRadius();
	
	/**
	 * Gets the rex.
	 * 
	 * @return the rex
	 */
	public double getRex();
		
	/**
	 * Predict.
	 * 
	 * @param frequency
	 *          the frequency
	 * @return the double[]
	 */
	public RelaxationDatum predict(double frequency);
	
	public double[] predict(double frequency, RelaxationDatum datum);
	
	public double predictEtaXy(double frequency);
	
	public double predictEtaZ(double frequency);
	
	/**
	 * Predict noe.
	 * 
	 * @param frequency
	 *          the frequency
	 * @return the double
	 */
	public double predictNOE(double frequency);
	
	/**
	 * Predict r1.
	 * 
	 * @param frequency
	 *          the frequency
	 * @return the double
	 */
	public double predictR1(double frequency);
	
	/**
	 * Predict r2.
	 * 
	 * @param frequency
	 *          the frequency
	 * @return the double
	 */
	public double predictR2(double frequency);

	public double predictRho(double frequency);
	
	public double predictRhoEta(double frequency);
}
