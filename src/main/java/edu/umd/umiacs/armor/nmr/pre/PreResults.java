/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.ProgressObservable;
import edu.umd.umiacs.armor.util.ProgressObserver;

public class PreResults implements ProgressObservable
{
	//private final boolean rescale;
	private ArrayList<ProgressObserver> observers;
	//private final boolean robust;
	//private final ExperimentalPreData preData;
	private final PreSolution solution;

	public PreResults(ExperimentalPreData preData, Molecule molecule, boolean robust, boolean rescale) throws AtomNotFoundException
	{
    //this.robust = robust;
    //this.rescale = rescale;
    //this.preData = preData;
		
		PrePositionComputor computor = new PrePositionComputor(preData, robust, rescale);
		this.solution = computor.solve(molecule);
		
	}
	
	@Override
	public void addProgressObserver(ProgressObserver o)
	{
		this.observers.add(o);
	}

	public void computeStatistic()
	{
		// TODO Auto-generated method stub
		
	}

	public synchronized String generateOutputString()
	{
		StringBuilder str = new StringBuilder();
		
	  str.append("========PRE Position=========\n");
		str.append(String.format("Position: [%.2f, %.2f %.2f]\n", this.solution.getPrePosition().x,this.solution.getPrePosition().y,this.solution.getPrePosition().z));
	  str.append(String.format("Q = %.3f  R = %.3f  chi^2=%.3f (Rigid Only)\n", this.solution.qualityFactor(), 
	  		this.solution.pearsonsCorrelation(),
	  		this.solution.chi2()));
		
	  str.append("\n========Full PRE Results=========\n");
		str.append("<residue><chain><atom1><pre_exp><pre_pred><rel_error><*outlier>\n");

		double[] predictedValues = this.solution.predict();
		double[] relativeError = this.solution.getExperimentalData().absErrorsByStd(predictedValues);
		double sigma = BasicStat.std(this.solution.createRigid().getExperimentalData().absErrorsByStd(predictedValues));
		double maxError = 0.0;
		for (double error : relativeError)
			maxError = Math.max(maxError,Math.abs(error));
		
		int index = 0;
	  for (PreDatum pre : this.solution.getExperimentalData())
	  {
  		str.append(String.format("%d %s %s %.3f %.3f %.4f", pre.getAtom().getPrimaryInfo().getResidueNumber(),
  				pre.getAtom().getPrimaryInfo().getChainID(),
  				pre.getAtom().getPrimaryInfo().getAtomName(),
  				pre.value(),
  				predictedValues[index],
  				pre.absErrorByStd(predictedValues[index])));

			//put star if outlier
			if (Math.abs(relativeError[index])>=maxError)
				str.append(" ***");
			else
			if (Math.abs(relativeError[index])>BasicStat.NORMAL_CONFIDENCE_INTERVAL_99*sigma)
					str.append(" **");
			else
			if (Math.abs(relativeError[index])>BasicStat.NORMAL_CONFIDENCE_INTERVAL_95*sigma)
				str.append(" *");
  		str.append("\n");
			
  		index++;
  		
	  }
	  str.append(String.format("[%.3f*sigma (95%% rigid conf. interv.), %.3f*sigma (99%% rigid conf. interv.)] [abs max] relative errors: [%.3f, %.3f] [%.3f]\n", 
	  		BasicStat.NORMAL_CONFIDENCE_INTERVAL_95, BasicStat.NORMAL_CONFIDENCE_INTERVAL_99,
	  		BasicStat.NORMAL_CONFIDENCE_INTERVAL_95*sigma, BasicStat.NORMAL_CONFIDENCE_INTERVAL_99*sigma, 
	  		maxError));

		
		return str.toString();
	}

	public PreSolution getSolution()
	{
		return this.solution;
	}

	@Override
	public void removeProgressObserver(ProgressObserver o)
	{
		this.observers.remove(o);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return generateOutputString();
	}
}
