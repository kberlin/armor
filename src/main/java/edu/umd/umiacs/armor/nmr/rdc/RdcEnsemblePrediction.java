/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2014 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.linear.LinearMathFileIO;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.DuplicateAtomException;
import edu.umd.umiacs.armor.molecule.Model;
import edu.umd.umiacs.armor.molecule.MoleculeException;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.filters.ResidueStructureFilter;
import edu.umd.umiacs.armor.refinement.AbstractSparseEnsembleSelection;
import edu.umd.umiacs.armor.refinement.EnsembleInputProgressObserver;
import edu.umd.umiacs.armor.refinement.RefinementRuntimeException;
import edu.umd.umiacs.armor.util.ParseOptions;

public final class RdcEnsemblePrediction implements EnsembleInputProgressObserver
{
	private int maxColumns = 0;
	private int columns = 0;

	public static void main(String[] args) throws IOException, PdbException, ClassNotFoundException, RuntimeException, RdcDataException, DuplicateAtomException, MoleculeException
	{
  	ParseOptions options = new ParseOptions();
  	options.addOption("bicelle", "bicelle", "Bicelle concentration for RDCs.", 0.05);
  	options.addRequiredOption("input", "input", "Molecule text files directory path.", String.class);
  	options.addRequiredOption("rdc", "rdc", "Pati formatted RDC filename.", String.class);
  	options.addRequiredOption("output", "output", "Output directory with the resulting matrix and data vector.", String.class);
  	options.addOption("rdcOverlay", "overlay", "Overlay file when computing RDCs. Rarely used, two domain molecule only. Set to empty for none.", "");
  	
  	try 
  	{
  		options.parse(args);
  		if (options.needsHelp())
  		{
  			System.out.println(options.helpMenuString());
  			System.exit(0);
  		}
  		
  		options.checkParameters();
  	}
  	catch (Exception e)
  	{
  		System.err.println(e.getMessage());
  		System.err.println(options.helpMenuString());
  		System.exit(1);
  	}
  	
  	//process the input
  	RdcEnsembleInput input = createInput(options);
  	input.addProgressObserver(new RdcEnsemblePrediction());
  	
  	List<File> moleculeFilesText = getMoleculeFiles(options,"txt");
  	List<File> moleculeFilesPdb = getMoleculeFiles(options,"pdb");
  	
  	if (moleculeFilesText.isEmpty() && moleculeFilesPdb.isEmpty())
  	{
  		System.out.println("Not molecule files found in "+options.get("input").getString()+".");
  		System.exit(1);
  	}
 	
  	//process the directory of files
  	System.out.println("\n=====PROCESSING=====");
  	try
  	{
  		int textProcessed = 0;
  		if (!moleculeFilesText.isEmpty())
  		{
	  		System.out.print("Processing molecule text files...");
	  		input.addTextModels(moleculeFilesText);
	  		System.out.println(""+input.ensembleSize()+"...done.");
	  		textProcessed = input.ensembleSize();
  		}

  		if (!moleculeFilesPdb.isEmpty())
  		{
	  		System.out.print("Processing molecule pdb files...");
	  		input.addPdbModels(moleculeFilesPdb);
	  		System.out.println(""+(input.ensembleSize()-textProcessed)+"...done.");
  		}
  		
  		System.out.println("Total molecules processed: "+input.ensembleSize());
  	}
  	catch (RefinementRuntimeException e)
  	{
  		System.err.println("Processing failed. Check if overlay file is needed.");
  		e.printStackTrace();
  		System.err.println(options.helpMenuString());
  		System.exit(1);
  	}
  	
  	System.out.println("\n=====OUPUT=====");
  	
  	//output the file
  	String predictionFile = options.get("output").getString()+File.separator+"rdc_matrix.txt";
  	String expFile = options.get("output").getString()+File.separator+"rdc_data.txt";

  	System.out.println("Writting the prediction matrix to "+predictionFile+".");
  	LinearMathFileIO.write(new File(options.get("output").getString()+File.separator+"rdc_matrix.txt"), input.getPredictionMatrix());

  	System.out.println("Writting the experimental data vector and error vector to "+expFile+".");
  	LinearMathFileIO.write(new File(options.get("output").getString()+File.separator+"rdc_data.txt"), input.getExperimentalData().valuesAndErrors());
 }
	
	public static List<File> getMoleculeFiles(ParseOptions options, final String type) throws PdbException
	{
		if (options.get("input").getString().isEmpty())
			return null;
		
		//get pdb files
		FilenameFilter filter = new FilenameFilter() 
		{
	    @Override
			public boolean accept(File dir, String name) 
	    {
	      return name.endsWith("."+type);
	      //return Integer.valueOf(name.substring(name.length()-7, name.length()-4))<=1;
	    }
		};
		
	 	List<File> pdbFiles = AbstractSparseEnsembleSelection.getMoleculeFiles(options.get("input").getString(), filter);
	 	
	 	return pdbFiles;
	}
	
	public static RdcEnsembleInput createInput(ParseOptions options)
	{
   	if (options.get("rdc").getString().isEmpty())
  	{
  		System.err.println("RDC filename name cannot be empty.");
  		System.err.println(options.helpMenuString());
  		System.exit(1);
  	}

		String rdcFile = options.get("rdc").getString();
		String overlayPdb  =  options.get("rdcOverlay").getString();
		
	 	try
		{
	 		System.out.println("Reading in experimental data from "+options.get("rdc").getString()+".");
	 		
			//create data list
		  ExperimentalRdcData experimentalData = ExperimentalRdcData.readPati(new File(rdcFile));
			
			//create predictor
		 	PatiPredictor rdcPredictor = PatiPredictor.createBicellePredictor(options.get("bicelle").getDouble());
	
		 	//create filter
		 	ResidueStructureFilter structFilter = null;
		 	
		 	//open the overlay structure
		 	ArrayList<BasicMolecule> molList = null;
		 	if (!overlayPdb.isEmpty())
		 	{
			 	Model model = MoleculeFileIO.readPDB(overlayPdb).getModel(0);
			 	
			 	ArrayList<String> chains = model.getModelChains();
			 	molList = new ArrayList<BasicMolecule>(chains.size());
			 	
			 	for (String chain : chains)
			 		molList.add(model.createChain(chain).createBasicMolecule());
		 	}
		 	
		 	//create the input
		 	RdcEnsembleInput ensembleInput = new RdcEnsembleInput(experimentalData, rdcPredictor, molList, structFilter);
		 	
		 	return ensembleInput;
		}
		catch (PdbException e)
		{
			throw new RefinementRuntimeException(e);
		}	 		
		catch (IOException e)
		{
			throw new RefinementRuntimeException(e);
		}	 		
	}

	@Override
	public synchronized void updateProcessed(int number)
	{	
		this.maxColumns = Math.max(number, this.maxColumns);
		if (this.maxColumns==number && number>=this.columns+100)
		{
			System.out.format("%d...", number);			
			System.out.flush();
			this.columns = number;
		}
	}
	
}
