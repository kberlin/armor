/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.molecule.AbstractMolecule;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.molecule.VirtualMoleculeWrapper;
import edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor;

/**
 * The Class TwoDomainPatiPredictor.
 */
public final class TwoDomainPatiPredictor implements TwoDomainRigidPredictor
{
	
	/** The computor. */
	private final AlignmentComputor computor;

	/** The domain two transform. */
	private final RigidTransform domainTwoTransform;
	
	/** The height function. */
	private final TwoDomainHeightFunction heightFunction;
	
	/** The m1. */
	private final Molecule m1;
	
	/** The m2. */
	private final Molecule m2;
	
	/** The predictor. */
	private final PatiPredictor predictor;
	
	/**
	 * Instantiates a new two domain pati predictor.
	 * 
	 * @param m1
	 *          the m1
	 * @param m2
	 *          the m2
	 * @param predictor
	 *          the predictor
	 * @param computor
	 *          the computor
	 */
	public TwoDomainPatiPredictor(Molecule m1, Molecule m2, PatiPredictor predictor, AlignmentComputor computor)
	{
		this.domainTwoTransform = RigidTransform.identityTransform();

		this.m1 = m1;
		this.m2 = m2;
		this.predictor = predictor;
		this.heightFunction = new TwoDomainHeightFunction(m1, m2);
		this.computor = computor;
	}
	
	/**
	 * Instantiates a new two domain pati predictor.
	 * 
	 * @param old
	 *          the old
	 * @param t
	 *          the t
	 */
	private TwoDomainPatiPredictor(TwoDomainPatiPredictor old, RigidTransform t)
	{
		this.domainTwoTransform = old.domainTwoTransform.union(t);

		//the surface is transformed in the actual call function
		this.m1 = old.m1;
		this.m2 = old.m2;
		this.computor = old.computor;
		this.heightFunction = old.heightFunction;
		
		this.predictor = old.predictor;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#createRotated(edu.umd.umiacs.armor.math.Rotation)
	 */
	@Override
	public TwoDomainPatiPredictor createRotated(Rotation R)
	{
		return new TwoDomainPatiPredictor(this, new RigidTransform(R, null));
	}
	
	@Override
	public ExperimentalRdcData getData()
	{
		return this.computor.getData();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#getDomainOne()
	 */
	@Override
	public Molecule getDomainOne()
	{
		return this.m1;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#getDomainTwo()
	 */
	@Override
	public Molecule getDomainTwo()
	{
		return new VirtualMoleculeWrapper(this.m2).createTransformedMolecule(this.domainTwoTransform);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#initPositions(edu.umd.umiacs.armor.math.RigidTransform)
	 */
	@Override
	public ArrayList<Point3d> initPositions(RigidTransform t)
	{
		RigidTransform totalTransform = this.domainTwoTransform.union(t);
		
	  Point3d c1 = this.m1.center();
	  Point3d c2 = totalTransform.transform(this.m2.center());
	  Point3d twoOntoOneShift = c1.subtract(c2);
	  
	  double r1 = radiusMoleculeOne();
	  double r2 = radiusMoleculeTwo();
	  double dist = r1+r2;
	  
	  //generate initial points
	  ArrayList<Point3d> x0List = new ArrayList<Point3d>(6);
	  x0List.add(twoOntoOneShift.add(new Point3d(-dist,0,0)));
	  x0List.add(twoOntoOneShift.add(new Point3d(dist,0,0)));
	  x0List.add(twoOntoOneShift.add(new Point3d(0,-dist,0)));
	  x0List.add(twoOntoOneShift.add(new Point3d(0,dist,0)));
	  x0List.add(twoOntoOneShift.add(new Point3d(0,0,-dist)));
	  x0List.add(twoOntoOneShift.add(new Point3d(0,0,dist)));
	  
	  return x0List;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#predict(edu.umd.umiacs.armor.math.Point3d)
	 */
	@Override
	public double[] predict(Point3d translation)
	{
		return predict(new RigidTransform(null, translation));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#predict(edu.umd.umiacs.armor.math.RigidTransform)
	 */
	@Override
	public double[] predict(RigidTransform t)
	{
		AlignmentTensor tensor = predictTensor(t);
		
		try
		{
			AbstractMolecule complex = new MultiDomainComplex(this.m1, this.m2, this.domainTwoTransform.union(t));
			
			return this.computor.predict(complex, tensor);
		}
		catch (AtomNotFoundException e)
		{
			throw new RdcRuntimeException("Unexpected AtomNotFoundException.",e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#predict(edu.umd.umiacs.armor.math.Rotation)
	 */
	@Override
	public double[] predict(Rotation R)
	{
		return predict(new RigidTransform(R, null));
	}

	/**
	 * Predict tensor.
	 * 
	 * @param t
	 *          the t
	 * @return the alignment tensor
	 */
	public AlignmentTensor predictTensor(RigidTransform t)
	{
		RigidTransform totalTransform = this.domainTwoTransform.union(t);
		return this.predictor.predictTensor(this.heightFunction.createTransformed(totalTransform));
	}

	public double radiusMoleculeOne()
	{
		return this.m1.maxAtomDistance()/2.0;
	}

	public double radiusMoleculeTwo()
	{
		return this.m2.maxAtomDistance()/2.0;
	}

}
