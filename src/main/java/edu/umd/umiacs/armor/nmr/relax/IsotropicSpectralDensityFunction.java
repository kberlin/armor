/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Class IsotropicSpectralDensityFunction.
 */
public final class IsotropicSpectralDensityFunction implements MFSpectralDensityFunction<IsotropicSpectralDensityFunction>
{
	
	private final double S2Fast;

	/** The S2. */
	private final double S2Slow;
  
	/** The tau c. */
  private final double tauC; //in seconds

	private final double tauFast; //in seconds
	
	/** The tau local. */
	private final double tauLocal; //in seconds

	/**
	 * 
	 */
	private static final long serialVersionUID = -6492002109448188211L;
	
	/** The Constant TWOFIFTHS. */
	private final static double TWOFIFTHS = 2.0/5.0;
	
	/**
	 * Convert tau.
	 * 
	 * @param value
	 *          the value
	 * @return the double
	 */
	private static double convertTau(double value)
	{
		return value*1.0e-9;
	}
	
	public IsotropicSpectralDensityFunction(double tauC, double S2Slow, double tauLocal, double S2Fast, double tauFast)
	{
		this(tauC, S2Slow, tauLocal, S2Fast, tauFast, false);
	}
	
	private IsotropicSpectralDensityFunction(double tauC, double S2Slow, double tauLocal, double S2Fast, double tauFast, boolean directCopy)
	{		
		if (directCopy)
		{
			this.S2Slow = S2Slow;
			this.tauLocal = tauLocal;
			this.S2Fast = S2Fast;
			this.tauFast = tauFast;
			this.tauC = tauC;
		}
		else
		{
			this.S2Slow = S2Slow;
			this.tauLocal = convertTau(tauLocal);
			this.S2Fast = S2Fast;
			this.tauFast = convertTau(tauFast);
			this.tauC = convertTau(tauC);
		}
		
		if (S2Slow<0.0 || S2Slow>1.0 || Double.isNaN(S2Slow))
			throw new RelaxationRuntimeException("Invalid value for S2: "+S2Slow);
		if (tauLocal<0.0 || Double.isNaN(tauLocal))
			throw new RelaxationRuntimeException("Invalid value for tauLocal: "+tauLocal);
		if (S2Fast<0.0 || S2Fast>1.0 || Double.isNaN(S2Fast))
			throw new RelaxationRuntimeException("Invalid value for S2: "+S2Fast);
		if (tauFast<0.0 || Double.isNaN(tauFast))
			throw new RelaxationRuntimeException("Invalid value for tauLocal: "+tauFast);
		
		if (tauC<0.0 || Double.isNaN(tauC))
			throw new RelaxationRuntimeException("Invalid value for tauC: "+tauC);

	}
	
	/**
	 * Creates the from.
	 * 
	 * @param <T>
	 *          the generic type
	 * @param func
	 *          the func
	 * @return the isotropic spectral density function
	 */
	public <T extends MFSpectralDensityFunction<T>> IsotropicSpectralDensityFunction createFrom(T func)
	{
		return new IsotropicSpectralDensityFunction(func.getTensor().getTauC(), func.getS2Slow(), func.getTauLocal(), func.getS2Fast(), func.getTauFast());
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewLocal(double, double)
	 */
	@Override
	public IsotropicSpectralDensityFunction createWithLocal(double S2, double tauLocal)
	{
		IsotropicSpectralDensityFunction J = new IsotropicSpectralDensityFunction(this.tauC, S2, convertTau(tauLocal), this.S2Fast, this.tauFast, true);		
		
		return J;

	}
	
	@Override
	public IsotropicSpectralDensityFunction createWithLocal(double S2, double tauLocal, double S2Fast, double tauLocalFast)
	{
		IsotropicSpectralDensityFunction J = new IsotropicSpectralDensityFunction(this.tauC, S2, convertTau(tauLocal), S2Fast, convertTau(tauLocalFast), true);		
		
		return J;
	}

	@Override
	public IsotropicSpectralDensityFunction createWithS2Fast(double S2Fast)
	{
		IsotropicSpectralDensityFunction J = new IsotropicSpectralDensityFunction(this.tauC, this.S2Slow, this.tauLocal, S2Fast, this.tauFast, true);		
		
		return J;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewS2(double)
	 */
	@Override
	public IsotropicSpectralDensityFunction createWithS2Slow(double S2)
	{
		IsotropicSpectralDensityFunction J = new IsotropicSpectralDensityFunction(this.tauC, S2, this.tauLocal, this.S2Fast, this.tauFast, true);		
		
		return J;
	}

	/**
	 * Creates the with new tau c.
	 * 
	 * @param tauC
	 *          the tau c
	 * @return the isotropic spectral density function
	 */
	public IsotropicSpectralDensityFunction createWithTauC(double tauC)
	{
		IsotropicSpectralDensityFunction J = new IsotropicSpectralDensityFunction(convertTau(tauC), this.S2Slow, this.tauLocal, this.S2Fast, this.tauFast, true);		
		return J;
	}

	@Override
	public IsotropicSpectralDensityFunction createWithTauFast(double tauFast)
	{
		IsotropicSpectralDensityFunction J = new IsotropicSpectralDensityFunction(this.tauC, this.S2Slow, this.tauLocal, this.S2Fast, convertTau(tauFast), true);		
		
		return J;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewTauLocal(double)
	 */
	@Override
	public IsotropicSpectralDensityFunction createWithTauLocal(double tauLocal)
	{
		IsotropicSpectralDensityFunction J = new IsotropicSpectralDensityFunction(this.tauC, this.S2Slow, convertTau(tauLocal), this.S2Fast, this.tauFast, true);		
		
		return J;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewRotationalDiffusionTensor(edu.umd.umiacs.armor.nmr.relax.RotationalDiffusionTensor)
	 */
	@Override
	public IsotropicSpectralDensityFunction createWithTensor(RotationalDiffusionTensor tensor)
	{
		return createWithTauC(tensor.getTauC());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewVector(edu.umd.umiacs.armor.math.Point3d)
	 */
	@Override
	public IsotropicSpectralDensityFunction createWithVector(Point3d bondVector)
	{
		return this;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#getS2()
	 */
	@Override
	public double getS2()
	{
		return this.S2Slow*this.S2Fast;
	}

	@Override
	public double getS2Fast()
	{
		return this.S2Fast;
	}
	
	@Override
	public double getS2Slow()
	{
		return this.S2Slow;
	}

	@Override
	public double getTauC()
	{
		return this.tauC*1.0e9; //convert to ns
	}

	@Override
	public double getTauFast()
	{
		return this.tauFast*1.0e9;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#getTauLocal()
	 */
	@Override
	public double getTauLocal()
	{
		return this.tauLocal*1.0e9;
	}

	@Override
	public RotationalDiffusionTensor getTensor()
	{
		return RotationalDiffusionTensor.create(getTauC());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.relax.SpectralDensityFunction#value(double)
	 */
	@Override
	public final double value(double omega)
	{
		double Jordered = valueOrdered(omega);
		double Jdisordered = valueDisordered(omega);
		
		double J = Jordered+Jdisordered;
		
		return J;
	}

	public final double valueDisordered(double omega)
	{	   
		double S2 = getS2();
		
	  double Jdisordered = 0.0;
	  if (this.S2Fast-S2>0.0)
	  {
	   	//tau_e = 1.0/(1.0/tauC+1.0/tauLocal);
			double tau_e = this.tauC*this.tauLocal/(this.tauC+this.tauLocal);
	    double omegaTau = omega*tau_e;		  
	  	Jdisordered = (this.S2Fast-S2)*tau_e/(1.0+omegaTau*omegaTau);
	  }
	  
	  double JdisorderedFast = 0.0;
	  if (this.S2Fast<1.0)
	  {
			double tau_f = this.tauC*this.tauFast/(this.tauC+this.tauFast);
	    double omegaTauFast = omega*tau_f;
		  JdisorderedFast = (1.0-this.S2Fast)*tau_f/(1.0+omegaTauFast*omegaTauFast);
	  }

	  return (TWOFIFTHS)*(Jdisordered+JdisorderedFast);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.relax.SpectralDensityFunction#valueOrdered(double)
	 */
	@Override
	public final double valueOrdered(double omega)
	{
	  double omegaTau = omega*this.tauC;
	  double Jordered = (getS2()*this.tauC)/(1.0+omegaTau*omegaTau);

	  return (TWOFIFTHS)*Jordered;
	}

}
