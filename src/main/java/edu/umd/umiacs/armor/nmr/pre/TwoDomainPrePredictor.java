/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OutlierDampingFunction;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.molecule.filters.MoleculeSVDAligner;
import edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor;
import edu.umd.umiacs.armor.util.Pair;

public final class TwoDomainPrePredictor implements TwoDomainRigidPredictor
{
	private final class PositionFunction implements MultivariateVectorFunction
	{
		private final double[] expectedValues;
		private final RigidTransform initPosition;
		
		private PositionFunction(RigidTransform initPosition, double[] expectedValues)
		{
			this.initPosition = initPosition;
			this.expectedValues = expectedValues;
		}

		@Override
		public final double[] value(double[] x) throws IllegalArgumentException
		{
			RigidTransform totalTransform = this.initPosition.union(new RigidTransform(Rotation.identityRotation(), new Point3d(x[0],x[1],x[2])));

			double[] predictedValues = predict(totalTransform);
			if (TwoDomainPrePredictor.this.rescale)
			{
				double scale = BasicStat.optimalScaling(predictedValues, this.expectedValues);
				predictedValues = BasicMath.mult(predictedValues, scale);
			}
			
			return predictedValues;
		}
	}
	
	private final MultiDomainComplex complex;
	private final ArrayList<PrePositionComputor> computor1List;
	private final ArrayList<PrePositionComputor> computor2List;
	private final ArrayList<PrePositionComputor> computorCombinedList;
	private final ArrayList<ArrayList<Point3d>> mol1Positions;
	private final ArrayList<ArrayList<Point3d>> mol2Positions;
	private final double outlierThreshold;
	private final boolean rescale;
	private final boolean rigid;
	private final RigidTransform rigidTransform;
	private final boolean robust;
	private final int size;
	
	public TwoDomainPrePredictor(Molecule mol1, Molecule mol2, List<ExperimentalPreData> dataList, boolean robust, boolean rescale, double outlierThreshold) throws AtomNotFoundException
	{
		this.rigid = false;
		this.robust = robust;
		this.rescale = rescale;
		this.outlierThreshold = outlierThreshold;
		this.computor1List = new ArrayList<PrePositionComputor>();
		this.computor2List = new ArrayList<PrePositionComputor>();
		this.computorCombinedList = new ArrayList<PrePositionComputor>();
		this.rigidTransform = RigidTransform.identityTransform();
		
		int size = 0;
		this.mol1Positions = new ArrayList<ArrayList<Point3d>>(dataList.size());
		this.mol2Positions = new ArrayList<ArrayList<Point3d>>(dataList.size());
		for (ExperimentalPreData data : dataList)
		{
			data = data.createRigid();
			
			PrePositionComputor combinedComputor = new PrePositionComputor(data, this.robust, this.rescale, this.outlierThreshold);
			this.computorCombinedList.add(combinedComputor);
			size += combinedComputor.size();
			
			ExperimentalPreData data1 = data.createFromExistingAtoms(mol1);
			if (data1.size()<3 || (data1.size()<4 && rescale))
				continue;

			ExperimentalPreData data2 = data.createFromExistingAtoms(mol2);
			if (data2.size()<3 || (data2.size()<4 && rescale))
				continue;
			
			//extract the atomic positions for first computor
			PrePositionComputor computor1 = new PrePositionComputor(data1, this.robust, this.rescale, this.outlierThreshold);			
			this.mol1Positions.add(computor1.extractPositions(mol1));			
			this.computor1List.add(computor1);
			
			//extract the atomic positions for first computor
			PrePositionComputor computor2 = new PrePositionComputor(data2, this.robust, this.rescale, this.outlierThreshold);			
			this.mol2Positions.add(computor2.extractPositions(mol2));
			this.computor2List.add(computor2);
		}
		
		if (this.computor1List.isEmpty())
			throw new AtomNotFoundException("PRE data must exists for both molecules.", null);

		
		this.complex = new MultiDomainComplex(mol1, mol2);

		
		
		this.size = size;
	}
	
	private TwoDomainPrePredictor(TwoDomainPrePredictor original, RigidTransform t)
	{
		this.rigid = original.rigid;
		this.robust = original.robust;
		this.rescale = original.rescale;
		this.outlierThreshold = original.outlierThreshold;
		this.computorCombinedList = original.computorCombinedList;
		this.computor1List = original.computor1List;
		this.computor2List = original.computor2List;
		this.mol1Positions = original.mol1Positions;
		this.mol2Positions = original.mol2Positions;
		this.size = original.size;

		this.rigidTransform = original.rigidTransform.union(t);
		this.complex = original.complex;
	}

	public Point3d computeTranslation(RigidTransform t)
	{
		//current complex
		Molecule mol = this.complex.createSecondDomainTransformed(this.rigidTransform.union(t));
		
		ArrayList<Point3d> points = new ArrayList<Point3d>(numberOfPre());
		for (int iter=0; iter<numberOfPre(); iter++)
		{			
			Point3d p1;
			Point3d p2;
			try
			{
				p1 = this.computor1List.get(iter).solvePosition(mol);
				p2 = this.computor2List.get(iter).solvePosition(mol);
			}
			catch (AtomNotFoundException e)
			{
				throw new PreRuntimeException("Unexpected error.", e);
			}
			
			points.add(p1.subtract(p2));
		}
		
		Point3d optimalTranslation = Point3d.meanPosition(points);
		
		//get the experimental values
		ExperimentalPreData expData = getData();
		final double[] expectedValues = expData.values();
		final double[] errors = expData.errors();

		//create the function that predicts the values based on position
		PositionFunction positionFunc = new PositionFunction(RigidTransform.identityTransform(), expectedValues);

		//create the optimizer
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(3000);
		optimizer.setAbsPointDifference(PrePositionComputor.ABSOLUTE_POSITION_ERROR*1.0e4);
		optimizer.setObservations(expectedValues);
		optimizer.setErrors(errors);
		
		MultivariateVectorFunction func = positionFunc;
		if (this.robust)
			func = new  OutlierDampingFunction(func, expectedValues, errors, this.outlierThreshold);
		optimizer.setFiniteDifferenceFunction(func);

		//solve for a better solution
		OptimizationResultImpl sol = optimizer.optimize(optimalTranslation.toArray());
		optimalTranslation = new Point3d(sol.getPoint());
		
		return optimalTranslation;
	}
	
	@Override
	public TwoDomainPrePredictor createRotated(Rotation R)
	{
		return new TwoDomainPrePredictor(this, new RigidTransform(R, null));
	}

	public TwoDomainPrePredictor createTransformed(RigidTransform t)
	{
		return new TwoDomainPrePredictor(this, t);
	}

	public PrePositionComputor getComputor(int index)
	{
		return this.computorCombinedList.get(index);
	}

	@Override
	public ExperimentalPreData getData()
	{
		ArrayList<PreDatum> expData = new ArrayList<PreDatum>(this.size);
		for (PrePositionComputor data: this.computorCombinedList)
			for (PreDatum datum : data.getData())
				expData.add(datum);
		
		return new ExperimentalPreData(expData);
	}

	@Override
	public Molecule getDomainOne()
	{
		return this.complex.getDomainOne();
	}

	@Override
	public Molecule getDomainTwo()
	{
		return this.complex.getDomainTwo();
	}

	public boolean hasAlignmentInformation()
	{
		return numberOfPre()>1;
	}
	
	public RigidTransform initialTransform(RigidTransform t) throws AtomNotFoundException
	{
		RigidTransform optimalTransform = null;
		
		Molecule mol = this.complex.createSecondDomainTransformed(this.rigidTransform.union(t));
				
		//if one point then just position them on top of each other
		if (!hasAlignmentInformation())
		{
			Point3d p1 = this.computor1List.get(0).solvePosition(mol);
			Point3d p2 = this.computor2List.get(0).solvePosition(mol);
			
			optimalTransform = new RigidTransform(Rotation.identityRotation(), p1.subtract(p2));
		}
		else
		{
			//has alignment information
			ArrayList<Pair<Point3d,Point3d>> positions = new ArrayList<Pair<Point3d,Point3d>>();
			for (int iter=0; iter<numberOfPre(); iter++)
			{			
				Point3d p1 = this.computor1List.get(iter).solvePosition(mol);
				Point3d p2 = this.computor2List.get(iter).solvePosition(mol);
				
				positions.add(new Pair<Point3d,Point3d>(p1, p2));
			}

			//align the domains
			optimalTransform = MoleculeSVDAligner.SVDAligner(positions);
		}
					
		return optimalTransform;
	}
	
	@Override
	public ArrayList<Point3d> initPositions(RigidTransform t)
	{
		ArrayList<Point3d> translationList = new ArrayList<Point3d>(1);
		translationList.add(computeTranslation(t));
		
		return translationList;
	}
	
	public int numberOfPre()
	{
		return this.computorCombinedList.size();
	}

	@Override
	public double[] predict(Point3d translation)
	{
		return predict(new RigidTransform(null, translation));
	}
	
	@Override
	public double[] predict(RigidTransform t)
	{
		RigidTransform combinedTransform = this.rigidTransform.union(t);
		
		double[] values = new double[this.size];
		int offset = 0;
		int counter = 0;
		for (PrePositionComputor computor : this.computorCombinedList)
		{
			ArrayList<Point3d> combinedList = new ArrayList<Point3d>(this.mol1Positions.get(counter).size()+this.mol2Positions.get(counter).size());
			combinedList.addAll(this.mol1Positions.get(counter));
			
			//rotate the second domain
			for (Point3d atomPosition : this.mol2Positions.get(counter))
				combinedList.add(combinedTransform.transform(atomPosition));
			
			PreSolution sol = computor.solve(combinedList);
			
			double[] currValues = sol.predict();
			for (int iter=0; iter<currValues.length; iter++)
			{
				values[offset+iter] = currValues[iter];
			}
			
			offset += currValues.length;
			counter++;
		}
		
		return values;
	}

	@Override
	public double[] predict(Rotation R)
	{
		return predict(new RigidTransform(R, null));
	}
}
