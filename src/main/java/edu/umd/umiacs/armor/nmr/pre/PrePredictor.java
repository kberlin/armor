/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.refinement.Predictor;

public final class PrePredictor implements Predictor
{
	private final ArrayList<PrePositionComputor> computorList;
	private final ArrayList<ExperimentalPreData> dataList;
	private final double outlierThreshold;
	private final boolean rescale;
	private final boolean robust;
	private final int size;

	public PrePredictor(List<ExperimentalPreData> dataList, boolean robust, boolean rescale) throws PreDataException
	{
		this(dataList,robust,rescale,PrePositionComputor.OUTLIER_THRESHOLD);
	}

	public PrePredictor(List<ExperimentalPreData> dataList, boolean robust, boolean rescale, double outlierThreshold) throws PreDataException
	{
		if (dataList==null)
			throw new NullPointerException("PRE data list cannot be null.");
		if (dataList.size()<=0)
			throw new PreDataException("PRE data list must have at least one dataset.");
		
		this.robust = robust;
		this.rescale = rescale;
		this.outlierThreshold = outlierThreshold;
		
		this.dataList = new ArrayList<ExperimentalPreData>(dataList.size());
		this.computorList = new ArrayList<PrePositionComputor>(dataList.size());
		int size = 0;
		for (ExperimentalPreData data : dataList)
		{
			data = data.createRigid();
			
			PrePositionComputor combinedComputor = new PrePositionComputor(data, robust, rescale, outlierThreshold);
			
			this.computorList.add(combinedComputor);
			this.dataList.add(data);
			
			size+= data.size();
		}
		
		this.size = size;
		
		if (this.size<=0)
			throw new PreDataException("PRE data must have more than one rigid component.");
	}

	@Override
	public TwoDomainPrePredictor createTwoDomainRigidPredictor(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		return new TwoDomainPrePredictor(mol1, mol2, this.dataList, this.robust, this.rescale, this.outlierThreshold);
	}

	public PrePositionComputor getComputor(int index)
	{
		return this.computorList.get(index);
	}

	@Override
	public ExperimentalPreData getData()
	{
		ArrayList<PreDatum> expData = new ArrayList<PreDatum>();
		for (PrePositionComputor data : this.computorList)
			for (PreDatum datum : data.getData())
				expData.add(datum);
		
		return new ExperimentalPreData(expData);
	}
	
	/**
	 * @return the outlierThreshold
	 */
	public double getOutlierThreshold()
	{
		return this.outlierThreshold;
	}

	/**
	 * @return the robust
	 */
	public boolean isRobust()
	{
		return this.robust;
	}

	@Override
	public double[] predict(Molecule mol) throws AtomNotFoundException
	{
		double[] values = new double[this.size];
		int offset = 0;
		
		for (PrePositionComputor computor : this.computorList)
		{
			PreSolution sol;
			try
			{
				sol = computor.solve(mol);
			}
			catch (AtomNotFoundException e)
			{
				throw new PreRuntimeException("Unexpected error.", e);
			}
			
			double[] currValues = sol.predict();
			for (int iter=0; iter<currValues.length; iter++)
			{
				values[offset+iter] = currValues[iter];
			}
			
			offset += currValues.length;
		}
		
		return values;	}

}
