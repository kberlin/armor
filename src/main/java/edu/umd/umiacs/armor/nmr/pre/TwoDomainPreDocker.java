/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OutlierDampingFunction;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.refinement.DockInfo;
import edu.umd.umiacs.armor.refinement.RigidDocker;
import edu.umd.umiacs.armor.util.ProgressObserver;

public final class TwoDomainPreDocker implements RigidDocker
{
	private final class OrientationFunction implements MultivariateVectorFunction
	{
		private final RigidTransform initPosition;
		private final TwoDomainPrePredictor predictor;

		private OrientationFunction(TwoDomainPrePredictor predictor, RigidTransform initPosition)
		{
			this.predictor = predictor;
			this.initPosition = initPosition;
		}
		
		public final Rotation getRotation(double[] x)
		{
			return Rotation.createZYZ(x[0], x[1], x[2]);
		}
		
		@Override
		public final double[] value(double[] x) throws IllegalArgumentException
		{
			//generate the positioner
			RigidTransform combinedTransform = this.initPosition.union(new RigidTransform(getRotation(x), null));
			
			//get the best translation
			Point3d bestTranslation = this.predictor.computeTranslation(combinedTransform);
			combinedTransform = combinedTransform.union(new RigidTransform(null, bestTranslation));
			
			//predict the values
			double[] predictedValues = this.predictor.predict(combinedTransform);
			
			return predictedValues;
		}
	}
	
	private final ArrayList<ProgressObserver> observers;
	private final double outlierThreshold;
	private final PrePredictor predictor;
	private final boolean robust;

	public TwoDomainPreDocker(PrePredictor predictor, boolean robust)
	{
		this(predictor, robust, PrePositionComputor.OUTLIER_THRESHOLD);
	}
	
	public TwoDomainPreDocker(PrePredictor predictor, boolean robust, double outlierThreshold) 
	{
		this.robust = robust;
		this.outlierThreshold = outlierThreshold;
		this.predictor = predictor;
		this.observers = new ArrayList<ProgressObserver>();
	}
	
	@Override
	public synchronized void addProgressObserver(ProgressObserver o)
	{
		this.observers.add(o);
	}
	
	public RigidTransform computeTransform(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		TwoDomainPrePredictor predictor = this.predictor.createTwoDomainRigidPredictor(mol1, mol2);
		
		//get the initial extimate of the position
		RigidTransform initPosition = predictor.initialTransform(RigidTransform.identityTransform());
		
		//for two and one pre sets, the solution is the initial solution
		if (predictor.numberOfPre()<=2)
			return initPosition;
				
		//get the experimental data
		ExperimentalPreData expData = predictor.getData();
		final double[] expectedValues = expData.values();
		final double[] errors = expData.errors();
		
		//create the optimizer
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(3000);
		optimizer.setAbsPointDifference(PrePositionComputor.ABSOLUTE_POSITION_ERROR*1.0e4);
		optimizer.setObservations(expectedValues);
		optimizer.setErrors(errors);
		
		OrientationFunction orientationFunc = new OrientationFunction(predictor, initPosition);
		MultivariateVectorFunction func = orientationFunc;
		if (this.robust)
			func = new  OutlierDampingFunction(func, expectedValues, errors, this.outlierThreshold);
		optimizer.setFiniteDifferenceFunction(func);
		
		final double[] x0 = {0.0,0.0,0.0};
		OptimizationResultImpl sol = optimizer.optimize(x0);
		
		//get the solution
		Rotation rotationSol = orientationFunc.getRotation(sol.getPoint());
		
		//recompute the optimal position
		Point3d positionSol = predictor.computeTranslation(initPosition.union(new RigidTransform(rotationSol, null)));
		
		//extract the translation
		RigidTransform optimalPosition = initPosition.union(new RigidTransform(rotationSol, positionSol));
		
		return optimalPosition;
	}

	@Override
	public ArrayList<DockInfo> dock(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		ArrayList<DockInfo> solList = new ArrayList<DockInfo>(1);
		solList.add(new DockInfo(computeTransform(mol1, mol2), null));
		
		return solList;
	}

	public RigidTransform initalTransform(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		TwoDomainPrePredictor predictor = this.predictor.createTwoDomainRigidPredictor(mol1, mol2);
		
		return predictor.initialTransform(RigidTransform.identityTransform());
	}

	@Override
	public synchronized void removeProgressObserver(ProgressObserver o)
	{
		int index = this.observers.indexOf(o);
		
		if (index>=0)
			this.observers.remove(index);
	}
}
