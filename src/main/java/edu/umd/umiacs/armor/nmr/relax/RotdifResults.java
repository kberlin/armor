/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Ftest;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.physics.BasicPhysics;
import edu.umd.umiacs.armor.physics.Tensor3d;
import edu.umd.umiacs.armor.util.ProgressObservable;
import edu.umd.umiacs.armor.util.ProgressObserver;
import edu.umd.umiacs.armor.util.SortType;

/**
 * The Class RotdifResults.
 */
public final class RotdifResults implements ProgressObservable
{
	
	private double aniAic;

	private final AnisotropicRelaxationComputor aniComputor;
	
	/** The ani monte carlo. */
	private final List<RelaxationSolution<AnisotropicModelFreePredictor>> aniMonteCarlo;
	
	/** The ani computor. */
	private final RelaxationSolution<AnisotropicModelFreePredictor> aniSolution;

	private final RelaxationSolution<AnisotropicModelFreePredictor> aniSolutionDynamics;
	
	/** The ani tensor. */
	private final RotationalDiffusionTensor aniTensor;
	
	/** The ani tensor monte carlo. */
	private final List<RotationalDiffusionTensor> aniTensorMonteCarlo;
	
	private double axiAic;
	
	private final AxiallySymmetricRelaxationComputor axiComputor;
	
	/** The axi monte carlo. */
	private final List<RelaxationSolution<AnisotropicModelFreePredictor>> axiMonteCarlo;

	/** The axi computor. */
	private final RelaxationSolution<AnisotropicModelFreePredictor> axiSolution;
	
	private final RelaxationSolution<AnisotropicModelFreePredictor> axiSolutionDynamics;
	
	/** The axi tensor. */
	private final RotationalDiffusionTensor axiTensor;
	
	/** The axi tensor monte carlo. */
	private final List<RotationalDiffusionTensor> axiTensorMonteCarlo;
	
	private final RelaxationSolution<? extends ModelFreePredictor> bestDynamicsSolution;
	
	private final RelaxationSolution<? extends ModelFreePredictor> bestSolution;
	
	private String bestSolutionType;
	
	private boolean computationComplete;
		
	/** The exception errors. */
	private final List<RelaxationRuntimeException> exceptionErrors;

	/** The full statistics. */
	private final boolean fullStatistics;
	
	private double isoAic;
	
	private final IsotropicRelaxationComputor isoComputor;
	
	/** The iso monte carlo. */
	private final List<RelaxationSolution<IsotropicModelFreePredictor>> isoMonteCarlo;
	
	/** The iso computor. */
	private final RelaxationSolution<IsotropicModelFreePredictor> isoSolution;
	
	private final RelaxationSolution<IsotropicModelFreePredictor> isoSolutionDynamics;
	
	/** The iso tensor. */
	private final RotationalDiffusionTensor isoTensor;
	
	/** The iso tensor monte carlo. */
	private final List<RotationalDiffusionTensor> isoTensorMonteCarlo;

	/** The list. */
	private final ExperimentalRelaxationData list;

	/** The molecule. */
	private final Molecule molecule;

	/** The number of required samples. */
	private final int numberOfRequiredSamples;

	/** The observers. */
	private final ArrayList<ProgressObserver> observers;

	/** The predicted tensor. */
	private final RotationalDiffusionTensor predictedTensor;

	/** The robust fit. */
	private final boolean robust;

	private final boolean computeDynamics;
	
	/** The Constant NUM_MONTECARLO_SAMPLES. */
	public static final int NUM_MONTECARLO_SAMPLES = 220;

	/** The Constant REX_PERCENT_CUTOFF. */
	public static final double REX_PERCENT_CUTOFF = .10;

	/** The Constant S2_FLIXIBLE_CUTOFF. */
	public static final double S2_FLIXIBLE_CUTOFF = .75;

	/**
	 * Gets the proper eigen decompsotion3d list.
	 * 
	 * @param monteCarloTensors
	 *          the monte carlo tensors
	 * @return the proper eigen decompsotion3d list
	 */
	private static ArrayList<EigenDecomposition3d> getProperEigenDecomposition3dList(List<RotationalDiffusionTensor> monteCarloTensors)
	{
		ArrayList<EigenDecomposition3d> eigList = new ArrayList<EigenDecomposition3d>();
		for (RotationalDiffusionTensor tensor : monteCarloTensors)
			eigList.add(tensor.getProperEigenDecomposition());
		
		return eigList;
	}
	
	public RotdifResults(ExperimentalRelaxationData list, Molecule molecule, boolean fullStatistics, boolean computeDynamics, boolean robust, List<ProgressObserver> observers) throws AtomNotFoundException
	{
		this.computationComplete = false;
		
		this.observers = new ArrayList<ProgressObserver>();
		this.fullStatistics = fullStatistics;
		this.computeDynamics = computeDynamics;
		
		if (fullStatistics)
			this.numberOfRequiredSamples = NUM_MONTECARLO_SAMPLES;
		else
			this.numberOfRequiredSamples = 0;

		if (observers!=null)
			this.observers.addAll(observers);
		
		this.robust = robust;
		
		this.molecule = molecule;
		
		this.exceptionErrors = Collections.synchronizedList(new ArrayList<RelaxationRuntimeException>());

		this.list = list;
		
		this.isoMonteCarlo = Collections.synchronizedList(new ArrayList<RelaxationSolution<IsotropicModelFreePredictor>>());
		this.axiMonteCarlo = Collections.synchronizedList(new ArrayList<RelaxationSolution<AnisotropicModelFreePredictor>>());
		this.aniMonteCarlo = Collections.synchronizedList(new ArrayList<RelaxationSolution<AnisotropicModelFreePredictor>>());
		this.isoTensorMonteCarlo = Collections.synchronizedList(new ArrayList<RotationalDiffusionTensor>());
		this.axiTensorMonteCarlo = Collections.synchronizedList(new ArrayList<RotationalDiffusionTensor>());
		this.aniTensorMonteCarlo = Collections.synchronizedList(new ArrayList<RotationalDiffusionTensor>());
		
		//perform initial notification
		notifyProgressObservers();
		
		this.isoComputor = new IsotropicRelaxationComputor(this.list, this.robust);
		
		if (computeDynamics)
		{
			this.isoSolution = this.isoComputor.solve(molecule);
			this.isoSolutionDynamics = this.isoSolution;
		}
		else
		{
			this.isoSolution = this.isoComputor.solveTensor(molecule);
			this.isoSolutionDynamics = null;
		}
		this.isoTensor = this.isoSolution.getTensor();
				
		//init the AIC
	  this.isoAic = BasicStat.akaikeInformationCriterion(this.isoSolution.rhoChi2Effective(), 1);
		//double isoAic = Statistics.akaikeInformationCriterionCorrected(this.isoSolution.rhoChi2Effective(), 1, this.isoSolution.createRigid().sizeRho());
	  this.axiAic = Double.POSITIVE_INFINITY;
	  this.aniAic = Double.POSITIVE_INFINITY;
		
		this.axiComputor = new AxiallySymmetricRelaxationComputor(this.list, this.robust);
		if (this.axiComputor.isTensorComputable())
		{
			if (computeDynamics)
			{
				this.axiSolutionDynamics = this.axiComputor.solve(molecule);
				this.axiSolution = this.axiSolutionDynamics;
			}
			else
			{
				this.axiSolution = this.axiComputor.solveTensor(molecule);
				this.axiSolutionDynamics = null;
			}
			this.axiTensor = this.axiSolution.getTensor();

			this.axiAic = BasicStat.akaikeInformationCriterion(this.axiSolution.rhoChi2Effective(), 4);
		  //axiAic = Statistics.akaikeInformationCriterionCorrected(this.axiSolution.rhoChi2Effective(), 4, this.axiSolution.createRigid().sizeRho());
		}
		else
		{
			this.axiSolution = null;
			this.axiTensor = null;
			this.axiSolutionDynamics = null;
		}

		this.aniComputor = new AnisotropicRelaxationComputor(this.list, this.robust);
		if (this.aniComputor.isTensorComputable())
		{
			if (computeDynamics)
			{
				this.aniSolutionDynamics = this.aniComputor.solve(molecule);
				this.aniSolution = this.aniSolutionDynamics;
			}
			else
			{
				this.aniSolution = this.axiComputor.solveTensor(molecule);
				this.aniSolutionDynamics = null;
			}
			this.aniTensor = this.aniSolution.getTensor();
		  this.aniAic = BasicStat.akaikeInformationCriterion(this.aniSolution.rhoChi2Effective(), 6);
			//aniAic = Statistics.akaikeInformationCriterionCorrected(this.aniSolution.rhoChi2Effective(), 6, this.aniSolution.createRigid().sizeRho());
		}
		else
		{
			this.aniSolution = null;
			this.aniTensor = null;
			this.aniSolutionDynamics = null;
		}
		
		//select best solution based on rigid bond fit AIC
	  if (this.aniAic<this.isoAic && this.aniAic<this.axiAic)
	  {
	  	this.bestSolutionType = "Anisotropic";
	  	this.bestSolution = this.aniSolution;
	  	this.bestDynamicsSolution = this.aniSolutionDynamics;
	  }
	  else
	  if (this.axiAic<this.isoAic)
	  {
	  	this.bestSolutionType = "Axially-Symmetric";
	  	this.bestSolution = this.axiSolution;
	  	this.bestDynamicsSolution = this.axiSolutionDynamics;
	  }
	  else
	  {
	  	this.bestSolutionType = "Isotropic";
	  	this.bestSolution = this.isoSolution;
	  	this.bestDynamicsSolution = this.isoSolutionDynamics;
	  }
			
		//compute the approximate solutions			
	  this.predictedTensor = new ElmPredictor(300.0).predictTensor(molecule);
		
		notifyProgressObservers();
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ProgressObservable#addProgressObserver(edu.umd.umiacs.armor.util.ProgressObserver)
	 */
	@Override
	public void addProgressObserver(ProgressObserver o)
	{
		this.observers.add(o);
	}
	
	/**
	 * Compute additional samples.
	 * 
	 * @param number
	 *          the number
	 */
	private void computeAdditionalSamples(int number)
	{
		if (number<0)
			return;
		
		//ExecutorService execSvc = Executors.newCachedThreadPool();
		ExecutorService execSvc = Executors.newFixedThreadPool(Math.max(1, Runtime.getRuntime().availableProcessors()-1));
		
    Runnable sampleTask = new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					ExperimentalRelaxationData randomizedList = RotdifResults.this.list.createMonteCarlo();
					
					IsotropicRelaxationComputor isoComputor = new IsotropicRelaxationComputor(randomizedList, 
							RotdifResults.this.isoSolution.getPredictors(), 
							RotdifResults.this.fullStatistics, RotdifResults.this.fullStatistics,
							RotdifResults.this.robust, AbstractModelFreeComputor.OUTLIER_THRESHOLD, 1.0e-5);

					RelaxationSolution<IsotropicModelFreePredictor> isoSolution;
					if (RotdifResults.this.computeDynamics)
						isoSolution = isoComputor.solve(RotdifResults.this.molecule, RotdifResults.this.isoSolutionDynamics.getPredictors());
					else
						isoSolution = isoComputor.solveTensor(RotdifResults.this.molecule);
					RotdifResults.this.isoMonteCarlo.add(isoSolution);
					RotdifResults.this.isoTensorMonteCarlo.add(isoSolution.getTensor());

					if (RotdifResults.this.axiSolution!=null)
					{
						AxiallySymmetricRelaxationComputor axiComputor = new AxiallySymmetricRelaxationComputor(randomizedList, 
								RotdifResults.this.axiSolution.getPredictors(), 
								RotdifResults.this.fullStatistics, RotdifResults.this.fullStatistics,
								RotdifResults.this.robust, AbstractModelFreeComputor.OUTLIER_THRESHOLD, 1.0e-5);
						if (axiComputor.isTensorComputable())
						{
							RelaxationSolution<AnisotropicModelFreePredictor> axiSolution;
							if (RotdifResults.this.computeDynamics)
								axiSolution = axiComputor.solve(RotdifResults.this.molecule, RotdifResults.this.aniSolutionDynamics.getPredictors());
							else
								axiSolution = axiComputor.solveTensor(RotdifResults.this.molecule);
							RotdifResults.this.axiMonteCarlo.add(axiSolution);
							RotdifResults.this.axiTensorMonteCarlo.add(axiSolution.getTensor());
						}
					}
					
					if (RotdifResults.this.aniSolution!=null)
					{
						AnisotropicRelaxationComputor aniComputor = new AnisotropicRelaxationComputor(randomizedList, 
								RotdifResults.this.aniSolution.getPredictors(), 
								RotdifResults.this.fullStatistics, RotdifResults.this.fullStatistics,
								RotdifResults.this.robust, AbstractModelFreeComputor.OUTLIER_THRESHOLD, 1.0e-5);
						if (aniComputor.isTensorComputable())
						{
							RelaxationSolution<AnisotropicModelFreePredictor> aniSolution;
							if (RotdifResults.this.computeDynamics)
								aniSolution = aniComputor.solve(RotdifResults.this.molecule, RotdifResults.this.aniSolutionDynamics.getPredictors());
							else
								aniSolution = aniComputor.solveTensor(RotdifResults.this.molecule);
							RotdifResults.this.aniMonteCarlo.add(aniSolution);
							RotdifResults.this.aniTensorMonteCarlo.add(aniSolution.getTensor());
						}
					}
					
					//set the observer update
					notifyProgressObservers();
				}
				catch (Exception e)
				{
					RotdifResults.this.exceptionErrors.add(new RelaxationRuntimeException(e.getMessage(), e));
				}
			}
		};
    
		for (int iter=0; iter<number; iter++)
		{
			execSvc.execute(sampleTask);
		}
		
    //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(120L, TimeUnit.MINUTES);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new RelaxationRuntimeException("Unable to finish all tasks.", e);
    }
    
    if (!this.exceptionErrors.isEmpty())
    	throw this.exceptionErrors.get(0);
    
    if (numberOfRequiredSamples()<=numberOfCurrentSamples())
    	this.computationComplete = true;

    notifyProgressObservers();
	}
	
	/**
	 * Compute statistic.
	 */
	public void computeStatistic()
	{
		computeAdditionalSamples(numberOfRequiredSamples()-numberOfCurrentSamples());
	}
		
	/**
	 * Generate output string.
	 * 
	 * @return the string
	 */
	public synchronized String generateOutputString()
	{
		StringBuilder str = new StringBuilder();
		
	  //create temp storage variables
	  EigenDecomposition3d eig;
	  ArrayList<EigenDecomposition3d> eigList;
	  double[] angles;
	  double[] anglesStd;
	  
	  Tensor3d intertiaTensor = BasicPhysics.moleculeInertiaTensor(this.molecule);

	  EigenDecomposition3d eigIntertiaTensor = intertiaTensor.getEigenDecomposition().createSortedEigenDecomposition(SortType.DESCENDING);
	  EigenDecomposition3d eigPredictedTensor = this.predictedTensor.getProperEigenDecomposition();
	  
	  //create rigid solutions
	  RelaxationSolution<IsotropicModelFreePredictor> isoSolutionRigid = this.isoSolution.createRigid();
	  
	  RelaxationSolution<AnisotropicModelFreePredictor> axiSolutionRigid = null;
	  if (this.axiSolution!=null)
		  axiSolutionRigid = this.axiSolution.createRigid();

	  RelaxationSolution<AnisotropicModelFreePredictor> aniSolutionRigid = null;
	  if (this.aniSolution!=null)
	    aniSolutionRigid = this.aniSolution.createRigid();
	  
	  str.append("=====ROBUST REGRESSION: "+ (this.robust?"On":"Off") +"=====\n");

	  if (axiSolutionRigid!=null)
	  {
		  str.append("\n=====ORIENTATION SAMPLING=====\n");
		  str.append(String.format("Number of rigid bonds = %d\n", isoSolutionRigid.size()));
		  double xi = axiSolutionRigid.computeOrientationalXi();
		  str.append(String.format("Sampling Param Xi = %.3f", xi));
		  if (xi>.25)
		  	str.append(" Warning Xi>.25! Vectors are not uniformly distributed!\n");
		  else
		  	str.append(" Vectors are uniformly distributed.\n");
		  
		  double[] xiEigenvalues = axiSolutionRigid.computeOrientalXiEigenvalues();
		  str.append(String.format("Sampling Tensor Components = [%.3f, %.3f, %.3f].\n", xiEigenvalues[0], xiEigenvalues[1], xiEigenvalues[2]));
	  }
	  
	  str.append("\n=====ISOTROPIC MODEL=====\n");
	  eig = this.isoTensor.getProperEigenDecomposition();
	  eigList = getProperEigenDecomposition3dList(this.isoTensorMonteCarlo);
	  str.append(String.format("Diso = %.2f +/- %.2f *(10^7) 1/s\n", eig.getEigenvalue(0), eig.computeEigenvalueStd(0, eigList)));
	  str.append(String.format("TAUc = %.2f +/- %.2f ns\n", this.isoTensor.getTauC(), this.isoTensor.computeTauCStd(this.isoTensorMonteCarlo)));
	  str.append(String.format("Q = %.3f  chi^2 = %.3f  chi^2/df = %.3f (Rigid Only)\n", isoSolutionRigid.rhoQualityFactor(), 
	  		isoSolutionRigid.rhoChi2Effective(), 
	  		isoSolutionRigid.rhoChi2Effective()/(isoSolutionRigid.sizeRho()-1.0)));
	  
	  //AIC
	  str.append(String.format("Akaike Information Criterion (AIC): Value = %.3f\n", this.isoAic));

	  if (this.axiSolution != null)
	  {
		  eig = this.axiTensor.getProperEigenDecomposition();
		  eigList = getProperEigenDecomposition3dList(this.axiTensorMonteCarlo);
		  angles = eig.getRotation().getAnglesZYZ();
		  anglesStd = eig.computeZYZAnglesStd(eigList);
		  
		  //figure out the right order of the tensor values
		  double dx, dz, dxStd, dzStd, interiaMajorAxisDiff, predictedMajorAxisDiff;
		  if (Math.abs(eig.getEigenvalue(0)-eig.getEigenvalue(1))<Math.abs(eig.getEigenvalue(1)-eig.getEigenvalue(2)))
		  {
		  	dx = eig.getEigenvalue(0);
		  	dz = eig.getEigenvalue(2);
		  	dxStd = eig.computeEigenvalueStd(0, eigList);
		  	dzStd = eig.computeEigenvalueStd(2, eigList);
			  interiaMajorAxisDiff = BasicMath.angleAbsolute(eigIntertiaTensor.getRotation().getColumn(2), eig.getRotation().getColumn(2));
			  predictedMajorAxisDiff = BasicMath.angleAbsolute(eigPredictedTensor.getRotation().getColumn(2), eig.getRotation().getColumn(2));
			  str.append("\n=====AXIALLY-SYMMETRIC MODEL (PROLATE)=====\n");
		  }
		  else
		  {
		  	dx = eig.getEigenvalue(2);
		  	dz = eig.getEigenvalue(0);
		  	dxStd = eig.computeEigenvalueStd(2, eigList);
		  	dzStd = eig.computeEigenvalueStd(0, eigList);
			  interiaMajorAxisDiff = BasicMath.angleAbsolute(eigIntertiaTensor.getRotation().getColumn(0), eig.getRotation().getColumn(0));
			  predictedMajorAxisDiff = BasicMath.angleAbsolute(eigIntertiaTensor.getRotation().getColumn(0), eig.getRotation().getColumn(0));
			  str.append("\n=====AXIALLY-SYMMETRIC MODEL (OBLATE)=====\n");
		  }
		  
		  str.append(String.format("D_perp = %.2f +/- %.2f *(10^7) 1/s\n", dx, dxStd));
			str.append(String.format("D_parallel = %.2f +/- %.2f *(10^7) 1/s\n",dz, dzStd));		
			str.append(String.format("alpha = %.0f +/- %.0f  deg\n", angles[0]*180.0/BasicMath.PI,anglesStd[0]*180.0/BasicMath.PI));
			str.append(String.format("beta = %.0f +/- %.0f  deg\n", angles[1]*180.0/BasicMath.PI,anglesStd[1]*180.0/BasicMath.PI));
		  str.append(String.format("Orientational Uncertainty = %.0f deg\n", eig.computeOrientationalStd(eigList)*180.0/BasicMath.PI));
			str.append(String.format("TAUc = %.2f +/- %.2f ns\n", this.axiTensor.getTauC(),this.axiTensor.computeTauCStd(this.axiTensorMonteCarlo)));
			str.append(String.format("anisotropy = %.2f +/- %.2f\n",this.axiTensor.anisotropy(),this.axiTensor.computeAnisotropyStd(this.axiTensorMonteCarlo)));
		  str.append(String.format("Q = %.3f  R = %.3f  chi^2 = %.3f  chi^2/df = %.3f (Rigid Only)\n", axiSolutionRigid.rhoQualityFactor(), 
		  		axiSolutionRigid.rhoPearsonsCorrelation(),
		  		axiSolutionRigid.rhoChi2Effective(), 
		  		axiSolutionRigid.rhoChi2Effective()/(axiSolutionRigid.sizeRho()-4.0)));
		  
		  str.append(String.format("Inertia Tensor Unique Axis Difference = %.0f deg\n", interiaMajorAxisDiff*180.0/BasicMath.PI));
		  str.append(String.format("ELM Predicted Tensor Unique Axis Difference = %.0f deg\n", predictedMajorAxisDiff*180.0/BasicMath.PI));

		  //F-test
		  if (axiSolutionRigid.sizeRho()>4)
		  {
			  Ftest ftestIsoAxi = new Ftest(isoSolutionRigid.sizeRho()-1, isoSolutionRigid.rhoChi2Effective(), axiSolutionRigid.sizeRho()-4, axiSolutionRigid.rhoChi2Effective());
			  str.append(String.format("F-test (0.05): Axially-Symmetric model better than Isotropic? %b, ", ftestIsoAxi.isNullHypothesis(0.05)));
			  str.append(String.format("P = %.2e, F = %.2f\n", ftestIsoAxi.getProbability(), ftestIsoAxi.fValue()));
		  }
		  
		  //AIC
		  str.append(String.format("Akaike Information Criterion (AIC): Value = %.3f,\n   =>Axially-Symmetric model better than Isotropic? %b\n", this.axiAic, this.axiAic<this.isoAic));
		  
		  //output the full tensor results
		  double[][] D =  this.axiSolution.getTensor().toArray();
		  double[][] Dstd = this.axiSolution.getTensor().computeTensorStd(this.axiTensorMonteCarlo);

		  str.append("\n==Axially-Symmetric Diffusion Tensor==\n");
		  str.append(String.format("    [% 7.3f % 7.3f % 7.3f]     [% 7.3f % 7.3f % 7.3f]\n", D[0][0],D[0][1],D[0][2],Dstd[0][0],Dstd[0][1],Dstd[0][2]));
		  str.append(String.format("D = [% 7.3f % 7.3f % 7.3f] +/- [% 7.3f % 7.3f % 7.3f] *(10^7) 1/s\n",D[1][0],D[1][1],D[1][2],Dstd[1][0],Dstd[1][1],Dstd[1][2]));
		  str.append(String.format("    [% 7.3f % 7.3f % 7.3f]     [% 7.3f % 7.3f % 7.3f]\n",D[2][0],D[2][1],D[2][2],Dstd[2][0],Dstd[2][1],Dstd[2][2]));

		  str.append("\n==Axially-Symmetric Diffusion Tensor Sorted Eigendecomposition==\n");
		  double[][] V = eig.getV().toArray();
		  double[][] VT = eig.getVT().toArray();
		  str.append(String.format("    [% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f]\n",V[0][0],V[0][1],V[0][2],eig.getEigenvalue(0),0.0,0.0, VT[0][0],VT[0][1],VT[0][2]));
		  str.append(String.format("D = [% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f] *(10^7) 1/s\n",V[1][0],V[1][1],V[1][2],0.0,eig.getEigenvalue(1),0.0, VT[1][0],VT[1][1],VT[1][2]));
		  str.append(String.format("    [% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f]\n",V[2][0],V[2][1],V[2][2],0.0,0.0,eig.getEigenvalue(2), VT[2][0],VT[2][1],VT[2][2]));    
	  }
	  else
	  {
	  	str.append("Warning, not enough vector samples to properly compute axially-symmetric model!\n");
	  }
	  
	  if (this.aniSolution != null)
	  {
		  eig = this.aniTensor.getProperEigenDecomposition();
		  eigList = getProperEigenDecomposition3dList(this.aniTensorMonteCarlo);
		  angles = eig.getRotation().getAnglesZYZ();
		  anglesStd = eig.computeZYZAnglesStd(eigList);
	
		  //figure out the right order of the tensor values
		  double interiaMajorAxisDiff, predictedMajorAxisDiff;
		  double axiMajorAxisDiff;
		  if (Math.abs(eig.getEigenvalue(0)-eig.getEigenvalue(1))<Math.abs(eig.getEigenvalue(1)-eig.getEigenvalue(2)))
		  {
			  axiMajorAxisDiff = BasicMath.angleAbsolute(this.axiTensor.getProperEigenDecomposition().getRotation().getColumn(2), eig.getRotation().getColumn(2));
			  interiaMajorAxisDiff = BasicMath.angleAbsolute(eigIntertiaTensor.getRotation().getColumn(2), eig.getRotation().getColumn(2));
			  predictedMajorAxisDiff = BasicMath.angleAbsolute(eigPredictedTensor.getRotation().getColumn(2), eig.getRotation().getColumn(2));
		  }
		  else
		  {
			  axiMajorAxisDiff = BasicMath.angleAbsolute(this.axiTensor.getProperEigenDecomposition().getRotation().getColumn(0), eig.getRotation().getColumn(0));
			  interiaMajorAxisDiff = BasicMath.angleAbsolute(eigIntertiaTensor.getRotation().getColumn(0), eig.getRotation().getColumn(0));
			  predictedMajorAxisDiff = BasicMath.angleAbsolute(eigIntertiaTensor.getRotation().getColumn(0), eig.getRotation().getColumn(0));
		  }
		  
		  str.append("\n=====FULLY ANISOTROPIC MODEL=====\n");			
		  str.append(String.format("Dx = %.2f +/- %.2f *(10^7) 1/s\n", eig.getEigenvalue(0), eig.computeEigenvalueStd(0, eigList)));
		  str.append(String.format("Dy = %.2f +/- %.2f *(10^7) 1/s\n", eig.getEigenvalue(1), eig.computeEigenvalueStd(1, eigList)));
		  str.append(String.format("Dz = %.2f +/- %.2f *(10^7) 1/s\n", eig.getEigenvalue(2), eig.computeEigenvalueStd(2, eigList)));
		  str.append(String.format("alpha = %.0f +/- %.0f  deg\n", angles[0]*180.0/BasicMath.PI, anglesStd[0]*180.0/BasicMath.PI));
		  str.append(String.format("beta = %.0f +/- %.0f  deg\n", angles[1]*180.0/BasicMath.PI, anglesStd[1]*180.0/BasicMath.PI));
		  str.append(String.format("gamma = %.0f +/- %.0f deg\n", angles[2]*180.0/BasicMath.PI, anglesStd[2]*180.0/BasicMath.PI));
		  str.append(String.format("Orientational Uncertainty = %.0f deg\n", eig.computeOrientationalStd(eigList)*180.0/BasicMath.PI));
		  str.append(String.format("TAUc = %.2f +/- %.2f ns\n", this.aniTensor.getTauC(), this.aniTensor.computeTauCStd(this.aniTensorMonteCarlo)));
		  str.append(String.format("anisotropy = %.2f +/- %.2f\n", this.aniTensor.anisotropy(), this.aniTensor.computeAnisotropyStd(this.aniTensorMonteCarlo)));
		  str.append(String.format("rhombicity = %.2f +/- %.2f\n",this.aniTensor.rhombicity(), this.aniTensor.computeRhombicityStd(this.aniTensorMonteCarlo)));
		  str.append(String.format("Q = %.3f  R = %.3f  chi^2 = %.3f  chi^2/df = %.3f (Rigid Only)\n", aniSolutionRigid.rhoQualityFactor(), 
		  		aniSolutionRigid.rhoPearsonsCorrelation(),
		  		aniSolutionRigid.rhoChi2Effective(), 
		  		aniSolutionRigid.rhoChi2Effective()/(aniSolutionRigid.sizeRho()-6.0)));
		  
		  str.append(String.format("Axially-Symmetric Tensor Primary Axis Difference = %.0f deg\n", axiMajorAxisDiff*180.0/BasicMath.PI));
		  str.append(String.format("Inertia Tensor Primary Axis Difference = %.0f deg\n", interiaMajorAxisDiff*180.0/BasicMath.PI));
		  str.append(String.format("Inertia Tensor Orientational Difference = %.0f deg\n", eig.orientationalDistance(eigIntertiaTensor)*180.0/BasicMath.PI));
		  str.append(String.format("ELM Predicted Tensor Primary Axis Difference = %.0f deg\n", predictedMajorAxisDiff*180.0/BasicMath.PI));
		  str.append(String.format("ELM Predicted Tensor Orientational Difference = %.0f deg\n", eig.orientationalDistance(eigPredictedTensor)*180.0/BasicMath.PI));

		  if (aniSolutionRigid.sizeRho()>6)
		  {
			  Ftest ftestIsoAni = new Ftest(isoSolutionRigid.sizeRho()-1, isoSolutionRigid.rhoChi2Effective(), aniSolutionRigid.sizeRho()-6, aniSolutionRigid.rhoChi2Effective());
			  Ftest ftestAxiAni = new Ftest(axiSolutionRigid.sizeRho()-4, axiSolutionRigid.rhoChi2Effective(), aniSolutionRigid.sizeRho()-6, aniSolutionRigid.rhoChi2Effective());
			  str.append(String.format("F-test (0.05): Anisotropic model better than Isotropic? %b, ", ftestIsoAni.isNullHypothesis(0.05)));
			  str.append(String.format("P = %.2e, F = %.2f\n", ftestIsoAni.getProbability(), ftestIsoAni.fValue()));
			  str.append(String.format("F-test (0.05): Anisotropic model better than Axially-Symmetric? %b, ", ftestAxiAni.isNullHypothesis(0.05)));
			  str.append(String.format("P = %.2e, F = %.2f\n", ftestAxiAni.getProbability(), ftestAxiAni.fValue()));
		  }

		  //AIC
		  str.append(String.format("Akaike Information Criterion (AIC): Value = %.3f,\n   =>Anisotropic model better than Isotropic? %b\n", this.aniAic, this.aniAic<this.isoAic));
		  str.append(String.format("   =>Anisotropic model better than Axially-Symmteric? %b\n", this.aniAic<this.axiAic));

		  //output the full tensor results
		  double[][] D =  this.aniSolution.getTensor().toArray();
		  double[][] Dstd = this.aniSolution.getTensor().computeTensorStd(this.aniTensorMonteCarlo);

		  str.append("\n==Anistropic Diffusion Tensor==\n");
		  str.append(String.format("    [% 7.3f % 7.3f % 7.3f]     [% 7.3f % 7.3f % 7.3f]\n", D[0][0],D[0][1],D[0][2],Dstd[0][0],Dstd[0][1],Dstd[0][2]));
		  str.append(String.format("D = [% 7.3f % 7.3f % 7.3f] +/- [% 7.3f % 7.3f % 7.3f] *(10^7) 1/s\n",D[1][0],D[1][1],D[1][2],Dstd[1][0],Dstd[1][1],Dstd[1][2]));
		  str.append(String.format("    [% 7.3f % 7.3f % 7.3f]     [% 7.3f % 7.3f % 7.3f]\n",D[2][0],D[2][1],D[2][2],Dstd[2][0],Dstd[2][1],Dstd[2][2]));

		  str.append("\n==Anistropic Diffusion Tensor Sorted Eigendecomposition==\n");
		  double[][] V = eig.getV().toArray();
		  double[][] VT = eig.getVT().toArray();
		  str.append(String.format("    [% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f]\n",V[0][0],V[0][1],V[0][2],eig.getEigenvalue(0),0.0,0.0, VT[0][0],VT[0][1],VT[0][2]));
		  str.append(String.format("D = [% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f] *(10^7) 1/s\n",V[1][0],V[1][1],V[1][2],0.0,eig.getEigenvalue(1),0.0, VT[1][0],VT[1][1],VT[1][2]));
		  str.append(String.format("    [% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f][% 8.3f  % 8.3f  % 8.3f]\n",V[2][0],V[2][1],V[2][2],0.0,0.0,eig.getEigenvalue(2), VT[2][0],VT[2][1],VT[2][2]));    
	  }
	  else
	  {
	  	str.append("Warning, not enough vector samples to properly compute fully anistropic model!\n");
	  }
	  	
	  //display warning messages
   	if (this.fullStatistics)
  	{
  	 	int index = 0;
    	boolean hasBadBond = true;
		  for (ModelFreePredictor predictor : this.bestSolution.createRigid().getPredictors())
		  {
		  	//if the bond data can be trusted, since CSA value is reasonble
		  	if (predictor.getCSA()>=this.bestSolution.getRelaxationData().getBondRelaxation(index).getModelFreeConstraints().minCSA+0.1 &&
		  			predictor.getCSA()<=this.bestSolution.getRelaxationData().getBondRelaxation(index).getModelFreeConstraints().maxCSA-0.1
		  			)
		  	{
			  	double rexStd = rexError(index);
			  	double s2Std = stdS2(index);
			  	
			  	//if within the threshold of a bad value
			  	if (rexWithinPercent(predictor.getRex(), rexStd, this.bestSolution.getRelaxationData().getBondRelaxation(index)) ||
			  			s2BelowRigid(this.bestSolution.getPredictor(index).getS2(),s2Std))
			  	{
			  		if (hasBadBond)
			  		{
			  		  str.append(String.format("\n====Potential Rigid Bonds with High Rex (>%.0f%% of R2) or Low S2 (<%.2f) ====\n", REX_PERCENT_CUTOFF*100.0, S2_FLIXIBLE_CUTOFF));
			  			str.append("<residue><chain><atom1><atom2><CSA><Radius><Rex><S2>\n");
			  			hasBadBond = false;
			  		}
			  		
			  		str.append(String.format("%4d %s %s %s %.2f %.2f %.2f %.2f\n", 
			  				predictor.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
			  				predictor.getBond().getFromAtom().getPrimaryInfo().getChainID(),
			  				predictor.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
			  				predictor.getBond().getToAtom().getPrimaryInfo().getAtomName(),
			  				predictor.getCSA(),
			  				predictor.getRadius(),
			  				predictor.getRex(),
			  				predictor.getS2()));
			  	}
		  	}
				
				index++;
		  }
  	}
	    
	  str.append("\n====Full Rotdif Results: Isotropic Solution====\n");
	  str.append(this.isoSolution.toRotdifResults());
	  	  
	  if (this.axiSolution!=null)
	  {
		  str.append("\n\n====Full Rotdif Results: Axially-Symmetric Solution====\n");
		  str.append(this.axiSolution.toRotdifResults());
	  }

	  if (this.aniSolution!=null)
	  {
		  str.append("\n\n====Full Rotdif Results: Anisotropic Solution====\n");
		  str.append(this.aniSolution.toRotdifResults());
	  }

	  if (this.isoSolutionDynamics!=null)
	  {
		  str.append("\n====Full Dynamics Results: Isotropic Solution====\n");
		  str.append(this.isoSolutionDynamics.toDynamicsResults(this.isoMonteCarlo));
	  }
	  	  
	  if (this.axiSolutionDynamics!=null)
	  {
		  str.append("\n\n====Full Dynamics Results: Axially-Symmetric Solution====\n");
		  str.append(this.axiSolutionDynamics.toDynamicsResults(this.axiMonteCarlo));
	  }

	  if (this.aniSolutionDynamics!=null)
	  {
		  str.append("\n\n====Full Dynamics Results: Anisotropic Solution====\n");
		  str.append(this.aniSolutionDynamics.toDynamicsResults(this.aniMonteCarlo));
	  }

	  return str.toString();
	}	
	
	public final AnisotropicRelaxationComputor getAniComputor()
	{
		return this.aniComputor;
	}
	
	public final RelaxationSolution<AnisotropicModelFreePredictor> getAniSolution()
	{
		return this.aniSolution;
	}
	
	public final RelaxationSolution<AnisotropicModelFreePredictor> getAniSolutionDynamics()
	{
		return this.aniSolutionDynamics;
	}
	
	/**
	 * Gets the ani tensor.
	 * 
	 * @return the ani tensor
	 */
	public RotationalDiffusionTensor getAniTensor()
	{
		return this.aniTensor;
	}
	
	/**
	 * Gets the ani tensor monte carlo.
	 * 
	 * @return the ani tensor monte carlo
	 */
	public final List<RotationalDiffusionTensor> getAniTensorMonteCarlo()
	{
		return this.aniTensorMonteCarlo;
	}
	
	public final AxiallySymmetricRelaxationComputor getAxiComputor()
	{
		return this.axiComputor;
	}
	
	public final RelaxationSolution<AnisotropicModelFreePredictor> getAxiSolution()
	{
		return this.axiSolution;
	}
		
	public final RelaxationSolution<AnisotropicModelFreePredictor> getAxiSolutionDynamics()
	{
		return this.axiSolutionDynamics;
	}

	/**
	 * Gets the axi tensor.
	 * 
	 * @return the axi tensor
	 */
	public RotationalDiffusionTensor getAxiTensor()
	{
		return this.axiTensor;
	}

	/**
	 * Gets the axi tensor monte carlo.
	 * 
	 * @return the axi tensor monte carlo
	 */
	public final List<RotationalDiffusionTensor> getAxiTensorMonteCarlo()
	{
		return this.axiTensorMonteCarlo;
	}
	
	public RelaxationSolution<? extends ModelFreePredictor> getBestDynamicsSolution()
	{
		return this.bestDynamicsSolution;
	}

	public RelaxationSolution<? extends ModelFreePredictor> getBestSolution()
	{
		return this.bestSolution;
	}
	
	public String getBestSolutionType()
	{
		return this.bestSolutionType;
	}
	
	public final IsotropicRelaxationComputor getIsoComputor()
	{
		return this.isoComputor;
	}

	public final RelaxationSolution<IsotropicModelFreePredictor> getIsoSolution()
	{
		return this.isoSolution;
	}
	
	public final RelaxationSolution<IsotropicModelFreePredictor> getIsoSolutionDynamics()
	{
		return this.isoSolutionDynamics;
	}

	/**
	 * Gets the iso tensor.
	 * 
	 * @return the iso tensor
	 */
	public RotationalDiffusionTensor getIsoTensor()
	{
		return this.isoTensor;
	}
	
	/**
	 * Gets the iso tensor monte carlo.
	 * 
	 * @return the iso tensor monte carlo
	 */
	public List<RotationalDiffusionTensor> getIsoTensorMonteCarlo()
	{
		return this.isoTensorMonteCarlo;
	}

	/**
	 * Gets the list.
	 * 
	 * @return the list
	 */
	public ExperimentalRelaxationData getList()
	{
		return this.list;
	}

	public RotationalDiffusionTensor getUnscaledPredictedTensor()
	{
		return this.predictedTensor;
	}

	/**
	 * Notify progress observers.
	 */
	public synchronized void notifyProgressObservers()
	{
		double percent = 0.05+0.90*((double)numberOfCurrentSamples()+1.0)/((double)numberOfRequiredSamples()+1.0);
		
		if (this.computationComplete)
			percent += 0.05;
		
		for (ProgressObserver o : this.observers)
			o.update(percent);
	}

	/**
	 * Number of current samples.
	 * 
	 * @return the int
	 */
	public synchronized int numberOfCurrentSamples()
	{
		return this.isoTensorMonteCarlo.size();
	}

	/**
	 * Number of required samples.
	 * 
	 * @return the int
	 */
	public int numberOfRequiredSamples()
	{
		return this.numberOfRequiredSamples;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ProgressObservable#removeProgressObserver(edu.umd.umiacs.armor.util.ProgressObserver)
	 */
	@Override
	public void removeProgressObserver(ProgressObserver o)
	{
		this.observers.remove(o);
	}

	/**
	 * Rex error.
	 * 
	 * @param index
	 *          the index
	 * @return the double
	 */
	private double rexError(int index)
	{
		double[] val = new double[this.aniMonteCarlo.size()];
		
		int counter = 0;
		for (RelaxationSolution<AnisotropicModelFreePredictor> computor : this.aniMonteCarlo)
		{
			val[counter] = computor.getPredictor(index).getRex();
			
			counter++;
		}
		
		return BasicStat.std(val);
	}

	/**
	 * Rex within percent.
	 * 
	 * @param value
	 *          the value
	 * @param std
	 *          the std
	 * @param bondRelax
	 *          the bond relax
	 * @return true, if successful
	 */
	private boolean rexWithinPercent(double value, double std, BondRelaxation bondRelax)
	{
		for (RelaxationDatum datum : bondRelax.getRelaxationList())
		{
			double convertedValue = ModelFreePredictorImpl.rexAbsolute(value, datum.getFrequency());
			double convertedStd = ModelFreePredictorImpl.rexAbsolute(std, datum.getFrequency());
			
			if (datum.getR2().value()*REX_PERCENT_CUTOFF<(convertedValue-3.0*convertedStd))
				return true;
		}
		
		return false;
	}
	
	/**
	 * S2 below rigid.
	 * 
	 * @param value
	 *          the value
	 * @param std
	 *          the std
	 * @return true, if successful
	 */
	private boolean s2BelowRigid(double value, double std)
	{
		return (value+3.0*std)<S2_FLIXIBLE_CUTOFF;
	}

	/**
	 * Std s2.
	 * 
	 * @param index
	 *          the index
	 * @return the double
	 */
	private double stdS2(int index)
	{
		double[] val = new double[this.aniMonteCarlo.size()];
		
		int counter = 0;
		for (RelaxationSolution<AnisotropicModelFreePredictor> computor : this.aniMonteCarlo)
		{
			val[counter] = computor.getPredictor(index).getS2();
			
			counter++;
		}
		
		return BasicStat.std(val);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return generateOutputString();
	}

	public String generatePredictionString(Molecule molecule, RotationalDiffusionTensor tensor) throws AtomNotFoundException
	{
		StringBuilder str = new StringBuilder();
		
		
	  str.append("\n========ELM Tensor Prediction=========\n");
		str.append(tensor.toLongString());
		
		if (this.aniComputor!=null)
		{
			double[] predictedRho = this.aniComputor.predictRho(molecule, tensor);
	
			str.append("\n========ELM Detailed Results=========\n");
			str.append("<residue><chain><atom1><atom2><freq><rho_exp><rho_pred><rho_rel_error>\n");
			
			int index = 0;
		  for (BondRelaxation bondRelax : this.aniComputor.getRelaxationData().getBondRelaxationData())
		  {
		  	for (RelaxationDatum datum : bondRelax.getRelaxationListRef())
		  	{
		  		str.append(String.format("% 4d %s %-5s %-5s %7.2f %7.3f %7.3f %7.3f\n", bondRelax.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
		  																																					bondRelax.getBond().getFromAtom().getPrimaryInfo().getChainID(),
		  																																					bondRelax.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
		  																																					bondRelax.getBond().getToAtom().getPrimaryInfo().getAtomName(),
		  																																					datum.getFrequency(),
		  																																					datum.getRho().value(),
		  																																					predictedRho[index],
		  																																					(predictedRho[index]-datum.getRho().value())/datum.getRho().error()));
		  		
		  		index++;
		  	}
		  }
		}
		

  	return str.toString();
	}
}