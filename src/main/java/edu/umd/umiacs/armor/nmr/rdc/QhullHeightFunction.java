/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.ConvexHull3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.Molecule;

/**
 * The Class QhullHeightFunction.
 */
public final class QhullHeightFunction implements MoleculeHeightFunction, Serializable
{
	
	private final double[][] hullVertices;

	private final double radii[];
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2152295605054584691L;
	
	private QhullHeightFunction(double[][] hullVerticies, double[] radii, boolean computeHull, boolean centerMolecule)
	{
		if (!computeHull)
		{
			this.hullVertices = hullVerticies;
			this.radii = radii;
			
			return;
		}
		
		ConvexHull3d hull = new ConvexHull3d();		
		
		//compute indicies
		int[] indicies = hull.getHullVertexIndices(hullVerticies);

		this.hullVertices = new double[indicies.length][3];
		this.radii = new double[indicies.length];
		for (int iter=0; iter<indicies.length; iter++)
		{
			double[] val = hullVerticies[indicies[iter]];
			
			this.hullVertices[iter][0] = val[0];
			this.hullVertices[iter][1] = val[1];
			this.hullVertices[iter][2] = val[2];
			this.radii[iter] = radii[indicies[iter]];
		}
		
		if (centerMolecule)
		{
			double[] center = BasicStat.meanColumns(this.hullVertices);
			
			//adjust so it is centered in the middle
			for (int iter=0; iter<this.hullVertices.length; iter++)
			{
				this.hullVertices[iter][0] -= center[0];
				this.hullVertices[iter][1] -= center[1];
				this.hullVertices[iter][2] -= center[2];
			}
		}
	}
	
	
	/**
	 * Instantiates a new qhull height function.
	 * 
	 * @param mol
	 *          the mol
	 */
	public QhullHeightFunction(Molecule mol)
	{
		this(mol, true);
	}
	
	private QhullHeightFunction(Molecule mol, boolean centerMolecule)
	{
		ConvexHull3d hull = new ConvexHull3d();		
		
		double[][] positions = mol.getAtomPositionArray();
		
		//compute indicies
		int[] indicies = hull.getHullVertexIndices(positions);

		this.hullVertices = new double[indicies.length][];
		this.radii = new double[indicies.length];
		for (int iter=0; iter<indicies.length; iter++)
		{
			this.hullVertices[iter] = positions[indicies[iter]];
			this.radii[iter] = mol.getAtomSurfaceRadius(indicies[iter]);
		}
		
		if (centerMolecule)
		{
			double[] center = BasicStat.meanColumns(this.hullVertices);
			
			//adjust so it is centered in the middle
			for (int iter=0; iter<this.hullVertices.length; iter++)
			{
				this.hullVertices[iter][0] -= center[0];
				this.hullVertices[iter][1] -= center[1];
				this.hullVertices[iter][2] -= center[2];
			}
		}
	}
	
	public QhullHeightFunction createTransformed(RigidTransform t)
	{
		double[][] transformedVerticies = new double[this.hullVertices.length][];

		for (int iter=0; iter<this.hullVertices.length; iter++)
			transformedVerticies[iter] = t.transform(this.hullVertices[iter][0], this.hullVertices[iter][1], this.hullVertices[iter][2]).toArray();
		
		return new QhullHeightFunction(transformedVerticies, this.radii, false, false);
	}
		
	/**
	 * Height.
	 * 
	 * @param R31
	 *          the r31
	 * @param R32
	 *          the r32
	 * @param R33
	 *          the r33
	 * @return the double
	 */
	public double height(double R31, double R32, double R33)
	{
    double Rx1 = R31;
    double Rx2 = R32;
    double Rx3 = R33;
    
    double maxVal = Double.NEGATIVE_INFINITY;
		for (int iter=0; iter<this.hullVertices.length; iter++)
		{
	    double currVal = Rx1*this.hullVertices[iter][0]+Rx2*this.hullVertices[iter][1]+Rx3*this.hullVertices[iter][2];
	    
	    //adjust for the surface
	    currVal -= this.radii[iter];
	    
	    maxVal = Math.max(-currVal,maxVal);
		}
		
		if (maxVal<0.0)				
			throw new RdcRuntimeException("Molecule height from the center cannot be negative ("+maxVal+").");
		
		return maxVal;
	}

	public QhullHeightFunction merge(QhullHeightFunction f2, boolean centerMolecule)
	{
		double[][] mergedVerticies = new double[this.hullVertices.length+f2.hullVertices.length][];
		double[] mergedRadii = new double[this.hullVertices.length+f2.hullVertices.length];
		
		for (int iter=0; iter<this.hullVertices.length; iter++)
		{
			mergedVerticies[iter] = this.hullVertices[iter];
			mergedRadii[iter] = this.radii[iter];
		}
		for (int iter=0; iter<f2.hullVertices.length; iter++)
		{
			mergedVerticies[this.hullVertices.length+iter] = f2.hullVertices[iter];
			mergedRadii[this.hullVertices.length+iter] = f2.radii[iter];
		}
		
		return new QhullHeightFunction(mergedVerticies, mergedRadii, true, centerMolecule);
	}

	
	/*
	public double value(double alpha, double u)
	{
    double sb = BasicMath.sqrt(1-u*u);
    double cb = u;
    double sa = BasicMath.sin(alpha);
    double ca = BasicMath.cos(alpha);

    double Rx1 = -sb*ca;
    double Rx2 = sb*sa;
    double Rx3 = cb;
    
    double maxVal = Double.NEGATIVE_INFINITY;
		for (int iter=0; iter<hullVertices.length; iter++)
		{
	    double currVal = Rx1*hullVertices[iter][0]+Rx2*hullVertices[iter][1]+Rx3*hullVertices[iter][2]-AtomType.HYDROGEN.vanDerWaalsRadius;
	    maxVal = Math.max(-currVal,maxVal);
		}
		
		if (maxVal<0.0)				
			throw new RdcRuntimeException("Molecule height from the center cannot be negative ("+maxVal+").");
		
		return maxVal;
	}
	*/
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.func.TwoVariableFunction#value(double, double)
	 */
	@Override
	public double value(double alpha, double u)
	{
		//Rotation R = PatiPredictor.getPatiRotation(alpha, u);
    
	  double sb = BasicMath.sqrt(1-u*u);
	  double cb = u;
	  double sa = BasicMath.sin(alpha);
	  double ca = BasicMath.cos(alpha);

	  double Rx1 = -sb*ca;
    double Rx2 = sb*sa;
    double Rx3 = cb;

    return height(Rx1, Rx2, Rx3);
	}
}