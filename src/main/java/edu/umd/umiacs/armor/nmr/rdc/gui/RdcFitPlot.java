/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.nmr.rdc.RdcDatum;
import edu.umd.umiacs.armor.nmr.rdc.RdcSolution;
import edu.umd.umiacs.armor.util.plot.Limit;
import edu.umd.umiacs.armor.util.plot.Line;
import edu.umd.umiacs.armor.util.plot.Plot2d;

import java.awt.GridLayout;

/**
 * The Class RdcFitPlot.
 */
public class RdcFitPlot extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5377069641898712954L;

	/**
	 * Instantiates a new rdc fit plot.
	 * 
	 * @param computor
	 *          the computor
	 */
	public RdcFitPlot(RdcSolution computor)
	{
		getContentPane().setBackground(java.awt.Color.WHITE);

		setTitle("Fit Plot");
		setBounds(100, 100, 500, 600);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		// create the double layout
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

		// plot only the rigid information
		computor = computor.createRigid();
		
		// prediction scatter plot
		double[] residuals = computor.residuals();

		int counter = 0;
		ArrayList<String> toolTips = new ArrayList<String>();
		for (RdcDatum coupling : computor.getExperimentalData())
		{
			toolTips.add(String.format(
					"<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Residual: %.3f</html>", coupling
							.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(), coupling.getBond().getFromAtom().getPrimaryInfo().getChainID(),
					coupling.getBond().getFromAtom().getPrimaryInfo().getAtomName(), coupling.getBond().getToAtom().getPrimaryInfo().getAtomName(),
					residuals[counter]));

				counter++;
		}
		
		double[] values = computor.values();
		double[] prediction = computor.predict();
		
		Line scatterLine = new Line(values, prediction, "Inliers", toolTips);
		scatterLine.setDrawLine(false);
		scatterLine.setDrawShape(true);
		scatterLine.setPaint(Color.BLUE);
		scatterLine.setShape(new Ellipse2D.Double(-2.5, -2.5, 5, 5));

		double minValue = Math.min(BasicMath.min(prediction), BasicMath.min(values));
		double maxValue = Math.max(BasicMath.max(prediction), BasicMath.max(values));
		double diff = maxValue-minValue;
		Limit limit = new Limit(minValue - diff * .1, maxValue + diff * .1);

		Line regressionLine = new Line(new double[] {limit.getMin(), limit.getMax()}, new double[] {limit.getMin(), limit.getMax()}, "y=x");
		regressionLine.setDrawLine(true);
		regressionLine.setDrawShape(false);
		regressionLine.setPaint(Color.BLACK);

		Plot2d plot = new Plot2d();
		plot.addLine(regressionLine);
		plot.addLine(scatterLine);
		plot.setXLabel("RDC Exp.");
		plot.setYLabel("RDC Comp.");
		plot.setTitle(
				String.format("%s (Q=%.3f, R=%.3f)", "Tensor Fit", computor.qualityFactor(), computor.pearsonsCorrelation()),
				new Font("Ariel", Font.PLAIN, 14));

		// set the axes limits
		plot.setXAxisLimit(limit);
		plot.setYAxisLimit(limit);

		// add the plot
		getContentPane().add(plot);

		// generate a bar plot for each chain
		ArrayList<RdcSolution> computorListOfChains = computor.seperateByChain();
		for (RdcSolution chainComputor : computorListOfChains)
		{
			Plot2d barPlot = new Plot2d();
			barPlot.setRenderingOrderReverse();
			
			barPlot.setTitle(
					String.format("Residuals for Chain %s", chainComputor.getPredictor(0).getBond()
							.getFromAtom().getPrimaryInfo().getChainID()), new Font("Ariel", Font.PLAIN, 12));
			barPlot.setIntegerTicks(true);
			barPlot.setXLabel("Residue/Nucleotide #");
			barPlot.setYLabel("Residuals");
			getContentPane().add(barPlot);
			// barPlot.removeLegend();

			ArrayList<RdcSolution> computorListOfBonds = chainComputor.seperateByBondType();
			for (RdcSolution bondComputor : computorListOfBonds)
			{
				// for each relaxation list
				double[] residues = new double[bondComputor.size()];
				toolTips = new ArrayList<String>();
				
				counter = 0;
				residuals = bondComputor.residuals();
				for (RdcDatum coupling : bondComputor.getExperimentalData())
				{
					residues[counter] = coupling.getBond().getFromAtom().getPrimaryInfo().getResidueNumber();
					toolTips.add(String.format(
							"<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Residual: %.3f</html>", coupling
									.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(), coupling.getBond().getFromAtom().getPrimaryInfo().getChainID(),
									coupling.getBond().getFromAtom().getPrimaryInfo().getAtomName(), coupling.getBond().getToAtom().getPrimaryInfo().getAtomName(),
									residuals[counter]));
	
					counter++;
				}
	
				// label of the current dataset
				String label = String.format("%s, %s-%s", bondComputor.getPredictor(0).getBond().getFromAtom().getPrimaryInfo().getChainID(), 
						bondComputor.getPredictor(0).getBond().getBondType().getFromAtomName(), 
						bondComputor.getPredictor(0).getBond().getBondType().getToAtomName());
				
				Line residualLine = new Line(residues, bondComputor.residuals(), label, toolTips);
				// residualLine.setPaint(new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F,
				// 0.0F, new Color(0, 0, 64)));
	
				// add to the bar list
				barPlot.addBar(residualLine);
			}
		}

		setVisible(true);
	}
}
