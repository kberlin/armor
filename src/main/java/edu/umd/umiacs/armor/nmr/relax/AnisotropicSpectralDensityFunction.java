/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Class AnisotropicSpectralDensityFunction.
 */
public final class AnisotropicSpectralDensityFunction implements MFSpectralDensityFunction<AnisotropicSpectralDensityFunction>
{
	
	private final double A1, A2, A3, A4, A5;

	private final Point3d bondVector;
	
	private final double D1, D2, D3, D4, D5;
	
	private final double S2Fast;
	
	private final double S2Slow;
	
	private final double tauLocal;
	
	private final double tauLocalFast;

	private final RotationalDiffusionTensor tensor;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5432049802770759542L;
	
	/**
	 * Creates the.
	 * 
	 * @param tensor
	 *          the tensor
	 * @param bondVector
	 *          the bond vector
	 * @param S2
	 *          the s2
	 * @param tauLocal
	 *          the tau local
	 * @return the anisotropic spectral density function
	 */
	public static AnisotropicSpectralDensityFunction create(RotationalDiffusionTensor tensor, Point3d bondVector, double S2, double tauLocal, double S2Fast, double tauLocalFast)
	{
		if (S2<0.0 || S2>1.0 || Double.isNaN(S2))
			throw new RelaxationRuntimeException("Invalid value for S2: "+S2);
		if (tauLocal<0.0 || Double.isNaN(tauLocal))
			throw new RelaxationRuntimeException("Invalid value for tauLocal: "+tauLocal);
		if (S2Fast<0.0 || S2Fast>1.0 || Double.isNaN(S2Fast))
			throw new RelaxationRuntimeException("Invalid value for S2: "+S2Fast);
		if (tauLocalFast<0.0 || Double.isNaN(tauLocalFast))
			throw new RelaxationRuntimeException("Invalid value for tauLocal: "+tauLocalFast);
		
		AnisotropicSpectralDensityFunction J = new AnisotropicSpectralDensityFunction(tensor, bondVector, S2, tauLocal, S2Fast, tauLocalFast);
		
		return J;
	}
	
	
	private AnisotropicSpectralDensityFunction(AnisotropicSpectralDensityFunction oldFunc, double S2, double tauLocal, double S2Fast, double tauLocalFast)
	{
		this.bondVector = oldFunc.bondVector;
		this.S2Slow = S2;
		this.tauLocal = tauLocal*1.0e-9;
		this.S2Fast = S2Fast;
		this.tauLocalFast = tauLocalFast*1.0e-9;
		this.tensor = oldFunc.tensor;
		
		this.A1 = oldFunc.A1;
		this.A2 = oldFunc.A2;
		this.A3 = oldFunc.A3;
		this.A4 = oldFunc.A4;
		this.A5 = oldFunc.A5;
		this.D1 = oldFunc.D1;
		this.D2 = oldFunc.D2;
		this.D3 = oldFunc.D3;
		this.D4 = oldFunc.D4;
		this.D5 = oldFunc.D5;
	}
	
	private AnisotropicSpectralDensityFunction(RotationalDiffusionTensor tensor, Point3d bondVector, double S2, double tauLocal, double S2Fast, double tauLocalFast)
	{
		this.bondVector = bondVector;
		this.S2Slow = S2;
		this.tauLocal = tauLocal*1.0e-9;
		this.S2Fast = S2Fast;
		this.tauLocalFast = tauLocalFast*1.0e-9;
		this.tensor = tensor;
		
		//get the nessary values
		double[] dValues = tensor.getDValues();
 	  
		//inverse rotation
	  Point3d v = tensor.getProperEigenDecomposition().getRotation().multInv(bondVector);
	  Point3d vs = v.squared();
	  Point3d vq = vs.squared();
	  
	  this.D1 = dValues[0];
	  this.D2 = dValues[1];
	  this.D3 = dValues[2];
	  this.D4 = dValues[3];
	  this.D5 = dValues[4];
	  
    double delta1 = dValues[5];
    double delta2 = dValues[6];
    double delta3 = dValues[7];
	  
	  this.A1 = 3.0*vs.y*vs.z;
	  this.A2 = 3.0*vs.x*vs.z;
	  this.A3 = 3.0*vs.x*vs.y;
	  
	  double part1 = 1.0/4.0*(3.0*(vq.x+vq.y+vq.z)-1.0);
	  double part2 = 1.0/12.0*(
	  							delta1*(3.0*vq.x+2.0*this.A1-1.0)+
	  							delta2*(3.0*vq.y+2.0*this.A2-1.0)+
	                delta3*(3.0*vq.z+2.0*this.A3-1.0));
	  
	  this.A4 = part1-part2;
	  this.A5 = part1+part2;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewLocal(double, double)
	 */
	@Override
	public AnisotropicSpectralDensityFunction createWithLocal(double S2, double tauLocal)
	{
		return new AnisotropicSpectralDensityFunction(this, S2, tauLocal, this.S2Fast, getTauFast());
	}
	
	@Override
	public AnisotropicSpectralDensityFunction createWithLocal(double S2, double tauLocal, double S2Fast, double tauFast)
	{
		return new AnisotropicSpectralDensityFunction(this, S2, tauLocal, S2Fast, tauFast);
	}
		
	@Override
	public AnisotropicSpectralDensityFunction createWithS2Fast(double S2Fast)
	{
		return new AnisotropicSpectralDensityFunction(this, this.S2Slow, getTauLocal(), S2Fast, getTauFast());
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewS2(double)
	 */
	@Override
	public AnisotropicSpectralDensityFunction createWithS2Slow(double S2)
	{
		return new AnisotropicSpectralDensityFunction(this, S2, getTauLocal(), this.S2Fast, getTauFast());
	}
	
	@Override
	public AnisotropicSpectralDensityFunction createWithTauFast(double tauLocalFast)
	{
		return new AnisotropicSpectralDensityFunction(this, this.S2Slow, this.tauLocal, this.S2Fast, tauLocalFast);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewTauLocal(double)
	 */
	@Override
	public AnisotropicSpectralDensityFunction createWithTauLocal(double tauLocal)
	{
		return new AnisotropicSpectralDensityFunction(this, this.S2Slow, tauLocal, this.S2Fast, getTauFast());
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewRotationalDiffusionTensor(edu.umd.umiacs.armor.nmr.relax.RotationalDiffusionTensor)
	 */
	@Override
	public AnisotropicSpectralDensityFunction createWithTensor(RotationalDiffusionTensor tensor)
	{
		AnisotropicSpectralDensityFunction func = new AnisotropicSpectralDensityFunction(tensor, this.bondVector, this.S2Slow, this.getTauLocal(), this.S2Fast, getTauFast());
		
		return func;
	}


	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#createWithNewVector(edu.umd.umiacs.armor.math.Point3d)
	 */
	@Override
	public AnisotropicSpectralDensityFunction createWithVector(Point3d bondVector)
	{
		bondVector = bondVector.normalized();
		if (this.bondVector.distanceSquared(bondVector)<1.0e-12)
			return this;
		
		AnisotropicSpectralDensityFunction func = new AnisotropicSpectralDensityFunction(this.tensor, bondVector, this.S2Slow, this.getTauLocal(), this.S2Fast, getTauFast());
		
		return func;
	}

	public Point3d getNormalizedBondVector()
	{
		return this.bondVector;
	}


	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#getS2()
	 */
	@Override
	public double getS2()
	{
		return this.S2Slow*this.S2Fast;
	}


	@Override
	public double getS2Fast()
	{
		return this.S2Fast;
	}

	@Override
	public double getS2Slow()
	{
		return this.S2Slow;
	}

	@Override
	public double getTauC()
	{
		return value(0.0,1.0,1.0)*1.0e9*2.5;
	}


	@Override
	public double getTauFast()
	{
		return this.tauLocalFast*1.0e9;
	}


	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.MFSpectralDensityFunction#getTauLocal()
	 */
	@Override
	public double getTauLocal()
	{
		return this.tauLocal*1.0e9;
	}


	@Override
	public final RotationalDiffusionTensor getTensor()
	{
		return this.tensor;
	}


	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.SpectralDensityFunction#value(double)
	 */
	@Override
	public final double value(double omega)
	{
		return value(omega, this.S2Slow, this.S2Fast);
	}

	private final double value(double omega, double S2Slow, double S2Fast)
	{	  
	  double omega2 = omega*omega;
	  double S2 = this.S2Slow*this.S2Fast;
	  
    double Df1;
    double Df2;
    double Df3;
    double Df4;
    double Df5;

    if (omega2>0.0)
    {
	    Df1 = this.D1/(this.D1*this.D1+omega2);
	    Df2 = this.D2/(this.D2*this.D2+omega2);
	    Df3 = this.D3/(this.D3*this.D3+omega2);
	    Df4 = this.D4/(this.D4*this.D4+omega2);
	    Df5 = this.D5/(this.D5*this.D5+omega2);
    }
    else
    {
	    Df1 = 1.0/(this.D1);
	    Df2 = 1.0/(this.D2);
	    Df3 = 1.0/(this.D3);
	    Df4 = 1.0/(this.D4);
	    Df5 = 1.0/(this.D5);    	
    }
    
    double Jordered = S2*(this.A1*Df1+this.A2*Df2+this.A3*Df3+this.A4*Df4+this.A5*Df5);
    
    double Jdisordered = 0.0;
    if (S2Fast-S2>0.0)
    {
    	double tauLocalAdj = this.tauLocal;
    	
	    //double tau_e1 = 1.0/(D1+1.0/tauLocalAdj);
	    //double tau_e2 = 1.0/(D2+1.0/tauLocalAdj);
	    //double tau_e3 = 1.0/(D3+1.0/tauLocalAdj);
	    //double tau_e4 = 1.0/(D4+1.0/tauLocalAdj);
	    //double tau_e5 = 1.0/(D5+1.0/tauLocalAdj);
	    double tau_e1 = tauLocalAdj/(this.D1*tauLocalAdj+1.0);
	    double tau_e2 = tauLocalAdj/(this.D2*tauLocalAdj+1.0);
	    double tau_e3 = tauLocalAdj/(this.D3*tauLocalAdj+1.0);
	    double tau_e4 = tauLocalAdj/(this.D4*tauLocalAdj+1.0);
	    double tau_e5 = tauLocalAdj/(this.D5*tauLocalAdj+1.0);
 
      Jdisordered = (S2Fast-S2)*
      	 ((this.A1*tau_e1)/(1.0+omega2*tau_e1*tau_e1)+
          (this.A2*tau_e2)/(1.0+omega2*tau_e2*tau_e2)+
          (this.A3*tau_e3)/(1.0+omega2*tau_e3*tau_e3)+
          (this.A4*tau_e4)/(1.0+omega2*tau_e4*tau_e4)+
          (this.A5*tau_e5)/(1.0+omega2*tau_e5*tau_e5));
    }
    
    double JdisorderedFast = 0.0;
    if (S2Fast<1.0)
    {
    	double tauLocalAdj = this.tauLocalFast;
    	
	    //double tau_e1 = 1.0/(D1+1.0/tauLocalAdj);
	    //double tau_e2 = 1.0/(D2+1.0/tauLocalAdj);
	    //double tau_e3 = 1.0/(D3+1.0/tauLocalAdj);
	    //double tau_e4 = 1.0/(D4+1.0/tauLocalAdj);
	    //double tau_e5 = 1.0/(D5+1.0/tauLocalAdj);
	    double tau_e1 = tauLocalAdj/(this.D1*tauLocalAdj+1.0);
	    double tau_e2 = tauLocalAdj/(this.D2*tauLocalAdj+1.0);
	    double tau_e3 = tauLocalAdj/(this.D3*tauLocalAdj+1.0);
	    double tau_e4 = tauLocalAdj/(this.D4*tauLocalAdj+1.0);
	    double tau_e5 = tauLocalAdj/(this.D5*tauLocalAdj+1.0);
 
	    JdisorderedFast = (1.0-S2Fast)*
      	 ((this.A1*tau_e1)/(1.0+omega2*tau_e1*tau_e1)+
          (this.A2*tau_e2)/(1.0+omega2*tau_e2*tau_e2)+
          (this.A3*tau_e3)/(1.0+omega2*tau_e3*tau_e3)+
          (this.A4*tau_e4)/(1.0+omega2*tau_e4*tau_e4)+
          (this.A5*tau_e5)/(1.0+omega2*tau_e5*tau_e5));
    }

    double J = (2.0/5.0)*(Jordered+Jdisordered+JdisorderedFast);
    
    return J;
	}


	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.SpectralDensityFunction#valueOrdered(double)
	 */
	@Override
	public final double valueOrdered(double omega)
	{
		return value(omega, 1.0, 1.0);
	}
}
