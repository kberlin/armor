/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.util.AbstractSolution;
import edu.umd.umiacs.armor.util.BaseExperimentalData;

public final class PreSolution extends AbstractSolution<ExperimentalPreData,PreDatum> 
{
	final ArrayList<Point3d> atomPositions;
	final Point3d prePosition;
	/**
	 * 
	 */
	private static final long serialVersionUID = 533979818785039367L;

	public PreSolution(Point3d prePosition, List<Point3d> atomPositions, ExperimentalPreData data)
	{
		super(data);
		this.prePosition = prePosition;
		this.atomPositions = new ArrayList<Point3d>(atomPositions);
	}
	
	public PreSolution createRigid()
	{
		ArrayList<Point3d> newPositions = new ArrayList<Point3d>();
		ArrayList<PreDatum> newPres = new ArrayList<PreDatum>();
		
		int counter=0;
		for (PreDatum datum : getExperimentalData())
		{
			if (datum.isRigid())
			{
				newPositions.add(this.atomPositions.get(counter));
				newPres.add(datum);
			}
			
			counter++;
		}
		
		return new PreSolution(this.prePosition, newPositions, new ExperimentalPreData(newPres));
	}

	private PreSolution createSubList(ExperimentalPreData list)
	{
		ArrayList<Integer> indices = getAssociatedIndices(list);
		
		ArrayList<Point3d> atomPositions = new ArrayList<Point3d>(list.size());
		for (int index : indices)
			atomPositions.add(this.atomPositions.get(index));
		
		return new PreSolution(this.prePosition, atomPositions, list);
	}

	private ArrayList<PreSolution> createSubLists(ArrayList<ExperimentalPreData> list)
	{
		ArrayList<PreSolution> solutions = new ArrayList<PreSolution>(list.size());
		for (ExperimentalPreData data : list)
			solutions.add(createSubList(data));
		
		return solutions;
	}
	
	private final ArrayList<Integer> getAssociatedIndices(ExperimentalPreData list)
	{
		ArrayList<Integer> indices = new ArrayList<Integer>(list.size());
		List<Atom> atoms = getExperimentalData().getAtoms();
		for (PreDatum datum : list)
		{			
			int index = atoms.indexOf(datum.getAtom());
			indices.add(index);
		}
		
		return indices;
	}
	
	public Point3d getAtomPosition(int index)
	{
		return this.atomPositions.get(index);
	}

	public Point3d getPrePosition()
	{
		return this.prePosition;
	}
	
	@Override
	public double[] predict()
	{
		double[] pred = new double[size()];
		
		for (int iter=0; iter<pred.length; iter++)
			pred[iter] = getExperimentalData().get(iter).predict(this.prePosition, this.atomPositions.get(iter));
		
		return pred;
	}
	
	public double[] predictDeltaR2()
	{
		double[] pred = new double[size()];
		
		for (int iter=0; iter<pred.length; iter++)
			pred[iter] = getExperimentalData().get(iter).predictDeltaR2(this.prePosition, this.atomPositions.get(iter));
		
		return pred;
	}
	
	public double qualityFactor()
	{
		return BasicStat.rotdifQualityFactor(predict(), getExperimentalData().values());
	}
	
	public double[] residualsDeltaR2()
	{
		BaseExperimentalData r2Data = getExperimentalData().getDeltaR2Data();
		return BasicStat.residuals(predictDeltaR2(), r2Data.values(), r2Data.errors());
	}
	
	public ArrayList<PreSolution> seperateByChain()
	{
		return createSubLists(getExperimentalData().seperateByChain());
	}
	
	public double[] valuesDeltaR2()
	{
		return getExperimentalData().getDeltaR2Data().values();
	}
}
