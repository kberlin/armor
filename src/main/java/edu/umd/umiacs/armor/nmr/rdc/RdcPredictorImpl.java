/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.physics.BasicPhysics;

/**
 * The Class RdcPredictorImpl.
 */
public final class RdcPredictorImpl implements RdcPredictor, Serializable
{
	/** The A. */
	private final AlignmentTensor A;

	/** The bond. */
	private final Bond bond;
	
	/** The g p. */
	private final double gP;
	
	/** The g q. */
	private final double gQ;
	
	/** The order parameter. */
	private final double orderParameter;
	
	/** The Constant CUBE_TWOPI. */
	private static final double CUBE_TWOPI = BasicMath.TWOPI*BasicMath.TWOPI*BasicMath.TWOPI;
	
	/** The Constant DEFAULT_ORDER_PARAMETER. */
	public static final double DEFAULT_ORDER_PARAMETER = 1.0;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2753947109275313209L;
	
	public static double getRDCBondConst(double gP, double gQ, double r, double liparoS_LS)
	{
		r = r*1.0e-10; //in meters
		
    //The RDCs are usually always measured as positive, hence the abs
    double Cpq = -(-1)*liparoS_LS*BasicPhysics.mu0*Math.abs(gP*gQ)*(BasicPhysics.h/(CUBE_TWOPI*BasicMath.cube(r)));
    
    return Cpq;
	}
	
	public RdcPredictorImpl(AlignmentTensor A, Bond bond, double orderParameter)
	{
		this.A = A;
		
		this.bond = bond;
		
		this.gP = bond.getFromAtom().getType().getGyromagneticRatio();
		this.gQ = bond.getToAtom().getType().getGyromagneticRatio();
		this.orderParameter = orderParameter;
	}
	
	public RdcPredictorImpl(AlignmentTensor A, Bond bond)
	{
		this(A, bond, DEFAULT_ORDER_PARAMETER);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.rdc.RdcPredictor#createWithNewOrderParameter(double)
	 */
	@Override
	public RdcPredictorImpl createWithNewOrderParameter(double orderParameter)
	{
		return new RdcPredictorImpl(this.A, this.bond, orderParameter);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.rdc.RdcPredictor#createWithNewTensor(edu.umd.umiacs.armor.nmr.rdc.AlignmentTensor)
	 */
	@Override
	public RdcPredictorImpl createWithNewTensor(AlignmentTensor A)
	{
		return new RdcPredictorImpl(A, this.bond, this.orderParameter);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.rdc.RdcPredictor#createWithNewVector(edu.umd.umiacs.armor.math.Point3d)
	 */
	@Override
	public RdcPredictorImpl createWithNewVector(Point3d p)
	{
		Point3d newP = this.bond.getFromAtom().getPosition().add(p);		
		Bond newBond = new Bond(this.bond.getFromAtom(), this.bond.getToAtom().createWithNewPosition(newP));
		
		return new RdcPredictorImpl(this.A, newBond, this.orderParameter);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.rdc.RdcPredictor#getAlignmentTensor()
	 */
	@Override
	public AlignmentTensor getAlignmentTensor()
	{
		return this.A;
	}

	@Override
	public Bond getBond()
	{
		return this.bond;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.rdc.RdcPredictor#getOrderParameter()
	 */
	@Override
	public double getOrderParameter()
	{
		return this.orderParameter;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.rdc.RdcPredictor#getScalingConstant()
	 */
	@Override
	public double getScalingConstant()
	{
		return getRDCBondConst(this.gP, this.gQ, this.bond.getBondType().getMeanRadius(), this.orderParameter);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.rdc.RdcPredictor#predict()
	 */
	@Override
	public double predict()
	{
		Point3d bondVector = this.bond.getVector();
		double r = bondVector.length();
		
		//do not assume it is normalized
		double x = bondVector.x/r;
		double y = bondVector.y/r;
		double z = bondVector.z/r;
		
	  double value = this.A.get(0,0)*x*x + this.A.get(1,1)*y*y + this.A.get(2,2)*z*z
	  							+2.0*this.A.get(1,0)*x*y + 2.0*this.A.get(2,0)*x*z + 2.0*this.A.get(2,1)*y*z;
	  
	  return value*getScalingConstant();
	}

	@Override
	public double predict(Molecule mol, AlignmentTensor A) throws AtomNotFoundException
	{
		Point3d p1 = mol.getAtomPosition(this.bond.getFromAtom());
		Point3d p2 = mol.getAtomPosition(this.bond.getToAtom());
		
		return new RdcPredictorImpl(A, this.bond.createNewVector(p2.subtract(p1)), this.orderParameter).predict();
	}
}
