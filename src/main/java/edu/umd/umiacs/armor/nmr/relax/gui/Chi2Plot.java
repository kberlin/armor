/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax.gui;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JFrame;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.TwoVariableFunction;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor;
import edu.umd.umiacs.armor.nmr.relax.RelaxationSolution;
import edu.umd.umiacs.armor.nmr.relax.RotationalDiffusionTensor;
import edu.umd.umiacs.armor.util.plot.Limit;
import edu.umd.umiacs.armor.util.plot.Surface;
import edu.umd.umiacs.armor.util.plot.SurfacePlot;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import org.jzy3d.colors.Color;

/**
 * The Class Chi2Plot.
 */
public class Chi2Plot extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -556714678881725208L;

	/**
	 * Instantiates a new chi2 plot.
	 * 
	 * @param computor
	 *          the computor
	 */
	public Chi2Plot(RelaxationSolution<? extends RelaxationRatePredictor> solution) 
	{
		getContentPane().setBackground(java.awt.Color.WHITE);
		
		setTitle("Chi^2 Plot");
		setBounds(100, 100, 400, 400);
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		//don't resize until 3d plot graphs are fixed
		this.setResizable(false);
		
		JLabel lblChiPlot = new JLabel("Chi^2 Plot");
		lblChiPlot.setBackground(java.awt.Color.WHITE);
		lblChiPlot.setHorizontalAlignment(SwingConstants.CENTER);
		//getContentPane().add(lblChiPlot, BorderLayout.NORTH);
		
		//plot only the rigid information
		final RelaxationSolution<? extends RelaxationRatePredictor> rigidSolution = solution.createRigid();
		
		// Define a function to plot
		TwoVariableFunction chi2Func = new TwoVariableFunction()
		{
			@Override
			public double value(double a, double b)
			{
				RotationalDiffusionTensor newTensor = rigidSolution.getTensor().createTensorWithOrientation(Rotation.createZYZ(a/180.0*BasicMath.PI, b/180.0*BasicMath.PI, 0));	
				return rigidSolution.rhoChi2(newTensor);
			}
		};
		
		SurfacePlot plot = new SurfacePlot();

		//create limits
		Limit rangeAlpha = new Limit(0.0, 180.0);
		Limit rangeBeta = new Limit(0.0, 180.0);
		
		Surface surface = new Surface(chi2Func, rangeAlpha, 25, rangeBeta, 25);
		plot.addSurface(surface);

		plot.setXLabel("alpha");
		plot.setYLabel("beta");
		plot.setZLabel("RMSD (Angstrom)");
		plot.addColorBar(surface);
		
		//add the solution
		ArrayList<EigenDecomposition3d> eigList = rigidSolution.getTensor().getProperEigenDecomposition().generateEquivalentEigenDecompositions();
		for (EigenDecomposition3d currEig : eigList)
		{
			double[] angles = currEig.getRotation().getAnglesZYZ();
			angles[0] = (angles[0]*180.0/BasicMath.PI);
			angles[1] = (angles[1]*180.0/BasicMath.PI);
			
			if (rangeAlpha.contains(angles[0]) && rangeBeta.contains(angles[1]))
			{
				ArrayList<Point3d> pointList = new ArrayList<Point3d>(1);
				pointList.add(new Point3d(angles[0], angles[1], chi2Func.value(angles[0], angles[1])));		
				plot.addScatter(pointList, 10.0, Color.RED);
				break;
			}
		}
						
		//add the plot
		getContentPane().add(plot, BorderLayout.CENTER);
		
		setVisible(true);
	}
}
