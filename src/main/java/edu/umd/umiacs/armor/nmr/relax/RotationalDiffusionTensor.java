/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.physics.Tensor3d;
import edu.umd.umiacs.armor.util.SortType;

/**
 * The Class RotationalDiffusionTensor.
 */
public final class RotationalDiffusionTensor extends Tensor3d
{
	
	private double[] dValues;

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793453381774666407L;

	/**
	 * Creates the.
	 * 
	 * @param tauC
	 *          the tau c
	 * @return the rotational diffusion tensor
	 */
	public static RotationalDiffusionTensor create(double tauC)
	{
		double dc = tensorPrincipalValue(tauC);

		return new RotationalDiffusionTensor(dc, dc, dc, Rotation.identityRotation());
	}
	
	public static RotationalDiffusionTensor createZYZ(double dx, double dy, double dz, double alpha, double beta, double gamma)
	{
		Rotation R = Rotation.createZYZ(alpha, beta, gamma);

		return new RotationalDiffusionTensor(dx, dy, dz, R);
	}

	public static double tensorPrincipalValue(double tauC)
	{
		// convert from ns to 1e-7s
		tauC = tauC * 1.0e-2;

		double dc = 1.0/(6.0*tauC);

		return dc;
	}

	public RotationalDiffusionTensor(double dx, double dy, double dz, Rotation R)
	{
		super(dx, dy, dz, R);
		this.dValues = null;
		
		if (dx < 0.0 || dy < 0.0 || dz < 0.0)
			throw new RelaxationRuntimeException("Rotational diffusion tensor must have positive eigenvalues.");
	}

	public RotationalDiffusionTensor(RealMatrix tensor)
	{
		super(tensor);
		this.dValues = null;
	}

	/**
	 * Anisotropy.
	 * 
	 * @return the double
	 */
	public double anisotropy()
	{
		EigenDecomposition3d eig = getEigenDecomposition().createSortedEigenDecomposition(SortType.ASCENDING);
		
		//prolate
	  if (Math.abs(eig.getEigenvalue(0)-eig.getEigenvalue(1))<Math.abs(eig.getEigenvalue(1)-eig.getEigenvalue(2))) 	
	  	return 2.0 * eig.getEigenvalue(2) / (eig.getEigenvalue(0) + eig.getEigenvalue(1));
	  else
	  	return 2.0 * eig.getEigenvalue(0) / (eig.getEigenvalue(1) + eig.getEigenvalue(2));
	}

	/**
	 * Compute anisotropy std.
	 * 
	 * @param tensorList
	 *          the tensor list
	 * @return the double
	 */
	public double computeAnisotropyStd(List<RotationalDiffusionTensor> tensorList)
	{
		if (tensorList==null || tensorList.isEmpty())
			return Double.NaN;
		
		double anisotropy = 0.0;
		double myAni = anisotropy();
		for (RotationalDiffusionTensor tensor : tensorList)
			anisotropy += BasicMath.square(tensor.anisotropy()-myAni);

		anisotropy = BasicMath.sqrt(anisotropy/tensorList.size());
		
		return anisotropy;
	}
	
	/**
	 * Compute rhombicity std.
	 * 
	 * @param tensorList
	 *          the tensor list
	 * @return the double
	 */
	public double computeRhombicityStd(List<RotationalDiffusionTensor> tensorList)
	{
		if (tensorList==null || tensorList.isEmpty())
			return Double.NaN;
		
		double rhombicity = 0.0;
		double myRhomb = rhombicity();
		for (RotationalDiffusionTensor tensor : tensorList)
			rhombicity += BasicMath.square(tensor.rhombicity()-myRhomb);

		rhombicity = BasicMath.sqrt(rhombicity/tensorList.size());
		
		return rhombicity;
	}
	
	/**
	 * Compute tau c std.
	 * 
	 * @param tensorList
	 *          the tensor list
	 * @return the double
	 */
	public double computeTauCStd(List<RotationalDiffusionTensor> tensorList)
	{
		if (tensorList==null || tensorList.isEmpty())
			return Double.NaN;

		double tauCStd = 0.0;
		double myTauC = getTauC();
		for (RotationalDiffusionTensor tensor : tensorList)
			tauCStd += BasicMath.square(tensor.getTauC()-myTauC);

		tauCStd = BasicMath.sqrt(tauCStd/tensorList.size());
		
		return tauCStd;
	}

	/**
	 * Creates the rotated tensor.
	 * 
	 * @param R
	 *          the r
	 * @return the rotational diffusion tensor
	 */
	public RotationalDiffusionTensor createRotatedTensor(Rotation R)
	{
		EigenDecomposition3d eig = this.getProperEigenDecomposition();
		Rotation newR = eig.getRotation().mult(R);
		
		return new RotationalDiffusionTensor(eig.getEigenvalue(0), eig.getEigenvalue(1), eig.getEigenvalue(2), newR);
	}
	
	/**
	 * Creates the tensor with orientation.
	 * 
	 * @param R
	 *          the r
	 * @return the rotational diffusion tensor
	 */
	public RotationalDiffusionTensor createTensorWithOrientation(Rotation R)
	{
		EigenDecomposition3d eig = this.getProperEigenDecomposition();
		return new RotationalDiffusionTensor(eig.getEigenvalue(0), eig.getEigenvalue(1), eig.getEigenvalue(2), R);
	}
	
	protected synchronized double[] getDValues()
	{
		if (this.dValues!=null)
			return this.dValues;
		
		this.dValues = new double[8];
		
		EigenDecomposition3d eig = this.getProperEigenDecomposition();
		
		double Dx = eig.getEigenvalue(0)*1.0e7; // 1/s
		double Dy = eig.getEigenvalue(1)*1.0e7; // 1/s
		double Dz = eig.getEigenvalue(2)*1.0e7; // 1/s
		
	  double Diso = (Dx+Dy+Dz)/3.0;
	  
	  //numerically unstable !!!!!
	  //Ds = (Dx*Dy+Dy*Dz+Dx*Dz)/3;
	  //Dr = sqrt(Diso^2-Ds);
	  //do this instead
	  double e1 = Dy-Dx;
	  double e2 = Dz-Dx;
	  double Dr3 = BasicMath.sqrt(e1*e1-e1*e2+e2*e2);
	  
	  this.dValues[0] = 4.0*Dx+Dy+Dz;
	  this.dValues[1] = Dx+4.0*Dy+Dz;
	  this.dValues[2] = Dx+Dy+4.0*Dz;
	  this.dValues[3] = 6.0*Diso+2.0*Dr3;
	  this.dValues[4] = 6.0*Diso-2.0*Dr3;
	  
	  //improve on numerical stability
	  //delta(1) = (Dx-Diso)/Dr3/3;
	  //delta(2) = (Dy-Diso)/Dr3/3;
	  //delta(3) = (Dz-Diso)/Dr3/3;
  	
	  double Dr3Inv = 0.0;
  	if (Dr3>Dx*1.0e-6)
  		Dr3Inv = 1.0/Dr3;
  	
  	this.dValues[5] = (-e1-e2)*Dr3Inv;
  	this.dValues[6] = (2.0*e1-e2)*Dr3Inv;
  	this.dValues[7] = (2.0*e2-e1)*Dr3Inv;
  	
  	return this.dValues;
	}
	
	/**
	 * Gets the tau c.
	 * 
	 * @return the tau c
	 */
	public double getTauC()
	{
		double tauc = 1.0/(2.0*trace());

		// convert from 1e-7s to ns
		tauc = tauc * 1.0e2;

		return tauc;
	}
	
	/**
	 * Orientational difference.
	 * 
	 * @param tensor
	 *          the tensor
	 * @return the double
	 */
	public double orientationalDifference(RotationalDiffusionTensor tensor)
	{
		EigenDecomposition3d eig = this.getProperEigenDecomposition();
		return eig.orientationalDistance(tensor.getProperEigenDecomposition());
	}
	
	/**
	 * Rhombicity.
	 * 
	 * @return the double
	 */
	public double rhombicity()
	{
		EigenDecomposition3d eig = getEigenDecomposition().createSortedEigenDecomposition(SortType.ASCENDING);

		if (Math.abs(eig.getEigenvalue(0)-eig.getEigenvalue(1))<Math.abs(eig.getEigenvalue(1)-eig.getEigenvalue(2))) 	
			return 1.5*(eig.getEigenvalue(1)-eig.getEigenvalue(0))/(eig.getEigenvalue(2)-0.5*eig.getEigenvalue(0)-0.5*eig.getEigenvalue(1));
		else
			return 1.5*(eig.getEigenvalue(1)-eig.getEigenvalue(2))/(eig.getEigenvalue(0)-0.5*eig.getEigenvalue(1)-0.5*eig.getEigenvalue(2));
			
	}

	/**
	 * To long string.
	 * 
	 * @return the string
	 */
	public String toLongString()
	{
	  EigenDecomposition3d eig = this.getProperEigenDecomposition();
	  double[] angles = eig.getRotation().getAnglesZYZ();
	  
	  StringBuilder str = new StringBuilder();
	  
	  str.append(String.format("Dx = %.5f *(10^7) 1/s\n", eig.getEigenvalue(0)));
	  str.append(String.format("Dy = %.5f *(10^7) 1/s\n", eig.getEigenvalue(1)));
	  str.append(String.format("Dz = %.5f *(10^7) 1/s\n", eig.getEigenvalue(2)));
	  str.append(String.format("alpha = %.2f deg\n", angles[0]*180.0/BasicMath.PI));
	  str.append(String.format("beta = %.2f deg\n", angles[1]*180.0/BasicMath.PI));
	  str.append(String.format("gamma = %.2f deg\n", angles[2]*180.0/BasicMath.PI));
	  str.append(String.format("TAUc = %.5f ns\n", this.getTauC()));
	  str.append(String.format("anisotropy = %.5f \n", this.anisotropy()));
	  str.append(String.format("rhombicity = %.5f \n",this.rhombicity()));
	  
	  double[][] D = this.toArray();
	  str.append("\n====Diffusion Tensor====\n");
	  str.append(String.format("    [% 8.5f % 8.5f % 8.5f]\n", D[0][0],D[0][1],D[0][2]));
	  str.append(String.format("D = [% 8.5f % 8.5f % 8.5f] *(10^7) 1/s\n",D[1][0],D[1][1],D[1][2]));
	  str.append(String.format("    [% 8.5f % 8.5f % 8.5f]\n",D[2][0],D[2][1],D[2][2]));

	  str.append("\n====Diffusion Tensor Sorted Eigendecomposition====\n");
	  double[][] V = eig.getV().toArray();
	  double[][] VT = eig.getVT().toArray();
	  str.append(String.format("    [% 8.4f  % 8.4f  % 8.4f][% 8.4f  % 8.4f  % 8.4f][% 8.4f  % 8.4f  % 8.4f]\n",V[0][0],V[0][1],V[0][2],eig.getEigenvalue(0),0.0,0.0, VT[0][0],VT[0][1],VT[0][2]));
	  str.append(String.format("D = [% 8.4f  % 8.4f  % 8.4f][% 8.4f  % 8.4f  % 8.4f][% 8.4f  % 8.4f  % 8.4f] *(10^7) 1/s\n",V[1][0],V[1][1],V[1][2],0.0,eig.getEigenvalue(1),0.0, VT[1][0],VT[1][1],VT[1][2]));
	  str.append(String.format("    [% 8.4f  % 8.4f  % 8.4f][% 8.4f  % 8.4f  % 8.4f][% 8.4f  % 8.4f  % 8.4f]\n",V[2][0],V[2][1],V[2][2],0.0,0.0,eig.getEigenvalue(2), VT[2][0],VT[2][1],VT[2][2]));    

	  return str.toString();
	}
}
