/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.refinement.AbstractTwoDomainAligner;
import edu.umd.umiacs.armor.refinement.TwoDomainTensorAligner;
import edu.umd.umiacs.armor.util.SortType;

public final class TwoDomainRdcAligner extends AbstractTwoDomainAligner implements TwoDomainTensorAligner
{
	/** The domain1 computor. */
	private final AlignmentComputor domain1Computor;
	
	/** The domain2 computor. */
	private final AlignmentComputor domain2Computor;
	
	/** The merged computor. */
	private final AlignmentComputor mergedComputor;
	
	public TwoDomainRdcAligner(ExperimentalRdcData data, boolean robust) throws RdcDataException
	{
		super(robust, AlignmentComputor.OUTLIER_THRESHOLD);
		
		ArrayList<String> chains = data.getChains();
		
		if (chains.size()!=2)
			throw new RdcDataException("Relaxation data must have only 2 chains.");
		
		data = data.createRigid();
		this.mergedComputor = new AlignmentComputor(data, robust);
		this.domain1Computor = this.mergedComputor.createSubList(data.createFromChain(chains.get(0)));
		this.domain2Computor = this.mergedComputor.createSubList(data.createFromChain(chains.get(1)));

	}
	
	public AlignmentComputor getComputor()
	{
		return this.mergedComputor;
	}

	@Override
	public ExperimentalRdcData getData()
	{
		return this.mergedComputor.getData();
	}

	@Override
	public List<Rotation> initRotations(Molecule m1, Molecule m2) throws AtomNotFoundException
	{
	  //find the initial alignment
	  AlignmentTensor tensor1 = this.domain1Computor.solve(m1).getTensor();
	  AlignmentTensor tensor2 = this.domain2Computor.solve(m2).getTensor();
	  
	  //find the equivalent eigendecompositions
	  EigenDecomposition3d tensor1SortedEig = tensor1.getEigenDecomposition().createSortedEigenDecomposition(SortType.ASCENDING);
	  ArrayList<EigenDecomposition3d> tensor2SortedEigs = tensor2.getEigenDecomposition().generateEquivalentEigenDecompositions(SortType.ASCENDING);
	  
	  //perform the optimization over all initial points
	  ArrayList<Rotation> rotations = new ArrayList<Rotation>();
	  for (EigenDecomposition3d tensor2Eig : tensor2SortedEigs)
	  {
	  	//inverse rotation, since computing rotation of the PQ vectors
	  	Rotation R0 = tensor1SortedEig.getRotation().mult(tensor2Eig.getRotation().inverse());
	  	rotations.add(R0);
	  }
	  
	  return rotations;
	}

	@Override
	public double[] predict(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		MultiDomainComplex combinedMol = new MultiDomainComplex(mol1, mol2);
		AlignmentTensor tensor = this.mergedComputor.computeTensor(combinedMol);
		return this.mergedComputor.predict(combinedMol, tensor);
	}
}