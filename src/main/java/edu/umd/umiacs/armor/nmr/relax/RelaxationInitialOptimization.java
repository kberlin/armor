/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.CMAESOptimizer;
import edu.umd.umiacs.armor.math.optim.LeastSquaresConverter;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OptimizationRuntimeException;
import edu.umd.umiacs.armor.util.BaseExperimentalData;

/**
 * The Class RelaxationInitialOptimization.
 */
public final class RelaxationInitialOptimization implements MultivariateVectorFunction
{
	private final BondRelaxation bondRelaxation;
	
	private final ModelFreeBondConstraints constraint;
	
	private final int model;
	
	private final IsotropicModelFreePredictor predictor;
	
	private static final double ABSOLUTE_OPTIMIZATION_ERROR = 1.0e-4;
	
	public static final double INITIAL_TUAC_GUESS = 10.0; //ns 
	
	private static final double MAX_TAUC_GUESS = 100.0;
	
	private static final double MIN_TAUC_GUESS = 1.0;
	
	private static final int NUM_OPTIMIZATION_ITERATIONS = 1000;

	
	public static IsotropicModelFreePredictor computeOptimalIsotropicPredictor(BondRelaxation bondRelaxation, double initTauC)
	{
		int modelNumber = 1;
		if (bondRelaxation.sizeObservations() >= 5)
			modelNumber = 4;
		else
		if (bondRelaxation.sizeObservations() >= 4)
			modelNumber = 3;
		else
		if (bondRelaxation.sizeObservations() >= 3)
			modelNumber = 2;
		
		ModelFreeBondConstraints constraints = bondRelaxation.getModelFreeConstraints();
		
		double s2slow = constraints.expS2Slow;
		if (bondRelaxation.isRigid())
			s2slow = .90;

		IsotropicSpectralDensityFunction isoJ = new IsotropicSpectralDensityFunction(initTauC, s2slow, 0.0, constraints.maxS2Fast, 0.0);
		IsotropicModelFreePredictor bestPredictor = new IsotropicModelFreePredictor(isoJ, bondRelaxation.getBond(), 
				constraints.expCSA, bondRelaxation.getBond().getBondType().getMeanRadius(), constraints.expCsaAngle, constraints.minRex);

		BaseExperimentalData data = bondRelaxation.getObservedData();
		double[] targetValues = data.values();
		double[] weights = data.weights();
		
    //try to improve upon the initial guess for tauC
    try
    {
    	//create a single list
    	ArrayList<BondRelaxation> oneList = new ArrayList<BondRelaxation>(1);
    	oneList.add(bondRelaxation);
    	
    	//compute tauC
    	double tauC = AbstractModelFreeComputor.computeTauCApproximate(new ExperimentalRelaxationData(oneList, true));
    	
    	if (tauC<MIN_TAUC_GUESS)
    		tauC = MIN_TAUC_GUESS;
    	else
    	if (tauC>MAX_TAUC_GUESS)
    		tauC = MAX_TAUC_GUESS;
    	
    	bestPredictor = bestPredictor.createWithTauC(tauC);
    }
    catch (RelaxationRuntimeException e) {}
    
		//create the model
		RelaxationInitialOptimization model = new RelaxationInitialOptimization(bondRelaxation, bestPredictor, modelNumber);
		
		//create the initial guess
		double[] x0 = model.createInitGuess();
		double[] lowerBound = model.createLowerBounds();
		double[] upperBound = model.createUpperBounds();
		
		//create the standard deviation guess
		double[] deviation = new double[x0.length];
		for (int x=0; x<x0.length; x++)
		{
			deviation[x] = (upperBound[x]-lowerBound[x])*0.1;
			if (Double.isNaN(deviation[x]) || Double.isInfinite(deviation[x]))
				deviation[x] = x0[x]*0.1;
		}


		CMAESOptimizer optimizer = new CMAESOptimizer(9+(int)Math.floor(3.0*Math.log((double)x0.length)), deviation);
		optimizer.setNumberEvaluations(NUM_OPTIMIZATION_ITERATIONS);
		optimizer.setAbsPointDifference(ABSOLUTE_OPTIMIZATION_ERROR);
		optimizer.setBounds(lowerBound, upperBound);
		optimizer.setFunction(new LeastSquaresConverter(model, targetValues, weights));
		
		try
		{
	  	OptimizationResultImpl result = optimizer.optimize(x0);
	  	
	  	bestPredictor = model.createPredictor(result.getPoint());
		}
		catch(OptimizationRuntimeException e) 
		{
		}
	
		/*
		System.out.format("tauC=%.2f, CSA=%.2f, radius=%.2f, rex=%.2f, s2Slow=%.2f, tauLocal=%.4f\n",
				bestPredictor.getTauC(),
		bestPredictor.getCSA(),
		bestPredictor.getRadius(),
		bestPredictor.getRex(),
		bestPredictor.getS2(),
		bestPredictor.getTauLocal());
		*/
		
		return bestPredictor;
	}

	private RelaxationInitialOptimization(BondRelaxation bondRelaxation, IsotropicModelFreePredictor predictor, int model)
	{
		this.bondRelaxation = bondRelaxation;
		this.constraint = bondRelaxation.getModelFreeConstraints();
		this.predictor = predictor;
		this.model = model;
	}
	
	public double[] createInitGuess()
	{
		double[] initGuess = null;
		
		switch (this.model)
		{
			case 1: initGuess = new double[]{this.predictor.getTauC()} ; break;
			case 2: initGuess = new double[]{this.predictor.getTauC(), this.predictor.getCSA()} ; break;
			case 3: initGuess = new double[]{this.predictor.getTauC(), this.predictor.getCSA(), this.predictor.getRex()} ; break;
			case 4: initGuess = new double[]{this.predictor.getTauC(), this.predictor.getCSA(), this.predictor.getRex(), this.predictor.getS2()} ; break;
			default : throw new RelaxationRuntimeException("Uknown model number: "+this.model);
		}
		
		return initGuess;
	}

	public double[] createLowerBounds()
	{
		double[] lowerBound = null;
		
		switch (this.model)
		{
			case 1: lowerBound = new double[]{MIN_TAUC_GUESS} ; break;
			case 2: lowerBound = new double[]{MIN_TAUC_GUESS, this.constraint.minCSA} ; break;
			case 3: lowerBound = new double[]{MIN_TAUC_GUESS, this.constraint.minCSA, this.constraint.minRex} ; break;
			case 4: lowerBound = new double[]{MIN_TAUC_GUESS, this.constraint.minCSA, this.constraint.minRex, this.constraint.minS2Slow} ; break;
		}
		
		return lowerBound;
	}

	public IsotropicModelFreePredictor createPredictor(double[] x)
	{
		IsotropicModelFreePredictor newPredictor = null;
		switch (this.model)
		{
			case 1: newPredictor = this.predictor.createWithTauC(x[0]); break;
			case 2: newPredictor = this.predictor.createWithTauC(x[0]).createWithCSA(x[1]); break;
			case 3: newPredictor = this.predictor.createWithTauC(x[0]).createWithCSA(x[1]).createWithRex(x[2]); break;
			case 4: newPredictor = this.predictor.createWithTauC(x[0]).createWithCSA(x[1]).createWithRex(x[2]).createWithS2Slow(x[3]); break;
		}

		return newPredictor;
	}
	
	public double[] createUpperBounds()
	{
		double[] upperBound = null;
		
		switch (this.model)
		{
			case 1: upperBound = new double[]{MAX_TAUC_GUESS} ; break;
			case 2: upperBound = new double[]{MAX_TAUC_GUESS, this.constraint.maxCSA} ; break;
			case 3: upperBound = new double[]{MAX_TAUC_GUESS, this.constraint.maxCSA, this.constraint.maxRex} ; break;
			case 4: upperBound = new double[]{MAX_TAUC_GUESS, this.constraint.maxCSA, this.constraint.maxRex, this.constraint.maxS2Slow} ; break;
		}
		
		return upperBound;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.math3.analysis.MultivariateVectorFunction#value(double[])
	 */
	@Override
	public final double[] value(double[] x) throws IllegalArgumentException
	{
		try
		{
			double[] value = AbstractModelFreeComputor.predict(createPredictor(x), this.bondRelaxation);
			
			return value;
		}
		catch (RelaxationRuntimeException e)
		{
			throw e;
		}
	}
}