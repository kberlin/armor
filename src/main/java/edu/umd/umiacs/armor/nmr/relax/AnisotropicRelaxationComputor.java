/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;
import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.LeastSquaresOptimizationResult;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OptimizationRuntimeException;
import edu.umd.umiacs.armor.math.optim.OutlierDampingFunction;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;

/**
 * The Class AnisotropicRelaxationComputor.
 */
public final class AnisotropicRelaxationComputor extends AbstractModelFreeComputor<AnisotropicModelFreePredictor> 
{
	/**
	 * Generate predictors.
	 * 
	 * @param relaxList
	 *          the relax list
	 * @param bondConstraints
	 *          the bond constraints
	 * @return the array list
	 */
	protected static ArrayList<AnisotropicModelFreePredictor> generatePredictors(ExperimentalRelaxationData relaxList)
	{
		ArrayList<AnisotropicModelFreePredictor> predictors = new ArrayList<AnisotropicModelFreePredictor>(relaxList.size());
		for (BondRelaxation bondRelax : relaxList.getBondRelaxationDataRef())
		{
	    Bond bond = bondRelax.getBond();
	  	ModelFreeBondConstraints constraints = bondRelax.getModelFreeConstraints();
	  		  	
	   	AnisotropicSpectralDensityFunction aniJ = AnisotropicSpectralDensityFunction.create(
	   			RotationalDiffusionTensor.create(RelaxationInitialOptimization.INITIAL_TUAC_GUESS), 
					bond.getNormVector(), 
					constraints.expS2Slow, 
					constraints.expTauLoc,
					constraints.expS2Fast,
					constraints.expTauLocFast);
	   	AnisotropicModelFreePredictor aniPredictor = new AnisotropicModelFreePredictor(
	   			aniJ, bond, constraints.expCSA, bond.getBondType().getMeanRadius(), constraints.expCsaAngle, constraints.minRex);
	   	aniPredictor = aniPredictor.createWithVector(bond.getVector());
	   	
	  	predictors.add(aniPredictor);
		}

		return predictors;
	}

	public AnisotropicRelaxationComputor(ExperimentalRelaxationData relaxList)
	{
		this(relaxList, false);
	}
	
	protected AnisotropicRelaxationComputor(ExperimentalRelaxationData relaxList, ArrayList<AnisotropicModelFreePredictor> predictors, 
			boolean optimizePredictors, boolean optimizeRho, 
			boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		super(relaxList, predictors, optimizePredictors, optimizeRho, robust, outlierThreshold, absoluteDiffusionError);
	}

	public AnisotropicRelaxationComputor(ExperimentalRelaxationData relaxList, ArrayList<AnisotropicModelFreePredictor> predictors, 
	  boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		this(relaxList, predictors, false, true, robust, outlierThreshold, absoluteDiffusionError);
	}
	
	public AnisotropicRelaxationComputor(ExperimentalRelaxationData relaxList,
			boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		this(relaxList, generatePredictors(relaxList),  true, true, robust, outlierThreshold, absoluteDiffusionError);
	}
	
	public AnisotropicRelaxationComputor(ExperimentalRelaxationData relaxList, boolean robust)
	{
		this(relaxList, robust, OUTLIER_THRESHOLD, ABSOLUTE_DIFFUSION_ERROR);
	}
	
	public AnisotropicRelaxationComputor(ExperimentalRelaxationData relaxList, boolean robust, double outlierThreshold)
	{
		this(relaxList, robust, outlierThreshold, ABSOLUTE_DIFFUSION_ERROR);
	}
		
	@Override
	protected TensorStorage computeTensor(
			final ArrayList<AnisotropicModelFreePredictor> predictors, 
			final ExperimentalRelaxationData data,
			boolean robust, 
			double outlierThreshold) throws AtomNotFoundException
	{
 		//generate the target and the associated weights
		BaseExperimentalData rhoList = data.getRhoData();
	  double[] targetValues = rhoList.values();
	  double[] errors = rhoList.errors();
	  
	  //create the optimizers
	  MultivariateVectorFunction anisotropicPredictionFunction = new MultivariateVectorFunction()
		{
	  	public final RotationalDiffusionTensor createTensor(double[] x)
	  	{
	  		return RotationalDiffusionTensor.createZYZ(BasicMath.square(x[0]), BasicMath.square(x[1]), 
						BasicMath.square(x[2]), x[3], x[4], x[5]);
	  	}
	  	
			@Override
			public final double[] value(double[] x) throws IllegalArgumentException
			{
				RotationalDiffusionTensor tensor = createTensor(x);
				
				return predictRho(predictors, data, tensor);
			}
		};

	  //create the optimizers
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		if (robust)
			optimizer.setNumberEvaluations(3000);
		else
			optimizer.setNumberEvaluations(2000);
		optimizer.setAbsPointDifference(ABSOLUTE_DIFFUSION_ERROR);
		optimizer.setErrors(errors);
		optimizer.setObservations(targetValues);
		
		MultivariateVectorFunction predictionFunction = anisotropicPredictionFunction;
	  if (robust)
	  {
	  	predictionFunction = new OutlierDampingFunction(predictionFunction, targetValues, errors, outlierThreshold);
	  }
	  optimizer.setFiniteDifferenceFunction(predictionFunction);
	  
	  double[] alpha = BasicUtils.uniformLinePoints(0.0, BasicMath.PI/2.0, 2); //4
	  double[] beta = BasicUtils.uniformLinePoints(0.0, BasicMath.PI/2.0, 2); //4
	  double[] gamma = BasicUtils.uniformLinePoints(0.0, BasicMath.PI/2.0, 2); //4 
	  
	  double[] x0;

	  //create the initial guess based off the isotropic model
	  double dc = RotationalDiffusionTensor.tensorPrincipalValue(AbstractModelFreeComputor.computeTauCApproximate(data));
  	x0 = new double[]{BasicMath.sqrt(0.75*dc), BasicMath.sqrt(dc), BasicMath.sqrt(1.25*dc)}; 
	  
	  //perform the optimization over all initial points
	  ArrayList<LeastSquaresOptimizationResult> results = new ArrayList<LeastSquaresOptimizationResult>();
	  for (double a : alpha)
		  for (double b : beta)
			  for (double g : gamma)
			  {
			  	try
			  	{
			  		LeastSquaresOptimizationResult result = optimizer.optimize(new double[]{x0[0], x0[1], x0[2], a, b, g});
				  	
			  		results.add(result);
			  	}
			  	catch (OptimizationRuntimeException e) {}
			  }

	  LeastSquaresOptimizationResult result = OptimizationResultImpl.getBestSolution(results);
	  double[] x = result.getPoint();
	  
		RotationalDiffusionTensor sol = RotationalDiffusionTensor.createZYZ(BasicMath.square(x[0]), BasicMath.square(x[1]), 
				BasicMath.square(x[2]), x[3], x[4], x[5]);
		
	  return new TensorStorage(sol, result.getChiSquared());
	}
	
	/**
	 * Compute tensor with ratio.
	 * 
	 * @return the rotational diffusion tensor
	 * @throws AtomNotFoundException 
	 */
	public RotationalDiffusionTensor computeTensorWithRatio(Molecule mol) throws AtomNotFoundException
	{
		final AnisotropicRelaxationComputor rigidComputor = this.createRigid();
		
   	//get the predictors with the updated position
  	final ArrayList<AnisotropicModelFreePredictor> predictors;
  	if (mol==null)
  		predictors = rigidComputor.predictors;
  	else
  		predictors = rigidComputor.getUpdatedPredictors(mol);

  	//generate the target and the associated weights
	  final R1R2Ratio anisotropicPredictionFunction = new R1R2Ratio(predictors, rigidComputor.relaxationData);
	  double[] targetValues = anisotropicPredictionFunction.values();
	  double[] errors = anisotropicPredictionFunction.errors();
	  
  	//create the optimizers
	  MultivariateVectorFunction anisotropicPredictionWrapper = new MultivariateVectorFunction()
		{			
			@Override
			public double[] value(double[] x) throws IllegalArgumentException
			{
	  		double[] xadj = new double[]{BasicMath.square(x[0]), BasicMath.square(x[1]), BasicMath.square(x[2]), x[3], x[4], x[5]};
				return anisotropicPredictionFunction.value(xadj);
			}
		};

	  //create the optimizers
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(2000);
		optimizer.setAbsPointDifference(ABSOLUTE_DIFFUSION_ERROR);
		optimizer.setErrors(errors);
		optimizer.setObservations(targetValues);
		
		MultivariateVectorFunction predictionFunction = anisotropicPredictionWrapper;
	  if (this.robust)
	  {
	  	predictionFunction = new OutlierDampingFunction(predictionFunction, targetValues, errors, this.outlierThreshold);
	  }
	  optimizer.setFiniteDifferenceFunction(predictionFunction);
	  
	  //create initial guess
	  double tauC = AbstractModelFreeComputor.computeTauCApproximate(rigidComputor.relaxationData);
	  double dc = RotationalDiffusionTensor.tensorPrincipalValue(tauC);
	  
	  double[] alpha = BasicUtils.uniformLinePoints(0.0, BasicMath.PI/2.0, 2); //4
	  double[] beta = BasicUtils.uniformLinePoints(0.0, BasicMath.PI/2.0, 2); //4
	  double[] gamma = BasicUtils.uniformLinePoints(0.0, BasicMath.PI/2.0, 2); //4 

  	double[] x0 = new double[]{BasicMath.sqrt(.5*dc), BasicMath.sqrt(dc), BasicMath.sqrt(1.5*dc)};
	  
	  //perform the optimization over all initial points
	  ArrayList<OptimizationResultImpl> results = new ArrayList<OptimizationResultImpl>();
	  for (double a : alpha)
		  for (double b : beta)
			  for (double g : gamma)
			  {
			  	try
			  	{
				  	OptimizationResultImpl result = optimizer.optimize(new double[]{x0[0], x0[1], x0[2], a, b, g});
			  		results.add(result);
			  	}
			  	catch (OptimizationRuntimeException e) {}
			  }			  

	  double[] x = OptimizationResultImpl.getBestSolution(results).getPoint();
		RotationalDiffusionTensor sol = RotationalDiffusionTensor.createZYZ(BasicMath.square(x[0]), BasicMath.square(x[1]), BasicMath.square(x[2]), x[3], x[4], x[5]);
	  
	  return sol;
	}

	@Override
	protected AnisotropicModelFreePredictor createFromMolecule(AnisotropicModelFreePredictor predictor, Molecule mol) throws AtomNotFoundException
	{
		return predictor.createFromMolecule(mol);
	}

	@Override
	public AnisotropicModelFreePredictor createPredictor(ModelFreePredictor predictor)
	{
		return (AnisotropicModelFreePredictor)predictor;
	}

	@Override
	public AnisotropicRelaxationComputor createRigid()
	{
		if (this.relaxationData.isRigid())
			return this;
		
		ExperimentalRelaxationData newList = this.relaxationData.createRigid();
		
		return createSubList(newList);		
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.AbstractModelFreeComputor#createSubList(edu.umd.umiacs.armor.nmr.relax.ExperimentalRelaxation)
	 */
	@Override
	public AnisotropicRelaxationComputor createSubList(ExperimentalRelaxationData newList)
	{
		if (newList==this.relaxationData)
			return this;
		
		ArrayList<Integer> indicies = getAssociatedIndices(newList);
		ArrayList<AnisotropicModelFreePredictor> predictors = getBondPredictors(indicies);
		
		return new AnisotropicRelaxationComputor(newList, predictors, false, false, this.robust, this.outlierThreshold, this.absoluteDiffusionError);
	}

	@Override
	protected AnisotropicModelFreePredictor createWithTensor(AnisotropicModelFreePredictor predictor, RotationalDiffusionTensor tensor)
	{
		return predictor.createWithTensor(tensor);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.AbstractModelFreeComputor#isTensorComputable()
	 */
	@Override
	public boolean isTensorComputable()
	{
		if (this.relaxationData.createRigid().size()<6)
			return false;
		
		return true;
	}
}
