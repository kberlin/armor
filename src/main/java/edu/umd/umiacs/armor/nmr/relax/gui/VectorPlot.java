/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax.gui;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JFrame;

import org.jzy3d.colors.Color;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.util.plot.SurfacePlot;

/**
 * The Class VectorPlot.
 */
public class VectorPlot extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3212427625550900376L;

	public VectorPlot(List<Bond> bonds)
  {
		getContentPane().setBackground(java.awt.Color.WHITE);
		
		setTitle("Bond Orientation Plot");
		setBounds(100, 100, 400, 400);
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		//don't resize until 3d plot graphs are fixed
		this.setResizable(false);
		
		SurfacePlot plot = new SurfacePlot();
		
		//create limits
		//Limit rangeAlpha = new Limit(0.0, BasicMath.PI);
		//Limit rangeBeta = new Limit(0.0, BasicMath.PI);

		//prediction scatter plot
		for (Bond bond : bonds)
		{
			plot.addVector(Point3d.getOrigin(), bond.getNormVector(), Color.BLUE);
		}
		
		plot.setXLabel("x");
		plot.setYLabel("y");
		plot.setZLabel("z");
		
		//add the plot
		getContentPane().add(plot, BorderLayout.CENTER);
		
		setVisible(true);
  }
}
