/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.NonNegativeExperimentalDatum;

/**
 * The Class BondRelaxation.
 */
public final class BondRelaxation implements Serializable
{
	
	/** The bond. */
  private final Bond bond;

	/** The experimental relaxation data. */
	private final ArrayList<RelaxationDatum> experimentalRelaxationData;
	
	/** The is rigid. */
	private final boolean rigid;
  
	/** The size of observations. */
	private final int sizeOfObservations;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1086992456793179046L;
  
  /**
	 * Instantiates a new bond relaxation.
	 * 
	 * @param bond
	 *          the bond
	 * @param data
	 *          the data
	 */
  public BondRelaxation(Bond bond, List<RelaxationDatum> data)
  {
  	this.bond = bond;
  	this.experimentalRelaxationData = new ArrayList<RelaxationDatum>(data.size());
  	this.experimentalRelaxationData.addAll(data);
  	
  	//get count on the number of relaxation restraints and check that either all are rigid or not rigid
		int sizeCount = 0;
		boolean rigid = true;
		for (RelaxationDatum datum : this.experimentalRelaxationData)
		{
			if (!datum.isRigid())
				rigid = false;
			
			sizeCount+= datum.size();
		}
		
    this.sizeOfObservations = sizeCount;
    this.rigid = rigid;
  }
  
  protected BondRelaxation(BondRelaxation prev, ArrayList<RelaxationDatum> data)
  {
  	this.bond = prev.bond;
  	this.sizeOfObservations = prev.sizeOfObservations;
  	this.rigid = prev.rigid;  	
		this.experimentalRelaxationData = data;
  }

	/**
	 * Creates the monte carlo.
	 * 
	 * @return the bond relaxation
	 */
	public BondRelaxation createMonteCarlo()
	{
  	ArrayList<RelaxationDatum> newDataList = new ArrayList<RelaxationDatum>(this.experimentalRelaxationData.size());
  	
  	for (RelaxationDatum datum : this.experimentalRelaxationData)
  		newDataList.add(datum.createMonteCarlo());

  	return new BondRelaxation(this, newDataList);
	}
	
	public BondRelaxation createWithRhos(List<NonNegativeExperimentalDatum> rhos)
	{
		ArrayList<RelaxationDatum> data = new ArrayList<RelaxationDatum>(this.experimentalRelaxationData.size());
		
		Iterator<NonNegativeExperimentalDatum> rhoIter = rhos.iterator();
		for (RelaxationDatum datum : this.experimentalRelaxationData)
		{
			data.add(datum.createWithRho(rhoIter.next()));
		}
		
		return new BondRelaxation(this, data);
	}
	
	/**
	 * Gets the bond.
	 * 
	 * @return the bond
	 */
	public Bond getBond()
	{
		return this.bond;
	}
	
	public ModelFreeBondConstraints getModelFreeConstraints()
	{
		return this.experimentalRelaxationData.get(0).getConstraints();
	}
	
	public ArrayList<BaseExperimentalDatum> getNoeData()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(size());
		for (RelaxationDatum datum : this.experimentalRelaxationData)
		{
			data.add(datum.getNoe());
		}
		
		return data;
	}
	
	public BaseExperimentalData getObservedData()
	{
		ArrayList<BaseExperimentalDatum> relaxationValues = new ArrayList<BaseExperimentalDatum>(sizeObservations());
		
		for (RelaxationDatum datum : this.experimentalRelaxationData)
		{			
			for (BaseExperimentalDatum expValue : datum.getValues())
				relaxationValues.add(expValue);
		}

		return new BaseExperimentalData(relaxationValues);
	}
	
	public ArrayList<BaseExperimentalDatum> getR1Data()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(size());
		for (RelaxationDatum datum : this.experimentalRelaxationData)
		{
			data.add(datum.getR1());
		}
		
		return data;
	}
	
	public ArrayList<BaseExperimentalDatum> getR2Data()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(size());
		for (RelaxationDatum datum : this.experimentalRelaxationData)
		{
			data.add(datum.getR2());
		}
		
		return data;
	}
	
	public BaseExperimentalData getRatioData()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(size());
		for (RelaxationDatum datum : this.experimentalRelaxationData)
		{
			data.add(datum.getRatio());
		}
		
		return new BaseExperimentalData(data);
	}

	/**
	 * Gets the relaxation datum.
	 * 
	 * @param index
	 *          the index
	 * @return the relaxation datum
	 */
	public RelaxationDatum getRelaxationDatum(int index)
	{
		return this.experimentalRelaxationData.get(index);
	}

	public ArrayList<RelaxationDatum> getRelaxationList()
	{
		return new ArrayList<RelaxationDatum>(this.experimentalRelaxationData);				
	}

	protected ArrayList<RelaxationDatum> getRelaxationListRef()
	{
		return this.experimentalRelaxationData;
	}

	public ArrayList<BaseExperimentalDatum> getRhoData()
	{
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(size());
		for (RelaxationDatum datum : this.experimentalRelaxationData)
		{
			data.add(datum.getRho());
		}
		
		return data;
	}

	public boolean isRigid()
	{
		return this.rigid;
	}
	
	public int size()
	{
		return this.experimentalRelaxationData.size();
	}
	
	public int sizeObservations()
	{
		return this.sizeOfObservations;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();

		str.append("Bond: "+this.bond+"\n");
		for (RelaxationDatum d : this.experimentalRelaxationData)
			str.append("  "+d+"\n");
		
		return str.toString();
	}
}
