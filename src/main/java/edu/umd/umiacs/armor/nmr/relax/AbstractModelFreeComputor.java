/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.math3.exception.NumberIsTooSmallException;
import org.apache.commons.math3.exception.OutOfRangeException;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.CMAESOptimizer;
import edu.umd.umiacs.armor.math.optim.LeastSquaresConverter;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OptimizationRuntimeException;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.physics.BasicPhysics;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;

/**
 * The Class AbstractModelFreeComputor.
 * 
 * @param <Computor>
 *          the generic type
 * @param <Predictor>
 *          the generic type
 */
public abstract class AbstractModelFreeComputor<Predictor extends ModelFreePredictor> implements ModelFreeComputor<Predictor>
{	
	public final static class ModelDynamics implements MultivariateVectorFunction
	{
		/** The bond relaxation. */
		private final BondRelaxation bondRelaxation;
		
		private final boolean fitCSA;
		
		private final int model;
		
		/** The predictor. */
		private final ModelFreePredictor predictor;

		private final ModelFreeBondConstraints constraint;
		
		public static final int NUM_MODELS = 8;
		
		public ModelDynamics(ModelFreePredictor predictor, BondRelaxation bondRelaxation, int model, boolean fitCSA)
		{
			if (model<1 || model>NUM_MODELS)
				throw new RelaxationRuntimeException("Uknown modelfree model number: "+model);
			
			this.predictor = predictor;
			this.bondRelaxation = bondRelaxation;
			this.constraint = bondRelaxation.getModelFreeConstraints();
			this.model = model;
			this.fitCSA = fitCSA;
		}
		
		public double[] createInitGuess()
		{
			double[] initGuess = null;
			
			double tauCBound = this.predictor.getTauC();
			tauCBound = tauCBound*0.33;
			
			switch (this.model)
			{
				case 1: initGuess = new double[]{this.predictor.getS2Slow()} ; break;
				case 2: initGuess = new double[]{this.predictor.getS2Slow(), Math.min(this.constraint.minTauLoc+1.0e-5, tauCBound-1.0e-5)} ; break;
				case 3: initGuess = new double[]{this.predictor.getS2Slow(), this.predictor.getRex()} ; break;
				case 4: initGuess = new double[]{this.predictor.getS2Slow(), Math.min(this.constraint.minTauLoc+1.0e-5, tauCBound-1.0e-5), this.predictor.getRex()} ; break;
				case 5: initGuess = new double[]{this.predictor.getS2Slow(), Math.min(this.constraint.minTauLoc+1.0e-5, tauCBound-1.0e-5), this.predictor.getS2Fast()} ; break;
				case 6: initGuess = new double[]{this.predictor.getS2Slow(), Math.min(this.constraint.minTauLoc+1.0e-5, tauCBound-1.0e-5), this.predictor.getS2Fast(), this.predictor.getRex()} ; break;
				case 7: initGuess = new double[]{this.predictor.getS2Slow(), Math.min(this.constraint.minTauLoc+1.0e-5, tauCBound-1.0e-5), this.predictor.getS2Fast(), this.constraint.minTauFast+1.0e-5} ; break;
				case 8: initGuess = new double[]{this.predictor.getS2Slow(), Math.min(this.constraint.minTauLoc+1.0e-5, tauCBound-1.0e-5), this.predictor.getS2Fast(), this.constraint.minTauFast+1.0e-5, this.predictor.getRex()} ; break;
				default : throw new RelaxationRuntimeException("Uknown model "+this.model);
			}
			
			//make sure value away from 0
			for (int iter=0; iter<initGuess.length; iter++)
				if (initGuess[iter]<=1.0e-5)
					initGuess[iter] = 1.0e-5;
			
			//set the CSA
			if (this.fitCSA)
			{
				double[] initGuessNew = new double[initGuess.length+1];
				for (int iter=0; iter<initGuess.length; iter++)
					initGuessNew[iter] = initGuess[iter];
				
				initGuessNew[initGuessNew.length-1] = this.constraint.expCSA;
				
				//set the initial guess size
				initGuess = initGuessNew;
			}
						
			return initGuess;
		}
		
		public double[] createLowerBounds()
		{
			double[] lowerBound = null;
			
			switch (this.model)
			{
				case 1: lowerBound = new double[]{this.constraint.minS2Slow} ; break;
				case 2: lowerBound = new double[]{this.constraint.minS2Slow, 0.0} ; break;
				case 3: lowerBound = new double[]{this.constraint.minS2Slow, this.constraint.minRex} ; break;
				case 4: lowerBound = new double[]{this.constraint.minS2Slow, 0.0, this.constraint.minRex} ; break;
				case 5: lowerBound = new double[]{this.constraint.minS2Slow, this.constraint.minTauLoc, this.constraint.minS2Fast} ; break;
				case 6: lowerBound = new double[]{this.constraint.minS2Slow, this.constraint.minTauLoc, this.constraint.minS2Fast, this.constraint.minRex} ; break;
				case 7: lowerBound = new double[]{this.constraint.minS2Slow, this.constraint.minTauLoc, this.constraint.minS2Fast, this.constraint.minTauFast} ; break;
				case 8: lowerBound = new double[]{this.constraint.minS2Slow, this.constraint.minTauLoc, this.constraint.minS2Fast, this.constraint.minTauFast, this.constraint.minRex} ; break;
			}
			
			//set the CSA
			if (this.fitCSA)
			{
				double[] lowerBoundNew = new double[lowerBound.length+1];
				for (int iter=0; iter<lowerBound.length; iter++)
					lowerBoundNew[iter] = lowerBound[iter];
				
				lowerBoundNew[lowerBoundNew.length-1] = this.constraint.minCSA;
				lowerBound = lowerBoundNew;
			}

			return lowerBound;
		}
		
		public ModelFreePredictor createPredictor(double[] x)
		{
			ModelFreePredictor newPredictor = null;
			switch (this.model)
			{
				case 1: newPredictor = this.predictor.createWithLocal(x[0], 0.0, 1.0, 0.0).createWithRex(0.0); break;
				case 2: newPredictor = this.predictor.createWithLocal(x[0], x[1], 1.0, 0.0).createWithRex(0.0); break;
				case 3: newPredictor = this.predictor.createWithLocal(x[0], 0.0, 1.0, 0.0).createWithRex(x[1]); break;
				case 4: newPredictor = this.predictor.createWithLocal(x[0], x[1], 1.0, 0.0).createWithRex(x[2]); break;
				case 5: newPredictor = this.predictor.createWithLocal(x[0], x[1], x[2], 0.0).createWithRex(0.0); break;
				case 6: newPredictor = this.predictor.createWithLocal(x[0], x[1], x[2], 0.0).createWithRex(x[3]); break;
				case 7: newPredictor = this.predictor.createWithLocal(x[0], x[1], x[2], x[3]).createWithRex(0.0); break;
				case 8: newPredictor = this.predictor.createWithLocal(x[0], x[1], x[2], x[3]).createWithRex(x[4]); break;
			}
			
			if (this.fitCSA)
				newPredictor = newPredictor.createWithCSA(x[x.length-1]);
			
			//set the model
			newPredictor = newPredictor.createWithModel(this.model);

			return newPredictor;
		}
		
		public int getModel()
		{
			return this.model;
		}
		
		public double[] createUpperBounds()
		{
			double[] upperBound = null;
			
			//make sure that maximun value is below the local tauC
			double tauCBound = this.predictor.getTauC();
			tauCBound = tauCBound*0.33;
			
			switch (this.model)
			{
				case 1: upperBound = new double[]{this.constraint.maxS2Slow} ; break;
				case 2: upperBound = new double[]{this.constraint.maxS2Slow, tauCBound} ; break;
				case 3: upperBound = new double[]{this.constraint.maxS2Slow, this.constraint.maxRex} ; break;
				case 4: upperBound = new double[]{this.constraint.maxS2Slow, tauCBound, this.constraint.maxRex} ; break;
				case 5: upperBound = new double[]{this.constraint.maxS2Slow, tauCBound, this.constraint.maxS2Fast} ; break;
				case 6: upperBound = new double[]{this.constraint.maxS2Slow, tauCBound, this.constraint.maxS2Fast, this.constraint.maxRex} ; break;
				case 7: upperBound = new double[]{this.constraint.maxS2Slow, tauCBound, this.constraint.maxS2Fast, this.constraint.maxTauFast} ; break;
				case 8: upperBound = new double[]{this.constraint.maxS2Slow, tauCBound, this.constraint.maxS2Fast, this.constraint.maxTauFast, this.constraint.maxRex} ; break;
			}
			
			//set the CSA
			if (this.fitCSA)
			{
				double[] upperBoundNew = new double[upperBound.length+1];
				for (int iter=0; iter<upperBound.length; iter++)
					upperBoundNew[iter] = upperBound[iter];
				
				upperBoundNew[upperBoundNew.length-1] = this.constraint.maxCSA;
				upperBound = upperBoundNew;
			}

			return upperBound;
		}

		/* (non-Javadoc)
		 * @see org.apache.commons.math3.analysis.MultivariateVectorFunction#value(double[])
		 */
		@Override
		public final double[] value(double[] x) throws IllegalArgumentException
		{
			return predict(createPredictor(x), this.bondRelaxation);
		}
	}	
	
  protected static final class R1R2Ratio implements MultivariateVectorFunction
	{
		private ExperimentalRelaxationData data;
		private final ArrayList<? extends ModelFreePredictor> predictors;
		
		public R1R2Ratio(ArrayList<? extends ModelFreePredictor> predictors, ExperimentalRelaxationData data)
		{
			this.predictors = predictors;
			this.data = data;
		}
		
		public double[] errors()
		{
			return this.data.getRatioData().errors();			
		}
		
		protected final double[] predictRatio(ArrayList<ModelFreePredictor> predictors)
		{
			double[] rho = new double[this.data.sizeRho()];
			
			Iterator<ModelFreePredictor> predictorIterator = predictors.iterator();

			int count = 0;
			for (BondRelaxation b : this.data.getBondRelaxationDataRef())
			{
				ModelFreePredictor predictor = predictorIterator.next();
				
				for (RelaxationDatum datum : b.getRelaxationListRef())
				{
			    rho[count] = predictor.predictR2(datum.getFrequency())/predictor.predictR1(datum.getFrequency());
					count++;
				}
			}
			
			return rho;
		}
		
		/* (non-Javadoc)
		 * @see org.apache.commons.math3.analysis.MultivariateVectorFunction#value(double[])
		 */
		@Override
		public final double[] value(double[] x) throws IllegalArgumentException
		{
			RotationalDiffusionTensor tensor = RotationalDiffusionTensor.createZYZ(x[0], x[1], x[2], x[3], x[4], x[5]);

			ArrayList<ModelFreePredictor> predictors = new ArrayList<ModelFreePredictor>(this.predictors.size());
			for (ModelFreePredictor predictor : this.predictors)
			{
				//adjust based on the values commonly taken in literature
				predictor = predictor.createWithCSA(predictor.getModelFreeBondConstraint().expCSA);
				predictor = predictor.createWithLocal(1.0, 0.0, 1.0, 0.0).createWithRex(0.0);
				
				//init the tensor value
				predictor = predictor.createWithTensor(tensor);
				
				predictors.add(predictor);
			}
			
			return predictRatio(predictors);
		}
		
		public double[] values()
		{
			return this.data.getRatioData().values();
		}
		
	}	
	
	private final class SolutionStorage
	{
		public final ExperimentalRelaxationData data;
		public final ArrayList<Predictor> predictors;
		public final double rhoChi2Effective;

		public SolutionStorage(ArrayList<Predictor> predictors, ExperimentalRelaxationData data, double rhoChi2Effecive)
		{
			this.predictors = predictors;
			this.data = data;
			this.rhoChi2Effective = rhoChi2Effecive;
		}
	}
	
	protected final class TensorStorage
	{
		public final double rhoChi2Effective;
		public final RotationalDiffusionTensor tensor;

		public TensorStorage(RotationalDiffusionTensor tensor, double rhoChi2Effecive)
		{
			this.tensor = tensor;
			this.rhoChi2Effective = rhoChi2Effecive;
		}
	}
	
	protected final double absoluteDiffusionError;
	
	protected final double outlierThreshold;
	
	/** The predictors. */
	protected final ArrayList<Predictor> predictors;
	
	/** The experimental data. */
	protected final ExperimentalRelaxationData relaxationData;
	
	protected final boolean robust;
	
	/** The Constant ABSOLUTE_DIFFUSION_ERROR. */
	public static final double ABSOLUTE_DIFFUSION_ERROR = 1.0e-6;
	
	private static final int NUM_LOCAL_OPTIMIZATION_ITERATIONS = 1500;
	
	private static final int NUM_RHO_REFINEMENT = 2;
	

	/** The Constant OUTLIER_THRESHOLD. */
	public static final double OUTLIER_THRESHOLD = 3.0; //3.0
	
	/**
	 * Compute tau c approximate.
	 * 
	 * @param rho
	 *          the rho
	 * @param omegaP
	 *          the omega p
	 * @return the double
	 */
	public final static double computeTauCApproximate(double rho, double omegaP)
	{
		if (rho<4.0/3.0)
			throw new RelaxationRuntimeException("Rho value must be greater than 4/3.");
				
		//in 1/seconds
	  //double dc = Math.abs(omegaP/6.0*BasicMath.sqrt(rho/(3.0/4.0-rho)));
		double tauC = Math.abs(BasicMath.sqrt(0.75*rho-1.0)/omegaP);
		
	  return tauC*1.0e9; //conver to ns
	}

	/**
	 * Compute tau c approximate.
	 * 
	 * @return the double
	 */
	public static final double computeTauCApproximate(ExperimentalRelaxationData data)
	{
		ArrayList<Double> tauCList = new ArrayList<Double>(data.sizeRho());
		
		Iterator<BaseExperimentalDatum> rhoIterator = data.getRhoData().iterator();
		for (BondRelaxation b : data.getBondRelaxationDataRef())
		{
			Bond bond = b.getBond();
			for (RelaxationDatum datum : b.getRelaxationListRef())
			{
				double omegaP = BasicPhysics.larmorFrequency(datum.getFrequency(), bond.getFromAtom().getType().getGyromagneticRatio());
				
				try
				{
					double tauC = computeTauCApproximate(rhoIterator.next().value(), omegaP);
					tauCList.add(tauC);
				}
				catch(RelaxationRuntimeException e) {}
			}
		}
		
		if (tauCList.isEmpty())
			return RelaxationInitialOptimization.INITIAL_TUAC_GUESS;
		
		return BasicStat.median(tauCList);
	}
	
	public final static double[] predict(ModelFreePredictor predictor, BondRelaxation bondRelaxation)
	{
		double[] values = new double[bondRelaxation.sizeObservations()];
		
		int sizeCount = 0;
		for (RelaxationDatum datum : bondRelaxation.getRelaxationListRef())
		{
			double[] predictionData = predictor.predict(datum.getFrequency(), datum);
			for (int iter=0; iter<datum.numValues(); iter++)
			{
				if (datum.hasValue(iter))
				{
					values[sizeCount] = predictionData[iter];
					sizeCount++;
				}
			}
		}				
		
		return values;
	}
	
	protected AbstractModelFreeComputor(ExperimentalRelaxationData relaxList, ArrayList<Predictor> predictors, 
			boolean optimizePredictors, boolean optimizeRho,
			boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		if (relaxList.size()!=predictors.size())
			throw new RelaxationRuntimeException("Relaxation and predictor sizes must match.");
		
		this.robust = robust;
		this.outlierThreshold = outlierThreshold;
		this.absoluteDiffusionError = absoluteDiffusionError;
		
		if (optimizePredictors)
			this.predictors = singleBondOptimization(relaxList, predictors);
		else
			this.predictors = predictors;
		
		if (optimizeRho)
			this.relaxationData = relaxList.createWithUpdatedRhos(this.predictors);
		else
			this.relaxationData = relaxList;
	}
	
	protected abstract TensorStorage computeTensor(ArrayList<Predictor> predictors, ExperimentalRelaxationData currData, boolean robust, double outlierThreshold) throws AtomNotFoundException;
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreeComputor#computeTensor(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public RotationalDiffusionTensor computeTensor(Molecule mol) throws AtomNotFoundException
	{
		return solveTensor(mol).getTensor();
	}
	
	protected abstract Predictor createFromMolecule(Predictor predictor, Molecule mol) throws AtomNotFoundException;
		
	protected abstract Predictor createPredictor(ModelFreePredictor predictor);

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreeComputor#createRigid()
	 */
	@Override
	public abstract AbstractModelFreeComputor<Predictor> createRigid();
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.ModelFreeComputor#createSubList(edu.umd.umiacs.armor.nmr.relax.ExperimentalRelaxationData)
	 */
	@Override
	public abstract ModelFreeComputor<Predictor> createSubList(ExperimentalRelaxationData newList);
	
	protected abstract Predictor createWithTensor(Predictor predictor, RotationalDiffusionTensor tensor);
		
	/**
	 * Gets the associated indices.
	 * 
	 * @param list
	 *          the list
	 * @return the associated indices
	 */
	protected final ArrayList<Integer> getAssociatedIndices(ExperimentalRelaxationData list)
	{
		ArrayList<Integer> indices = new ArrayList<Integer>(list.size());
		ArrayList<Bond> bonds = this.relaxationData.getBonds();
		for (BondRelaxation bondRelax : list.getBondRelaxationDataRef())
		{			
			int index = bonds.indexOf(bondRelax.getBond());
			indices.add(index);
		}
		
		return indices;
	}

	/**
	 * Gets the bond predictors.
	 * 
	 * @param indicies
	 *          the indicies
	 * @return the bond predictors
	 */
	protected final ArrayList<Predictor> getBondPredictors(ArrayList<Integer> indicies)
	{
		 ArrayList<Predictor> bondPredictors = new ArrayList<Predictor>(indicies.size());
		 for (int index : indicies)
			 bondPredictors.add(this.predictors.get(index));
		 
		 return bondPredictors;
	}

	@Override
	public final BaseExperimentalData getData()
	{
		return this.relaxationData.getRhoData();
	}
	
	/**
	 * @return the outlierThreshold
	 */
	public final double getOutlierThreshold()
	{
		return this.outlierThreshold;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationComputor#getPredictor(int)
	 */
	@Override
	public final Predictor getPredictor(int index)
	{
		return this.predictors.get(index);
	}
	
	@Override
	public final ExperimentalRelaxationData getRelaxationData()
	{
		return this.relaxationData;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationComputor#getRelaxationRatePredictors()
	 */
	@Override
	public final ArrayList<Predictor> getRelaxationRatePredictors()
	{
		return this.predictors;
	}

	public final ArrayList<Predictor> getUpdatedPredictors(Molecule mol) throws AtomNotFoundException
	{
		if (mol==null)
			return this.predictors;
		
		ArrayList<Predictor> updatedPredictorList = new ArrayList<Predictor>(this.predictors.size());
		
		for (Predictor predictor : this.predictors)
		{
			Predictor castPredictor = createFromMolecule(predictor, mol);
			updatedPredictorList.add(castPredictor);
		}
		
		return updatedPredictorList;
	}
	/**
	 * @return the robust
	 */
	public final boolean isRobust()
	{
		return this.robust;
	}
	
	/**
	 * Checks if is tensor computable.
	 * 
	 * @return true, if is tensor computable
	 */
	public abstract boolean isTensorComputable();

	/**
	 * Optimize local parameters.
	 * 
	 * @param uid
	 *          the index
	 * @return the predictor
	 */
	protected final Predictor optimizeLocalParameters(Predictor modelFreePredictor, BondRelaxation bondRelaxation, Predictor prevPredictor)
	{
 	//generate the target and the associated weights
  	BaseExperimentalData data = bondRelaxation.getObservedData();
	  double[] targetValues = data.values();
	  double[] weights = data.weights();
	  
	  ModelFreeBondConstraints constraint = bondRelaxation.getModelFreeConstraints();
	  
	  ModelFreePredictor bestPredictor = modelFreePredictor;
	  
	  //init the holding arrays
	  ArrayList<ModelDynamics> models = new ArrayList<ModelDynamics>(ModelDynamics.NUM_MODELS);
	  ArrayList<OptimizationResultImpl> results = new ArrayList<OptimizationResultImpl>(models.size());
	  ArrayList<Integer> numParametersArray = new ArrayList<Integer>(models.size());
	  
	  //zero the Rex
	  bestPredictor = bestPredictor.createWithRex(0.0);
	  
	  boolean fitCSA;
		if (bondRelaxation.sizeObservations()>3)
		{
			fitCSA = true;
		}
		else
		{
			fitCSA = false;
			
			//set the CSA values to expected values
		  bestPredictor = bestPredictor.createWithCSA(constraint.expCSA);			
		}
	  
	  for (int iter=0; iter<ModelDynamics.NUM_MODELS; iter++)
	  {
  		if (prevPredictor==null || prevPredictor.getModel()==(iter+1))
	  	{
		  	ModelDynamics model = new ModelDynamics(bestPredictor, bondRelaxation, iter+1, fitCSA);
		  	
		  	double[] initGuess = model.createInitGuess();
		  	
		  	//make sure that you are not over fitting models
		  	if (initGuess.length>bondRelaxation.sizeObservations())
		  		break;
		  	
		  	models.add(model);
		  	
		  	//create the constraints
		  	double[] lowerBounds = model.createLowerBounds();
		  	double[] upperBounds = model.createUpperBounds();
		  	
	  		//get number of parameters
		  	int numParameters = initGuess.length;
		  	numParametersArray.add(new Integer(numParameters));
		  	
		  	//create the standard deviation guess
		  	double[] deviation = new double[numParameters];
		  	for (int x=0; x<numParameters; x++)
		  	{
		  		deviation[x] = (upperBounds[x]-lowerBounds[x])*0.1;
		  		if (Double.isNaN(deviation[x]) || Double.isInfinite(deviation[x]))
	  			deviation[x] = initGuess[x]*0.1;
		  	}
		  	
		  	//create the optimizer
		  	CMAESOptimizer optimizer = new CMAESOptimizer(9+(int)Math.floor(3.0*Math.log((double)numParameters)), deviation);
		  	optimizer.setNumberEvaluations(NUM_LOCAL_OPTIMIZATION_ITERATIONS);
		  	optimizer.setAbsPointDifference(1.0e-8);
		  	optimizer.setBounds(lowerBounds, upperBounds);
		  	optimizer.setFunction(new LeastSquaresConverter(model, targetValues, weights));
		  	
		  	//store result
		  	try
		  	{
		  		//System.out.println("Upper="+BasicUtils.toString(upperBounds)+" Model="+(iter+1));
		  		//System.out.println("Init="+BasicUtils.toString(initGuess)+" Model="+(iter+1));
		  		//System.out.println("Lower="+BasicUtils.toString(lowerBounds)+" Model="+(iter+1)+"Residue"+bondRelaxation.getBond());
		  		//System.out.flush();
			  	OptimizationResultImpl result = optimizer.optimize(initGuess);
			  	
			  	results.add(result);
		  	}
				catch(OptimizationRuntimeException e) 
				{
					throw new RelaxationRuntimeException("Unable to perform local optimization.",e);
				}
		  	catch (NumberIsTooSmallException e)
		  	{
					//throw new RelaxationRuntimeException("Initial constraints violated for "+constraint.nameP+" "+constraint.nameQ+" for model "+(iter+1)+".",e);
		  	}
		  	catch (OutOfRangeException e)
		  	{
					//throw new RelaxationRuntimeException("Initial constraints violated for "+constraint.nameP+" "+constraint.nameQ+" for model "+(iter+1)+".",e);
		  	}
	  	}
	  }
	  
	  //figure out the best model and store that result
	  double bestAIC = Double.POSITIVE_INFINITY;
	  ModelFreePredictor optimalSolution = null;
	  for (int iter=0; iter<results.size(); iter++)
	  {
	  	double currAIC = BasicStat.akaikeInformationCriterion(results.get(iter).getValue(),numParametersArray.get(iter));
	  	//double currAIC = Statistics.akaikeInformationCriterionCorrected(results.get(iter).getValue(),numParametersArray.get(iter), bondRelaxation.sizeObservations());
	  	if (currAIC<bestAIC)
	  	{
		  	bestAIC = currAIC;
		  	optimalSolution = models.get(iter).createPredictor(results.get(iter).getPoint());
	  	}
	  }
	  
	  return createPredictor(optimalSolution);
	}

	public final Predictor optimizePredictor(final BondRelaxation bondRelax, Predictor predictor)
  {
  	//get the optimal results
  	IsotropicModelFreePredictor currPredictor = RelaxationInitialOptimization.computeOptimalIsotropicPredictor(bondRelax, predictor.getTauC());
  	
  	//store the results
		Predictor newPredictor = createPredictor(predictor.createWithParametersFrom(currPredictor));
  	
  	return newPredictor;
  }

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationComputor#predict(edu.umd.umiacs.armor.molecule.Molecule, edu.umd.umiacs.armor.nmr.relax.RotationalDiffusionTensor)
	 */
	@Override
	public final double[] predict(Molecule mol, RotationalDiffusionTensor tensor) throws AtomNotFoundException
	{
		return predictRho(mol, tensor);
	}

	protected final double[] predictRho(ArrayList<Predictor> predictors)
	{
		double[] rho = new double[this.relaxationData.sizeRho()];
		
		Iterator<Predictor> predictorIterator = predictors.iterator();

		int count = 0;
		for (BondRelaxation b : this.relaxationData.getBondRelaxationDataRef())
		{
			Predictor predictor = predictorIterator.next();
			
			for (RelaxationDatum datum : b.getRelaxationListRef())
			{
		    rho[count] = predictor.predictRho(datum.getFrequency());
				count++;
			}
		}
		
		return rho;
	}
	
	
	protected final double[] predictRho(ArrayList<Predictor> predictors, ExperimentalRelaxationData relaxData, RotationalDiffusionTensor tensor)
	{
		double[] rho = new double[relaxData.sizeRho()];
		
		Iterator<Predictor> predictorIterator = predictors.iterator();

		int count = 0;
		for (BondRelaxation b : relaxData.getBondRelaxationDataRef())
		{
			Predictor predictor = createWithTensor(predictorIterator.next(), tensor);

	    for (RelaxationDatum datum : b.getRelaxationListRef())
			{
		    rho[count] = predictor.predictRho(datum.getFrequency());
				count++;
			}
		}
		
		return rho;
	}
	
	public final double[] predictRho(Molecule mol, RotationalDiffusionTensor tensor) throws AtomNotFoundException
	{
		return predictRho(getUpdatedPredictors(mol), this.relaxationData, tensor);
	}

	protected final double[] predictRhoEta(ArrayList<Predictor> predictors, ExperimentalRelaxationData relaxData, RotationalDiffusionTensor tensor)
	{
		double[] rho = new double[relaxData.sizeRho()];
		
		Iterator<Predictor> predictorIterator = predictors.iterator();

		int count = 0;
		for (BondRelaxation b : relaxData.getBondRelaxationDataRef())
		{
			Predictor predictor = createWithTensor(predictorIterator.next(), tensor);

	    for (RelaxationDatum datum : b.getRelaxationListRef())
			{
		    rho[count] = predictor.predictRhoEta(datum.getFrequency());
				count++;
			}
		}
		
		return rho;
	}
	
	protected final ArrayList<Predictor> singleBondOptimization(ExperimentalRelaxationData relaxList, ArrayList<Predictor> predictors)
	{
		Iterator<Predictor> predictorIterator = predictors.iterator();
		ArrayList<Predictor> newPredictors = new ArrayList<Predictor>();
		for (BondRelaxation bondRelax : relaxList.getBondRelaxationDataRef())
		{
			newPredictors.add(optimizePredictor(bondRelax, predictorIterator.next()));
		}
			
		return newPredictors;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationComputor#size()
	 */
	@Override
	public final int size()
	{
		return this.relaxationData.size();
	}

	public final RelaxationSolution<Predictor> solve()
	{
		try
		{
			return solve(null);
		}
		catch (AtomNotFoundException e)
		{
			throw new RelaxationRuntimeException("Unexpected error.", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentSolver#solve(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public final RelaxationSolution<Predictor> solve(Molecule mol) throws AtomNotFoundException
	{
		return solve(mol, null);
	}
	
	protected final RelaxationSolution<Predictor> solve(Molecule mol, ArrayList<Predictor> prevPredictors) throws AtomNotFoundException
	{
		ArrayList<Predictor> predictors = getUpdatedPredictors(mol);
		SolutionStorage storage = solveTensorHelper(predictors);
		
		ArrayList<Predictor> newPredictors = new ArrayList<Predictor>(storage.predictors.size());
		int count = 0;
		Predictor prevPredictor = null;
		for (Predictor predictor : storage.predictors)
		{
			if (prevPredictors!=null)
				prevPredictor = prevPredictors.get(count);
				
			Predictor newPredictor = optimizeLocalParameters(predictor, storage.data.getBondRelaxation(count), prevPredictor);
			newPredictors.add(newPredictor);
			
			count++;
		}
		
		return new RelaxationSolution<Predictor>(newPredictors, storage.data, storage.rhoChi2Effective);
	}
	
	public final RelaxationSolution<Predictor> solveTensor()
	{
		try
		{
			return solveTensor(null);
		}
		catch (AtomNotFoundException e)
		{
			throw new RelaxationRuntimeException("Unexpected error.", e);
		}
	}
	
	@Override
	public final RelaxationSolution<Predictor> solveTensor(Molecule mol) throws AtomNotFoundException
	{
		ArrayList<Predictor> predictors = getUpdatedPredictors(mol);
		SolutionStorage storage = solveTensorHelper(predictors);
		
		return new RelaxationSolution<Predictor>(storage.predictors, storage.data, storage.rhoChi2Effective);		
	}

	private final SolutionStorage solveTensorHelper(ArrayList<Predictor> predictors) throws AtomNotFoundException
	{		
	 	if (!this.isTensorComputable())
  		throw new NotEnoughVectorsException("Number of well distributed active relaxation bonds are too small.");
	 	
		ExperimentalRelaxationData experimentalData = this.relaxationData;
		
		int numIter = NUM_RHO_REFINEMENT; 
		TensorStorage tensorStorage = null;
		for (int iter=0; iter<numIter; iter++)
		{
			//create storage for the rigid data
			ArrayList<BondRelaxation> rigidBondRelaxList = new ArrayList<BondRelaxation>(experimentalData.size());
			ArrayList<Predictor> rigidPredictors = new ArrayList<Predictor>(predictors.size());

			//extract the rigid components
			Iterator<Predictor> predIter = predictors.iterator();
			for (BondRelaxation bond : experimentalData.getBondRelaxationDataRef())
			{
				Predictor predictor = predIter.next();
				if (bond.isRigid())
				{
					rigidBondRelaxList.add(bond);
					rigidPredictors.add(predictor);
				}
			}
			ExperimentalRelaxationData rigidData = new ExperimentalRelaxationData(rigidBondRelaxList, true);
			
			//compute the tensor on the rigid components
			tensorStorage = computeTensor(rigidPredictors, rigidData, this.robust, this.outlierThreshold);
			
			//update the predictors
			ArrayList<Predictor> newPredictors = new ArrayList<Predictor>(predictors.size());
			for (Predictor predictor : predictors)
			{
				Predictor castPredictor = createPredictor(predictor.createWithTensor(tensorStorage.tensor));
				newPredictors.add(castPredictor);
			}
			
			//update the values
			predictors = newPredictors;
			experimentalData = experimentalData.createWithUpdatedRhos(predictors);
		}

		return new SolutionStorage(predictors, experimentalData, tensorStorage.rhoChi2Effective);
	}
}
