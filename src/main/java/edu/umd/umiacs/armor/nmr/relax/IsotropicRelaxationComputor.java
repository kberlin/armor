/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.LeastSquaresOptimizationResult;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OutlierDampingFunction;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BaseExperimentalData;

/**
 * The Class IsotropicRelaxationComputor.
 */
public final class IsotropicRelaxationComputor extends AbstractModelFreeComputor<IsotropicModelFreePredictor>
{	
	/**
	 * Generate predictors.
	 * 
	 * @param relaxList
	 *          the relax list
	 * @param bondConstraints
	 *          the bond constraints
	 * @return the array list
	 */
	private static ArrayList<IsotropicModelFreePredictor> generatePredictors(ExperimentalRelaxationData relaxList)
	{
		ArrayList<IsotropicModelFreePredictor> predictors = new ArrayList<IsotropicModelFreePredictor>(relaxList.size());
		for (BondRelaxation bondRelax : relaxList.getBondRelaxationDataRef())
		{
	    Bond bond = bondRelax.getBond();
	  	ModelFreeBondConstraints constraints = bondRelax.getModelFreeConstraints();
	  		  	
	  	//create predictor for isotropic tensor
	  	IsotropicSpectralDensityFunction isoJ = new IsotropicSpectralDensityFunction(
	  			RelaxationInitialOptimization.INITIAL_TUAC_GUESS, 
	  			constraints.expS2Slow, 
	  			constraints.expTauLoc,
	  			constraints.expS2Fast,
	  			constraints.expTauLocFast);
	  	IsotropicModelFreePredictor isoPredictor = new IsotropicModelFreePredictor(
	  			isoJ, bond, constraints.expCSA, bond.getBondType().getMeanRadius(), constraints.expCSA, constraints.minRex);
	  	isoPredictor = isoPredictor.createWithVector(bond.getVector());
	  	
	  	predictors.add(isoPredictor);
		}

		return predictors;
	}
	
	/**
	 * Predict rho iso.
	 * 
	 * @param tauC
	 *          the tau c
	 * @return the double[]
	 */
	public static double[] predictRhoIso(List<IsotropicModelFreePredictor> predictors, ExperimentalRelaxationData relaxData, double tauC)
	{
		double[] rho = new double[relaxData.sizeRho()];
		
		int count = 0;
		Iterator<BondRelaxation> iterBond = relaxData.getBondRelaxationDataRef().iterator();
		for (IsotropicModelFreePredictor predictor : predictors)
		{
			predictor = predictor.createWithTauC(tauC);
			for (RelaxationDatum datum : iterBond.next().getRelaxationListRef())
			{
				rho[count] = predictor.predictRho(datum.getFrequency());
				count++;
			}
		}
		
		return rho;
	}
	
	public IsotropicRelaxationComputor(ExperimentalRelaxationData relaxList)
	{
		this(relaxList, false);
	}

	protected IsotropicRelaxationComputor(ExperimentalRelaxationData relaxList, ArrayList<IsotropicModelFreePredictor> predictors, 
			 boolean optimizePredictors, boolean optimizeRho,
			boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		super(relaxList, predictors, optimizePredictors, optimizeRho, robust, outlierThreshold, absoluteDiffusionError);
	}
	
	public IsotropicRelaxationComputor(ExperimentalRelaxationData relaxList, ArrayList<IsotropicModelFreePredictor> predictors,
			boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		this(relaxList, predictors, false, true, robust, outlierThreshold, absoluteDiffusionError);
	}
	
	public IsotropicRelaxationComputor(ExperimentalRelaxationData relaxList,
			boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		this(relaxList, generatePredictors(relaxList), true, true, robust, outlierThreshold, absoluteDiffusionError);
	}
	
	public IsotropicRelaxationComputor(ExperimentalRelaxationData relaxList, boolean robust)
	{
		this(relaxList, robust, OUTLIER_THRESHOLD);
	}
	
	public IsotropicRelaxationComputor(ExperimentalRelaxationData relaxList, boolean robust, double outlierThreshold)
	{
		this(relaxList, robust, outlierThreshold, ABSOLUTE_DIFFUSION_ERROR);
	}
	
	
	@Override
	protected TensorStorage computeTensor(
			final ArrayList<IsotropicModelFreePredictor> predictors, 
			final ExperimentalRelaxationData data,
			boolean robust, 
			double outlierThreshold)
	{
		//generate the target and the associated weights
		BaseExperimentalData rhoList = data.getRhoData();
	  double[] targetValues = rhoList.values();
	  double[] errors = rhoList.errors();

	  //create the optimizers
	  MultivariateVectorFunction isotropicPredictionFunction = new MultivariateVectorFunction()
		{
			@Override
			public double[] value(double[] tauC) throws IllegalArgumentException
			{
				return predictRhoIso(predictors, data, tauC[0]*tauC[0]);
			}
		};
	  
	  //create the optimizers
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(2000);
		optimizer.setAbsPointDifference(ABSOLUTE_DIFFUSION_ERROR);
		optimizer.setErrors(errors);
		optimizer.setObservations(targetValues);
		
		MultivariateVectorFunction predictionFunction = isotropicPredictionFunction;
	  if (robust)
	  {
	  	predictionFunction = new OutlierDampingFunction(predictionFunction, targetValues, errors, outlierThreshold);
	  }
	  optimizer.setFiniteDifferenceFunction(predictionFunction);
	  
	  //create initial guess
	  double[] x0 = {BasicMath.sqrt(computeTauCApproximate(data))};
	  
	  //compute the result
	  LeastSquaresOptimizationResult result = optimizer.optimize(x0);
	  	  
		return new TensorStorage(RotationalDiffusionTensor.create(BasicMath.square(result.getPoint()[0])), result.getChiSquared());
	}
	
	public RotationalDiffusionTensor computeTensorWithRatio(Molecule mol)
	{
		final IsotropicRelaxationComputor rigidComputor = this.createRigid();
		
   	//get the predictors with the updated position
  	final ArrayList<IsotropicModelFreePredictor> predictors;
 		predictors = rigidComputor.predictors;

  	//generate the target and the associated weights
	  final R1R2Ratio isotropicPredictionFunction = new R1R2Ratio(predictors, rigidComputor.relaxationData);
	  double[] targetValues = isotropicPredictionFunction.values();
	  double[] errors = isotropicPredictionFunction.errors();
	  
  	//create the optimizers
	  MultivariateVectorFunction isotropicPredictionWrapper = new MultivariateVectorFunction()
		{			
			@Override
			public double[] value(double[] x) throws IllegalArgumentException
			{
				double xadj = BasicMath.square(x[0]);
				double[] xfull = new double[]{xadj, xadj, xadj, 0.0, 0.0, 0.0};
				return isotropicPredictionFunction.value(xfull);
			}
		};
	  
	  //create the optimizers
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(2000);
		optimizer.setAbsPointDifference(ABSOLUTE_DIFFUSION_ERROR);
		optimizer.setErrors(errors);
		optimizer.setObservations(targetValues);
		
		MultivariateVectorFunction predictionFunction = isotropicPredictionWrapper;
	  if (this.robust)
	  {
	  	predictionFunction = new OutlierDampingFunction(predictionFunction, targetValues, errors, this.outlierThreshold);
	  }
	  optimizer.setFiniteDifferenceFunction(predictionFunction);
	  
	  //create initial guess
	  double tauC = AbstractModelFreeComputor.computeTauCApproximate(rigidComputor.relaxationData);
	  double dc = RotationalDiffusionTensor.tensorPrincipalValue(tauC);	  
  	double[] x0 = new double[]{BasicMath.sqrt(dc)};
  	
	  //perform the optimization over all initial points
  	OptimizationResultImpl result = optimizer.optimize(x0);

	  double[] x = result.getPoint();
		double xadj = BasicMath.square(x[0]);
		RotationalDiffusionTensor sol = new RotationalDiffusionTensor(xadj, xadj, xadj, Rotation.identityRotation());
	  
		return sol;
	}
	
	@Override
	protected IsotropicModelFreePredictor createFromMolecule(IsotropicModelFreePredictor predictor, Molecule mol)
			throws AtomNotFoundException
	{
		return predictor.createFromMolecule(mol);
	}

	@Override
	protected IsotropicModelFreePredictor createPredictor(ModelFreePredictor predictor)
	{
		return (IsotropicModelFreePredictor)predictor;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RelaxationComputor#createRigid()
	 */
	@Override
	public final IsotropicRelaxationComputor createRigid()
	{
		if (this.relaxationData.isRigid())
			return this;

		ExperimentalRelaxationData newList = this.relaxationData.createRigid();
		
		return createSubList(newList);		
	}
	

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.AbstractModelFreeComputor#createSubList(edu.umd.umiacs.armor.nmr.relax.ExperimentalRelaxation)
	 */
	@Override
	public IsotropicRelaxationComputor createSubList(ExperimentalRelaxationData newList)
	{
		if (newList==this.relaxationData)
			return this;
		
		ArrayList<Integer> indicies = getAssociatedIndices(newList);
		ArrayList<IsotropicModelFreePredictor> predictors = getBondPredictors(indicies);
		
		return new IsotropicRelaxationComputor(newList, predictors, false, false, this.robust, this.outlierThreshold, this.absoluteDiffusionError);
	}

	@Override
	protected IsotropicModelFreePredictor createWithTensor(IsotropicModelFreePredictor predictor, RotationalDiffusionTensor tensor)
	{
		return predictor.createWithTensor(tensor);
	}

	/**
	 * Form new list array.
	 * 
	 * @param relaxLists
	 *          the relax lists
	 * @return the array list
	 */
	protected final ArrayList<IsotropicRelaxationComputor> formNewListArray(ArrayList<ExperimentalRelaxationData> relaxLists)
	{
		ArrayList<IsotropicRelaxationComputor> computorList = new ArrayList<IsotropicRelaxationComputor>(relaxLists.size());
		for (ExperimentalRelaxationData relaxList : relaxLists)
			computorList.add(createSubList(relaxList));
		
		return computorList;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.AbstractModelFreeComputor#isTensorComputable()
	 */
	@Override
	public boolean isTensorComputable()
	{
    return this.relaxationData.createRigid().size()>=1;
	}
}
