/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3d;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.Solution;

public final class RelaxationSolution<Predictor extends ModelFreePredictor> implements Solution
{
	private final ExperimentalRelaxationData experimentalData;

	private final ArrayList<Predictor> predictors;
	private final double rhoChi2Effective;
	/**
	 * 
	 */
	private static final long serialVersionUID = 5752017676940316374L;
	
	/**
	 * Compute oriental xi eigenvalues.
	 * 
	 * @param omega
	 *          the omega
	 * @return the double[]
	 */
	private static double[] computeOrientalXiEigenvalues(SymmetricMatrix3d omega)
	{
		return omega.getEigenDecomposition().getEigenvalues();		
	}
	
	/**
	 * Compute orientational xi.
	 * 
	 * @param omega
	 *          the omega
	 * @return the double
	 */
	private static double computeOrientationalXi(SymmetricMatrix3d omega)
	{
		double[][] omegaArray = omega.toArray();
		
		//sum the total
		double sum = 0.0;
		for (double[] column : omegaArray)
			for (double val : column)
				sum += val*val;
		
		return 2.0/3.0*sum;
	}
	
	
	public RelaxationSolution(List<Predictor> predictors, ExperimentalRelaxationData data, double effectiveRhoChi2)
	{
		if (predictors.size()!=data.size())
			throw new RelaxationRuntimeException("Number of predictors must match number of bonds.");
		
		this.predictors = new ArrayList<Predictor>(predictors);
		this.experimentalData = data;
		this.rhoChi2Effective = effectiveRhoChi2;
	}
	
	/**
	 * Compute oriental xi eigenvalues.
	 * 
	 * @return the double[]
	 */
	public double[] computeOrientalXiEigenvalues()
	{
		return computeOrientalXiEigenvalues(BasicStat.orientationalSamplingMeasure(getBondNorms()));
	}
	
	/**
	 * Compute orientational xi.
	 * 
	 * @return the double
	 */
	public double computeOrientationalXi()
	{
		return computeOrientationalXi(BasicStat.orientationalSamplingMeasure(getBondNorms()));
	}

	public RelaxationSolution<Predictor> createRigid()
	{
		if (this.experimentalData.isRigid())
			return this;

		return createSubList(this.experimentalData.createRigid());
	}
	
	private RelaxationSolution<Predictor> createSubList(ExperimentalRelaxationData newList)
	{
		if (newList==this.experimentalData)
			return this;
		
		ArrayList<Integer> indicies = getAssociatedIndices(newList);
		ArrayList<Predictor> newPredictors = new ArrayList<Predictor>(indicies.size());
		for (int index : indicies)
		{
			newPredictors.add(this.predictors.get(index));
		}
		
		return new RelaxationSolution<Predictor>(newPredictors, newList, this.rhoChi2Effective);
	}
	
	private ArrayList<RelaxationSolution<Predictor>> createSubLists(ArrayList<ExperimentalRelaxationData> experimentalList)
	{
		ArrayList<RelaxationSolution<Predictor>> newLists = new ArrayList<RelaxationSolution<Predictor>>(experimentalList.size());
		for (ExperimentalRelaxationData currData : experimentalList)
			newLists.add(createSubList(currData));
		
		return newLists;
	}
	
	@Override
	public double[] errors()
	{
		return this.experimentalData.getRhoData().errors();
	}
	
	private final ArrayList<Integer> getAssociatedIndices(ExperimentalRelaxationData list)
	{
		ArrayList<Integer> indices = new ArrayList<Integer>(list.size());
		ArrayList<Bond> bonds = this.experimentalData.getBonds();
		for (BondRelaxation bondRelax : list.getBondRelaxationDataRef())
		{			
			int index = bonds.indexOf(bondRelax.getBond());
			indices.add(index);
		}
		
		return indices;
	}
	
	public ArrayList<Point3d> getBondNorms()
	{
		ArrayList<Point3d> bonds = new ArrayList<Point3d>(this.predictors.size());
		for (Predictor predictor : this.predictors)
		{
			bonds.add(predictor.getBond().getNormVector());
		}
		
		return bonds;
	}
		
	public Predictor getPredictor(int index)
	{
		return this.predictors.get(index);
	}
	
	public ArrayList<Predictor> getPredictors()
	{
		return new ArrayList<Predictor>(this.predictors);
	}
	
	public ExperimentalRelaxationData getRelaxationData()
	{
		return this.experimentalData;
	}
	
	public RotationalDiffusionTensor getTensor()
	{
		return this.predictors.get(0).getTensor();
	}

	@Override
	public double[] predict()
	{
		return predictRho();
	}
	
	public double[] predictObservations(int index)
	{
		BondRelaxation data = this.experimentalData.getBondRelaxation(index);
		double[] prediction = new double[data.sizeObservations()];
		Predictor predictor = getPredictor(index);
		
		int count = 0;
		for (RelaxationDatum datum : data.getRelaxationList())
		{
			double[] currPrediction = predictor.predict(datum.getFrequency(), datum);
			for (int iter=0; iter<currPrediction.length; iter++)
			{
				prediction[count] = currPrediction[iter];
				count++;
			}
		}
		
		return prediction;
	}
	
	public double[] predictRho()
	{
		double[] rho = new double[this.experimentalData.sizeRho()];
		
		int count = 0;
		Iterator<Predictor> predictorIterator = this.predictors.iterator();
		for (BondRelaxation bondRelaxation : this.experimentalData.getBondRelaxationDataRef())
		{
			Predictor predictor = predictorIterator.next();
			for (RelaxationDatum datum : bondRelaxation.getRelaxationListRef())
			{
				rho[count] = predictor.predictRho(datum.getFrequency());
				count++;
			}
		}
		
		return rho;
	}

	private double[] predictRho(RotationalDiffusionTensor tensor)
	{
		double[] pred = new double[this.experimentalData.sizeRho()];
		int count = 0;
		int bondIndex = 0;
		for (BondRelaxation bondRelaxation : this.experimentalData.getBondRelaxationDataRef())
		{
			RelaxationRatePredictor predictor = this.predictors.get(bondIndex).createWithTensor(tensor);
			for (RelaxationDatum datum : bondRelaxation.getRelaxationListRef())
			{
				pred[count] = predictor.predictRho(datum.getFrequency());
				
				count++;
			}
			
			bondIndex++;
		}
		
		return pred;
	}
	
	public double rhoChi2()
	{
		double[] rhoPredicted = predictRho();
		BaseExperimentalData data = this.experimentalData.getRhoData();
		double[] expValues = data.values();
		double[] expErrors = data.errors();
		
		return BasicStat.chi2(rhoPredicted, expValues, expErrors);
	}
	
	public double chi2Observations(int index)
	{
		BondRelaxation bondData = this.experimentalData.getBondRelaxation(index);
		double[] expValues = bondData.getObservedData().values();
		double[] expErrors = bondData.getObservedData().errors();
		double[] prediction = predictObservations(index);
		
		return BasicStat.chi2(prediction, expValues, expErrors);
	}
	
	public double rhoChi2(RotationalDiffusionTensor tensor)
	{
		double[] rhoPredicted = predictRho(tensor);
		BaseExperimentalData data = this.experimentalData.getRhoData();
		double[] expValues = data.values();
		double[] expErrors = data.errors();
		
		return BasicStat.chi2(rhoPredicted, expValues, expErrors);
	}

	public double rhoChi2Effective()
	{
		return this.rhoChi2Effective;
	}

	public double rhoPearsonsCorrelation()
	{
		double[] rhoPredicted = predictRho();
		BaseExperimentalData data = this.experimentalData.getRhoData();
		double[] expValues = data.values();
		
		return  BasicStat.pearsonsCorrelation(rhoPredicted, expValues);
	}

	public double rhoQualityFactor()
	{
		double[] rhoPredicted = predictRho();
		BaseExperimentalData data = this.experimentalData.getRhoData();
		double[] expValues = data.values();
		//double[] expError = ExperimentalDatum.getErrors(data);

		return  BasicStat.rotdifQualityFactor(rhoPredicted, expValues);
	}

	public double[] rhoResiduals()
	{
		double[] rhoPredicted = predictRho();
		BaseExperimentalData data = this.experimentalData.getRhoData();
		double[] expValues = data.values();
		double[] expErrors = data.errors();
		
		return BasicStat.residuals(rhoPredicted, expValues, expErrors);
	}

	public ArrayList<RelaxationSolution<Predictor>> seperateByBondType()
	{
		return createSubLists(this.experimentalData.seperateByBondType());
	}

	public ArrayList<RelaxationSolution<Predictor>> seperateByBondTypeAndFrequency()
	{
		return createSubLists(this.experimentalData.seperateByBondTypeAndFrequency());
	}

	public ArrayList<RelaxationSolution<Predictor>> seperateByChain()
	{
		return createSubLists(this.experimentalData.seperateByChain());
	}

	public int size()
	{
		return this.predictors.size();
	}
	
	public int sizeRho()
	{
		return this.experimentalData.sizeRho();
	}

	public String toDynamicsResults(List<RelaxationSolution<Predictor>> monteCarloSolutions)
	{
		StringBuilder str = new StringBuilder();
		
		str.append("<residue><chain><atom1><atom2><freq><csa><s2><s2_err><tau_loc><Rex><s2_fast><tau_fast><chi2>\n");
		
    int predictorIndex = 0;
	  for (BondRelaxation bondRelax : getRelaxationData().getBondRelaxationData())
	  {
	  	Predictor predictor = getPredictor(predictorIndex);
	  	
	  	double[] s2var = new double[monteCarloSolutions.size()];
	  	int iter=0; 
	  	for (RelaxationSolution<Predictor> mSol : monteCarloSolutions)
	  	{
	  		Predictor montePredictor = mSol.getPredictor(predictorIndex);	  		
	  		s2var[iter] = montePredictor.getS2();
	  				
	  		iter++;
	  	}
	  	
	  	double s2std = BasicStat.std(s2var, predictor.getS2());
	  	
	  	for (RelaxationDatum datum : bondRelax.getRelaxationListRef())
	  	{
	  		str.append(String.format("% 4d %s %-5s %-5s %7.2f %7.2f %5.3f %5.3f %8.2e %6.3f %5.3f %8.2e %8.2e\n", 
	  																																					bondRelax.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
	  																																					bondRelax.getBond().getFromAtom().getPrimaryInfo().getChainID(),
	  																																					bondRelax.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
	  																																					bondRelax.getBond().getToAtom().getPrimaryInfo().getAtomName(),
	  																																					datum.getFrequency(),
	  																																					predictor.getCSA(),
	  																																					predictor.getS2(),
	  																																					s2std,
	  																																					predictor.getTauLocal(),
	  																																					ModelFreePredictorImpl.rexAbsolute(predictor.getRex(), datum.getFrequency()),
	  																																					predictor.getS2Fast(),
	  																																					predictor.getTauFast(),
	  																																					chi2Observations(predictorIndex)));
	  																															
	  		
	  	}
	  	
	  	predictorIndex++;
	  }
		
	  return str.toString();
	}
	
	public String toRotdifResults()
	{
		StringBuilder str = new StringBuilder();
		
		str.append("<residue><chain><atom1><atom2><freq><rho_exp><rho_pred><rho_error>\n");
	  
		//compute the predicted rhos
		double[] predictedRho = predictRho();

  	int index = 0;
	  for (BondRelaxation bondRelax : getRelaxationData().getBondRelaxationData())
	  {
	  	int freqIndex = 0;
	  	ArrayList<BaseExperimentalDatum> rhoData = bondRelax.getRhoData();
	  	for (RelaxationDatum datum : bondRelax.getRelaxationListRef())
	  	{
	  		str.append(String.format("% 4d %s %-5s %-5s %7.2f %7.3f %7.3f %7.3f\n", bondRelax.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
	  																																					bondRelax.getBond().getFromAtom().getPrimaryInfo().getChainID(),
	  																																					bondRelax.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
	  																																					bondRelax.getBond().getToAtom().getPrimaryInfo().getAtomName(),
	  																																					datum.getFrequency(),
	  																																					rhoData.get(freqIndex).value(),
	  																																					predictedRho[index],
	  																																					rhoData.get(freqIndex).error()));
	  		
	  		freqIndex++;
	  		index++;
	  	}
	  }
		
	  return str.toString();
	}
	
	@Override
	public double[] values()
	{
		BaseExperimentalData data = this.experimentalData.getRhoData();
		return data.values();
	}

	@Override
	public BaseExperimentalData getExperimentalData()
	{
		return getExperimentalData();
	}
}
