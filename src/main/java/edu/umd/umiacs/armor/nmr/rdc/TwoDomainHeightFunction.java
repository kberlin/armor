/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;

/**
 * The Class TwoDomainHeightFunction.
 */
public final class TwoDomainHeightFunction implements MoleculeHeightFunction, Serializable
{
	/** The combined height function. */
	private final MoleculeHeightFunction combinedHeightFunction; 
	
	private final MultiDomainComplex complex;

	/**
	 * 
	 */
	private static final long serialVersionUID = -829006279578523498L;

	public TwoDomainHeightFunction(Molecule m1, Molecule m2)
	{
		this(new MultiDomainComplex(m1, m2));
	}
	
	private TwoDomainHeightFunction(MultiDomainComplex complex)
	{
		this.complex = complex;
		this.combinedHeightFunction = new QhullHeightFunction(this.complex);
	}
			
	/**
	 * Creates the transformed.
	 * 
	 * @param transform
	 *          the transform
	 * @return the two domain height function
	 */
	public TwoDomainHeightFunction createTransformed(RigidTransform transform)
	{
		//in principle we can resuise the convex hulls, but to simply code it was removed
		
		return new TwoDomainHeightFunction(this.complex.createSecondDomainTransformed(transform));
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.func.TwoVariableFunction#value(double, double)
	 */
	@Override
	public double value(double alpha, double u)
	{
		return this.combinedHeightFunction.value(alpha,u);
	}
}
