/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;

public final class IsotropicModelFreePredictor implements ModelFreePredictor
{
	private final ModelFreePredictorImpl<IsotropicSpectralDensityFunction> predictor;

	/**
	 * 
	 */
	private static final long serialVersionUID = -7895560173834475130L;
	
	public IsotropicModelFreePredictor(IsotropicSpectralDensityFunction J, Bond bond, double CSA, double radius, double csaAngle, double Rex)
	{
		this.predictor = new ModelFreePredictorImpl<IsotropicSpectralDensityFunction>(J, bond, CSA, radius, csaAngle, Rex, 0);
	}
	
	public IsotropicModelFreePredictor(ModelFreePredictorImpl<IsotropicSpectralDensityFunction> predictor)
	{
		this.predictor = predictor;
	}

	@Override
	public IsotropicModelFreePredictor createFromMolecule(Molecule mol) throws AtomNotFoundException
	{
		return new IsotropicModelFreePredictor(this.predictor.createFromMolecule(mol));
	}

	@Override
	public IsotropicModelFreePredictor createWithCSA(double CSA)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithCSA(CSA));
	}

	@Override
	public IsotropicModelFreePredictor createWithCsaAngle(double csaAngle)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithCsaAngle(csaAngle));
	}


	@Override
	public IsotropicModelFreePredictor createWithLocal(double S2Slow, double tauLocal, double S2Fast, double tauLocalFast)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithLocal(S2Slow, tauLocal, S2Fast, tauLocalFast));
	}

	@Override
	public IsotropicModelFreePredictor createWithModel(int model)
	{
	  return new IsotropicModelFreePredictor(this.predictor.createWithModel(model));
	}

	@Override
	public IsotropicModelFreePredictor createWithParameters(double CSA, double r, double csaAngle, double Rex)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithParameters(CSA, r, csaAngle, Rex));
	}
	
	@Override
	public <T extends ModelFreePredictor> ModelFreePredictor createWithParametersFrom(T predictor)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithParametersFrom(predictor));
	}
	
	@Override
	public IsotropicModelFreePredictor createWithRadius(double r)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithRadius(r));
	}


	@Override
	public IsotropicModelFreePredictor createWithRex(double Rex)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithRex(Rex));
	}

	@Override
	public IsotropicModelFreePredictor createWithS2Fast(double S2Fast)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithS2Fast(S2Fast));
	}

	@Override
	public IsotropicModelFreePredictor createWithS2Slow(double S2)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithS2Slow(S2));
	}

	public IsotropicModelFreePredictor createWithTauC(double tauC)
	{
		return new IsotropicModelFreePredictor(new ModelFreePredictorImpl<IsotropicSpectralDensityFunction>(
				this.predictor, this.predictor.j.createWithTauC(tauC), this.predictor.model)); 
	}

	@Override
	public IsotropicModelFreePredictor createWithTauLocal(double tauLocal)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithTauLocal(tauLocal));
	}
	
	@Override
	public IsotropicModelFreePredictor createWithTauLocalFast(double tauLocalFast)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithTauLocalFast(tauLocalFast));
	}
	
	@Override
	public IsotropicModelFreePredictor createWithTensor(RotationalDiffusionTensor tensor)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithTensor(tensor));
	}
	
	@Override
	public IsotropicModelFreePredictor createWithVector(Point3d bondVector)
	{
		return new IsotropicModelFreePredictor(this.predictor.createWithVector(bondVector));
	}

	public RelaxationDatum derivativeRex(double frequency)
	{
		return this.predictor.derivativeRex(frequency);
	}

	@Override
	public Bond getBond()
	{
		return this.predictor.getBond();
	}

	@Override
	public double getCSA()
	{
		return this.predictor.getCSA();
	}

	@Override
	public double getCsaAngle()
	{
		return this.predictor.getCsaAngle();
	}

	@Override
	public double getDipolarConstant()
	{
		return this.predictor.getDipolarConstant();
	}

	@Override
	public double getGammaFromAtom()
	{
		return this.predictor.getGammaFromAtom();
	}

	@Override
	public double getGammaToAtom()
	{
		return this.predictor.getGammaToAtom();
	}

	@Override
	public int getModel()
	{
		return this.predictor.getModel();
	}

	@Override
	public ModelFreeBondConstraints getModelFreeBondConstraint()
	{
		return this.predictor.constraint;
	}

	@Override
	public double getRadius()
	{
		return this.predictor.getRadius();
	}

	@Override
	public double getRex()
	{
		return this.predictor.getRex();
	}

	@Override
	public double getS2()
	{
		return this.predictor.getS2();
	}

	@Override
	public double getS2Fast()
	{
		return this.predictor.getS2Fast();
	}

	@Override
	public double getS2Slow()
	{
		return this.predictor.getS2Slow();
	}

	@Override
	public IsotropicSpectralDensityFunction getSpectralDensityFunction()
	{
		return this.predictor.getSpectralDensityFunction();
	}

	@Override
	public double getTauC()
	{
		return this.predictor.getTauC();
	}

	@Override
	public double getTauFast()
	{
		return this.predictor.getTauFast();
	}

	@Override
	public double getTauLocal()
	{
		return this.predictor.getTauLocal();
	}

	@Override
	public RotationalDiffusionTensor getTensor()
	{
		return this.predictor.getTensor();
	}

	@Override
	public RelaxationDatum predict(double frequency)
	{
		return this.predictor.predict(frequency);
	}

	@Override
	public double[] predict(double frequency, RelaxationDatum datum)
	{
		return this.predictor.predict(frequency, datum);
	}

	@Override
	public double predictEtaXy(double frequency)
	{
		return this.predictor.predictEtaXy(frequency);
	}

	@Override
	public double predictEtaZ(double frequency)
	{
		return this.predictor.predictEtaZ(frequency);
	}

	@Override
	public double predictNOE(double frequency)
	{
		return this.predictor.predictNOE(frequency);
	}

	@Override
	public double predictR1(double frequency)
	{
		return this.predictor.predictR1(frequency);
	}

	@Override
	public double predictR2(double frequency)
	{
		return this.predictor.predictR2(frequency);
	}

	@Override
	public double predictRho(double frequency)
	{
		return this.predictor.predictRho(frequency);
	}

	@Override
	public double predictRhoEta(double frequency)
	{
		return this.predictor.predictRhoEta(frequency);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return this.predictor.toString();
	}
}
