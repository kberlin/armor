/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.func.UnivariateFunction;
import edu.umd.umiacs.armor.math.integration.LegendreGaussIntegrator;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResult;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.physics.BasicPhysics;
import edu.umd.umiacs.armor.util.BasicUtils;

/**
 * The Class PerrinEquation.
 */
public final class PerrinEquation implements MultivariateVectorFunction
{
	/** The temperature adjustment. */
	private final double scalingFactor;
	
	/**
	 * Instantiates a new perrin equation.
	 * 
	 * @param temperatureKelvin
	 *          the temperature kelvin
	 */
	public PerrinEquation(double temperatureKelvin)
	{
		//convert to celsius
	  double tCelsius = (temperatureKelvin-273.15);

	  //compute temperature adjustment 
		double viscosityOfWater = ((1.7753)-(0.0565)*tCelsius
				+(1.0751e-3)*tCelsius*tCelsius
				-(9.2222e-6)*tCelsius*tCelsius*tCelsius)*1.0e-3;
		
		this.scalingFactor = 1.0e-7*(1.0e30*BasicPhysics.kB)*temperatureKelvin*3.0/(16.0*BasicMath.PI*viscosityOfWater);
	}
	
	/**
	 * Invert.
	 * 
	 * @param d1
	 *          the d1
	 * @param d2
	 *          the d2
	 * @param d3
	 *          the d3
	 * @return the double[]
	 */
	public double[] invert(double d1, double d2, double d3)
	{
		
		double[] target = {d1, d2, d3};
		double[] errors = {1.0, 1.0, 1.0};
		
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(5000);
		optimizer.setAbsPointDifference(1.0e-6);
		optimizer.setObservations(target);
		optimizer.setErrors(errors);
		
		MultivariateVectorFunction squareFunc = new MultivariateVectorFunction()
		{
			@Override
			public double[] value(double[] x) throws IllegalArgumentException
			{
				return PerrinEquation.this.value(BasicMath.square(x[0]), BasicMath.square(x[1]), BasicMath.square(x[2]));
			}
		};
		
		optimizer.setFiniteDifferenceFunction(squareFunc);
		
		double[] xList = BasicUtils.uniformLinePoints(5.0, 10.0, 1);
		double[] yList = BasicUtils.uniformLinePoints(5.0, 10.0, 1);
		double[] zList = BasicUtils.uniformLinePoints(5.0, 10.0, 1);
		
		ArrayList<OptimizationResultImpl> solutions = new ArrayList<OptimizationResultImpl>();
		for (double x : xList)
			for (double y : yList)
				for (double z : zList)
				{
					solutions.add(optimizer.optimize(new double[]{BasicMath.sqrt(x), BasicMath.sqrt(y), BasicMath.sqrt(z)}));
				}
		
		OptimizationResult sol = OptimizationResultImpl.getBestSolution(solutions);
		
		double[] xsol = sol.getPoint();
		xsol[0] = BasicMath.square(xsol[0]);
		xsol[1] = BasicMath.square(xsol[1]);
		xsol[2] = BasicMath.square(xsol[2]);
		
		return xsol;
	}
	
	/**
	 * Invert.
	 * 
	 * @param d
	 *          the d
	 * @return the double[]
	 */
	public double[] invert(double[] d)
	{
		return invert(d[0], d[1], d[2]);
	}
	
	/**
	 * Invert to covariance.
	 * 
	 * @param d1
	 *          the d1
	 * @param d2
	 *          the d2
	 * @param d3
	 *          the d3
	 * @return the double[]
	 */
	public double[] invertToCovariance(double d1, double d2, double d3)
	{
		double[] lengths = invert(d1, d2, d3);

		lengths[0] = Math.pow(lengths[0], 2.0/3.0);
		lengths[1] = Math.pow(lengths[1], 2.0/3.0);
		lengths[2] = Math.pow(lengths[2], 2.0/3.0);

		return lengths;
	}
	
	/**
	 * Length squared to perrin.
	 * 
	 * @param l1s
	 *          the l1s
	 * @param l2s
	 *          the l2s
	 * @param l3s
	 *          the l3s
	 * @return the double
	 */
	private double lengthSquaredToPerrin(final double l1s, final double l2s, final double l3s)
	{
		final UnivariateFunction properFunc = new UnivariateFunction()
		{
			@Override
			public double value(double x)
			{				
				//compute 1.0/BasicMath.sqrt(BasicMath.cube(l1s+x)*(l2s+x)*(l3s+x));
				
				double sl1 = BasicMath.sqrt(l1s+x);
				double sl2 = BasicMath.sqrt(l2s+x);
				double sl3 = BasicMath.sqrt(l2s+x);
				
				double val = sl1*sl1*sl1*(sl2*sl3);

				return 1.0/val;
			}
		};
		
		final UnivariateFunction improperFunc = new UnivariateFunction()
		{			
			@Override
			public double value(double u)
			{
				double uInv = 1.0/u;
				double adjustedValue = properFunc.value(uInv)*uInv*uInv;
				 
				return adjustedValue;
			}
		};
		
		LegendreGaussIntegrator integrator = new LegendreGaussIntegrator(5, 1.0e-8, 1.0e-8);

		//integrate normally from 0 to 1
		double val = integrator.integrate(properFunc, 0.0, 1.0, 300000);
		
		//integrate from 1 to infinity via a change of variable u = 1/x
		val += integrator.integrate(improperFunc, 1.0e-7, 1.0, 300000);
		
		return val;
	}
	
	/**
	 * Value.
	 * 
	 * @param l1
	 *          the l1
	 * @param l2
	 *          the l2
	 * @param l3
	 *          the l3
	 * @return the double[]
	 */
	public double[] value(double l1, double l2, double l3)
	{
		//square the number
		double l1s = l1*l1;
		double l2s = l2*l2;
		double l3s = l3*l3;

		//compute integrals
		double q1 = lengthSquaredToPerrin(l1s,l2s,l3s);
		double q2 = lengthSquaredToPerrin(l2s,l3s,l1s);
		double q3 = lengthSquaredToPerrin(l3s,l1s,l2s);
		
		double f1 = (l2s*q2 + l3s*q3)/(l2s+l3s);
		double f2 = (l3s*q3 + l1s*q1)/(l3s+l1s);
		double f3 = (l1s*q1 + l2s*q2)/(l1s+l2s);
				
		return new double[]{f1*this.scalingFactor, f2*this.scalingFactor, f3*this.scalingFactor};
	}
	
	/* (non-Javadoc)
	 * @see org.apache.commons.math3.analysis.MultivariateVectorFunction#value(double[])
	 */
	@Override
	public double[] value(double[] lengths) throws IllegalArgumentException
	{
		return value(lengths[0], lengths[1], lengths[2]);
	}
	
}