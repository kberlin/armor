/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Interface ModelFreePredictor.
 * 
 * @param <E>
 *          the element type
 */
public interface ModelFreePredictor extends RelaxationRatePredictor, Serializable
{
	@Override
	public ModelFreePredictor createWithCSA(double CSA);
	
	@Override
	public ModelFreePredictor createWithCsaAngle(double csaAngle);
	
	public ModelFreePredictor createWithLocal(double S2Slow, double tauLocal, double S2Fast, double tauLocalFast);

	@Override
	public ModelFreePredictor createWithParameters(double CSA, double r, double csaAngle, double Rex);
	
	public <T extends ModelFreePredictor> ModelFreePredictor createWithParametersFrom(T predictor);
	
	@Override
	public ModelFreePredictor createWithRex(double Rex);
	
	public ModelFreePredictor createWithS2Fast(double S2Fast);
	
	public ModelFreePredictor createWithS2Slow(double S2Slow);

	public ModelFreePredictor createWithTauLocal(double tauLocal);
	
	public ModelFreePredictor createWithTauLocalFast(double tauLocalFast);

	@Override
	public ModelFreePredictor createWithTensor(RotationalDiffusionTensor tensor);
	
	@Override
	public ModelFreePredictor createWithVector(Point3d bondVector);
	
	public ModelFreePredictor createWithModel(int model);
	
	public double getDipolarConstant();
	
	public int getModel();
	
	public ModelFreeBondConstraints getModelFreeBondConstraint();
	
	public double getS2();
	
	public double getS2Fast();

	public double getS2Slow();

	public MFSpectralDensityFunction<?> getSpectralDensityFunction();
	
	public double getTauC();
	
	public double getTauFast();
	
	public double getTauLocal();

	public RotationalDiffusionTensor getTensor();
}