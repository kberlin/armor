/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.util.ArrayList;
import java.util.Iterator;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3dImpl;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OutlierDampingFunction;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.ExperimentComputor;

/**
 * The Class AlignmentComputor.
 */
public final class AlignmentComputor implements ExperimentComputor<RdcSolution>
{
	private final double outlierThreshold;
	
	/** The predictors. */
	private final ArrayList<? extends RdcPredictor> predictors;
	
	/** The experimental data. */
	private final ExperimentalRdcData rdcData;
	
	private final boolean robust;

	public static final double OUTLIER_THRESHOLD = 3.0; //3.0 

	/**
	 * Compute alignment tensor.
	 * 
	 * @param M
	 *          the m
	 * @param b
	 *          the b
	 * @param errors
	 *          the errors
	 * @return the alignment tensor
	 */
	private static AlignmentTensor computeAlignmentTensor(final double[][] M, double[] b, double[] errors, boolean robust, double outlierThreshold)
	{
		if (M.length < 5)
			throw new RdcRuntimeException("Not enough elements to compute the alignment tensor.");
		
		for (int iter=0; iter<M.length; iter++)
		{
			if (BaseExperimentalDatum.isValue(errors[iter]))
			{
				b[iter] = b[iter]/errors[iter];
				for (int iterM=0; iterM<M[iter].length; iterM++)
					M[iter][iterM] = M[iter][iterM]/errors[iter];
			}
		}
		
		//solve the inverse problem
		double[] x = BasicMath.solveSVD(M, b, 1.0e-8);
		
		if (robust)
		{
			LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
			optimizer.setNumberEvaluations(3000);
			optimizer.setAbsPointDifference(1.0e-8);
			
			MultivariateVectorFunction func = new MultivariateVectorFunction()
			{
				@Override
				public double[] value(double[] x) throws IllegalArgumentException
				{
					return BasicMath.mult(M, x);
				}
			};
			
			errors = BasicUtils.createArray(b.length,1.0);
			func = new OutlierDampingFunction(func, b, errors, outlierThreshold);
			
			optimizer.setFiniteDifferenceFunction(func);
			optimizer.setObservations(b);
			optimizer.setErrors(errors);
			
			OptimizationResultImpl sol = optimizer.optimize(x);
			
			x = sol.getPoint();
		}
		
		double[][] A = new double[3][3];
		A[0][0]=x[0]; A[0][1]=x[1]; A[0][2]=x[3];
		A[1][0]=x[1]; A[1][1]=x[2]; A[1][2]=x[4]; 
		A[2][0]=x[3]; A[2][1]=x[4]; A[2][2]=-x[0]-x[2];
		
		return new AlignmentTensor(new SymmetricMatrix3dImpl(A, false));		
	}

	private static double[][] getM(ArrayList<? extends RdcPredictor> predictors)
	{
		double[][] M = new double[predictors.size()][5];
		
		for (int iter=0; iter<predictors.size(); iter++)
		{
			RdcPredictor predictor = predictors.get(iter);
			Point3d r = predictor.getBond().getNormVector();
			
		  M[iter][0] = r.x*r.x-r.z*r.z; 
		  M[iter][1] = 2.0*r.x*r.y; 
		  M[iter][2] = r.y*r.y-r.z*r.z;
		  M[iter][3] = 2.0*r.x*r.z;
		  M[iter][4] = 2.0*r.y*r.z;

		  //scale by the scaling constant
		  double c = predictor.getScalingConstant();
		  for (int columnIter=0; columnIter<M[iter].length; columnIter++)
		  	M[iter][columnIter] = M[iter][columnIter]*c;
		  
		}

		return M;
	}

	private static RdcPredictor updatePredictor(Molecule mol, RdcPredictor predictor) throws AtomNotFoundException
	{
		Point3d p1 = mol.getAtomPosition(predictor.getBond().getFromAtom());
		Point3d p2 = mol.getAtomPosition(predictor.getBond().getToAtom());
		
		return predictor.createWithNewVector(p2.subtract(p1));
	}
	
	private static ArrayList<RdcPredictor> updatePredictors(AlignmentTensor tensor, ArrayList<? extends RdcPredictor> predictors) throws AtomNotFoundException
	{
		ArrayList<RdcPredictor> newPredictors = new ArrayList<RdcPredictor>(predictors.size());
		for (RdcPredictor predictor : predictors)
		{
			newPredictors.add(predictor.createWithNewTensor(tensor));
		}
		
		return newPredictors;
	}
	
	private static ArrayList<RdcPredictor> updatePredictors(Molecule mol, ArrayList<? extends RdcPredictor> predictors) throws AtomNotFoundException
	{
		ArrayList<RdcPredictor> newPredictors = new ArrayList<RdcPredictor>(predictors.size());
		for (RdcPredictor predictor : predictors)
		{
			newPredictors.add(updatePredictor(mol, predictor));
		}
		
		return newPredictors;
	}

	public AlignmentComputor(ExperimentalRdcData experimentalData)
	{
		this(experimentalData, false, 0.0);
	}
	
	private AlignmentComputor(ExperimentalRdcData rdcData, ArrayList<? extends RdcPredictor> predictors, boolean directCopy, boolean robust, double outlierThreshold)
	{
		this.robust = robust;
		this.outlierThreshold = outlierThreshold;
		this.rdcData = rdcData;
		
		if (directCopy)
		{
			this.predictors = predictors;
		}
		else
		{
			ArrayList<RdcPredictor> newPredictors = new ArrayList<RdcPredictor>();
			Iterator<RdcDatum> couplingIterator = rdcData.getDataRef().iterator();
			for (RdcPredictor predictor : predictors)
				newPredictors.add(predictor.createWithNewVector(couplingIterator.next().getBond().getVector()));
			
			this.predictors = newPredictors;
		}
	}
	
	public AlignmentComputor(ExperimentalRdcData couplingList, ArrayList<? extends RdcPredictor> predictors, boolean robust, double outlierThreshold)
	{
		this(couplingList, predictors, false, robust, outlierThreshold);
	}

	public AlignmentComputor(ExperimentalRdcData experimentalData, boolean robust)
	{
		this(experimentalData, robust, OUTLIER_THRESHOLD);
	}
	
	public AlignmentComputor(ExperimentalRdcData experimentalData, boolean robust, double outlierThreshold)
	{
		this.robust = robust;
		this.outlierThreshold = outlierThreshold;
		this.rdcData = experimentalData;
		
		ArrayList<RdcPredictorImpl> predictors = new ArrayList<RdcPredictorImpl>(experimentalData.size());
		for (RdcDatum rdc : experimentalData.getDataRef())
		{
			predictors.add(new RdcPredictorImpl(AlignmentTensor.create(1.0, -1.0, 0.0, Rotation.identityRotation()), rdc.getBond()));
		}
		
		this.predictors = predictors;
	}
	
	private AlignmentTensor computeAlignmentTensor(ArrayList<RdcPredictor> predictors)
	{
		//compute the alignment tensor only on the rigid components
		ArrayList<RdcPredictor> rigidPredictors = new ArrayList<RdcPredictor>(predictors.size());
		ArrayList<RdcDatum> rigidData = new ArrayList<RdcDatum>(predictors.size());
		int count = 0;
		for (RdcPredictor predictor : predictors)
		{
			if (this.rdcData.get(count).isRigid())
			{
				rigidPredictors.add(predictor);
				rigidData.add(this.rdcData.get(count));
			}
			
			count++;
		}
		
		ExperimentalRdcData rigidRdcData = new ExperimentalRdcData(rigidData);
		
		double[][] M = getM(rigidPredictors);		
		double[] b = rigidRdcData.values();
		double[] errors = rigidRdcData.errors();

		return computeAlignmentTensor(M, b, errors, this.robust, this.outlierThreshold);
	}
	
	public AlignmentTensor computeTensor(Molecule mol) throws AtomNotFoundException
	{
		return computeAlignmentTensor(updatePredictors(mol, this.predictors));
	}

	public AlignmentComputor createSubList(ExperimentalRdcData data)
	{
		ArrayList<Bond> bondList = this.rdcData.getBonds();
		
		ArrayList<RdcDatum> rdcList = new ArrayList<RdcDatum>(data.size());
		ArrayList<RdcPredictor> newPredictors = new ArrayList<RdcPredictor>(data.size());
		
		for (RdcDatum rdc : data.getDataRef())
		{
			int index = bondList.indexOf(rdc.getBond());
			if (index>=0)
			{
				rdcList.add(this.rdcData.getDataRef().get(index));
				newPredictors.add(this.predictors.get(index).createWithNewVector(rdc.getBond().getVector()));
			}
		}
		
		if (rdcList.size()<=0)
			throw new RdcRuntimeException("RDC must have experimental data.");
				
		return new AlignmentComputor(new ExperimentalRdcData(rdcList), newPredictors, true, this.robust, this.outlierThreshold);
	}
	
	public RealMatrix getScaledVectorMatrix(Molecule mol) throws AtomNotFoundException
	{
		return new Array2dRealMatrix(getM(updatePredictors(mol, this.predictors)), false);
	}

	@Override
	public ExperimentalRdcData getData()
	{
		return this.rdcData;
	}
	
	/**
	 * @return the outlierThreshold
	 */
	public double getOutlierThreshold()
	{
		return this.outlierThreshold;
	}
	
	/**
	 * @return the robust
	 */
	public boolean isRobust()
	{
		return this.robust;
	}

	public double[] predict(Molecule mol, AlignmentTensor tensor) throws AtomNotFoundException
	{
		double[] b = new double[this.rdcData.size()];
		
		int count = 0;
		for (RdcPredictor predictor : this.predictors)
		{
		  b[count] = predictor.predict(mol, tensor);
		  count++;
		}
		
		return b;
	}

	public double predict(Molecule mol, AlignmentTensor tensor, int index) throws AtomNotFoundException
	{
		return this.predictors.get(index).predict(mol, tensor);
	}

	@Override
	public RdcSolution solve(Molecule mol) throws AtomNotFoundException
	{
		ArrayList<RdcPredictor> newPredictors = updatePredictors(mol, this.predictors);
		AlignmentTensor tensor = computeAlignmentTensor(newPredictors);
		newPredictors = updatePredictors(tensor, newPredictors);
		
		return new RdcSolution(newPredictors, this.rdcData);
	}
}
