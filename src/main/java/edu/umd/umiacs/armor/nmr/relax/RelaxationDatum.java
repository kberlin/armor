/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.io.Serializable;
import java.util.ArrayList;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.physics.BasicPhysics;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.NonNegativeExperimentalDatum;

/**
 * The Class RelaxationDatum.
 */
public final class RelaxationDatum implements Serializable
{
  private final Bond bond;
	
	private final ModelFreeBondConstraints constraints;

  private final double freq; //in Mhz
  
  private final NonNegativeExperimentalDatum rho;
  
  private final boolean rigid;
  
  private final int validDataSize; 
  
  private final BaseExperimentalData values;
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 2782539827395971644L;
  
  /**
	 * Compute rho.
	 * 
	 * @param datum
	 *          the datum
	 * @param currPredictor
	 *          the curr predictor
	 * @return the experimental datum
	 */
  public final static NonNegativeExperimentalDatum computeRho(RelaxationDatum datum, ModelFreePredictor currPredictor)
  {
  	//if the required values don't exist return NaN
  	if (datum.getR1().isNaN() || datum.getR2().isNaN())
  		return new NonNegativeExperimentalDatum(Double.NaN, Double.NaN);
  	
  	//make sure order parameter is within rigid range
  	if (currPredictor.getS2()<0.85)
  		currPredictor = currPredictor.createWithLocal(0.85, 0.0, 1.0, 0.0);
  	
    //currPredictor = currPredictor.createWithTensor(RotationalDiffusionTensor.create(10.0));
  	
    //compute the gamma values
    double gammaP = currPredictor.getGammaFromAtom();
    double gammaQ = currPredictor.getGammaToAtom();
    double gammaRatio = gammaP/gammaQ;

    //get the larmor frequencies
		double omegaP = BasicPhysics.larmorFrequency(datum.getFrequency(), gammaP);
		double omegaQ = BasicPhysics.larmorFrequency(datum.getFrequency(), gammaQ);

		//compute the spectral density functions
		double Jq = currPredictor.getSpectralDensityFunction().valueOrdered(omegaQ);
  	double Jq_plus_p = currPredictor.getSpectralDensityFunction().valueOrdered(omegaQ+omegaP);
  	double Jq_minus_p = currPredictor.getSpectralDensityFunction().valueOrdered(omegaQ-omegaP);
  	
  	//old rotdif values
    //gammaRatio = -2710.5/26750.0;
  	//double fR1= BasicMath.square(0.87/0.921);
  	//double fRj= BasicMath.square(0.87/1.0);
  	//double C1=7.0/5.0*fR1;
  	//double C3=6.0/5.0*fRj;
  	//double C2 = (C3+C1)/2.0;
  	
    //derive the scaling constants
    double c1 = (6.0*Jq_plus_p+Jq_minus_p)/(6.0*Jq_plus_p-Jq_minus_p);
    double c2 = 0.5*(6.0*Jq+6.0*Jq_plus_p+Jq_minus_p)/(6.0*Jq_plus_p-Jq_minus_p);
    
    //if (currPredictor.getTauC()!=10.0)
    //	System.out.println(""+c1+" "+(c2*2.0-c1));
        
    //extract the values
    double r1 = datum.getR1().value();
    double r1Error = datum.getR1().error();
    double r2 = datum.getR2().value();
    double r2Error = datum.getR2().error();

    //use predicted values if no NOE exists
    double noe = 0.0;
    double noeError = 0.0;
    double r1m;
    double r2m;
    if (datum.hasNoe())
    {
    	noe = datum.getNoe().value();
    	noeError = datum.getNoe().error();

    	//compute high frequency approximation
      double hf = (noe-1.0)*r1*gammaRatio;
      
      //remove high frequencies from the relaxation rates
      r1m = r1-c1*hf;
      r2m = r2-c2*hf;
    }
    else
    {
    	double d2 = BasicMath.square(currPredictor.getDipolarConstant());

    	//estimate noe
    	noe = 1.0+((gammaQ/gammaP)*d2*(6.0*Jq_plus_p-Jq_minus_p))/r1;
    	noeError = noe*0.10;
    	
     	//compute high frequency approximation
      double hf = (noe-1.0)*r1*gammaRatio;
      
      //remove high frequencies from the relaxation rates
      r1m = r1-c1*hf;
      r2m = r2-c2*hf;
    	
    	//perform high frequency subtraction
    	//r1m = r1 - d2*(6.0*Jq_plus_p+Jq_minus_p);
    	//r2m = r2 - 0.5*d2*(6.0*Jq+6.0*Jq_plus_p+Jq_minus_p);
    }
            
    //System.out.println(""+R1m+" "+R2m+" "+C1*HF+" "+C2*HF);

    //high frequency estimate is not reliable
    if (r1m<1.0e-6 || r2m<1.0e-6)
    {
    	r1m = r1;
    	r2m = r2;
    	c1 = 0.0;
    	c2 = 0.0;
    }
    
    //compute the rho value
    double rho = (2.0*r2m-r1m)/r1m;

    //check the rho value is correct
    if (Double.isNaN(rho) || Double.isInfinite(rho))
    {
    	throw new RelaxationRuntimeException("Invalid rho value: "+rho);
    }
    
    //adjust rho value if too low
    if (rho<4.0/3.0)
    	rho = 4.0/3.0+1.0e-6;
        
    //old error propagation to compute variance, using the inverse definition
    //double den2 = den*den;
    //double R1d = ((2.0*C1*R2 - 2.0*C1*NOE*R2)*gammaRatio + 2.0*R2)/den2;
    //double R2d = (-2.0*R1 + 2.0*C1*R1*gammaRatio*(NOE - 1.0))/den2;
    //double NOEd = (-2.0*R1*gammaRatio*(C1*R2 - C2*R1))/den2;

    double den2 = r1m*r1m;
    double r1Derivative = 2.0*r2*(c1*gammaRatio*(noe - 1.0)-1.0)/den2;
    double r2Derivative = -2.0*r1*(c1*gammaRatio*(noe - 1.0)-1.0)/den2;
    double noeDerivative = 2.0*r1*gammaRatio*(c1*r2 - c2*r1)/den2;
    
    //var = sum of squares
    double rhoVariance = BasicMath.square(r1Derivative*r1Error)+BasicMath.square(r2Derivative*r2Error)+BasicMath.square(noeDerivative*noeError);
        
    //System.out.println(currPredictor.getTauC()+" "+computeTauCApproximate(rho, omegaP));
    
    return new NonNegativeExperimentalDatum(rho, BasicMath.sqrt(rhoVariance));
  }
	
 	public RelaxationDatum(Bond bond, ModelFreeBondConstraints constraints, double r1, double r2, double noe, double eta_xy, double eta_z, double freq, double rho)
	{
		this(bond, constraints,
				new NonNegativeExperimentalDatum(r1, Double.NaN), 
				new NonNegativeExperimentalDatum(r2, Double.NaN), 
				new BaseExperimentalDatum(noe, Double.NaN), 
				new BaseExperimentalDatum(eta_xy, Double.NaN), 
				new BaseExperimentalDatum(eta_z, Double.NaN), 
				freq,
				true,
				new NonNegativeExperimentalDatum(rho, Double.NaN));

	}
	
	public RelaxationDatum(Bond bond, ModelFreeBondConstraints constraints, NonNegativeExperimentalDatum r1, NonNegativeExperimentalDatum r2, BaseExperimentalDatum noe, 
			BaseExperimentalDatum eta_xy, BaseExperimentalDatum eta_z,
			double freq, boolean rigid)
	{
		this(bond, constraints, r1, r2, noe, eta_xy, eta_z, freq, rigid, null);
	}
	
	public RelaxationDatum(Bond bond, ModelFreeBondConstraints constraint, NonNegativeExperimentalDatum r1, NonNegativeExperimentalDatum r2, BaseExperimentalDatum noe, 
			BaseExperimentalDatum eta_xy, BaseExperimentalDatum eta_z,
			double freq, boolean rigid, ExperimentalDatum rho)
	{
		ArrayList<BaseExperimentalDatum> dataList = new ArrayList<BaseExperimentalDatum>(3);
		
		if ((eta_xy.hasValue() && !eta_z.hasValue()) || (!eta_xy.hasValue() && eta_z.hasValue()))
			throw new RelaxationRuntimeException("Etas must all be NaN or have values.");
		
		dataList.add(r1);
		dataList.add(r2);
		dataList.add(noe);
		dataList.add(eta_xy);
		dataList.add(eta_z);

		this.values = new BaseExperimentalData(dataList);
		
		this.bond = bond;
		this.freq = freq;
		this.rigid = rigid;
		this.constraints = constraint;
		
		int size = 0;
		for (BaseExperimentalDatum datum : this.values)
			if (datum.hasValue())
				size++;

		this.validDataSize = size;
		
		this.rho = computeRho();
	}
	
	private RelaxationDatum(RelaxationDatum datum, NonNegativeExperimentalDatum rho)
	{
		this.values = datum.values;
		this.bond = datum.bond;
		this.freq = datum.freq;
		this.rigid = datum.rigid;
		this.validDataSize = datum.validDataSize;
		this.constraints = datum.constraints;
		
		this.rho = rho;
	}
	
	private NonNegativeExperimentalDatum computeRho()
	{
		IsotropicSpectralDensityFunction J = new IsotropicSpectralDensityFunction(10.0, 1.0, 0.0, 1.0, 0.0);
		IsotropicModelFreePredictor f = new IsotropicModelFreePredictor(J, this.bond, this.constraints.expCSA, 
				this.bond.getBondType().getMeanRadius(), 0.0, 0.0);
		
		return computeRho(this, f);
	}
	
	/**
	 * Creates the monte carlo.
	 * 
	 * @return the relaxation datum
	 */
	public RelaxationDatum createMonteCarlo()
	{
		double newR1 = BasicStat.normalDistributionSamplePositive(getR1().value(), getR1().error());
		double newR2 = BasicStat.normalDistributionSamplePositive(getR2().value(), getR2().error());
		double newNoe = BasicStat.normalDistributionSample(getNoe().value(), getNoe().error());
		double newEta_xy = BasicStat.normalDistributionSample(getEtaXy().value(), getEtaXy().error());
		double newEta_z = BasicStat.normalDistributionSample(getEtaZ().value(), getEtaZ().error());
		
		NonNegativeExperimentalDatum r1 = new NonNegativeExperimentalDatum(newR1, getR1().error());
		NonNegativeExperimentalDatum r2 = new NonNegativeExperimentalDatum(newR2, getR2().error());
		BaseExperimentalDatum noe = new BaseExperimentalDatum(newNoe, getNoe().error());
		BaseExperimentalDatum eta_xy = new BaseExperimentalDatum(newEta_xy, getEtaXy().error());
		BaseExperimentalDatum eta_z = new BaseExperimentalDatum(newEta_z, getEtaZ().error());
		
		return new RelaxationDatum(this.bond, this.constraints, r1, r2, noe, eta_xy, eta_z, this.freq, this.rigid, null);
	}
			
	public RelaxationDatum createWithRho(NonNegativeExperimentalDatum rho)
	{
		return new RelaxationDatum(this, rho);
	}
	
	public ModelFreeBondConstraints getConstraints()
	{
		return this.constraints;
	}
	
	public BaseExperimentalDatum getEtaXy()
	{
		return this.values.get(3);
	}
	
	public BaseExperimentalDatum getEtaZ()
	{
		return this.values.get(4);
	}
	
	/**
	 * Gets the frequency.
	 * 
	 * @return the frequency
	 */
	public double getFrequency()
	{
		return this.freq;
	}
	
	public BaseExperimentalDatum getNoe()
	{
		return this.values.get(2);
	}
	
	public BaseExperimentalDatum getR1()
	{
		return this.values.get(0);
	}

	public BaseExperimentalDatum getR2()
	{
		return this.values.get(1);
	}
	
	public BaseExperimentalDatum getRatio()
	{
		double ratioValue = getR2().x/getR1().x;
		return new BaseExperimentalDatum(ratioValue, 
				ratioValue*BasicMath.sqrt(BasicMath.square(getR1().error()/getR1().value())+
						BasicMath.square(getR2().error()/getR2().value())));
	}
	
	public NonNegativeExperimentalDatum getRho()
	{
		return this.rho;
	}
		
	public BaseExperimentalDatum getRhoEta()
	{
		double value = getEtaXy().value()/getEtaZ().value();
		if (Double.isNaN(value))
			return new BaseExperimentalDatum(value, value);
		
		double err2 = value*value*(BasicMath.square(getEtaXy().error()/getEtaXy().value())+BasicMath.square(getEtaZ().error()/getEtaZ().value()));
		
		return new BaseExperimentalDatum(value, BasicMath.sqrt(err2));
	}
	
	public BaseExperimentalDatum getValue(int index)
	{
		return this.values.get(index);
	}
	
	public BaseExperimentalData getValues()
	{
		ArrayList<BaseExperimentalDatum> list = new ArrayList<BaseExperimentalDatum>();
		for (BaseExperimentalDatum datum : this.values)
			if (datum.hasValue())
				list.add(datum);
		
		return new BaseExperimentalData(list);
	}
	
	/**
	 * Checks for noe.
	 * 
	 * @return true, if successful
	 */
	public boolean hasNoe()
	{
		return getNoe().hasValue();
	}

	public boolean hasValue(int index)
	{
		return index<this.values.size() && this.values.get(index).hasValue();
	}

	/**
	 * Checks if is rigid.
	 * 
	 * @return true, if is rigid
	 */
	public boolean isRigid()
	{
		return this.rigid;
	}

	public int numValues()
	{
		return this.values.size();
	}

	public int size()
	{
		return this.validDataSize;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return new String("[freq="+this.freq+" R1="+getR1().value()+" R2="+getR1().value()+" NOE="+getNoe().value()+"]");
	}
 
}
