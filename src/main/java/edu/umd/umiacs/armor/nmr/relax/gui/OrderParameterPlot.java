/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax.gui;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import javax.swing.JFrame;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.nmr.relax.ModelFreePredictor;
import edu.umd.umiacs.armor.nmr.relax.RelaxationSolution;
import edu.umd.umiacs.armor.util.plot.Limit;
import edu.umd.umiacs.armor.util.plot.Line;
import edu.umd.umiacs.armor.util.plot.Plot2d;

/**
 * The Class OrderParameterPlot.
 */
public class OrderParameterPlot extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2696309817931180351L;

	/**
	 * Instantiates a new order parameter plot.
	 * 
	 * @param solution
	 *          the solution
	 */
	public <Predictor extends ModelFreePredictor> OrderParameterPlot(RelaxationSolution<Predictor> solution, String modelType)
	{
		setTitle("Dynamics Parameters ("+modelType+" Model)");
		setBounds(100, 100, 500, 600);
		//setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		ArrayList<RelaxationSolution<Predictor>> chainSolutions = solution.seperateByChain();
		int numberDomains = chainSolutions.size();
		
		boolean plotS2_fast = plotS2_fast(solution);
		boolean plot1 = hasData(solution,1);
		boolean plot2 = hasData(solution,2);
		boolean plot3 = hasData(solution,3);
		
		int numberPlots = 1;
		if (plot1)
			numberPlots++;
		if (plot2)
			numberPlots++;
		if (plotS2_fast)
			numberPlots++;
		if (plot3)
			numberPlots++;
		
		setLayout(new GridLayout(numberPlots, numberDomains, 0, 0));

		for (RelaxationSolution<Predictor> currSolution : chainSolutions)
			getContentPane().add(plotS2(currSolution,1));
		
		if (plot1)
			for (RelaxationSolution<Predictor> currSolution : chainSolutions)
				getContentPane().add(barPlot(currSolution,1));
		if (plot2)
			for (RelaxationSolution<Predictor> currSolution : chainSolutions)
				getContentPane().add(barPlot(currSolution,2));
		if (plotS2_fast)
			for (RelaxationSolution<Predictor> currSolution : chainSolutions)
				getContentPane().add(plotS2(currSolution,2));
		if (plot3)
			for (RelaxationSolution<Predictor> currSolution : chainSolutions)
				getContentPane().add(barPlot(currSolution,3));
		
		setVisible(true);
	}
	
	private boolean plotS2_fast(RelaxationSolution<? extends ModelFreePredictor> solution)
	{
		ArrayList<? extends ModelFreePredictor> predictors = solution.getPredictors();
		for (ModelFreePredictor predictor : predictors)
			if (predictor.getS2Fast()<1.0)
				return true;
		
		return false;
	}
	
	private boolean hasData(RelaxationSolution<? extends ModelFreePredictor> solution, int type)
	{
		for (RelaxationSolution<? extends ModelFreePredictor> chainSolution : solution.seperateByChain())
			for (RelaxationSolution<? extends ModelFreePredictor> bondSolution  : chainSolution.seperateByBondType())
				for (ModelFreePredictor predictor  : bondSolution.getPredictors())
					if (type==1)
					{
						if (predictor.getTauLocal()>0.0)
							return true;
					}
					else
					if (type==2)
					{
					  if (predictor.getRex()>0.0)
					  	return true;
					}
					else
					if (type==3)
					{
						if (predictor.getTauFast()>0.0)
							return true;
					}
		
		return false;
	}
	
	private Plot2d barPlot(RelaxationSolution<? extends ModelFreePredictor> solution, int type)
	{
		Plot2d plot = new Plot2d();		
		
		for (RelaxationSolution<? extends ModelFreePredictor> bondSolution : solution.seperateByBondType())
		{
			Bond bond = bondSolution.getPredictor(0).getBond();
			double[] residues = new double[bondSolution.size()];
			for (int iter=0; iter<residues.length; iter++)
				residues[iter] = bondSolution.getPredictor(iter).getBond().getFromAtom().getPrimaryInfo().getResidueNumber();
			
			String label = String.format("%s, %s-%s", 
					bond.getFromAtom().getPrimaryInfo().getChainID(),
					bond.getBondType().getFromAtomName(),
					bond.getBondType().getToAtomName());
			
			ArrayList<String> toolTips = new ArrayList<String>();
			double[] values = new double[bondSolution.size()];
			for (int iter=0; iter<values.length; iter++)
			{					
				if (type==1)
					values[iter] = bondSolution.getPredictor(iter).getTauLocal();
				else
			  if (type==2)
					values[iter] = bondSolution.getPredictor(iter).getRex();
				else
				if (type==3)
				  values[iter] = bondSolution.getPredictor(iter).getTauFast();
				
				toolTips.add(String.format("<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Value: %.2f", 
						bondSolution.getRelaxationData().getBondRelaxation(iter).getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
						bondSolution.getRelaxationData().getBondRelaxation(iter).getBond().getFromAtom().getPrimaryInfo().getChainID(),
						bondSolution.getRelaxationData().getBondRelaxation(iter).getBond().getFromAtom().getPrimaryInfo().getAtomName(),
						bondSolution.getRelaxationData().getBondRelaxation(iter).getBond().getToAtom().getPrimaryInfo().getAtomName(),
						values[iter]));

			}
							
			Line barLine = new Line(residues, values, label, toolTips);
			barLine.setDrawLine(true);
			barLine.setDrawShape(true);
			//rhoLine.setPaint(Color.BLUE);
			barLine.setShape(new Ellipse2D.Double(-2.5,-2.5,5,5));
		
			plot.addBar(barLine);
		}

		plot.setXLabel("Residue/Nucleotide #");
		if (type==1)
		{
			plot.setYLabel("\u03C4_loc (ns)");
			plot.setTitle("\u03C4_loc Plot", new Font("Ariel", Font.PLAIN, 14));
		}
		else
		if (type==2)
		{
			plot.setYLabel("Rex (1/s)");
			plot.setTitle("Rex Plot", new Font("Ariel", Font.PLAIN, 14));
		}
		else
		if (type==3)
		{
			plot.setYLabel("\u03C4_fast (ns)");
			plot.setTitle("\u03C4_fast Plot", new Font("Ariel", Font.PLAIN, 14));
		}
		
		return plot;
	}
	
	private Plot2d plotS2(RelaxationSolution<? extends ModelFreePredictor> solution, int type)
	{
		Plot2d plot = new Plot2d();
		
		double minS2 = 1.0;
		
		for (RelaxationSolution<? extends ModelFreePredictor> bondSolution  : solution.seperateByBondType())
		{
			Bond bond = bondSolution.getPredictor(0).getBond();
			double[] residues = new double[bondSolution.size()];
			for (int iter=0; iter<residues.length; iter++)
				residues[iter] = bondSolution.getPredictor(iter).getBond().getFromAtom().getPrimaryInfo().getResidueNumber();
			
			String label = String.format("%s, %s-%s", 
					bond.getFromAtom().getPrimaryInfo().getChainID(),
					bond.getBondType().getFromAtomName(),
					bond.getBondType().getToAtomName());
			
			ArrayList<String> toolTips = new ArrayList<String>();
			double[] orderParameters = new double[bondSolution.size()];
			for (int iter=0; iter<orderParameters.length; iter++)
			{		
				if (type==1)
					orderParameters[iter] = bondSolution.getPredictor(iter).getS2();
				else
			  if (type==2)
					orderParameters[iter] = bondSolution.getPredictor(iter).getS2Fast();
				
				toolTips.add(String.format("<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Value: %.2f", 
						bondSolution.getRelaxationData().getBondRelaxation(iter).getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
						bondSolution.getRelaxationData().getBondRelaxation(iter).getBond().getFromAtom().getPrimaryInfo().getChainID(),
						bondSolution.getRelaxationData().getBondRelaxation(iter).getBond().getFromAtom().getPrimaryInfo().getAtomName(),
						bondSolution.getRelaxationData().getBondRelaxation(iter).getBond().getToAtom().getPrimaryInfo().getAtomName(),
						orderParameters[iter]));

			}
			
			minS2 = Math.min(minS2, BasicMath.min(orderParameters));
							
			Line s2Line = new Line(residues, orderParameters, label, toolTips);
			s2Line.setDrawLine(true);
			s2Line.setDrawShape(true);
			//rhoLine.setPaint(Color.BLUE);
			s2Line.setShape(new Ellipse2D.Double(-2.5,-2.5,5,5));
		
			plot.addLine(s2Line);
		}

		plot.setYAxisLimit(new Limit(Math.max(0.0, minS2-.1), 1.0));
		
		plot.setXAxisTicksInteger();
		plot.setXLabel("Residue/Nucleotide #");
		if (type==1)
		{
			plot.setYLabel("S\u00B2");
			plot.setTitle("S\u00B2 Plot", new Font("Ariel", Font.PLAIN, 14));
		}
		else
		if (type==2)
		{
			plot.setYLabel("S\u00B2_fast");
			plot.setTitle("S\u00B2_fast Plot", new Font("Ariel", Font.PLAIN, 14));			
		}
		
		return plot;
	}
}
