/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3dImpl;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.molecule.VirtualMoleculeWrapper;
import edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;

/**
 * The Class TwoDomainElmPredictor.
 */
public final class TwoDomainRelaxationPredictor implements TwoDomainRigidPredictor
{	
	
	/** The domain two transform. */
	private final RigidTransform domainTwoTransform;
	
	/** The m1. */
	private final Molecule m1;
	
	/** The m2. */
	private final Molecule m2;
	
	/** The predictor. */
	private final ElmPredictor predictor;
	
	/** The surface1. */
	private final List<Point3d> surface1;
	
	/** The surface2. */
	private final List<Point3d> surface2;
	
	/** The two domain computor. */
	private final ModelFreeComputor<?> twoDomainComputor;
	
	public TwoDomainRelaxationPredictor(Molecule m1, Molecule m2, ElmPredictor predictor, ModelFreeComputor<?> computor)
	{
		this.m1 = m1;
		this.m2 = m2;
		
		//compute the simplified surface of the molecule
		this.surface1 = predictor.getSurfacePoints(m1);
		this.surface2 = predictor.getSurfacePoints(m2);
		
		this.domainTwoTransform = RigidTransform.identityTransform();
	  this.predictor = predictor;
	  
	  this.twoDomainComputor = computor;
	}
	
	private TwoDomainRelaxationPredictor(TwoDomainRelaxationPredictor old, RigidTransform t)
	{
		this.domainTwoTransform = old.domainTwoTransform.union(t);
		this.twoDomainComputor = old.twoDomainComputor;

		//the surface is transformed in the actual call function
		this.m1 = old.m1;
		this.m2 = old.m2;
		this.surface1 = old.surface1;
		this.surface2 = old.surface2;
		
		this.predictor = old.predictor;
	}
	

	/**
	 * Compute cov matrix.
	 * 
	 * @param t
	 *          the t
	 * @return the symmetric matrix3d
	 */
	public SymmetricMatrix3d computeCovMatrix(RigidTransform t, boolean noCollision)
	{
		ArrayList<Point3d> newList = new ArrayList<Point3d>();
		
		RigidTransform newT = this.domainTwoTransform.union(t);
		
		//compute collision of the domain 1 into domain 2
		for (int iter=0; iter<this.surface1.size(); iter++)
		{
			Point3d p = this.surface1.get(iter);
			if (noCollision || !this.m2.isCloserThan(newT.inverseTransform(p), this.predictor.getWaterCollisionDistance()))
				newList.add(p);
		}
		
		//compute collision of the domain 2 into domain 1
		for (int iter=0; iter<this.surface2.size(); iter++)
		{
			Point3d p = newT.transform(this.surface2.get(iter));
			if (noCollision || !this.m1.isCloserThan(p, this.predictor.getWaterCollisionDistance()))
				newList.add(p);
		}
		
		if (newList.isEmpty())
			throw new RelaxationRuntimeException("Two molecules fully collide, cannot compute covariance matrix.");

		SymmetricMatrix3d covMatrix = BasicStat.covarianceMatrix(newList);

		return covMatrix;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#createRotated(edu.umd.umiacs.armor.math.Rotation)
	 */
	@Override
	public TwoDomainRelaxationPredictor createRotated(Rotation R)
	{
		return new TwoDomainRelaxationPredictor(this, new RigidTransform(R, null));
	}
	
	/**
	 * Creates the transformed.
	 * 
	 * @param t
	 *          the t
	 * @return the two domain elm predictor
	 */
	public TwoDomainRelaxationPredictor createTransformed(RigidTransform t)
	{
		return new TwoDomainRelaxationPredictor(this, t);
	}
	
	@Override
	public ExperimentalData<? extends ExperimentalDatum> getData()
	{
		return this.twoDomainComputor.getData();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#getDomainOne()
	 */
	@Override
	public Molecule getDomainOne()
	{
		return this.m1;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#getDomainTwo()
	 */
	@Override
	public Molecule getDomainTwo()
	{
		return new VirtualMoleculeWrapper(this.m2, this.domainTwoTransform);
	}

	/**
	 * Gets the transfrom.
	 * 
	 * @return the transfrom
	 */
	public RigidTransform getTransfrom()
	{
		return this.domainTwoTransform;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#initPositions(edu.umd.umiacs.armor.math.RigidTransform)
	 */
	@Override
	public ArrayList<Point3d> initPositions(RigidTransform t)
	{
		return tensorInitPositions(t);
		//return noTensorInitPositions(t);
	}
	
	/**
	 * Optimize root.
	 * 
	 * @param a
	 *          the a
	 * @param b
	 *          the b
	 * @param currCov
	 *          the curr cov
	 * @param expCov
	 *          the exp cov
	 * @param minDiff
	 *          the min diff
	 * @return the double[]
	 */
	private double[] optimizeRoot(final double a, final double[] b, final SymmetricMatrix3d currCov, final SymmetricMatrix3d expCov, final double minDiff)
	{
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(300);
		optimizer.setAbsPointDifference(1.0e-6);
		optimizer.setObservations(new double[]{0.0});
		optimizer.setErrors(new double[]{1.0});
		
		final double[] distDiff = new double[3];
		final double[] c = new double[3];
		final double[] root = new double[3];
		
		final MultivariateVectorFunction improperFunc = new MultivariateVectorFunction()
		{
			@Override
			public double[] value(double[] x)
			{
				double penalty = 0.0;
				
				for (int dim=0; dim<3; dim++)
				{
					c[dim] = x[0]*currCov.get(dim,dim)-expCov.get(dim,dim);
					
					root[dim] = b[dim]*b[dim]-4.0*a*c[dim];

					//compute root
					if (root[dim]<0.0)
					{
						distDiff[dim] = 0.0;
						penalty = root[dim]*root[dim];
					}
					else
						distDiff[dim] = BasicMath.sqrt(root[dim])/(2.0*a);
				}				
				
				//if all values fall below 0 add penalty term
				if (BasicMath.norm(distDiff)<=0.0)
					penalty = BasicMath.square(BasicMath.max(root));
				
				return new double[]{BasicMath.square(BasicMath.norm(distDiff)-minDiff)+penalty};
			}
		};
		
		optimizer.setFiniteDifferenceFunction(improperFunc);
		OptimizationResultImpl sol = optimizer.optimize(new double[]{1.0});
		
		//store the solution
		double x = sol.getPoint()[0];
		for (int dim=0; dim<3; dim++)
		{
			c[dim] = x*currCov.get(dim,dim)-expCov.get(dim,dim);
			root[dim] = b[dim]*b[dim]-4.0*a*c[dim];
		}
		
		return root;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#predict(edu.umd.umiacs.armor.math.Point3d)
	 */
	@Override
	public double[] predict(Point3d translation)
	{
		return predict(new RigidTransform(null, translation));
	}
	
	/*
	private ArrayList<Point3d> noTensorInitPositions(RigidTransform t)
	{
		RigidTransform totalTransform = domainTwoTransform.union(t);
		
	  Point3d c1 = m1.center();
	  Point3d c2 = totalTransform.transform(m2.center());
	  Point3d twoOntoOneShift = c1.subtract(c2);
	  
	  double r1 = radiusMoleculeOne();
	  double r2 = radiusMoleculeTwo();
	  double dist = r1+r2;
	  
	  //generate initial points
	  ArrayList<Point3d> x0List = new ArrayList<Point3d>(6);
	  x0List.add(twoOntoOneShift.add(new Point3d(-dist,0,0)));
	  x0List.add(twoOntoOneShift.add(new Point3d(dist,0,0)));
	  x0List.add(twoOntoOneShift.add(new Point3d(0,-dist,0)));
	  x0List.add(twoOntoOneShift.add(new Point3d(0,dist,0)));
	  x0List.add(twoOntoOneShift.add(new Point3d(0,0,-dist)));
	  x0List.add(twoOntoOneShift.add(new Point3d(0,0,dist)));
	  
	  return x0List;
	}
	*/
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#predict(edu.umd.umiacs.armor.math.RigidTransform)
	 */
	@Override
	public double[] predict(RigidTransform t)
	{	
		//predict the diffusion tensor
		RotationalDiffusionTensor tensor = predictTensor(t, false);
		
		//generate the transformed molecule
		MultiDomainComplex complex = new MultiDomainComplex(this.m1, this.m2, this.getTransfrom().union(t));
		
		//get the computor for the updated structure
		try
		{
			return this.twoDomainComputor.predict(complex, tensor);
		}
		catch (AtomNotFoundException e)
		{
			throw new RelaxationRuntimeException("Unexpected error. Could not match atoms in complex.", e);
		}
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor#predict(edu.umd.umiacs.armor.math.Rotation)
	 */
	@Override
	public double[] predict(Rotation R)
	{
		return predict(new RigidTransform(R, null));
	}

	/**
	 * Predict tensor.
	 * 
	 * @param t
	 *          the t
	 * @return the rotational diffusion tensor
	 */
	public RotationalDiffusionTensor predictTensor(RigidTransform t, boolean noCollision)
	{
		//compute the covariance matrix
		SymmetricMatrix3d covMatrix = computeCovMatrix(t, noCollision);
		
		//predict the diffusion tensor
		RotationalDiffusionTensor tensor = this.predictor.predict(covMatrix);		
		
		return tensor;
	}

	public double radiusMoleculeOne()
	{
		return  this.m1.maxAtomDistance()/2.0;
	}

	public double radiusMoleculeTwo()
	{
		return this.m2.maxAtomDistance()/2.0;
	}

	/**
	 * Tensor init positions.
	 * 
	 * @param t
	 *          the t
	 * @return the array list
	 */
	private ArrayList<Point3d> tensorInitPositions(RigidTransform t)
	{
		RotationalDiffusionTensor tensor = predictTensor(t, true);
		
		EigenDecomposition3d eig = tensor.getProperEigenDecomposition();
		
		//get the experimental covariance
		double[] covValues = new PerrinEquation(this.predictor.getTemperature())
				.invertToCovariance(eig.getEigenvalue(0), eig.getEigenvalue(1), eig.getEigenvalue(2));
		SymmetricMatrix3d expCov = new SymmetricMatrix3dImpl(covValues[0], covValues[1], covValues[2], eig.getRotation());
		
		//the difference/2 between initial points
		double minInitPointDifference = radiusMoleculeOne()/2.0;
		
		//combine the surface points
		ArrayList<Point3d> fullPoints = new ArrayList<Point3d>(this.surface1);
		for (Point3d p : this.surface2)
			fullPoints.add(t.transform(p));
		SymmetricMatrix3d currCov = BasicStat.covarianceMatrix(fullPoints);
				
		int size1 = this.surface1.size();
		int size2 = this.surface2.size();
		
		int n = size1+size2;
		
		//sum all the point columns
		double[] sums1 = {0.0, 0.0, 0.0};
		for (Point3d p : this.surface1)
		{
			sums1[0] += p.x;
			sums1[1] += p.y;
			sums1[2] += p.z;
		}
		
		double[] sums2 = {0.0, 0.0, 0.0};
		for (Point3d p : this.surface2)
		{
			Point3d newP = t.transform(p);
			sums2[0] += newP.x;
			sums2[1] += newP.y;
			sums2[2] += newP.z;
		}

		//solution
		double[][] dx = new double[8][3];
		
		double a = (double)size1*(double)size2/((double)n*(double)n);
		
		//find the error adjustment so minimum dist is at least +/- minDiff/6 size
		double[] b = new double[3];
		double[] c = new double[3];
		double[] root = new double[3];
		double[] distDiff = new double[3];

		for (int dim=0; dim<3; dim++)
		{
			b[dim] = 2.0*((double)n*sums2[dim]-(double)size2*(sums1[dim]+sums2[dim]))/((double)n*(double)n);
			c[dim] = currCov.get(dim,dim)-expCov.get(dim,dim);
			
			root[dim] = b[dim]*b[dim]-4.0*a*c[dim];

			//compute root
			if (root[dim]<0.0)
				distDiff[dim] = 0.0;
			else
				distDiff[dim] = BasicMath.sqrt(root[dim])/(2.0*a);
		}
		
		//make sure that the distance between initial points is large enough
		if (BasicMath.norm(distDiff)<minInitPointDifference)
			root = optimizeRoot(a,b,currCov,expCov,minInitPointDifference);
			
		for (int dim=0; dim<3; dim++)
		{

			//make sure the optimal is far away from center
	    if (root[dim]<=0.0)
	    {
	    	for (int dxIter=0; dxIter<dx.length; dxIter++)
	    		dx[dxIter][dim] = -b[dim]/(2.0*a);
	    }
	    else
	    {	    
	      double soln = (-b[dim]-BasicMath.sqrt(root[dim]))/(2.0*a);
	      double solp = (-b[dim]+BasicMath.sqrt(root[dim]))/(2.0*a);
	      switch (dim)
	      {
	        case 0 :
	        	dx[0][dim] = soln;
	        	dx[1][dim] = soln;
	        	dx[2][dim] = soln;
	        	dx[3][dim] = soln;
	        	dx[4][dim] = solp;
	        	dx[5][dim] = solp;
	        	dx[6][dim] = solp;
	        	dx[7][dim] = solp;
	          break;
	        case 1 :
	        	dx[0][dim] = soln;
	        	dx[1][dim] = soln;
	        	dx[4][dim] = soln;
	        	dx[5][dim] = soln;
	        	dx[2][dim] = solp;
	        	dx[3][dim] = solp;
	        	dx[6][dim] = solp;
	        	dx[7][dim] = solp;
	          break;
	        case 2 :
	        	dx[0][dim] = soln;
	        	dx[2][dim] = soln;
	        	dx[4][dim] = soln;
	        	dx[6][dim] = soln;
	        	dx[1][dim] = solp;
	        	dx[3][dim] = solp;
	        	dx[5][dim] = solp;
	        	dx[7][dim] = solp;
	          break;
		    }
	    }
		}
		
		ArrayList<Point3d> sol = new ArrayList<Point3d>();
		for (int iter=0; iter<dx.length; iter++)
		{
			Point3d p = new Point3d(dx[iter][0], dx[iter][1], dx[iter][2]);
			if (!sol.contains(p))
				sol.add(p);
		}
		
		return sol;
	}
}
