/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.nmr.rdc.ExperimentalRdcData;
import edu.umd.umiacs.armor.nmr.rdc.TwoDomainRdcAligner;
import edu.umd.umiacs.armor.nmr.relax.TwoDomainRelaxationAligner;
import edu.umd.umiacs.armor.refinement.AbstractTwoDomainAligner;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;

/**
 * The Class TwoDomainElmPatiAligner.
 */
public final class TwoDomainElmPatiAligner extends AbstractTwoDomainAligner
{
	/** The rdc computor. */
	private final TwoDomainRdcAligner rdcComputor;
	
	/** The relax computor. */
	private final TwoDomainRelaxationAligner relaxComputor;

	public TwoDomainElmPatiAligner(TwoDomainRelaxationAligner relaxComputor, TwoDomainRdcAligner rdcComputor)
	{
		super(false, 0.0);
		this.relaxComputor = relaxComputor;
		this.rdcComputor = rdcComputor;
	}


	@Override
	public BaseExperimentalData getData()
	{
		BaseExperimentalData relaxValues = this.relaxComputor.getData();
		ExperimentalRdcData rdcValues = this.rdcComputor.getData();
		
		ArrayList<BaseExperimentalDatum> total = new ArrayList<BaseExperimentalDatum>(relaxValues.size()+rdcValues.size());		
		for (BaseExperimentalDatum datum : relaxValues)
			total.add(datum);
		for (BaseExperimentalDatum datum : rdcValues)
			total.add(datum);
		
		return new BaseExperimentalData(total);
	}
	
	@Override
	public List<Rotation> initRotations(Molecule m1, Molecule m2) throws AtomNotFoundException
	{
		ArrayList<Rotation> relaxOrientations =  this.relaxComputor.align(m1, m2);
		ArrayList<Rotation> rdcOrientations =  this.rdcComputor.align(m1, m2);
		
		ArrayList<Rotation> initRotations = new ArrayList<Rotation>(relaxOrientations);
		initRotations.addAll(rdcOrientations);
		
		return initRotations;
	}


	@Override
	public double[] predict(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		return BasicUtils.combineArrays(this.relaxComputor.predict(mol1, mol2), this.rdcComputor.predict(mol1, mol2));
	}

}
