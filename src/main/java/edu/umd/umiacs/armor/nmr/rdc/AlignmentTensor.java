/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3dImpl;
import edu.umd.umiacs.armor.physics.Tensor3d;
import edu.umd.umiacs.armor.util.SortType;

/**
 * The Class AlignmentTensor.
 */
public final class AlignmentTensor extends Tensor3d
{
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 1644576573389327191L;

	/**
	 * Creates the.
	 * 
	 * @param dx
	 *          the dx
	 * @param dy
	 *          the dy
	 * @param dz
	 *          the dz
	 * @param R
	 *          the r
	 * @return the alignment tensor
	 */
  public static AlignmentTensor create(double dx, double dy, double dz, Rotation R)
  {
 	 if (Math.abs(dx+dy+dz)>1.0e-10)
 		 throw new RdcRuntimeException("Alignment tensor must be traceless.");
 	 
 	 return new AlignmentTensor(new SymmetricMatrix3dImpl(dx, dy, dz, R));
  }
    
  /**
	 * Creates the zyz.
	 * 
	 * @param dx
	 *          the dx
	 * @param dy
	 *          the dy
	 * @param dz
	 *          the dz
	 * @param alpha
	 *          the alpha
	 * @param beta
	 *          the beta
	 * @param gamma
	 *          the gamma
	 * @return the alignment tensor
	 */
  public static AlignmentTensor createZYZ(double dx, double dy, double dz, double alpha, double beta, double gamma)
  {
 	 if (Math.abs(dx+dy+dz)>1.0e-10)
 		 throw new RdcRuntimeException("Alignment tensor must be traceless.");
 	 
 	 Rotation R = Rotation.createZYZ(alpha, beta, gamma);
 	 
 	 return create(dx,dy,dz, R);
  }

  /**
	 * Instantiates a new alignment tensor.
	 * 
	 * @param A
	 *          the a
	 */
  protected AlignmentTensor(SymmetricMatrix3d A)
  {
  	super(A);
  }
  
  /**
	 * Compute da std.
	 * 
	 * @param tensorList
	 *          the tensor list
	 * @return the double
	 */
	public double computeDaStd(List<AlignmentTensor> tensorList)
	{
		double da = 0.0;
		double myDa = getDa();
		for (AlignmentTensor tensor : tensorList)
			da += BasicMath.square(tensor.getDa()-myDa);

		da = BasicMath.sqrt(da/tensorList.size());
		
		return da;
	}
  
  /**
	 * Compute dr std.
	 * 
	 * @param tensorList
	 *          the tensor list
	 * @return the double
	 */
	public double computeDrStd(List<AlignmentTensor> tensorList)
	{
		double dr = 0.0;
		double myDr = getDr();
		for (AlignmentTensor tensor : tensorList)
			dr += BasicMath.square(tensor.getDr()-myDr);

		dr = BasicMath.sqrt(dr/tensorList.size());
		
		return dr;
	}
  
	/**
	 * Compute rhombicity std.
	 * 
	 * @param tensorList
	 *          the tensor list
	 * @return the double
	 */
	public double computeRhombicityStd(List<AlignmentTensor> tensorList)
	{
		double rhombicity = 0.0;
		double myRhomb = rhombicity();
		for (AlignmentTensor tensor : tensorList)
			rhombicity += BasicMath.square(tensor.rhombicity()-myRhomb);

		rhombicity = BasicMath.sqrt(rhombicity/tensorList.size());
		
		return rhombicity;
	}
	
	/**
	 * Gets the da.
	 * 
	 * @return the da
	 */
  public double getDa()
  {
  	//A Robust Method for Determining the Magnitude of the Fully
  	//Asymmetric Alignment Tensor of Oriented Macromolecules
  	//in the Absence of Structural Information
  	//G. Marius Clore, Angela M. Gronenborn, and Ad Bax

  	EigenDecomposition3d eig = getProperEigenDecomposition();
  	double[] eigValues = eig.getEigenvalues();
  	
  	return 1.0/3.0*(eigValues[2]-(eigValues[0]+eigValues[1])/2.0);
  }
  
  /**
	 * Gets the dr.
	 * 
	 * @return the dr
	 */
  public double getDr()
  {
  	//A Robust Method for Determining the Magnitude of the Fully
  	//Asymmetric Alignment Tensor of Oriented Macromolecules
  	//in the Absence of Structural Information
  	//G. Marius Clore, Angela M. Gronenborn, and Ad Bax

  	EigenDecomposition3d eig = getProperEigenDecomposition();
  	double[] eigValues = eig.getEigenvalues();
  	
  	return 1.0/3.0*(eigValues[0]-eigValues[1]);
  }
  
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.physics.Tensor3d#getProperEigenDecomposition3d()
	 */
	@Override
	public synchronized EigenDecomposition3d getProperEigenDecomposition()
	{
		return super.getProperEigenDecomposition().createSortedEigenDecomposition(SortType.ABSOLUTE_ASCENDING);
	}

	/**
	 * Rhombicity.
	 * 
	 * @return the double
	 */
  public double rhombicity()
  {
   	//A Robust Method for Determining the Magnitude of the Fully
  	//Asymmetric Alignment Tensor of Oriented Macromolecules
  	//in the Absence of Structural Information
  	//G. Marius Clore, Angela M. Gronenborn, and Ad Bax

  	return getDr()/getDa();
  }
}
