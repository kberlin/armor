/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.List;

import edu.umd.umiacs.armor.math.Ellipsoid;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3d;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.SolventAccessibleSurface;
import edu.umd.umiacs.armor.molecule.SolventAccessibleSurfaceImpl;

/**
 * The Class ElmPredictor.
 */
public final class ElmPredictor implements RotationalDiffusionTensorPredictor
{
	
	/** The shell radius. */
	private final double shellRadius;
	
	/** The temperature kelvin. */
	private final double temperatureKelvin;
	
	/** The water radius. */
	private final double waterRadius;

	public static final double DEFAULT_MOLECULE_SHELL_RADIUS = 2.2;
	
	public static final double DEFAULT_WATER_BALL_RADIUS = 1.4;

	/**
	 * Instantiates a new elm predictor.
	 * 
	 * @param temperature
	 *          the temperature
	 */
	public ElmPredictor(double temperature)
	{
		this(temperature, DEFAULT_MOLECULE_SHELL_RADIUS, DEFAULT_WATER_BALL_RADIUS);
	}
	
	/**
	 * Instantiates a new elm predictor.
	 * 
	 * @param temperature
	 *          the temperature
	 * @param shellRadius
	 *          the shell radius
	 */
	public ElmPredictor(double temperature, double shellRadius)
	{
		this(temperature, shellRadius, DEFAULT_WATER_BALL_RADIUS);
	}
	
	/**
	 * Instantiates a new elm predictor.
	 * 
	 * @param temperature
	 *          the temperature
	 * @param shellRadius
	 *          the shell radius
	 * @param waterRadius
	 *          the water radius
	 */
	public ElmPredictor(double temperature, double shellRadius, double waterRadius)
	{
		this.temperatureKelvin = temperature;
		this.shellRadius = shellRadius;
		this.waterRadius = waterRadius;
	}
	
	/**
	 * Gets the shell radius.
	 * 
	 * @return the shell radius
	 */
	public final double getShellRadius()
	{
		return this.shellRadius;
	}
	
	public List<Point3d> getSurfacePoints(Molecule mol)
	{
		if (mol==null)
			throw new NullPointerException("Molecule cannot be null.");
		
		SolventAccessibleSurface surface = new SolventAccessibleSurfaceImpl(mol, this.waterRadius, this.shellRadius);
		
		return surface.surfacePoints();
	}
	
	/**
	 * Gets the temperature.
	 * 
	 * @return the temperature
	 */
	public final double getTemperature()
	{
		return this.temperatureKelvin;
	}
	
	/**
	 * Gets the water collision distance.
	 * 
	 * @return the water collision distance
	 */
	public double getWaterCollisionDistance()
	{
		double waterCollisionDistance = this.shellRadius+this.waterRadius;

		return waterCollisionDistance;
	}
	
	/**
	 * Gets the water radius.
	 * 
	 * @return the water radius
	 */
	public final double getWaterRadius()
	{
		return this.waterRadius;
	}

	/**
	 * Predict.
	 * 
	 * @param covMatrix
	 *          the cov matrix
	 * @return the rotational diffusion tensor
	 */
	public RotationalDiffusionTensor predict(SymmetricMatrix3d covMatrix)
	{
	  //generate the ellipsoid from the covariance matrix
	  Ellipsoid ellipsoid = Ellipsoid.createFromCovariance(covMatrix);

	  //compute the lengths of the ellipsoid
	  double[] lengths = ellipsoid.getLengths();
	  
	  //convert the lengths to diffusion tensor values
	  PerrinEquation perrinFunc = new PerrinEquation(this.temperatureKelvin);
	  double[] diagD =  perrinFunc.value(lengths[0], lengths[1], lengths[2]);
	  
	  return new RotationalDiffusionTensor(diagD[0], diagD[1], diagD[2], ellipsoid.getEigenDecomposition().getRotation());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.RotationalDiffusionPredictor#predictTensor(edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public RotationalDiffusionTensor predictTensor(Molecule mol)
	{
		//compute the surface points
	  List<Point3d> points = getSurfacePoints(mol);
	  
	  return predict(BasicStat.covarianceMatrix(points));
	}
}
