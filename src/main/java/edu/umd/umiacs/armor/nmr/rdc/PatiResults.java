/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.AtomType;
import edu.umd.umiacs.armor.molecule.BondType;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.nmr.relax.RelaxationRuntimeException;
import edu.umd.umiacs.armor.util.ProgressObservable;
import edu.umd.umiacs.armor.util.ProgressObserver;

/**
 * The Class PatiResults.
 */
public class PatiResults implements ProgressObservable
{
  
  private final AlignmentComputor computor;

	/** The exception errors. */
	private final List<RelaxationRuntimeException> exceptionErrors;
	
	/** The molecule. */
	private final Molecule molecule;
	
	/** The number of required samples. */
	private final int numberOfRequiredSamples;
	
	/** The observers. */
	private final ArrayList<ProgressObserver> observers;
	
	/** The robust fit. */
	private final boolean robust;
	
	/** The computor. */
	private final RdcSolution solution;
	
	/** The computor monte carlo. */
	private final List<RdcSolution> solutionMonteCarlo;

	/** The tensor. */
	private final AlignmentTensor tensor;

	/** The tensor monte carlo. */
	private final List<AlignmentTensor> tensorMonteCarlo;

	/** The Constant TENSOR_SCALING_FACTOR. */
  public static final double TENSOR_SCALING_FACTOR = 1.0e4;
	
	public static String generatePatiString(Molecule molecule, double volumeFraction, RdcSolution solution) throws AtomNotFoundException
	{
		StringBuilder str = new StringBuilder();

		PatiPredictor predictor = PatiPredictor.createBicellePredictor(volumeFraction);
		AlignmentTensor tensor = predictor.predictTensor(molecule);
	  EigenDecomposition3d eig = tensor.getProperEigenDecomposition();
	  double[] angles = eig.getRotation().getAnglesZYZ();

	  str.append("========Predicted Alignment Tensor Results=========\n");
	  str.append(String.format("Sxx = %.3f *(%.0e)\n", eig.getEigenvalue(0)*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("Syy = %.3f *(%.0e)\n", eig.getEigenvalue(1)*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("Szz = %.3f *(%.0e)\n", eig.getEigenvalue(2)*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("alpha = %.0f deg\n", angles[0]*180.0/BasicMath.PI));
	  str.append(String.format("beta = %.0f deg\n", angles[1]*180.0/BasicMath.PI));
	  str.append(String.format("gamma = %.0f deg\n", angles[2]*180.0/BasicMath.PI));
	  str.append(String.format("C_nh = %.3e, C_ch = %.3e\n", 
	  		RdcPredictorImpl.getRDCBondConst(AtomType.NITROGEN.getGyromagneticRatio(), AtomType.HYDROGEN.getGyromagneticRatio(), BondType.N_H.getMeanRadius(), RdcPredictorImpl.DEFAULT_ORDER_PARAMETER),
	  		RdcPredictorImpl.getRDCBondConst(AtomType.CARBON.getGyromagneticRatio(), AtomType.HYDROGEN.getGyromagneticRatio(), BondType.C_H.getMeanRadius(), RdcPredictorImpl.DEFAULT_ORDER_PARAMETER)));
	  str.append(String.format("Da = %.3f *(%.0e)\n", tensor.getDa()*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("Dr = %.3f *(%.0e)\n", tensor.getDr()*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("rhombicity = %.2f\n",tensor.rhombicity()));
	  
	  double[][] A = tensor.mult(TENSOR_SCALING_FACTOR).toArray();
	  str.append("\n====Predicted Alignment Tensor====\n");
	  str.append(String.format("    [% 6.3f  % 6.3f  % 6.3f]\n", A[0][0],A[0][1],A[0][2]));
	  str.append(String.format("A = [% 6.3f  % 6.3f  % 6.3f] *(%.0e) \n",A[1][0],A[1][1],A[1][2],1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("    [% 6.3f  % 6.3f  % 6.3f]\n",A[2][0],A[2][1],A[2][2]));

	  str.append("\n====Predicted Alignment Tensor Sorted Eigendecomposition====\n");
	  double[][] V = eig.getV().toArray();
	  double[][] VT = eig.getVT().toArray();
	  str.append(String.format("    [% 6.3f  % 6.3f  % 6.3f][% 6.3f    0        0    ][% 6.3f  % 6.3f  % 6.3f]\n",V[0][0],V[0][1],V[0][2],eig.getEigenvalue(0)*TENSOR_SCALING_FACTOR, VT[0][0],VT[0][1],VT[0][2]));
	  str.append(String.format("A = [% 6.3f  % 6.3f  % 6.3f][ 0       % 6.3f    0    ][% 6.3f  % 6.3f  % 6.3f] *(%.0e) \n",V[1][0],V[1][1],V[1][2],eig.getEigenvalue(1)*TENSOR_SCALING_FACTOR, VT[1][0],VT[1][1],VT[1][2], 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("    [% 6.3f  % 6.3f  % 6.3f][ 0        0       % 6.3f][% 6.3f  % 6.3f  % 6.3f]\n",V[2][0],V[2][1],V[2][2],eig.getEigenvalue(2)*TENSOR_SCALING_FACTOR, VT[2][0],VT[2][1],VT[2][2]));    

		if (solution !=null)
		{
		  str.append("\n========Prediction RDC Results=========\n");
			str.append("<residue><chain><atom1><atom2><rdc_exp><rdc_predicted>\n");
	  	int index = 0;
		  for (RdcDatum coupling : solution.getExperimentalData())
		  {
	  		str.append(String.format("% 4d %3s %3s % 4d %3s %3s %8.3f %8.3f\n", 
	   				coupling.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
	  				coupling.getBond().getFromAtom().getPrimaryInfo().getChainID(),
	  				coupling.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
	  				coupling.getBond().getToAtom().getPrimaryInfo().getResidueNumber(),
	  				coupling.getBond().getToAtom().getPrimaryInfo().getChainID(),
	  				coupling.getBond().getToAtom().getPrimaryInfo().getAtomName(),
	  				coupling.value(),
	  				solution.getPredictor(index).predict(molecule, tensor)));
	
	  		index++;
		  }
	  }
	  
		return str.toString();
	}

	private static ArrayList<EigenDecomposition3d> getProperEigenDecompsotion3dList(EigenDecomposition3d eig, List<AlignmentTensor> tensorMonteCarlo)
	{
		ArrayList<EigenDecomposition3d> eigList = new ArrayList<EigenDecomposition3d>();
		for (AlignmentTensor tensor : tensorMonteCarlo)
		{
			EigenDecomposition3d[] val = new EigenDecomposition3d[6]; 
			val[0] = tensor.getProperEigenDecomposition();
			val[1] = val[0].createPermutedEigenDecomposition(new int[]{0,2,1});
			val[2] = val[0].createPermutedEigenDecomposition(new int[]{1,0,2});
			val[3] = val[0].createPermutedEigenDecomposition(new int[]{1,2,0});
			val[4] = val[0].createPermutedEigenDecomposition(new int[]{2,0,1});
			val[5] = val[0].createPermutedEigenDecomposition(new int[]{2,1,0});
			
			double bestError = Double.POSITIVE_INFINITY;
			EigenDecomposition3d bestEig = null;
			for (int iter=0; iter<val.length; iter++)
			{
				double currValue = BasicStat.rmsd(eig.getEigenvalues(), val[iter].getEigenvalues());
				if (currValue<bestError)
				{
					bestError = currValue;
					bestEig = val[iter];
				}
			}
			
			eigList.add(bestEig);
		}
		
		return eigList;
	}
	
	/**
	 * Instantiates a new pati results.
	 * 
	 * @param list
	 *          the list
	 * @param molecule
	 *          the molecule
	 * @param robust
	 *          the robust fit
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public PatiResults(ExperimentalRdcData list, Molecule molecule, boolean robust) throws AtomNotFoundException
	{
		this.observers = new ArrayList<ProgressObserver>();
		this.robust = robust;
		this.numberOfRequiredSamples = 400;
		this.molecule = molecule;
		
		this.exceptionErrors = Collections.synchronizedList(new ArrayList<RelaxationRuntimeException>());
		
		this.solutionMonteCarlo = Collections.synchronizedList(new ArrayList<RdcSolution>());
		this.tensorMonteCarlo = Collections.synchronizedList(new ArrayList<AlignmentTensor>());
		
		this.computor = new AlignmentComputor(list, this.robust);
		this.solution = this.computor.solve(molecule);
		
		//compute the approximate solutions
		this.tensor = this.solution.getTensor();
		
		computeAdditionalSamples(2);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ProgressObservable#addProgressObserver(edu.umd.umiacs.armor.util.ProgressObserver)
	 */
	@Override
	public void addProgressObserver(ProgressObserver o)
	{
		this.observers.add(o);
	}
	
	/**
	 * Compute additional samples.
	 * 
	 * @param number
	 *          the number
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	private void computeAdditionalSamples(final int number) throws AtomNotFoundException
	{
		ExecutorService execSvc = Executors.newCachedThreadPool();

    Runnable sampleTask = new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					ExperimentalRdcData randomizedList = PatiResults.this.computor.getData().createMonteCarlo();
					AlignmentComputor computor = new AlignmentComputor(randomizedList, PatiResults.this.robust);
					
					RdcSolution solution = computor.solve(PatiResults.this.molecule);
					PatiResults.this.solutionMonteCarlo.add(solution);
					PatiResults.this.tensorMonteCarlo.add(solution.getTensor());
										
					notifyProgressObservers();
				}
				catch (Exception e)
				{
					PatiResults.this.exceptionErrors.add(new RelaxationRuntimeException(e.getMessage(), e));
				}

			}
		};
		
		for (int iter=0; iter<number; iter++)
		{
			execSvc.execute(sampleTask);
		}
		
    //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(120L, TimeUnit.MINUTES);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new RelaxationRuntimeException("Unable to finish all tasks.", e);
    }
    
    if (!this.exceptionErrors.isEmpty())
    	throw this.exceptionErrors.get(0);
	}
	
	/**
	 * Compute statistic.
	 * 
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public void computeStatistic() throws AtomNotFoundException
	{
		computeAdditionalSamples(numberOfRequiredSamples()-numberOfCurrentSamples());
	}
	
	/**
	 * Generate output string.
	 * 
	 * @return the string
	 */
	public synchronized String generateOutputString()
	{
		StringBuilder str = new StringBuilder();
		
	  EigenDecomposition3d eig = this.tensor.getProperEigenDecomposition();
	  ArrayList<EigenDecomposition3d> eigList = getProperEigenDecompsotion3dList(eig, this.tensorMonteCarlo);
	  double[] angles = eig.getRotation().getAnglesZYZ();
	  double[] anglesStd = eig.computeZYZAnglesStd(eigList);
	  
	  RdcSolution rigidSolution = this.solution.createRigid();
	  
	  str.append("========Computed Alignment Tensor (ZYZ-rotation notation)=========\n");
	  str.append(String.format("Sxx = %.3f +/- %.3f *(%.0e) \n", eig.getEigenvalue(0)*TENSOR_SCALING_FACTOR, eig.computeEigenvalueStd(0, eigList)*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("Syy = %.3f +/- %.3f *(%.0e) \n", eig.getEigenvalue(1)*TENSOR_SCALING_FACTOR, eig.computeEigenvalueStd(1, eigList)*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("Szz = %.3f +/- %.3f *(%.0e) \n", eig.getEigenvalue(2)*TENSOR_SCALING_FACTOR, eig.computeEigenvalueStd(2, eigList)*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("alpha = %.0f +/- %.0f  deg\n", angles[0]*180.0/BasicMath.PI, anglesStd[0]*180.0/BasicMath.PI));
	  str.append(String.format("beta = %.0f +/- %.0f  deg\n", angles[1]*180.0/BasicMath.PI, anglesStd[1]*180.0/BasicMath.PI));
	  str.append(String.format("gamma = %.0f +/- %.0f deg\n", angles[2]*180.0/BasicMath.PI, anglesStd[2]*180.0/BasicMath.PI));
	  str.append(String.format("C_nh = %.3e, C_ch = %.3e\n", RdcPredictorImpl.getRDCBondConst(AtomType.NITROGEN.getGyromagneticRatio(), AtomType.HYDROGEN.getGyromagneticRatio(), 1.02, RdcPredictorImpl.DEFAULT_ORDER_PARAMETER),
	  		RdcPredictorImpl.getRDCBondConst(AtomType.CARBON.getGyromagneticRatio(), AtomType.HYDROGEN.getGyromagneticRatio(), 1.09, RdcPredictorImpl.DEFAULT_ORDER_PARAMETER)));
	  str.append(String.format("Orientational Uncertainty = %.0f deg\n", eig.computeOrientationalStd(eigList)*180.0/BasicMath.PI));
	  str.append(String.format("Da = %.3f +/- %.3f *(%.0e) \n", this.tensor.getDa()*TENSOR_SCALING_FACTOR, this.tensor.computeDaStd(this.tensorMonteCarlo)*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("Dr = %.3f +/- %.3f *(%.0e) \n", this.tensor.getDr()*TENSOR_SCALING_FACTOR, this.tensor.computeDrStd(this.tensorMonteCarlo)*TENSOR_SCALING_FACTOR, 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("rhombicity = %.3f +/- %.3f\n",this.tensor.rhombicity(), this.tensor.computeRhombicityStd(this.tensorMonteCarlo)));
	  str.append(String.format("Q = %.3f  R = %.3f  chi^2=%.3f  chi^2/df=%.3f (Rigid Only)\n", rigidSolution.qualityFactor(), 
	  		rigidSolution.pearsonsCorrelation(),
	  		rigidSolution.chi2(), 
	  		rigidSolution.chi2()/(rigidSolution.size()-5.0)));
	  
	  double[][] A = this.tensor.mult(TENSOR_SCALING_FACTOR).toArray();
	  double[][] Astd = BasicMath.mult(this.tensor.computeTensorStd(this.tensorMonteCarlo), TENSOR_SCALING_FACTOR);
	  str.append("\n====Alignment Tensor====\n");
	  str.append(String.format("    [% 6.3f  % 6.3f  % 6.3f]     [% 6.3f  % 6.3f  % 6.3f]\n", A[0][0],A[0][1],A[0][2],Astd[0][0],Astd[0][1],Astd[0][2]));
	  str.append(String.format("A = [% 6.3f  % 6.3f  % 6.3f] +/- [% 6.3f  % 6.3f  % 6.3f] *(%.0e) \n",A[1][0],A[1][1],A[1][2],Astd[1][0],Astd[1][1],Astd[1][2], 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("    [% 6.3f  % 6.3f  % 6.3f]     [% 6.3f  % 6.3f  % 6.3f]\n",A[2][0],A[2][1],A[2][2],Astd[2][0],Astd[2][1],Astd[2][2]));

	  str.append("\n====Alignment Tensor Sorted Eigendecomposition====\n");
	  double[][] V = eig.getV().toArray();
	  double[][] VT = eig.getVT().toArray();
	  str.append(String.format("    [% 6.3f  % 6.3f  % 6.3f][% 6.3f    0         0    ][% 6.3f  % 6.3f  % 6.3f]\n",V[0][0],V[0][1],V[0][2],eig.getEigenvalue(0)*TENSOR_SCALING_FACTOR, VT[0][0],VT[0][1],VT[0][2]));
	  str.append(String.format("A = [% 6.3f  % 6.3f  % 6.3f][ 0       % 6.3f     0    ][% 6.3f  % 6.3f  % 6.3f] *(%.0e) \n",V[1][0],V[1][1],V[1][2],eig.getEigenvalue(1)*TENSOR_SCALING_FACTOR, VT[1][0],VT[1][1],VT[1][2], 1.0/TENSOR_SCALING_FACTOR));
	  str.append(String.format("    [% 6.3f  % 6.3f  % 6.3f][ 0        0       % 6.3f][% 6.3f  % 6.3f  % 6.3f]\n",V[2][0],V[2][1],V[2][2],eig.getEigenvalue(2)*TENSOR_SCALING_FACTOR, VT[2][0],VT[2][1],VT[2][2]));    
	  
	  str.append("\n========Full RDC Results=========\n");
		str.append("<residue><chain><atom1><atom2><rdc_exp><rdc_backcalcuted><rel_error><*outlier>\n");

		double[] predictedValues = this.solution.predict();
		double[] relativeError = this.solution.getExperimentalData().absErrorsByStd(predictedValues);
		double sigma = BasicStat.std(this.solution.createRigid().getExperimentalData().absErrorsByStd(predictedValues));
		double maxError = 0.0;
		for (double error : relativeError)
			maxError = Math.max(maxError,Math.abs(error));
		
		int index = 0;
	  for (RdcDatum coupling : this.solution.getExperimentalData())
	  {
  		str.append(String.format("% 4d %3s %3s % 4d %3s %3s %8.3f %8.3f %9.4f", 
  				coupling.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
  				coupling.getBond().getFromAtom().getPrimaryInfo().getChainID(),
  				coupling.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
  				coupling.getBond().getToAtom().getPrimaryInfo().getResidueNumber(),
  				coupling.getBond().getToAtom().getPrimaryInfo().getChainID(),
  				coupling.getBond().getToAtom().getPrimaryInfo().getAtomName(),
  				coupling.value(),
  				predictedValues[index],
  				coupling.absErrorByStd(predictedValues[index])));

			//put star if outlier
			if (Math.abs(relativeError[index])>=maxError)
				str.append(" ***");
			else
			if (Math.abs(relativeError[index])>BasicStat.NORMAL_CONFIDENCE_INTERVAL_99*sigma)
					str.append(" **");
			else
			if (Math.abs(relativeError[index])>BasicStat.NORMAL_CONFIDENCE_INTERVAL_95*sigma)
				str.append(" *");
  		str.append("\n");
			
  		index++;
  		
	  }
	  str.append(String.format("[%.3f*sigma (95%% rigid conf. interv.), %.3f*sigma (99%% rigid conf. interv.)] [abs max] relative errors: [%.3f, %.3f] [%.3f]\n", 
	  		BasicStat.NORMAL_CONFIDENCE_INTERVAL_95, BasicStat.NORMAL_CONFIDENCE_INTERVAL_99,
	  		BasicStat.NORMAL_CONFIDENCE_INTERVAL_95*sigma, BasicStat.NORMAL_CONFIDENCE_INTERVAL_99*sigma, 
	  		maxError));

	  return str.toString();
	}
	
	public String generatePredictionString(double volumeFraction) throws AtomNotFoundException
	{
		return generatePatiString(this.molecule, volumeFraction, this.solution);
	}
	
	/**
	 * @return the computor
	 */
	public AlignmentComputor getComputor()
	{
		return this.computor;
	}
	
	/**
	 * Gets the computor.
	 * 
	 * @return the computor
	 */
	public RdcSolution getSolution()
	{
		return this.solution;
	}		
	
	/**
	 * Notify progress observers.
	 */
	public synchronized void notifyProgressObservers()
	{
		double percent = (double)numberOfCurrentSamples()/(double)numberOfRequiredSamples();
		for (ProgressObserver o : this.observers)
			o.update(percent);
	}
	
	/**
	 * Number of current samples.
	 * 
	 * @return the int
	 */
	public synchronized int numberOfCurrentSamples()
	{
		return this.tensorMonteCarlo.size();
	}
	
	/**
	 * Number of required samples.
	 * 
	 * @return the int
	 */
	public int numberOfRequiredSamples()
	{
		return this.numberOfRequiredSamples;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ProgressObservable#removeProgressObserver(edu.umd.umiacs.armor.util.ProgressObserver)
	 */
	@Override
	public void removeProgressObserver(ProgressObserver o)
	{
		this.observers.remove(o);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return generateOutputString();
	}
}
