/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.optim.ActiveSetLeastSquares;
import edu.umd.umiacs.armor.math.optim.FiniteDifferenceDMVFWrapper;
import edu.umd.umiacs.armor.math.optim.LeastSquaresOptimizationResult;
import edu.umd.umiacs.armor.math.optim.LinearConstraints;
import edu.umd.umiacs.armor.math.optim.LinearSolution;
import edu.umd.umiacs.armor.math.optim.LinearSolutions;
import edu.umd.umiacs.armor.math.optim.sparse.SparseLinearSolution;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.PrimaryStructure;
import edu.umd.umiacs.armor.molecule.filters.MolecularEnsembleSolutions;
import edu.umd.umiacs.armor.nmr.pre.PreDatum.ExperimentType;
import edu.umd.umiacs.armor.refinement.AbstractSparseEnsembleSelection;
import edu.umd.umiacs.armor.refinement.LinearEnsembleInput;
import edu.umd.umiacs.armor.refinement.RefinementRuntimeException;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.ParseOptions;

public final class PreSparseEnsembleSelection extends AbstractSparseEnsembleSelection<PreEnsembleInput>
{
	public static void main(String[] args) throws IOException, PdbException, AtomNotFoundException, ClassNotFoundException, RuntimeException
	{
  	ParseOptions options = new ParseOptions();
  	options.addOption("pre", "pre", "ARMOR formatted PRE filename.", "");
  	options.addOption("probe", "probe", "Names of the probe chainID.", "");
  	options.addOption("sim", "sim", "Simulate locations of the probe.", "");
  	options.addOption("maxsum", "maxsum", "Maximum sum of the column weights.", 2.0);

		//create the solver
  	PreSparseEnsembleSelection selection = new PreSparseEnsembleSelection(options, args);
		
		selection.run("pre");
	}

	public PreSparseEnsembleSelection(ParseOptions options, String[] args)
	{
		super(options, args);
		setDefaultSolver();
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.AbstractSparseEnsembleSelection#recomputeErrors(edu.umd.umiacs.armor.molecule.filters.MolecularEnsembleSolutions)
	 */
	@Override
	protected MolecularEnsembleSolutions adjustSolutions(MolecularEnsembleSolutions solution)
	{
		PreEnsembleInput input = (PreEnsembleInput) solution.getInput();
		ExperimentalPreData data = input.getOriginalExperimentalData();
		if (data.getExperimentType()!=ExperimentType.RATIO)
			return solution;
				
		ArrayList<LinearSolution> updateSolutions = new ArrayList<LinearSolution>();
		for (LinearSolution sol : solution.getSolutions())
		{
			sol = optimizeWeights(sol, input);
			updateSolutions.add(sol);
		}
		
		return new MolecularEnsembleSolutions(input, new LinearSolutions(updateSolutions));
	}
	
	private SparseLinearSolution optimizeWeights(LinearSolution sol, PreEnsembleInput input)
	{
		final ExperimentalPreData data = input.getOriginalExperimentalData();
		int[] columnList = sol.getActiveColumns();

		ActiveSetLeastSquares solver = ActiveSetLeastSquares.getLevenbergMarquardtOptimizer();
		
		//create the constraints
		LinearConstraints ln = LinearConstraints.createMaxSum(columnList.length, this.getOptions().get("maxsum").getDouble());
		solver.setLinearInequalityConstraint(ln);
		solver.setLowerBound(BasicUtils.createArray(columnList.length, 0.0));

		double eps = 2.0e-5;
		solver.setAbsPointDifference(eps);
		solver.setObservations(data.values());
		solver.setErrors(data.errors());
		solver.setNumberEvaluations(2000);
		
		//get the required internal data
		final RealMatrix A = input.getPredictionMatrix().getColumns(sol.getActiveColumns());
		
		MultivariateVectorFunction func = new MultivariateVectorFunction()
		{			
			@Override
			public double[] value(double[] x)
			{
	  		double[] prediction = A.mult(x);
	  		
	  		//convert to ratio prediction
	  		int iter = 0;
	  		for (PreDatum datum : data)
	  		{
	  			double r2 = prediction[iter];
	 		  	prediction[iter] = datum.predictRatio(r2);	 		  	
	 		  	
					if (Double.isNaN(prediction[iter]) || Double.isInfinite(prediction[iter]))
						throw new RefinementRuntimeException("Returned prediction is NaN or Infinite for deltaR2 value of "+r2+" and x value of: "+BasicUtils.toString(x)+".");

					iter++;
	  		}
	  			  		
	  		return prediction;
			}
		};
		
		//set the finite differences function
		solver.setFunction(new FiniteDifferenceDMVFWrapper(func, eps*5.0e-3));
		
		//solve the optimization problem
		LeastSquaresOptimizationResult result = solver.optimize(sol.getActiveWeights());
		
		//compute the fit of the solution
		double[] prediction = func.value(result.getPoint());
		double chi2 = BasicStat.chi2(prediction, data.values(), data.errors());
		
		return new SparseLinearSolution(BasicMath.sqrt(chi2), result.getPoint(), sol.getActiveColumns(), sol.dimension());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.AbstractSparseEnsembleSelection#dataNorm(edu.umd.umiacs.armor.refinement.LinearEnsembleInput)
	 */
	@Override
	protected double dataNorm(LinearEnsembleInput input)
	{
		PreEnsembleInput preInput = (PreEnsembleInput)input;
		if (preInput.getOriginalExperimentalData().getExperimentType()!=ExperimentType.RATIO)
			return super.dataNorm(input);

		return BasicMath.norm(preInput.getOriginalExperimentalData().relativeValues());
	}
	
	@Override
	public PreEnsembleInput createInput()
	{
   	if (getOptions().get("pre").getString().isEmpty())
  	{
  		System.err.println("PRE filename name cannot be empty.");
  		System.err.println(getOptions().helpMenuString());
  		System.exit(1);
  	}

   	try
   	{
			//create data list
		  HashMap<Integer, ExperimentalPreData> experimentalDataSet = ExperimentalPreData.readDataFile(new File(getOptions().get("pre").getString()));
	
		  PreEnsembleInput ensembleInput = null;
		  if (!getOptions().get("probe").getString().isEmpty())
		  {
			  //get the probe locations
			  List<String> probeIds = Arrays.asList(getOptions().get("probe").getString().split("[\t ]+"));
				
				//create input
			 	ensembleInput = new PreEnsembleInput(new ArrayList<ExperimentalPreData>(experimentalDataSet.values()), probeIds);
		  }
		  else
		  if (!getOptions().get("sim").getString().isEmpty())
		  {
			  //get the probe locations
			  List<String> probeIds = Arrays.asList(getOptions().get("sim").getString().split("[\t ]+"));
			  
			  if (probeIds.size()%3!=0)
			  	throw new PreRuntimeException("Invalid number of information for probe ids. Format <residue> <name> <chain id>.");
			  
			  ArrayList<PrimaryStructure> primary = new ArrayList<PrimaryStructure>();
			  for (int iter=0; iter<probeIds.size(); iter+=3)
			  {
			  	primary.add(new PrimaryStructure(Integer.parseInt(probeIds.get(iter)), probeIds.get(iter+1), probeIds.get(iter+2), "X"));
			  }
			  				
				//create input
			 	ensembleInput = new PreEnsembleInput(new ArrayList<ExperimentalPreData>(experimentalDataSet.values()), primary, true);		  	
		  }
		  else
		  {
	  		System.err.println("PRE location is not set.");
	  		System.err.println(getOptions().helpMenuString());
	  		System.exit(1);		  	
		  }
			
		 	return ensembleInput;
   	}
		catch (PreFileException e)
		{
			throw new RefinementRuntimeException(e);
		}	 
		catch (PreDataException e)
		{
			throw new RefinementRuntimeException(e);
		}	 		
		catch (IOException e)
		{
			throw new RefinementRuntimeException(e);
		}	 		
	}

	/*
	 * TODO undo this
	@Override
	public void outputSolution(LinearEnsembleInput input, LinearSolution sol, String title)
	{
		super.outputSolution(input,sol,title);
		outputRatioSolution((PreEnsembleInput)input, sol, title+" ==Ratio Plot==");
	}
	*/

	public void outputRatioSolution(PreEnsembleInput input, LinearSolution sol, String title)
	{	
		ExperimentalData<PreDatum> data = input.getOriginalExperimentalData();
		
		//multiply by std to get back the value
		double[] prediction = BasicMath.mult(input.getA().mult(sol.getX()), input.getExperimentalData().errors());
		for (int iter=0; iter<prediction.length; iter++)
		  	prediction[iter] = data.get(iter).predictRatio(prediction[iter]);
		double[] relativeResiduals = data.absErrorsByStd(prediction);

		double maxError = 0.0;
		for (double error : relativeResiduals)
			maxError = Math.max(maxError,Math.abs(error));
		
		double sigma = BasicStat.std(relativeResiduals);
		double chi2 = BasicStat.chi2(prediction, data.values(), data.errors());
		
		System.out.println("\n==="+title+" ===");
		System.out.println("Relative Error: "+BasicMath.norm(data.absErrorsByStd(prediction))/BasicMath.norm(data.relativeValues()));
		System.out.println("Chi2: "+chi2);
		System.out.println("N: "+data.size()+", Chi2/N: "+chi2/(double)data.size());
		System.out.println("<index><exp data info><exp. value><pred. value><relative err.><*-outlier>");
		for (int index=0; index<data.size(); index++)
		{
			ExperimentalDatum datum = data.get(index);

			//output index
			System.out.format("(%5d)",index+1);

			//output data info
			System.out.print("\t"+datum.valueInfoString());

			System.out.format("\t%1.2f\t%1.2f\t%3.3f", datum.value(), prediction[index], relativeResiduals[index]);
			
			//put star if outlier
			//put star if outlier
			if (Math.abs(relativeResiduals[index])>=maxError)
				System.out.print(" ***");
			else
			if (Math.abs(relativeResiduals[index])>BasicStat.NORMAL_CONFIDENCE_INTERVAL_99*sigma)
				System.out.print(" **");
			else
			if (Math.abs(relativeResiduals[index])>BasicStat.NORMAL_CONFIDENCE_INTERVAL_95*sigma)
				System.out.print(" *");
			System.out.println();
		}
	  System.out.format("[%.3f*sigma (95%% rigid conf. interv.), %.3f*sigma (99%% rigid conf. interv.)] [abs max] relative errors: [%.3f, %.3f] [%.3f]\n", 
	  		BasicStat.NORMAL_CONFIDENCE_INTERVAL_95, BasicStat.NORMAL_CONFIDENCE_INTERVAL_99,
	  		BasicStat.NORMAL_CONFIDENCE_INTERVAL_95*sigma, BasicStat.NORMAL_CONFIDENCE_INTERVAL_99*sigma, 
	  		maxError);
	}

}
