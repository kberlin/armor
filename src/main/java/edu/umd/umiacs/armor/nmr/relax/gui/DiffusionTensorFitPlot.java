/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.nmr.relax.AbstractModelFreeComputor;
import edu.umd.umiacs.armor.nmr.relax.BondRelaxation;
import edu.umd.umiacs.armor.nmr.relax.RelaxationDatum;
import edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor;
import edu.umd.umiacs.armor.nmr.relax.RelaxationSolution;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.plot.Limit;
import edu.umd.umiacs.armor.util.plot.Line;
import edu.umd.umiacs.armor.util.plot.Plot2d;

import java.awt.GridLayout;

/**
 * The Class DiffusionTensorFitPlot.
 */
public class DiffusionTensorFitPlot extends JFrame
{
	/** The error bars. */
	private JCheckBox errorBars;

	/** The graph panel. */
	private JPanel graphPanel;
	
	/** The outlier plots. */
	private JCheckBox outlierPlots;
	
	/** The scatter panel. */
	private JPanel scatterPanel;
	
	/** The scatter plot. */
	private Plot2d scatterPlot = null;
	
	/** The solution. */
	
	private RelaxationSolution<? extends RelaxationRatePredictor> solution;
	
	/** The title. */
	private String title;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5820707896059733841L;
	
	/**
	 * Instantiates a new diffusion tensor fit plot.
	 * 
	 * @param <solution>
	 *          the generic type
	 * @param <Predictor>
	 *          the generic type
	 * @param solution
	 *          the solution
	 * @param title
	 *          the title
	 */
	public DiffusionTensorFitPlot(RelaxationSolution<? extends RelaxationRatePredictor> solution, String title)
	{
		//only rigid components displayed
		solution = solution.createRigid();
		
		this.solution = solution;
		this.title = title;
		
		setTitle("Fit Plot");
		setBounds(100, 100, 500, 600);
		//setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		getContentPane().setBackground(java.awt.Color.WHITE);
		getContentPane().setLayout(new BorderLayout(0, 0));

		this.graphPanel = new JPanel();
		this.graphPanel.setLayout(new GridLayout(0, 1, 0, 0));
		this.graphPanel.setBackground(java.awt.Color.WHITE);
		
		//create the scatter plot
		this.scatterPanel = new JPanel();
		this.scatterPanel.setLayout(new BorderLayout(0, 0));
		this.scatterPanel.setBackground(java.awt.Color.WHITE);
		this.graphPanel.add(this.scatterPanel);
		
		getContentPane().add(this.graphPanel, BorderLayout.CENTER);
		
		JPanel optionsPanel = new JPanel();
		optionsPanel.setBackground(java.awt.Color.WHITE);
		optionsPanel.setLayout(new FlowLayout());
		
		this.errorBars = new JCheckBox("Error Bars");
		this.errorBars.setBackground(Color.WHITE);
		this.errorBars.setToolTipText("Add error bars to scatter plot.");
		this.errorBars.setSelected(false);
		//errorBars.setHorizontalAlignment(SwingConstants.CENTER);
		optionsPanel.add(this.errorBars, BorderLayout.CENTER);
		this.errorBars.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent arg0)
			{
				DiffusionTensorFitPlot.this.scatterPanel.removeAll();
				DiffusionTensorFitPlot.this.scatterPanel.add(drawScatterPlot());
				setVisible(true);
			}
		});
		
		this.outlierPlots = new JCheckBox("Highlight Outliers");
		this.outlierPlots.setBackground(Color.WHITE);
		this.outlierPlots.setToolTipText("Color current outliers in red.");
		this.outlierPlots.setSelected(false);
		//outlierPlots.setHorizontalAlignment(SwingConstants.CENTER);
		optionsPanel.add(this.outlierPlots, BorderLayout.CENTER);
		this.outlierPlots.addChangeListener(new ChangeListener()
		{
			@Override
			public void stateChanged(ChangeEvent arg0)
			{
				DiffusionTensorFitPlot.this.scatterPanel.removeAll();
				DiffusionTensorFitPlot.this.scatterPanel.add(drawScatterPlot());
				setVisible(true);
			}
		});
		
		this.scatterPanel.add(drawScatterPlot());
		
		//generate a bar plot for each chain
		ArrayList<? extends RelaxationSolution<? extends RelaxationRatePredictor>> computorListOfChains = solution.seperateByChain();
		ArrayList<Plot2d> barPlots = new ArrayList<Plot2d>();
		for (RelaxationSolution<? extends RelaxationRatePredictor> chainSolution : computorListOfChains)
		{
			Plot2d barPlot = new Plot2d();
			barPlot.setRenderingOrderReverse();

			barPlot.setTitle(String.format("Residuals for Chain %s",chainSolution.getRelaxationData().getBondRelaxation(0).getBond().getFromAtom().getPrimaryInfo().getChainID()),
					new Font("Ariel", Font.PLAIN, 12));
			barPlot.setIntegerTicks(true);
			barPlot.setXLabel("Residue/Nucleotide #");
			barPlot.setYLabel("Residuals/\u03c3");
			//barPlot.removeLegend();			

			barPlots.add(barPlot);
			this.graphPanel.add(barPlot);
		
			//for each relaxation list		
			ArrayList<? extends RelaxationSolution<? extends RelaxationRatePredictor>> solutionList = chainSolution.seperateByBondTypeAndFrequency();
			//ArrayList<Line> lines = new ArrayList<Line>();
			for (RelaxationSolution<? extends RelaxationRatePredictor> relaxComputor : solutionList)
			{
				ArrayList<String> toolTips = new ArrayList<String>();
				
				double[] residues = new double[relaxComputor.getRelaxationData().size()];
	
				int counter = 0;
				double[] residuals = relaxComputor.rhoResiduals();
				for (BondRelaxation bondRelaxation : relaxComputor.getRelaxationData().getBondRelaxationData())
				{
					residues[counter] = bondRelaxation.getBond().getFromAtom().getPrimaryInfo().getResidueNumber();
					toolTips.add(String.format("<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Freq: %.2f<br>Residual: %.3f</html>", 
							bondRelaxation.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
							bondRelaxation.getBond().getFromAtom().getPrimaryInfo().getChainID(),
							bondRelaxation.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
							bondRelaxation.getBond().getToAtom().getPrimaryInfo().getAtomName(),
							bondRelaxation.getRelaxationDatum(0).getFrequency(),
							residuals[counter]));
					
					counter++;
				}
								
				//label of the current dataset
				String label = String.format("%s, %s-%s, %.2f MHz", 
						relaxComputor.getRelaxationData().getBondRelaxation(0).getBond().getFromAtom().getPrimaryInfo().getChainID(),
						relaxComputor.getRelaxationData().getBondRelaxation(0).getBond().getBondType().getFromAtomName(),
						relaxComputor.getRelaxationData().getBondRelaxation(0).getBond().getBondType().getToAtomName(), 
						relaxComputor.getRelaxationData().getBondRelaxation(0).getRelaxationDatum(0).getFrequency());
				
				Line residualLine = new Line(residues, relaxComputor.rhoResiduals(), label, toolTips);
				//residualLine.setPaint(new GradientPaint(0.0F, 0.0F, Color.blue, 0.0F, 0.0F, new Color(0, 0, 64)));
	
				//add to the bar list
				barPlot.addBar(residualLine);
				//lines.add(residualLine);
			}
			
			//barPlot.addStackedBar(lines);
		}
		
		getContentPane().add(optionsPanel, BorderLayout.SOUTH);
		
		setVisible(true);
	}
	
	/**
	 * Draw scatter plot.
	 * 
	 * @return the plot2 d
	 */
	private Plot2d drawScatterPlot()
	{
		this.scatterPlot = new Plot2d();
		
		//prediction scatter plot
		BaseExperimentalData rhoData = this.solution.getRelaxationData().getRhoData();
		double[] rhoExperimental = rhoData.values();
		double[] rhoErrors = rhoData.errors();
		double[] rhoResiduals = this.solution.rhoResiduals();
		double[] rhoPredicted = this.solution.predictRho();
		
		ArrayList<Integer> inlierIndex = new ArrayList<Integer>();
		ArrayList<Integer> outlierIndex = new ArrayList<Integer>();
		
		int counter = 0;
		ArrayList<String> toolTipsInlier = new ArrayList<String>();
		ArrayList<String> toolTipsOutlier = new ArrayList<String>();
		for (BondRelaxation bondRelaxation : this.solution.getRelaxationData().getBondRelaxationData())
		{
			for (RelaxationDatum data : bondRelaxation.getRelaxationList())
			{
				String tooltip = String.format("<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Freq: %.2f<br>Residual: %.3f</html>", 
						bondRelaxation.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
						bondRelaxation.getBond().getFromAtom().getPrimaryInfo().getChainID(),						
						bondRelaxation.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
						bondRelaxation.getBond().getToAtom().getPrimaryInfo().getAtomName(),
						data.getFrequency(),
						rhoResiduals[counter]);
				
				if (!this.outlierPlots.isSelected() || Math.abs(rhoResiduals[counter])<=AbstractModelFreeComputor.OUTLIER_THRESHOLD)
				{
					toolTipsInlier.add(tooltip);
					inlierIndex.add(counter);
				}
				else
				{
					toolTipsOutlier.add(tooltip);
					outlierIndex.add(counter);
				}
				
				counter++;
			}
		}
		
		Line scatterLineInlier = new Line(BasicUtils.doubleArraySubset(rhoExperimental, inlierIndex), 
				BasicUtils.doubleArraySubset(rhoPredicted, inlierIndex), 
				BasicUtils.doubleArraySubset(rhoErrors, inlierIndex)
				, "Inliers", toolTipsInlier);
		scatterLineInlier.setDrawLine(false);
		scatterLineInlier.setDrawShape(true);
		scatterLineInlier.setPaint(Color.BLUE);
		scatterLineInlier.setShape(new Ellipse2D.Double(-2.5,-2.5,5,5));
		
		Line scatterLineOutlier = null;
		if (outlierIndex.size()>0)
		{
			scatterLineOutlier = new Line(BasicUtils.doubleArraySubset(rhoExperimental, outlierIndex), 
					BasicUtils.doubleArraySubset(rhoPredicted, outlierIndex), 
					BasicUtils.doubleArraySubset(rhoErrors, outlierIndex)
					, "Outliers", toolTipsOutlier);
			scatterLineOutlier.setDrawLine(false);
			scatterLineOutlier.setDrawShape(true);
			scatterLineOutlier.setPaint(Color.RED);
			scatterLineOutlier.setShape(new Ellipse2D.Double(-2.5,-2.5,5,5));
		}
				
		double minValue = Math.min(BasicMath.min(rhoPredicted), BasicMath.min(rhoExperimental));
		double maxValue = Math.max(BasicMath.max(rhoPredicted), BasicMath.max(rhoExperimental));
		double diff = maxValue-minValue;
		Limit limit = new Limit(minValue - diff * .1, maxValue + diff * .1);
		Line regressionLine = new Line(new double[] {limit.getMin(), limit.getMax()}, new double[] {limit.getMin(), limit.getMax()}, "y=x");
		regressionLine.setDrawLine(true);
		regressionLine.setDrawShape(false);
		regressionLine.setPaint(Color.BLACK);
		
		this.scatterPlot.addLine(regressionLine);
		if (this.errorBars.isSelected())
		{
			this.scatterPlot.addXErrorLine(scatterLineInlier);
			if (outlierIndex.size()>0)
				this.scatterPlot.addXErrorLine(scatterLineOutlier);
		}
		else
		{
		  this.scatterPlot.addLine(scatterLineInlier);
		  if (outlierIndex.size()>0)
		  	this.scatterPlot.addLine(scatterLineOutlier);
		}
		
		this.scatterPlot.setXLabel("\u03C1 Exp.");
		this.scatterPlot.setYLabel("\u03C1 Computed");
		this.scatterPlot.setTitle(String.format("%s (Q=%.3f, R=%.3f)", this.title, this.solution.rhoQualityFactor(), this.solution.rhoPearsonsCorrelation()),
									new Font("Ariel", Font.PLAIN, 14));
		
		//set the axes limits
		this.scatterPlot.setXAxisLimit(limit);
		this.scatterPlot.setYAxisLimit(limit);
		
		this.scatterPlot.setVisible(true);
		
		return this.scatterPlot;
	}
}