/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.io.Serializable;
import java.util.HashMap;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.BondType;

public class ModelFreeBondConstraints implements Serializable
{
	public final BondType bondType;
	/** The exp csa. */
	
	public final double minCSA, maxCSA, expCSA;
	/** The exp rex. */
	
	public final double minCsaAngle, maxCsaAngle, expCsaAngle;
	/** The exp rex. */

	public final double minRex, maxRex, expRex;
	
	public final double minS2Fast, maxS2Fast, expS2Fast;
	public final double minS2Slow, maxS2Slow, expS2Slow;
	public final double minTauFast, maxTauFast, expTauLocFast;
	
	public final double minTauLoc, maxTauLoc, expTauLoc;
	public final static double _CSA_VARIABILITY = 30.0;
	
	private final static HashMap<BondType,ModelFreeBondConstraints> _DEFAULT_CONSTRAINT_LIST = new HashMap<BondType,ModelFreeBondConstraints>();
	
	public final static double _DEFAULT_expCsaAngle = 15.0/180.0*BasicMath.PI;
	public final static double _DEFAULT_expRex = 0.001;
	public final static double _DEFAULT_expS2Fast = 1.0;
	public final static double _DEFAULT_expS2Slow = 0.80;
	public final static double _DEFAULT_expTauLoc = 0.1001; 
	public final static double _DEFAULT_expTauLocFast = 0.00501; 
	
	public final static double _DEFAULT_maxCsaAngle = BasicMath.PI;
	public final static double _DEFAULT_maxRex = 50;
  public final static double _DEFAULT_maxS2Fast = 1.0;
  public final static double _DEFAULT_maxS2Slow = 1.0;
  public final static double _DEFAULT_maxTauLoc = 2.0;
	public final static double _DEFAULT_maxTauLocFast = 0.0999; 
  
  public final static double _DEFAULT_minCsaAngle = 0.0;
  public final static double _DEFAULT_minRex = 0.0;
  public final static double _DEFAULT_minS2Fast = 0.05;
  public final static double _DEFAULT_minS2Slow = .05; 
	public final static double _DEFAULT_minTauLoc = 0.1;
	public final static double _DEFAULT_minTauLocFast = 0.005; 
	
  public final static ModelFreeBondConstraints C_H = new ModelFreeBondConstraints(BondType.C_H,  
  		/*CSA*/ 130,220,170, true);
  public final static ModelFreeBondConstraints C1p_H1p = new ModelFreeBondConstraints (BondType.C1p_H1p, 
  		 /*CSA*/ 20,40,29, true);
  public final static ModelFreeBondConstraints C2_H2 = new ModelFreeBondConstraints (BondType.C2_H2,  
  		 /*CSA*/ 145,170,150, true);
  public final static ModelFreeBondConstraints C2p_H2p = new ModelFreeBondConstraints (BondType.C2p_H2p, 
    		 /*CSA*/ 5,23.1+ModelFreeBondConstraints._CSA_VARIABILITY,23.1, true);
  public final static ModelFreeBondConstraints C3_H3 = new ModelFreeBondConstraints (BondType.C3_H3,
	  			 /*CSA*/ 130,220,170, true);
  public final static ModelFreeBondConstraints C3p_H3p = new ModelFreeBondConstraints (BondType.C3p_H3p, 
      		 /*CSA*/ 83.0-ModelFreeBondConstraints._CSA_VARIABILITY,83.0+ModelFreeBondConstraints._CSA_VARIABILITY,83, true);
  public final static ModelFreeBondConstraints C4_H4 = new ModelFreeBondConstraints (BondType.C4_H4,  
    		/*CSA*/ 130,220,170, true);
  public final static ModelFreeBondConstraints C4p_H4p = new ModelFreeBondConstraints (BondType.C4p_H4p, 
    		 /*CSA*/ 79.5-ModelFreeBondConstraints._CSA_VARIABILITY,79.5+ModelFreeBondConstraints._CSA_VARIABILITY,79.5, true);
  public final static ModelFreeBondConstraints C5_H5 = new ModelFreeBondConstraints (BondType.C5_H5,  
    		/*CSA*/ 130,220,170, true);
  
  public final static ModelFreeBondConstraints C5p_H5p = new ModelFreeBondConstraints (BondType.C5p_H5p,
    		 /*CSA*/ 55.5-ModelFreeBondConstraints._CSA_VARIABILITY,55.5+ModelFreeBondConstraints._CSA_VARIABILITY,55.5, true);
  
	public final static ModelFreeBondConstraints C5p_H5pp = new ModelFreeBondConstraints (BondType.C5p_H5pp, 
     		 /*CSA*/ 55.5-ModelFreeBondConstraints._CSA_VARIABILITY,55.5+ModelFreeBondConstraints._CSA_VARIABILITY,55.5, true);

  public final static ModelFreeBondConstraints C6_H6 = new ModelFreeBondConstraints (BondType.C6_H6,  
    		/*CSA*/ 150,230,180, true); 
	
	public final static ModelFreeBondConstraints C7_H7 = new ModelFreeBondConstraints (BondType.C7_H7,  
    		/*CSA*/ 160,179,165, true);
	
	public final static ModelFreeBondConstraints C8_H8 = new ModelFreeBondConstraints (BondType.C8_H8, 
   		/*CSA*/ 140-ModelFreeBondConstraints._CSA_VARIABILITY,140+ModelFreeBondConstraints._CSA_VARIABILITY,140, true);
	
	public final static ModelFreeBondConstraints CA_HA = new ModelFreeBondConstraints (BondType.CA_HA, 
     		 /*CSA*/ 5,70,30, true);
	
	public final static ModelFreeBondConstraints N_H  = new ModelFreeBondConstraints(BondType.N_H, 
      		 /*CSA*/  120,220,160, true);

	public final static ModelFreeBondConstraints N1_H1  = new ModelFreeBondConstraints(BondType.N1_H1, 
   	 		 /*CSA*/  100,160,130, true);
	
	public final static ModelFreeBondConstraints N3_H3  = new ModelFreeBondConstraints(BondType.N3_H3, 
   		 	 /*CSA*/  65,125,95, true);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2040883496792069394L;
	
	public static ModelFreeBondConstraints getBondConstants(BondType bondType)
	{
		ModelFreeBondConstraints c = _DEFAULT_CONSTRAINT_LIST.get(bondType);
		
		if (c==null)
			throw new RelaxationRuntimeException("No relaxation constraints set for this bond type.");
		
		return c;
	}
	
	public ModelFreeBondConstraints(BondType bondType, double minCSA, double maxCSA, double expCSA)
	{
		this(bondType,
				_DEFAULT_minS2Slow, _DEFAULT_maxS2Slow, _DEFAULT_expS2Slow, 
				_DEFAULT_minTauLoc, _DEFAULT_maxTauLoc, _DEFAULT_expTauLoc, 
				_DEFAULT_minS2Fast, _DEFAULT_maxS2Fast, _DEFAULT_expS2Fast,
				_DEFAULT_minTauLocFast, _DEFAULT_maxTauLocFast, _DEFAULT_expTauLocFast,
				minCSA, maxCSA, expCSA,
				_DEFAULT_minCsaAngle, _DEFAULT_maxCsaAngle, _DEFAULT_expCsaAngle,
				_DEFAULT_minRex, _DEFAULT_maxRex, _DEFAULT_expRex);
	}
	

	private ModelFreeBondConstraints(BondType bondType, double minCSA, double maxCSA, double expCSA, boolean initLoad)
	{
		this(bondType,minCSA, maxCSA, expCSA);
		_DEFAULT_CONSTRAINT_LIST.put(bondType, this);
	}

	public ModelFreeBondConstraints(BondType bond, 
  							double minS2Slow, double maxS2Slow, double expS2Slow, 
  							double minTauLoc, double maxTauLoc, double expTauLoc, 
  							double minS2Fast, double maxS2Fast, double expS2Fast,
  							double minTauLocFast, double maxTauLocFast, double expTauLocFast,
  							double minCSA, double maxCSA, double expCSA,
  							double minCsaAngle, double maxCsaAngle, double expCsaAngle,
  							double minRex, double maxRex, double expRex)
  {
  	if (minCSA>expCSA || maxCSA<expCSA)
  		throw new RelaxationRuntimeException("Invalid CSA values.");
  	if (minCsaAngle>expCsaAngle || maxCsaAngle<expCsaAngle)
  		throw new RelaxationRuntimeException("Invalid csaAngle values.");
  	if (minRex>expRex || maxRex<expRex)
  		throw new RelaxationRuntimeException("Invalid Rex values.");
  	if (minS2Slow>expS2Slow || maxS2Slow<expS2Slow)
  		throw new RelaxationRuntimeException("Invalid S2 values.");
  	if (minTauLoc>expTauLoc || maxTauLoc<expTauLoc)
  		throw new RelaxationRuntimeException("Invalid tauLoc values.");
  	if (minS2Fast>expS2Fast || maxS2Fast<expS2Fast)
  		throw new RelaxationRuntimeException("Invalid S2Fast values.");
  	if (minTauLocFast>expTauLocFast || maxTauLocFast<expTauLocFast)
  		throw new RelaxationRuntimeException("Invalid tauLocFast values.");
  	
  	this.bondType = bond;
  	this.minS2Slow = minS2Slow;
  	this.maxS2Slow = maxS2Slow;
  	this.expS2Slow = expS2Slow;
  	this.minTauLoc = minTauLoc;
  	this.maxTauLoc = maxTauLoc;
  	this.expTauLoc = expTauLoc;
  	this.minS2Fast = minS2Fast;
  	this.maxS2Fast = maxS2Fast;
  	this.expS2Fast = expS2Fast;
  	this.minTauFast = minTauLocFast;
  	this.maxTauFast = maxTauLocFast;
  	this.expTauLocFast = expTauLocFast;
  	this.minCSA = minCSA;
  	this.maxCSA = maxCSA;
  	this.expCSA = expCSA;
  	this.minCsaAngle = minCsaAngle;
  	this.maxCsaAngle = maxCsaAngle;
  	this.expCsaAngle = expCsaAngle;
  	this.minRex = minRex;
  	this.maxRex = maxRex;
  	this.expRex = expRex;
  }
}
