package edu.umd.umiacs.armor.nmr.pcs;

import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.ExperimentalDatum;

public class PcsDatum extends BaseExperimentalDatum
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 96100812501184625L;

	public PcsDatum(double value)
	{
		super(value);
	}

	public PcsDatum(double value, double error)
	{
		super(value, error);
	}

	public PcsDatum(ExperimentalDatum datum)
	{
		super(datum);
	}

}
