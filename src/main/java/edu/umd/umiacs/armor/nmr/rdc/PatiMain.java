/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.Model;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.nmr.rdc.gui.RdcFitPlot;
import edu.umd.umiacs.armor.refinement.ConvexAlignmentTranslationDocker;
import edu.umd.umiacs.armor.refinement.DockInfo;
import edu.umd.umiacs.armor.util.FileIO;
import edu.umd.umiacs.armor.util.ParseOptions;
import edu.umd.umiacs.armor.util.ProgressObserver;

/**
 * The Class PatiMain.
 */
public class PatiMain implements ProgressObserver
{
	
	/** The last result. */
	double lastResult = 0;
	
	private static void computeAlignmentError(ExperimentalRdcData experimentalData, Model model, final int number, boolean robust) throws AtomNotFoundException, RdcDataException, PdbException
	{
		Model model1 = model.createChain(0);
		Model model2 = model.createChain(1);
		
		Molecule mol1 = model1.createBasicMolecule();
		Molecule mol2 = model2.createBasicMolecule();
		
		experimentalData = experimentalData.createRigid();
	  TwoDomainRdcAligner computor = new TwoDomainRdcAligner(experimentalData, robust);	  
	  ArrayList<Rotation> alignmentSolution = computor.align(mol1, mol2);
		
	  ArrayList<ArrayList<Rotation>> alignmentMonteCarlo = new ArrayList<ArrayList<Rotation>>();
		for (int iter=0; iter<number; iter++)
		{
			if (iter%20==0)
				System.out.print(".");
			
			ExperimentalRdcData randomizedList = experimentalData.createMonteCarlo();

		  computor = new TwoDomainRdcAligner(randomizedList, false);	  
		  alignmentMonteCarlo.add(computor.align(mol1, mol2));
		}
		System.out.println("done.");
		
		//find the closest orientation
		double largestError = 0.0;
		for (Rotation currSolution : alignmentSolution)
		{
			double errorSum = 0.0;
			for (ArrayList<Rotation> monteCarloSolution  : alignmentMonteCarlo)
			{
				double minError = Double.POSITIVE_INFINITY;				
				for (Rotation currRotation : monteCarloSolution)
					minError = Math.min(minError, currRotation.distance(currSolution));
				
				errorSum += minError;
			}
			
			largestError = Math.max(largestError, errorSum/(double)alignmentMonteCarlo.size());
		}
		
		
		System.out.println("\n========Uncertainty in Two Domain Alignment=========");
	  System.out.format("Alignment Orientational Uncertainty = %.0f deg\n\n", largestError*180.0/BasicMath.PI);
	}
	
	public static List<DockInfo> dockResults(String outputPdbFile, ExperimentalRdcData experimentalData, PdbStructure pdb, int modelNumber, double volumeFraction, boolean robust) throws AtomNotFoundException, IOException, PdbException, RdcDataException
	{
		ArrayList<ExperimentalRdcData> chainList = experimentalData.createRigid().seperateByChain();
		
		if (chainList.size()!=2)
			throw new RdcRuntimeException("Docking must be performed on a two domain file only.");
		
		Model model = pdb.getModel(modelNumber);
	  Model model1 = model.createChain(0);
	  Model model2 = model.createChain(1);
		
		BasicMolecule mol1 = model1.createBasicMolecule();
		BasicMolecule mol2 = model2.createBasicMolecule();
		
  	TwoDomainRdcAligner twoDomainComputor = new TwoDomainRdcAligner(experimentalData, robust);
  	RdcDataPredictor predictor = new RdcDataPredictor(twoDomainComputor.getComputor(), PatiPredictor.createBicellePredictor(volumeFraction));
  	
  	ConvexAlignmentTranslationDocker patiDock = new ConvexAlignmentTranslationDocker(twoDomainComputor, predictor, 1.5);
  	patiDock.addProgressObserver(new PatiMain());

  	List<DockInfo> dockResults = patiDock.dock(mol1, mol2);
  	System.out.println("done.");
	  System.out.print("Docking Transformations ([x_t;y_t;z_t] = R*[x;y;z]+t):");
	  int count = 1;
	  for (DockInfo t : dockResults)
	  {
		  System.out.print("Solution "+count+":\n");
	  	System.out.println("Chi^2 = "+t.getOptimizationResult().getValue());
	  	System.out.print("R = [");
	  	System.out.print(t.getRigidTransform().getRotation()+"]\n");
	  	System.out.print("t = ");
	  	System.out.format("[%.3f %.3f %.3f]\n", t.getRigidTransform().getTranslation().x, t.getRigidTransform().getTranslation().y, t.getRigidTransform().getTranslation().z);
	  	System.out.println();
	  	
	  	count++;
	  }
	  System.out.println("Total number of solutions = "+dockResults.size());
	  
	  if (!outputPdbFile.isEmpty())
	  {	  
		  System.out.print("Outputing results to: "+outputPdbFile+"...");
		 	PdbStructure patiResults = pdb.createFromMoleculeList(new MultiDomainComplex(mol1, mol2).createSecondDomainTranslatedDockList(dockResults));
			MoleculeFileIO.writePDB(new File(outputPdbFile), patiResults.toPDB());
		  System.out.print("done.");
	  }
	  
	  return dockResults;
	}
	
	public static void main(String[] args) throws IOException, RdcFileException, PdbException, AtomNotFoundException, RdcDataException 
	{
  	ParseOptions options = new ParseOptions();
  	options.addRequiredOption("pdb", "-pdb", "Name of the PDB file to use for calculations.", String.class);
  	options.addOption("model", "-model", "Model number of the pdb file.", 1);
  	options.addOption("rdc", "-rdc", "Name of the RDC file to use for calculations. CNS or PATI format.", "");
  	options.addOption("axes", "-axes", "Name of the PyMOL script file for tensor axes. Not generated if set to empty.", "");
  	options.addOption("bicelle", "-bicelle", "Bicelle volume fraction.", 0.03);
  	options.addOption("axeslength", "-axesl", "Length of PyMOL axes extension beyond the molecule bounds (in Angstroms).", 5.0);
  	options.addOption("dock", "-dock", "Perform docking of a two domain system and output rotation and translation.", false);
  	options.addOption("dockPdb", "-dockpdb", "Name of the output PDB file used for Patidock. Not generated if set to empty.", "");
  	options.addOption("draw", "-draw", "Draw the fit plots.", false);
  	options.addOption("nostat", "-nostat", "Do not compute statistics (speeds up computation).", false);
  	options.addOption("robust", "-robust", "Perform robust least-squares regression (outlier suppression) were possible.", false);
  	
  	try 
  	{
  		options.parse(args);
  		if (options.needsHelp())
  		{
  			System.out.println(options.helpMenuString());
  			System.exit(0);
  		}
  		
  		options.checkParameters();
  	}
  	catch (Exception e)
  	{
  		System.err.println(e.getMessage());
  		System.err.println(options.helpMenuString());
  		System.exit(1);
  	}
  	
 		int status = runProgram(options);
 		
 		if (status!=0)
 			System.out.println("Execution failed!");
 		else
 			System.out.println("Computation complete!");

	}

	
	private static int runProgram(ParseOptions options) throws IOException, RdcFileException, PdbException, AtomNotFoundException, RdcDataException
	{
		if (options.get("help").getBoolean() || !options.get("pdb").isSet())
		{
  		System.err.println(options.helpMenuString());
  		return 0;
		}

		System.out.println("Options:");
		System.out.println(options);
		
		String rdcFile = options.get("rdc").getString();
	  String pdbFile = options.get("pdb").getString();
	  boolean robust = options.get("robust").getBoolean();

	  //read experimental data
	  System.out.print("Extracting experimental data....");
	  ExperimentalRdcData experimentalData = null;
	  if (!rdcFile.isEmpty())
	  {
		  try
		  {
		  	experimentalData = ExperimentalRdcData.readCNS(new File(rdcFile));
		  }
		  catch (Exception e)
		  {
		  	try
		  	{
		  		experimentalData = ExperimentalRdcData.readPati(new File(rdcFile));
		  	}
		  	catch (Exception e2)
		  	{
		  		System.out.println("Could not parse RDC data.\n"+e2.getMessage());
		  		return 1;
		  	}
		  }
	  }

	  //adjust model number to start with 0
	  int model = options.get("model").getInteger()-1;
	  
	  //read the pdb file
	  PdbStructure pdb = null;
	  try
	  {
	  	pdb = MoleculeFileIO.readPDB(pdbFile);
	  }
	  catch(Exception e)
	  {
  		System.out.println("Could not parse PDB data.\n"+e.getMessage());
  		return 1;  	
	  }
		
	  //check that model number is ok
		if (pdb.getNumberOfModels()<=model)
		{
			System.out.println("Invalid model number: "+model+". Failed!");
			return 1;
		}
		
		Molecule molecule = pdb.getModel(model).createBasicMolecule();
		double volumeFraction = options.get("bicelle").getDouble();
		
		System.out.println("done.");
 		
		if (!rdcFile.isEmpty())
		{
			//compute the results
		  System.out.print("Computing results....");
		  PatiResults results;
		  try
		  {
		  	results = new PatiResults(experimentalData, molecule, robust);
		  }
		  catch (AtomNotFoundException e)
		  {
		  	System.out.println(e.getMessage());
		  	return 1;
		  }
			System.out.println("done.");
			
			//add listener
			results.addProgressObserver(new PatiMain());
			
			//display fit
			if (options.get("draw").getBoolean())
			{
			  System.out.println("Displaying fit...");
				new RdcFitPlot(results.getSolution()).repaint();
			}
			
			if (!options.get("dock").isBoolean())
			{
				ArrayList<ExperimentalRdcData> chainList = experimentalData.createRigid().seperateByChain();
				
				if (chainList.size()!=2 )
				{
					System.out.println("Docking must be performed on a two domain file only.");
					return 1;
				}
			}
			
			//compute the statistic
			if (!options.get("nostat").getBoolean())
			{
			  System.out.print("Computing statistics..");
				results.computeStatistic();
			  System.out.println("done.");
			  
			  if (!options.get("dockPdb").getString().isEmpty())
			  {
				  System.out.print("Computing alignment uncertainty..");
			  	computeAlignmentError(experimentalData, pdb.getModel(model), 150, robust);
			  }			  
			}
			
		  //show backcalculatation results
			System.out.println(results.generateOutputString());
			
			//show prediction results
			System.out.println(results.generatePredictionString(volumeFraction));
			
		  double lengthAddition = options.get("axeslength").getDouble();

		  if (!options.get("axes").getString().isEmpty())
			{
				String fileName = options.get("axes").getString();
				
				File axesFile = FileIO.ensureExtension(new File(fileName), "", "py");
			  System.out.print("Saving tensor axes to \""+axesFile+"\"...");
			  MoleculeFileIO.writePymolAxesScript(axesFile, pdb.getModel(model).createBasicMolecule(), results.getSolution().getTensor().getProperEigenDecomposition(), lengthAddition);
			  System.out.println("done.");
			  
			  //now save the predicted axes
				PatiPredictor predictor = PatiPredictor.createBicellePredictor(volumeFraction);
				AlignmentTensor tensor = predictor.predictTensor(molecule);

				axesFile = FileIO.ensureExtension(new File(fileName), "_predicted", "py");
			  System.out.print("Saving predicted tensor axes to \""+axesFile+"\"...");
			  MoleculeFileIO.writePymolAxesScript(axesFile, pdb.getModel(model).createBasicMolecule(), tensor.getProperEigenDecomposition(), lengthAddition);
			  System.out.println("done.\n");
			}

			
			//if docking version turned on
			if (options.get("dock").getBoolean())
			{
				String dockPdbName = options.get("dockPdb").getString();
				
				System.out.print("Computing docking results...");
				dockResults(dockPdbName, experimentalData, pdb, model, volumeFraction, robust);
				System.out.println("");
				
				if (!options.get("axes").getString().isEmpty())
				{
					String dockAxesFile = FileIO.removeExtension(options.get("axes").getString());
					
				  System.out.print("Saving docking tensor axes to \""+dockAxesFile+"*.py\"...");
				  
				  //read the pdb file
				  PdbStructure dockedPdb = MoleculeFileIO.readPDB(new File(dockPdbName));
				  
				  for (int iter=0; iter<dockedPdb.getNumberOfModels(); iter++)
				  {
				  	String name1 = dockedPdb.getModel(iter).getModelChains().get(0);
				  	String name2 = dockedPdb.getModel(iter).getModelChains().get(1);
				  	Molecule mol1 = dockedPdb.getModel(iter).createChain(0).createBasicMolecule();
				  	Molecule mol2 = dockedPdb.getModel(iter).createChain(1).createBasicMolecule();
				  	
			  	  ExperimentalRdcData data1 = experimentalData.createFromExistingAtoms(mol1);	  
			  	  ExperimentalRdcData data2 = experimentalData.createFromExistingAtoms(mol2);
			  	  
			    	AlignmentComputor d1Computor = new AlignmentComputor(data1, results.getComputor().isRobust(), results.getComputor().getOutlierThreshold());
			    	AlignmentComputor d2Computor = new AlignmentComputor(data2, results.getComputor().isRobust(), results.getComputor().getOutlierThreshold());

			    	AlignmentTensor tensor1 = d1Computor.computeTensor(mol1);
			    	AlignmentTensor tensor2 = d2Computor.computeTensor(mol2);
				  	
					  MoleculeFileIO.writePymolAxesScript(new File(dockAxesFile.concat("_"+name1+"_"+(iter+1)+".py")), mol1, tensor1.getProperEigenDecomposition(), lengthAddition);
					  MoleculeFileIO.writePymolAxesScript(new File(dockAxesFile.concat("_"+name2+"_"+(iter+1)+".py")), mol2, tensor2.getProperEigenDecomposition(), lengthAddition);
				  }
				  
				  System.out.println("done.");
				}

			}
		}
		else
			System.out.println(PatiResults.generatePatiString(molecule, volumeFraction, null));
				
		return 0;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ProgressObserver#update(double)
	 */
	@Override
	public synchronized void update(double progressPercentage)
	{
		if (this.lastResult+.10<progressPercentage)
		{
			System.out.format("%.0f%%..", progressPercentage*100);		
			this.lastResult = progressPercentage;
		}
	}

}
