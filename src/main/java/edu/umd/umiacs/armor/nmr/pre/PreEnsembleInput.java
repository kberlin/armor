/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PrimaryStructure;
import edu.umd.umiacs.armor.refinement.AbstractLinearEnsembleInput;

public class PreEnsembleInput extends AbstractLinearEnsembleInput
{
	private final List<ExperimentalPreData> experimentalData;
	private final ArrayList<String> probeChainId;
	private final List<ExperimentalPreData> probePositionData;
	private final List<PrimaryStructure> primaryPositions;

	/**
	 * 
	 */
	private static final long serialVersionUID = 2583490329069276069L;

	public PreEnsembleInput(List<ExperimentalPreData> experimentalData, List<String> probeChainId) throws PreDataException
	{
		super(true);
		
		this.primaryPositions = null;
		
		if (experimentalData.size()!=probeChainId.size())
			throw new PreRuntimeException("Number of data points must equal number of probe domain locations.");
			
		this.probeChainId = new ArrayList<String>(probeChainId);
		
		this.probePositionData = new ArrayList<ExperimentalPreData>(experimentalData.size());
		int count = 0;
		for (ExperimentalPreData probeData : experimentalData)
		{
			this.probePositionData.add(probeData.createFromChain(this.probeChainId.get(count)).createRigid());
			
			if (this.probePositionData.get(count).size()<3)
				throw new PreRuntimeException("Not enough data found for probe number "+count+".");
			
			count++;
		}
		
		this.experimentalData = new ArrayList<ExperimentalPreData>(experimentalData.size());
		count = 0;
  	for (ExperimentalPreData probeData : experimentalData)
		{
			ArrayList<PreDatum> datumList = new ArrayList<PreDatum>();
			for (PreDatum datum : probeData)
				if (!datum.getAtom().getPrimaryInfo().getChainID().equals(this.probeChainId.get(count)))
					datumList.add(datum);
			
			this.experimentalData.add(new ExperimentalPreData(datumList));
			count++;
		}
	}
	
	public PreEnsembleInput(List<ExperimentalPreData> experimentalData, List<PrimaryStructure> primaryInfo, boolean nothing) throws PreDataException
	{
		super(true);
		
		this.probeChainId = null;
		this.probePositionData = null;
		this.experimentalData = experimentalData;
		
		this.primaryPositions = new ArrayList<PrimaryStructure>(primaryInfo);
	}

	@Override
	public ExperimentalPreData getExperimentalData()
	{
		ArrayList<PreDatum> fullList = new ArrayList<PreDatum>();
		for (ExperimentalPreData data : this.experimentalData)
			for (PreDatum datum : data)
				fullList.add(datum.createAsDeltaR2Type());
		
		return new ExperimentalPreData(fullList);
	}
	
	public ExperimentalPreData getOriginalExperimentalData()
	{
		ArrayList<PreDatum> fullList = new ArrayList<PreDatum>();
		for (ExperimentalPreData data : this.experimentalData)
			for (PreDatum datum : data)
				fullList.add(datum);
		
		return new ExperimentalPreData(fullList);
	}
	
	public double optimalTauC(Molecule mol, ExperimentalPreData data, final Point3d pos) throws AtomNotFoundException
	{
		//generate the target and the associated weights
	  final double[] targetValues = data.values();
	  final double[] errors = data.errors();
	  
  	final PrePositionComputor computor = new PrePositionComputor(data, false, true);
  	final ArrayList<Point3d> positions = computor.extractPositions(mol);
	  
	  //create the optimizers
	  MultivariateVectorFunction prePredictionFunction = new MultivariateVectorFunction()
		{
			@Override
			public final double[] value(double[] x) throws IllegalArgumentException
			{
				double[] predictedValues;
				predictedValues = computor.predict(positions, pos, x[0]);
							  
				return predictedValues;
			}
		};
	  
	  //create initial guess
	  double[] x0 = new double[]{data.get(0).getTauC()};

	  LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(2000);
		optimizer.setAbsPointDifference(1.0e-5);
		optimizer.setObservations(targetValues);
		optimizer.setErrors(errors);
		
	  optimizer.setFiniteDifferenceFunction(prePredictionFunction);
	  
	  //compute the result
	  OptimizationResultImpl result = optimizer.optimize(x0);
	  return result.getPoint()[0];
	}

	@Override
	public double[] predict(Molecule mol) throws AtomNotFoundException
	{
		ArrayList<Double> predictionList = new ArrayList<Double>();
		PrePositionComputor computor;
		
		//iterate through all the probes
		int probeCounter = 0;
		for (ExperimentalPreData data : this.experimentalData)
		{
			Point3d p = null;
			
			if (this.primaryPositions!=null)
			{
				//get the probe atom
				PrimaryStructure primary = this.primaryPositions.get(probeCounter);
				
				for (Atom a : mol)
				{
					if (a.getPrimaryInfo().equals(primary))
					{
						p = a.getPosition();
						break;
					}
				}
			}
			else
			{
				//get the info to compute probe position
				ExperimentalPreData probeRigidData = this.probePositionData.get(probeCounter);
				computor = new PrePositionComputor(probeRigidData, false, false);
	
				//predict the location
				p = computor.solvePosition(mol);

				//System.out.println(optimalTauC(mol, probeRigidData, p)+" "+p);				
			}
			
			if (p==null)
				throw new PreRuntimeException("Could not locate the position of a probe.");
			
			PrePositionComputor predictor = new PrePositionComputor(data, false);
			double[] prediction = predictor.predictDeltaR2(mol, p);
			//double[] prediction = predictor.predictDeltaR2Cutoff(mol, p);
			for (double value : prediction)
				predictionList.add(value);
			
			probeCounter++;
		}
		
		//combine the lists from different probes
		double[] prediction = new double[predictionList.size()];
		probeCounter = 0;
		for (double value : predictionList)
		{
			prediction[probeCounter] = value;
			probeCounter++;
		}
		
		return prediction;
	}

}
