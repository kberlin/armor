/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Interface MFSpectralDensityFunction.
 * 
 * @param <E>
 *          the element type
 */
public interface MFSpectralDensityFunction<E extends MFSpectralDensityFunction<E>> extends SpectralDensityFunction
{	
	public E createWithLocal(double S2, double tauLocal);
	
	public E createWithLocal(double S2Slow, double tauLocal, double S2Fast, double tauLocalFast);

	public E createWithS2Fast(double S2Fast);
	
	public E createWithS2Slow(double S2Slow);

	public E createWithTauFast(double tauFast);
	
	public E createWithTauLocal(double tauLocal);

	public E createWithTensor(RotationalDiffusionTensor tensor);
	
	public E createWithVector(Point3d bondVector);
	
	public double getS2();
	
	public double getS2Fast();

	public double getS2Slow();

	public double getTauC();

	public double getTauFast();
	
	public double getTauLocal();

	public RotationalDiffusionTensor getTensor();

	public double valueOrdered(double omega);
}