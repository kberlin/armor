/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.math.stat.NormalDistribution;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.physics.BasicPhysics;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.NonNegativeExperimentalDatum;
import edu.umd.umiacs.armor.util.Pair;

public final class PreDatum implements ExperimentalDatum
{
	public enum ExperimentType
	{
		DELTA_R2,
		RATIO;
	}
	
	private final Atom atom;
	
	private final NonNegativeExperimentalDatum deltaR2;
	private final double distance;
	private final ExperimentType experimentType;
	private final double freq;
	private final double maxR2;
	private final NonNegativeExperimentalDatum ratio;
	private final boolean rigid;
	private final double scaleValue;
	private final double t2Dia;
	private final double tauC;
	private final double time;
	private final PreType type;
	private static final int NUM_ERROR_ESTIMATES = 1000;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2460988004471551360L;

	private static double getR2Diamagnatic(double t2Dia)
	{
		return 1.0/(t2Dia*1.0e-3);
	}
	
	private static double predictDeltaR2Helper(double distSquared, double scale)
  {
		double dist6 = distSquared*distSquared*distSquared;		
		double deltaR2 = scale/dist6;

		return deltaR2;
  }
	
	public static double predictRatio(double deltaR2, double t2Dia, double time)
	{
		time = time*1.0e-3;
		double r2Diamagnatic = getR2Diamagnatic(t2Dia);
		return r2Diamagnatic*Math.exp(-deltaR2*time)/(r2Diamagnatic+deltaR2);
	}

	public PreDatum(NonNegativeExperimentalDatum deltaR2, double tauC, double freq, Atom atom, PreType type, boolean rigid)
	{
		this(null, null, deltaR2, tauC, freq, 0.0, 0.0, atom, type, rigid);
	}
	
	
	public PreDatum(NonNegativeExperimentalDatum iOx, NonNegativeExperimentalDatum iRed, double tauC, double freq, double time, double t2Dia, Atom atom, PreType type, boolean rigid)
	{
		this(iOx, iRed, null, tauC, freq, time, t2Dia, atom, type, rigid);
	}
	
	private PreDatum(NonNegativeExperimentalDatum iOx, NonNegativeExperimentalDatum iRed, NonNegativeExperimentalDatum deltaR2, double tauC, double freq, double time, double t2Dia, Atom atom, PreType type, boolean rigid)
	{
		this.rigid = rigid;

		if (Double.isNaN(freq) || freq<0.0)
			throw new PreRuntimeException("Invalid value for frequency: "+freq+".");
		this.freq = freq;
		
		this.atom = atom;
		this.type = type;
		
 		this.time = time;
		this.t2Dia = t2Dia;
		
		//compute deltaR2 value
		if (deltaR2!=null)
		{
			this.experimentType = ExperimentType.DELTA_R2;
			this.ratio = null;
			
			//store value
			this.deltaR2 = deltaR2;
		}
		else
		{
			this.experimentType = ExperimentType.RATIO;

			Pair<Double, Double> values = computeRatioAndDeltaR2(iOx.value(), iRed.value(), 0.0);
			
			//give same distrubtion so results are consistant between runs
			NormalDistribution distribution = new NormalDistribution(1);
			
			//perform statistics
			double[] ratioValues = new double[NUM_ERROR_ESTIMATES];
			double[] deltaR2Values = new double[NUM_ERROR_ESTIMATES];
			for (int iter=0; iter<ratioValues.length; iter++)
			{
				double iOxValue = distribution.sample(iOx.value(), iOx.error());
				double iRedValue = distribution.sample(iRed.value(), iRed.error());
				
				Pair<Double, Double> errorValues = computeRatioAndDeltaR2(iOxValue, iRedValue, values.y);
				
				//TODO fix this
				ratioValues[iter] = errorValues.x;
				if (!Double.isInfinite(errorValues.y))
					deltaR2Values[iter] = errorValues.y;
				else
					deltaR2Values[iter] = values.y;
			}
						
			this.ratio = new NonNegativeExperimentalDatum(values.x, BasicStat.std(ratioValues, values.x));
			this.deltaR2 = new NonNegativeExperimentalDatum(values.y, BasicStat.std(deltaR2Values, values.y));
		}
		
		this.tauC = tauC;
    this.scaleValue = computeScale(this.tauC);
    
    //compute the maxium possible value for deltaR2
		this.maxR2 = Math.min(predictDeltaR2Helper(this.type.getMinDistance(), this.scaleValue), 100.0);
				
		//compute distance in Angstroms
		double distance = Math.pow(this.scaleValue/this.deltaR2.value(), 1.0/6.0);

		//check that the distance makes sense
		if (Double.isInfinite(this.distance))
		{
			this.distance = Double.NaN;
		}
		else
			this.distance = distance;
		
		//System.err.println((predictDeltaR2(this.atom.getPosition(), this.atom.getPosition().add(new Point3d(0,0,this.distance)), 11)-this.deltaR2.value())/this.deltaR2.value());
	}
	
	private PreDatum(PreDatum datum, ExperimentType type)
	{
		this.tauC = datum.tauC;
		this.type = datum.type;
		this.time = datum.time;
		this.rigid = datum.rigid;
		this.freq = datum.freq;
		
		this.ratio = datum.ratio;
		this.deltaR2 = datum.deltaR2;
		this.t2Dia = datum.t2Dia;
		this.scaleValue = datum.scaleValue;
		this.distance = datum.distance;
		this.experimentType = type;
		this.maxR2 = datum.maxR2;

		this.atom = datum.atom;
	}
	
	public PreDatum(PreDatum datum, Point3d position)
	{
		this.tauC = datum.tauC;
		this.type = datum.type;
		this.time = datum.time;
		this.rigid = datum.rigid;
		this.freq = datum.freq;
		
		this.ratio = datum.ratio;
		this.deltaR2 = datum.deltaR2;
		this.t2Dia = datum.t2Dia;
		this.scaleValue = datum.scaleValue;
		this.distance = datum.distance;
		this.experimentType = datum.experimentType;
		this.maxR2 = datum.maxR2;

		this.atom = datum.atom.createWithNewPosition(position);
	}

	private Pair<Double, Double> computeRatioAndDeltaR2(double iOx, double iRed, double deltaR2InitGuess)
	{		
		if (Double.isNaN(this.time))
			throw new PreRuntimeException("time cannot be NaN.");		
 		
		if (Double.isNaN(this.t2Dia))
			throw new PreRuntimeException("t2Diamagnatic cannot be NaN.");

		if (Double.isNaN(iOx))
			throw new PreRuntimeException("iOx cannot be NaN.");

		if (Double.isNaN(iRed))
			throw new PreRuntimeException("iRed cannot be NaN.");

		//make sure ratio takes on reasonable value
		double ratioValue = iOx/iRed;
		if (ratioValue > 1.0)
		{
			ratioValue = 1.0;
		}
		
		double deltaR2Value;
	 	if (ratioValue>1.0e-8)
 		{	 		
 		  //create the optimizer that will sovle for the r2dia
	 		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
	 		optimizer.setNumberEvaluations(10000);
	 		optimizer.setAbsPointDifference(1.0e-6);
				
	 		//optimize for the deltaR2value
			final double time = this.time;
			final double t2dia = this.t2Dia;
			MultivariateVectorFunction f = new MultivariateVectorFunction()
			{			
				@Override
				public final double[] value(double[] deltaR2) throws IllegalArgumentException
				{
					return new double[]{predictRatio(deltaR2[0], t2dia, time)};
				}
			};
			
			optimizer.setObservations(new double[]{ratioValue});
			optimizer.setErrors(new double[]{1.0});
			optimizer.setFiniteDifferenceFunction(f);
			OptimizationResultImpl sol = optimizer.optimize(new double[]{deltaR2InitGuess});
			
			//compute deltaR2 value
			deltaR2Value = Math.max(0.0, sol.getPoint()[0]);
 		}
 		else
 			deltaR2Value = Double.POSITIVE_INFINITY;
				
		//adjust deltaR2 if needed
		if (deltaR2Value>this.maxR2)
		{
			//deltaR2Value = this.maxR2;
		}		
		
		return new Pair<Double,Double>(ratioValue, deltaR2Value);
	}

	public double computeScale(double tauC)
	{
		if (Double.isNaN(tauC))
			throw new PreRuntimeException("tauC cannot be NaN.");		
		tauC = tauC*1.0e-9;
		
		//compute the scale value
		double omega = BasicPhysics.larmorFrequency(this.freq, this.atom.getType().getGyromagneticRatio());
    double omegaTau = omega*tauC;
    
    //in angstrom units (1.0e60)
		double scaleValue = (1.0/15.0)*this.atom.getType().getSpin()*(1.0+this.atom.getType().getSpin())  
				*BasicMath.square(this.atom.getType().getGyromagneticRatio())
				*BasicMath.square(this.type.getElectronicGFactor())
				*(BasicMath.square(BasicPhysics.muB)*1.0e60)
				*(4.0*tauC+3.0*tauC/(1.0+omegaTau*omegaTau));

		return scaleValue;
	}
	
	public PreDatum createAsDeltaR2Type() 
	{
		return new PreDatum(this, ExperimentType.DELTA_R2);
	}

	public PreDatum createFromMolecule(Molecule mol) throws AtomNotFoundException
	{
		return new PreDatum(this, mol.getAtomPosition(this.atom));
	}
	
	public PreDatum createFromRigidTransform(RigidTransform t)
	{
		return new PreDatum(this, t.transform(this.atom.getPosition()));
	}

	@Override
	public double error()
	{
		if (this.experimentType == ExperimentType.RATIO)
			return this.ratio.error();
		
		return this.deltaR2.error();
	}

	/**
	 * @return the atom
	 */
	public Atom getAtom()
	{
		return this.atom;
	}
	
	public BaseExperimentalDatum getDatum()
	{
		if (this.experimentType == ExperimentType.RATIO)
		return this.ratio;
		
		return this.deltaR2;
	}

	public NonNegativeExperimentalDatum getDeltaR2()
	{
		return this.deltaR2;
	}

	/**
	 * @return the distance
	 */
	public double getDistance()
	{
		return this.distance;
	}

	/**
	 * @return the experimentType
	 */
	public ExperimentType getExperimentType()
	{
		return this.experimentType;
	}

	/**
	 * @return the freq
	 */
	public double getFreq()
	{
		return this.freq;
	}
	
	public double getMaxR2Delta()
	{
		return this.maxR2;
	}

	/**
	 * @return the ratio
	 */
	public NonNegativeExperimentalDatum getRatio()
	{
		return this.ratio;
	}
	
  @Override
	public double getRelativeValue()
	{
		return value()/error();
	}

	/**
	 * @return the scaleValue
	 */
	public double getScaleValue()
	{
		return this.scaleValue;
	}
	
	/**
	 * @return the tauC
	 */
	public double getTauC()
	{
		return this.tauC;
	}

	/**
	 * @return the type
	 */
	public PreType getType()
	{
		return this.type;
	}

	public boolean hasActiveDistance()
	{
		double distance = getDistance();
		return (!Double.isNaN(distance) && !Double.isInfinite(distance) && distance>=getType().getMinDistance() && distance<=getType().getMaxDistance());
	}
	
	@Override
	public boolean hasError()
	{
		return !Double.isNaN(error());
	}

	@Override
	public boolean hasValue()
	{
		return !Double.isNaN(value());
	}

	@Override
	public boolean isInfinite()
	{
		return Double.isInfinite(value());
	}

	@Override
	public boolean isNaN()
	{
		return Double.isNaN(value());
	}

	@Override
	public boolean isNaNOrInfinite()
	{
		double val = value();
		return Double.isNaN(val) || Double.isInfinite(val);
	}

	/**
	 * @return the rigid
	 */
	public boolean isRigid()
	{
		return this.rigid;
	}

	public double predict(Point3d prePosition, Point3d atomPosition)
	{
		double deltaR2 = predictDeltaR2(prePosition, atomPosition);
		
		if (this.experimentType == ExperimentType.DELTA_R2)
			return deltaR2;
		
		return predictRatio(deltaR2);
	}
	
	public double predict(Point3d prePosition, Point3d atomPosition, double tauC)
	{
		double deltaR2 = predictDeltaR2(prePosition, atomPosition, tauC);
		
		if (this.experimentType == ExperimentType.DELTA_R2)
			return deltaR2;
		
		return predictRatio(deltaR2);
	}
	
	public double predictDeltaR2(Point3d prePosition, Point3d atomPosition)
  {
		double distSquared = prePosition.distanceSquared(atomPosition);
		return predictDeltaR2Helper(distSquared, this.scaleValue);
  }

	public double predictDeltaR2(Point3d prePosition, Point3d atomPosition, double tauC)
  {
		double distSquared = prePosition.distanceSquared(atomPosition);
		return predictDeltaR2Helper(distSquared, computeScale(tauC));
  }
	
	public double predictRatio(double deltaR2) throws PreRuntimeException
	{
		if (this.experimentType != ExperimentType.RATIO)
			throw new PreRuntimeException("Cannot predict ratio from deltaR2, since no value for r2Diamagnatic and time is known.");
		
		return predictRatio(deltaR2, this.t2Dia, this.time);
	}
	
	@Override
	public double absErrorByStd(double predictedValue)
	{
		if (hasError())
			return (predictedValue-value())/error();
		
		return predictedValue-value();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "[Value="+value()+ ", atom=" + this.atom+ ", distance=" + this.distance
				+ ", tauC=" + getTauC() + ", freq=" + this.freq 	+ ", type=" + this.type + ", rigid=" + this.rigid + "]";
	}

	@Override
	public double value()
	{
		if (this.experimentType == ExperimentType.RATIO)
			return this.ratio.value();

		return this.deltaR2.value();
	}

	@Override
	public String valueInfoString()
	{
 		return String.format("%d %s %s", getAtom().getPrimaryInfo().getResidueNumber(),
				getAtom().getPrimaryInfo().getChainID(),
				getAtom().getPrimaryInfo().getAtomName());

	}
}
