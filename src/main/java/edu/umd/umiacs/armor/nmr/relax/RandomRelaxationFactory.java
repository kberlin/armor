/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomType;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.AtomImpl;
import edu.umd.umiacs.armor.molecule.PrimaryStructure;
import edu.umd.umiacs.armor.molecule.SecondaryStructure;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.NonNegativeExperimentalDatum;

/**
 * A factory for creating RandomRelaxation objects.
 */
public class RandomRelaxationFactory
{
	
	/** The atom full name p. */
	private String atomFullNameP;
	
	/** The atom full name q. */
	private String atomFullNameQ;
	
	/** The field strength. */
	private double[] fieldStrength;
	
	/** The generate noe. */
	private boolean generateNOE;
	
	/** The noe error. */
	private double noeError;
	
	/** The r1 error. */
	private double r1Error;
	
	/** The r2 error. */
	private double r2Error;
	
	/** The tensor. */
	private RotationalDiffusionTensor tensor;

	private double etaXyError;

	private double etaZError;
	
	/**
	 * Instantiates a new random relaxation factory.
	 * 
	 * @param fieldStrength
	 *          the field strength
	 * @param fromAtomName
	 *          the from atom name
	 * @param toAtomName
	 *          the to atom name
	 */
	public RandomRelaxationFactory(double[] fieldStrength, String fromAtomName, String toAtomName)
	{
		this.fieldStrength = fieldStrength;
		
		this.r1Error = 0.03;
		this.r2Error = 0.03;
		this.noeError = 0.05;
		this.etaXyError = 0.05;
		this.etaZError = 0.05;
	
		this.atomFullNameP = fromAtomName;
		this.atomFullNameQ = toAtomName;
		
		this.generateNOE = true;
		
		Rotation rotation = Rotation.createZYZ(BasicStat.uniformDistributionSample(0, BasicMath.PI/2.0),
				BasicStat.uniformDistributionSample(0, BasicMath.PI/2.0),
				BasicStat.uniformDistributionSample(0, BasicMath.PI/2.0));
				
		this.tensor = new RotationalDiffusionTensor(2.0, 3.0, 5.0, rotation);
	}
	
	/**
	 * Generate bond.
	 * 
	 * @param residue
	 *          the residue
	 * @return the bond
	 */
	private Bond generateBond(int residue)
	{
		Point3d atom2Point = new Point3d(BasicStat.uniformDistributionSample(0.0, 1.0), 
				 BasicStat.uniformDistributionSample(0.0, 1.0), 
				 BasicStat.uniformDistributionSample(0.0, 1.0));
		
    PrimaryStructure primary1 = new PrimaryStructure(residue, this.atomFullNameP, "A", "UKW", 0.0);
    PrimaryStructure primary2 = new PrimaryStructure(residue, this.atomFullNameQ, "A", "UKW", 0.0);
    SecondaryStructure secondary1 = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
    SecondaryStructure secondary2 = new SecondaryStructure(SecondaryStructure.Type.DISORDERED);
		
		AtomImpl a1 = new AtomImpl(new Point3d(0,0,0), primary1, secondary1);
		AtomImpl a2 = new AtomImpl(atom2Point, primary2, secondary2);

		Bond bond = new Bond(a1, a2);
		
		return bond;
	}
	
	/**
	 * Generate ordered relaxation list.
	 * 
	 * @param n
	 *          the n
	 * @param noise
	 *          the noise
	 * @return the experimental relaxation
	 */
	public ExperimentalRelaxationData generateOrderedRelaxationList(int n, boolean noise)
	{
		return generateRandomRelaxationList(n, 0.8, 0.0, noise);
	}
	
	/**
	 * Generate perfectly ordered relaxation list.
	 * 
	 * @param n
	 *          the n
	 * @param noise
	 *          the noise
	 * @return the experimental relaxation
	 */
	public ExperimentalRelaxationData generatePerfectlyOrderedRelaxationList(int n, boolean noise)
	{
		return generateRandomRelaxationList(n, 1.0, 0.0, noise);
	}

	/**
	 * Generate random relaxation list.
	 * 
	 * @param numElements
	 *          the num elements
	 * @param minS2
	 *          the min s2
	 * @param rexPercentage
	 *          the rex percentage
	 * @param noise
	 *          the noise
	 * @return the experimental relaxation
	 */
	public ExperimentalRelaxationData generateRandomRelaxationList(double numElements, double minS2, double rexPercentage, boolean noise)
	{
		ArrayList<BondRelaxation> bondRelaxationList = new ArrayList<BondRelaxation>();
		
		for (int iter=0; iter<numElements; iter++)
		{
			Bond bond = generateBond(iter+1);
			ModelFreeBondConstraints type = ModelFreeBondConstraints.getBondConstants(bond.getBondType());
			
			//generate the random data
			double s2 = BasicStat.uniformDistributionSample(minS2, 1.0);
			double tauLocal = BasicStat.uniformDistributionSample(0.001, .1);
			double csa = BasicStat.uniformDistributionSample(type.minCSA, type.maxCSA);
			double csaAngle = BasicStat.uniformDistributionSample(type.minCsaAngle, type.maxCsaAngle);
			double radius = BasicStat.uniformDistributionSample(bond.getBondType().getMinRadius(), bond.getBondType().getMaxRadius());
			
			double rex = 0.0;
		  if (Math.random()<rexPercentage)
		  {
		  	if (bond.getFromAtom().getType()==AtomType.NITROGEN)
		  		rex = BasicStat.uniformDistributionSample(5.0, 10.0);
		  	else
		  	if (bond.getFromAtom().getType()==AtomType.CARBON)
		  		rex = BasicStat.uniformDistributionSample(25.0, 50.0);
		  	else
		  		throw new RelaxationRuntimeException("Uknown atom type.");
		  }

		  /*
			radius = type.expRadius;
			csa = type.expCSA;
			rex = 0.0;
			s2 = 1.0;
			tauLocal = 0.0;
			*/
			
			//predict the data
			AnisotropicSpectralDensityFunction J = AnisotropicSpectralDensityFunction.create(this.tensor, 
					bond.getNormVector(), 
					s2, tauLocal,
					1.0, 0.0);
			AnisotropicModelFreePredictor predictor = new AnisotropicModelFreePredictor(J, bond, csa, radius, csaAngle, rex);
			
		  ArrayList<RelaxationDatum> data = new ArrayList<RelaxationDatum>();
			for (int fieldIter=0; fieldIter<this.fieldStrength.length; fieldIter++)
			{
				RelaxationDatum prediction = predictor.predict(this.fieldStrength[fieldIter]);
				
			  //add noise
				RelaxationDatum datum;
			  if (noise)
			  {
			  	double r1 = BasicStat.normalDistributionSamplePositive(prediction.getR1().value(), this.r1Error);
			  	double r2 = BasicStat.normalDistributionSamplePositive(prediction.getR2().value(), this.r2Error);
			  	double noe = BasicStat.normalDistributionSample(prediction.getNoe().value(), this.noeError);
			  	double eta_xy = BasicStat.normalDistributionSample(prediction.getEtaXy().value(), this.etaXyError);
			  	double eta_z = BasicStat.normalDistributionSample(prediction.getEtaZ().value(), this.etaZError);
			  			  	
			  	datum = new RelaxationDatum(bond, prediction.getConstraints(), new NonNegativeExperimentalDatum(r1, this.r1Error), 
			  			new NonNegativeExperimentalDatum(r2, this.r2Error),
			  			new BaseExperimentalDatum(noe, this.noeError),
			  			new BaseExperimentalDatum(eta_xy, this.etaXyError),
			  			new BaseExperimentalDatum(eta_z, this.etaZError),
			  			this.fieldStrength[fieldIter], true);
			  }
			  else
			  	datum = new RelaxationDatum(bond, prediction.getConstraints(),
			  			new NonNegativeExperimentalDatum(prediction.getR1().value(), this.r1Error),
			  			new NonNegativeExperimentalDatum(prediction.getR2().value(), this.r2Error),
			  			new BaseExperimentalDatum(prediction.getNoe().value(),this.noeError),
			  			new BaseExperimentalDatum(prediction.getEtaXy().value(), this.etaXyError),
			  			new BaseExperimentalDatum(prediction.getEtaZ().value(), this.etaZError),
			  			this.fieldStrength[fieldIter], true);
			  
			  if (!this.generateNOE)
			  {
			  	datum = new RelaxationDatum(bond, 
			  			prediction.getConstraints(),
			  			new NonNegativeExperimentalDatum(datum.getR1().value(), datum.getR1().error()),
			  			new NonNegativeExperimentalDatum(datum.getR2().value(), datum.getR2().error()),
			  			new NonNegativeExperimentalDatum(Double.NaN,Double.NaN),
			  			new NonNegativeExperimentalDatum(prediction.getEtaXy().value(), this.etaXyError),
			  			new NonNegativeExperimentalDatum(prediction.getEtaZ().value(), this.etaZError),
			  			datum.getFrequency(), true);
			  }
		  	
			  data.add(datum);
			}
		  
		  BondRelaxation bondRelaxation = new BondRelaxation(bond, data);  
		  bondRelaxationList.add(bondRelaxation);
		}
		
		return new ExperimentalRelaxationData(bondRelaxationList);
	}
	
	/**
	 * Gets the tensor.
	 * 
	 * @return the tensor
	 */
	public RotationalDiffusionTensor getTensor()
	{
		return this.tensor;
	}
	
	/**
	 * Sets the generate noe.
	 * 
	 * @param val
	 *          the new generate noe
	 */
	public void setGenerateNoe(boolean val)
	{
		this.generateNOE = val;
	}
	
	/**
	 * Sets the tensor.
	 * 
	 * @param tensor
	 *          the new tensor
	 */
	public void setTensor(RotationalDiffusionTensor tensor)
	{
		this.tensor = tensor;
	}
}
