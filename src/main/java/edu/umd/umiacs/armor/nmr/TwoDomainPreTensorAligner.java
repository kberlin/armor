/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.nmr.pre.PrePredictor;
import edu.umd.umiacs.armor.nmr.pre.TwoDomainPrePredictor;
import edu.umd.umiacs.armor.refinement.AbstractTwoDomainAligner;
import edu.umd.umiacs.armor.refinement.TwoDomainTensorAligner;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.ExperimentalDatum;

public final class TwoDomainPreTensorAligner extends AbstractTwoDomainAligner
{
	private final PrePredictor prePredictor;
	private final double preWeight;
	private final TwoDomainTensorAligner tensorAligner;
	
	public TwoDomainPreTensorAligner(TwoDomainTensorAligner tensorAligner, PrePredictor prePredictor, double preRelativeWeight)
	{
		super(false, 0.0);
		this.tensorAligner = tensorAligner;
		this.prePredictor = prePredictor;
		this.preWeight = BasicMath.sqrt(preRelativeWeight);
	}

	@Override
	public BaseExperimentalData getData()
	{
		ArrayList<BaseExperimentalDatum> combinedList = new ArrayList<BaseExperimentalDatum>();
		
		//add the tensor data
		for (ExperimentalDatum tensorDatum : this.tensorAligner.getData())
			combinedList.add(new BaseExperimentalDatum(tensorDatum.value(), tensorDatum.error()));
		
		//add the pre data
		for (ExperimentalDatum preDatum : this.prePredictor.getData())
			combinedList.add(new BaseExperimentalDatum(preDatum.value(), preDatum.error()/(this.preWeight)));
		
		return new BaseExperimentalData(combinedList);
	}

	@Override
	public List<Rotation> initRotations(Molecule m1, Molecule m2) throws AtomNotFoundException
	{
		return this.tensorAligner.align(m1, m2);
	}

	@Override
	public double[] predict(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		double[] tensorPred = this.tensorAligner.predict(mol1, mol2);
		
		//find optimal translation before return prediction
		TwoDomainPrePredictor prePredictor = this.prePredictor.createTwoDomainRigidPredictor(mol1, mol2);
		Point3d bestTranslation = prePredictor.computeTranslation(RigidTransform.identityTransform());		
		double[] prePred = prePredictor.predict(bestTranslation);
		
		return BasicUtils.combineArrays(tensorPred, prePred);
	}


}
