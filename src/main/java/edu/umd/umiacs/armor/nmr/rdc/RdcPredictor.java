/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Bond;
import edu.umd.umiacs.armor.molecule.Molecule;

/**
 * The Interface RdcPredictor.
 * 
 * @param <E>
 *          the element type
 */
public interface RdcPredictor extends Serializable
{
	
	/**
	 * Creates the with new order parameter.
	 * 
	 * @param orderParameter
	 *          the order parameter
	 * @return the e
	 */
	public RdcPredictor createWithNewOrderParameter(double orderParameter);	
	
	/**
	 * Creates the with new tensor.
	 * 
	 * @param A
	 *          the a
	 * @return the e
	 */
	public RdcPredictor createWithNewTensor(AlignmentTensor A);
	
	/**
	 * Creates the with new vector.
	 * 
	 * @param p
	 *          the p
	 * @return the e
	 */
	public RdcPredictor createWithNewVector(Point3d p);
	
	/**
	 * Gets the alignment tensor.
	 * 
	 * @return the alignment tensor
	 */
	public AlignmentTensor getAlignmentTensor();
	
	public Bond getBond();
	
	/**
	 * Gets the order parameter.
	 * 
	 * @return the order parameter
	 */
	public double getOrderParameter();

	/**
	 * Gets the scaling constant.
	 * 
	 * @return the scaling constant
	 */
	public double getScalingConstant();	
		
	/**
	 * Predict.
	 * 
	 * @return the double
	 */
	public double predict();
	
	public double predict(Molecule mol, AlignmentTensor A) throws AtomNotFoundException;
}
