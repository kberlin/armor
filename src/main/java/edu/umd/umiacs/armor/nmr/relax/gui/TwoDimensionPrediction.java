/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax.gui;

import java.awt.BasicStroke;
import java.awt.Font;
import java.awt.Paint;
import java.util.ArrayList;

import javax.swing.JFrame;

import org.jfree.chart.plot.DefaultDrawingSupplier;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.nmr.relax.BondRelaxation;
import edu.umd.umiacs.armor.nmr.relax.RelaxationRatePredictor;
import edu.umd.umiacs.armor.nmr.relax.RelaxationSolution;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.plot.Limit;
import edu.umd.umiacs.armor.util.plot.Line;
import edu.umd.umiacs.armor.util.plot.Plot2d;

/**
 * The Class TwoDimensionPrediction.
 */
public class TwoDimensionPrediction extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7328099339167007636L;

	public TwoDimensionPrediction(RelaxationSolution<? extends RelaxationRatePredictor> solution)
	{
		setTitle("Two Dimension Prediction");
		setBounds(100, 100, 500, 600);
		//setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		Plot2d plot = new Plot2d();
		
		double[] theta = BasicUtils.uniformLinePoints(0.0, 180.0, 50);
		
		double minValue = Double.POSITIVE_INFINITY;
		double maxValue = Double.NEGATIVE_INFINITY;
		
		//only plot rigid data
		solution = solution.createRigid();
		
		//get the diffusion tensor rotation
		Rotation tensorRotation = solution.getTensor().getProperEigenDecomposition().getRotation();
		
		Point3d[] predictedPQUpper = new Point3d[theta.length];
		Point3d[] predictedPQLower = new Point3d[theta.length];
		for (int iter=0; iter<theta.length; iter++)
		{
			predictedPQUpper[iter] = tensorRotation.mult(sphericalToCartesian(theta[iter]/180.0*BasicMath.PI, BasicMath.PI/2.0)); 
			predictedPQLower[iter] = tensorRotation.mult(sphericalToCartesian(theta[iter]/180.0*BasicMath.PI, 0.0)); 
		}
		
		ArrayList<? extends RelaxationSolution<? extends RelaxationRatePredictor>> solutionList = solution.seperateByBondTypeAndFrequency();
		
		//store the paint methods for all the units
		ArrayList<Paint> paintList = new ArrayList<Paint>(solutionList.size());
		DefaultDrawingSupplier supplier = new DefaultDrawingSupplier();
		for (int index=0; index<solutionList.size(); index++)
			paintList.add(supplier.getNextPaint());	
		
		int listCounter = 0;
		for (RelaxationSolution<? extends RelaxationRatePredictor> list : solutionList)
		{
			RelaxationSolution<? extends RelaxationRatePredictor> currList = list;
						
			//compute the x-y values for the predicted lines
			double[] upperLineRhoValues = new double[predictedPQUpper.length];
			double[] lowerLineRhoValues = new double[predictedPQLower.length];
			BondRelaxation exampleBond = currList.getRelaxationData().getBondRelaxation(0);
			for (int iter=0; iter<theta.length; iter++)
			{
				RelaxationRatePredictor predictor = currList.getPredictor(0);
				upperLineRhoValues[iter] = predictor.createWithVector(predictedPQUpper[iter]).predictRho(exampleBond.getRelaxationDatum(0).getFrequency());
				lowerLineRhoValues[iter] = predictor.createWithVector(predictedPQLower[iter]).predictRho(exampleBond.getRelaxationDatum(0).getFrequency());
			}
			
			minValue = Math.min(minValue, BasicMath.min(lowerLineRhoValues));
			minValue = Math.min(minValue, BasicMath.min(upperLineRhoValues));
			maxValue = Math.max(maxValue, BasicMath.max(upperLineRhoValues));
			maxValue = Math.max(maxValue, BasicMath.max(lowerLineRhoValues));
			
			ArrayList<String> toolTips = new ArrayList<String>();
			double[] thetaExp = new double[list.size()];
			int counter = 0;
			for (RelaxationRatePredictor predictor : list.getPredictors())
			{
				toolTips.add(String.format("<html>Residue: %d<br>Type: %s-%s<br>Freq: %.2f", predictor.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
						predictor.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
						predictor.getBond().getToAtom().getPrimaryInfo().getAtomName(),
						exampleBond.getRelaxationDatum(0).getFrequency()));
				
				thetaExp[counter] = Math.acos(tensorRotation.multInv(predictor.getBond().getNormVector()).z)*180.0/BasicMath.PI;
				
				counter++;
			}
			
			BaseExperimentalData rhoData = list.getRelaxationData().getRhoData();
			double[] rhoValues = rhoData.values();
			double[] rhoErrors = rhoData.errors();
		  minValue = Math.min(minValue, BasicMath.min(rhoValues));
			maxValue = Math.max(maxValue, BasicMath.max(rhoValues));
			
			String label = String.format("%s-%s, %.2f MHz", list.getRelaxationData().getBondRelaxation(0).getBond().getBondType().getFromAtomName(),
					list.getRelaxationData().getBondRelaxation(0).getBond().getBondType().getToAtomName(), 
					list.getRelaxationData().getBondRelaxation(0).getRelaxationDatum(0).getFrequency());
			
			
			Line rhoLine = new Line(thetaExp, rhoValues, rhoErrors, label, toolTips);
			rhoLine.setDrawLine(false);
			rhoLine.setDrawShape(true);
			rhoLine.setPaint(paintList.get(listCounter));
	
			Line upperLine = new Line(theta, upperLineRhoValues, label);
			upperLine.setDrawLine(true);
			upperLine.setDrawShape(false);
			upperLine.setStroke(new BasicStroke(2.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER, 10.0f, new float[]{10.0f,10.0f}, 0.0f));
			upperLine.setVisibleInLegend(false);
			upperLine.setPaint(paintList.get(listCounter));

			Line lowerLine = new Line(theta, lowerLineRhoValues, label);
			lowerLine.setDrawLine(true);
			lowerLine.setDrawShape(false);
			lowerLine.setStroke(new BasicStroke(2.0f));
			lowerLine.setVisibleInLegend(false);
			lowerLine.setPaint(paintList.get(listCounter));

			plot.addLine(upperLine);
			plot.addLine(lowerLine);
			plot.addErrorLine(rhoLine);
			
			listCounter++;
		}
		
		plot.setRenderingOrderReverse();

		plot.setXLabel("\u03B8");
		plot.setYLabel("\u03C1 Exp.");
		plot.setTitle("Experimental \u03C1", new Font("Ariel", Font.PLAIN, 14));
		
		//set the axes limits
		plot.setYAxisLimit(new Limit(minValue-minValue*.1, maxValue+maxValue*.1));
		
		
		//add the plot
		getContentPane().add(plot);
		
		setVisible(true);
	}
	
	/**
	 * Spherical to cartesian.
	 * 
	 * @param theta
	 *          the theta
	 * @param phi
	 *          the phi
	 * @return the point3d
	 */
	private Point3d sphericalToCartesian(double theta, double phi)
	{
	  return new Point3d(BasicMath.sin(theta)*BasicMath.cos(phi), BasicMath.sin(theta)*BasicMath.sin(phi), BasicMath.cos(theta));
	}
}
