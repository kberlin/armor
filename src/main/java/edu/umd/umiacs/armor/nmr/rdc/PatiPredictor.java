/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.TwoVariableVectorFunction;
import edu.umd.umiacs.armor.math.integration.SimpsonDoubleVectorQuad;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3dImpl;
import edu.umd.umiacs.armor.molecule.Molecule;

/**
 * The Class PatiPredictor.
 */
public final class PatiPredictor implements AligmentTensorPredictor
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3788798245275462456L;

	/**
	 * The Class AlignmentFunction.
	 */
	private final static class AlignmentFunction implements TwoVariableVectorFunction, Serializable
	{
		
		/** The eta. */
		private final MoleculeHeightFunction eta;
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 8227859389204174672L;
		
		/**
		 * Instantiates a new alignment function.
		 * 
		 * @param eta
		 *          the eta
		 */
		public AlignmentFunction(MoleculeHeightFunction eta) 
		{
			this.eta = eta;
		}
		
		/* (non-Javadoc)
		 * @see edu.umd.umiacs.armor.math.func.TwoVariableVectorFunction#value(double, double)
		 */
		@Override
		public double[] value(double alpha, double u)
		{
		  double ca2 = BasicMath.square(BasicMath.cos(alpha));
		  double u2 = u*u;
		  double sa = BasicMath.sin(alpha);
		  double ca = BasicMath.cos(alpha);
		  double sval = u*BasicMath.sqrt(1-u2);
			
			double[] F = new double[7];
			
      F[0] = (-3.0*ca2*u2+3.0*ca2-1.0);
    	//F[1] = (3.0*ca2*u2-3.0*ca2-3.0*u2+2.0);
    	F[2] = (3.0*u2-1.0);
    	F[3] = 1.0;
    	F[4] = (sa*ca*(u2-1.0));
    	F[5] = -sval*ca;
    	F[6] = sval*sa;
    	
    	//compute from the other two values
    	F[1] = -F[0]-F[2];
    	
    	double currEta = this.eta.value(alpha, u);
    	for (int iter=0; iter<F.length; iter++)
    	{
    		F[iter] = F[iter]*currEta;
    	}
    	  
			return F;
		}
	}
	
	/** The const ajustment. */
	private final double[] constAjustment;

	private final double h;
	
	private final double absError;
		
	/** The Constant ABSOLUTE_INTEGRATION_ERROR. */
	public static final double ABSOLUTE_INTEGRATION_ERROR = 2.0e-5;
	
	/** The Constant BICELLE_ALIGNMENT. */
	private static final double[] BICELLE_ALIGNMENT = {1.0, 0.0, 0.0};	
	
	/** The Constant BICELLE_THICKNESS. */
	private static final double BICELLE_THICKNESS = 40.0;
	
	/** The Constant PALES_ADJUSTMENT_FACTOR. */
	private static final double PALES_ADJUSTMENT_FACTOR = -.8*.8;
	
	/**
	 * Creates the bicelle predictor.
	 * 
	 * @param volumeFraction
	 *          the volume fraction
	 * @return the pati predictor
	 */
	public static PatiPredictor createBicellePredictor(double volumeFraction)
	{
		return new PatiPredictor(BICELLE_ALIGNMENT, BICELLE_THICKNESS/volumeFraction/2.0 , ABSOLUTE_INTEGRATION_ERROR);		
	}
	
	/**
	 * Gets the pati rotation.
	 * 
	 * @param alpha
	 *          the alpha
	 * @param u
	 *          the u
	 * @return the pati rotation
	 */
	public static Rotation getPatiRotation(double alpha, double u)
	{
		return Rotation.createZYZ(0.0, BasicMath.acos(u), alpha);				
	}
		
	/**
	 * Instantiates a new pati predictor.
	 * 
	 * @param B
	 *          the b
	 * @param h
	 *          the h
	 */
	public PatiPredictor(double[] B, double h)
	{
		this (B, h, ABSOLUTE_INTEGRATION_ERROR);
	}
	
	/**
	 * Instantiates a new pati predictor.
	 * 
	 * @param B
	 *          the b
	 * @param h
	 *          the h
	 * @param absError
	 *          the abs error
	 */
	public PatiPredictor(double[] B, double h, double absError)
	{
		this.h = h;
		this.absError = absError;
		
	  double Sc = 1.0-3.0*B[2]*B[2];
	  
	  this.constAjustment = new double[7];
	  
	  this.constAjustment[0] = Sc/(16.0*BasicMath.PI)*PALES_ADJUSTMENT_FACTOR;
	  this.constAjustment[1] = this.constAjustment[0];
	  this.constAjustment[2] = this.constAjustment[0];
	  
	  //the height constant
	  this.constAjustment[3] = -1.0/(4.0*BasicMath.PI);
	  
	  this.constAjustment[4] = 3.0*Sc/(16.0*BasicMath.PI)*PALES_ADJUSTMENT_FACTOR;
	  this.constAjustment[5] = this.constAjustment[4];
	  this.constAjustment[6] = this.constAjustment[4];
	}

	/**
	 * Predict tensor.
	 * 
	 * @param mol
	 *          the mol
	 * @return the alignment tensor
	 */
	@Override
	public AlignmentTensor predictTensor(Molecule mol)
	{
		QhullHeightFunction eta = new QhullHeightFunction(mol);
		AlignmentTensor tensor = predictTensor(eta);
				
		return tensor;
	}

	public AlignmentTensor predictTensor(MoleculeHeightFunction eta)
	{
		AlignmentFunction f = new AlignmentFunction(eta);
		SimpsonDoubleVectorQuad integrator = new SimpsonDoubleVectorQuad(this.absError, 1.0/(BasicMath.TWOPI*200.0));
		
		//integrate the function
		double[] Feta = integrator.integrate(f, 0.0, BasicMath.TWOPI, -1.0, 1.0, 50000);
		
		//high scale
		double N = this.h+Feta[3]*this.constAjustment[3];
		
		if (N<1.0e-12)
			throw new RdcRuntimeException("Alignment scaling value is too small or negative.");
		
		double Ninv = 1.0/N;

  	double[][] A = new double[3][3];

	  A[0][0] = Feta[0]*this.constAjustment[0]*Ninv;
	  A[1][1] = Feta[1]*this.constAjustment[1]*Ninv;
	  A[2][2] = Feta[2]*this.constAjustment[2]*Ninv;
	  
	  A[1][0] = Feta[4]*this.constAjustment[4]*Ninv;
	  A[2][0] = Feta[5]*this.constAjustment[5]*Ninv;
	  A[2][1] = Feta[6]*this.constAjustment[6]*Ninv;
	  
	  A[0][1] = A[1][0];
	  A[0][2] = A[2][0];
	  A[1][2] = A[2][1];
	  
	  return new AlignmentTensor(new SymmetricMatrix3dImpl(A, false));
	}
}
