/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.molecule.filters.MoleculeAligner;
import edu.umd.umiacs.armor.molecule.filters.MoleculeAlignmentException;
import edu.umd.umiacs.armor.molecule.filters.MoleculeSVDAligner;
import edu.umd.umiacs.armor.molecule.filters.StructureFilter;
import edu.umd.umiacs.armor.refinement.AbstractLinearEnsembleInput;
import edu.umd.umiacs.armor.refinement.RefinementRuntimeException;

public final class RdcEnsembleInput extends AbstractLinearEnsembleInput
{
	private final MoleculeAligner aligner;

	private final ArrayList<Molecule> domains;

	private final ExperimentalRdcData experimentalData;
	
	private final AligmentTensorPredictor predictor;
	
	private final StructureFilter structureFilter;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4631445213351284926L;
		
	public RdcEnsembleInput(ExperimentalRdcData experimentalData, AligmentTensorPredictor predictor)
	{
		this(experimentalData, predictor, null);
	}
	
	public RdcEnsembleInput(ExperimentalRdcData experimentalData, AligmentTensorPredictor predictor, List<? extends Molecule> domains)
	{
		this(experimentalData, predictor, domains, null);		
	}

	protected RdcEnsembleInput(ExperimentalRdcData experimentalData, AligmentTensorPredictor predictor, List<? extends Molecule> domains, StructureFilter structureFilter)
	{
		this(experimentalData, predictor, domains, structureFilter, null);
	}

	public RdcEnsembleInput(ExperimentalRdcData experimentalData, AligmentTensorPredictor predictor, List<? extends Molecule> domains, StructureFilter structureFilter, MoleculeAligner aligner)
	{
		super(true);
		
		this.experimentalData = experimentalData.createRigid();
		this.predictor = predictor;
		this.structureFilter = structureFilter;
		
		if (domains!=null && !domains.isEmpty())
			this.domains = new ArrayList<Molecule>(domains);
		else
			this.domains = null;
		
		if (aligner!=null)
			this.aligner = aligner;
		else
			this.aligner = new MoleculeSVDAligner("CA");
	}
	
	@Override
	public ExperimentalRdcData getExperimentalData()
	{
		return this.experimentalData;
	}

	public AligmentTensorPredictor getPredictor()
	{
		return this.predictor;
	}
	
	private Molecule createVectorOverlayedMolecule(Molecule mol) throws MoleculeAlignmentException
	{
		//update the vector orientations
		if (this.domains != null && !this.domains.isEmpty())
		{
			ArrayList<Molecule> alignedDomains = new ArrayList<Molecule>(this.domains.size());
			
			for (Molecule domain : this.domains)
			{
				alignedDomains.add(this.aligner.align(mol, domain));
			}
			
			mol = new MultiDomainComplex(alignedDomains);
		}
		
		return mol;
	}
	
	@Override
	public double[] predict(Molecule mol) throws AtomNotFoundException
	{
		if (this.structureFilter!=null)
			mol = this.structureFilter.filter(mol);
		
		//predict the alignment tensor on the original molecule
		AlignmentTensor tensor = this.predictor.predictTensor(mol);
		
		//create an overlayed molecule
		Molecule vectAdjMolecule;
		try
		{
			vectAdjMolecule = createVectorOverlayedMolecule(mol);
		}
		catch (MoleculeAlignmentException e)
		{
			throw new AtomNotFoundException(e.getMessage(), mol.getAtom(0));
		}
		
		//generate new computor
		AlignmentComputor computor = new AlignmentComputor(this.experimentalData);
		double[] predRdc = computor.predict(vectAdjMolecule, tensor);
		
		return predRdc;			
	}
	
	public RealMatrix getScaledVectorMatrix()
	{
		//generate new computor
		AlignmentComputor computor = new AlignmentComputor(this.experimentalData);

		ArrayList<RealMatrix> matrixList = new ArrayList<RealMatrix>();
		for (File pdbFile : this.getPdbFiles())
		{
			try
			{
			//read the file
				PdbStructure pdbStruct = MoleculeFileIO.readPDB(pdbFile);				
	
				for (int model=0; model<pdbStruct.getNumberOfModels(); model++)
				{
					//create the vector adjusted molecule
					Molecule mol = pdbStruct.getModel(model).createBasicMolecule();
					mol = createVectorOverlayedMolecule(mol);

					matrixList.add(computor.getScaledVectorMatrix(mol));
				}
			}
			catch (AtomNotFoundException e)
			{
				throw new RefinementRuntimeException(e);
			}
			catch (PdbException e)
			{
				throw new RefinementRuntimeException(e);
			}
			catch (MoleculeAlignmentException e)
			{
				throw new RefinementRuntimeException(e);
			}
		}
		
		BlockRealMatrix combinedVectorMatrix = new BlockRealMatrix(matrixList.get(0).numRows(), matrixList.size()*5);
		int offset = 0;
		for (RealMatrix currVectorMatrix : matrixList)
		{
			for (int row=0; row<currVectorMatrix.numRows(); row++)
				for (int column=0; column<currVectorMatrix.numColumns(); column++)
					combinedVectorMatrix.set(row, offset+column, currVectorMatrix.get(row, column));
			
			offset += currVectorMatrix.numColumns();
		}
	
		return combinedVectorMatrix;
	}
}
