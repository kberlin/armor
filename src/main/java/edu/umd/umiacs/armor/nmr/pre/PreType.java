/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.pre;

public enum PreType
{
	MTSL(1.99919558e-7, 8.0, 30.0);
	
	private final double electronicGFactor;
	private final double maxDistance;
	private final double minDistance;
	
	private PreType(double electronicGFactor, double minDistance, double maxDistance)
	{
		this.electronicGFactor = electronicGFactor;
		this.minDistance = minDistance;
		this.maxDistance = maxDistance;
	}

	/**
	 * @return the electronicGFactor
	 */
	public double getElectronicGFactor()
	{
		return this.electronicGFactor;
	}
	
	public double getMaxDistance()
	{
		return this.maxDistance;
	}
	
	public double getMinDistance()
	{
		return this.minDistance;
	}
}
