/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.LeastSquaresOptimizationResult;
import edu.umd.umiacs.armor.math.optim.LevenbergMarquardtOptimizer;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OptimizationRuntimeException;
import edu.umd.umiacs.armor.math.optim.OutlierDampingFunction;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;

/**
 * The Class AxiallySymmetricRelaxationComputor.
 */
public final class AxiallySymmetricRelaxationComputor extends AbstractModelFreeComputor<AnisotropicModelFreePredictor>
{	
	public AxiallySymmetricRelaxationComputor(ExperimentalRelaxationData relaxList)
	{
		this(relaxList, false);
	}
	
	protected AxiallySymmetricRelaxationComputor(ExperimentalRelaxationData relaxList, ArrayList<AnisotropicModelFreePredictor> predictors, 
			boolean optimizePredictor, boolean optimizeRho,
			boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		super(relaxList, predictors, optimizePredictor, optimizeRho, robust, outlierThreshold, absoluteDiffusionError);
	}

	public AxiallySymmetricRelaxationComputor(ExperimentalRelaxationData relaxList, ArrayList<AnisotropicModelFreePredictor> predictors, 
			ArrayList<ModelFreeBondConstraints> bondConstraints, boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		this(relaxList, predictors, false, true, robust, outlierThreshold, absoluteDiffusionError);
	}
	
	public AxiallySymmetricRelaxationComputor(ExperimentalRelaxationData relaxList,
			boolean robust, double outlierThreshold, double absoluteDiffusionError)
	{
		this(relaxList, AnisotropicRelaxationComputor.generatePredictors(relaxList), true, true, robust, outlierThreshold, absoluteDiffusionError);
	}
	
	public AxiallySymmetricRelaxationComputor(ExperimentalRelaxationData relaxList, boolean robust)
	{
		this(relaxList, robust, ABSOLUTE_DIFFUSION_ERROR);
	}
	
	public AxiallySymmetricRelaxationComputor(ExperimentalRelaxationData relaxList, boolean robust, double outlierThreshold)
	{
		this(relaxList, robust, outlierThreshold, ABSOLUTE_DIFFUSION_ERROR);
	}

	@Override
	protected TensorStorage computeTensor(
			final ArrayList<AnisotropicModelFreePredictor> predictors, 
			final ExperimentalRelaxationData data,
			boolean robust, 
			double outlierThreshold) throws AtomNotFoundException
	{
		//generate the target and the associated weights
		BaseExperimentalData rhoList = data.getRhoData();
	  double[] targetValues = rhoList.values();
	  double[] errors = rhoList.errors();
	  
  	//create the optimizers
	  MultivariateVectorFunction axiPredictionFunction = new MultivariateVectorFunction()
		{
			@Override
			public double[] value(double[] x) throws IllegalArgumentException
			{
				RotationalDiffusionTensor tensor = RotationalDiffusionTensor.createZYZ(BasicMath.square(x[0]), BasicMath.square(x[1]), BasicMath.square(x[1]), x[2], x[3], 0.0);
				return predictRho(predictors, data, tensor);
			}
		};

	  //create the optimizers
		LevenbergMarquardtOptimizer optimizer = new LevenbergMarquardtOptimizer();
		optimizer.setNumberEvaluations(2000);
		optimizer.setAbsPointDifference(ABSOLUTE_DIFFUSION_ERROR);
		optimizer.setErrors(errors);
		optimizer.setObservations(targetValues);
		
		MultivariateVectorFunction predictionFunction = axiPredictionFunction;
	  if (robust)
	  {
	  	predictionFunction = new OutlierDampingFunction(predictionFunction, targetValues, errors, outlierThreshold);
	  }
	  optimizer.setFiniteDifferenceFunction(predictionFunction);
	  
	  //create initial guess
 	  double dc = RotationalDiffusionTensor.tensorPrincipalValue(computeTauCApproximate(data));

 	  //generate initial guesses
		double[] alpha = BasicUtils.uniformLinePoints(0.0, BasicMath.PI/2.0, 2); //2
		double[] beta = BasicUtils.uniformLinePoints(0.0, BasicMath.PI/2.0, 2); //2
 	  double[][] x0List = null;
 	  if (robust)
 	  {
 	  	x0List = new double[2][2];
 	  	x0List[0] = new double[]{BasicMath.sqrt(0.75*dc), BasicMath.sqrt(1.25*dc)};
 	  	x0List[1] = new double[]{BasicMath.sqrt(0.50*dc), BasicMath.sqrt(1.50*dc)};
 	  }
 	  else
 	  {
 	  	x0List = new double[1][2];
 	  	x0List[0] = new double[]{BasicMath.sqrt(0.75*dc), BasicMath.sqrt(1.25*dc)};
 	  }
	  	  
	  //perform the optimization over all initial points
	  ArrayList<LeastSquaresOptimizationResult> results = new ArrayList<LeastSquaresOptimizationResult>();
	  for (double[] x0 : x0List)
		  for (double a : alpha)
			  for (double b : beta)
			  {
			  	//try the prolate case
			  	try
			  	{
			  		LeastSquaresOptimizationResult result = optimizer.optimize(new double[]{x0[1], x0[0], a, b});
			  		results.add(result);
			  	}
			  	catch (OptimizationRuntimeException e) { }
			  	
			  	//try the oblete case
			  	try
			  	{
			  		LeastSquaresOptimizationResult result = optimizer.optimize(new double[]{x0[0], x0[1], a, b});
				  	results.add(result);
			  	}
			  	catch (OptimizationRuntimeException e) { }
			  }			  

	  LeastSquaresOptimizationResult result = OptimizationResultImpl.getBestSolution(results);
	  double[] x = result.getPoint();
	  RotationalDiffusionTensor sol = RotationalDiffusionTensor.createZYZ(BasicMath.square(x[0]), BasicMath.square(x[1]), BasicMath.square(x[1]), x[2], x[3], 0.0);
	  
	  return new TensorStorage(sol, result.getChiSquared());
	}

	@Override
	protected AnisotropicModelFreePredictor createFromMolecule(AnisotropicModelFreePredictor predictor, Molecule mol)
			throws AtomNotFoundException
	{
		return predictor.createFromMolecule(mol);
	}

	@Override
	protected AnisotropicModelFreePredictor createPredictor(ModelFreePredictor predictor)
	{
		return (AnisotropicModelFreePredictor)predictor;
	}
	
	@Override
	public AxiallySymmetricRelaxationComputor createRigid()
	{
		if (this.relaxationData.isRigid())
			return this;

		ExperimentalRelaxationData newList = this.relaxationData.createRigid();
		
		return createSubList(newList);		
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.AbstractModelFreeComputor#createSubList(edu.umd.umiacs.armor.nmr.relax.ExperimentalRelaxation)
	 */
	@Override
	public AxiallySymmetricRelaxationComputor createSubList(ExperimentalRelaxationData newList)
	{
		if (newList==this.relaxationData)
			return this;
		
		ArrayList<Integer> indicies = getAssociatedIndices(newList);
		ArrayList<AnisotropicModelFreePredictor> predictors = getBondPredictors(indicies);
		
		return new AxiallySymmetricRelaxationComputor(newList, predictors, false, false, this.robust, this.outlierThreshold, this.absoluteDiffusionError);
	}

	@Override
	protected AnisotropicModelFreePredictor createWithTensor(AnisotropicModelFreePredictor predictor, RotationalDiffusionTensor tensor)
	{
		return predictor.createWithTensor(tensor);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.nmr.relax.AbstractModelFreeComputor#isTensorComputable()
	 */
	@Override
	public boolean isTensorComputable()
	{
		if (this.relaxationData.createRigid().size()<4)
			return false;
		
		return true;
	}
}
