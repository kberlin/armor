/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax.gui;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

import javax.swing.JFrame;

import edu.umd.umiacs.armor.nmr.relax.BondRelaxation;
import edu.umd.umiacs.armor.nmr.relax.ExperimentalRelaxationData;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.plot.Line;
import edu.umd.umiacs.armor.util.plot.Plot2d;

/**
 * The Class ExperimentalRatioPlot.
 */
public class ExperimentalRatioPlot extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1631448811091131918L;

	/**
	 * Instantiates a new experimental ratio plot.
	 * 
	 * @param <solution>
	 *          the generic type
	 * @param <Predictor>
	 *          the generic type
	 * @param solution
	 *          the solution
	 */
	public ExperimentalRatioPlot(ExperimentalRelaxationData data) 
	{
		setTitle("Experimental Data Plot");
		setBounds(100, 100, 500, 600);
		//setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		//java.awt.geom.Ellipse2D.Double rigid = new Ellipse2D.Double(-2.5,-2.5,5,5);
		//java.awt.geom.Rectangle2D.Double flexible = new Rectangle2D.Double(-2.5,-2.5,5,5);
		
		ArrayList<ExperimentalRelaxationData> chainData = data.seperateByChain();
		int numberDomains = chainData.size();
		
		boolean hasNoe = false;
		for (ExperimentalDatum datum : data.getNoeData())
			if (!Double.isNaN(datum.value()))
			{
				hasNoe = true;
				break;
			}
		
		int numberData = 3;
		if (hasNoe)
			numberData++;
		
	  setLayout(new GridLayout(numberData, numberDomains, 0, 0));
		ArrayList<Plot2d> plots = new ArrayList<Plot2d>(numberData*numberDomains);
		
		for (ExperimentalRelaxationData chainList : chainData)
		{
			String chainId = chainList.getBondRelaxation(0).getBond().getFromAtom().getPrimaryInfo().getChainID();
			Plot2d plotRho = new Plot2d();
			plotRho.setXAxisTicksInteger();
			Plot2d plotR1 = new Plot2d();		
			plotR1.setXAxisTicksInteger();
			Plot2d plotR2 = new Plot2d();		
			plotR2.setXAxisTicksInteger();
			Plot2d plotNoe = new Plot2d();	
			plotNoe.setXAxisTicksInteger();
			
			ArrayList<ExperimentalRelaxationData> dataList = chainList.seperateByBondTypeAndFrequency();
				
			for (ExperimentalRelaxationData currData : dataList)
			{
				ArrayList<String> toolTips = new ArrayList<String>();
				ArrayList<String> toolTipsR1 = new ArrayList<String>();
				ArrayList<String> toolTipsR2 = new ArrayList<String>();
				ArrayList<String> toolTipsNoe = new ArrayList<String>();
				double[] residues = new double[currData.size()];
				
				int iter=0;
				//should only have one value
				for (BondRelaxation bondData : currData.getBondRelaxationData())
				{
					residues[iter] = bondData.getBond().getFromAtom().getPrimaryInfo().getResidueNumber();
				
					toolTips.add(String.format("<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Value: %.2f<br>Error: %.2f<br>Rigid: %b", 
							bondData.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
							bondData.getBond().getFromAtom().getPrimaryInfo().getChainID(),
							bondData.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
							bondData.getBond().getToAtom().getPrimaryInfo().getAtomName(),
							bondData.getRelaxationDatum(0).getRho().value(),
							bondData.getRelaxationDatum(0).getRho().error(),
							bondData.isRigid()));
					
					toolTipsR1.add(String.format("<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Value: %.2f<br>Error: %.2f<br>Rigid: %b", 
							bondData.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
							bondData.getBond().getFromAtom().getPrimaryInfo().getChainID(),
							bondData.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
							bondData.getBond().getToAtom().getPrimaryInfo().getAtomName(),
							bondData.getRelaxationDatum(0).getR1().value(),
							bondData.getRelaxationDatum(0).getR1().error(),
							bondData.isRigid()));

					toolTipsR2.add(String.format("<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Value: %.2f<br>Error: %.2f<br>Rigid: %b", 
							bondData.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
							bondData.getBond().getFromAtom().getPrimaryInfo().getChainID(),
							bondData.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
							bondData.getBond().getToAtom().getPrimaryInfo().getAtomName(),
							bondData.getRelaxationDatum(0).getR2().value(),
							bondData.getRelaxationDatum(0).getR2().error(),
							bondData.isRigid()));
					
					toolTipsNoe.add(String.format("<html>Residue: %d<br>Chain: %s<br>Type: %s-%s<br>Value: %.2f<br>Error: %.2f<br>Rigid: %b", 
							bondData.getBond().getFromAtom().getPrimaryInfo().getResidueNumber(),
							bondData.getBond().getFromAtom().getPrimaryInfo().getChainID(),
							bondData.getBond().getFromAtom().getPrimaryInfo().getAtomName(),
							bondData.getBond().getToAtom().getPrimaryInfo().getAtomName(),
							bondData.getRelaxationDatum(0).getNoe().value(),
							bondData.getRelaxationDatum(0).getNoe().error(),
							bondData.isRigid()));

					if (bondData.getRelaxationDatum(0).hasNoe())
						hasNoe = true;
					
					iter++;
				}
				
				String label = String.format("%s, %s-%s, %.2f MHz", 
						currData.getBondRelaxation(0).getBond().getFromAtom().getPrimaryInfo().getChainID(),
						currData.getBondRelaxation(0).getBond().getBondType().getFromAtomName(),
						currData.getBondRelaxation(0).getBond().getBondType().getToAtomName(), 
						currData.getBondRelaxation(0).getRelaxationDatum(0).getFrequency());
								
				BaseExperimentalData rhoData = currData.getRhoData();
				Line rhoLine = new Line(residues, rhoData.values(), rhoData.errors(), label, toolTips);
				rhoLine.setDrawLine(true);
				rhoLine.setDrawShape(true);
				rhoLine.setShape(new Ellipse2D.Double(-2.5,-2.5,5,5));
				plotRho.addErrorLine(rhoLine);
				
				BaseExperimentalData r1Data = currData.getR1Data();
				Line r1Line = new Line(residues, r1Data.values(), r1Data.errors(), label, toolTipsR1);
				r1Line.setDrawLine(true);
				r1Line.setDrawShape(true);
				r1Line.setShape(new Ellipse2D.Double(-2.5,-2.5,5,5));
				plotR1.addErrorLine(r1Line);

				BaseExperimentalData r2Data = currData.getR2Data();
				Line r2Line = new Line(residues, r2Data.values(), r2Data.errors(), label, toolTipsR2);
				r2Line.setDrawLine(true);
				r2Line.setDrawShape(true);
				r2Line.setShape(new Ellipse2D.Double(-2.5,-2.5,5,5));
				plotR2.addErrorLine(r2Line);

				BaseExperimentalData noeData = currData.getNoeData();
				Line noeLine = new Line(residues, noeData.values(), noeData.errors(), label, toolTipsNoe);
				noeLine.setDrawLine(true);
				noeLine.setDrawShape(true);
				noeLine.setShape(new Ellipse2D.Double(-2.5,-2.5,5,5));
				plotNoe.addErrorLine(noeLine);		
			}
			
			plotRho.setXLabel("Residue/Nucleotide #");
			plotRho.setYLabel("\u03C1 Exp.");
			plotRho.setTitle("Experimental \u03C1 "+chainId, new Font("Ariel", Font.PLAIN, 14));
			plots.add(plotRho);
			
			plotR1.setXLabel("Residue/Nucleotide #");
			plotR1.setYLabel("R1 Exp. (1/s)");
			plotR1.setTitle("Experimental R1 "+chainId, new Font("Ariel", Font.PLAIN, 14));
			plots.add(plotR1);

			plotR2.setXLabel("Residue/Nucleotide #");
			plotR2.setYLabel("R2 Exp. (1/s)");
			plotR2.setTitle("Experimental R2 "+chainId, new Font("Ariel", Font.PLAIN, 14));
			plots.add(plotR2);

			plotNoe.setXLabel("Residue/Nucleotide #");
			plotNoe.setYLabel("NOE Exp.");
			plotNoe.setTitle("Experimental NOE "+chainId, new Font("Ariel", Font.PLAIN, 14));
			if (hasNoe)
				plots.add(plotNoe);
		}

		Plot2d firstPlot = plots.get(0);
		for (int dataIndex=0; dataIndex<numberData; dataIndex++)
			for (int domain=0; domain<numberDomains; domain++)
			{
				Plot2d plot = plots.get(numberData*domain+dataIndex);
				if (dataIndex<numberData-1)
					plot.removeLegend();
				
				getContentPane().add(plot);
				plot.alignX(firstPlot);
			}
		
		setVisible(true);
	}
}
