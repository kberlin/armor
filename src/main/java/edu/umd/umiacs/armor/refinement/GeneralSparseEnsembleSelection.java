/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.refinement;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import edu.umd.umiacs.armor.math.linear.LinearMathFileIO;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;
import edu.umd.umiacs.armor.util.ParseOptions;

public final class GeneralSparseEnsembleSelection extends AbstractSparseEnsembleSelection<GeneralLinearEnsembleInput>
{
	public static void main(String[] args) throws IOException, PdbException, AtomNotFoundException, ClassNotFoundException, RuntimeException
	{
		ParseOptions options = new ParseOptions();
		options.addRequiredOption("data", "data", "Experimental data size filename.", String.class);
		options.addRequiredOption("matrix", "matrix", "File name of the data matrix file.", String.class);
  	options.addOption("pdbDir", "pdb", "Directory of PDB files. Ignore, if not pdb files available.", "");
	
		//create the solver
		GeneralSparseEnsembleSelection selection = new GeneralSparseEnsembleSelection(options, args);
		
		selection.run("general");
	}

	public GeneralSparseEnsembleSelection(ParseOptions options, String[] args)
	{
		super(options, args);
		setDefaultSolver();
	}
	
	@Override
	public GeneralLinearEnsembleInput createInput()
	{
		try
		{
		 	System.out.print("Reading Data...");
		 	RealMatrix myData = LinearMathFileIO.read(new File(getOptions().get("data").getString()));
		 	System.out.print("["+myData.numRows()+"  "+myData.numColumns()+"] ");
		 	System.out.println("done.");
		 	
		 	if (myData.numColumns()<2)
		 		throw new RefinementRuntimeException("Data file does not have at least 2 columns. Experimental data file must contain data and error values.");
		 	
		 	//create the data
		 	ArrayList<BaseExperimentalDatum> dataList = new ArrayList<BaseExperimentalDatum>();
		 	for (int row=0; row<myData.numRows(); row++)
		 		dataList.add(new BaseExperimentalDatum(myData.get(row, 0), myData.get(row, 1)));	 	
		 	BaseExperimentalData data = new BaseExperimentalData(dataList);
	
		 		 	
			//create input
		 	GeneralLinearEnsembleInput ensembleInput = new GeneralLinearEnsembleInput(data);
		
		 	return ensembleInput;
		}
		catch (IOException e)
		{
			throw new RefinementRuntimeException(e);
		}
	}
}
