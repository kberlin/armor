package edu.umd.umiacs.armor.refinement;

import java.util.Locale;

import edu.umd.umiacs.armor.math.optim.LinearSolution;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;

public final class MultiInputLinearSolution<T extends ExperimentalDatum> implements LinearSolution
{
	private final ExperimentalData<T> data;
	
	private final LinearSolution sol;	
	private final String sourceName;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2863912917570533509L;
	
	public MultiInputLinearSolution(LinearSolution sol, ExperimentalData<T> data, String sourceName)
	{
		this.data = data;
		this.sourceName = sourceName;
		this.sol = sol;
	}
	
	@Override
	public double absoluteError()
	{
		return this.sol.absoluteError();
	}
	
	@Override
	public double chiSquared()
	{
		return this.sol.chiSquared();
	}
	
	@Override
	public int compareTo(LinearSolution o)
	{
		return this.sol.compareTo(o);
	}
	
	@Override
	public int dimension()
	{
		return this.sol.dimension();
	}
	
	@Override
	public int[] getActiveColumns()
	{
		return this.sol.getActiveColumns();
	}
	
	@Override
	public double[] getActiveWeights()
	{
		return this.sol.getActiveWeights();
	}
	
	public ExperimentalData<T> getExperimentalData()
	{
		return this.data;
	}
	
	@Override
	public int[] getOrderedActiveColumns()
	{
		return this.sol.getOrderedActiveColumns();
	}
	
	@Override
	public double[] getOrderedActiveWeights()
	{
		return this.sol.getOrderedActiveWeights();
	}
	
	@Override
	public double[] getOrderedNormalizedActiveWeights()
	{
		return this.sol.getOrderedNormalizedActiveWeights();
	}
	
	public String getSourceName()
	{
		return this.sourceName;
	}
	
	@Override
	public double[] getX()
	{
		return this.sol.getX();
	}
	
	@Override
	public int l0Norm()
	{
		return this.sol.l0Norm();
	}
	
	@Override
	public String toString(double yNorm)
	{
		return this.sol.toString(yNorm);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.math.optim.LinearSolution#toFileString()
	 */
	@Override
	public String toFileString()
	{
		StringBuilder s = new StringBuilder();
		
		//print norm
		s.append(String.format(Locale.US,"%d ", l0Norm()));
		
		//print source
		s.append(String.format(Locale.US,"%s ", getSourceName()));
		
		//print chi^2
		s.append(String.format(Locale.US,"%.8g ", chiSquared()));
		
		for (int col : getOrderedActiveColumns())
			s.append(String.format(Locale.US,"%d\t", col+1));
		for (double weight : getOrderedActiveWeights())
			s.append(String.format(Locale.US,"%.8g\t", weight));				

		return s.toString();
	}
}
