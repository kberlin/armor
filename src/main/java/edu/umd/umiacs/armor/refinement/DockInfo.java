package edu.umd.umiacs.armor.refinement;

import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.optim.OptimizationResult;

public final class DockInfo implements Comparable<DockInfo>
{
	
	/** The result. */
	private final OptimizationResult result;
	
	/** The transform. */
	private final RigidTransform transform;
	
	/**
	 * Instantiates a new dock info.
	 * 
	 * @param transform
	 *          the transform
	 * @param result
	 *          the result
	 */
	public DockInfo(RigidTransform transform, OptimizationResult result)
	{
		this.transform = transform;
		this.result = result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(DockInfo sol)
	{
		return Double.compare(this.result.getValue(), sol.result.getValue());
	}

	/**
	 * @return the result
	 */
	public OptimizationResult getOptimizationResult()
	{
		return this.result;
	}

	/**
	 * @return the transform
	 */
	public RigidTransform getRigidTransform()
	{
		return this.transform;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return this.transform.toString();
	}
}