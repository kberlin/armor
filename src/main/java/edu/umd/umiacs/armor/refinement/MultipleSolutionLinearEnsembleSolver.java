/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.refinement;

import java.util.ArrayList;

import edu.umd.umiacs.armor.math.optim.LinearSolutions;
import edu.umd.umiacs.armor.math.optim.sparse.MultipleSolutionSparseLinearLeastSquaresOptimizer;
import edu.umd.umiacs.armor.molecule.filters.MolecularEnsembleSolutions;
import edu.umd.umiacs.armor.util.ProgressObserver;

public class MultipleSolutionLinearEnsembleSolver implements LinearEnsembleSolver, EnsembleSolverProgressObservable
{
	private final MultipleSolutionSparseLinearLeastSquaresOptimizer solver;
	private final ArrayList<EnsembleSolverProgressObserver> observers;

	public MultipleSolutionLinearEnsembleSolver(MultipleSolutionSparseLinearLeastSquaresOptimizer solver)
	{
		this.solver = solver;
		this.observers = new ArrayList<EnsembleSolverProgressObserver>();
		
		//add the observer
		this.solver.addProgressObserver(new ProgressObserver()
		{
			
			@Override
			public void update(double progressPercentage)
			{
				for (EnsembleSolverProgressObserver o : MultipleSolutionLinearEnsembleSolver.this.observers)
					o.updateIteration((int)progressPercentage);
			}
		});
	}

	@Override
	public MolecularEnsembleSolutions solve(LinearEnsembleInput input)
	{
		//solve the max-likelyhood problem
		this.solver.setA(input.getA());
		LinearSolutions solutions = this.solver.solveAlt(input.getY());
		
		return new MolecularEnsembleSolutions(input, solutions);
	}
	
	public MolecularEnsembleSolutions solve(LinearEnsembleInput input, int maxEnsembleSize)
	{
		//solve the max-likelyhood problem
		this.solver.setA(input.getA());
		LinearSolutions solutions = this.solver.solveAlt(input.getY(), maxEnsembleSize);
		
		return new MolecularEnsembleSolutions(input, solutions);
	}

	@Override
	public void addProgressObserver(EnsembleSolverProgressObserver o)
	{
		this.observers.add(o);
	}

	@Override
	public void removeProgressObserver(EnsembleSolverProgressObserver o)
	{
		this.observers.remove(o);
	}
}
