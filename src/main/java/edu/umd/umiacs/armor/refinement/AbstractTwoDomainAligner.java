/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.refinement;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.MultivariateFunction;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.BOBYQAOptimizer;
import edu.umd.umiacs.armor.math.optim.IterativeOptimizer;
import edu.umd.umiacs.armor.math.optim.LeastSquaresConverter;
import edu.umd.umiacs.armor.math.optim.OptimizationResult;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.math.optim.OutlierDampingFunction;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MultiDomainComplex;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;

public abstract class AbstractTwoDomainAligner implements TwoDomainAligner
{
	private final class AnglePredictionFunction implements MultivariateVectorFunction
	{
		private final MultiDomainComplex twoDomainMolecule;
		
		public AnglePredictionFunction(MultiDomainComplex twoDomainMolecule)
		{
			this.twoDomainMolecule = twoDomainMolecule;
		}
		
		public double[] predictWithRotation(Rotation R)
		{
			try
			{
				MultiDomainComplex mol = this.twoDomainMolecule.createSecondDomainTransformed(new RigidTransform(R, null));
				double[] values = predict(mol.getDomainOne(), mol.getDomainTwo());
				return values;
			}
			catch (AtomNotFoundException e)
			{
				throw new IllegalArgumentException(e.getMessage(), e);
			}			
		}

		/* (non-Javadoc)
		 * @see org.apache.commons.math3.analysis.MultivariateVectorFunction#value(double[])
		 */
		@Override
		public double[] value(double[] angles) throws IllegalArgumentException
		{
			//create the new transform
			Rotation R = Rotation.createZYZ(angles[0], angles[1], angles[2]);
			
			return predictWithRotation(R);
		}
	}
	private final double outlierThreshold;
	
	private final boolean robust;
	
	public AbstractTwoDomainAligner(boolean robust, double outlierThreshold)
	{
		this.robust = robust;
		this.outlierThreshold = outlierThreshold;
	}
	
	@Override
	public ArrayList<Rotation> align(Molecule m1, Molecule m2) throws AtomNotFoundException
	{  
		ExperimentalData<? extends ExperimentalDatum> experimentalData = getData();
	  double[] targetValues = experimentalData.values();
	  double[] weights = experimentalData.weights();
	  
	  //create the optimizer
	  IterativeOptimizer<MultivariateFunction> optimizer = new BOBYQAOptimizer(7, 3.0/180.0*BasicMath.PI);
	  optimizer.setAbsPointDifference(0.01/180.0*BasicMath.PI);
	  optimizer.setNumberEvaluations(2000);
	  
	  MultiDomainComplex complex = new MultiDomainComplex(m1, m2);
	  	  
	  //perform the optimization over all initial points
	  ArrayList<OptimizationResult> results = new ArrayList<OptimizationResult>();
	  ArrayList<Rotation> resultRotations = new ArrayList<Rotation>();
	  List<Rotation> initRotations = initRotations(m1, m2);
	  
	  double[] x0 = new double[]{0.0,0.0,0.0};
	  for (Rotation rotation0 : initRotations)
	  {	  	
		  //compute the result
		  MultiDomainComplex rotatedComplex = complex.createSecondDomainTransformed(new RigidTransform(rotation0, null));
		  
		  MultivariateVectorFunction func = new AnglePredictionFunction(rotatedComplex);
		  if (this.robust)
		  	func = new OutlierDampingFunction(func, targetValues, weights, this.outlierThreshold);
		  
		  LeastSquaresConverter scalarFunc = new LeastSquaresConverter(func, targetValues, weights);
		  optimizer.setFunction(scalarFunc);
	  	OptimizationResult currentSol = optimizer.optimize(x0);
	  	
	  	//get the result total rotation relative to the original frame
  		double[] currAngles = currentSol.getPoint();
			Rotation Rtotal = Rotation.createZYZ(currAngles[0], currAngles[1], currAngles[2]).mult(rotation0);
			
			//System.out.println("Diff="+Rtotal.distance(rotation0)*180.0/BasicMath.PI+" Val Before="+scalarFunc.chi2(x0)+" Val After="+scalarFunc.chi2(currAngles));
			//System.out.println("Rotation Initial="+BasicUtils.toString(rotation0.getAnglesZYZ())+" Final="+BasicUtils.toString(Rtotal.getAnglesZYZ()));
						
	  	//store the current soltuion
			results.add(currentSol);
	   	resultRotations.add(Rtotal);
		}
	  
	  ArrayList<Rotation> filteredResults = OptimizationResultImpl.filterRotations(results, resultRotations, 2.0/180.0*BasicMath.PI);
	  
		return filteredResults;
	}

	public abstract List<Rotation> initRotations(Molecule m1, Molecule m2) throws AtomNotFoundException;
}
