/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.refinement;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.LinearMathFileIO;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.optim.LinearSolution;
import edu.umd.umiacs.armor.math.optim.NonNegativeLinearLeastSquares;
import edu.umd.umiacs.armor.math.optim.sparse.MultiOrthogonalMatchingPursuit;
import edu.umd.umiacs.armor.math.optim.sparse.MultipleSolutionSparseLinearLeastSquaresOptimizer;
import edu.umd.umiacs.armor.math.optim.sparse.SparseLinearSolution;
import edu.umd.umiacs.armor.math.optim.sparse.SparseLogLikelihoodSolver;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.filters.MolecularEnsemble;
import edu.umd.umiacs.armor.molecule.filters.MolecularEnsembleSolutions;
import edu.umd.umiacs.armor.molecule.filters.MoleculeAlignmentException;
import edu.umd.umiacs.armor.molecule.filters.MoleculeSVDAligner;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.FileIO;
import edu.umd.umiacs.armor.util.ParseOptions;

public abstract class AbstractSparseEnsembleSelection<Input extends AbstractLinearEnsembleInput> implements EnsembleSolverProgressObserver, EnsembleInputProgressObserver
{
	private final class BestResult
	{
		public final double absError;
		public final ExperimentalData<? extends ExperimentalDatum> data;
		public final double normY;
		public final double[] prediction;
		public final double[] relativePredictions;
		public final double[] relativeResiduals;
		
		public BestResult(Input input, LinearSolution sol)
		{
			this.data = input.getExperimentalData();
			this.relativePredictions = input.getA().mult(sol.getX());
			this.prediction = BasicMath.mult(this.relativePredictions, input.getExperimentalData().errors());
			this.absError = sol.absoluteError();
			
			double[] y = input.getY();
			this.relativeResiduals = BasicMath.subtract(this.relativePredictions, y);		
			this.normY = BasicMath.norm(y);
		}
	}
	private int columns;
	private int l0;
	private int maxColumns;
	private final ParseOptions options;
	private MultipleSolutionLinearEnsembleSolver solver;
	public static final double DEFAULT_RELATIVE_ERROR_TOL = 1.0e-3;
	private static final int MAX_CLUSTERABLES_SOLUTIONS = 1000;
	private static final double MAX_L0_CUTOFF = 0.01;
	
	private static final int MAX_SAVED_SOLUTIONS = 5000;
	
	public static List<File> getMoleculeFiles(String pdbDirectory, FilenameFilter filter) throws PdbException
	{
  	if (pdbDirectory.isEmpty())
  	{
  		throw new PdbException("Directory name cannot be empty.");
  	}
  	
		File dir = new File(pdbDirectory);
		File[] fileList = dir.listFiles(filter);
		
		if (fileList==null || fileList.length==0)
		{
			return new ArrayList<File>();
		}
		
		//sort the files
		Comparator<File> fileNameComparator = new Comparator<File>()
		{
			@Override
			public int compare(File o1, File o2)
			{
				return o1.getName().compareTo(o2.getName());
			}
		};
		Arrays.sort(fileList, fileNameComparator);

		//store in list
		List<File> pdbFiles = Arrays.asList(fileList);
		
		System.out.println("Found "+pdbFiles.size()+" molecule file(s):");
		for (File name : pdbFiles)
		{
			System.out.println(name.getPath());
		}
		
		return pdbFiles;
	}
	
	public AbstractSparseEnsembleSelection(ParseOptions options, String[] args)
	{
		this.options = options;
		
		//add default values
  	options.addOption("align", "align", "Name of the atom type (ex. H, CA, etc) to use for alignment and RMSD computation during postprocess clustering. Set to * for any", "CA");
  	options.addOption("best", "best", "Output the best possible x>0 (NNLS) solution.", false);
  	options.addOption("precond", "precond", "Preconditioning of the linear system before M-OMP: 0) None, 1) Rotation and Compression (cutoff=reltol*0.1).", 1);
  	options.addOption("top", "top", "Max relative error of top solutions relative to input. All output solutions have abs. error <= best sol abs. error + top*||(input vector)./(error vector)||_2.", MultiOrthogonalMatchingPursuit.DEFAULT_SOLUTION_STORAGE_RELATIVE_CUTOFF);
  	options.addOption("maxsum", "maxsum", "Maximum possible sum of the column weights.", Double.POSITIVE_INFINITY);
  	options.addOption("l0max", "l0max", "Maximum ensemble size to compute.", Integer.MAX_VALUE);
  	options.addOption("rmsd", "rmsd", "Cluster RMSD value for solution pdb files (see align, outalign options).", 4.0);
  	options.addOption("reltol", "reltol", "M-OMP solver's relative  error tolerance for termination.", MultiOrthogonalMatchingPursuit.DEFAULT_RELATIVE_ERROR);
		options.addOption("matrix", "matrix", "File name of the prediction matrix file. This data will be used instead of the prediction."
				+ "If file name is a directory, all the files inside are assumed to be matrix files, are solved individually, and the best results combined as output. "
				+ "File format can be as a text or binary (big-endian). If file is text, each column must be seperated by a new line. "
				+ "If file is binary, first two values are int32 matrix dimensions [m n], followed by m*n double values (row order).", "");
  	options.addOption("output", "out", "Output directory file name.", "solution");
  	options.addOption("outputAlign", "outalign", "Type of alignment for models inside the output pdb file. 0) no alignment 1) align based on whole struct. 2) Align by first chain.", 0);
  	options.addOption("storejava", "storejava", "Output the java object with all results. Can be used for parsing using ARMOR API.", false);
  	options.addRequiredOption("pdbDir", "pdb", "Directory of PDB files.", String.class);
  	options.addOption("K", "K", "Number of top solutions to store per iteration of M-OMP.", MultiOrthogonalMatchingPursuit.DEFAULT_TOP_SOLUTION_SIZE);
  	
  	try 
  	{
  		options.parse(args);
  		if (options.needsHelp())
  		{
  			System.out.println(options.helpMenuString());
  			System.exit(0);
  		}
  		
  		options.checkParameters();
  		
  		if (options.get("K").getInteger()<1)
  			throw new RefinementRuntimeException("Invalid value for K");
  		if (options.get("l0max").getInteger()<1)
  			throw new RefinementRuntimeException("Invalid value for l0max");
  	}
  	catch (Exception e)
  	{
  		System.err.println(e.getMessage());
  		System.err.println(options.helpMenuString());
  		System.exit(1);
  	}
	}
	
	protected MolecularEnsembleSolutions adjustSolutions(MolecularEnsembleSolutions solution)
	{
		return solution;
	}
	
	public double[] computeRelaxedSolution(Input input)
  {
  	System.out.print("Computing the best x>0 possible solution...");
  	
  	//compute the non-negative solution
  	NonNegativeLinearLeastSquares solver = new NonNegativeLinearLeastSquares(1.0e-8, 1000);
  	solver.setA(input.getA());
		double[] x = solver.solve(input.getY());
		
		SparseLinearSolution sol = new SparseLinearSolution(BasicMath.norm(BasicMath.subtract(input.getA().mult(x), input.getY())), x, 1.0e-4);
		
		BestResult bestResult = new BestResult(input, sol);
			
		outputSolution(bestResult, "Best x>0 Possible Solution");		
		
		return x;
  }
	
	protected abstract Input createInput();
	
	protected double dataNorm(LinearEnsembleInput linearEnsembleInput)
	{
		return linearEnsembleInput.dataNorm();
	}
	
	public void displayInput(Input input)
	{
	 	//output the input information
		System.out.println("Input Data Size: "+input.getExperimentalData().size());
		System.out.println("Input Column Size: "+input.ensembleSize());		
	  System.out.println("Input Data Norm: "+BasicMath.norm(input.getExperimentalData().relativeValues()));				
	}
	
	public List<MolecularEnsemble> generateClusters(MolecularEnsembleSolutions solutions, int l0, double topRelativeError, double rmsd, MoleculeSVDAligner aligner, MoleculeSVDAligner postAligner) throws AtomNotFoundException, PdbException, MoleculeAlignmentException
	{
		List<LinearSolution> l0Solutions = solutions.getTopRelativeSolutions(l0, topRelativeError);
		System.out.print("l0-norm="+l0+": found "+l0Solutions.size()+" solutions, ");

		if (l0Solutions.size()>MAX_CLUSTERABLES_SOLUTIONS)
		{
			System.out.println("Warning, too many alternative solutions, selecting top "+MAX_CLUSTERABLES_SOLUTIONS+" only!");
			l0Solutions = l0Solutions.subList(0, MAX_CLUSTERABLES_SOLUTIONS);
		}
		
		//cluster the solutions
		List<MolecularEnsemble> moleculeSolutions = solutions.generateEnsembleClusters(l0Solutions, aligner, rmsd, postAligner);
		
		System.out.println("clustered into "+moleculeSolutions.size()+" solution(s).");
		if (moleculeSolutions.size()>40)
		{
			System.out.println("Too many alternative solutions. Shrinking list.");
			moleculeSolutions = moleculeSolutions.subList(0, 40);
		}
		
		return moleculeSolutions;
	}
	
	public ParseOptions getOptions()
  {
  	return this.options;
  }
	
	protected List<File> getPdbFiles() throws PdbException
	{
		if (this.options.get("pdbDir").getString().isEmpty())
			return null;
		
		//get pdb files
		FilenameFilter filter = new FilenameFilter() 
		{
	    @Override
			public boolean accept(File dir, String name) 
	    {
	      return name.endsWith(".pdb");
	      //return Integer.valueOf(name.substring(name.length()-7, name.length()-4))<=1;
	    }
		};
		
	 	List<File> pdbFiles = getMoleculeFiles(this.options.get("pdbDir").getString(), filter);
	 	
	 	if (pdbFiles.isEmpty())
	 		throw new PdbException("No molecule files found in: "+this.options.get("pdbDir").getString());
	 	
	 	return pdbFiles;
	}
	
	public ArrayList<File> inputFiles()
	{
		ArrayList<File> files = new ArrayList<File>();
		if (!this.options.get("matrix").getString().isEmpty())
		{
			File file = new File(this.options.get("matrix").getString());
			
			if (!file.exists())
				throw new RefinementRuntimeException("No matrix files found.");
			
			//if not dictory just add the file
			if (!file.isDirectory())
			{
				files.add(file);
				return files;
			}
			
			//read the directory content
			File[] fileList = file.listFiles(new FilenameFilter()
			{				
				@Override
				public boolean accept(File dir, String name)
				{
					if (!name.startsWith(".") && !(new File(dir.toString()+File.separator+name).isDirectory()))
						return true;
					
					return false;
				}
			});

			
			for (File cf : fileList)
				files.add(cf);
			
			//sort the files in alphabetical order
			Collections.sort(files);
			
			System.out.println("Found "+files.size()+" matrix files:");
			for (File currFile : files)
			{
				System.out.println(currFile.getName());
			}
			
			return files;
		}
		else
		{
			files.add(null);
		 	return files;
		}
	}

	public Input loadInput(File file, Input prevInput) throws PdbException, FileNotFoundException
	{
		//get the pdb files
		List<File> pdbFiles = null;
		Input input = null;
		if (file==null || prevInput==null)
		{
			//create the input
			input = createInput();

			//add observer
			input.addProgressObserver(this);

			pdbFiles = getPdbFiles();
		}
		else
		{
			input = prevInput;
		}

		if (file!=null)
		{
		 	System.out.print("Reading Matrix...");
		 	RealMatrix A = loadMatrixInput(file);
		 	System.out.print("["+A.numRows()+"  "+A.numColumns()+"] ");
		 	System.out.println("done.");

		 	System.out.print("\nModels processed...");
		 	
		 	if (prevInput==null)
		 		input.addPdbModels(pdbFiles, A);
		 	else
		 	{
		 		input.setPredictionMatrix(A);
		 	}
		}
		else
		{
		 	System.out.print("\nModels processed...");
			input.addPdbModels(pdbFiles);
		}
		
		System.out.println(""+input.ensembleSize()+"...done.");
		
		//postprocess the input
		input = postprocessInput(input);

		this.columns = 0;

		return input;
	}
	
	public RealMatrix loadMatrixInput(File file) throws PdbException, FileNotFoundException
	{
	 	RealMatrix A = LinearMathFileIO.read(file);	
	 	
	 	return A;
	}
	
	public void outputBasicSolutionInfo(MolecularEnsembleSolutions solution)
	{
		System.out.println(solution.toString(dataNorm(solution.getInput())));
	}
	
	public void outputBestSolution(HashMap<Integer, BestResult> bestResults, double maxL0)
	{
		//output best solution detailed information
		System.out.println();
		for (int l0=1; l0<=maxL0; l0++)
		{
			outputSolution(bestResults.get(l0), "Best L0="+l0+" Solution");
			System.out.println();			
		}
	}

	public void outputSolution(BestResult result, String title)
	{
		System.out.println("\n==="+title+"===");
	
		double maxError = 0.0;
		for (double error : result.relativeResiduals)
			maxError = Math.max(maxError,Math.abs(error));

		//multiply by std to get back the value		
		double sigma = BasicStat.std(result.relativeResiduals);
		double chi2 = BasicStat.chi2(result.prediction, result.data.values(), result.data.errors());
		
		System.out.println("Relative Error: "+BasicMath.norm(result.relativeResiduals)/result.normY);
		System.out.println("Chi2: "+chi2);
		System.out.println("L: "+result.data.size()+", Chi2/L: "+(chi2/(double)result.data.size()));
		System.out.println("<index><exp data info><exp. value><pred. value><relative err.><*-outlier>");
		for (int index=0; index<result.data.size(); index++)
		{
			ExperimentalDatum datum = result.data.get(index);
			
			//output index
			System.out.format("%5d",index+1);

			//output data info
			System.out.print("\t"+datum.valueInfoString());
			
			if (Math.abs(datum.value())>.01 && Math.abs(result.prediction[index])>.01)
				System.out.format("\t%10.3f\t%10.3f\t%7.3f", datum.value(), result.prediction[index], result.relativeResiduals[index]);
			else
				System.out.format("\t%10.3e\t%10.3e\t%7.3f", datum.value(), result.prediction[index], result.relativeResiduals[index]);
			
			//put star if outlier
			//put star if outlier
			if (Math.abs(result.relativeResiduals[index])>=maxError)
				System.out.print(" ***");
			else
			if (Math.abs(result.relativeResiduals[index])>BasicStat.NORMAL_CONFIDENCE_INTERVAL_99*sigma)
				System.out.print(" **");
			else
			if (Math.abs(result.relativeResiduals[index])>BasicStat.NORMAL_CONFIDENCE_INTERVAL_95*sigma)
				System.out.print(" *");
			System.out.println();
		}
	  System.out.format("[%.3f*sigma (95%% rigid conf. interv.), %.3f*sigma (99%% rigid conf. interv.)] [abs max] relative errors: [%.3f, %.3f] [%.3f]\n", 
	  		BasicStat.NORMAL_CONFIDENCE_INTERVAL_95, BasicStat.NORMAL_CONFIDENCE_INTERVAL_99,
	  		BasicStat.NORMAL_CONFIDENCE_INTERVAL_95*sigma, BasicStat.NORMAL_CONFIDENCE_INTERVAL_99*sigma, 
	  		maxError);
	}
	
  protected Input postprocessInput(Input input)
	{
		//default is nothing
		return input;
	}

	public void run(String experimentType)
	{
		this.maxColumns = -1;
		
		System.out.println("Number of Threads: "+Runtime.getRuntime().availableProcessors());
		System.out.println("Options:\n"+this.options+"\n");
		
		try
		{
			File outputDirectory = new File(this.options.get("output").getString());
			if (!outputDirectory.isDirectory())
				throw new RefinementRuntimeException("No output directory "+this.options.get("output").getString()+" found.");
			
			System.out.println("Loading input...\n");
			ArrayList<File> inputFiles = inputFiles();
			
			HashMap<Integer, BestResult> bestSolutions = new HashMap<Integer, BestResult>();
			MolecularEnsembleSolutions solution = null;
			int numColumns = 0;
			
			Input input = null;
			for (int l0Iter=0; l0Iter<inputFiles.size(); l0Iter++)
			{
				input = loadInput(inputFiles.get(l0Iter), input);
			
				//save the prediction matrix only if predicted one
				if (inputFiles.get(l0Iter)==null)
				{
					String fileName = this.options.get("output").getString()+File.separatorChar+experimentType+"_prediction_matrix.dat";
					System.out.print("Saving prediction matrix to: "+fileName+"...");
					LinearMathFileIO.write(new File(fileName), input.getPredictionMatrix());
					System.out.println("done.");

					fileName = this.options.get("output").getString()+File.separatorChar+experimentType+"_expdata_matrix.dat";
					System.out.print("Saving experimental data matrix to: "+fileName+"...");
					LinearMathFileIO.write(new File(fileName), new Array2dRealMatrix(input.getExperimentalData().valuesAndErrors(), false));
					System.out.println("done.");
				}
	
				if (inputFiles.size()>1)
				{
					//check that file sizes are consistant
					if (l0Iter>0 && numColumns!=input.getPredictionMatrix().numColumns())
						throw new RefinementRuntimeException("Inconsistent number of columns in input files.");						
						
					numColumns = input.getPredictionMatrix().numColumns();	
					System.out.format("Starting computation of %s (%d of %d)...\n",inputFiles.get(l0Iter).getName(), l0Iter+1, inputFiles.size());
				}
				else
					System.out.println("Starting computation...\n");
	
		    long computeStartTime = System.currentTimeMillis();	  	
	
		    //compute the best solution
				if (this.options.get("best").getBoolean())
					computeRelaxedSolution(input);
							
				System.out.print("\nSolver Iteration: ");
				this.l0 = -1;
				MolecularEnsembleSolutions currSolution = this.solver.solve(input, this.options.get("l0max").getInteger());

				System.out.println("done.\n");

				//output timing resutls
		    System.out.println("Computation Finished: "+((System.currentTimeMillis()-computeStartTime)/1000.0)+" seconds.\n");

		    System.out.print("Adjusting chi^2 fit in case of a non-linear problem...");
		    currSolution = adjustSolutions(currSolution);
				System.out.println("done.\n");
	
				System.out.print("Recording solutions...");

				//generate name
				if (inputFiles.get(l0Iter)!=null)
					currSolution = currSolution.createWithSourceName(inputFiles.get(l0Iter).getName());
				else
					currSolution = currSolution.createWithSourceName("<generated>");
				
				//figure out if best results need updating
				for (int bestSolIter=1; bestSolIter<=currSolution.maxL0(); bestSolIter++)
				{
					BestResult bestResult = bestSolutions.get(bestSolIter);
					LinearSolution bestSol = currSolution.getSolutions().getBest(bestSolIter);
					
					if (bestResult==null || bestSol.absoluteError()<bestResult.absError)
					{
						//record the solution
						bestSolutions.put(bestSolIter, new BestResult(input, bestSol));
					}
				}
				
				System.out.println("done.\n");
				    			
				if (solution==null)
					solution = currSolution;
				else
					solution = new MolecularEnsembleSolutions(solution.getInput(), solution.getSolutions().merge(currSolution.getSolutions()));
				
				currSolution.getSolutions();
			}
			
			//output the solution
			outputBasicSolutionInfo(solution);
			
			//store the solution
			storeSolution(solution, this.options.get("output").getString(), experimentType, this.options.get("storejava").getBoolean());
			
			//compute maximum displayed solution
			int maxL0 =  Math.min(solution.maxL0(), solution.maxL0(MAX_L0_CUTOFF));

			//output best solution detailed information
			outputBestSolution(bestSolutions, maxL0);
			
			//create aligner
			MoleculeSVDAligner aligner = null;
			if (this.options.get("align").getString().equals("*"))
			  aligner = new MoleculeSVDAligner();
			else
				aligner = new MoleculeSVDAligner(this.options.get("align").getString());

			System.out.println("Generating the output complex up to size "+maxL0+"...");
			
			//cluster the solutions
			int alignmentOption = this.options.get("outputAlign").getInteger();
			MoleculeSVDAligner postAligner = null;
			if (alignmentOption==1)
				postAligner = new MoleculeSVDAligner(null, false);
			else
			if (alignmentOption==2)
				postAligner = new MoleculeSVDAligner(null, true);


			//cluster and save results
			for (int l0=1; l0<=maxL0; l0++)
			{
				try
				{
					List<MolecularEnsemble> cluster = generateClusters(solution, l0, this.options.get("top").getDouble(), this.options.get("rmsd").getDouble(), aligner, postAligner);
					saveEnsembles(solution, cluster, this.options.get("output").getString(), experimentType);
				}
				catch (PdbException e)
				{
					System.out.println("PDB files for clustered ensemble of size "+l0+" not found. Cluster not saved.");
				}
			}
			
			System.out.println("\nProgram Finished. Success!");
		}
		catch (Exception e)
		{
			e.printStackTrace(System.err);
			System.out.println(this.options);
		}
	}
  
  public void saveEnsembles(MolecularEnsembleSolutions solution, List<MolecularEnsemble> ensembles, String ouputDirectory, String expType) throws IOException, AtomNotFoundException, PdbException
	{
		HashMap<Integer, Integer> count = new HashMap<Integer, Integer>();
		for (MolecularEnsemble ensemble : ensembles)
		{
			int l0 = ensemble.size();
			
			//keep record of the count
			int currentL0Count = 1;
			if (count.get(l0)!=null)
				currentL0Count = count.get(l0)+1;

		  File file = new File(ouputDirectory+File.separator+expType+"_ensemble_"+l0+"_"+currentL0Count+".pdb");
			System.out.println("Storing l0="+l0+" ensemble number "+(currentL0Count)+" to "+file.getPath()+".");
			MoleculeFileIO.writePDB(file, solution.generateEnsemblePdb(ensemble).toPDB());
			System.out.println("\tCluster Information:");
			for (int iter=0; iter<ensemble.clusterSize(); iter++)
			{
				//offset by one
				int[] currColumns = ensemble.getClusterColumns(iter);
				for (int col=0; col<currColumns.length; col++)
					currColumns[col] = currColumns[col]+1;
				
				System.out.println("\t\t"+(iter+1)+": Columns: "+BasicUtils.toString(currColumns)
						+", Weights="+BasicUtils.toString(ensemble.getClusterWeights(iter))
						+", Chi^2="+ensemble.getClusterChi2(iter));
			}
			
			count.put(l0, currentL0Count);
		}
	}
  
  protected void setBaseSolver(MultipleSolutionSparseLinearLeastSquaresOptimizer solver)
	{
		SparseLogLikelihoodSolver.Preconditioning conditioning = SparseLogLikelihoodSolver.Preconditioning.NONE;
		if (this.options.get("precond").getInteger()==0)
			conditioning = SparseLogLikelihoodSolver.Preconditioning.NONE;
		else
		if (this.options.get("precond").getInteger()==1)			
			conditioning = SparseLogLikelihoodSolver.Preconditioning.COMPRESSION;
		else
		if (this.options.get("precond").getInteger()==2)
			conditioning = SparseLogLikelihoodSolver.Preconditioning.PUFFER;		
		
		//create the solver
		setSolver(new MultipleSolutionLinearEnsembleSolver(new SparseLogLikelihoodSolver(solver, conditioning)));
	}
	
  protected void setDefaultSolver()
  {
		MultipleSolutionSparseLinearLeastSquaresOptimizer ompSolver = new MultiOrthogonalMatchingPursuit(
				true, //CG is the default now
				this.options.get("K").getInteger(), 
				true,
				this.options.get("maxsum").getDouble(),
				this.options.get("top").getDouble()*2.0);
		ompSolver.setRelativeErrorTol(this.options.get("reltol").getDouble());
		setBaseSolver(ompSolver);
  }
	
  public void setSolver(MultipleSolutionLinearEnsembleSolver solver)
  {
  	this.solver = solver;
  	solver.addProgressObserver(this);
  }
  
  public MolecularEnsembleSolutions solve(Input input)
	{
		System.out.print("\nSolver Iteration: ");
		MolecularEnsembleSolutions solution = this.solver.solve(input);
		System.out.println("done.\n");
		
		return solution;
	}
  

	public void storeSolution(MolecularEnsembleSolutions solution, String outputDirectory, String outputFile, boolean storeJava) throws FileNotFoundException, IOException
	{
		//store the solution
		System.out.print("Storing Solutions to ");
		File outFile = new File(outputDirectory+File.separator+outputFile+"_solutions");
		
		outFile = FileIO.ensureExtension(outFile, null, "dat");
		System.out.print(outFile.getAbsolutePath()+"...");
		solution.getSolutions().save(outFile, MAX_SAVED_SOLUTIONS, solution.getInput().yNorm());
		System.out.println("done.");		
		
		if (storeJava)
		{
			System.out.print("Saving Java serialized object to ");
			outFile = FileIO.ensureExtension(outFile, null, "sav");
			System.out.print(outFile.getAbsolutePath()+"...");
			solution.save(outFile);
			System.out.println("done.");
		}
	}

	@Override
	public synchronized void updateIteration(int numIterations)
	{
		if (numIterations>this.l0)
		{
			this.l0 = numIterations;
			System.out.print(""+numIterations+"...");
			System.out.flush();
		}
	}
	
	@Override
	public synchronized void updateProcessed(int number)
	{
		this.maxColumns = Math.max(number, this.maxColumns);
		if (this.maxColumns==number && number>=this.columns+100)
		{
			System.out.format("%d...", number);			
			System.out.flush();
			this.columns = number;
		}
	}
}
