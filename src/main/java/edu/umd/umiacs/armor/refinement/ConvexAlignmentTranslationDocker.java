/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.refinement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.func.MultivariateFunction;
import edu.umd.umiacs.armor.math.func.MultivariateVectorFunction;
import edu.umd.umiacs.armor.math.optim.CMAESOptimizer;
import edu.umd.umiacs.armor.math.optim.IterativeOptimizer;
import edu.umd.umiacs.armor.math.optim.LeastSquaresConverter;
import edu.umd.umiacs.armor.math.optim.OptimizationResult;
import edu.umd.umiacs.armor.math.optim.OptimizationResultImpl;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.ProgressObserver;

/**
 * The Class ConvexDocker.
 */
public final class ConvexAlignmentTranslationDocker implements AlignmentTranslationRigidDocker
{
	
	/** The aligner. */
	private final TwoDomainAligner aligner;
	
	/** The observer list. */
	private final ArrayList<ProgressObserver> observerList;

	private final Predictor predictor;

	/** The relative error tolerance. */
	private final double relativeErrorTolerance;
	
	/** The Constant ABSOLUTE_DOCK_ERROR. */
	public final static double ABSOLUTE_DOCK_ERROR=1.0e-1;

	/**
	 * Instantiates a new convex docker.
	 * 
	 * @param aligner
	 *          the aligner
	 * @param relativeErrorTolerance
	 *          the relative error tolerance
	 */
	public ConvexAlignmentTranslationDocker(TwoDomainAligner aligner, Predictor predictor, double relativeErrorTolerance)
	{
		this.aligner = aligner;
		this.relativeErrorTolerance = relativeErrorTolerance;
		this.predictor = predictor;
							  
	  this.observerList = new ArrayList<ProgressObserver>();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.AlignmentTranslationRigidDocker#addProgressObserver(edu.umd.umiacs.armor.util.ProgressObserver)
	 */
	@Override
	synchronized public void addProgressObserver(ProgressObserver o)
	{
		this.observerList.add(o);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.AlignmentTranslationRigidDocker#dock(edu.umd.umiacs.armor.refinement.TwoDomainRigidPredictor)
	 */
	@Override
	public List<DockInfo> dock(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		final ArrayList<Rotation> alignmentRotations = getAlignment(mol1, mol2);
		
		//notify of progress
		notifyObservers(0.1);
		
		final TwoDomainRigidPredictor rigidPredictor = this.predictor.createTwoDomainRigidPredictor(mol1, mol2);
		final List<DockInfo> solutions = Collections.synchronizedList(new ArrayList<DockInfo>());
		final AtomicInteger counter = new AtomicInteger();
	
	  //setup up parallel task
		final List<RuntimeException> exceptionErrors = Collections.synchronizedList(new ArrayList<RuntimeException>());

		ExecutorService execSvc = Executors.newCachedThreadPool();
		//ExecutorService execSvc = Executors.newSingleThreadExecutor();
		
		for (final Rotation R: alignmentRotations)
		{		
		  Runnable sampleTask = new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						ArrayList<DockInfo> currOrientationSolutions = dockInstance(rigidPredictor, R);
						solutions.addAll(currOrientationSolutions);
						
						//notify of progress
						counter.incrementAndGet();
						
						notifyObservers(0.1+0.9*counter.doubleValue()/(double)alignmentRotations.size());

					}
					catch (Exception e)
					{
						exceptionErrors.add(new RefinementRuntimeException(e.getMessage(), e));
					}
				}
			};
				
			execSvc.execute(sampleTask);			
		}
				  
    //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(120L, TimeUnit.MINUTES);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new RefinementRuntimeException("Unable to finish all tasks.", e);
    }
    
    if (!exceptionErrors.isEmpty())
    	throw exceptionErrors.get(0);
					
		Collections.sort(solutions);
			
		return solutions;
	}
	
	/**
	 * Dock instance.
	 * 
	 * @param predictor
	 *          the predictor
	 * @param R
	 *          the r
	 * @return the array list
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	private ArrayList<DockInfo> dockInstance(TwoDomainRigidPredictor predictor, Rotation R) throws AtomNotFoundException
	{
		final TwoDomainRigidPredictor currPredictor = predictor.createRotated(R);
		List<Point3d> x0List = predictor.initPositions(new RigidTransform(R, null));
		
		final MultivariateVectorFunction func = new MultivariateVectorFunction()
		{
			@Override
			public double[] value(double[] position) throws IllegalArgumentException
			{
				Point3d pos = new Point3d(position[0],position[1],position[2]);				
				double[] values = currPredictor.predict(pos);
				
				return values;
			}
		};
		
		//get experimental data
		ExperimentalData<? extends ExperimentalDatum> experimentalData = predictor.getData();
		final double[] experimentalValues = experimentalData.values();
		final double[] experimentalWeights = experimentalData.weights();
		
		double radius = predictor.getDomainOne().maxAtomDistance()/2.0;
		final double distance = radius/8.0;

	  //perform the optimization over all initial points
	  final List<OptimizationResult> results = Collections.synchronizedList(new ArrayList<OptimizationResult>());
	  
	  //setup up parallel task
		final List<RuntimeException> exceptionErrors = Collections.synchronizedList(new ArrayList<RuntimeException>());
		
		ExecutorService execSvc = Executors.newCachedThreadPool();
		//ExecutorService execSvc = Executors.newSingleThreadExecutor();
		
	  for (final Point3d x0 : x0List)
	  {
			Runnable sampleTask = new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
					  //IterativeOptimizer<MultivariateFunction> optimizer = new BOBYQAOptimizer(7, distance);
						double[] std = new double[3];
						Arrays.fill(std, distance);
						IterativeOptimizer<MultivariateFunction> optimizer = new CMAESOptimizer(3*20*0+9, std);
						
					  optimizer.setAbsPointDifference(ABSOLUTE_DOCK_ERROR);
					  optimizer.setNumberEvaluations(5000);
						
					  LeastSquaresConverter dockFunc = new LeastSquaresConverter(func, experimentalValues, experimentalWeights);
					  optimizer.setFunction(dockFunc);
				  	OptimizationResult result = optimizer.optimize(new double[]{x0.x,x0.y,x0.z});
				  	
				  	results.add(result);
					}
					catch (Exception e)
					{
						exceptionErrors.add(new RefinementRuntimeException(e.getMessage(), e));
					}
				}
			};
			
			execSvc.execute(sampleTask);			
	  }
			  
    //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(120L, TimeUnit.MINUTES);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new RefinementRuntimeException("Unable to finish all tasks.", e);
    }
    
    if (!exceptionErrors.isEmpty())
    	throw exceptionErrors.get(0);
    
    //sort the solutions
    Collections.sort(results);
		  
	  //filter solutions to get only the close solutions
    List<OptimizationResult> filteredResults = results;
	  filteredResults = OptimizationResultImpl.filterSolutions(results, this.relativeErrorTolerance, 5.0/BasicMath.sqrt(3.0));
	  
	  ArrayList<DockInfo> resultArray = new ArrayList<DockInfo>(filteredResults.size()); 
	  for (OptimizationResult result : filteredResults)
	  {
	  	double[] p = result.getPoint();
	  	resultArray.add(new DockInfo(new RigidTransform(R, new Point3d(p[0],p[1],p[2])), result));
	  }
	  
	  //sort the results
	  Collections.sort(resultArray);
	  
	  return resultArray;
	}

	@Override
	public ArrayList<DockInfo> dockWithoutAlignment(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		TwoDomainRigidPredictor rigidPredictor = this.predictor.createTwoDomainRigidPredictor(mol1, mol2);
		ArrayList<DockInfo> solutions = dockInstance(rigidPredictor, Rotation.identityRotation());
		
		Collections.sort(solutions);
		
		return solutions;
	}

	@Override
	public ArrayList<Rotation> getAlignment(Molecule mol1, Molecule mol2) throws AtomNotFoundException
	{
		return this.aligner.align(mol1, mol2);
	}

	/**
	 * Notify observers.
	 * 
	 * @param value
	 *          the value
	 */
	synchronized private void notifyObservers(double value)
	{
		for (ProgressObserver o : this.observerList)
			o.update(value);
	}

	@Override
	public synchronized void removeProgressObserver(ProgressObserver o)
	{
		this.observerList.remove(o);
	}

}
