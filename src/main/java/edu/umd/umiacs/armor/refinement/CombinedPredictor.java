/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.refinement;

import java.util.ArrayList;

import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.BaseExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.BaseExperimentalDatum;

public final class CombinedPredictor implements Predictor
{

	private final Predictor predictor1;
	private final Predictor predictor2;

	public CombinedPredictor(Predictor predictor1, Predictor predictor2)
	{
		this.predictor1 = predictor1;
		this.predictor2 = predictor2;
	}

	@Override
	public CombinedTwoDomainRigidPredictor createTwoDomainRigidPredictor(Molecule mol1, Molecule mol2)
			throws AtomNotFoundException
	{
		return new CombinedTwoDomainRigidPredictor(this.predictor1.createTwoDomainRigidPredictor(mol1, mol2),
				this.predictor2.createTwoDomainRigidPredictor(mol1, mol2));
	}

	@Override
	public BaseExperimentalData getData()
	{
		ExperimentalData<? extends ExperimentalDatum> data1 = this.predictor1.getData();
		ExperimentalData<? extends ExperimentalDatum> data2 = this.predictor2.getData();
		
		ArrayList<BaseExperimentalDatum> data = new ArrayList<BaseExperimentalDatum>(data1.size()+data2.size());
		for (ExperimentalDatum datum : this.predictor1.getData())
			data.add(new BaseExperimentalDatum(datum.value(), datum.error()));
		for (ExperimentalDatum datum : this.predictor2.getData())
			data.add(new BaseExperimentalDatum(datum.value(), datum.error()));
		
		return new BaseExperimentalData(data);
	}

	@Override
	public double[] predict(Molecule mol) throws AtomNotFoundException
	{
		return BasicUtils.combineArrays(this.predictor1.predict(mol), this.predictor2.predict(mol));
	}

}
