/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.refinement;

import java.util.List;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;

/**
 * The Interface TwoDomainRigidPredictor.
 */
public interface TwoDomainRigidPredictor
{
	
	/**
	 * Creates the rotated.
	 * 
	 * @param R
	 *          the r
	 * @return the two domain rigid predictor
	 */
	public TwoDomainRigidPredictor createRotated(Rotation R);
	
	public ExperimentalData<? extends ExperimentalDatum> getData();
	
	/**
	 * Gets the domain one.
	 * 
	 * @return the domain one
	 */
	public Molecule getDomainOne();
	
	/**
	 * Gets the domain two.
	 * 
	 * @return the domain two
	 */
	public Molecule getDomainTwo();
	
	/**
	 * Inits the positions.
	 * 
	 * @param t
	 *          the t
	 * @return the array list
	 */
	public List<Point3d> initPositions(RigidTransform t);
	
	/**
	 * Predict.
	 * 
	 * @param translation
	 *          the translation
	 * @return the double[]
	 */
	public double[] predict(Point3d translation);
	
	/**
	 * Predict.
	 * 
	 * @param t
	 *          the t
	 * @return the double[]
	 */
	public double[] predict(RigidTransform t);

	/**
	 * Predict.
	 * 
	 * @param R
	 *          the r
	 * @return the double[]
	 */
	public double[] predict(Rotation R);
}
