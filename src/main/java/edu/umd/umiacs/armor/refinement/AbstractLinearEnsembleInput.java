/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.refinement;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrixDynamic;
import edu.umd.umiacs.armor.math.optim.OptimizationRuntimeException;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.DuplicateAtomException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.Pair;

public abstract class AbstractLinearEnsembleInput implements LinearEnsembleInput, EnsembleInputProgressObservable
{
	protected final class PredictorRun implements Runnable
	{
		private final List<Exception> exceptionList;
		private final int index;
		private final Molecule mol;
		private final AtomicInteger numberOfModelsProcessed;
		private final RealMatrixDynamic predictionMatrix;
		
		public PredictorRun(Molecule mol, List<Exception> exceptionList, RealMatrixDynamic A, int index, AtomicInteger numberOfModelsProcessed)
		{
			this.mol = mol;
			this.exceptionList = exceptionList;
			this.predictionMatrix = A;
			this.index = index;
			this.numberOfModelsProcessed= numberOfModelsProcessed;
		}

		@Override
		public void run()
		{
			try
			{
				//perform the parallel operation
				double[] prediction = predict(this.mol);
				
				//update the matrix and the progress observers
				synchronized (this.predictionMatrix)
				{
					this.predictionMatrix.setColumn(this.index, prediction);
				}

				//record the process
				int num = this.numberOfModelsProcessed.getAndIncrement();
				notifyUpdatedColumnGenerated(num+1);
			}
			catch (Exception e)
			{
				this.exceptionList.add(e);
			}
		}
	}

	private final ArrayList<Pair<Integer,Integer>> modelIndicies;
	
	private transient ArrayList<EnsembleInputProgressObserver> observers;
	private final boolean parallel; 
	private ArrayList<File> pdbFiles;
	private RealMatrix predictionMatrix;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8771030103737623620L;
	
	public AbstractLinearEnsembleInput(AbstractLinearEnsembleInput prev, List<Integer> indicies) 
	{
		this.parallel = prev.parallel;
		this.pdbFiles = new ArrayList<File>(prev.pdbFiles);
		this.observers = new ArrayList<EnsembleInputProgressObserver>();
		this.modelIndicies =  new ArrayList<Pair<Integer,Integer>>(prev.modelIndicies);
		
		BlockRealMatrix A = new BlockRealMatrix(indicies.size(), this.modelIndicies.size());
		int count = 0;
		for (int index : indicies)
		{
			//add the row to the matrix
			for (int col=0; col<A.numColumns(); col++)
				A.set(count, col, prev.predictionMatrix.get(index, col));
				
		  count++;
		}
		
		this.predictionMatrix = A;
	}
	
	public AbstractLinearEnsembleInput(boolean parallel)
	{
		this.parallel = parallel;
		this.pdbFiles = new ArrayList<File>();
		this.observers = new ArrayList<EnsembleInputProgressObserver>();
		this.modelIndicies =  new ArrayList<Pair<Integer,Integer>>();
		this.predictionMatrix = null;
	}
	
	public AbstractLinearEnsembleInput(List<File> files, RealMatrix A, boolean parallel) throws PdbException
	{
		this.parallel = parallel;
		this.pdbFiles = new ArrayList<File>();
		this.observers = new ArrayList<EnsembleInputProgressObserver>();
		this.modelIndicies =  new ArrayList<Pair<Integer,Integer>>();
		this.predictionMatrix = A;
		
		int count =0;
		for (File name : files)
		{
			//read the file
			PdbStructure pdbStruct = MoleculeFileIO.readPDB(name);
			
			//get number of models
			int numModels = pdbStruct.getNumberOfModels();
			
			for (int model=0; model<numModels; model++)
			{
				this.modelIndicies.add(new Pair<Integer, Integer>(count, model));
				
				//check if already exceeded the number of models
				if (this.modelIndicies.size()>this.predictionMatrix.numColumns())
					break;
			}
			
			count++;
		}
		
		if (this.predictionMatrix.numColumns()!=this.modelIndicies.size())
			throw new RefinementRuntimeException("Matrix column size does not match number of models in pdb.");
		
	}
	
	private void addModelsFromFile(List<File> moleculeFiles, RealMatrix predictionMatrix, boolean isTextFile) throws PdbException
	{
		ExperimentalData<? extends ExperimentalDatum> expData = getExperimentalData();
		int expSize = expData.size();
		
		//if nothing, do nothing
		if (predictionMatrix==null && moleculeFiles==null)
			return;

		//check the variable sizes
		if (predictionMatrix!=null && predictionMatrix.numRows()!= expSize)
			throw new RefinementRuntimeException("Data size ("+expSize+") does not match number of matrix rows ("+predictionMatrix.numRows()+").");
		
		//create an empty file
		if (moleculeFiles==null)
		{
			moleculeFiles = new ArrayList<File>(1);
			moleculeFiles.add(new File(""));
		}
		
		//create thread pool
    ExecutorService execSvc = Executors.newCachedThreadPool();
    final List<Exception> exceptionList = Collections.synchronizedList(new ArrayList<Exception>());
    final AtomicInteger numberOfModelsProcessed = new AtomicInteger(0);
    
    ArrayList<File> addedPdbFiles = new ArrayList<File>();
    ArrayList<Pair<Integer,Integer>> addedModelIndicies = new ArrayList<Pair<Integer,Integer>>();
		
    //add previous result
		final ArrayList<RealMatrix> matrixList = new ArrayList<RealMatrix>();
		
		int count = 0;
		for (File name : moleculeFiles)
		{
			//check if already any exceptions
			if (!exceptionList.isEmpty())
	    	throw new RefinementRuntimeException(exceptionList.get(0));				
			
			//read the file
			PdbStructure pdbStruct = null;
			Molecule mol = null;
			int numberOfModels = 0;
			if (!name.toString().isEmpty())
			{
				if (isTextFile)
				{
					try
					{
						mol = MoleculeFileIO.readTextFile(name);
					}
					catch (DuplicateAtomException e)
					{
						throw new PdbException(e);
					}
					numberOfModels = 1;
				}
				else
				{
					pdbStruct = MoleculeFileIO.readPDB(name);
					numberOfModels = pdbStruct.getNumberOfModels();
				}
			}
			else
				numberOfModels = predictionMatrix.numColumns()-count;

			
			//add the file to the list
			addedPdbFiles.add(name);

			//if need to predict the values
			if (predictionMatrix==null)
			{
				final RealMatrixDynamic Anew = new Array2dRealMatrix(expSize, numberOfModels);
				
				for (int model=0; model<numberOfModels; model++) 
				{				
					//gerenate a molecule from the pdb structure
					if (!isTextFile)
						mol = pdbStruct.getModel(model).createBasicMolecule();
					
					//enqueue the task
					if (!this.parallel)
					{
						PredictorRun singleRun = new PredictorRun(mol, exceptionList, Anew, model, numberOfModelsProcessed);
						singleRun.run();
					}
					else
						execSvc.execute(new PredictorRun(mol, exceptionList, Anew, model, numberOfModelsProcessed));
					
					//store the location of solution in the pdb file list
					addedModelIndicies.add(new Pair<Integer,Integer>(addedPdbFiles.size()-1, model));
				}

				//add the matrix represent all the models to a list of matricies
				matrixList.add(Anew);
			}
			else
			{
				//just add the values and the associated pdb name
				
				if (pdbStruct!=null && (count+numberOfModels>predictionMatrix.numColumns()))
					throw new RefinementRuntimeException("Matrix size does not match number of models in files.");
				
				//add the model to list
				for (int model=0; model<numberOfModels; model++)
					addedModelIndicies.add(new Pair<Integer,Integer>(addedPdbFiles.size()-1, model));
				
				//add the matrix represent all the models to a list of matricies
				matrixList.add(predictionMatrix.getSubMatrix(0, predictionMatrix.numRows()-1, count, count+numberOfModels-1));
				notifyUpdatedColumnGenerated(count+numberOfModels);
			}
			
			count+= numberOfModels;
	  }
		
    //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(60L, TimeUnit.DAYS);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new RefinementRuntimeException("Unable to finish all tasks.", e);
    }

    //if there was exception, throw it
    if (!exceptionList.isEmpty())
    	throw new RefinementRuntimeException(exceptionList.get(0));
		
		//combine the matrices from different threads
		int numberOfColumns = 0;
		for (int iter=0; iter<matrixList.size(); iter++)
			numberOfColumns+= matrixList.get(iter).numColumns();
		
		synchronized (this)
		{
			//check if adding data to previous data
			if (this.predictionMatrix != null)
			{
				if (this.predictionMatrix.numRows()!=matrixList.get(0).numRows())
					throw new RefinementRuntimeException("Number of rows of newly added matrix must match previously added matrix.");
				
				numberOfColumns += this.predictionMatrix.numColumns();
				matrixList.add(0, this.predictionMatrix);
			}

			//merge the information
			for (Pair<Integer,Integer> index : addedModelIndicies)
			{
				File currPdb = addedPdbFiles.get(index.x);
				int pdbIndex = this.pdbFiles.indexOf(currPdb);
				
				//if the file does not exist, add it
				if (pdbIndex<0)
				{
					this.modelIndicies.add(new Pair<Integer,Integer>(this.pdbFiles.size(), index.y));
					this.pdbFiles.add(currPdb);
				}
				else
				{
					//just add the index
					this.modelIndicies.add(new Pair<Integer,Integer>(pdbIndex, index.y));
				}
			}

			BlockRealMatrix Acombined = new BlockRealMatrix(expSize, numberOfColumns);
			int offset = 0;
			for (int iter=0; iter<matrixList.size(); iter++)
			{
				RealMatrix currA = matrixList.get(iter);
				for (int row=0; row<currA.numRows(); row++)
					for (int column=0; column<currA.numColumns(); column++)
						Acombined.set(row, offset+column, currA.get(row, column));
				
				offset += currA.numColumns();
			}
					
			this.predictionMatrix = Acombined;		
		}
	}
	
	public void addPdbModels(List<File> pdbFiles) throws PdbException
	{
		addModelsFromFile(pdbFiles, null, false);
	}
	
	public void addPdbModels(List<File> pdbFiles, RealMatrix predictionMatrix) throws PdbException
	{
		addModelsFromFile(pdbFiles, predictionMatrix, false);
	}

	
	public void addPdbModels(RealMatrix A) throws PdbException
	{
		addPdbModels(null, A);
	}
	
	@Override
	public void addProgressObserver(EnsembleInputProgressObserver o)
	{
		synchronized (this.observers)
		{
			this.observers.add(o);			
		}
	}
	
	public void addTextModels(List<File> pdbFiles) throws PdbException
	{
		addModelsFromFile(pdbFiles, null, true);
	}
	
	@Override
	public double dataNorm()
	{
		return BasicMath.norm(getExperimentalData().relativeValues());
	}

	@Override
	public int ensembleSize()
	{
		return this.predictionMatrix.numColumns();
	}
	
	@Override
	public RealMatrix getA()
	{
		double[] errors = getExperimentalData().errors();
		
		boolean allOnes = true;
		for (double val : errors)
			if (Math.abs(val-1.0)>1.0e-12)
			{
				allOnes = false;
				break;
			}
		
		if (allOnes)
			return this.predictionMatrix;
		
		//change to max likelyhood
		BlockRealMatrix predictionMatrix = new BlockRealMatrix(this.predictionMatrix.toArray());
		for (int row=0; row<predictionMatrix.numRows(); row++)
		{
			double error = errors[row];
			
			if (Double.isInfinite(error) || error==0.0)
				throw new OptimizationRuntimeException("Invalid value for error: "+error);
			
			if (Double.isNaN(error))
				error = 1.0;

			for (int col=0; col<predictionMatrix.numColumns(); col++)
				predictionMatrix.set(row, col, predictionMatrix.get(row, col)/error);
		}
		
		return predictionMatrix;
	}
		
	@Override
	public Pair<Integer,Integer> getModelFileIndex(int column)
	{
		return this.modelIndicies.get(column);
	}
	
	@Override
	public File getPdbFile(int index)
	{
		return this.pdbFiles.get(index);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.refinement.LinearEnsembleInput#getPdbFiles()
	 */
	@Override
	public List<File> getPdbFiles()
	{
		return new ArrayList<File>(this.pdbFiles);
	}
	
	@Override
	public RealMatrix getPredictionMatrix()
	{
		return this.predictionMatrix;
	}
	
	@Override
	public double[] getY()
	{
		return getExperimentalData().relativeValues();
	}
	
	private void notifyUpdatedColumnGenerated(int num)
	{
		synchronized (this.observers)
		{
			for (EnsembleInputProgressObserver o : this.observers)
				o.updateProcessed(num);
		}
	}

	@Override
	public int numFiles()
	{
		return this.pdbFiles.size();
	}

	public abstract double[] predict(Molecule mol) throws AtomNotFoundException;
	
	private void readObject(ObjectInputStream aStream) throws IOException, ClassNotFoundException 
	{
	  aStream.defaultReadObject();
	  this.observers = new ArrayList<EnsembleInputProgressObserver>();
	}

	@Override
	public void removeProgressObserver(EnsembleInputProgressObserver o)
	{
		synchronized (this.observers)
		{
			this.observers.remove(o);
		}
	}

	@Override
	public void setNewPdbDirectory(File dir)
	{
		ArrayList<File> newFileList = new ArrayList<File>(this.pdbFiles.size());
		for (File file : this.pdbFiles)
		{
			newFileList.add(new File(dir.getPath()+File.separator+file.getName()));
		}
		
		this.pdbFiles = newFileList;
	}

	protected void setPredictionMatrix(RealMatrix A)
	{
		if (this.predictionMatrix.numColumns()!=A.numColumns() || this.predictionMatrix.numRows()!=A.numRows())
			throw new RefinementRuntimeException("Updated matrix must be the same size as original.");
	
		this.predictionMatrix = A;
	}

	private void writeObject(ObjectOutputStream aStream) throws IOException 
	{
		aStream.defaultWriteObject();
	}
	
	@Override
	public double dataChi2()
	{
		return BasicStat.chi2(getExperimentalData().values(), getExperimentalData().errors());
	}
	
	@Override
	public double yNorm()
	{
		return BasicMath.norm(getY());
	}
}
