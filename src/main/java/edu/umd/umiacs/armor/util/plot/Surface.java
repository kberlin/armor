/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util.plot;

import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.colors.colormaps.ColorMapGrayscale;
import org.jzy3d.colors.colormaps.ColorMapRainbow;
import org.jzy3d.maths.Range;
import org.jzy3d.plot3d.builder.Builder;
import org.jzy3d.plot3d.builder.Mapper;
import org.jzy3d.plot3d.builder.concrete.OrthonormalGrid;
import org.jzy3d.plot3d.primitives.Shape;
import edu.umd.umiacs.armor.math.func.TwoVariableFunction;

/**
 * The Class Surface.
 */
public final class Surface
{
	
	/**
	 * The Class TwoVariableFunctionToMapper.
	 */
	private class TwoVariableFunctionToMapper extends Mapper
	{
		
		/** The func. */
		private TwoVariableFunction func;
		
		/**
		 * Instantiates a new two variable function to mapper.
		 * 
		 * @param func
		 *          the func
		 */
		public TwoVariableFunctionToMapper(TwoVariableFunction func)
		{
			this.func = func;
		}
		
		/* (non-Javadoc)
		 * @see org.jzy3d.plot3d.builder.Mapper#f(double, double)
		 */
		@Override
		public double f(double x, double y)
		{
			return this.func.value(x, y);
		}
		
	}
	
	/** The surface. */
	private final Shape surface;
	
	/**
	 * Instantiates a new surface.
	 * 
	 * @param func
	 *          the func
	 * @param xLimit
	 *          the x limit
	 * @param numXSamples
	 *          the num x samples
	 * @param yLimit
	 *          the y limit
	 * @param numYSamples
	 *          the num y samples
	 */
	public Surface(TwoVariableFunction func, Limit xLimit, int numXSamples, Limit yLimit, int numYSamples)
	{
		TwoVariableFunctionToMapper mapper = new TwoVariableFunctionToMapper(func);
		
		Range rangeX = new Range(xLimit.getMin(), xLimit.getMax());
		Range rangeY = new Range(yLimit.getMin(), yLimit.getMax());
		
		this.surface = (Shape) Builder.buildOrthonormal(new OrthonormalGrid(rangeX, numXSamples, rangeY, numYSamples), mapper);
		
		this.surface.setColorMapper(new ColorMapper(new ColorMapRainbow(), this.surface.getBounds().getZmin(), this.surface.getBounds().getZmax(), new Color(1, 1, 1, 1.0f)));
		this.surface.setFaceDisplayed(true);
		this.surface.setWireframeDisplayed(true);
		this.surface.setWireframeColor(Color.BLACK);
	}
		
	/**
	 * Gets the surface.
	 * 
	 * @return the surface
	 */
	protected Shape getSurface()
	{
		return this.surface;
	}
	
	/**
	 * Sets the gray color.
	 * 
	 * @param color
	 *          the new gray color
	 */
	public void setGrayColor(Color color)
	{
		this.surface.setColorMapper(new ColorMapper(new ColorMapGrayscale(), this.surface.getBounds().getZmin(), this.surface.getBounds().getZmax()*1.3f, color));		
	}
	
	/**
	 * Sets the wireframe displayed.
	 * 
	 * @param value
	 *          the new wireframe displayed
	 */
	public void setWireframeDisplayed(boolean value)
	{
		this.surface.setWireframeDisplayed(value);
	}
}
