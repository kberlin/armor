/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * The Class BasicUtils.
 */
public final class BasicUtils
{
	
	public static double[] combineArrays(double[] a, double[] b)
	{
		double[] newArray = new double[a.length+b.length];
		
		for (int iter=0; iter<a.length; iter++)
			newArray[iter] = a[iter];
		for (int iter=0; iter<b.length; iter++)
			newArray[a.length+iter] = b[iter];
		
		return newArray;
	}
	
	/**
	 * Copy array.
	 * 
	 * @param A
	 *          the a
	 * @return the double[][]
	 */
	public static double[][] copyArray(double[][] A)
	{
		if (A==null)
			return null;
		
		double[][] val = new double[A.length][];
		for (int row=0; row<A.length; row++)
		{
			val[row] = new double[A[row].length];
			for (int col=0; col<A[row].length; col++)
				val[row][col] = A[row][col];
		}
			
		return val;
	}
	
	/**
	 * Creates the array.
	 * 
	 * @param size
	 *          the size
	 * @param initValue
	 *          the init value
	 * @return the double[]
	 */
	public static double[] createArray(int size, double initValue)
	{
		double[] array = new double[size];
		
		Arrays.fill(array, initValue);
		
		return array;
	}

	/**
	 * Double array subset.
	 * @param array
	 *          the array
	 * @param index
	 *          the index
	 * 
	 * @return the double[]
	 */
	public static double[] doubleArraySubset(double[] array, int[] index)
	{
		double[] newArray = new double[index.length];
		
		for (int iter=0; iter<index.length; iter++)
			newArray[iter] = array[index[iter]];
		
		return newArray;
	}
	
	/**
	 * Double array subset.
	 * @param array
	 *          the array
	 * @param index
	 *          the index
	 * 
	 * @return the double[]
	 */
	public static double[] doubleArraySubset(double[] array, List<Integer> index)
	{
		double[] newArray = new double[index.size()];
		
		int iter = 0;
		for (int value : index)
		{
			newArray[iter] = array[value];
			iter++;
		}
		
		return newArray;
	}
	
	/**
	 * Gets the file scanner.
	 * 
	 * @param file
	 *          the file
	 * @return the file scanner
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	public static Scanner getFileScanner(File file) throws IOException
	{
		FileInputStream stream = new FileInputStream(file);
		InputStreamReader inputStream = new InputStreamReader(stream);
		
		Scanner scanner = new Scanner(new BufferedReader(inputStream));
		scanner.useLocale(Locale.US);
		  
		return scanner;			
	}
	
	public static boolean hasInfinite(double[] a)
	{
		if (a==null)
			return false;
		
		for (int iter=0; iter<a.length; iter++)
			if (Double.isInfinite(a[iter]))
			  return true;
			
		return false;
	}
			
	/**
	 * Checks for na n.
	 * 
	 * @param a
	 *          the a
	 * @return true, if successful
	 */
	public static boolean hasNaN(double[] a)
	{
		if (a==null)
			return false;
		
		for (int iter=0; iter<a.length; iter++)
			if (Double.isNaN(a[iter]))
			  return true;
			
		return false;
	}
	
	public static boolean isMac() 
	{
 
		String os = System.getProperty("os.name").toLowerCase();
		// Mac
		return (os.indexOf("mac") >= 0);
 
	}
	
	public static boolean isSolaris() 
	{
 
		String os = System.getProperty("os.name").toLowerCase();
		// Solaris
		return (os.indexOf("sunos") >= 0);
 
	}

	public static boolean isUnix() 
	{
 
		String os = System.getProperty("os.name").toLowerCase();
		// linux or unix
		return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);
 
	}
	
	public static boolean isWindows() 
	{
		 
		String os = System.getProperty("os.name").toLowerCase();
		// windows
		return (os.indexOf("win") >= 0);
 
	}

	
	public static double[] permute(double[] a, int[] order)
	{
		double[] newA = new double[a.length];
		
		for (int iter=0; iter<a.length; iter++)
			newA[iter] = a[order[iter]];
		
		return newA;
	}
	
	/**
	 * Read file to string.
	 * 
	 * @param file
	 *          the file
	 * @return the string
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	public static String readFileToString(File file) throws IOException
	{
		Scanner scanner = getFileScanner(file);
		scanner.useLocale(Locale.US);
		
		try
		{
			StringBuilder str = new StringBuilder();
			while (scanner.hasNextLine())
				str.append(scanner.nextLine()+"\n");
	
			return str.toString();
		}
		finally
		{
			scanner.close();
		}
	}

	public static String readStreamToString(InputStream in) throws IOException
	{
		Scanner scanner = new Scanner(new BufferedReader(new InputStreamReader(in)));
		scanner.useLocale(Locale.US);
		
		try
		{
			StringBuilder str = new StringBuilder();
			while (scanner.hasNextLine())
				str.append(scanner.nextLine()+"\n");
	
			return str.toString();
		}
		finally
		{
			scanner.close();
		}
	}
	
	/**
	 * Sets the array.
	 * 
	 * @param a
	 *          the a
	 * @param value
	 *          the value
	 */
	public static void setArray(double[] a, double value)
	{
		setArray(a,0,a.length-1,value);
	}
	
	/**
	 * Sets the array.
	 * 
	 * @param a
	 *          the a
	 * @param start
	 *          the start
	 * @param end
	 *          the end
	 * @param value
	 *          the value
	 */
	public static void setArray(double[] a, int start, int end, double value)
	{
		if (a==null)
			return;
		
		for (int iter=start; iter<=end; iter++)
			a[iter] = value;
	}

	public static int[] sortIndex(final double[] val)
  {
  	Integer[] index = new Integer[val.length];
  	
  	for (int iter=0; iter<index.length; iter++)
  		index[iter] = iter;
  	
  	Arrays.sort(index, new Comparator<Integer>() 
  	{
      @Override public int compare(final Integer i1, final Integer i2) 
      {
          return Double.compare(val[i1], val[i2]);
      }
  	});
  	
  	int[] ret = new int[index.length];
    for (int i = 0; i < ret.length; i++)
        ret[i] = index[i];

    return ret;
  }
	
	public static int[] sortIndexReverse(final double[] val)
  {
  	Integer[] index = new Integer[val.length];
  	
  	for (int iter=0; iter<index.length; iter++)
  		index[iter] = iter;
  	
  	Arrays.sort(index, new Comparator<Integer>() 
  	{
      @Override public int compare(final Integer i1, final Integer i2) 
      {
          return Double.compare(val[i2], val[i1]);
      }
  	});
  	
  	int[] ret = new int[index.length];
    for (int i = 0; i < ret.length; i++)
        ret[i] = index[i];

    return ret;
  }
	
	public static int[] toArray(List<Integer> list)
	{
		int[] array = new int[list.size()];
		
		int count = 0;
		for (int value : list)
		{
			array[count] = value;
			count++;
		}
		
		return array;
	}
	
	public static String toString(double[] a)
	{
		return Arrays.toString(a);
	}
	
  public static String toString(double[][] A)
	{
		StringBuilder s = new StringBuilder();
		
		s.append("[");

		for (double[] a : A)
		{
			if (a!=null)
			{
				for (int iter=0; iter<a.length-1; iter++)
					s.append(""+a[iter]+",");
				
				if (a.length>0)
					s.append(""+a[a.length-1]);
			}
			s.append("\n");
		}
		s.append("]");
		
		return new String(s);
	}
  
  /**
	 * Array to string.
	 * 
	 * @param a
	 *          the a
	 * @return the string
	 */
	public static String toString(float[] a)
	{
		return Arrays.toString(a);
	}

	public static String toString(int[] a)
	{
		return Arrays.toString(a);
	}
	
	public static double[] uniformLinePoints(double min, double max, int n)
	{
		if (n<=0)
			return null;
		
		double[] val = new double[n];
		
		if (n==1)
		{
			val[0] = min;
			return val;
		}
		
		double h = (max-min)/(n-1);
		
		for (int iter=0; iter<n; iter++)
			val[iter] = min+iter*h;
		
		return val;
	}
 
	/**
	 * Uniform line points exclusive.
	 * 
	 * @param min
	 *          the min
	 * @param max
	 *          the max
	 * @param n
	 *          the n
	 * @return the double[]
	 */
	public static double[] uniformLinePointsExclusive(double min, double max, int n)
	{
		if (n<=0)
			return null;
		
		double[] val = new double[n];
		
		if (n==1)
		{
			val[0] = min;
			return val;
		}
		
		double h = (max-min)/n;
		
		for (int iter=0; iter<n; iter++)
			val[iter] = min+iter*h;
		
		return val;
	}
 
	/**
	 * Uniform spaced points.
	 * 
	 * @param min
	 *          the min
	 * @param max
	 *          the max
	 * @param spacing
	 *          the spacing
	 * @return the double[]
	 */
	public static double[] uniformSpacedPoints(double min, double max, double spacing)
	{
		int n = (int)Math.floor((max-min)/spacing)+1;
		
		double[] line = new double[n];
		
		for (int iter=0; iter<n; iter++)
			line[iter] = min+(double)iter*spacing;
		
		return line;
	}
 
	/**
	 * Uniform spaced points exclusive.
	 * 
	 * @param min
	 *          the min
	 * @param max
	 *          the max
	 * @param spacing
	 *          the spacing
	 * @return the double[]
	 */
	public static double[] uniformSpacedPointsExclusive(double min, double max, double spacing)
	{
		int n = (int)Math.floor((max-min)/spacing);
		
		double[] line = new double[n];
		
		for (int iter=0; iter<n; iter++)
			line[iter] = min+(double)iter*spacing;
		
		return line;
	}

}
