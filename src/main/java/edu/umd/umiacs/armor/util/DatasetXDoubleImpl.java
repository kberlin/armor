/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * The Class DatasetXDoubleImpl.
 * 
 * @param <YType>
 *          the generic type
 */
public class DatasetXDoubleImpl<YType> implements DatasetXDouble<YType>
{
	/** The point. */
	private final ArrayList<SortablePair<Double,YType>> data;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 672871163830519675L;
	
	/**
	 * Instantiates a new dataset x double impl.
	 */
	public DatasetXDoubleImpl()
	{
		this.data = new ArrayList<SortablePair<Double,YType>>();
	}
	
	public DatasetXDoubleImpl(DatasetXDoubleImpl<YType> value)
	{
		this(value, false);
	}
	
	protected DatasetXDoubleImpl(DatasetXDoubleImpl<YType> prev, boolean byReference)
	{
		if (!byReference)
			this.data = new ArrayList<SortablePair<Double,YType>>(prev.data);
		else
			this.data = prev.data;
	}
		
	/**
	 * Instantiates a new dataset x double impl.
	 * 
	 * @param xList
	 *          the x list
	 * @param yList
	 *          the y list
	 */
	public DatasetXDoubleImpl(List<Double> xList, List<YType> yList)
	{
		if (xList.size()!= yList.size())
			throw new ArrayIndexOutOfBoundsException("List sizes must be equal.");
		
		this.data = new ArrayList<SortablePair<Double,YType>>();

		Iterator<YType> yListIterator = yList.iterator();
		for (Double x : xList)
		{
			this.data.add(new SortablePair<Double, YType>(x, yListIterator.next()));
		}
	}
		
	public void add(DatasetXDoubleImpl<YType> data)
	{
		this.data.addAll(data.data);
	}
	
	public void add(Double x, YType y)
  {
  	this.data.add(new SortablePair<Double,YType>(x, y));
  }
	
	public void add(List<SortablePair<Double, YType>> data)
	{
		this.data.addAll(data);
	}
	
	public void add(SortablePair<Double, YType> p)
	{
		this.data.add(p);
	}

  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.DatasetXDouble#cut(double, double)
	 */
	@Override
	public DatasetXDoubleImpl<YType> cut(double min, double max)
	{
		DatasetXDoubleImpl<YType> newData = new DatasetXDoubleImpl<YType>();
		
		for (int index=0; index<size(); index++)
		{
			SortablePair<Double,YType> p = getValue(index);
			if (p.x<min || p.x>max)
			{
				newData.add(p);
			}
		}
		
		return newData;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.DatasetXDouble#filter(double, double)
	 */
	@Override
	public DatasetXDoubleImpl<YType> filter(double min, double max)
	{
		DatasetXDoubleImpl<YType> newData = new DatasetXDoubleImpl<YType>();
		
		for (int index=0; index<size(); index++)
		{
			SortablePair<Double,YType> p = this.data.get(index);
			if (p.x>=min && p.x<=max)
			{
				newData.add(p);
			}
		}
		
		return newData;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.DatasetInt#getValue(int)
	 */
	@Override
	public SortablePair<Double,YType> getValue(int index)
	{
		return this.data.get(index);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.Dataset#getX(int)
	 */
	@Override
	public Double getX(int index)
	{
		return this.data.get(index).x;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.DatasetXDouble#getXArray()
	 */
	@Override
	public double[] getXArray()
	{
		double[] array = new double[size()];
		
		int count = 0;
		for (SortablePair<Double,YType> p : this.data)
		{
			array[count] = p.x;
			count++;
		}
		
		return array;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.Dataset#getXList()
	 */
	@Override
	public ArrayList<Double> getXList()
	{
		ArrayList<Double> list = new ArrayList<Double>(this.data.size());
		for (SortablePair<Double, YType> p : this.data)
		{
			list.add(p.x);
		}
		
		return list;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.Dataset#getY(int)
	 */
	@Override
	public YType getY(int index)
	{
		return this.data.get(index).y;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.Dataset#getYList()
	 */
	@Override
	public ArrayList<YType> getYList()
	{
		ArrayList<YType> list = new ArrayList<YType>(this.data.size());
		for (SortablePair<Double, YType> p : this.data)
		{
			list.add(p.y);
		}
		
		return list;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.DatasetXDouble#hasSameX(edu.umd.umiacs.armor.util.DatasetXDouble)
	 */
	@Override
	public boolean hasSameX(DatasetXDouble<YType> dataset)
	{
		if (size() != dataset.size())
			return false;
		
		for (int iter=0; iter<size(); iter++)
			if (Math.abs(getX(iter).doubleValue()-dataset.getX(iter).doubleValue())>1.0e-16)
				return false;
		
		return true;
	}
	
	@Override
	public Iterator<SortablePair<Double, YType>> iterator()
	{
		return this.data.iterator();
	}
	
	/**
	 * Sets the.
	 * 
	 * @param index
	 *          the index
	 * @param val
	 *          the val
	 */
	public void set(int index, YType val)
	{
		SortablePair<Double,YType> p = this.data.get(index);
		this.data.set(index, new SortablePair<Double,YType>(p.x, val));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.DatasetInt#size()
	 */
	@Override
	public int size()
	{
		return this.data.size();
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.Dataset#sort()
	 */
	@Override
	public DatasetXDoubleImpl<YType> sort()
	{
		return sort(SortType.ASCENDING);
	}
	
	@Override
	public DatasetXDoubleImpl<YType> sort(SortType type)
	{
		ArrayList<SortablePair<Double, Integer>> pairList = new ArrayList<SortablePair<Double, Integer>>(this.data.size());
		for (int iter=0; iter<this.data.size(); iter++)
			pairList.add(new SortablePair<Double, Integer>(this.data.get(iter).x, iter));
		
		//sort the list
		Collections.sort(pairList);
		
		//extract the information back
		Double[] xval = new Double[pairList.size()];
		ArrayList<YType> yval = new ArrayList<YType>(pairList.size());
		
		//create enough entries in list
		for (int iter=0; iter<pairList.size(); iter++)
			yval.add(null);
		
		//create sorted list
		for (int iter=0; iter<pairList.size(); iter++)
		{
			int index = pairList.get(iter).y;
			
			xval[iter] = this.data.get(index).x;
			yval.set(iter, this.data.get(index).y);
		}
		
		//add the sorted list to the dataset
		DatasetXDoubleImpl<YType> newDataset = new DatasetXDoubleImpl<YType>();
		for (int iter=0; iter<xval.length; iter++)
			newDataset.add(new SortablePair<Double,YType>(xval[iter], yval.get(iter)));
		
		return newDataset;
	}

	public String toMatlab()
	{
		StringBuilder str = new StringBuilder();
		
		str.append("[");
		for (int iter=0; iter<this.data.size(); iter++)
		{
			str.append(""+getX(iter)+", "+ getY(iter));			
		}
		str.append("]");
		
		return str.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		
		for (int iter=0; iter<this.data.size(); iter++)
		{
			str.append("["+getX(iter)+", "+ getY(iter)+"]");
			if (iter<this.data.size()-1)
				str.append(", ");
		}
		
		return str.toString();
	}
}
