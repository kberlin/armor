/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util.plot;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

/**
 * The Class InstantPlot.
 */
public class InstantPlot extends JFrame
{
	/** The plot. */
	private Plot2d plot;
	/**
	 * 
	 */
	private static final long serialVersionUID = 4370035550301019527L;
	
	/**
	 * Instantiates a new instant plot.
	 */
	public InstantPlot()
	{
		getContentPane().setBackground(java.awt.Color.WHITE);
	
		setBounds(100, 100, 500, 600);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		// create the double layout
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));

		//add plot to current window
		this.plot = new Plot2d();
		getContentPane().add(this.plot);

		setVisible(true);

	}
	
	/**
	 * Gets the plot.
	 * 
	 * @return the plot
	 */
	public Plot2d getPlot()
	{
		return this.plot;
	}
}
