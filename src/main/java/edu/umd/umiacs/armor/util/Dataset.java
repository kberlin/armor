/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import java.io.Serializable;
import java.util.List;

/**
 * The Interface Dataset.
 * 
 * @param <XType>
 *          the generic type
 * @param <YType>
 *          the generic type
 */
public interface Dataset<XType extends Comparable<XType>, YType> extends Serializable, Iterable<SortablePair<XType,YType>>
{
	/**
	 * Gets the value.
	 * 
	 * @param index
	 *          the index
	 * @return the value
	 */
	public SortablePair<? extends XType,? extends YType> getValue(int index);
	
	/**
	 * Gets the x.
	 * 
	 * @param index
	 *          the index
	 * @return the x
	 */
	public XType getX(int index);

	/**
	 * Gets the x list.
	 * 
	 * @return the x list
	 */
	public List<XType> getXList();

	/**
	 * Gets the y.
	 * 
	 * @param index
	 *          the index
	 * @return the y
	 */
	public YType getY(int index);

	/**
	 * Gets the y list.
	 * 
	 * @return the y list
	 */
	public List<YType> getYList();
	
	/**
	 * Size.
	 * 
	 * @return the int
	 */
	public int size();

	/**
	 * Sort.
	 * 
	 * @return the dataset
	 */
	public Dataset<XType,YType> sort();

	/**
	 * Sort.
	 * 
	 * @param type
	 *          the type
	 * @return the dataset
	 */
	public Dataset<XType,YType> sort(SortType type);

}