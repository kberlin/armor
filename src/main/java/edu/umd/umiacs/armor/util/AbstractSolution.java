package edu.umd.umiacs.armor.util;

import edu.umd.umiacs.armor.math.stat.BasicStat;

public abstract class AbstractSolution<ExpData extends ExperimentalData<ExpDatum>, ExpDatum extends ExperimentalDatum> implements Solution
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3948782065061868254L;
	
	private final ExpData data;
	
	public AbstractSolution(ExpData data)
	{
		this.data = data;
	}

	@Override
	public ExpData getExperimentalData()
	{
		return this.data;
	}
	
	public double chi2()
	{
		return BasicStat.chi2(predict(), this.data.values(), this.data.errors());
	}

	@Override
	public double[] errors()
	{
		return this.data.errors();
	}

	public double pearsonsCorrelation()
	{
		return BasicStat.pearsonsCorrelation(predict(), this.data.values());
	}

	public double[] residuals()
	{
		return BasicStat.residuals(predict(), values(), errors());
	}

	public int size()
	{
		return this.data.size();
	}

	@Override
	public double[] values()
	{
		return this.data.values();
	}

}