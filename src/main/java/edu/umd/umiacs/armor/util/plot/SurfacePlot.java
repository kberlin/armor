/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util.plot;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.jzy3d.chart.Chart;
import org.jzy3d.chart.controllers.mouse.ChartMouseController;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Cylinder;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.primitives.enlightables.EnlightableSphere;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.jzy3d.plot3d.rendering.legends.colorbars.ColorbarLegend;
import org.jzy3d.plot3d.rendering.lights.Light;

import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Class SurfacePlot.
 */
public class SurfacePlot extends JPanel
{
	/** The chart. */
	private final Chart chart;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1175145858577608669L;

	/**
	 * Instantiates a new surface plot.
	 */
	public SurfacePlot()
	{
		super();
		
		setLayout(new BorderLayout(0, 0));
		
		this.chart = getChart();

		this.add((javax.swing.JComponent) this.chart.getCanvas(), BorderLayout.CENTER);
		//addLight(chart, new Coord3d());
		
		this.chart.addController(new ChartMouseController(this.chart));
	}

	/**
	 * Adds the color bar.
	 * 
	 * @param surface
	 *          the surface
	 */
	public void addColorBar(Surface surface)
	{
		ColorbarLegend legend = new ColorbarLegend(surface.getSurface(), this.chart.getView().getAxe().getLayout().getZTickProvider(), this.chart.getView().getAxe().getLayout().getZTickRenderer());
		surface.getSurface().setLegend(legend);
		surface.getSurface().setLegendDisplayed(true); // opens a colorbar on the right part of
	}
	
	/**
	 * Adds the light.
	 * 
	 * @param position
	 *          the position
	 */
	public void addLight(Point3d position)
	{
		Light light = new Light(0);
		light.setPosition(new Coord3d(position.x, position.y, position.z));
		light.setAmbiantColor(Color.WHITE);
		light.setDiffuseColor(Color.CYAN);
		light.setSpecularColor(Color.CYAN);
		this.chart.getScene().add(light);
	}
	
	/**
	 * Adds the scatter.
	 * 
	 * @param pointList
	 *          the point list
	 * @param radius
	 *          the radius
	 * @param color
	 *          the color
	 */
	public void addScatter(ArrayList<Point3d> pointList, double radius, Color color)
	{
		Coord3d[] coordList = new Coord3d[pointList.size()];
		for (int index=0; index<coordList.length; index++)
			coordList[index] = new Coord3d(pointList.get(index).x, pointList.get(index).y, pointList.get(index).z);
		
		this.chart.getScene().add(new Scatter(coordList, color, (float)radius));
	}
	
	/**
	 * Adds the sphere.
	 * 
	 * @param p
	 *          the p
	 * @param radius
	 *          the radius
	 * @param color
	 *          the color
	 */
	public void addSphere(Point3d p, double radius, Color color)
	{
		// Define range and precision for the function to plot		
		EnlightableSphere s = new EnlightableSphere(new Coord3d(p.x, p.y, p.z), (float) radius, 10, color);
		// s.setWireframeColor(Color.WHITE);
		s.setWireframeDisplayed(false);
		s.setFaceDisplayed(true);
		s.setMaterialAmbiantReflection(Color.BLACK);
		this.chart.getScene().add(s);
		
		//return s;
	}

	/**
	 * Adds the surface.
	 * 
	 * @param surface
	 *          the surface
	 */
	public void addSurface(Surface surface)
	{
		this.chart.getScene().add(surface.getSurface());
	}
	
	public void addCylinder(Point3d position, double height, double radius, Color color)
	{
		Cylinder cyl = new Cylinder();
		cyl.setData(new Coord3d(position.x, position.y, position.z), (float)height,  (float)radius, 4, 4, color);
		
		cyl.setColor(color);
		cyl.setWireframeColor(color);
		cyl.setWireframeDisplayed(true);
		
		this.chart.getScene().add(cyl);
	}

	/**
	 * Adds the vector.
	 * 
	 * @param startPoint
	 *          the start point
	 * @param endPoint
	 *          the end point
	 * @param color
	 *          the color
	 */
	public void addVector(Point3d startPoint, Point3d endPoint, Color color)
	{
		double offset = .01;
		
		Polygon p = new Polygon();
		p.setWireframeWidth(.5f);
		p.add(new Point(new Coord3d(startPoint.x-offset, startPoint.y-offset, startPoint.z-offset)));
		p.add(new Point(new Coord3d(endPoint.x-offset, endPoint.y-offset, endPoint.z-offset)));
		p.add(new Point(new Coord3d(endPoint.x+offset, endPoint.y+offset, endPoint.z+offset)));
		p.add(new Point(new Coord3d(startPoint.x+offset, startPoint.y+offset, startPoint.z+offset)));
		//p.add(new Point(new Coord3d(endPoint.x-offset, endPoint.y-offset, endPoint.z+offset)));
		//p.add(new Point(new Coord3d(startPoint.x-offset, startPoint.y-offset, startPoint.z+offset)));
		//p.add(new Point(new Coord3d(startPoint.x-offset, startPoint.y+offset, startPoint.z+offset)));
		//p.add(new Point(new Coord3d(endPoint.x-offset, endPoint.y+offset, endPoint.z+offset)));
		
		p.setColor(color);
		p.setWireframeColor(color);
		p.setWireframeDisplayed(true);
		
		this.chart.getScene().add(p);
	}
	
	/**
	 * Gets the chart.
	 * 
	 * @return the chart
	 */
	private Chart getChart()
	{
		// Create a chart that updates the surface colormapper when scaling changes
		Chart chart = new Chart(Quality.Nicest, "swing");
		chart.getView().setBackgroundColor(Color.WHITE);
		chart.getView().getAxe().getLayout().setGridColor(Color.BLACK);
		chart.getView().getAxe().getLayout().setFaceDisplayed(false);
		//chart.getView().set

		return chart;
	}
	
	/**
	 * Sets the title.
	 * 
	 * @param title
	 *          the new title
	 */
	public void setTitle(String title)
	{
		//setTitle(title);
	}
	
	/**
	 * Sets the x label.
	 * 
	 * @param title
	 *          the new x label
	 */
	public void setXLabel(String title)
	{
		this.chart.getAxeLayout().setXAxeLabel(title);		
	}

	/**
	 * Sets the y label.
	 * 
	 * @param title
	 *          the new y label
	 */
	public void setYLabel(String title)
	{
		this.chart.getAxeLayout().setYAxeLabel(title);		
	}
	
	/**
	 * Sets the z label.
	 * 
	 * @param title
	 *          the new z label
	 */
	public void setZLabel(String title)
	{
		this.chart.getAxeLayout().setZAxeLabel(title);		
	}
}
