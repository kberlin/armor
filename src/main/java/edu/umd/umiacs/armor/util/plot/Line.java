/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util.plot;

import java.awt.Paint;
import java.awt.Shape;
import java.awt.Stroke;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import org.jfree.chart.labels.CustomXYToolTipGenerator;
import org.jfree.chart.renderer.xy.StackedXYBarRenderer;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.DefaultTableXYDataset;
import org.jfree.data.xy.XIntervalSeries;
import org.jfree.data.xy.XIntervalSeriesCollection;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.data.xy.YIntervalSeries;
import org.jfree.data.xy.YIntervalSeriesCollection;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.util.SortablePair;

/**
 * The Class Line.
 */
public class Line implements Serializable
{
	
	/** The draw line. */
	private boolean drawLine;

	/** The draw shape. */
	private boolean drawShape;
	
	/** The name. */
	private String name;
	
	/** The paint. */
	private Paint paint;
	
	/** The shape. */
	private Shape shape;
	
	/** The stroke. */
	private Stroke stroke;
	
	/** The tool tips. */
	private ArrayList<String> toolTips;
	
	/** The visible in legend. */
	private boolean visibleInLegend;
	
	/** The x data. */
	private double[] xData;
	
	/** The y data. */
	private double[] yData;
	
	/** The y data error. */
	private double[] yDataError;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7567638422760043004L;
	
	/**
	 * Instantiates a new line.
	 * 
	 * @param xData
	 *          the x data
	 * @param yData
	 *          the y data
	 * @param yDataError
	 *          the y data error
	 * @param name
	 *          the name
	 */
	public Line(double[] xData, double[] yData, double[] yDataError, String name)
	{
		this(xData, yData, yDataError, name, null);
	}

	/**
	 * Instantiates a new line.
	 * 
	 * @param xData
	 *          the x data
	 * @param yData
	 *          the y data
	 * @param yDataError
	 *          the y data error
	 * @param name
	 *          the name
	 * @param tooltips
	 *          the tooltips
	 */
	public Line(double[] xData, double[] yData, double[] yDataError, String name, ArrayList<String> tooltips)
	{
		//sort data
		ArrayList<SortablePair<Double, Integer>> sortIndex = new ArrayList<SortablePair<Double,Integer>>(xData.length);
		for (int index=0; index<xData.length; index++)
			sortIndex.add(new SortablePair<Double, Integer>(xData[index], index));
		Collections.sort(sortIndex);
		
		this.xData = new double[xData.length];
		this.yData = new double[yData.length];
		if (yDataError!=null)
			this.yDataError = new double[yDataError.length];
		else
			this.yDataError = null;
		if (tooltips!=null)
			this.toolTips = new ArrayList<String>(tooltips.size());
		else
			this.toolTips = null;
		
		for (int index=0; index<sortIndex.size(); index++)
		{
			int oldIndex = sortIndex.get(index).y;
			this.xData[index] = xData[oldIndex];
			this.yData[index] = yData[oldIndex];
			if (yDataError!=null)
				this.yDataError[index] = yDataError[oldIndex];
			if (tooltips!=null)
				this.toolTips.add(tooltips.get(oldIndex));
		}
		
		this.name = name;
		this.drawLine = true;
		this.drawShape = true;
		this.visibleInLegend = true;
		this.stroke = null;
		this.shape = null;
		this.paint = null;
	}
	
	/**
	 * Instantiates a new line.
	 * 
	 * @param xData
	 *          the x data
	 * @param yData
	 *          the y data
	 * @param name
	 *          the name
	 */
	public Line(double[] xData, double[] yData, String name)
	{
		this(xData, yData, null, name, null);
	}
	
	/**
	 * Instantiates a new line.
	 * 
	 * @param xData
	 *          the x data
	 * @param yData
	 *          the y data
	 * @param name
	 *          the name
	 * @param tooltips
	 *          the tooltips
	 */
	public Line(double[] xData, double[] yData, String name, ArrayList<String> tooltips)
	{
		this(xData, yData, null, name, tooltips);
	}
	
	/**
	 * Gets the name.
	 * 
	 * @return the name
	 */
	public final String getName()
	{
		return this.name;
	}
	
	/**
	 * Gets the paint.
	 * 
	 * @return the paint
	 */
	public final Paint getPaint()
	{
		return this.paint;
	}
	
	/**
	 * Gets the shape.
	 * 
	 * @return the shape
	 */
	public Shape getShape()
	{
		return this.shape;
	}
	
	/**
	 * Gets the x interval series collection.
	 * 
	 * @return the x interval series collection
	 */
	public XIntervalSeriesCollection getXIntervalSeriesCollection()
	{
		XIntervalSeries newSeries = new XIntervalSeries(this.name, false, true);
		
		for (int iter=0; iter<this.xData.length; iter++)
		{
			newSeries.add(this.xData[iter], this.xData[iter]-this.yDataError[iter], this.xData[iter]+this.yDataError[iter], this.yData[iter]);
		}
		
		XIntervalSeriesCollection collection = new XIntervalSeriesCollection();
		collection.addSeries(newSeries);
		
		return collection;
	}

	/**
	 * Gets the x limit.
	 * 
	 * @return the x limit
	 */
	public Limit getXLimit()
	{
		return new Limit(BasicMath.min(this.xData), BasicMath.max(this.xData));
	}

	/**
	 * Gets the xY bar renderer.
	 * 
	 * @return the xY bar renderer
	 */
	protected XYBarRenderer getXYStackBarRenderer()
	{
		XYBarRenderer lineRenderer = new StackedXYBarRenderer();
		lineRenderer.setSeriesPaint(0, getPaint());
		
		lineRenderer.setBarPainter(new StandardXYBarPainter());
		lineRenderer.setShadowVisible(false);
		lineRenderer.setDrawBarOutline(false);
		lineRenderer.setGradientPaintTransformer(null);
		lineRenderer.setSeriesVisibleInLegend(0, this.visibleInLegend);
		
		if (this.toolTips!=null)
		{
			CustomXYToolTipGenerator toolTip = new CustomXYToolTipGenerator();
			toolTip.addToolTipSeries(this.toolTips);
			lineRenderer.setBaseToolTipGenerator(toolTip);
		}

		return lineRenderer;
	}
	
	/**
	 * Gets the xY error renderer.
	 * 
	 * @return the xY error renderer
	 */
	protected XYErrorRenderer getXYErrorRenderer()
	{
		XYErrorRenderer lineRenderer = new XYErrorRenderer();
		lineRenderer.setSeriesPaint(0, getPaint());
		lineRenderer.setSeriesShape(0, getShape());
		lineRenderer.setBaseShapesVisible(isDrawShape());
		lineRenderer.setBaseLinesVisible(isDrawLine());
		lineRenderer.setSeriesVisibleInLegend(0, this.visibleInLegend);
		
		if (this.toolTips!=null)
		{
			CustomXYToolTipGenerator toolTip = new CustomXYToolTipGenerator();
			toolTip.addToolTipSeries(this.toolTips);
			lineRenderer.setBaseToolTipGenerator(toolTip);
		}
		
		if (this.stroke!=null)
		{
			lineRenderer.setSeriesStroke(0, this.stroke);
		}

		return lineRenderer;
	}
	
	/**
	 * Gets the xY line and shape renderer.
	 * 
	 * @return the xY line and shape renderer
	 */
	protected XYLineAndShapeRenderer getXYLineAndShapeRenderer()
	{
		XYLineAndShapeRenderer lineRenderer = new XYLineAndShapeRenderer(isDrawLine() , isDrawShape());
		lineRenderer.setSeriesPaint(0, getPaint());
		lineRenderer.setSeriesShape(0, getShape());
		lineRenderer.setSeriesVisibleInLegend(0, this.visibleInLegend);
		if (this.toolTips!=null)
		{
			CustomXYToolTipGenerator toolTip = new CustomXYToolTipGenerator();
			toolTip.addToolTipSeries(this.toolTips);
			lineRenderer.setBaseToolTipGenerator(toolTip);
		}

		if (this.stroke!=null)
		{
			lineRenderer.setSeriesStroke(0, this.stroke);
		}
		
		return lineRenderer;
	}
	
	/**
	 * Gets the xY series.
	 * 
	 * @return the xY series
	 */
	protected XYSeries getXYSeries()
	{
		return getXYSeries(true);
	}

	/**
	 * Gets the xY series.
	 * 
	 * @param allowRepeats
	 *          the allow repeats
	 * @return the xY series
	 */
	protected XYSeries getXYSeries(boolean allowRepeats)
	{
		XYSeries newSeries = new XYSeries(this.name, false, allowRepeats);
		
		for (int iter=0; iter<this.xData.length; iter++)
		{
			newSeries.add(this.xData[iter], this.yData[iter]);
		}
		
		return newSeries;
	}
	
	protected double[] getX()
	{
		return this.xData;
	}
	
	protected double[] getY()
	{
		return this.yData;
	}
	
	protected double[] getError()
	{
		return this.yDataError;
	}
	
	public double getX(int index)
	{
		return this.xData[index];
	}
	
	public double getY(int index)
	{
		return this.yData[index];
	}
	
	public double getError(int index)
	{
		return this.yDataError[index];
	}
	
	/**
	 * Gets the xY series collection.
	 * 
	 * @return the xY series collection
	 */
	protected XYSeriesCollection getXYSeriesCollection()
	{
		XYSeriesCollection collection = new XYSeriesCollection(getXYSeries());
		
		return collection;
	}
	
	protected XYSeriesCollection getXYSeriesCollection(boolean allowRepeats)
	{
		XYSeriesCollection collection = new XYSeriesCollection();
		collection.addSeries(getXYSeries(allowRepeats));
		
		return collection;
	}
	
	protected DefaultTableXYDataset getDefaultTableXYDataset()
	{
		DefaultTableXYDataset collection = new DefaultTableXYDataset();
		collection.addSeries(getXYSeries(false));
		
		return collection;
	}
	
	/**
	 * Gets the y interval series collection.
	 * 
	 * @return the y interval series collection
	 */
	public YIntervalSeriesCollection getYIntervalSeriesCollection()
	{
		YIntervalSeries newSeries = new YIntervalSeries(this.name, false, true);
		
		for (int iter=0; iter<this.xData.length; iter++)
		{
			newSeries.add(this.xData[iter], this.yData[iter], this.yData[iter]-this.yDataError[iter],this.yData[iter]+this.yDataError[iter]);
		}
		
		YIntervalSeriesCollection collection = new YIntervalSeriesCollection();
		collection.addSeries(newSeries);
		
		return collection;
	}
	
	/**
	 * Gets the y limit.
	 * 
	 * @return the y limit
	 */
	public Limit getYLimit()
	{
		return new Limit(BasicMath.min(this.xData), BasicMath.max(this.xData));
	}
	
	/**
	 * Checks if is draw line.
	 * 
	 * @return true, if is draw line
	 */
	public boolean isDrawLine()
	{
		return this.drawLine;
	}


	/**
	 * Checks if is draw shape.
	 * 
	 * @return true, if is draw shape
	 */
	public boolean isDrawShape()
	{
		return this.drawShape;
	}

	/**
	 * Sets the draw line.
	 * 
	 * @param drawLine
	 *          the new draw line
	 */
	public final void setDrawLine(boolean drawLine)
	{
		this.drawLine = drawLine;
	}

	/**
	 * Sets the draw shape.
	 * 
	 * @param drawShape
	 *          the new draw shape
	 */
	public final void setDrawShape(boolean drawShape)
	{
		this.drawShape = drawShape;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *          the new name
	 */
	public final void setName(String name)
	{
		this.name = name;
	}

	/**
	 * Sets the paint.
	 * 
	 * @param paint
	 *          the new paint
	 */
	public final void setPaint(Paint paint)
	{
		this.paint = paint;
	}

	/**
	 * Sets the shape.
	 * 
	 * @param shape
	 *          the new shape
	 */
	public final void setShape(Shape shape)
	{
		this.shape = shape;
	}

	/**
	 * Sets the stroke.
	 * 
	 * @param stroke
	 *          the new stroke
	 */
	public void setStroke(Stroke stroke)
	{
		this.stroke = stroke;
	}

	/**
	 * Sets the visible in legend.
	 * 
	 * @param visibleInLegend
	 *          the new visible in legend
	 */
	public final void setVisibleInLegend(boolean visibleInLegend)
	{
		this.visibleInLegend = visibleInLegend;
	}

}
