/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.JTextComponent;

/**
 * The Class FileEditor.
 */
public class FileEditor extends JFrame
{
	// A very simple exit action
	/**
	 * The Class ExitAction.
	 */
	public class ExitAction extends AbstractAction
	{	
		/**
		 * 
		 */
		private static final long serialVersionUID = 2835282800347208347L;

		/**
		 * Instantiates a new exit action.
		 */
		public ExitAction()
		{
			super("Exit");
		}

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent ev)
		{
			System.exit(0);
		}
	}

	// An action that opens an existing file
	/**
	 * The Class OpenAction.
	 */
	class OpenAction extends AbstractAction
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -6627626020337540398L;

		/**
		 * Instantiates a new open action.
		 */
		public OpenAction()
		{
			super("Open");
		}

		// Query user for a filename and attempt to open and read the file into
		// the
		// text component.
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent ev)
		{
			JFileChooser chooser = new JFileChooser();
			if (chooser.showOpenDialog(FileEditor.this) != JFileChooser.APPROVE_OPTION)
				return;
			File file = chooser.getSelectedFile();
			if (file == null)
				return;

			FileReader reader = null;
			try
			{
				reader = new FileReader(file);
				FileEditor.this.textComp.read(reader, null);
			}
			catch (IOException ex)
			{
				JOptionPane.showMessageDialog(FileEditor.this, "File Not Found", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
			finally
			{
				if (reader != null)
				{
					try
					{
						reader.close();
					}
					catch (IOException x)
					{
					}
				}
			}
		}
	}
	
	// An action that saves the document to a file
	/**
	 * The Class SaveAction.
	 */
	class SaveAction extends AbstractAction
	{
		
		/** The Constant serialVersionUID. */
		private static final long serialVersionUID = 1L;

		/**
		 * Instantiates a new save action.
		 */
		public SaveAction()
		{
			super("Save");
		}

		// Query user for a filename and attempt to open and write the text
		// component's content to the file.
		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		@Override
		public void actionPerformed(ActionEvent ev)
		{
			JFileChooser chooser = new JFileChooser();
			if (chooser.showSaveDialog(FileEditor.this) != JFileChooser.APPROVE_OPTION)
				return;
			File file = chooser.getSelectedFile();
			if (file == null)
				return;

			FileWriter writer = null;
			try
			{
				writer = new FileWriter(file);
				FileEditor.this.textComp.write(writer);
			}
			catch (IOException ex)
			{
				JOptionPane.showMessageDialog(FileEditor.this, "File Not Saved", "ERROR", JOptionPane.ERROR_MESSAGE);
			}
			finally
			{
				if (writer != null)
				{
					try
					{
						writer.close();
					}
					catch (IOException x)
					{
					}
				}
			}
		}
	}
	
	/** The open action. */
	private Action openAction = new OpenAction();

	/** The save action. */
	private Action saveAction = new SaveAction();

	/** The scroll pane. */
	private JScrollPane scrollPane;
	
	/** The text comp. */
	private JEditorPane textComp;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1873373065124372369L;
	
	// Create an editor.
	/**
	 * Instantiates a new file editor.
	 * 
	 * @param file
	 *          the file
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	public FileEditor(File file) throws IOException
	{
		this(BasicUtils.readFileToString(file));			
	}
	
	public FileEditor(InputStream in) throws IOException
	{
		this(BasicUtils.readStreamToString(in));			
	}

	/**
	 * Instantiates a new file editor.
	 * 
	 * @param string
	 *          the string
	 */
	public FileEditor(String string)
	{
		super("File Editor: ");

		this.textComp = new JEditorPane();
		this.textComp.setFont(new Font("Andale Mono", Font.PLAIN, 14));
		
		this.textComp.setText(string);
		
		Container content = getContentPane();
		
		this.scrollPane = new JScrollPane(this.textComp);
		
		//makeActionsPretty();

		content.add(this.scrollPane, BorderLayout.CENTER);
		//content.add(createToolBar(), BorderLayout.NORTH);
		setJMenuBar(createMenuBar());
		setBounds(100, 100, 780, 680);
		
		//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setVisible(true);
    
		this.scrollPane.getVerticalScrollBar().setValue(0);
		this.scrollPane.getHorizontalScrollBar().setValue(0);
		this.textComp.setCaretPosition(0);
	}

	// Create a JMenuBar with file & edit menus.
	/**
	 * Creates the menu bar.
	 * 
	 * @return the j menu bar
	 */
	private JMenuBar createMenuBar()
	{
		JMenuBar menubar = new JMenuBar();
		JMenu file = new JMenu("File");
		JMenu edit = new JMenu("Edit");
		menubar.add(file);
		menubar.add(edit);

		file.add(getOpenAction());
		file.add(getSaveAction());
		file.add(new ExitAction());
		edit.add(this.textComp.getActionMap().get(DefaultEditorKit.cutAction));
		edit.add(this.textComp.getActionMap().get(DefaultEditorKit.copyAction));
		edit.add(this.textComp.getActionMap().get(DefaultEditorKit.pasteAction));
		edit.add(this.textComp.getActionMap().get(DefaultEditorKit.selectAllAction));
		return menubar;
	}

	// Create a simple JToolBar with some buttons.
	/**
	 * Creates the tool bar.
	 * 
	 * @return the j tool bar
	 */
	protected JToolBar createToolBar()
	{
		JToolBar bar = new JToolBar();

		// Add simple actions for opening & saving.
		bar.add(getOpenAction()).setText("");
		bar.add(getSaveAction()).setText("");
		bar.addSeparator();

		// Add cut/copy/paste buttons.
		bar.add(this.textComp.getActionMap().get(DefaultEditorKit.cutAction)).setText("");
		bar.add(this.textComp.getActionMap().get(DefaultEditorKit.copyAction)).setText("");
		bar.add(this.textComp.getActionMap().get(DefaultEditorKit.pasteAction)).setText("");
		return bar;
	}

	// Subclass can override to use a different open action.
	/**
	 * Gets the open action.
	 * 
	 * @return the open action
	 */
	protected Action getOpenAction()
	{
		return this.openAction;
	}

	// Subclass can override to use a different save action.
	/**
	 * Gets the save action.
	 * 
	 * @return the save action
	 */
	protected Action getSaveAction()
	{
		return this.saveAction;
	}

	/**
	 * Gets the text component.
	 * 
	 * @return the text component
	 */
	protected JTextComponent getTextComponent()
	{
		return this.textComp;
	}

	// ********** ACTION INNER CLASSES ********** //

	// Add icons and friendly names to actions we care about.
	/**
	 * Make actions pretty.
	 */
	protected void makeActionsPretty()
	{
		Action a;
		a = this.textComp.getActionMap().get(DefaultEditorKit.cutAction);
		a.putValue(Action.NAME, "Cut");

		a = this.textComp.getActionMap().get(DefaultEditorKit.copyAction);
		a.putValue(Action.NAME, "Copy");

		a = this.textComp.getActionMap().get(DefaultEditorKit.pasteAction);
		a.putValue(Action.NAME, "Paste");

		a = this.textComp.getActionMap().get(DefaultEditorKit.selectAllAction);
		a.putValue(Action.NAME, "Select All");
	}

	/**
	 * Sets the editable.
	 * 
	 * @param value
	 *          the new editable
	 */
	public void setEditable(boolean value)
	{
		this.textComp.setEditable(value);
	}

	/**
	 * Sets the main text.
	 * 
	 * @param string
	 *          the new main text
	 */
	public void setMainText(String string)
	{
		this.textComp.setText(string);		
		this.scrollPane.getVerticalScrollBar().setValue(0);
	}
}
