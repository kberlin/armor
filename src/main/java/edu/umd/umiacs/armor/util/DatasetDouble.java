/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import edu.umd.umiacs.armor.math.MathRuntimeException;

/**
 * The Class DatasetDouble.
 */
public class DatasetDouble extends DatasetXDoubleImpl<Double>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7075800944860297247L;
	
	public DatasetDouble()
	{
		super();
	}
	
	public DatasetDouble(DatasetDouble prev)
	{
		super(prev, false);
	}
	
	public DatasetDouble(DatasetXDoubleImpl<Double> prev, boolean byReference)
	{
		super(prev, byReference);
	}
	
	public DatasetDouble(double[] xArray, double[] yArray)
	{
		this();
		
		if (xArray.length != yArray.length)
			throw new MathRuntimeException("Array sizes must be equal.");
		
		for (int index=0; index<xArray.length; index++)
		{
			add(xArray[index], yArray[index]);
		}
	}

	public DatasetDouble createNonZero()
	{
		DatasetDouble newSet = new DatasetDouble();
		for (int iter=0; iter<size(); iter++)
		{
			if (Math.abs(getY(iter))>1.0e-16)
			{
				newSet.add(getValue(iter));					
			}
		}
		
		return newSet;
	}
	
	public double[] getYArray()
	{
		double[] val = new double[size()];
		
		int count = 0;
		for (SortablePair<Double, Double> pair : this)
		{
			val[count] = pair.y;
			count++;
		}
		
		return val;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.DatasetXDoubleImpl#sort()
	 */
	@Override
	public DatasetDouble sort()
	{
		return new DatasetDouble(super.sort(), true);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.DatasetXDoubleImpl#sort(edu.umd.umiacs.armor.util.SortType)
	 */
	@Override
	public DatasetDouble sort(SortType type)
	{
		return new DatasetDouble(super.sort(type), true);
	}

	public double[][] toArray()
	{
		double[][] a = new double[size()][2];
		
		int counter = 0;
		for (SortablePair<Double,Double> pair : this)
		{
			a[counter][0] = pair.x;
			a[counter][1] = pair.y;
			
			counter++;
		}
		
		return a;
	}
}
