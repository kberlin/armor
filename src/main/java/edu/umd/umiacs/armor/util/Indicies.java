/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import java.util.Arrays;


public final class Indicies
{
	private final int[] indicies;
	private final int hashCode;
	
	public Indicies(int[] indicies, boolean copy)
	{
		//copy by reference
		if (copy)
			this.indicies = indicies.clone();
		else
			this.indicies = indicies;
		
		//sort the results
		if (this.indicies!=null)
			Arrays.sort(this.indicies);
		
		this.hashCode = hashCodeHelper(this.indicies);
	}
	
	public Indicies(int[] indicies, int newIndex)
	{
		//combine the new column
		if (indicies!=null)
			this.indicies = Arrays.copyOf(indicies, indicies.length+1);
		else
			this.indicies = new int[1];
		
		//add the new addition element 
		this.indicies[this.indicies.length-1] = newIndex;
		
		//update the lookup info
		Arrays.sort(this.indicies);
		this.hashCode = hashCodeHelper(this.indicies);
	}
	
	public int[] getIndiciesRef()
	{
		return this.indicies;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		Indicies other = (Indicies) obj;
		
		if (this.hashCode!=other.hashCode)
			return false;
		
		if (this.indicies==null && other.indicies!=null)
			return false;

		if (other.indicies==null && this.indicies!=null)
			return false;

		if (this.indicies.length!=other.indicies.length)
			return false;
		
		//the columns are always sorted
		for (int iter=0; iter<this.indicies.length; iter++)
			if (this.indicies[iter] != other.indicies[iter])
				return false;
			
		return true;
	}
	
	public Indicies createWithNewIndex(int column)
	{
		return new Indicies(this.indicies, column);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return this.hashCode;
	}

	private int hashCodeHelper(int[] columns)
	{
    int result = HashCodeUtil.SEED;
    
    if (columns!=null)
	    for (int index : columns)
		    result = HashCodeUtil.hash(result, index);

    return result;		
  }

	public boolean isEmpty()
	{
		return this.indicies==null;
	}

	public boolean contains(int column)
	{
		if (this.indicies==null)
			return false;
		
		return Arrays.binarySearch(this.indicies, column)>=0;
	}
}