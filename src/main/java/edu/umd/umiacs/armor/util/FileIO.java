/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.Scanner;

/**
 * The Class FileIO.
 */
public class FileIO
{
  
  /**
	 * Read.
	 * 
	 * @param file
	 *          the file
	 * @return the string
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
  public static String read(File file) throws IOException 
  {
    StringBuilder text = new StringBuilder();
    String NL = System.getProperty("line.separator");
    Scanner scanner = new Scanner(new FileInputStream(file));
    try 
    {
      while (scanner.hasNextLine())
      {
        text.append(scanner.nextLine() + NL);
      }
    }
    
    finally
    {
      scanner.close();
    }
    
    return text.toString();
  }
  
  public static File ensureExtension(File file, String addition, String defaultExtension)
  {				  	
  	if (file==null)
  		return null;
  	
  	//extract the name
  	String[] fileStringArray = file.getName().split("\\.");
  	String fullFileName = file.getAbsolutePath();

  	//get the name of the file without the extension
  	
  	if (fileStringArray.length>1)
  	{
  		defaultExtension = fileStringArray[fileStringArray.length-1];
  		fullFileName = removeExtension(fullFileName);
  	}
  	
  	//add the addition string
  	if (addition!=null)
  		fullFileName = fullFileName.concat(addition);

		//add back the extension
		File newFile = new File(fullFileName+"."+defaultExtension);
		
		return newFile;
  }
  
  public static String removeExtension(String name)
  {
  	String[] fileStringArray = name.split("\\.");

  	//get the name of the file without the extension
  	if (fileStringArray.length>1)
  	{
  		name = name.substring(0, name.length()-fileStringArray[fileStringArray.length-1].length()-1);
  	}
  	
  	return name;
  }
  
  /**
	 * Write.
	 * 
	 * @param file
	 *          the file
	 * @param text
	 *          the text
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
  public static void write(File file, String text) throws IOException  
  {
    Writer out = new OutputStreamWriter(new FileOutputStream(file));
    try 
    {
      out.write(text);
    }
    finally 
    {
      out.close();
    }
  }
  
  public static Object readObject(File file) throws IOException, ClassNotFoundException
  {
		ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));		
		Object sol = in.readObject();

		in.close();
		
		return sol;
  }
  
  public static void writeObject(File file, Serializable obj) throws FileNotFoundException, IOException
  {
		ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
		out.writeObject(obj);
		out.close();
  }
}
