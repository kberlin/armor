/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util.plot;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.HashMap;
import java.util.List;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogarithmicAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.DefaultTableXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import edu.umd.umiacs.armor.util.BasicUtils;

/**
 * The Class Plot2D.
 */
public class Plot2d extends JPanel
{	
	/** The chart. */
	private JFreeChart chart;
	/**
	 * 
	 */
	private static final long serialVersionUID = 6417094471617136091L;

	/**
	 * Instantiates a new plot2 d.
	 */
	public Plot2d()
	{
		setLayout(new BorderLayout(0, 0));
		
		XYSeriesCollection collection = new XYSeriesCollection();
		
		this.chart =  ChartFactory.createXYLineChart("", "X", "Y", collection,
				PlotOrientation.VERTICAL, true, true, false);
		ChartPanel chartpanel = new ChartPanel(this.chart);
		chartpanel.setMouseWheelEnabled(true);
		
		XYPlot xyplot = this.chart.getXYPlot();
		xyplot.setBackgroundAlpha(0.0f);
		xyplot.setDomainCrosshairVisible(true);
		xyplot.setRangeCrosshairVisible(true);
		xyplot.setDomainPannable(true);
		xyplot.setRangePannable(true);
		xyplot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		NumberAxis numberaxis = (NumberAxis) xyplot.getRangeAxis();
		numberaxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());
		
		this.add(chartpanel, BorderLayout.CENTER);
	}
	
	public void setRenderingOrderReverse()
	{
		this.chart.getXYPlot().setDatasetRenderingOrder(DatasetRenderingOrder.REVERSE);
	}
	
	/**
	 * Adds the error line.
	 * 
	 * @param line
	 *          the line
	 */
	public void addErrorLine(Line line)
	{
		int index = this.chart.getXYPlot().getDatasetCount();
		
		this.chart.getXYPlot().setDataset(index, line.getYIntervalSeriesCollection());
		this.chart.getXYPlot().setRenderer(index, line.getXYErrorRenderer());
	}
	
	public void addBar(Line line)
	{
		int index = this.chart.getXYPlot().getDatasetCount();
		
		this.chart.getXYPlot().setDataset(index, line.getDefaultTableXYDataset());
		this.chart.getXYPlot().setRenderer(index, line.getXYStackBarRenderer());		
	}
	
	public void addStackedBar(List<Line> lines)
	{
		int index = this.chart.getXYPlot().getDatasetCount();
		
		//generate a dataset
		HashMap<Double, double[]> values = new HashMap<Double, double[]>();
		int count = 0;
		for (Line line : lines)
		{
			double[] x = line.getX();
			double[] y = line.getY();
			for (int iter=0; iter<x.length; iter++)
			{
				double[] list = values.get(x[iter]);
				if (list==null)
				{
					list = BasicUtils.createArray(lines.size(), 0.0);
				  values.put(x[iter], list);
				}
				
				list[count] = y[iter];
			}
			
			count++;
		}
		
		//generate the collection
		DefaultTableXYDataset collection = new DefaultTableXYDataset();		
		for (int iter=0; iter<lines.size(); iter++)
		{
			XYSeries newSeries = new XYSeries(lines.get(iter).getName(), true, false);
			for (double x : values.keySet())
			{
				newSeries.add(x, values.get(x)[iter]);
			}
			
			collection.addSeries(newSeries);
		}
		
		this.chart.getXYPlot().setDataset(index, collection);
		this.chart.getXYPlot().setRenderer(index, lines.get(0).getXYStackBarRenderer());		
	}
	
	public void addLine(Line line)
	{
		int index = this.chart.getXYPlot().getDatasetCount();
		
		this.chart.getXYPlot().setDataset(index, line.getXYSeriesCollection());
		this.chart.getXYPlot().setRenderer(index, line.getXYLineAndShapeRenderer());		
	}

	public void setIntegerTicks(boolean value)
	{
		NumberAxis numberaxis = (NumberAxis)this.chart.getXYPlot().getDomainAxis();
		
		if (value)
			numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		else
			numberaxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());
	}
	
	public void addXErrorLine(Line line)
	{
		int index = this.chart.getXYPlot().getDatasetCount();
		
		this.chart.getXYPlot().setDataset(index, line.getXIntervalSeriesCollection());
		this.chart.getXYPlot().setRenderer(index, line.getXYErrorRenderer());
	}
	
	/**
	 * Removes the legend.
	 */
	public void removeLegend()
	{
		this.chart.removeLegend();
	}
	
	/**
	 * Sets the title.
	 * 
	 * @param title
	 *          the new title
	 */
	public void setTitle(String title)
	{
		this.chart.setTitle(title);
	}
	
	/**
	 * Sets the title.
	 * 
	 * @param title
	 *          the title
	 * @param font
	 *          the font
	 */
	public void setTitle(String title, Font font)
	{
		this.chart.setTitle(new TextTitle(title, font));
	}
	
	/**
	 * Sets the x axis limit.
	 * 
	 * @param limit
	 *          the new x axis limit
	 */
	public void setXAxisLimit(Limit limit)
	{
		this.chart.getXYPlot().getDomainAxis().setRange(limit.getMin(), limit.getMax());
	}
	
	/**
	 * Sets the x axis ticks integer.
	 */
	public void setXAxisTicksInteger()
	{
		NumberAxis numberaxis = (NumberAxis)this.chart.getXYPlot().getDomainAxis();
		numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

	}
	
	/**
	 * Sets the x label.
	 * 
	 * @param label
	 *          the new x label
	 */
	public void setXLabel(String label)
	{
		this.chart.getXYPlot().getDomainAxis().setLabel(label);
	}

	/**
	 * Sets the y axis limit.
	 * 
	 * @param limit
	 *          the new y axis limit
	 */
	public void setYAxisLimit(Limit limit)
	{
		this.chart.getXYPlot().getRangeAxis().setRange(limit.getMin(), limit.getMax());
	}
	
	/**
	 * Sets the y label.
	 * 
	 * @param label
	 *          the new y label
	 */
	public void setYLabel(String label)
	{
		this.chart.getXYPlot().getRangeAxis().setLabel(label);
	}
	
	/**
	 * Sets the y log scale.
	 */
	public void setYLogScale()
	{
		NumberAxis logaxis = new LogarithmicAxis(this.chart.getXYPlot().getRangeAxis().getLabel());
		this.chart.getXYPlot().setRangeAxis(logaxis);
	}
	
	public void alignX(Plot2d toPlot)
	{
		this.chart.getXYPlot().setAxisOffset(toPlot.chart.getXYPlot().getAxisOffset());
	}
}
