/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractExperimentalData<E extends ExperimentalDatum> implements ExperimentalData<E>
{
	private List<E> data;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3346682783187924886L;
	
	protected AbstractExperimentalData()
	{
		this.data = new ArrayList<E>();
	}
	
	public AbstractExperimentalData(List<E> data)
	{
		this(data, false);
	}

	protected AbstractExperimentalData(List<E> data, boolean copy)
	{
		if (copy)
			this.data = new ArrayList<E>(data);
		else
			this.data = data;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalData#getRelativeErrors()
	 */
	@Override
	public double[] absErrorsByStd(double[] values)
	{
		double[] val = new double[size()];
		
		int counter = 0;
		for (E e : this)
		{
			val[counter] = e.absErrorByStd(values[counter]);
			counter++;
		}
		
		return val;
	}
	
	protected void add(E e)
	{
		this.data.add(e);
	}
	
	@Override
	public double[] errors()
	{
		double[] val = new double[size()];
		
		int counter = 0;
		for (E e : this)
		{
			val[counter] = e.error();
			counter++;
		}
		
		return val;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalData#get(int)
	 */
	@Override
	public E get(int index)
	{
		return this.data.get(index);
	}

	protected List<E> getDataRef()
	{
		return this.data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<E> iterator()
	{
		return this.data.iterator();
	}
	
	@Override
	public double[] relativeValues()
	{
		double[] val = new double[size()];
		
		int counter = 0;
		for (E e : this)
		{
			val[counter] = e.getRelativeValue();
			counter++;
		}
		
		return val;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalData#size()
	 */
	@Override
	public int size()
	{
		return this.data.size();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		
		for (int iter=0; iter<size(); iter++)     
			str.append(get(iter)+"\n");

		return str.toString();
	}

	@Override
	public double[] values()
	{
		double[] val = new double[size()];
		
		int counter = 0;
		for (E e : this)
		{
			val[counter] = e.value();
			counter++;
		}
		
		return val;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalData#valuesAndErrors()
	 */
	@Override
	public double[][] valuesAndErrors()
	{
		double[] values = values();
		double[] errors = errors();
		
		double[][] result = new double[values.length][2];
		
		for (int iter=0; iter<values.length; iter++)
		{
			result[iter][0] = values[iter];
			result[iter][1] = errors[iter];
		}	
		
		return result;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalData#getWeights()
	 */
	@Override
	public double[] weights()
	{
		double[] val = new double[size()];
		
		int counter = 0;
		for (E e : this)
		{
			double err = e.error();
			val[counter] = 1.0/(err*err);
			counter++;
		}
		
		return val;
	}
}
