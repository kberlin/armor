/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.util;

import edu.umd.umiacs.armor.math.BasicMath;

public class BaseExperimentalDatum extends DoublePair implements ExperimentalDatum
{
  /**
	 * 
	 */
	private static final long serialVersionUID = 8971767336131867385L;
	
	public final static boolean isValue(double value)
	{
		return !Double.isNaN(value);
	}
  
  public BaseExperimentalDatum(double value)
  {
  	this(value, Double.NaN);
  }
  
  public BaseExperimentalDatum(double value, double error)
	{
		super(value, error);

		if (error<=0.0)
			throw new ArithmeticException("Error cannot be smaller or equal to 0.");
 	}
  
  public BaseExperimentalDatum(ExperimentalDatum datum)
  {
  	super(datum.value(), datum.error());
  }
  
  public BaseExperimentalDatum add(ExperimentalDatum value)
	{
		return new BaseExperimentalDatum(this.x+value.value(), BasicMath.sqrt(this.y*this.y+value.error()*value.error()));
	}
  
  public BaseExperimentalDatum add(double value)
	{
		return new BaseExperimentalDatum(this.x+value, this.y);
	}
  
  public BaseExperimentalDatum divide(ExperimentalDatum value)
	{
		return new BaseExperimentalDatum(this.x/value.value(), BasicMath.sqrt(BasicMath.square(this.y/this.x))+BasicMath.square(value.error()/value.value()));
	}
  
  public BaseExperimentalDatum divide(double value)
	{
		return new BaseExperimentalDatum(this.x/value, this.y/value);
	}
  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalDatumInterface#getError()
	 */
  @Override
	public final double error()
  {
  	return this.y;
  }
  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalDatumInterface#getRelativeValue()
	 */
  @Override
	public final double getRelativeValue()
  {
  	if (hasError())
  		return this.x/this.y;  	
  	else
  		return this.x;
  }
  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalDatumInterface#getValue()
	 */
  @Override
	public final double value()
  {
  	return this.x;
  }
  
	@Override
	public boolean hasError()
  {
  	return isValue(this.y);
  }
  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalDatumInterface#hasValue()
	 */
  @Override
	public final boolean hasValue()
  {
  	return isValue(this.x);
  }

  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalDatumInterface#isInfinite()
	 */
  @Override
	public boolean isInfinite()
  {
  	if (Double.isInfinite(this.x) || Double.isInfinite(this.y))
  		return true;
  	
  	return false;
  }

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalDatumInterface#isNaN()
	 */
  @Override
	public boolean isNaN()
  {
  	if (Double.isNaN(this.x) || Double.isNaN(this.y))
  		return true;
  	
  	return false;
  }

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.util.ExperimentalDatumInterface#isNaNOrInfinite()
	 */
  @Override
	public boolean isNaNOrInfinite()
  {
  	return isNaN() || isInfinite();
  }
	
	public BaseExperimentalDatum mult(double value)
	{
		return new BaseExperimentalDatum(this.x*value, this.y*value);
	}
	
 	@Override
	public double absErrorByStd(double value)
	{
		if (hasError())
			return (value-this.x)/this.y;		
		else
			return value-this.x;
	}

	public BaseExperimentalDatum subtract(ExperimentalDatum value)
	{
		return new BaseExperimentalDatum(this.x-value.value(), BasicMath.sqrt(this.y*this.y+value.error()*value.error()));
	}
	
	public BaseExperimentalDatum subtract(double value)
	{
		return new BaseExperimentalDatum(this.x-value, this.y);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return new String("[Value: "+this.x+", Error: "+this.y+"]");
	}

	@Override
	public String valueInfoString()
	{
		return "";
	}
}
