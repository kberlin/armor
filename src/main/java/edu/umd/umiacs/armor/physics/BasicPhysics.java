/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.physics;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3dImpl;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.AtomType;
import edu.umd.umiacs.armor.molecule.Molecule;

/**
 * The Class BasicProperties.
 */
public final class BasicPhysics
{
	/** Planck's constant m^2*kg/s */
	public static final double h = 6.62606896e-34;

	/** The Constant hbar. */
	public static final double hbar = h/BasicMath.TWOPI;

	/** Boltzmann constant J*K^-1 **/
	public static final double kB = 1.380648813e-23;
	
	/** Vacuum permeability Vs/(Am) */
	public static final double mu0 = 4.0*BasicMath.PI*1.0e-7;
	
	/** Bohr magneton J*T^-1 **/
	public static final double muB = 9.27400968e-24;
	
	//frequency in Mhz, gamma in SI 
	/**
	 * Larmor frequency.
	 * 
	 * @param frequency
	 *          the frequency
	 * @param gamma
	 *          the gamma
	 * @return the double
	 */
	public final static double larmorFrequency(double frequency, double gamma)
	{
		//rad/s
		frequency = frequency*1.0e6*BasicMath.TWOPI;
		
	  //hydrogen gyromagnetic ratio  
	  final double gH = AtomType.HYDROGEN.getGyromagneticRatio(); //rad/s/Tesla

	  //figure out the magnetic field strength
	  double B0 = frequency/gH;
	  
	  //compute omega frequencies
	  double Wp = gamma*B0;
	  
	  return Wp;
	}
	
	/**
	 * Molecule i center of mass.
	 * 
	 * @param molecule
	 *          the molecule
	 * @return the point3d
	 */
	public static Point3d moleculeCenterOfMass(Molecule molecule)
	{
		//compute center of mass
		double c0 = 0.0;
		double c1 = 0.0;
		double c2 = 0.0;
		
		double totalMass = 0.0;
		for (Atom a : molecule)
		{
			Point3d pos = a.getPosition();
			AtomType type = a.getType();
			
			totalMass += type.getAtomicMass();
			c0 += type.getAtomicMass()*pos.x;
			c1 += type.getAtomicMass()*pos.y;
			c2 += type.getAtomicMass()*pos.z;
		}
		c0 = c0/totalMass;
		c1 = c1/totalMass;
		c2 = c2/totalMass;
		
		return new Point3d(c0, c1, c2);
	}

	/**
	 * Molecule inertia tensor.
	 * 
	 * @param molecule
	 *          the molecule
	 * @return the tensor3d
	 */
	public static Tensor3d moleculeInertiaTensor(Molecule molecule)
	{
		double[][] I = new double[3][3];

		I[0][0] = 0.0;
		I[1][1] = 0.0;
		I[2][2] = 0.0;
		I[0][1] = 0.0;
		I[0][2] = 0.0;
		I[1][2] = 0.0;
		
		//compute center of mass
		Point3d c = moleculeCenterOfMass(molecule);
		
		//compute tensor
		for (Atom a : molecule)
		{
			Point3d pos = a.getPosition().subtract(c);
			AtomType type = a.getType();
			
			I[0][0] += type.getAtomicMass()*(pos.y*pos.y+pos.z*pos.z);
			I[1][1] += type.getAtomicMass()*(pos.x*pos.x+pos.z*pos.z);
			I[2][2] += type.getAtomicMass()*(pos.x*pos.x+pos.y*pos.y);
			I[0][1] += -type.getAtomicMass()*(pos.x*pos.y);
			I[0][2] += -type.getAtomicMass()*(pos.x*pos.z);
			I[1][2] += -type.getAtomicMass()*(pos.y*pos.z);
		}
		
		I[1][0] = I[0][1];
		I[2][0] = I[0][2];
		I[2][1] = I[1][2];
		
		return new Tensor3d(new SymmetricMatrix3dImpl(I, false));
	}
	
	public static double radiusOfGyration(Molecule molecule)
	{
		double rg = 0.0;
		
		//compute center of mass
		Point3d c = moleculeCenterOfMass(molecule);

		//compute tensor
		double totalMass = 0.0;
		for (Atom a  : molecule)
		{
			rg += a.getType().getAtomicMass()*a.getPosition().distanceSquared(c);
			totalMass += a.getType().getAtomicMass();
		}
		rg = Math.sqrt(rg/totalMass);
		
		return rg;
	}

}
