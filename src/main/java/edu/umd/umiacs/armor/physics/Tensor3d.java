/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.physics;

import java.util.Collection;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3d;
import edu.umd.umiacs.armor.math.linear.SymmetricMatrix3dImpl;
import edu.umd.umiacs.armor.util.SortType;

/**
 * The Class Tensor3d.
 */
public class Tensor3d extends SymmetricMatrix3dImpl
{
	private EigenDecomposition3d properDecomposition;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6841310269725788310L;

	public Tensor3d(double x, double y, double z)
	{
		super(x, y, z, Rotation.identityRotation());
		this.properDecomposition = null;
	}

	public Tensor3d(double x, double y, double z, Rotation R)
	{
		super(new EigenDecomposition3d(x, y, z, R));
		this.properDecomposition = null;
	}
	
	public Tensor3d(SymmetricMatrix3d A)
	{
		super(A);
		this.properDecomposition = null;
	}
	
	public Tensor3d(RealMatrix A)
	{
		super(A);
		this.properDecomposition = null;
	}
	
	public double[][] computeTensorStd(Collection<? extends Tensor3d> tensorList)
	{
		double[][] tensorStd = new double[3][3];
		double[][] myTensor = this.toArray();
		
		if (tensorList==null || tensorList.isEmpty())
		{
			for (int iterRow=0; iterRow<tensorStd.length; iterRow++)
				for (int iterColumn=0; iterColumn<tensorStd.length; iterColumn++)
					tensorStd[iterRow][iterColumn] = Double.NaN;
			
			return tensorStd;
		}

		for (int iterRow=0; iterRow<tensorStd.length; iterRow++)
			for (int iterColumn=0; iterColumn<tensorStd.length; iterColumn++)
				tensorStd[iterRow][iterColumn] = 0.0;
		
		for (Tensor3d tensor : tensorList)
		{
			double[][] currTensor = tensor.toArray();
			
			for (int iterRow=0; iterRow<tensorStd.length; iterRow++)
				for (int iterColumn=0; iterColumn<tensorStd.length; iterColumn++)
					tensorStd[iterRow][iterColumn] += BasicMath.square(myTensor[iterRow][iterColumn]-currTensor[iterRow][iterColumn]);
		}

		for (int iterRow=0; iterRow<tensorStd.length; iterRow++)
			for (int iterColumn=0; iterColumn<tensorStd.length; iterColumn++)
				tensorStd[iterRow][iterColumn] = BasicMath.sqrt(tensorStd[iterRow][iterColumn]/(tensorList.size()-1));
		
		return tensorStd;
	}

	public synchronized EigenDecomposition3d getProperEigenDecomposition()
	{
		if (this.properDecomposition == null)
			this.properDecomposition = getEigenDecomposition().createSortedEigenDecomposition(SortType.ASCENDING);			

		return this.properDecomposition;
	}
}
