/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;

/**
 * The Class VirtualMoleculeWrapper.
 * 
 * @param <Molecule>
 *          the generic type
 */
public final class VirtualMoleculeWrapper extends AbstractMolecule
{
	private class VirtualIterator implements Iterator<Atom>
	{
		private final Iterator<Atom> molIter;
		private final RigidTransform t;
		
		public VirtualIterator(Molecule mol, RigidTransform t)
		{
			this.molIter = mol.iterator();
			this.t = t;
		}
		
		@Override
		public boolean hasNext()
		{
			return this.molIter.hasNext();
		}

		@Override
		public Atom next()
		{
			Atom a= this.molIter.next();
						
			return a.createWithNewPosition(this.t.transform(a.getPosition()));
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
		
	}
	
	/** The molecule. */
	private Molecule molecule;

	/** The t. */
	private RigidTransform t;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9170280593097598219L;
	
	/**
	 * Instantiates a new virtual molecule wrapper.
	 * 
	 * @param molecule
	 *          the molecule
	 */
	public VirtualMoleculeWrapper(Molecule molecule)
	{
		this(molecule, RigidTransform.identityTransform());
	}
	
	/**
	 * Instantiates a new virtual molecule wrapper.
	 * 
	 * @param molecule
	 *          the molecule
	 * @param t
	 *          the t
	 */
	public VirtualMoleculeWrapper(Molecule molecule, RigidTransform t)
	{
		if (molecule==null)
			throw new MoleculeRuntimeException("Molecule parameter cannot be null.");
		
		//prevent nesting
		if (molecule instanceof VirtualMoleculeWrapper)
		{
			VirtualMoleculeWrapper castMolecule = (VirtualMoleculeWrapper) molecule;
			this.molecule = castMolecule.molecule;
			this.t = castMolecule.t.union(t);
		}
		else
		{
			this.molecule = molecule;
			this.t = t;
		}
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#center()
	 */
	@Override
	public Point3d center()
	{
		return this.t.transform(this.molecule.center());
	}
 
	/**
	 * Creates the transformed molecule.
	 * 
	 * @param t
	 *          the t
	 * @return the virtual molecule wrapper
	 */
	public VirtualMoleculeWrapper createTransformedMolecule(RigidTransform t)
	{
		return new VirtualMoleculeWrapper(this.molecule, this.t.union(t));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#deuterate(double)
	 */
	@Override
	public VirtualMoleculeWrapper deuterate(double deuteriumFraction)
	{
		return new VirtualMoleculeWrapper(this.molecule.deuterate(deuteriumFraction),this.t);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#distanceToCore(int)
	 */
	@Override
	public double distanceToCore(int index)
	{
		return this.molecule.distanceToCore(index);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#distanceToCore(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public double distanceToCore(Point3d p)
	{
		return this.molecule.distanceToCore(this.t.inverseTransform(p));
	}
	
	

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#exists(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public boolean exists(Atom a)
	{
		return this.molecule.exists(a);
	}
	
	@Override
	public Atom getAtom(int index)
	{
		Atom a= this.molecule.getAtom(index);
		return a.createWithNewPosition(this.t.transform(a.getPosition()));
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public Point3d getAtomPosition(Atom a) throws AtomNotFoundException
	{
		return this.t.transform(this.molecule.getAtomPosition(a));
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(int)
	 */
	@Override
	public Point3d getAtomPosition(int index)
	{
		return this.t.transform(this.molecule.getAtomPosition(index));
	}

	@Override
	public ArrayList<Atom> getAtoms()
	{
		ArrayList<Atom> atomList = new ArrayList<Atom>(this.molecule.size());
		
		for (int iter=0; iter<size(); iter++)
			atomList.add(getAtom(iter));
		
		return atomList;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomSurfaceRadius(int)
	 */
	@Override
	public double getAtomSurfaceRadius(int index)
	{
		return this.molecule.getAtomSurfaceRadius(index);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomType(int)
	 */
	@Override
	public AtomType getAtomType(int index)
	{
		return this.molecule.getAtomType(index);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getBounds()
	 */
	@Override
	public RectangleBounds getBounds()
	{
		return transformBounds(this.molecule.getBounds());
	}

	@Override
	public ArrayList<Atom> getCloseAtoms(Point3d p, double maxDistance)
	{
		List<? extends Atom> list = this.molecule.getCloseAtoms(this.t.inverseTransform(p), maxDistance);
		
		//update the position of the atoms
		ArrayList<Atom> updatedList = new ArrayList<Atom>(list.size());
		for (Atom a : list)
			updatedList.add(a.createWithNewPosition(this.t.transform(p)));
		
		return updatedList;
	}

	@Override
	public ArrayList<Atom> getClosestAtoms(Point3d p, int n)
	{
		List<? extends Atom> list = this.molecule.getClosestAtoms(this.t.inverseTransform(p), n);
		
		//update the position of the atoms
		ArrayList<Atom> updatedList = new ArrayList<Atom>(list.size());
		for (Atom a : list)
			updatedList.add(a.createWithNewPosition(this.t.transform(p)));
		
		return updatedList;
	}

	/**
	 * Gets the molecule.
	 * 
	 * @return the molecule
	 */
	public Molecule getMolecule()
	{
		return this.molecule;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getSurfaceBounds()
	 */
	@Override
	public RectangleBounds getSurfaceBounds()
	{
		return transformBounds(this.molecule.getSurfaceBounds());
	}

	/**
	 * Gets the virtual transform.
	 * 
	 * @return the virtual transform
	 */
	public RigidTransform getVirtualTransform()
	{
		return this.t;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isCloserThan(edu.umd.umiacs.armor.math.Point3D, double)
	 */
	@Override
	public boolean isCloserThan(Point3d p, double distance)
	{
		return this.molecule.isCloserThan(this.t.inverseTransform(p),distance);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isContrastAtom(int)
	 */
	@Override
	public boolean isDisplacedAtom(int index)
	{
		return this.molecule.isDisplacedAtom(index);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isSolventAtom(int)
	 */
	@Override
	public boolean isSolventAtom(int index)
	{
		return this.molecule.isSolventAtom(index);
	}

	@Override
	public Iterator<Atom> iterator()
	{
		return new VirtualIterator(this.molecule, this.t);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#maxAtomDistance()
	 */
	@Override
	public double maxAtomDistance()
	{
		return this.molecule.maxAtomDistance();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#size()
	 */
	@Override
	public int size()
	{
		return this.molecule.size();
	}

	/**
	 * Transform bounds.
	 * 
	 * @param bounds
	 *          the bounds
	 * @return the rectangle bounds
	 */
	private RectangleBounds transformBounds(RectangleBounds bounds)
	{
		Point3d p1 = this.t.transform(bounds.lx, bounds.ly, bounds.lz);
		Point3d p2 = this.t.transform(bounds.lx, bounds.ly, bounds.uz);
		Point3d p3 = this.t.transform(bounds.lx, bounds.uy, bounds.lz);
		Point3d p4 = this.t.transform(bounds.lx, bounds.uy, bounds.uz);
		Point3d p5 = this.t.transform(bounds.ux, bounds.ly, bounds.lz);
		Point3d p6 = this.t.transform(bounds.ux, bounds.ly, bounds.uz);
		Point3d p7 = this.t.transform(bounds.ux, bounds.uy, bounds.lz);
		Point3d p8 = this.t.transform(bounds.ux, bounds.uy, bounds.uz);
		
		double[] xArray = new double[]{p1.x,p2.x,p3.x,p4.x,p5.x,p6.x,p7.x,p8.x};
		double[] yArray = new double[]{p1.y,p2.y,p3.y,p4.y,p5.y,p6.y,p7.y,p8.y};
		double[] zArray = new double[]{p1.z,p2.z,p3.z,p4.z,p5.z,p6.z,p7.z,p8.z};

		double lx = BasicMath.min(xArray);
		double ly = BasicMath.min(yArray);
		double lz = BasicMath.min(zArray);
		double ux = BasicMath.max(xArray);
		double uy = BasicMath.max(yArray);
		double uz = BasicMath.max(zArray);
		
		return new RectangleBounds(lx, ly, lz, ux, uy, uz);
	}
}
