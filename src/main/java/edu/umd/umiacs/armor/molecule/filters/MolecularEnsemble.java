/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule.filters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import edu.umd.umiacs.armor.cluster.Cluster;
import edu.umd.umiacs.armor.cluster.ClusteringSolution;
import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MoleculeRuntimeException;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.SortablePair;

public final class MolecularEnsemble implements Comparable<MolecularEnsemble>
{
	private final double chi2;
	private final ArrayList<Double> clusterChi2;
	private final ArrayList<int[]> clusterColumns;
	private final ArrayList<double[]> clusterWeights;
	
	private final int[] columns;
	private final ArrayList<Molecule> conformers;
	private final double[] weights;

	public static ArrayList<MolecularEnsemble> clusterEnsembles(List<MolecularEnsemble> solutionList, MoleculeAligner aligner, double rmsdCutoff) throws MoleculeAlignmentException
	{
		EnsembleStructureFiltering filter = new EnsembleStructureFiltering(aligner);
		
		//create sorted list of solutions
		ArrayList<MolecularEnsemble> sortedSolutionList = new ArrayList<MolecularEnsemble>(solutionList);
		Collections.sort(sortedSolutionList);
		
		//generate a list of aligned molecules, the order is sorted by chi2
		ArrayList<MolecularEnsemble> alignedSolutionList = new ArrayList<MolecularEnsemble>(sortedSolutionList.size());
		Molecule baseMolecule = sortedSolutionList.get(0).getConformer(0);
		for (MolecularEnsemble ensemble : sortedSolutionList)
		{
			ArrayList<Molecule> alignedMolecules = new ArrayList<Molecule>(ensemble.size());
			for (Molecule mol : ensemble.getConformerList())
				alignedMolecules.add(aligner.align(baseMolecule, mol));
			
			//generate the ensemble made out of aligned conformers
			alignedSolutionList.add(new MolecularEnsemble(alignedMolecules, 
					ensemble.getWeights(), ensemble.getColumns(), ensemble.getChi2()));
		}
		
		//put all the structures from all the ensembles in one list
		HashSet<Molecule> allStructures = new HashSet<Molecule>();		
		for (MolecularEnsemble ensemble : alignedSolutionList)
			allStructures.addAll(ensemble.getConformerList());
		
		//cluster all stuctures
		ClusteringSolution<ClusterableMolecule> clusteringSolution = filter.cluster(new ArrayList<Molecule>(allStructures), rmsdCutoff, true);
		
		//get the best non-aligned list, by checking against centroid based list
		ArrayList<MolecularEnsemble> storedCentroidEnsembleList = new ArrayList<MolecularEnsemble>();
		ArrayList<MolecularEnsemble> storedClusteredEnsembleList = new ArrayList<MolecularEnsemble>();
		Iterator<MolecularEnsemble> iter = sortedSolutionList.iterator();
		
		//go though the aligned ensembles, see what the centroids are, if does not exist add it
		for (MolecularEnsemble currentAlignedEnsemble : alignedSolutionList)
		{
			MolecularEnsemble currentNonAlignedEnsemble = iter.next();
			
			if (!Arrays.equals(currentNonAlignedEnsemble.columns, currentAlignedEnsemble.columns))
				throw new MoleculeRuntimeException("Aligned and non-aligned ensembles do not have same columns.");
			
			//find the associated centroids of this ensemble in the clusters
			ArrayList<Molecule> newEnsembleControidList = new ArrayList<Molecule>();
			for (Molecule mol : currentAlignedEnsemble.getConformerList())
			{
				ClusterableMolecule cMol = new ClusterableMolecule(mol, null, 0);
				Cluster<ClusterableMolecule> cluster = clusteringSolution.getContainingCluster(cMol);
				
				if (cluster==null)
					throw new MoleculeRuntimeException("Molecule not found in the clustering solution.");
				
				newEnsembleControidList.add(cluster.getCenter().getMolecule());
			}
			
			//create the clustered ensemble 
			MolecularEnsemble currentEnsembleCentroids = new MolecularEnsemble(newEnsembleControidList, 
					currentAlignedEnsemble.getWeights(), currentAlignedEnsemble.getColumns(), currentAlignedEnsemble.getChi2());
			
			//if not already in the list then add it
			boolean hasSame = false;
			int counter = 0;
			for (MolecularEnsemble currentStoredEnsembleCentroids : storedCentroidEnsembleList)
			{
				//try to get the current centroids in same order as
				int[] order = currentEnsembleCentroids.sameOrder(currentStoredEnsembleCentroids);
				if (order!=null)
				{
					hasSame = true;

					//add the statistics for this ensemble
					MolecularEnsemble permutedEnsemble = currentNonAlignedEnsemble.permute(order);					
					storedClusteredEnsembleList.get(counter).addInfo(permutedEnsemble);
					
					break;
				}
				
				counter++;
			}
			
			//if did not find this centroid cluster, add it to list
			if (!hasSame)
			{
				//add the centroid ensemble to the check list, and the original ensemble to the clusterd list
				storedCentroidEnsembleList.add(currentEnsembleCentroids);
				storedClusteredEnsembleList.add(currentNonAlignedEnsemble);
			}
		}
		
		return storedClusteredEnsembleList;
	}
	
	public MolecularEnsemble(List<Molecule> conformers, double[] weights, int[] columns, double chi2)
	{
		if (conformers==null || conformers.isEmpty())
			throw new MoleculeRuntimeException("Ensemble cannot be null or empty.");

		if (conformers.size()!=weights.length)
			throw new MoleculeRuntimeException("Ensemble size and weights must be consistant.");
		
		this.conformers = new ArrayList<Molecule>(conformers);
		this.weights = weights.clone();
		this.columns = columns.clone();
		this.chi2 = chi2;
		
		this.clusterColumns = new ArrayList<int[]>();
		this.clusterColumns.add(this.columns);
		this.clusterWeights = new ArrayList<double[]>();
		this.clusterWeights.add(this.weights);
		this.clusterChi2 = new ArrayList<Double>();
		this.clusterChi2.add(this.chi2);
	}
	
	private void addInfo(MolecularEnsemble m)
	{
		if (this.conformers.size()!=m.conformers.size())
			return;
		
		this.clusterWeights.add(m.weights);
		this.clusterColumns.add(m.columns);
		this.clusterChi2.add(m.chi2);
	}
	
	
	public MolecularEnsemble align(MoleculeAligner aligner) throws MoleculeAlignmentException
	{
		return align(aligner, this.conformers.get(0));
	}
	
	public MolecularEnsemble align(MoleculeAligner aligner, Molecule targetMolecule) throws MoleculeAlignmentException
	{
		ArrayList<Molecule> newList = new ArrayList<Molecule>(this.conformers.size());
		Iterator<Molecule> molIter = this.conformers.iterator();
		
		while(molIter.hasNext())
		{
			Molecule alignedMol = aligner.align(targetMolecule, molIter.next());
			newList.add(alignedMol);
		}
		
		return new MolecularEnsemble(newList, this.weights, this.columns, this.chi2);
	}
	
	public ArrayList<MolecularEnsemble> clusterConformers(MoleculeAligner aligner, double rmsdCutoff) throws MoleculeAlignmentException
	{
		EnsembleStructureFiltering filter = new EnsembleStructureFiltering(aligner);
		
		//generate a list of aligned molecules
		ArrayList<Molecule> alignedEnsemble = new ArrayList<Molecule>(size());
		Molecule baseMolecule = this.conformers.get(0);
		for (Molecule molecule : this.conformers)
		{
			alignedEnsemble.add(aligner.align(baseMolecule, molecule));
		}
		
		//cluster all stuctures
		ClusteringSolution<ClusterableMolecule> clusteringSolution = filter.cluster(alignedEnsemble, rmsdCutoff, true);
		
		//create the clusters
		ArrayList<SortablePair<Double, MolecularEnsemble>> ensembles = new ArrayList<SortablePair<Double, MolecularEnsemble>>();		
		for (Cluster<ClusterableMolecule> cluster : clusteringSolution)
		{
			//combine into one ensemble
			int[] columns = new int[cluster.size()];
			double[] weights = new double[cluster.size()];
			ArrayList<Molecule> newMolList = new ArrayList<Molecule>(size());

			int counter = 0;
			double totalWeight = 0.0;
			for (ClusterableMolecule mol : cluster.getPoints())
			{
				//get index
				int index = alignedEnsemble.indexOf(mol.getMolecule());
				
				columns[counter] = this.columns[index];
				weights[counter] = this.weights[index];
				totalWeight += this.weights[index];
				newMolList.add(this.conformers.get(index));
				
				counter++;
			}
			
			//add the new ensemble to the lsit
			ensembles.add(new SortablePair<Double, MolecularEnsemble>(-totalWeight, new MolecularEnsemble(newMolList, weights, columns, Double.NaN)));			
		}
		
		//sort in descending order
		Collections.sort(ensembles);
		
		//store the sorted list
		ArrayList<MolecularEnsemble> sortedEnsemble = new ArrayList<MolecularEnsemble>(ensembles.size());
		for (SortablePair<Double, MolecularEnsemble> pair : ensembles)
			sortedEnsemble.add(pair.y);
		
		return sortedEnsemble;
	}
	
	public int clusterSize()
	{
		return this.clusterChi2.size();
	}
	
	public MolecularEnsemble combineConformers(MoleculeAligner aligner, double rmsdCutoff) throws MoleculeAlignmentException
	{
		EnsembleStructureFiltering filter = new EnsembleStructureFiltering(aligner);
		
		//generate a list of aligned molecules
		ArrayList<Molecule> alignedEnsemble = new ArrayList<Molecule>(size());
		Molecule baseMolecule = this.conformers.get(0);
		for (Molecule molecule : this.conformers)
		{
			alignedEnsemble.add(aligner.align(baseMolecule, molecule));
		}
		
		//cluster all stuctures
		ClusteringSolution<ClusterableMolecule> clusteringSolution = filter.cluster(alignedEnsemble, rmsdCutoff, true);
		
		//combine into one ensemble
		int[] columns = new int[clusteringSolution.size()];
		double[] weights = new double[clusteringSolution.size()];
		ArrayList<Molecule> newMolList = new ArrayList<Molecule>(size());
		
		int counter = 0;
		for (Cluster<ClusterableMolecule> cluster : clusteringSolution)
		{
			int centroidIndex = alignedEnsemble.indexOf(cluster.getCenter().getMolecule());
			
			weights[counter] = 0;
			columns[counter] = this.columns[centroidIndex];
			newMolList.add(getConformer(centroidIndex));
			
			//add up the new weights
			for (ClusterableMolecule mol : cluster.getPoints())
			{
				int index = alignedEnsemble.indexOf(mol.getMolecule());

				weights[counter] += this.weights[index];
			}
			
			counter++;
		}
		
		MolecularEnsemble newEnsemble = new MolecularEnsemble(newMolList, weights, columns, this.chi2);

		return newEnsemble.sort();
	}
	
	@Override
	public int compareTo(MolecularEnsemble e)
	{
		return Double.compare(this.getChi2(), e.getChi2());
	}
	
	public double getChi2()
	{
		return this.chi2;
	}
	
	public double getAbsoluteError()
	{
		return Math.sqrt(this.chi2);
	}
	
	public double getRelativeError(double inputNorm)
	{
		return getAbsoluteError()/inputNorm;
	}
	
	public double getClusterChi2(int cluster)
	{
		return this.clusterChi2.get(cluster);
	}
	
	public double[] getClusterColumnNormalWeights(int index)
	{
		double[] weights = new double[clusterSize()];
		
		for (int iter=0; iter<weights.length; iter++)
		{
			double[] currWeights = this.clusterWeights.get(iter);
			double total = BasicMath.sum(currWeights);

			weights[iter] = currWeights[index]/total;
		}
		
		return weights;
	}
	
	public int[] getClusterColumns(int cluster)
	{
		return this.clusterColumns.get(cluster).clone();
	}
	
	public double[] getClusterWeights(int cluster)
	{
		return this.clusterWeights.get(cluster).clone();
	}
	
	public int[] getColumns()
	{
		return this.columns.clone();
	}
	
	public Molecule getConformer(int index)
	{
		return this.conformers.get(index);
	}
	
	public ArrayList<Molecule> getConformerList()
	{
		return new ArrayList<Molecule>(this.conformers);
	}

	public double[] getNormalizedWeights()
	{
		double total = BasicMath.sum(this.weights);
		
		return BasicMath.divide(this.weights, total);
	}
	
	public double[] getWeights()
	{
		return this.weights.clone();
	}
	
	public boolean isSameEnsemble(MolecularEnsemble m)
	{
		if (sameOrder(m)==null)
			return false;
		
		return true;
	}
	
	public MolecularEnsemble permute(int[] order)
	{
		//allocate new arrays
		double[] newWeights = new double[this.weights.length];
		int[] newColumns = new int[this.columns.length];
		ArrayList<Molecule> newMolecules = new ArrayList<Molecule>(this.conformers.size());
		
		//add the permuted elements
		int counter = 0;
		for (int index : order)
		{
			newColumns[counter] = this.columns[index];
			newWeights[counter] = this.weights[index];
			newMolecules.add(this.conformers.get(index));
					
			counter++;
		}

		return new MolecularEnsemble(newMolecules, newWeights, newColumns, this.chi2);
	}
	
	public int[] sameOrder(MolecularEnsemble m)
	{		
		if (this.conformers.size()!=m.conformers.size())
			return null;
		
		//allocate the permutation
		ArrayList<Integer> permutation = new ArrayList<Integer>(m.conformers.size());
		
		//for each conformer find its index in m
		for (Molecule mol : this.conformers)
		{
		  int sourceIndex = 0;
		  for (Molecule sourceMol : m.conformers)
		  {
		  	if (sourceMol==mol && !permutation.contains(sourceIndex))
		  	{
		  		permutation.add(sourceIndex);
		  		break;
		  	}
		  	sourceIndex++;
		  }
		  
		  //did not find the element so return null
		  if (sourceIndex==m.conformers.size())
		  	return null;
		}
		
		return BasicUtils.toArray(permutation);
	}

	public int size()
	{
		return this.conformers.size();
	}

	public MolecularEnsemble sort()
	{
		return permute(BasicUtils.sortIndexReverse(this.weights));
	}
	
}
