/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.refinement.DockInfo;
import edu.umd.umiacs.armor.util.Pair;
import edu.umd.umiacs.armor.util.SortablePair;

/**
 * The Class TwoDomainComplex.
 * 
 * @param <DomainOne>
 *          the generic type
 * @param <DomainTwo>
 *          the generic type
 */
public final class MultiDomainComplex extends AbstractMolecule
{	
	private final ArrayList<VirtualMoleculeWrapper> molList;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3348753196081506955L;
	
	public MultiDomainComplex(List<? extends Molecule> moleculeList)
	{
		this(moleculeList, null);
	}
	
	public MultiDomainComplex(List<? extends Molecule> moleculeList, List<RigidTransform> transforms)
	{
		if (moleculeList == null)
			throw new MoleculeRuntimeException("moleculeList cannot be null.");
		
		if (transforms != null && moleculeList.size()!=transforms.size())
			throw new MoleculeRuntimeException("Transform list size must match the number of molecules.");

		
		this.molList = new ArrayList<VirtualMoleculeWrapper>(moleculeList.size());
		
		Iterator<RigidTransform> iter = null;
		if (transforms!=null)
			iter = transforms.iterator();
		
		for (Molecule mol : moleculeList)
		{
			if (transforms==null)
			{
				this.molList.add(new VirtualMoleculeWrapper(mol));
			}
			else
			{
				RigidTransform t = iter.next();
				this.molList.add(new VirtualMoleculeWrapper(mol,t));
			}
		}
	}
	
	public MultiDomainComplex(Molecule mol1, Molecule mol2)
	{
		this(mol1, RigidTransform.identityTransform(), mol2, RigidTransform.identityTransform());
	}
	
	public MultiDomainComplex(Molecule mol1, Molecule mol2, RigidTransform t)
	{
		this(mol1, RigidTransform.identityTransform(), mol2, t);
	}
	
	public MultiDomainComplex(Molecule mol1, RigidTransform t1, Molecule mol2, RigidTransform t2)
	{
		this(Arrays.asList(mol1,mol2), Arrays.asList(t1,t2));
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#center()
	 */
	@Override
	public Point3d center()
	{
		Point3d c = Point3d.getOrigin();
		int totalSize = 0;
		
		for (Molecule mol : this.molList)
		{
			c = mol.center().mult(mol.size());
			totalSize += mol.size();
		}
		
		//adjust position to the virtual positions
		Point3d newCenter = c;
		if (totalSize>0.0)
		  newCenter = c.divide(totalSize);
		
		return newCenter;
	}
	
	public MultiDomainComplex createSecondDomainTransformed(RigidTransform transform)
	{
		if (this.numberDomains()!=2)
			throw new MoleculeRuntimeException("Can only be formed on a two domain system.");
		
		return createTransformed(RigidTransform.identityTransform(), transform);
	}
	
	public ArrayList<Molecule> createSecondDomainTranslatedDockList(List<DockInfo> transformList) throws AtomNotFoundException
	{
		ArrayList<RigidTransform> tList = new ArrayList<RigidTransform>(transformList.size());
		
		for (DockInfo d : transformList)
			tList.add(d.getRigidTransform());
		
		return createSecondDomainTranslatedList(tList);
	}

	
	public ArrayList<Molecule> createSecondDomainTranslatedList(List<RigidTransform> transformList) throws AtomNotFoundException
	{
		if (this.numberDomains()!=2)
			throw new MoleculeRuntimeException("Can only be formed on a two domain system.");
		
		ArrayList<Molecule> moleculeList = new ArrayList<Molecule>(transformList.size());
		for (RigidTransform t : transformList)
			moleculeList.add(this.createSecondDomainTransformed(t));
		
		return moleculeList;
	}
	
	/**
	 * Creates the transformed.
	 * 
	 * @param t1
	 *          the t1
	 * @param t2
	 *          the t2
	 * @return the two domain complex
	 */
	public MultiDomainComplex createTransformed(List<RigidTransform> transforms)
	{
		if (transforms.size()!=this.molList.size())
			throw new MoleculeRuntimeException("Number of transforms must match number of molecules.");
		
		ArrayList<Molecule> currMolList = new ArrayList<Molecule>(this.molList.size());
		ArrayList<RigidTransform> currTransformList = new ArrayList<RigidTransform>(this.molList.size());
		
		Iterator<RigidTransform> iter = transforms.iterator();
		for (VirtualMoleculeWrapper mol : this.molList)
		{
			currMolList.add(mol.getMolecule());
			currTransformList.add(mol.getVirtualTransform().union(iter.next()));
		}
		
		return new MultiDomainComplex(currMolList, currTransformList);
	}
	
	public MultiDomainComplex createTransformed(RigidTransform...transforms)
	{
		return createTransformed(Arrays.asList(transforms));
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#deuterate(double)
	 */
	@Override
	public MultiDomainComplex deuterate(double deuteriumFraction)
	{
		ArrayList<Molecule> molList = new ArrayList<Molecule>();
		
		for (Molecule mol : this.molList)
			molList.add(mol.deuterate(deuteriumFraction));

		return new MultiDomainComplex(molList, null);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#distanceToCore(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public double distanceToCore(Point3d p)
	{
		double minDist = Double.POSITIVE_INFINITY;
		
		for (Molecule mol : this.molList)
			minDist = Math.min(minDist, mol.distanceToCore(p));
		
	  return minDist;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#exists(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public boolean exists(Atom a)
	{
		for (Molecule mol : this.molList)
			if (mol.exists(a))
				return true;
		
		return false;
	}
	
	private Pair<Integer,Integer> findIndexPosition(int index)
	{
		int prevSizes = 0;
		
		int counter = 0;
		for (Molecule mol : this.molList)
		{
			if (index<prevSizes+mol.size())
				return new Pair<Integer,Integer>(counter, index-prevSizes);
			
			prevSizes += mol.size();
			counter++;
		}
		
		throw new IndexOutOfBoundsException();
	}

	@Override
	public Atom getAtom(int index)
	{
		Pair<Integer, Integer> pos = findIndexPosition(index);
		
		return this.molList.get(pos.x).getAtom(pos.y);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public Point3d getAtomPosition(Atom a) throws AtomNotFoundException
	{
		for (Molecule mol : this.molList)
		{
			try
			{
				return mol.getAtomPosition(a);
			}
			catch (AtomNotFoundException e)
			{
			}
		}
		
		throw new AtomNotFoundException("Atom not found.", a);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(int)
	 */
	@Override
	public Point3d getAtomPosition(int index)
	{
		Pair<Integer, Integer> pos = findIndexPosition(index);
		
		return this.molList.get(pos.x).getAtomPosition(pos.y);
	}

	@Override
	public ArrayList<Atom> getAtoms()
	{
		ArrayList<Atom> atomList = new ArrayList<Atom>(size());
		for (Molecule mol : this.molList)
			atomList.addAll(mol.getAtoms());
		
		return atomList;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomSurfaceRadius(int)
	 */
	@Override
	public double getAtomSurfaceRadius(int index)
	{
		Pair<Integer, Integer> pos = findIndexPosition(index);
		
		return this.molList.get(pos.x).getAtomSurfaceRadius(pos.y);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomType(int)
	 */
	@Override
	public AtomType getAtomType(int index)
	{
		Pair<Integer, Integer> pos = findIndexPosition(index);
		
		return this.molList.get(pos.x).getAtomType(pos.y);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getBounds()
	 */
	@Override
	public RectangleBounds getBounds()
	{
		Iterator<VirtualMoleculeWrapper> molIter = this.molList.iterator();
		
		RectangleBounds bounds = molIter.next().getBounds();
		
		while (molIter.hasNext())
			bounds = bounds.union(molIter.next().getBounds());
		
		return bounds;
	}

	@Override
	public List<? extends Atom> getCloseAtoms(Point3d p, double maxDist)
	{
		//combine the list from both atoms
		ArrayList<SortablePair<Double,Atom>> combinedSortableList = new ArrayList<SortablePair<Double,Atom>>();
		
		for (Molecule mol : this.molList)
		{
			List<? extends Atom> list = mol.getCloseAtoms(p, maxDist);
			for (Atom a : list)
				combinedSortableList.add(new SortablePair<Double, Atom>(a.distanceToSurface(p), a));
		}

		//sort the list
		Collections.sort(combinedSortableList);

		//add the first elements of sorted list
		ArrayList<Atom> combinedList = new ArrayList<Atom>(combinedSortableList.size());
		for (SortablePair<Double,Atom> pair : combinedSortableList)
		{
			combinedList.add(pair.y);
		}
		
		return combinedList;
	}

	@Override
	public ArrayList<Atom> getClosestAtoms(Point3d p, int n)
	{
		//combine the list from both atoms
		ArrayList<SortablePair<Double,Atom>> combinedSortableList = new ArrayList<SortablePair<Double,Atom>>();
		
		for (Molecule mol : this.molList)
		{
			List<? extends Atom> list = mol.getCloseAtoms(p, n);
			for (Atom a : list)
				combinedSortableList.add(new SortablePair<Double, Atom>(a.distanceToSurface(p), a));
		}
		
		//sort the list
		Collections.sort(combinedSortableList);

		//add the first elements of sorted list
		ArrayList<Atom> combinedList = new ArrayList<Atom>(n);
		for (SortablePair<Double,Atom> pair : combinedSortableList)
		{
			combinedList.add(pair.y);
			if (combinedList.size()>=n)
				break;
		}
		
		return combinedList;
	}
	
	public Molecule getDomain(int index)
	{
		return this.molList.get(index);
	}

	
	public Molecule getDomainOne()
	{
		return getDomain(0);
	}

	public List<Molecule> getDomains()
	{
		return new ArrayList<Molecule>(this.molList);
	}

	public Molecule getDomainTwo()
	{
		return getDomain(1);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getSurfaceBounds()
	 */
	@Override
	public RectangleBounds getSurfaceBounds()
	{
		Iterator<VirtualMoleculeWrapper> molIter = this.molList.iterator();
		
		RectangleBounds bounds = molIter.next().getSurfaceBounds();
		
		while (molIter.hasNext())
			bounds = bounds.union(molIter.next().getSurfaceBounds());
		
		return bounds;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isCloserThan(edu.umd.umiacs.armor.math.Point3D, double)
	 */
	@Override
	public boolean isCloserThan(Point3d p, double distance)
	{
		for (Molecule mol : this.molList)
			if (mol.isCloserThan(p,distance))
				return true;
		
		return false;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isContrastAtom(int)
	 */
	@Override
	public boolean isDisplacedAtom(int index)
	{
		Pair<Integer, Integer> pos = findIndexPosition(index);
		
		return this.molList.get(pos.x).isDisplacedAtom(pos.y);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isSolventAtom(int)
	 */
	@Override
	public boolean isSolventAtom(int index)
	{
		Pair<Integer, Integer> pos = findIndexPosition(index);
		
		return this.molList.get(pos.x).isSolventAtom(pos.y);
	}

	@Override
	public Iterator<Atom> iterator()
	{
		return new MultiDomainAtomIterator(this.molList);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#maxAtomDistance()
	 */
	@Override
	public double maxAtomDistance()
	{
		double maxDistance = 0.0;		
		Point3d c = getSurfaceBounds().center();
		
		for (Atom a : this)
		{
			maxDistance = Math.max(maxDistance, c.distanceSquared(a.getPosition()));
		}
			
		
		return BasicMath.sqrt(maxDistance)*2.0;	
	}
	
	public int numberDomains()
	{
		return this.molList.size();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#size()
	 */
	@Override
	public int size()
	{
		int size = 0;
		
		for (Molecule mol : this.molList)
			size += mol.size();
		
		return size;
	}
}
