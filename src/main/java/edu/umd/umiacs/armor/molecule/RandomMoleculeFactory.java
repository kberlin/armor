/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import edu.umd.umiacs.armor.math.Point3d;

/**
 * A factory for creating RandomMolecule objects.
 */
public class RandomMoleculeFactory
{
	
	/**
	 * The Enum Shape.
	 */
	public enum Shape
	{
	  
  	/** The SPHERE. */
  	SPHERE,
	  
  	/** The SQUARE. */
  	SQUARE;
	}
	
	/**
	 * The Enum Type.
	 */
	public enum Type
	{
	  
  	/** The PROTEIN. */
  	PROTEIN(.02),
	  
  	/** The RNA. */
  	RNA(.02);
	  
	  /** The atom density. */
  	public final double atomDensity;
	  
	  /**
		 * Instantiates a new type.
		 * 
		 * @param atomDensity
		 *          the atom density
		 */
  	Type(double atomDensity)
	  {
	  	this.atomDensity = atomDensity;
	  }
	}

	/**
	 * Generate molecule.
	 * 
	 * @param type
	 *          the type
	 * @param shape
	 *          the shape
	 * @param numAtoms
	 *          the num atoms
	 * @return the basic molecule
	 * @throws MoleculeRuntimeException
	 *           the molecule runtime exception
	 */
	public static BasicMolecule generateMolecule(Type type, Shape shape, int numAtoms) throws MoleculeRuntimeException
	{
		BasicMolecule molecule = null;
		
		int maxInsertionTries = 10000;
		double radius = 0.0;
		double minPos;
		double maxPos;

		switch (shape)
		{
			case SPHERE: radius = Math.pow(numAtoms/type.atomDensity/Math.PI*3.0/4.0,1.0/3.0); break;
			case SQUARE: radius = Math.pow(numAtoms/type.atomDensity,1.0/3.0)/2.0; break;
		}
			
		minPos = -radius;
		maxPos = radius;
		
    java.util.Random randomGenerator = new java.util.Random();
    molecule = new BasicMolecule(new RectangleBounds(minPos, minPos, minPos, maxPos, maxPos+1.0, maxPos+1.0));
		
		for (int iter=0; iter<numAtoms; iter++)
		{
			AtomType currType = null;
			switch (type)
			{
				case PROTEIN : currType = getRandomAtomTypeProtein(); break;
				case RNA : currType = getRandomAtomTypeRNA(); break;		
			}

			int insertionTry = 0;			
			do
			{
				double x = randomGenerator.nextDouble()*radius*2.0-radius;
				double y = randomGenerator.nextDouble()*radius*2.0-radius;
				double z = randomGenerator.nextDouble()*radius*2.0-radius;
				
				Point3d newPoint = new Point3d(x,y,z);
				
				boolean isBounded = false;
				switch (shape)
				{
					case SPHERE: isBounded = (x*x+y*y+z*z)<(radius*radius); break;
					case SQUARE: isBounded = x>=minPos && x<maxPos && y>=minPos && y<maxPos && z>=minPos && z<maxPos; break;
				}
				
				if (isBounded)
				{
					//add only if does not collide with previous atoms
					if (!molecule.isCloserThan(newPoint, AtomImpl.surfaceRadius(currType)))
					{
						try
						{
							molecule.add(new AtomImpl(newPoint, currType));						
							break;
						} 
						catch (DuplicateAtomException e)
						{
							throw new MoleculeRuntimeException(e);
						}
					}
					else				
						insertionTry++;
				}
			}
			while(insertionTry<=maxInsertionTries);
			
			if (insertionTry>=maxInsertionTries)
			{
				throw new MoleculeRuntimeException("Could not generate random molecule. Desired molecule is too dense.");
			}
		}
		
		return molecule;
	}

	/**
	 * Generate protein.
	 * 
	 * @param shape
	 *          the shape
	 * @param numAtoms
	 *          the num atoms
	 * @return the basic molecule
	 * @throws MoleculeRuntimeException
	 *           the molecule runtime exception
	 */
	public static BasicMolecule generateProtein(Shape shape, int numAtoms) throws MoleculeRuntimeException
	{
		return generateMolecule(Type.PROTEIN, shape, numAtoms);
	}
	
	/**
	 * Generate sphere protein.
	 * 
	 * @param numAtoms
	 *          the num atoms
	 * @return the basic molecule
	 * @throws MoleculeRuntimeException
	 *           the molecule runtime exception
	 */
	public static BasicMolecule generateSphereProtein(int numAtoms) throws MoleculeRuntimeException
	{
		return generateMolecule(Type.PROTEIN, Shape.SPHERE, numAtoms);
	}
	
	/**
	 * Generate square protein.
	 * 
	 * @param numAtoms
	 *          the num atoms
	 * @return the basic molecule
	 * @throws MoleculeRuntimeException
	 *           the molecule runtime exception
	 */
	public static BasicMolecule generateSquareProtein(int numAtoms) throws MoleculeRuntimeException
	{
		return generateMolecule(Type.PROTEIN, Shape.SQUARE, numAtoms);
	}  
	
	/**
	 * Gets the random atom type protein.
	 * 
	 * @return the random atom type protein
	 */
	public static AtomType getRandomAtomTypeProtein()
	{
		AtomType[] typeArray = {
					AtomType.HYDROGEN,
					AtomType.HYDROGEN,
					AtomType.HYDROGEN,
					AtomType.HYDROGEN,
					AtomType.HYDROGEN,
					AtomType.HYDROGEN,
					AtomType.HYDROGEN,
					AtomType.HYDROGEN,
					AtomType.HYDROGEN,
					AtomType.OXYGEN,
					AtomType.OXYGEN,
					AtomType.NITROGEN,
					AtomType.NITROGEN,
					AtomType.CARBON,
					AtomType.CARBON,
					AtomType.CARBON,
					AtomType.CARBON,
					AtomType.CARBON,
					AtomType.CARBON,
					AtomType.SULFUR,
					AtomType.PHOSPHORUS
				};

 		int radomNumber = (int)(Math.random()*typeArray.length);
 		
  	return typeArray[radomNumber];
	}

	/**
	 * Gets the random atom type rna.
	 * 
	 * @return the random atom type rna
	 */
	public static AtomType getRandomAtomTypeRNA()
	{
		return null;
	}	
}
