/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;

import edu.umd.umiacs.armor.util.HashCodeUtil;

public final class PrimaryStructure implements Serializable
{
	private final String chainID;
	
	private final int hashCode;
	private final String name;
	private final int residue;
	private final double tempFactor;
	private final AtomGroupType type;
	private final long uniqueId;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5094794332076697328L;
	
	private static int computeHashCode(int residue, String name, String chainID, long uniqueId)
  {
    int result = HashCodeUtil.SEED;
	    
    //collect the contributions of various fields
    result = HashCodeUtil.hash(result, residue);
    result = HashCodeUtil.hash(result, name);
    result = HashCodeUtil.hash(result, chainID);
    result = HashCodeUtil.hash(result, uniqueId);
	    
	  return result;
  }
	
  private static String fixChainName(String chainID)
	{
  	chainID = chainID.trim();
  	if (chainID.isEmpty())
  		chainID = "A";

  	return chainID;
	}

  private static String fixName(String name)
	{
		name = name.trim();
		
  	if (name.equalsIgnoreCase("HN"))
  		name = "H";

  	return name;
	}
	
	public static AtomGroupType getGroupType(String groupName)
	{
  	AtomGroupType type = PdbStructure.findGroupType(groupName);
  	if (type==null)
  	{
  		type = HetGroupType.UKW;
  	}
  	
  	return type;
	}
	
	protected PrimaryStructure(int residue, String name, String chainID, AtomGroupType type, double tempFactor, long uniqueId)
	{
  	this.residue = residue;
  	this.name = name;
  	this.chainID = chainID;
  	this.tempFactor = tempFactor;
  	this.uniqueId = uniqueId;
		this.type = type;
		
  	this.hashCode = computeHashCode(this.residue, this.name, this.chainID, this.uniqueId);
	}
	
	public PrimaryStructure(int residue, String name, String chainID, String groupName)
  {
  	this(residue, name, chainID, groupName, 0.0, 0);
  }
	
	public PrimaryStructure(int residue, String name, String chainID, String groupName, double tempFactor)
  {
  	this(residue, name, chainID, groupName, tempFactor, 0);
  }
	
	public PrimaryStructure(int residue, String name, String chainID, String groupName, double tempFactor, int uniqueId)
	{
		this(residue, fixName(name), fixChainName(chainID), getGroupType(groupName), tempFactor, uniqueId);
	}
	
	protected PrimaryStructure(String name, String chainID, AtomGroupType type, double tempFactor, long uniqueId)
	{
  	this.residue = (int)uniqueId;
  	this.name = name;
  	this.chainID = chainID;
  	this.tempFactor = tempFactor;
  	this.uniqueId = uniqueId;
		this.type = type;
		
  	this.hashCode = computeHashCode(this.residue, this.name, this.chainID, this.uniqueId);
	}
	
	public PrimaryStructure changeType(AtomType type)
	{
		return new PrimaryStructure(getResidueNumber(), type.getName(), getChainID(), getGroupType(), getTempFactor(), this.uniqueId);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		PrimaryStructure other = (PrimaryStructure) obj;
		
		if (this.hashCode!=other.hashCode)
			return false;
		
		if (this.residue != other.residue)
			return false;
		
		if (this.uniqueId != other.uniqueId)
			return false;
		
		if (this.chainID == null)
		{
			if (other.chainID != null)
				return false;
		}
		else 
		if (!this.chainID.equals(other.chainID))
			return false;
		
		if (this.name == null)
		{
			if (other.name != null)
				return false;
		}
		else 
		if (!this.name.equals(other.name))
			return false;
		
		return true;
	}
	
	public String getAtomName()
	{
		return this.name;
	}
	
	public String getChainID()
	{
		return this.chainID;
	}
	
	public AtomGroupType getGroupType()
	{
		return this.type;
	}

	public int getResidueNumber()
	{
		return this.residue;
	}

	public double getTempFactor()
	{
		return this.tempFactor;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
    return this.hashCode;
	}
	
	public boolean isExchangedHydrogen()
	{
  	//TODO: Add RNA and DNA information
		return ((this.name.equalsIgnoreCase("H") ||
				this.name.equalsIgnoreCase("HN") ||
				this.name.equalsIgnoreCase("HZ1") ||
				this.name.equalsIgnoreCase("HZ2") ||
				this.name.equalsIgnoreCase("HZ3") ||
				this.name.equalsIgnoreCase("HE") ||
				this.name.equalsIgnoreCase("HH11") ||
				this.name.equalsIgnoreCase("HH12") ||
				this.name.equalsIgnoreCase("HH21") ||
				this.name.equalsIgnoreCase("HH22") ||
				this.name.equalsIgnoreCase("HD21") ||
				this.name.equalsIgnoreCase("HD22") ||
				this.name.equalsIgnoreCase("HE21") ||
				this.name.equalsIgnoreCase("HE22"))
			);
	}

	public boolean isSameResidue(PrimaryStructure p)
	{
		return this.residue==p.residue && this.chainID.equals(p.chainID);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "[ChainID: "+getChainID()+", Residue: "+getResidueNumber()+", Name: "+getAtomName()+"]";
	}
	
	
}
