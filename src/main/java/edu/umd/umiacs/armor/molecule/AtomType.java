/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;
import java.util.HashMap;

import edu.umd.umiacs.armor.math.BasicMath;

/**
 * The Enum AtomType.
 */
public final class AtomType implements Comparable<AtomType>, Serializable
{ 

	/** The atomic mass. */
  private final double atomicMass; //g/mol

	/** The excluded water volume. */
  private final double excludedWaterVolume;
	
	/** The gyromagnetic ratio. */
  private final double gyromagneticRatio; //Hz/Tesla
	
	/** The name. */
  private final String name;
	
	/** The atomic mass. */
  private final double spin; //g/mol

	/** The van der waals radius. */
  private final double vanDerWaalsRadius; //Angstrom
	
	private final static HashMap<String,AtomType> _ATOM_TYPES = new HashMap<String,AtomType>();
	
	/** The CARBON. */
	public final static AtomType CARBON = new AtomType("C",1.70, 12.0107, 16.44, 10.705e6*BasicMath.TWOPI, 0.5);
	
	/** The DEUTERIUM. */
	public final static AtomType DEUTERIUM = new AtomType("D",1.09, 2.00794, 5.15, 6.53593*BasicMath.TWOPI, 0.5);
	
	/** The HYDROGEN. */
	public final static AtomType HYDROGEN = new AtomType("H",1.09, 1.00794, 5.15, 42.576e6*BasicMath.TWOPI, 0.5);
	 
	 /** The IODINE. */
	public final static AtomType IODINE= new AtomType ("I",1.98, 126.90447, 24.00, 53.5, 2.5); //TODO check values
	 
	/** The IRON. */
	public final static AtomType IRON = new AtomType("FE",2.00, 55.845, 24.86, 42.576e6/13.0*BasicMath.TWOPI, 0.5);
	
	/** The NITROGEN. */
	public final static AtomType NITROGEN = new AtomType("N",1.55, 14.0067, 2.49, -4.3156e6*BasicMath.TWOPI, 0.5);
  
  /** The OXYGEN. */
	public final static AtomType OXYGEN = new AtomType("O",1.52, 16.0, 9.13, -5.7716e6*BasicMath.TWOPI, 0.5);
  
  /** The PHOSPHORUS. */
	public final static AtomType PHOSPHORUS = new AtomType("P",1.80, 30.9737, 5.73, 17.235e6*BasicMath.TWOPI, 0.5); //TODO check the value
  
  private static final long serialVersionUID = 3763038015043514635L;
  
  /** The SUDO water. */
	public final static AtomType SUDO_WATER = new AtomType("WATER",0.01, 18.0, 1.0, 1.0*BasicMath.TWOPI, 0.5); //TODO check the value

  /** The SULFUR. */
	public final static AtomType SULFUR = new AtomType("S",1.80, 32.065, 19.86, 3.2627*BasicMath.TWOPI, 0.5);

  public static AtomType getAtomType(String name)
  {
  	return _ATOM_TYPES.get(name.toUpperCase());
  }
 
  public static AtomType getAtomTypeFromFull(String fullName)
	{
		String parsedName = fullName.trim();
		if (parsedName.equalsIgnoreCase("HN"))
			parsedName = "H";
 	
  	AtomType type;
		type = AtomType.getAtomType(parsedName.substring(0, 1));
		if (type == null && parsedName.length()>=2)
		{
			type = AtomType.getAtomType(parsedName.substring(1, 2));
		}
		if (type == null && parsedName.length()>=2)
			type = AtomType.getAtomType(parsedName.substring(0, 2));
 	
    if (type == null)
 		  throw new MoleculeRuntimeException("Uknown atom name "+fullName+".");
 	
 	  return type;
	}
  
  public AtomType(String name, double vanDerWaalsRadius, double mass, double excludedWaterVolume, double gyromagneticRatio, double spin) 
  {
  	name = name.toUpperCase();
  	if (_ATOM_TYPES.containsKey(name))
  		throw new MoleculeRuntimeException("Molecule type alread exists.");
  	
    this.vanDerWaalsRadius = vanDerWaalsRadius;
    this.excludedWaterVolume = excludedWaterVolume;
    this.name = name.intern();
    this.atomicMass = mass;
    this.gyromagneticRatio = gyromagneticRatio;
    this.spin = spin;
    
    _ATOM_TYPES.put(name, this);
  }

	@Override
	public int compareTo(AtomType a)
	{
		return this.name.compareTo(a.name);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AtomType other = (AtomType) obj;
		if (this.name == null)
		{
			if (other.name != null)
				return false;
		}
		else if (!this.name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * @return the atomicMass
	 */
	public double getAtomicMass()
	{
		return this.atomicMass;
	}

	/**
	 * @return the excludedWaterVolume
	 */
	public double getExcludedWaterVolume()
	{
		return this.excludedWaterVolume;
	}

	/**
	 * @return the gyromagneticRatio
	 */
	public double getGyromagneticRatio()
	{
		return this.gyromagneticRatio;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * @return the spin
	 */
	public double getSpin()
	{
		return this.spin;
	}

	/**
	 * @return the vanDerWaalsRadius
	 */
	public double getVanDerWaalsRadius()
	{
		return this.vanDerWaalsRadius;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.name == null) ? 0 : this.name.hashCode());
		return result;
	}
}