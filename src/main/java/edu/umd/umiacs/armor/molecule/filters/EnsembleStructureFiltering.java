/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule.filters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.cluster.Cluster;
import edu.umd.umiacs.armor.cluster.ClusteringSolution;
import edu.umd.umiacs.armor.cluster.HierarchicalClustering;
import edu.umd.umiacs.armor.cluster.HierarchicalSolution;
import edu.umd.umiacs.armor.math.Metric;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.refinement.RefinementRuntimeException;

public final class EnsembleStructureFiltering
{
	private final Metric<Molecule> metric;
	private final HierarchicalClustering<ClusterableMolecule> hierarchicalCluster;
	
  public EnsembleStructureFiltering(Metric<Molecule> metric)
  {
  	this.metric = metric;
  	this.hierarchicalCluster = new HierarchicalClustering<ClusterableMolecule>();
  }
  
  public ClusteringSolution<ClusterableMolecule> cluster(List<Molecule> molecules, double rmsdCutoff, boolean parallel)
  {
  	Iterator<Molecule> molIter = molecules.iterator();
  	Molecule firstMol = molIter.next();
  	
  	//add the first molecule
  	final ArrayList<Cluster<ClusterableMolecule>> clusters = new ArrayList<Cluster<ClusterableMolecule>>();
  	clusters.add(new Cluster<ClusterableMolecule>(new ClusterableMolecule(firstMol, this.metric, 0)));

  	//perform initial alignment of the molecules
  	int count = 1;
  	while(molIter.hasNext())
  	{
  		Molecule currMol = molIter.next(); 		  		
  		clusters.add(new Cluster<ClusterableMolecule>(new ClusterableMolecule(currMol, this.metric, count)));
  		
  		count++;
  	}  	
   	
  	//figure out number of cores
  	final int numThreads = Runtime.getRuntime().availableProcessors();

  	//compute all-to-all distances in parallel
  	ExecutorService execSvc;
  	if (parallel)
  		execSvc = Executors.newCachedThreadPool();
  	else
  		execSvc = Executors.newSingleThreadExecutor();
  	
   	//compute only important molecules
  	final Molecule[] moleculeArray = new Molecule[clusters.size()];
    for (int iter=0; iter<numThreads; iter++)
  	{
    	final int currCore = iter;
  		Runnable task = new Runnable()
			{					
				@Override
				public void run()
				{
			    for (int currIter=currCore; currIter<clusters.size(); currIter+=numThreads)
			    {
						ClusterableMolecule mol = clusters.get(currIter).getCenter();
						moleculeArray[currIter] = EnsembleStructureFiltering.this.metric.requiredSubObject(mol.getMolecule());
			    }
				}
			};
  	
    	//enqueue the task
			execSvc.execute(task);					
  	}
    
		//shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(365L, TimeUnit.DAYS);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new RefinementRuntimeException("Unable to finish all tasks.", e);
    }

  	if (parallel)
  		execSvc = Executors.newCachedThreadPool();
  	else
  		execSvc = Executors.newSingleThreadExecutor();

  	//store distance between important atoms
  	for (int iter=0; iter<numThreads; iter++)
  	{
     	final int currCore = iter;
      
     	Runnable task = new Runnable()
			{					
				@Override
				public void run()
				{
			    for (int fromIter=currCore; fromIter<clusters.size(); fromIter+=numThreads)
			    {
						ClusterableMolecule mol = clusters.get(fromIter).getCenter();
		    		for (int toIter=fromIter+1; toIter<clusters.size(); toIter++)
		    		{
		    			mol.setDistance(clusters.get(toIter).getCenter(), 
		    					EnsembleStructureFiltering.this.metric.distance(moleculeArray[fromIter], moleculeArray[toIter]));
		    		}
			    }
				}
			};

			//enqueue the task
			execSvc.execute(task);					
		}
  	
	  //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(365L, TimeUnit.DAYS);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new RefinementRuntimeException("Unable to finish all tasks.", e);
    }		
	
   	HierarchicalSolution<ClusterableMolecule> resultClusters = this.hierarchicalCluster.cluster(clusters, rmsdCutoff);
  	
  	//get the solution at the proper cutoff point
  	ClusteringSolution<ClusterableMolecule> solution = resultClusters.getSolution(rmsdCutoff);
  	
  	return solution;
  }
}
