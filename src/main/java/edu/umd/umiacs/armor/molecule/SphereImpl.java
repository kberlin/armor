/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;

public final class SphereImpl implements Sphere
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -504586323493669769L;

	private static final double SPHERE_MAX_POSITION_ERROR = 1.0e-8;

	/** The position. */
  private final Point3d position;

	/** The radius. */
	private final double radius;
	
	public SphereImpl(Point3d p, double radius)
	{
		this.position = p;
		this.radius = radius;
	}

	@Override
	public boolean isSamePosition(Sphere a)
	{
	   if (distanceToCenterSquared(a)<=SPHERE_MAX_POSITION_ERROR*SPHERE_MAX_POSITION_ERROR)
	      return true;
	    else
	      return false;
	}

	@Override
	public Point3d getPosition()
	{
		return this.position;
	}

	@Override
	public double distanceToCenter(Sphere a)
	{
    return BasicMath.sqrt(distanceToCenterSquared(a));
	}

	@Override
	public double distanceToCenter(Point3d p)
	{
    return this.position.distance(p);
	}

	@Override
	public double distanceToCenterSquared(Sphere a)
	{
    return this.position.distanceSquared(a.getPosition());
	}

	@Override
	public double distanceToCenterSquared(Point3d p)
	{
    return this.position.distanceSquared(p);
	}

	@Override
	public double distanceToSurface(double x, double y, double z)
	{
		return distanceToSurface(new Point3d(x,y,z));
	}

	@Override
	public double distanceToSurface(Point3d p)
	{
  	return distanceToCenter(p)-radius();
	}

	@Override
	public RectangleBounds getSurfaceBounds()
	{
  	double rad = radius();
  	
  	return new RectangleBounds(this.position.subtract(rad),this.position.add(rad));
	}
	
	@Override
	public double radius()
	{
  	return this.radius;
	}

	@Override
	public double getX()
	{
		return this.position.x;
	}

	@Override
	public double getY()
	{
		return this.position.y;
	}

	@Override
	public double getZ()
	{
		return this.position.z;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "[Position = ["+getX()+", "+getY()+", "+getZ()+"], Radius="+this.radius+"]";
	}
}
