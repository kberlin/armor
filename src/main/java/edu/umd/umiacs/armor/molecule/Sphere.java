/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.Point3d;

public interface Sphere extends Serializable
{
	public boolean isSamePosition(Sphere a);
	
	/**
	 * Gets the position.
	 * 
	 * @return the position
	 */
	public Point3d getPosition();
	
	/**
	 * Distance to center.
	 * 
	 * @param a
	 *          the a
	 * @return the double
	 */
	public double distanceToCenter(Sphere a);

	/**
	 * Distance to center.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
	public double distanceToCenter(Point3d p);

	/**
	 * Distance to center squared.
	 * 
	 * @param a
	 *          the a
	 * @return the double
	 */
	public double distanceToCenterSquared(Sphere a);

	/**
	 * Distance to center squared.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
	public double distanceToCenterSquared(Point3d p);

	public double distanceToSurface(double x, double y, double z);
	
	public RectangleBounds getSurfaceBounds();

	/**
	 * Distance to surface.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
	public double distanceToSurface(Point3d p);

	/**
	 * Surface radius.
	 * 
	 * @return the double
	 */
	public double radius();
	
	/**
	 * Gets the x.
	 * 
	 * @return the x
	 */
	public double getX();

	/**
	 * Gets the y.
	 * 
	 * @return the y
	 */
	public double getY();

	/**
	 * Gets the z.
	 * 
	 * @return the z
	 */
	public double getZ();

}
