/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Class BasicMolecule.
 */
public final class BasicMolecule extends AbstractMolecule
{	
	/**
	 * The Class AddAtoms.
	 */
	private final class AddTreeAtoms implements Runnable
	{
		
		/** The atom list. */
		private final List<? extends Atom> atomsToAdd;
		
		/** The molecule. */
		private final BasicMolecule molecule;

		/** The num threads. */
		private int numThreads;
		
		private final List<Exception> pdbExceptionList;

		/** The thread number. */
		private int threadNumber;
		
		private AddTreeAtoms(BasicMolecule molecule, int threadNumber, int numThreads, List<? extends Atom> atomsToAdd, List<Exception> pdbExceptionList)
		{
			this.molecule = molecule;
			this.threadNumber = threadNumber;
			this.numThreads = numThreads;
			this.atomsToAdd = atomsToAdd;
			this.pdbExceptionList = pdbExceptionList;
		}

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			for (int iter=this.threadNumber; iter<this.atomsToAdd.size(); iter+=this.numThreads)
			{
				try
				{
					this.molecule.addTree(this.atomsToAdd.get(iter));
				} 
				catch (Exception e)
				{
					this.pdbExceptionList.add(e);
					break;
				}
			}
		}
		
	}

	private final List<Atom> atomsArrayList;
	
	private final Map<Atom,Atom> atomsHashMap;

	/** The position tree. */
	private final OcTree<Atom> positionTree;
	
	/** The Constant MOLECULE_BORDER_SIZE. */
	private static final double MOLECULE_BORDER_SIZE = 0.01; //Angstrom
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6596513396057029102L;

	/**
	 * Gets the bounds.
	 * 
	 * @param atomList
	 *          the atom list
	 * @param layerThickness
	 *          the layer thickness
	 * @return the bounds
	 */
	protected static RectangleBounds getBounds(Collection<? extends Atom> atomList, double layerThickness)
	{
		double minX = Double.POSITIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double minZ = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;
		double maxZ = Double.NEGATIVE_INFINITY;

		// store the atom information
		for (Atom a : atomList)
		{
			minX = Math.min(a.getX(), minX);
			minY = Math.min(a.getY(), minY);
			minZ = Math.min(a.getZ(), minZ);
			maxX = Math.max(a.getX(), maxX);
			maxY = Math.max(a.getY(), maxY);
			maxZ = Math.max(a.getZ(), maxZ);
		}
		
	  return new RectangleBounds(minX-MOLECULE_BORDER_SIZE-layerThickness,
	  													 minY-MOLECULE_BORDER_SIZE-layerThickness,
	  													 minZ-MOLECULE_BORDER_SIZE-layerThickness,
	  													 maxX+MOLECULE_BORDER_SIZE+layerThickness,
	  													 maxY+MOLECULE_BORDER_SIZE+layerThickness,
	  													 maxZ+MOLECULE_BORDER_SIZE+layerThickness);
	}

	/**
	 * Instantiates a new basic molecule.
	 * 
	 * @param atomList
	 *          the atom list
	 * @throws OcException
	 *           the oc exception
	 */
	public BasicMolecule(List<? extends Atom> atomList) throws DuplicateAtomException
	{
		if (atomList==null)
			throw new NullPointerException("Atom list cannot be null.");
		
		this.positionTree = new OcTree<Atom>(getBounds(atomList,0.0));
		this.atomsArrayList = Collections.synchronizedList(new ArrayList<Atom>(atomList.size()));
		this.atomsHashMap = Collections.synchronizedMap(new HashMap<Atom,Atom>(atomList.size()));
		
		add(atomList);
	}
	
	/**
	 * Instantiates a new basic molecule.
	 * 
	 * @param bounds
	 *          the bounds
	 */
	protected BasicMolecule(RectangleBounds bounds)
	{
		//generate two storage structure for positions
		this.positionTree = new OcTree<Atom>(bounds);		
		this.atomsArrayList = Collections.synchronizedList(new ArrayList<Atom>(1024));
		this.atomsHashMap = Collections.synchronizedMap(new HashMap<Atom,Atom>(1024));
	}

	/**
	 * Adds the.
	 * 
	 * @param a
	 *          the a
	 * @throws OcException
	 *           the oc exception
	 */
	protected void add(Atom a) throws DuplicateAtomException
	{
		Atom prevAtom = this.atomsHashMap.put(a, a);
		
		if (prevAtom!=null)
		{
			throw new DuplicateAtomException("Cannot add duplicate atom to molecule: "+a, a);
		}
		
		this.positionTree.insert(a);
		this.atomsArrayList.add(a);
	}
	
	protected void add(List<? extends Atom> atomList) throws DuplicateAtomException
	{
    if (atomList.size()<1000)
    {
	    //first add normally to build up the tree
			for (Atom a : atomList)
			{
				try
				{
					add(a);
				}
				catch (OcException e)
				{
					throw new MoleculeRuntimeException("Could not add atom to molecule", e);
				}
			}
    }
    else
		{
    	//add all atoms in order to the list
  		this.atomsArrayList.addAll(atomList);
    	
	    int numThreads = Runtime.getRuntime().availableProcessors();
	    
	    //create exception list
	    List<Exception> exceptionList = Collections.synchronizedList(new ArrayList<Exception>());

	    //create a thread pool
	    ExecutorService execSvc = Executors.newFixedThreadPool(numThreads);
	
	    //start each thread with own offset
	    for (int iter=0; iter<numThreads; iter++)
	    {
	    	AddTreeAtoms addThread = new AddTreeAtoms(this, iter, numThreads, atomList, exceptionList);

	    	execSvc.execute(addThread);
	    }
	
	    //shutdown the service
	    execSvc.shutdown();
	    try
			{
				execSvc.awaitTermination(5L, TimeUnit.MINUTES);
			} 
	    catch (InterruptedException e) 
	    {
	    	execSvc.shutdownNow();
	    	throw new AssertionError("Unable to finish all tasks.");
	    }
	    
	    //rethrow the error
	    if (exceptionList.size()>0)
	    {
	    	try
				{
					throw exceptionList.get(0);
				}
	    	catch (DuplicateAtomException e)
	    	{
	    		throw e;
	    	}
	    	catch (Exception e)
	    	{
	    		throw new MoleculeRuntimeException(e);
	    	}
	    }
		}
    		    
    assert this.positionTree.size()==this.atomsArrayList.size() && this.atomsHashMap.size()==this.atomsArrayList.size() : "Tree size="+this.positionTree.size()+" List size="+this.atomsArrayList.size();
	}
	
	protected boolean addNonColliding(Atom a) throws DuplicateAtomException
	{
		if (!isColliding(a))
		{
			add(a);
			return true;
		}
		
		return false;
	}

	protected void addTree(Atom a) throws DuplicateAtomException
	{
		Atom prevAtom = this.atomsHashMap.put(a, a);
		
		if (prevAtom!=null)
		{
			throw new DuplicateAtomException("Cannot add duplicate atom to molecule: "+a, a);
		}
		
		this.positionTree.insert(a);
	}
	
	/**
	 * Atom distance.
	 * 
	 * @param atomIndex1
	 *          the atom index1
	 * @param atomIndex2
	 *          the atom index2
	 * @return the double
	 */
	public double atomDistance(int atomIndex1, int atomIndex2)
	{
		Atom a1 = this.atomsArrayList.get(atomIndex1);
		Atom a2 = this.atomsArrayList.get(atomIndex2);
		
		return a1.distanceToCenter(a2);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#center()
	 */
	@Override
	public Point3d center()
  {
  	return this.positionTree.getSurfaceBounds().center();
  }
  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#distanceToCore(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public double distanceToCore(Point3d p)
	{
		return this.positionTree.getClosestSphere(p).distanceToSurface(p);
	}

  /* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.molecule.MoleculeInterface#exists(edu.umd.umiacs.fushmanlab.molecule.Atom)
	 */
	@Override
	public boolean exists(Atom a)
	{
		return this.atomsHashMap.containsKey(a);
	}

  @Override
	public final Atom getAtom(int index)
	{
		return this.atomsArrayList.get(index);
	}
	
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public Point3d getAtomPosition(Atom a) throws AtomNotFoundException
	{
		Atom currAtom = this.atomsHashMap.get(a);
		
		if (currAtom==null)
			throw new AtomNotFoundException("Atom not found: "+a.toString(), a);
		
		return this.atomsHashMap.get(a).getPosition();
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.molecule.AtomicStructure#getAtomPositionX(int)
	 */
	@Override
	public Point3d getAtomPosition(int index)
	{
		return this.atomsArrayList.get(index).getPosition();
	}
	
	@Override
	public ArrayList<Atom> getAtoms()
	{
		ArrayList<Atom> newAtoms = new ArrayList<Atom>(this.atomsArrayList);
		
		return newAtoms;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomSurfaceRadius(int)
	 */
	@Override
	public double getAtomSurfaceRadius(int index)
	{
		return this.atomsArrayList.get(index).radius();
	}
		
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomType(int)
	 */
	@Override
	public AtomType getAtomType(int index)
	{
		return this.atomsArrayList.get(index).getType();
	}

	/* (non-Javadoc)
   * @see edu.umd.umiacs.armor.molecule.Molecule#getBounds()
   */
  @Override
	public RectangleBounds getBounds()
  {
  	return this.positionTree.getBounds();
  }

	@Override
	public List<? extends Atom> getCloseAtoms(Point3d p, double maxDistance)
	{
		return this.positionTree.getNearbySphere(p, maxDistance);
	}

	@Override
	public ArrayList<Atom> getClosestAtoms(Point3d p, int n)
	{
		if (n<0)
			throw new MoleculeRuntimeException("Number of atoms requested must be greater than 0.");
		
		if (n==1)
		{
			Atom a = this.positionTree.getClosestSphere(p);
			ArrayList<Atom> list = new ArrayList<Atom>(n);
			list.add(a);
			
			return list;
		}
		
		return this.positionTree.getClosestSphere(p, n);
	}

	/* (non-Javadoc)
   * @see edu.umd.umiacs.armor.molecule.Molecule#getSurfaceBounds()
   */
  @Override
	public RectangleBounds getSurfaceBounds()
  {
  	return this.positionTree.getSurfaceBounds();
  }

	/* (non-Javadoc)
   * @see edu.umd.umiacs.armor.molecule.Molecule#isCloserThan(edu.umd.umiacs.armor.math.Point3D, double)
   */
  @Override
	public boolean isCloserThan(Point3d p, double distance)
	{
		return this.positionTree.isCloserThan(p,distance);
	}

	/**
	 * Checks if is colliding.
	 * 
	 * @param a
	 *          the a
	 * @return true, if is colliding
	 */
  public boolean isColliding(Atom a)
	{
		return this.positionTree.isColliding(a);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isContrastAtom(int)
	 */
	@Override
	public boolean isDisplacedAtom(int index)
	{
		return false;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isSolventAtom(int)
	 */
	@Override
	public boolean isSolventAtom(int index)
	{
		return false;
	}

	@Override
	public Iterator<Atom> iterator()
	{
		return Collections.unmodifiableCollection(this.atomsArrayList).iterator();
	}


	/* (non-Javadoc)
	 * @see edu.umd.umiacs.fushmanlab.molecule.MoleculeInterface#maxAtomDistance()
	 */
  @Override
	public double maxAtomDistance()
	{
		double maxDistance = 0.0;		
		Point3d c = getSurfaceBounds().center();
		
		for (Atom a : this.atomsArrayList)
		{
			maxDistance = Math.max(maxDistance, c.distanceSquared(a.getPosition()));
		}
			
		
		return BasicMath.sqrt(maxDistance)*2.0;	
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#size()
	 */
	@Override
	public final int size()
	{
		return this.atomsArrayList.size();
	}
}
