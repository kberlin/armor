/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.util.concurrent.atomic.AtomicLong;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.util.HashCodeUtil;

/**
 * The Class AtomImpl.
 */
public final class AtomImpl implements Atom
{
	private final int hashCode;
	
	private final Sphere sphere;
	
  private final PrimaryStructure primaryStructure;
		
	private final SecondaryStructure secondaryStructure;
  
  /** The type. */
  private final AtomType type;
  
  /** The distance slack at which the two atoms are not colliding even though they intersect */
  public static final double ATOM_MAX_POSITION_ERROR = .01;
  
  private static String EMPTY_CHAIN_ID = "X";
	
	private static AtomGroupType EMPTY_GROUP_TYPE = HetGroupType.UKW; 

	/** distance at which two atoms are considered in location */
	public static final double HYDROGEN_EXCHANGE_CHANCE = 0.10;
	/**
	 * 
	 */
	private static final long serialVersionUID = -3471690204560264512L;
	private static AtomicLong UniqueAtomId = new AtomicLong(System.currentTimeMillis());
	  
  private static int computeHashCode(AtomType type, PrimaryStructure primaryStructure)
  {
    int result = HashCodeUtil.SEED;
    
    //collect the contributions of various fields
    result = HashCodeUtil.hash(result, type);
    result = HashCodeUtil.hash(result, primaryStructure);

    return result;
  }
  
	public static double surfaceRadius(AtomType currType)
	{
		return currType.getVanDerWaalsRadius();
	}
  
  public AtomImpl(Point3d position, AtomType type)
	{  	
  	this (position, type, new PrimaryStructure(type.getName(), EMPTY_CHAIN_ID, EMPTY_GROUP_TYPE, 0.0, UniqueAtomId.incrementAndGet()), null, -1.0);
	}
  
  private AtomImpl(Point3d position, AtomType type, PrimaryStructure primaryStructure, SecondaryStructure secondaryStructure, double radius)
  {
  	if (position==null)
  		throw new MoleculeRuntimeException("Position cannot be null");
  	if (type==null)
  		throw new MoleculeRuntimeException("AtomType cannot be null");
  	
    this.type = type;
    if (radius<0.0)
    	this.sphere = new SphereImpl(position, this.type.getVanDerWaalsRadius());
    else
    	this.sphere = new SphereImpl(position, radius);

  	this.primaryStructure = primaryStructure;
  	this.secondaryStructure = secondaryStructure;
  	
  	//compute the hashcode
  	this.hashCode = computeHashCode(this.type, primaryStructure);	
  }
  
  private AtomImpl(AtomImpl a, Point3d p)
  {
    this.primaryStructure = a.primaryStructure;
    this.secondaryStructure = a.secondaryStructure;
    this.sphere = new SphereImpl(p, a.sphere.radius());
    this.type = a.type;
    this.hashCode = a.hashCode;
  }
  
  public AtomImpl(Point3d position, PrimaryStructure primaryStructure)
	{
  	this(position, primaryStructure, null, -1);
	}
  
  public AtomImpl(Point3d position, PrimaryStructure primaryStructure, SecondaryStructure secondaryStructure)
	{
  	this(position, primaryStructure, secondaryStructure, -1);
	}
  
  public AtomImpl(Point3d position, PrimaryStructure primaryStructure, SecondaryStructure secondaryStructure, double radius)
	{
   	this(position, AtomType.getAtomTypeFromFull(primaryStructure.getAtomName()), primaryStructure, secondaryStructure, radius);
	}
  	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#createAtom(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public AtomImpl createWithNewPosition(Point3d p)
	{
		AtomImpl atom = new AtomImpl(this, p);
		
		return atom;
	}
  
  /* (non-Javadoc)
   * @see edu.umd.umiacs.armor.molecule.Atom#deuterate()
   */
  @Override
	public AtomImpl deuterate()
  {
  	AtomImpl returnAtom = this;
  	
		if (getType() == AtomType.HYDROGEN)
		{
			returnAtom = new AtomImpl(getPosition(), getPrimaryInfo().changeType(AtomType.DEUTERIUM), getSecondaryInfo());
		}
  	else
  	if (isExchangedHydrogen())
			returnAtom = new AtomImpl(getPosition(), getPrimaryInfo().changeType(AtomType.HYDROGEN), getSecondaryInfo());
  	
  	return returnAtom;
  }
  
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#distanceToCenter(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public double distanceToCenter(Sphere a)
	{
    return this.sphere.distanceToCenter(a);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#distanceToCenter(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public double distanceToCenter(Point3d p)
	{
    return this.sphere.distanceToCenter(p);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#distanceToCenterSquared(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public double distanceToCenterSquared(Sphere a)
	{
    return this.sphere.distanceToCenterSquared(a);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#distanceToCenterSquared(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public double distanceToCenterSquared(Point3d p)
	{
    return this.sphere.distanceToCenterSquared(p);
	}

	@Override
	public double distanceToSurface(double x, double y, double z)
	{
		return this.sphere.distanceToSurface(new Point3d(x,y,z));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#distanceToSurface(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public double distanceToSurface(Point3d p)
	{
  	return this.sphere.distanceToSurface(p);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if(obj == this)
      return true;
		
		if(obj == null || obj.getClass() != this.getClass())
      return false;
		
		AtomImpl atom = (AtomImpl)obj;
		
		if (this.hashCode!=atom.hashCode)
			return false;
		
		if (this.type != atom.type)
			return false;
		
		if (this.primaryStructure == null)
		{
			if (atom.primaryStructure != null)
				return false;
		}
		else 
		if (!this.primaryStructure.equals(atom.primaryStructure))
			return false;
		
	  return true;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#getPosition()
	 */
	@Override
	public Point3d getPosition()
	{
		return this.sphere.getPosition();
	}
	
	@Override
	public PrimaryStructure getPrimaryInfo()
	{
		return this.primaryStructure;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#getSSType()
	 */
	@Override
	public SecondaryStructure getSecondaryInfo()
	{
		return this.secondaryStructure;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#getSurfaceBounds()
	 */
	@Override
	public RectangleBounds getSurfaceBounds()
	{
  	return this.sphere.getSurfaceBounds();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#getType()
	 */
	@Override
	public AtomType getType()
	{
		return this.type;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#getVanDerWaalsRadius()
	 */
	@Override
	public double getVanDerWaalsRadius()
	{
		return this.sphere.radius();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#getX()
	 */
	@Override
	public double getX()
	{
		return this.sphere.getX();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#getY()
	 */
	@Override
	public double getY()
	{
		return this.sphere.getY();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#getZ()
	 */
	@Override
	public double getZ()
	{
		return this.sphere.getZ();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{		
    return this.hashCode;
	}
	
	public boolean isExchangedHydrogen()
	{
		return (isHydrogen() && this.primaryStructure!=null && this.primaryStructure.isExchangedHydrogen());
	}

	public boolean isHydrogen()
	{
  	if (this.type == AtomType.HYDROGEN || this.type == AtomType.DEUTERIUM)
  		return true;
  	
  	return false;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#isSamePosition(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public boolean isSamePosition(Sphere a)
	{
	   if (distanceToCenterSquared(a)<=ATOM_MAX_POSITION_ERROR*ATOM_MAX_POSITION_ERROR)
	      return true;
	    else
	      return false;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Atom#surfaceRadius()
	 */
	@Override
	public double radius()
	{
  	return this.sphere.radius();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return "[Type="+getType().getName()+", Position = ["+getX()+", "+getY()+", "+getZ()+"], Radius="+radius()+", PrimaryInfo="+this.primaryStructure+"]";
	}
}
