package edu.umd.umiacs.armor.molecule;

public class MoleculeException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8837603897171453402L;

	public MoleculeException()
	{
	}

	public MoleculeException(String message)
	{
		super(message);
	}

	public MoleculeException(Throwable cause)
	{
		super(cause);
	}

	public MoleculeException(String message, Throwable cause)
	{
		super(message, cause);
	}

}
