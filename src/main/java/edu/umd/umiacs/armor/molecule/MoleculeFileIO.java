/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.biojava.bio.structure.Structure;
import org.biojava.bio.structure.io.FileParsingParameters;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.linear.EigenDecomposition3d;
import edu.umd.umiacs.armor.physics.BasicPhysics;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.FileIO;

public class MoleculeFileIO
{
	
	public static BasicMolecule readHetAtoms(File file) throws IOException, DuplicateAtomException
	{
		BufferedReader reader = new BufferedReader(new FileReader(file));
		ArrayList<Atom> atomList = new ArrayList<Atom>();
		
		String line = reader.readLine();
		int count = 0;
		while (line!=null)
		{
			if (line.substring(1-1, 6).equals("HETATM"))
			{
				String name = line.substring(13-1, 16).trim();
				int residue = Integer.parseInt(line.substring(23-1,26).trim());
				String chainID = line.substring(22-1, 22).trim();
				String secondStructShortName = line.substring(18-1, 20).trim();
				double tempFactor = Double.parseDouble(line.substring(61-1, 72).trim());
				
				double x = Double.parseDouble(line.substring(31-1,38).trim());
				double y = Double.parseDouble(line.substring(39-1,46).trim());
				double z = Double.parseDouble(line.substring(47-1,54).trim());
				
				count++;
				PrimaryStructure p = new PrimaryStructure(residue, name, chainID, secondStructShortName, tempFactor, count);
				
				Atom a = new AtomImpl(new Point3d(x,y,z), p);
				atomList.add(a);
			}
			
			line = reader.readLine();
		}
		
		reader.close();
		
		if (atomList.isEmpty())
			return null;
		
		BasicMolecule mol = new BasicMolecule(atomList);
		
		return mol;
	}
	
	public static BasicMolecule readHetAtoms(String file) throws IOException, DuplicateAtomException
	{
		return readHetAtoms(new File(file));
	}
	
	public static BasicMolecule readTextFile(File file) throws DuplicateAtomException, PdbException
	{
		Scanner scanner = null;
		
		try
		{
			scanner = BasicUtils.getFileScanner(file);
			
			ArrayList<Atom> atomList = new ArrayList<Atom>();
			
	    Pattern word = Pattern.compile("[\\w']+");

	    int counter = 0;
			while (scanner.hasNextLine())
			{
				//mark the line number
				counter++;
				
				//get the next line
				String nextLine = scanner.nextLine();
				Scanner lineScanner = new Scanner(nextLine);
				
				//get rid of potential trash due to invalid conversion
				try
				{
					String atomName = lineScanner.next(word);
					String chainID = lineScanner.next(word);
					int residue = lineScanner.nextInt();
					double x = lineScanner.nextDouble();
					double y = lineScanner.nextDouble();
					double z = lineScanner.nextDouble();
					double radius = lineScanner.nextDouble();
					
					if (radius<0.0)
						throw new MoleculeException("Atom radius cannot be " + radius +".");
					
					PrimaryStructure pr = new PrimaryStructure(residue, atomName, chainID, "Xaa");
					
					atomList.add(new AtomImpl(new Point3d(x,y,z), pr, null, radius));
				}
				catch (Exception e)
				{
					throw new PdbException("Could not parse molecule input file, line "+counter+":\n  \""+nextLine+"\"", e);
				}
				finally
				{
					lineScanner.close();
				}
			}
			
			return new BasicMolecule(atomList);
		}
    catch (IOException e)
    {
    	throw new PdbException("Could not read molecule file "+file.getPath()+".");
    }
		finally
		{
			scanner.close();
		}
	}
	
	public static PdbStructure readPDB(File file) throws PdbException
	{
    try
		{    
	    // configure the parameters of file parsing
	    FileParsingParameters params = new FileParsingParameters();
	
	    // should the ATOM and SEQRES residues be aligned when creating the internal data model?
	    params.setAlignSeqRes(true);
	
	    // should secondary structure get parsed from the file
	    params.setParseSecStruc(true);
	    params.setParseCAOnly(false);
	    params.setAtomCaThreshold(Integer.MAX_VALUE);
	    params.setLoadChemCompInfo(false);
	    params.setAlignSeqRes(false);
	    
	    PdbFileParserModified fileParser = new PdbFileParserModified();
	    fileParser.setFileParsingParameters(params);
	    BufferedReader reader = new BufferedReader(new FileReader(file));
	    
	    Structure structure;
	    //ByteArrayOutputStream pipeOut = new ByteArrayOutputStream();
	    //PrintStream oldErrorStream = System.err;
	    //synchronized(System.err)
	    {	    	
	    	//System.setErr(new PrintStream(pipeOut));
	    	structure = fileParser.parsePDBFile(reader);
	    	//System.setErr(oldErrorStream);
	    }

			if (structure.size()==0)
				throw new PdbException("Could not parse PDB file "+file.getPath()+".");
			
			//return new PdbStructure(structure, new String(pipeOut.toByteArray()));
			return new PdbStructure(structure, null);
		}
    catch (IOException e)
    {
    	throw new PdbException("Could not read PDB file "+file.getPath()+".", e);
    }
 	}
	
	
	/**
	 * Read pdb.
	 * 
	 * @param file
	 *          the file
	 * @return the pdb structure
	 * @throws PdbException
	 *           the pdb exception
	 */
	public static PdbStructure readPDB(String file) throws PdbException
	{
		return readPDB(new File(file));
	}    
	
	/**
	 * Write pdb.
	 * 
	 * @param file
	 *          the file
	 * @param content
	 *          the content
	 * @throws IOException
	 *           Signals that an I/O exception has occurred.
	 */
	public static void writePDB(File file, String content) throws IOException
	{
		Writer output = new BufferedWriter(new FileWriter(file));
    try
    {
      output.write(content);
    }
    finally 
    {
      output.close();
    }
	}
	
	public static void writePymolAxesScript(File file, Molecule mol, EigenDecomposition3d tensor, double lengthExtension) throws IOException
	{		
		StringBuilder content = new StringBuilder();
		
		//create virtual molecule
		VirtualMoleculeWrapper rotMol = new VirtualMoleculeWrapper(mol);
		
		//get tensor rotation
		Rotation rot = tensor.getRotation();

		//rotate it so that it is aligned to tensor
		rotMol = rotMol.createTransformedMolecule(new RigidTransform(rot.inverse(), null));
		
		double minx = Double.POSITIVE_INFINITY;
		double miny = Double.POSITIVE_INFINITY;
		double minz = Double.POSITIVE_INFINITY;
		double maxx = Double.NEGATIVE_INFINITY;
		double maxy = Double.NEGATIVE_INFINITY;
		double maxz = Double.NEGATIVE_INFINITY;
		for (int iter=0; iter<rotMol.size(); iter++)
		{
			Point3d p = rotMol.getAtomPosition(iter);
			minx = Math.min(p.x, minx);
			miny = Math.min(p.y, miny);
			minz = Math.min(p.z, minz);
			maxx = Math.max(p.x, maxx);
			maxy = Math.max(p.y, maxy);
			maxz = Math.max(p.z, maxz);
		}
		
		//Point3d p = new Point3d(0.5*(maxx-minx)+minx, 0.5*(maxy-miny)+miny, 0.5*(maxz-minz)+minz);
		Point3d p = BasicPhysics.moleculeCenterOfMass(rotMol);
		
		Point3d x0 = rot.mult(new Point3d(minx-lengthExtension, p.y, p.z));
		Point3d x1 = rot.mult(new Point3d(maxx+lengthExtension, p.y, p.z));
		Point3d y0 = rot.mult(new Point3d(p.x, miny-lengthExtension, p.z));
		Point3d y1 = rot.mult(new Point3d(p.x, maxy+lengthExtension, p.z));
		Point3d z0 = rot.mult(new Point3d(p.x, p.y, minz-lengthExtension));
		Point3d z1 = rot.mult(new Point3d(p.x, p.y, maxz+lengthExtension));
		
		//get tensor eigenvalues
		double[] d = tensor.getEigenvalues();
		
		content.append("# axes plot script\n");
		content.append("from pymol.cgo import *\n");
		content.append("from pymol import cmd\n");
		content.append("from pymol.vfont import plain\n");
		 
		content.append("#create the axes object, draw axes with cylinders colored red (Dx), green (Dy), blue (Dz)\n\n");
		 
		content.append("obj = [\n");
		double maxD = Math.max(Math.abs(d[0]),Math.max(Math.abs(d[1]), Math.abs(d[2])));
		if (Math.abs(d[1]-d[0])>1.0e-3*maxD)
			content.append(String.format("   CYLINDER, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, 0.2, 1.0, 1.0, 1.0, 1.0, 0.0, 0.,\n",
					x0.x, x0.y, x0.z,
					x1.x, x1.y, x1.z));
		if (Math.abs(d[1]-d[0])>1.0e-3*maxD && Math.abs(d[2]-d[1])>1.0e-3*maxD)
			content.append(String.format("   CYLINDER, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, 0.2, 1.0, 1.0, 1.0, 0., 1.0, 0.,\n",
					y0.x, y0.y, y0.z,
					y1.x, y1.y, y1.z));
		
		//always show at least this axis
		if (Math.abs(d[1]-d[0])<=1.0e-3*maxD || Math.abs(d[2]-d[1])>1.0e-3*maxD)
			content.append(String.format("   CYLINDER, %.2f, %.2f, %.2f, %.2f, %.2f, %.2f, 0.2, 1.0, 1.0, 1.0, 0., 0.0, 1.0,\n",
					z0.x, z0.y, z0.z,
					z1.x, z1.y, z1.z));
			content.append("   ]\n\n");
		 		 
		//content.append(String.format("cyl_text(obj,plain,[10.,0.,0.],'A',0.20,axes=[[3,0,0],[0,3,0],[0,0,3]])\n"));
		//content.append(String.format("cyl_text(obj,plain,[0.,10.,0.],'B',0.20,axes=[[3,0,0],[0,3,0],[0,0,3]])\n"));
		//content.append(String.format("cyl_text(obj,plain,[0.,0.,10.],'C',0.20,axes=[[3,0,0],[0,3,0],[0,0,3]])\n"));
		 
		content.append("# then we load it into PyMOL\n\n");
		content.append(String.format("cmd.load_cgo(obj,'%s')\n", FileIO.removeExtension(file.getName())));
		
		Writer output = new BufferedWriter(new FileWriter(file));
    try
    {
      output.write(content.toString());
    }
    finally 
    {
      output.close();
    }
	}
  
	/*
  public void writeBinaryFile(String fileName, int model) throws IOException
  {
  	//the output will be in big-endian
  	FileOutputStream outputFile = new FileOutputStream(fileName);
    DataOutputStream outputStream = new DataOutputStream(outputFile);

    ArrayList<PDBAtom> atomList = getAtoms(model);
    
    // Write the data to the file in an integer/double pair
    try
    {
    	outputStream.writeInt(1);
    	outputStream.writeInt(atomList.size());
	    for (int iter=0; iter < atomList.size(); iter++) 
	    {
	    	outputStream.writeFloat((float)atomList.get(iter).getX());
	    	outputStream.writeFloat((float)atomList.get(iter).getY());
	    	outputStream.writeFloat((float)atomList.get(iter).getZ());
	    	outputStream.writeInt(atomList.get(iter).getType().ordinal());
	    }
    }    
	  catch (IOException e)
	  {
	  	throw e;
	  }
    finally
    {
      outputFile.close();
    }
  }
  */
}