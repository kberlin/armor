/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.util.SortablePair;

/**
 * The Class OcNode.
 * 
 * @param <GenericSphere>
 *          the generic type
 */
public final class OcNode<GenericSphere extends Sphere> implements Serializable
{
	
	/** The atom list. */
	private ArrayList<GenericSphere> atomList;

	/** The bounds. */
  private final RectangleBounds bounds;
	
  /** The children. */
  private ArrayList<OcNode<GenericSphere>> children;
  
  /** The parent. */
  private OcNode<GenericSphere> parent;
  
  /** The surface bounds. */
  private RectangleBounds surfaceBounds;
  
  /** The Constant MAX_ATOMS_PER_NODE. */
  public final static int MAX_SPHERE_PER_NODE = 8;
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 2531954041605938304L;
  
  /**
	 * Instantiates a new oc node.
	 * 
	 * @param lx
	 *          the lx
	 * @param ly
	 *          the ly
	 * @param lz
	 *          the lz
	 * @param ux
	 *          the ux
	 * @param uy
	 *          the uy
	 * @param uz
	 *          the uz
	 */
  public OcNode(double lx, double ly, double lz, double ux, double uy, double uz)
  {
  	this.bounds = new RectangleBounds(lx,ly,lz,ux,uy,uz);
    initNode();
  }
  
  /**
	 * Instantiates a new oc node.
	 * 
	 * @param boundsIn
	 *          the bounds in
	 */
  public OcNode(RectangleBounds boundsIn)
  {
  	this.bounds = boundsIn;
  	initNode();
  }
  
  /**
	 * Adds the to node.
	 * 
	 * @param a
	 *          the a
	 * @throws OcException
	 *           the oc exception
	 */
  private void addToNode(GenericSphere a) throws OcException
  {
  	assert isLeaf();
  	
  	this.atomList.add(a);
  	
  	//record insertion into this node
		recordInsertion(a);

  	//propagate information up the the ancestors
		updateAncestors();
  }
 
  /**
	 * Contains same position atom.
	 * 
	 * @param a
	 *          the a
	 * @return true, if successful
	 */
  private boolean containsSamePositionSphere(Sphere a)
  {
  	assert isLeaf();
  	
  	if (this.atomList==null)
  		return false;
  	
    for (GenericSphere atom : this.atomList)
    	if (atom.isSamePosition(a))
    		return true;
  	
    return false;
  }
  
  /**
	 * Exits.
	 * 
	 * @param a
	 *          the a
	 * @return true, if successful
	 */
  public boolean exits(GenericSphere a)
  {
    OcNode<GenericSphere> currNode = this;
    
    while (!currNode.isLeaf())
    	currNode = currNode.getBoundingChild(a);
    
    //lock and see if still is leaf
    boolean isMember = false;
  	if (currNode.isLeaf())
  		isMember = currNode.isNodeMember(a);
  	else
  		isMember = currNode.exits(a);
    
    return isMember;
  }
  
  /**
	 * Generate children.
	 * 
	 * @return the array list
	 */
  private ArrayList<OcNode<GenericSphere>> generateChildren()
  {
    double lx,ly,lz,mx,my,mz,ux,uy,uz;
    
    lx = this.bounds.lx;
    ly = this.bounds.ly;
    lz = this.bounds.lz;
    ux = this.bounds.ux;
    uy = this.bounds.uy;
    uz = this.bounds.uz;
    mx = (ux+lx)/2.0;
    my = (uy+ly)/2.0;
    mz = (uz+lz)/2.0;
    
    //allocate the space for children
    ArrayList<OcNode<GenericSphere>> currChildren = new ArrayList<OcNode<GenericSphere>>(8);
    
    currChildren.add(0, new OcNode<GenericSphere>(lx,ly,lz,mx,my,mz));
    currChildren.add(1, new OcNode<GenericSphere>(lx,ly,mz,mx,my,uz));
    currChildren.add(2, new OcNode<GenericSphere>(lx,my,lz,mx,uy,mz));
    currChildren.add(3, new OcNode<GenericSphere>(lx,my,mz,mx,uy,uz));
    currChildren.add(4, new OcNode<GenericSphere>(mx,ly,lz,ux,my,mz));
    currChildren.add(5, new OcNode<GenericSphere>(mx,ly,mz,ux,my,uz));
    currChildren.add(6, new OcNode<GenericSphere>(mx,my,lz,ux,uy,mz)); 
    currChildren.add(7, new OcNode<GenericSphere>(mx,my,mz,ux,uy,uz));
    
    //assign yourself as parent
    for (OcNode<GenericSphere> child : currChildren)
    	child.parent = this;
    
    return currChildren;
  }
  
  /**
	 * Gets the atom closer than.
	 * 
	 * @param p
	 *          the p
	 * @param distance
	 *          the distance
	 * @return the atom closer than
	 */
  public GenericSphere getSphereCloserThan(Point3d p, double distance)
	{
		double distSquared = distance*distance;
		GenericSphere colliding = null;
		
		if (!isLeaf())
		{
			for (OcNode<GenericSphere> child : this.children)
			{
				//if potentially colliding check the children
				if (child.surfaceBounds.distanceToPointSquared(p)<=distSquared)
				{
					colliding = child.getSphereCloserThan(p, distance);
					if (colliding != null)
						break;
				}
			}
		}
		else
  		colliding = getCurrentNodeAtomCloserThan(p,distance);

		return colliding;
	}
  
  /**
	 * Gets the bounding child.
	 * 
	 * @param a
	 *          the a
	 * @return the bounding child
	 */
  private OcNode<GenericSphere> getBoundingChild(GenericSphere a)
  {
  	//cannot find children in a leaf node
    assert !isLeaf();
    
    Point3d p = a.getPosition();
    for (OcNode<GenericSphere> child : this.children)
    {
      if (child.bounds.isBounding(p))
        return child;
    }

    throw new AssertionError("Node does not bound the GenericAtom.");
  }
  
  /**
	 * Gets the bounds.
	 * 
	 * @return the bounds
	 */
  public RectangleBounds getBounds()
  {
  	return this.bounds;
  }

  
  /**
	 * Gets the closest atom.
	 * 
	 * @param a
	 *          the a
	 * @return the closest atom
	 */
  public GenericSphere getClosestSphere(Sphere a)
  {
  	return getClosestAtom(a.getPosition());
  }
  
  /**
	 * Gets the closest atom.
	 * 
	 * @param p
	 *          the p
	 * @return the closest atom
	 */
  public GenericSphere getClosestAtom(Point3d p)
  {
  	GenericSphere closestAtom = null;
		double bestDist = Double.POSITIVE_INFINITY;
		double bestDist2 = bestDist;
		
		if (!isLeaf())
		{
			for (OcNode<GenericSphere> child : this.children)
			{				
				//if potentially closer than best distance check the children
				double boundDistance2 = child.surfaceBounds.distanceToPointSquared(p);
				if (boundDistance2<=0.0 || boundDistance2<bestDist2)
				{
					GenericSphere currAtom = child.getClosestAtom(p);
					if (currAtom != null)
					{
						double currDist = currAtom.distanceToSurface(p);
						if (currDist<bestDist)
						{
							bestDist = currDist;
							bestDist2 = (bestDist<=0.0) ? 0.0 : bestDist*bestDist;
							closestAtom = currAtom;
						}
					}
				}
			}
		}
		else
  		closestAtom = getCurrentNodeClosestAtom(p);

		return closestAtom;
  }
  
	public void getClosestAtoms(Point3d p, int n, final TreeSet<SortablePair<Double,GenericSphere>> closestAtoms)
	{
		if (!isLeaf())
		{
			for (OcNode<GenericSphere> child : this.children)
			{				
				double bestDist = Double.POSITIVE_INFINITY;
				if (closestAtoms.size()==n)
					bestDist = Math.max(0.0, closestAtoms.last().x);
				
				//if potentially closer than best distance check the children
				double boundDistance2 = child.surfaceBounds.distanceToPointSquared(p);
				if (boundDistance2<=0.0 || boundDistance2<bestDist*bestDist)
				{
					child.getClosestAtoms(p,n,closestAtoms);
				}
			}
		}
		else
  		getCurrentNodeClosestAtoms(p,n,closestAtoms);
	}
  
	public GenericSphere getCollidingSphere(GenericSphere a)
	{
		return getSphereCloserThan(a.getPosition(), a.radius());
	}
  
  private GenericSphere getCurrentNodeAtomCloserThan(Point3d p, double distance)
	{
  	GenericSphere GenericAtom = null;
  	
		assert isLeaf();
		
		for (GenericSphere currAtom : this.atomList)
		{
			double dist = distance+currAtom.radius();
			
			if (currAtom.distanceToCenterSquared(p) <= dist*dist)
			{
				GenericAtom = currAtom;
				break;
			}
		}
		
		return GenericAtom;
	}
  
  private GenericSphere getCurrentNodeClosestAtom(Point3d p)
	{
  	GenericSphere GenericAtom = null;
		double bestDist = Double.POSITIVE_INFINITY;
  	
		assert isLeaf();
		
		for (GenericSphere currAtom : this.atomList)
		{
			double currDist = currAtom.distanceToSurface(p);
			if (currDist<bestDist)
			{
				bestDist = currDist;
				GenericAtom = currAtom;
			}
		}
		
		return GenericAtom;
	}
  

  private void getCurrentNodeClosestAtoms(Point3d p, int n, final TreeSet<SortablePair<Double,GenericSphere>> closestAtoms)
	{
		assert isLeaf();
		
		for (GenericSphere currAtom : this.atomList)
		{
			double bestDist = Double.POSITIVE_INFINITY;
			if (closestAtoms.size()==n)
				bestDist = closestAtoms.last().x;

			double currDist = currAtom.distanceToSurface(p);
			if (currDist<bestDist)
			{
				closestAtoms.add(new SortablePair<Double,GenericSphere>(currDist, currAtom));
				if (closestAtoms.size()>n)
					closestAtoms.remove(closestAtoms.last());
			}
		}
	}
  
  private void getCurrentNodeNearbyAtoms(Point3d p, double dist, final List<GenericSphere> closestAtoms)
	{
		assert isLeaf();
		
		for (GenericSphere currAtom : this.atomList)
		{
			double currDist = currAtom.distanceToSurface(p);
			if (currDist<=dist)
			{
				closestAtoms.add(currAtom);
			}
		}
	}
  
  public void getNearbySpheres(Point3d p, double dist, final List<GenericSphere> closestAtoms)
	{
		if (!isLeaf())
		{
			double dist2 = dist*dist;
			for (OcNode<GenericSphere> child : this.children)
			{				
				//if potentially closer than best distance check the children
				double boundDistance2 = child.surfaceBounds.distanceToPointSquared(p);
				if (boundDistance2<=dist2)
				{
					child.getNearbySpheres(p,dist,closestAtoms);
				}
			}
		}
		else
  		getCurrentNodeNearbyAtoms(p,dist,closestAtoms);
	}
  
  /**
	 * Gets the parent.
	 * 
	 * @return the parent
	 */
  public OcNode<GenericSphere> getParent()
  {
  	return this.parent;
  }
  
  /**
	 * Gets the surface bounds.
	 * 
	 * @return the surface bounds
	 */
  public RectangleBounds getSurfaceBounds()
  {
  	return this.surfaceBounds;
  }
  
  /**
	 * Inits the node.
	 */
  private void initNode()
  {
    this.atomList = new ArrayList<GenericSphere>(MAX_SPHERE_PER_NODE);
    this.children = null;
    this.parent = null;
    
    this.surfaceBounds = new RectangleBounds(Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY,Double.POSITIVE_INFINITY,Double.NEGATIVE_INFINITY,Double.NEGATIVE_INFINITY,Double.NEGATIVE_INFINITY);
  }
  
  /**
	 * Insert.
	 * 
	 * @param a
	 *          the a
	 * @throws OcException
	 *           the oc exception
	 */
  public void insert(GenericSphere a) throws OcException
  {
  	if (!this.isBounding(a))
	  	throw new OcException("Tree root does not bound inserted GenericAtom.");
  	    
    OcNode<GenericSphere> currNode = this;
	  while (true)
    {
		  synchronized(currNode)
		  {
	    	if (currNode.isLeaf())
	    	{
	    		currNode.insertIntoLeaf(a);
	    		break;
	    	}
		  }
		  
   		currNode = currNode.getBoundingChild(a);
    }
  }
  
  /**
	 * Insert into leaf.
	 * 
	 * @param a
	 *          the a
	 * @throws OcException
	 *           the oc exception
	 */
  private void insertIntoLeaf(GenericSphere a) throws OcException
  { 
  	assert isLeaf();
  	  	
    if (containsSamePositionSphere(a))
     	addToNode(a);
    else
    if (this.atomList.size()<MAX_SPHERE_PER_NODE)
    	addToNode(a);
    else
    {
    	//create a deeper level
    	splitNode();
    	insert(a);
    }
  }
  
  /**
	 * Checks if is bounding.
	 * 
	 * @param a
	 *          the a
	 * @return true, if is bounding
	 */
  private boolean isBounding(Sphere a)
  {
  	return this.bounds.isBounding(a.getX(),a.getY(),a.getZ());
  }
  
  /**
	 * Checks if is closer than.
	 * 
	 * @param a
	 *          the a
	 * @param distance
	 *          the distance
	 * @return true, if is closer than
	 */
  public boolean isCloserThan(Sphere a, double distance)
	{
		return isCloserThan(a.getPosition(),distance);
	}
  
  /**
	 * Checks if is closer than.
	 * 
	 * @param p
	 *          the p
	 * @param distance
	 *          the distance
	 * @return true, if is closer than
	 */
  public boolean isCloserThan(Point3d p, double distance)
	{
		if (getSphereCloserThan(p, distance)==null)
			return false;
		else
		{
			return true;
		}
	}
  
  /**
	 * Checks if is colliding.
	 * 
	 * @param a
	 *          the a
	 * @return true, if is colliding
	 */
  public boolean isColliding(Sphere a)
	{
  	return isCloserThan(a.getPosition(), a.radius());
	}
  
  /**
	 * Checks if is leaf.
	 * 
	 * @return true, if is leaf
	 */
  private boolean isLeaf()
  {
    return this.children==null;
  }
  
  /**
	 * Checks if is node member.
	 * 
	 * @param a
	 *          the a
	 * @return true, if is node member
	 */
  private boolean isNodeMember(GenericSphere a)
  {
  	return this.atomList.contains(a);
  }
  
  /**
	 * Checks if is root.
	 * 
	 * @return true, if is root
	 */
  public boolean isRoot()
  {
    return this.parent==null;
  }
  
	/**
	 * Max distance.
	 * 
	 * @return the double
	 */
	public double maxDistance()
  {
  	return this.surfaceBounds.maxDistance();
  }

	/**
	 * Number of stored atoms.
	 * 
	 * @return the int
	 */
	public int numberOfStoredSpheres()
	{
		int numChildren = 0;
		
		if (!isLeaf())
			for (OcNode<GenericSphere> child : this.children)
				numChildren += child.numberOfStoredSpheres();
		else
			numChildren = this.atomList.size();

		return numChildren;
	}
	
	/**
	 * Record insertion.
	 * 
	 * @param a
	 *          the a
	 */
	private void recordInsertion(GenericSphere a)
  {
		Point3d p = a.getPosition();
		double surfaceRadius = a.radius();
		
  	double lx = Math.min(this.surfaceBounds.lx, p.x-surfaceRadius);
  	double ux = Math.max(this.surfaceBounds.ux, p.x+surfaceRadius);
  	double ly = Math.min(this.surfaceBounds.ly, p.y-surfaceRadius);
  	double uy = Math.max(this.surfaceBounds.uy, p.y+surfaceRadius);
  	double lz = Math.min(this.surfaceBounds.lz, p.z-surfaceRadius);
  	double uz = Math.max(this.surfaceBounds.uz, p.z+surfaceRadius);
  	
  	this.surfaceBounds = new RectangleBounds(lx,ly,lz,ux,uy,uz);
  }

	/**
	 * Reinsert members.
	 * 
	 * @throws OcException
	 *           the oc exception
	 */
	private void reinsertMembers() throws OcException
  {
  	ArrayList<GenericSphere> currList = this.atomList;
  	this.atomList = null;

  	//insert atoms starting using current node as root
  	for (GenericSphere atom : currList)
  		insert(atom);
  }
	
	/**
	 * Split node.
	 * 
	 * @throws OcException
	 *           the oc exception
	 */
	private void splitNode() throws OcException
  {
    ArrayList<OcNode<GenericSphere>> currChildren = generateChildren();
    
    //set the children nodes to current
    this.children = currChildren;
    
    //add the current node elements to children
    reinsertMembers();
    
    assert currChildren.size()==this.numberOfStoredSpheres();
  }
	
	/**
	 * Update ancestors.
	 */
	private void updateAncestors()
  {
  	OcNode<GenericSphere> currNode = this;
  	while (!currNode.isRoot())
  	{
    	currNode.updateParentSurfaceBounds();    	
  	  currNode = currNode.parent;
  	}
  }

	/**
	 * Update parent surface bounds.
	 */
	private void updateParentSurfaceBounds()
  {
  	assert !isRoot();
  	
  	//create new bounds
  	synchronized (this.parent.surfaceBounds)
		{
    	//no need to update if already bigger than child
    	if (this.parent.surfaceBounds.isBounding(this.surfaceBounds))
    		return;

    	this.parent.surfaceBounds = this.surfaceBounds.union(this.parent.surfaceBounds);			
		} 
  }

}
