/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule.filters;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.VirtualMoleculeWrapper;
import edu.umd.umiacs.armor.physics.BasicPhysics;

public class MoleculeMassAligner implements MoleculeAligner
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5264398215212373238L;

	@Override
	public Molecule align(Molecule mol1, Molecule mol2)
	{
		return new VirtualMoleculeWrapper(mol2, alignmentTransform(mol1, mol2));
	}

	@Override
	public double alignedRmsd(Molecule mol1, Molecule mol2)
	{
		Point3d c1 = BasicPhysics.moleculeCenterOfMass(mol1);
		Point3d c2 = BasicPhysics.moleculeCenterOfMass(mol2);
		
		return c2.distance(c1);
	}

	@Override
	public RigidTransform alignmentTransform(Molecule m1, Molecule m2)
	{
		Point3d c1 = BasicPhysics.moleculeCenterOfMass(m1);
		Point3d c2 = BasicPhysics.moleculeCenterOfMass(m2);
		
		return new RigidTransform(null, c1.subtract(c2));
	}

	@Override
	public double distance(Molecule a, Molecule b)
	{
		return rmsd(a,b);
	}

	@Override
	public double distanceSquard(Molecule a, Molecule b)
	{
		return BasicMath.square(rmsd(a,b));
	}

	@Override
	public Molecule requiredSubObject(Molecule a)
	{
		return a;
	}

	@Override
	public double rmsd(Molecule mol1, Molecule mol2)
	{
		return 0.0;
	}

}
