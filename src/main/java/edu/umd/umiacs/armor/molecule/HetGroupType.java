package edu.umd.umiacs.armor.molecule;

public enum HetGroupType implements AtomGroupType
{
	HOH("Water",	"HOH", "O"),
	UKW("Undefined",	"UKW", "X");
	
	private final String letter;
	private final String name;
	private final String shortName;
	
	private HetGroupType(String name, String shortName, String letter)
	{
		this.name= name;
		this.shortName = shortName;
		this.letter = letter;
	}

	@Override
	public String getLetter()
	{
		return this.letter;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public PrimaryStructureType getPrimaryStructureType()
	{
		return PrimaryStructureType.HETERO;
	}

	@Override
	public String getShortName()
	{
		return this.shortName;
	}
}
