/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.TreeSet;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.util.SortablePair;

/**
 * The Class OcTree.
 * 
 * @param <GenericSphere>
 *          the generic type
 */
public final class OcTree<GenericSphere extends Sphere> implements Serializable
{
 
  /** The root node. */
  private final OcNode<GenericSphere> rootNode;

	/** The size. */
  private int size;
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 5754164805831907700L;
  
  /**
	 * Instantiates a new oc tree.
	 * 
	 * @param lxIn
	 *          the lx in
	 * @param lyIn
	 *          the ly in
	 * @param lzIn
	 *          the lz in
	 * @param uxIn
	 *          the ux in
	 * @param uyIn
	 *          the uy in
	 * @param uzIn
	 *          the uz in
	 */
  public OcTree(double lxIn, double lyIn, double lzIn, double uxIn, double uyIn, double uzIn)
  {
  	this(new RectangleBounds(lxIn,lyIn,lzIn,uxIn,uyIn,uzIn));
  }
  
  /**
	 * Instantiates a new oc tree.
	 * 
	 * @param bounds
	 *          the bounds
	 */
  public OcTree(RectangleBounds bounds)
  {
    this.rootNode = new OcNode<GenericSphere>(bounds);
    this.size = 0;
  }
  
  /**
	 * Exists.
	 * 
	 * @param a
	 *          the a
	 * @return true, if successful
	 */
  public boolean exists(GenericSphere a)
  {
  	assert this.rootNode!=null;
  	
  	if (a==null)
  		return false;
  	  	
    return this.rootNode.exits(a);
  }
  
  /**
	 * Gets the bounds.
	 * 
	 * @return the bounds
	 */
  public RectangleBounds getBounds()
  {
  	return this.rootNode.getBounds();
  }
    
  public GenericSphere getClosestSphere(Sphere a)
  {
  	assert this.rootNode!=null;
  	
  	return this.rootNode.getClosestSphere(a);
  }
    
  public GenericSphere getClosestSphere(Point3d p)
  {
  	assert this.rootNode!=null;
  	
  	return this.rootNode.getClosestAtom(p);
  }
  
  public ArrayList<GenericSphere> getClosestSphere(Point3d p, int n)
  {
  	assert this.rootNode!=null;
  	
		TreeSet<SortablePair<Double, GenericSphere>> list = new TreeSet<SortablePair<Double, GenericSphere>>();
  	this.rootNode.getClosestAtoms(p,n,list);
  	
  	//store the values in a list
  	ArrayList<GenericSphere> sortedList = new ArrayList<GenericSphere>(list.size());
  	for (SortablePair<Double, GenericSphere> pair : list)
  		sortedList.add(pair.y);
  	
  	return sortedList;
  }
  
  /**
	 * Gets the colliding.
	 * 
	 * @param a
	 *          the a
	 * @return the colliding
	 */
  public GenericSphere getColliding(GenericSphere a)
  {
  	assert this.rootNode!=null;
  	
  	return this.rootNode.getCollidingSphere(a);
  }
  
  public ArrayList<GenericSphere> getNearbySphere(Point3d p, double maxDistance)
  {
  	assert this.rootNode!=null;
  	
		ArrayList<GenericSphere> list = new ArrayList<GenericSphere>();
  	this.rootNode.getNearbySpheres(p,maxDistance,list);
  	
  	return list;
  }

  /**
	 * Gets the surface bounds.
	 * 
	 * @return the surface bounds
	 */
  public RectangleBounds getSurfaceBounds()
  {
  	assert this.rootNode!=null;
  	
  	return this.rootNode.getSurfaceBounds();
  }

  /**
	 * Insert.
	 * 
	 * @param a
	 *          the a
	 * @throws OcException
	 *           the oc exception
	 */
  public void insert(GenericSphere a) throws OcException
  {
  	assert this.rootNode!=null;
  	
    this.rootNode.insert(a);
    
    synchronized (this)
		{
      this.size++;			
		}
  }
  
  /**
	 * Checks if is closer than.
	 * 
	 * @param a
	 *          the a
	 * @param distance
	 *          the distance
	 * @return true, if is closer than
	 */
  public boolean isCloserThan(Sphere a, double distance)
  {
  	assert this.rootNode!=null;
  	
  	return this.rootNode.isCloserThan(a, distance);
  }
  
  /**
	 * Checks if is closer than.
	 * 
	 * @param p
	 *          the p
	 * @param distance
	 *          the distance
	 * @return true, if is closer than
	 */
  public boolean isCloserThan(Point3d p, double distance)
  {
  	assert this.rootNode!=null;
  	
  	return this.rootNode.isCloserThan(p,distance);
  }
  
  /**
	 * Checks if is colliding.
	 * 
	 * @param a
	 *          the a
	 * @return true, if is colliding
	 */
  public boolean isColliding(Sphere a)
  {
  	assert this.rootNode!=null;
  	
  	return this.rootNode.isColliding(a);
  }

  /**
	 * Checks if is empty.
	 * 
	 * @return true, if is empty
	 */
  public boolean isEmpty()
  {
  	return this.size==0;
  }
  
  /**
	 * Max distance.
	 * 
	 * @return the double
	 */
  public double maxDistance()
  {
  	assert this.rootNode!=null;
  	
  	return this.rootNode.maxDistance();
  }
  
  /**
	 * Size.
	 * 
	 * @return the int
	 */
  public int size()
  {
  	assert this.size==this.rootNode.numberOfStoredSpheres();
  	
  	return this.size;
  }
}
