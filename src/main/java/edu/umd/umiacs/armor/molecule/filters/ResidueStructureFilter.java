/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule.filters;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.DuplicateAtomException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MoleculeRuntimeException;
import edu.umd.umiacs.armor.util.Pair;

public class ResidueStructureFilter implements StructureFilter
{

	private final HashSet<Pair<String, Integer>> excludedList;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1591491256235529078L;
	
	public ResidueStructureFilter(List<Pair<String,Integer>> excludedList)
	{
		this.excludedList = new HashSet<Pair<String,Integer>>(excludedList);
	}

	@Override
	public Molecule filter(Molecule mol)
	{
		if (this.excludedList == null)
			return mol;
		
		ArrayList<Atom> newList = new ArrayList<Atom>(mol.size());
		
		for (Atom atom : mol)
		{
			if (!this.excludedList.contains(new Pair<String, Integer>(atom.getPrimaryInfo().getChainID(), atom.getPrimaryInfo().getResidueNumber())))
			{
				newList.add(atom);
			}	
		}
		
		BasicMolecule newMolecule = null;
		try
		{
			newMolecule = new BasicMolecule(newList);
		}
		catch (DuplicateAtomException e)
		{
			throw new MoleculeRuntimeException(e);
		}
		
		return newMolecule;
	}

}
