/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule.filters;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.AtomType;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.Pair;

public final class DistanceMiner
{
	private final ArrayList<ArrayList<Double>> atomDistances;
	//private final ArrayList<ArrayList<Atom>> closestAtoms;
	private final HashMap<String,ArrayList<Pair<Integer,Integer>>> fullNameLookup;	
	
	private final int maxAtoms;
	private final int numClosestAtoms;
	private final boolean random;
	
	private final HashMap<AtomType,ArrayList<Pair<Integer,Integer>>> typeLookup;
	
	private final ArrayList<Double> waterDensity;
	private final ArrayList<Point3d> waterPositions;
	
	private static <Key> void updateLookup(HashMap<Key,ArrayList<Pair<Integer,Integer>>> lookupTable, Key name, Pair<Integer,Integer> currPosition)
	{
		//add the lookup information
		ArrayList<Pair<Integer,Integer>> value = lookupTable.get(name);
		if (value==null)
			value = new ArrayList<Pair<Integer,Integer>>();

		value.add(currPosition);
		lookupTable.put(name, value);
	}
	
	public DistanceMiner(int numClosestAtoms)
	{
		this(numClosestAtoms, Integer.MAX_VALUE, false);
	}
	
	public DistanceMiner(int numClosestAtoms, int maxAtoms)
	{
		this(numClosestAtoms, maxAtoms, false);
	}
	
	public DistanceMiner(int numClosestAtoms, int maxAtoms, boolean random)
	{
		this.fullNameLookup = new HashMap<String, ArrayList<Pair<Integer,Integer>>>();
		this.typeLookup = new HashMap<AtomType, ArrayList<Pair<Integer,Integer>>>();
		
		//this.closestAtoms = new ArrayList<ArrayList<Atom>>();
		this.atomDistances = new ArrayList<ArrayList<Double>>();
		this.waterPositions = new ArrayList<Point3d>();
		this.waterDensity = new ArrayList<Double>();
		
		this.numClosestAtoms = numClosestAtoms;
		this.maxAtoms = maxAtoms;
		this.random = random;
	}
	
	public void addDistances(Molecule mol, List<Atom> water)
	{
		int counter = 0;
		
		if (this.random)
		{
			water = new ArrayList<Atom>(water);
			Collections.shuffle(water);
		}
		
		for (Atom waterAtom : water)
		{
			this.waterPositions.add(waterAtom.getPosition());
			this.waterDensity.add(waterAtom.getPrimaryInfo().getTempFactor());
			
			//find atom
			List<? extends Atom> closestList = mol.getClosestAtoms(waterAtom.getPosition(), this.numClosestAtoms);
			
			//extract back the sorted list
			ArrayList<Atom> closestAtoms = new ArrayList<Atom>(closestList);
			ArrayList<Double> closestDistances = new ArrayList<Double>(closestList.size());
			
			int position = 0;
			for (Atom molAtom : closestAtoms)
			{
				//store the distances
				closestDistances.add(molAtom.distanceToSurface(waterAtom.getPosition()));
				
				//get the atom info
				String fullName = molAtom.getPrimaryInfo().getAtomName();
				AtomType atomType = molAtom.getType();
				
				//get the current position
				Pair<Integer,Integer> currPosition = new Pair<Integer, Integer>(counter, position);
				
				//update the lookup table
				updateLookup(this.fullNameLookup, fullName, currPosition);
				updateLookup(this.typeLookup, atomType, currPosition);
			
				position++;
			}
			
			//store the information
			//this.closestAtoms.add(closestAtoms);
			this.atomDistances.add(closestDistances);			

			counter++;
			if (counter>this.maxAtoms)
				break;
		}
		
	}
	
	public ArrayList<String> getAtomNameList()
	{
		ArrayList<String> list = new ArrayList<String>(this.fullNameLookup.keySet());
		Collections.sort(list);
		
		return list;
	}
	
	public ArrayList<AtomType> getAtomTypeList()
	{
		ArrayList<AtomType> list = new ArrayList<AtomType>(this.typeLookup.keySet());
		Collections.sort(list);

		return list;
	}
	
	public double[][] getData(AtomType atomType)
	{
		return getData(new AtomType[]{atomType});
	}
	
	public double[][] getData(AtomType ...atomType)
	{
		if (atomType.length>this.numClosestAtoms)
			throw new IndexOutOfBoundsException("Number of atom types must be <= "+this.numClosestAtoms+".");
		
		
		HashMap<Integer,ArrayList<Integer>> lookupHash = new HashMap<Integer,ArrayList<Integer>>();
		for (int iter=0; iter<atomType.length; iter++)
		{
			ArrayList<Pair<Integer, Integer>> lookupInfo = this.typeLookup.get(atomType[iter]);
			for (Pair<Integer, Integer> pair : lookupInfo)
			{
				if (iter==0 && pair.y<atomType.length)
				{
					ArrayList<Integer> indexArray = new ArrayList<Integer>();
					indexArray.add(pair.y);
					lookupHash.put(pair.x, indexArray);
				}
				else
				{
					ArrayList<Integer> indexArray = lookupHash.get(pair.x);
					if (indexArray==null || indexArray.size()<iter || pair.y>=atomType.length || indexArray.contains(pair.y))
						continue;

					indexArray.add(pair.y);
				}
			}
		}
		
		ArrayList<Pair<Integer,ArrayList<Integer>>> validIndicies = new ArrayList<Pair<Integer,ArrayList<Integer>>>();
		for (Integer index : lookupHash.keySet())
		{
			ArrayList<Integer> indexArray = lookupHash.get(index);
			
			//make sure it is a full set
			if (indexArray.size()<atomType.length)
				continue;
			
			//add to the list of valid obects
			validIndicies.add(new Pair<Integer,ArrayList<Integer>>(index, indexArray));
		}
		
		if (validIndicies.isEmpty())
			return null;
		
		double[][] returnVal = new double[validIndicies.size()][atomType.length+1];
		
		int counter = 0;
		for (Pair<Integer,ArrayList<Integer>> lookPair : validIndicies)
		{
			for (int iter=0; iter<atomType.length; iter++)
				returnVal[counter][iter] = this.atomDistances.get(lookPair.x).get(lookPair.y.get(iter));
			
			//add the water molecule
			returnVal[counter][atomType.length] = this.waterDensity.get(lookPair.x);
			
			counter++;
		}
		
		return returnVal;
	}
	
	public double[][] getData(String atomName)
	{
		ArrayList<Pair<Integer, Integer>> lookupInfo = this.fullNameLookup.get(atomName);
		
		if (lookupInfo==null)
			return null;
		
		double[][] returnVal = new double[lookupInfo.size()][2];
		
		int counter=0;
		for (Pair<Integer,Integer> lookPair : lookupInfo)
		{
			if (lookPair.y>0)
				continue;

			returnVal[counter][0] = this.atomDistances.get(lookPair.x).get(lookPair.y);
			returnVal[counter][1] = this.waterDensity.get(lookPair.x);
			counter++;
		}
		
		return returnVal;
	}
}
