/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.biojava.bio.structure.AminoAcid;
import org.biojava.bio.structure.Chain;
import org.biojava.bio.structure.SSBond;
import org.biojava.bio.structure.Structure;
import org.biojava.bio.structure.StructureImpl;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.molecule.filters.MoleculeAlignmentException;
import edu.umd.umiacs.armor.molecule.filters.MoleculeSVDAligner;

/**
 * The Class PdbStructure.
 */
public final class PdbStructure implements Serializable
{
	
	private final String errorOutput;
	
	/** The struct. */
	private final Structure struct;
	
	private static HashMap<String, AtomGroupType> GROUP_LOOKUP_TABLE = new HashMap<String, AtomGroupType>(100);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2201301589673837709L;
	
	public static AtomGroupType findGroupType(String name)
	{
		//if lookup table not created, create one
		synchronized (GROUP_LOOKUP_TABLE)
		{
			if (GROUP_LOOKUP_TABLE.isEmpty())
			{
				//add amino acids
				for (AminoAcidType type : AminoAcidType.values())
					GROUP_LOOKUP_TABLE.put(type.getShortName().toUpperCase(), type);

				//add nucleotides
				for (NucleotideType type : NucleotideType.values())
					GROUP_LOOKUP_TABLE.put(type.getShortName().toUpperCase(), type);

				//add hetero types
				for (HetGroupType type : HetGroupType.values())
					GROUP_LOOKUP_TABLE.put(type.getShortName().toUpperCase(), type);
			}
		}
		
		AtomGroupType type = GROUP_LOOKUP_TABLE.get(name.toUpperCase());
		
		return type;
	}
	

	
	/**
	 * Instantiates a new pdb structure.
	 * 
	 * @param struct
	 *          the struct
	 */
	protected PdbStructure(Structure struct, String errorOutput)
	{
		this.struct = struct;
		this.errorOutput = errorOutput;
	}
	
	public void addModel(Molecule molecule) throws AtomNotFoundException
	{
		if (molecule==null)
			throw new java.lang.NullPointerException("Molecule cannot be null.");
		
		this.struct.addModel(cloneModel(0));
		updateModel(this.struct.nrModels()-1, molecule);
	}
	
	/**
	 * Clone model.
	 * 
	 * @param index
	 *          the index
	 * @return the list
	 */
	private List<Chain> cloneModel(int index)
	{
		ArrayList<Chain> newModel = new ArrayList<Chain>();
		for (Chain chain : this.struct.getModel(index))
		  newModel.add((Chain)chain.clone());
		
		return newModel;
	}
	
	/**
	 * Creates the first model structure.
	 * 
	 * @return the pdb structure
	 */
	private PdbStructure createFirstModelStructure(int index)
	{
		Structure firstModelStruct = new StructureImpl();
		
		firstModelStruct.setPDBHeader(this.struct.getPDBHeader());
		firstModelStruct.setCompounds(this.struct.getCompounds());
		firstModelStruct.setConnections(this.struct.getConnections());
		firstModelStruct.setSites(this.struct.getSites());
		firstModelStruct.setNmr(true);
		
		for (SSBond bond : this.struct.getSSBonds())
		{
			firstModelStruct.addSSBond(bond.clone());
		}
		
		firstModelStruct.addModel(cloneModel(index));
		
		return new PdbStructure(firstModelStruct, new String());
	}
	
	public PdbStructure createFromMolecule(Molecule molecule) throws AtomNotFoundException
	{
		ArrayList<Molecule> moleculeList = new ArrayList<Molecule>();
		moleculeList.add(molecule);
		
		return createFromMoleculeList(moleculeList, 0);
	}
	
	public PdbStructure createFromMoleculeList(List<Molecule> moleculeList) throws AtomNotFoundException
	{
		return createFromMoleculeList(moleculeList, 0);
	}
	
	public PdbStructure createFromMoleculeList(List<Molecule> moleculeList, int exampleModel) throws AtomNotFoundException
	{
		Iterator<Molecule> iterator = moleculeList.iterator();
		
		if (!iterator.hasNext())
			return null;
		
		PdbStructure newStructure = createFirstModelStructure(exampleModel);
		newStructure.updateModel(0, iterator.next());
		
		while(iterator.hasNext())
		{
			newStructure.addModel(iterator.next());
		}
		
		return newStructure;
	}

	public PdbStructure createSubStructure(List<Integer> modelList)
	{
		Structure modelStructure = new StructureImpl();
		
		modelStructure.setPDBHeader(this.struct.getPDBHeader());
		modelStructure.setCompounds(this.struct.getCompounds());
		modelStructure.setSSBonds(this.struct.getSSBonds());
		modelStructure.setConnections(this.struct.getConnections());
		modelStructure.setSites(this.struct.getSites());
		modelStructure.setNmr(true);

		for (int model : modelList)
			modelStructure.addModel(cloneModel(model));
		
		return new PdbStructure(modelStructure, new String());
	}

	public ArrayList<Molecule> getAllMolecules()
	{
		try
		{
			ArrayList<Molecule> molList = new ArrayList<Molecule>(getNumberOfModels());
			for (int iter=0; iter<getNumberOfModels(); iter++)
				molList.add(getModel(iter).createBasicMolecule());
			
			return molList;
		}
		catch (PdbException e)
		{
			throw new MoleculeRuntimeException("No models detected in the pdb.", e);
		}
	}
	
	/**
	 * Gets the average model.
	 * 
	 * @return the average model
	 * @throws PdbException 
	 */
	public Model getAverageModel() throws PdbException
	{
		ArrayList<Point3d> positions = new ArrayList<Point3d>();
		ArrayList<Integer> count = new ArrayList<Integer>();
		
		ArrayList<Model> models = new ArrayList<Model>(this.struct.nrModels());
		for (int iter=0; iter<this.struct.nrModels(); iter++)
			models.add(getModel(iter));
		
		Model firstModel = models.get(0);
		for (int iter=0; iter<firstModel.size(); iter++)
		{
			positions.add(firstModel.get(iter).getPosition());
			count.add(new Integer(0));
		}
		
		for (int modelNumber=0; modelNumber<models.size(); modelNumber++)
		{
			Model currentModel = models.get(modelNumber);
			for (int iter=0; iter<firstModel.size(); iter++)
			{
				Atom a = currentModel.get(firstModel.get(iter));
				
				if (a!=null)
				{
					positions.set(iter, positions.get(iter).add(a.getPosition()));
					count.set(iter, count.get(iter)+1);
				}
			}
		}
		
		for (int iter=0; iter<firstModel.size(); iter++)
		{
			Atom oldAtom = firstModel.get(iter);
			Point3d atomPosition = oldAtom.getPosition().divide(count.get(iter));
			
			firstModel.set(iter, oldAtom.createWithNewPosition(atomPosition));
		}
		
		return firstModel;
	}
	
	public String getErrorOutput()
	{
		return this.errorOutput;
	}
	
	public Model getModel(int index) throws PdbException
	{
		if (index>=getNumberOfModels())
			throw new PdbException("Model "+index+" does not exist in the pdb.");
		
		return getModel(index,null);
	}
	
	/**
	 * Gets the model.
	 * 
	 * @param index
	 *          the index
	 * @return the model
	 */
	private Model getModel(int index, BasicMolecule molecule)
	{
		List<Chain> chains = this.struct.getModel(index);
		
		ArrayList<Atom> atoms = new ArrayList<Atom>();
		ArrayList<Atom> hetAtoms = new ArrayList<Atom>();
		
		int uniqueId = 0;
		for (Chain c : chains)
		{
			for (org.biojava.bio.structure.Group g : c.getAtomGroups())
			{
				boolean isAtom = true;
				if (g.getType().equalsIgnoreCase("hetatm"))
					isAtom = false;
				
				//TODO: need to hack for HSE, always an atom type
				if (!isAtom && g.getPDBName().equalsIgnoreCase("HSE"))
					isAtom = true;
				
				for (org.biojava.bio.structure.Atom a : g.getAtoms())
				{
					int currId = 0;
					if (!isAtom)
					{
						uniqueId++;
						currId = uniqueId;
					}
					
					Atom atom = parseGroupAtom(g, a, molecule, currId);
					
					if (atom != null)
					{
						if (isAtom)
							atoms.add(atom);
						else
							hetAtoms.add(atom);
					}
				}
			}
		}
		
		return new Model(atoms, hetAtoms);
	}
	
	/**
	 * Gets the number of models.
	 * 
	 * @return the number of models
	 */
	public int getNumberOfModels()
	{
		return this.struct.nrModels();
	}
	
	/**
	 * Gets the structure.
	 * 
	 * @return the structure
	 */
	protected Structure getStructure()
	{
		return this.struct;
	}

	private AtomImpl parseGroup(org.biojava.bio.structure.Group g, org.biojava.bio.structure.Atom a, int uniqueId)
	{
		return parseGroupAtom(g,a,null,uniqueId);
	}
	
	private AtomImpl parseGroupAtom(org.biojava.bio.structure.Group g, org.biojava.bio.structure.Atom a, BasicMolecule mol, int uniqueId)
	{
		AtomImpl atom = null;
		
		boolean isAminoAcid = g.getType().equalsIgnoreCase("amino");
		
		SecondaryStructure.Type secondaryType = SecondaryStructure.Type.DISORDERED;
		if (isAminoAcid)
		{
		  AminoAcid aa = (AminoAcid) g;
		  String value = aa.getSecStruc().get("PDB_AUTHOR_ASSIGNMENT");
		  
		  if (value != null)
		  {
			  if (value.equals("HELIX"))
			  	secondaryType = SecondaryStructure.Type.HELIX;
			  else
		  	if (value.equals("STRAND"))
		  		secondaryType = SecondaryStructure.Type.SHEET;
		  }
		}

		Point3d p = new Point3d(a.getX(), a.getY(), a.getZ());
		
		PrimaryStructure primaryStructure;
		SecondaryStructure secondaryStructure;
		
		//try to find existing structure in previous molecule		
		//if (mol == null)
		{
		  primaryStructure = new PrimaryStructure(a.getGroup().getResidueNumber().getSeqNum(), 
		  		a.getFullName(), 
		  		a.getGroup().getChainId(),
		  		a.getGroup().getPDBName(),
		  		a.getTempFactor(),
		  		uniqueId);
		  secondaryStructure = new SecondaryStructure(secondaryType);
		}
		
		atom = new AtomImpl(p, primaryStructure, secondaryStructure);
		
		return atom;
	}

	public void setTitle(String title)
	{
		this.struct.getPDBHeader().setTitle(title);
	}
	
	/**
	 * To pdb.
	 * 
	 * @return the string
	 */
	public String toPDB()
	{
		return this.struct.toPDB();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return toPDB();
	}

	public void updateModel(int index, Molecule molecule) throws AtomNotFoundException
	{
		for (Chain c : this.struct.getModel(index))
			for (org.biojava.bio.structure.Group g : c.getAtomGroups())
			{
				ArrayList<Atom> groupAtoms = new ArrayList<Atom>();
				
				for (org.biojava.bio.structure.Atom a : g.getAtoms())
					groupAtoms.add(parseGroup(g, a, 0));
				
				RigidTransform t = null;

				Iterator<Atom> atomIter = groupAtoms.iterator();
			  for (org.biojava.bio.structure.Atom a : g.getAtoms())
				{
					Atom currAtom = atomIter.next();
					
					//TODO: fix so that all atoms are updated

					try
					{
						Point3d p = molecule.getAtomPosition(currAtom);
						a.setX(p.x);
						a.setY(p.y);
						a.setZ(p.z);
					}
					catch (AtomNotFoundException e)
					{
						//get the transform if not cached already
						if (t==null)
							t = findRigidTransform(molecule, groupAtoms);
						
						//compute the transformed point
						Point3d p = t.transform(a.getX(), a.getY(), a.getZ());
						
						a.setX(p.x);
						a.setY(p.y);
						a.setZ(p.z);
					}
				}
			}
	}
	
	private RigidTransform findRigidTransform(Molecule molecule, ArrayList<Atom> groupAtoms)
	{
		ArrayList<Atom> adjAtoms = new ArrayList<Atom>();
		
		//locate the same residue
		PrimaryStructure prime = groupAtoms.get(0).getPrimaryInfo();
		for (Atom a : molecule)
		{
			if (a.getPrimaryInfo().isSameResidue(prime))
				adjAtoms.add(a);
		}
		
		if (adjAtoms.size()<4)
			return RigidTransform.identityTransform();
		
		MoleculeSVDAligner aligner = new MoleculeSVDAligner();
		
		try
		{
			return aligner.alignmentTransform(new BasicMolecule(groupAtoms), molecule);
		}
		catch (DuplicateAtomException e)
		{
			throw new MoleculeRuntimeException(e);
		}
		catch (MoleculeAlignmentException e)
		{
			return RigidTransform.identityTransform();
		}
	}
		
}
