/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.util.HashCodeUtil;

/**
 * The Class Bond.
 * 
 * @param <GenericAtom>
 *          the generic type
 */
public final class Bond implements Serializable
{
	
	/** The bond type. */
	private final BondType bondType;

	/** The from atom. */
	private final Atom fromAtom;
	
	/** The to atom. */
	private final Atom toAtom;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3595599456401501325L;
		
	public Bond(Atom fromAtom, Atom toAtom)
	{
		if (fromAtom.getPrimaryInfo()==null || toAtom.getPrimaryInfo()==null)
			throw new MoleculeRuntimeException("Bond atoms must have primary structure defined.");
		
		this.fromAtom = fromAtom;
		this.toAtom = toAtom;
		
		this.bondType = BondType.getBondType(fromAtom.getPrimaryInfo().getAtomName(), toAtom.getPrimaryInfo().getAtomName());
	}
	
	public Bond(Atom fromAtom, Atom toAtom, double minRadius, double maxRadius, double expRadius)
	{
		if (fromAtom.getPrimaryInfo()==null || toAtom.getPrimaryInfo()==null)
			throw new MoleculeRuntimeException("Bond atoms must have primary structure defined.");
		
		this.fromAtom = fromAtom;
		this.toAtom = toAtom;
		
		this.bondType = new BondType(fromAtom.getPrimaryInfo().getAtomName(), toAtom.getPrimaryInfo().getAtomName(), minRadius, maxRadius, expRadius);
	}
	
	public Bond createFromMolecule(Molecule mol) throws AtomNotFoundException
	{
		Atom a1 = this.fromAtom.createWithNewPosition(mol.getAtomPosition(this.fromAtom));
		Atom a2 = this.toAtom.createWithNewPosition(mol.getAtomPosition(this.toAtom));
		
		return new Bond(a1, a2);
	}
	
	public Bond createNewVector(Point3d p)
	{
		return new Bond(this.fromAtom, this.toAtom.createWithNewPosition(this.fromAtom.getPosition().add(p)));
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bond other = (Bond) obj;
		if (this.bondType == null)
		{
			if (other.bondType != null)
				return false;
		}
		else if (!this.bondType.equals(other.bondType))
			return false;
		if (this.fromAtom == null)
		{
			if (other.fromAtom != null)
				return false;
		}
		else if (!this.fromAtom.equals(other.fromAtom))
			return false;
		if (this.toAtom == null)
		{
			if (other.toAtom != null)
				return false;
		}
		else if (!this.toAtom.equals(other.toAtom))
			return false;
		return true;
	}


	/**
	 * Gets the bond type.
	 * 
	 * @return the bond type
	 */
	public BondType getBondType()
	{
		return this.bondType;
	}

	/**
	 * Gets the from atom.
	 * 
	 * @return the from atom
	 */
	public Atom getFromAtom()
	{
		return this.fromAtom;
	}
	
	/**
	 * Gets the norm vector.
	 * 
	 * @return the norm vector
	 */
	public Point3d getNormVector()
	{
		return getVector().normalized();
	}
	
	/**
	 * Gets the to atom.
	 * 
	 * @return the to atom
	 */
	public Atom getToAtom()
	{
		return this.toAtom;
	}
	
	/**
	 * Gets the vector.
	 * 
	 * @return the vector
	 */
	public Point3d getVector()
	{
		return this.toAtom.getPosition().subtract(this.fromAtom.getPosition());
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
   int result = HashCodeUtil.SEED;
    
    //collect the contributions of various fields
    result = HashCodeUtil.hash(result, this.toAtom);
    result = HashCodeUtil.hash(result, this.fromAtom);
    
    return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return new String("From="+getFromAtom()+" To="+getToAtom());
	}
	
	
}
