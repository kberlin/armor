/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;
import java.util.HashMap;
import edu.umd.umiacs.armor.util.HashCodeUtil;
import edu.umd.umiacs.armor.util.Pair;

/**
 * The Class BondType.
 */
public final class BondType implements Comparable<BondType>, Serializable
{
	private final String fromAtomName;
	private final AtomType fromAtomType;
	private final double maxRadius;
	private final double meanRadius;
	private final double minRadius;
	private final String toAtomName;
	private final AtomType toAtomType;
	
	private final static HashMap<Pair<String,String>,BondType> _STORED_BOND_TYPES = new HashMap<Pair<String,String>,BondType>();

	public final static BondType C_H = new BondType("C", "H",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C1p_H1p = new BondType("C1'", "H1'",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C2_H2 = new BondType("C2", "H2",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C2p_H2p = new BondType("C2'", "H2'",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C3_H3 = new BondType("C3", "H3",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C3p_H3p = new BondType("C3'", "H3'",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C4_H4 = new BondType("C4", "H4",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C4p_H4p = new BondType("C4'", "H4'",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C5_H5 = new BondType("C5", "H5",
	/* radius */1.07, 1.104, 1.09);
	public final static BondType C5p_H5p = new BondType("C5'", "H5'",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C5p_H5pp = new BondType("C5'", "H5''",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C6_H6 = new BondType("C6", "H6",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C7_H7 = new BondType("C7", "H7",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType C8_H8 = new BondType("C8", "H8",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CA_C = new BondType("CA", "C",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CA_CB = new BondType("CA", "CB",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CA_HA = new BondType("CA", "HA",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CB_CA = new BondType("CB", "CA",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CB_HB = new BondType("CB", "HB",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CD1_CG = new BondType("CD1", "CG",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CD1_CG1 = new BondType("CD1", "CG1",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CD2_CG = new BondType("CD2", "CG",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CG_HG = new BondType("CG", "HG",
	/* radius */1.07, 1.104, 1.09, true);
	public final static BondType CG1_CB = new BondType("CG1", "CB",
	/* radius */1.07, 1.104, 1.09, true);

	public final static BondType CG2_CB = new BondType("CG2", "CB",
	/* radius */1.07, 1.104, 1.09, true);

	public final static BondType N_C = new BondType("N", "C",
	/* radius */1.469, 1.499, 1.48, true);

	public final static BondType N_H = new BondType("N", "H",
	/* radius */0.98, 1.05, 1.02, true);
	public final static BondType N1_H1 = new BondType("N1", "H1",
	/* radius */0.98, 1.05, 1.02, true);
	public final static BondType N3_H3 = new BondType("N3", "H3",
	/* radius */0.98, 1.05, 1.02, true);
	public final static BondType NE1_HE1 = new BondType("NE1", "HE1",
	/* radius */0.98, 1.05, 1.02, true);

	/**
	 * 
	 */
	private static final long serialVersionUID = 4697839213512946716L;
	
	public static BondType getBondType(String fromAtomName, String toAtomName)
	{
		BondType type = _STORED_BOND_TYPES.get(new Pair<String,String>(fromAtomName,toAtomName));
		
		if (type==null)
			type = _STORED_BOND_TYPES.get(new Pair<String,String>(toAtomName, fromAtomName));

		if (type==null)
			throw new MoleculeRuntimeException("Uknown bond type: "+fromAtomName+"-"+toAtomName+".");
		
		return type;
	}
	
	public BondType(String fromAtomName, String toAtomName, double minRadius, double maxRadius, double meanRadius)
	{
		this.fromAtomName = fromAtomName.intern();
		this.toAtomName = toAtomName.intern();
		this.minRadius = minRadius;
		this.maxRadius = maxRadius;
		this.meanRadius = meanRadius;
		this.fromAtomType = AtomType.getAtomTypeFromFull(this.fromAtomName);
		this.toAtomType = AtomType.getAtomTypeFromFull(this.toAtomName);
	}
	
	private BondType(String fromAtomName, String toAtomName, double minRadius, double maxRadius, double meanRadius, boolean flag)
	{
		this(fromAtomName, toAtomName, minRadius, maxRadius, meanRadius);
		_STORED_BOND_TYPES.put(new Pair<String,String>(fromAtomName.intern(),toAtomName.intern()), this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(BondType b)
	{
		int result = this.fromAtomName.compareTo(b.fromAtomName);

		if (result == 0)
			return this.toAtomName.compareTo(b.toAtomName);

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		BondType other = (BondType) obj;
		if (this.fromAtomName == null)
		{
			if (other.fromAtomName != null)
				return false;
		}
		else if (!this.fromAtomName.equals(other.fromAtomName))
			return false;
		if (this.toAtomName == null)
		{
			if (other.toAtomName != null)
				return false;
		}
		else if (!this.toAtomName.equals(other.toAtomName))
			return false;
		return true;
	}

	/**
	 * Gets the from atom name.
	 * 
	 * @return the from atom name
	 */
	public final String getFromAtomName()
	{
		return this.fromAtomName;
	}

	public final AtomType getFromAtomType()
	{
		return this.fromAtomType;
	}

	/**
	 * @return the maxRadius
	 */
	public double getMaxRadius()
	{
		return this.maxRadius;
	}

	/**
	 * @return the meanRadius
	 */
	public double getMeanRadius()
	{
		return this.meanRadius;
	}

	/**
	 * @return the minRadius
	 */
	public double getMinRadius()
	{
		return this.minRadius;
	}
	
	/**
	 * Gets the to atom name.
	 * 
	 * @return the to atom name
	 */
	public final String getToAtomName()
	{
		return this.toAtomName;
	}
	
	public final AtomType getToAtomType()
	{
		return this.toAtomType;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		int result = HashCodeUtil.SEED;

		// collect the contributions of various fields
		result = HashCodeUtil.hash(result, this.fromAtomName);
		result = HashCodeUtil.hash(result, this.toAtomName);

		return result;
	}
}
