/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.util.SortablePair;

public final class SolvatedMolecule extends AbstractMolecule
{
	private static final class AddGridWater implements Runnable
	{
		
		private final double density;
		
		private final Molecule molecule;
		
		private final List<Double> newSolventDistance;
		
		private final List<AtomImpl> newSolventLayer;

		private final int numThreads;
		
		private final double thickness;
		
		private final int threadNumber;
		
		private final SolventType type;

		public AddGridWater(Molecule molecule, List<AtomImpl> newSolventLayer, List<Double> newSolventDistance, 
				SolventType type, double thickness, int threadNumber, int numThreads, double density)
		{
			this.molecule = molecule;
			this.newSolventLayer = newSolventLayer;
			this.newSolventDistance = newSolventDistance;
			this.threadNumber = threadNumber;
			this.numThreads = numThreads;
			this.type = type;
			this.thickness = thickness;
			this.density = density;
		}
		
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			//get molecule properties
			Point3d moleculeCenter = this.molecule.center();
	  	double moleculeRadius = this.molecule.maxAtomDistance()*0.5;
	  	RectangleBounds moleculeBounds = this.molecule.getSurfaceBounds();

	  	RectangleBounds totalBounds = this.molecule.getBounds().addThickness(this.thickness);
	  	

	   	RectangleBounds solventLayerBounds = null;
			switch (this.type)
	  	{
	  		case LAYER : 
	  			solventLayerBounds = moleculeBounds.addThickness(this.thickness); 
	  			break;
	  		case BOX : 
	  			solventLayerBounds = moleculeBounds.addThickness(this.thickness);
	  			break;
	  		case SPHERE : 
	  			solventLayerBounds = new RectangleBounds(moleculeCenter.subtract(moleculeRadius+this.thickness),
	  																							 moleculeCenter.add(moleculeRadius+this.thickness));
	  																				
	  			break;
	  		default : throw new MoleculeRuntimeException("Unknown water layer type.");
	  	}

			//add the sudo water atoms
    	for (double z=solventLayerBounds.lz+this.density*this.threadNumber; z<=solventLayerBounds.uz; z+=this.density*this.numThreads)
	  	{
	    	for (double y=solventLayerBounds.ly; y<=solventLayerBounds.uy; y+=this.density)
	    	{
	  	  	for (double x=solventLayerBounds.lx; x<=solventLayerBounds.ux; x+=this.density)
	      	{
		    		//adjust atom position by the new box offset value
		    		Point3d newPoint = new Point3d(x, y, z);
		    		
		     		//if potentially a member, these conditions are faster to check than the full molecule intersection
		    		boolean shouldAdd= true;
		    	  switch(this.type)
		    		{
		      		case LAYER : 
		      			shouldAdd =  this.molecule.isCloserThan(newPoint, this.thickness);
		      			break;
		      		case BOX : 
		      			shouldAdd =  totalBounds.isBounding(newPoint);
		       			break;			
		      		case SPHERE : 
		      			shouldAdd = newPoint.distance(moleculeCenter)<=(moleculeRadius+this.thickness);
		       			break;			
		    		}
		    	  
		    	  //add if the atom fits in the holes of the molecule
		    	  shouldAdd = shouldAdd && !this.molecule.isCloserThan(newPoint, Math.max(1.4, WATER_LAYER_DISTANCE));
		    	  
		    		if (shouldAdd)
		    		{
		    			//add the current atom to the solvation layer
		    			AtomImpl a = new AtomImpl(newPoint, AtomType.SUDO_WATER);
		      		
		      		try
							{
		      			this.newSolventLayer.add(a);
		      			
		      			//if the molecule is far enough, it becomes a solvent molecule and not a contrast molecule
								this.newSolventDistance.add(Math.max(0.0, this.molecule.distanceToCore(newPoint)));
							} 
		      		catch (OcException e)
							{
								throw new AssertionError(e);
							}
		      	}
	      	}
	    	}
    	}
		}
	}

	/**
	 * The Enum SolventType.
	 */
	public enum SolventType 
	{	
		/** The BOX. */
		BOX,
		
		/** The LAYER. */
		LAYER,
		
		/** The SPHERE. */
		SPHERE;
	}
	
	/** The molecule. */
	private final Molecule molecule;
	
	/** The solvent distance. */
	private final ArrayList<Double> solventDistance;
	
	/** The solvent layer. */
	private Solvent solventLayer;

	private final double layerThickness;

	private final double gridSize;

	public final static double DEFAULT_LAYER_THICKNESS = 8.0; //Angstrom	
	
	public final static double DEFAULT_WATER_GRID_SIZE= 0.5; //Angstrom	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2303976430122016818L;
	
	public final static double WATER_LAYER_DISTANCE = 1.4; //Angstrom	

	private static Solvent generateSolvent(Molecule mol, ArrayList<Double> solventDistance, SolventType type, double thickness, double density) 
  {
  	//generate an empty solvent layer molecule
  	List<AtomImpl> newSolventLayerList = Collections.synchronizedList(new ArrayList<AtomImpl>(128));  	
  	
  	//generate an empty solvent layer molecule
  	List<Double> newSolventDistance = Collections.synchronizedList(solventDistance);

  	//create a thread pool
    int numThreads = Runtime.getRuntime().availableProcessors();
    ExecutorService execSvc = Executors.newFixedThreadPool(numThreads);

  	for (int iter=0; iter<=numThreads; iter++)
  	{
    	AddGridWater addThread = new AddGridWater(mol, newSolventLayerList, newSolventDistance, type, thickness, iter, numThreads, density);
    	execSvc.execute(addThread);
  	}
  	
    //shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(20L, TimeUnit.MINUTES);
		} 
    catch (InterruptedException e) 
    {
    	execSvc.shutdownNow();
    	throw new AssertionError("Unable to finish all tasks.");
    }
    
   	try
		{
			return new Solvent(newSolventLayerList);
		}
		catch (DuplicateAtomException e)
		{
			throw new MoleculeRuntimeException(e);
		}
  }
	
	public SolvatedMolecule(Molecule molecule) throws OcException
	{
		this(molecule, DEFAULT_LAYER_THICKNESS, DEFAULT_WATER_GRID_SIZE);
	}
	
	public SolvatedMolecule(Molecule molecule, double layerThickness, double gridSize) throws OcException
	{
		this.molecule = molecule;
		this.solventDistance = new ArrayList<Double>(128);
		this.layerThickness = layerThickness;
		this.gridSize = gridSize;
		this.solventLayer = generateSolvent(molecule, this.solventDistance, SolventType.LAYER, layerThickness, gridSize);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#center()
	 */
	@Override
	public Point3d center()
	{
		return this.molecule.center();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#deuterate(double)
	 */
	@Override
	public SolvatedMolecule deuterate(double deuteriumFraction)
	{
		SolvatedMolecule newMolecule = new SolvatedMolecule(this.molecule.deuterate(deuteriumFraction), this.layerThickness, this.gridSize);
		newMolecule.solventLayer = newMolecule.solventLayer.deuterate(deuteriumFraction);
		
		return newMolecule;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#distanceToCore(int)
	 */
	@Override
	public double distanceToCore(int index)
	{
		if (index>=this.molecule.size())
			return this.solventDistance.get(index-this.molecule.size());
		
		return 0.0;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#distanceToCore(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public double distanceToCore(Point3d p)
	{
		return this.molecule.distanceToCore(p);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#exists(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public boolean exists(Atom a)
	{
		boolean value = this.molecule.exists(a);
		
		return value;
	}
	
  @Override
	public Atom getAtom(int index)
	{
		if (index<this.molecule.size())
			return this.molecule.getAtom(index);
		
		return this.solventLayer.getAtom(index-this.molecule.size());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public Point3d getAtomPosition(Atom a) throws AtomNotFoundException
	{
		if (this.molecule.exists(a))
			return this.molecule.getAtomPosition(a);
		else
			return this.solventLayer.getAtomPosition(a);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(int)
	 */
	@Override
	public Point3d getAtomPosition(int index)
	{
		if (index<this.molecule.size())
			return this.molecule.getAtomPosition(index);

		return this.solventLayer.getAtomPosition(index-this.molecule.size());
	}

	@Override
	public List<Atom> getAtoms()
	{
		List<Atom> atoms = this.molecule.getAtoms();		
		atoms.addAll(this.solventLayer.getAtoms());
		
		return atoms;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomSurfaceRadius(int)
	 */
	@Override
	public double getAtomSurfaceRadius(int index)
	{
		if (index<this.molecule.size())
			return this.molecule.getAtomSurfaceRadius(index);
		
		return this.solventLayer.getAtomSurfaceRadius(index-this.molecule.size());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomType(int)
	 */
	@Override
	public AtomType getAtomType(int index)
	{
		if (index<this.molecule.size())
			return this.molecule.getAtomType(index);
		
		return this.solventLayer.getAtomType(index-this.molecule.size());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getBounds()
	 */
	@Override
	public RectangleBounds getBounds()
	{
		if (this.solventLayer==null)
			return this.molecule.getBounds();
		else
			return this.molecule.getBounds().union(this.solventLayer.getBounds());
	}

	@Override
	public List<? extends Atom> getCloseAtoms(Point3d p, double maxDist)
	{
		List<? extends Atom> list1 = this.molecule.getCloseAtoms(p, maxDist);
		List<? extends Atom> list2 = this.solventLayer.getCloseAtoms(p, maxDist);
		
		//combine the list from both atoms
		ArrayList<SortablePair<Double,Atom>> combinedSortableList = new ArrayList<SortablePair<Double,Atom>>(list1.size()+list2.size());
		for (Atom a : list1)
			combinedSortableList.add(new SortablePair<Double, Atom>(a.distanceToSurface(p), a));
		for (Atom a : list2)
			combinedSortableList.add(new SortablePair<Double, Atom>(a.distanceToSurface(p), a));
		
		//sort the list
		Collections.sort(combinedSortableList);

		//add the first elements of sorted list
		ArrayList<Atom> combinedList = new ArrayList<Atom>(combinedSortableList.size());
		for (SortablePair<Double,Atom> pair : combinedSortableList)
		{
			combinedList.add(pair.y);
		}
		
		return combinedList;
	}

	@Override
	public List<? extends Atom> getClosestAtoms(Point3d p, int n)
	{
		List<? extends Atom> list1 = this.molecule.getClosestAtoms(p, n);
		List<? extends Atom> list2 = this.solventLayer.getClosestAtoms(p, n);
		
		//combine the list from both atoms
		ArrayList<SortablePair<Double,Atom>> combinedSortableList = new ArrayList<SortablePair<Double,Atom>>(list1.size()+list2.size());
		for (Atom a : list1)
			combinedSortableList.add(new SortablePair<Double, Atom>(a.distanceToSurface(p), a));
		for (Atom a : list2)
			combinedSortableList.add(new SortablePair<Double, Atom>(a.distanceToSurface(p), a));
		
		//sort the list
		Collections.sort(combinedSortableList);

		//add the first elements of sorted list
		ArrayList<Atom> combinedList = new ArrayList<Atom>(n);
		for (SortablePair<Double,Atom> pair : combinedSortableList)
		{
			combinedList.add(pair.y);
			if (combinedList.size()>=n)
				break;
		}
		
		return combinedList;
	}

	/**
	 * Gets the molecule.
	 * 
	 * @return the molecule
	 */
	public Molecule getMolecule()
	{
		return this.molecule;
	}

	/**
	 * Gets the solvent layer.
	 * 
	 * @return the solvent layer
	 */
	public Solvent getSolventLayer()
	{
		return this.solventLayer;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getSurfaceBounds()
	 */
	@Override
	public RectangleBounds getSurfaceBounds()
	{
		if (this.solventLayer==null)
			return this.molecule.getSurfaceBounds();
		else
			return this.molecule.getSurfaceBounds().union(this.solventLayer.getSurfaceBounds());
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isCloserThan(edu.umd.umiacs.armor.math.Point3D, double)
	 */
	@Override
	public boolean isCloserThan(Point3d p, double distance)
	{
		if (this.solventLayer==null)
			return this.molecule.isCloserThan(p, distance);
	  else
	  	return this.molecule.isCloserThan(p, distance) || this.solventLayer.isCloserThan(p, distance);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isContrastAtom(int)
	 */
	@Override
	public boolean isDisplacedAtom(int index)
	{
		if (index<this.molecule.size())
			return false;
		
		if (distanceToCore(index)>0.0)
			return false;
		
		return true;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isSolventAtom(int)
	 */
	@Override
	public boolean isSolventAtom(int index)
	{
		if (index<this.molecule.size())
			return false;
		
		if (distanceToCore(index)<=0.0)
			return false;
		
		return true;
	}

	@Override
	public Iterator<Atom> iterator()
	{
		return new MultiDomainAtomIterator(Arrays.asList(this.molecule, this.solventLayer));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#maxAtomDistance()
	 */
	@Override
	public double maxAtomDistance()
	{
		if (this.solventLayer==null)
			return this.molecule.maxAtomDistance();
	  else
	  	return Math.max(this.molecule.maxAtomDistance(), this.solventLayer.maxAtomDistance());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#size()
	 */
	@Override
	public int size()
	{
  	return this.molecule.size()+this.solventLayer.size();
	}
}

