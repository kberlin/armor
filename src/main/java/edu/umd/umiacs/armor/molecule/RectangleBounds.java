/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Class RectangleBounds.
 */
public final class RectangleBounds implements Serializable
{
  
  /** The lx. */
  public final double lx;

	/** The ly. */
  public final double ly;
  
  /** The lz. */
  public final double lz;
  
  /** The ux. */
  public final double ux;
  
  /** The uy. */
  public final double uy;
  
  /** The uz. */
  public final double uz;
  
  /**
	 * 
	 */
	private static final long serialVersionUID = 7920667790000724081L;
  
  /**
	 * Gets the bounds.
	 * 
	 * @param positionArray
	 *          the position array
	 * @return the bounds
	 */
  public static RectangleBounds getBounds(double[][] positionArray)
  {
		double minX = Double.POSITIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double minZ = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;
		double maxZ = Double.NEGATIVE_INFINITY;

		// store the atom information
		for (int iter = 0; iter < positionArray.length; iter++)
		{
			minX = Math.min(positionArray[iter][0], minX);
			minY = Math.min(positionArray[iter][1], minY);
			minZ = Math.min(positionArray[iter][2], minZ);
			maxX = Math.max(positionArray[iter][0], maxX);
			maxY = Math.max(positionArray[iter][1], maxY);
			maxZ = Math.max(positionArray[iter][2], maxZ);
		}
		
	  return new RectangleBounds(minX,minY,minZ,maxX+1e-4,maxY+1e-4,maxZ+1e-4);
  }
  
  /**
	 * Instantiates a new rectangle bounds.
	 * 
	 * @param lx
	 *          the lx
	 * @param ly
	 *          the ly
	 * @param lz
	 *          the lz
	 * @param ux
	 *          the ux
	 * @param uy
	 *          the uy
	 * @param uz
	 *          the uz
	 */
  public RectangleBounds(double lx, double ly, double lz, double ux, double uy, double uz)
  {
    this.lx = lx;
    this.ly = ly;
    this.lz = lz;
    this.ux = ux;
    this.uy = uy;
    this.uz = uz;
  }
  
  /**
	 * Instantiates a new rectangle bounds.
	 * 
	 * @param lower
	 *          the lower
	 * @param upper
	 *          the upper
	 */
  public RectangleBounds(Point3d lower, Point3d upper)
  {
    this.lx = lower.x;
    this.ly = lower.y;
    this.lz = lower.z;
    this.ux = upper.x;
    this.uy = upper.y;
    this.uz = upper.z;
  	
  }
  
  /**
	 * Instantiates a new rectangle bounds.
	 * 
	 * @param bounds
	 *          the bounds
	 */
  public RectangleBounds(RectangleBounds bounds)
  {
  	this(bounds.lx, bounds.ly, bounds.lz, bounds.ux, bounds.uy, bounds.uz);
  }
  
	/**
	 * Adds the thickness.
	 * 
	 * @param thickness
	 *          the thickness
	 * @return the rectangle bounds
	 */
  public RectangleBounds addThickness(double thickness)
  {
  	return new RectangleBounds(this.lx-thickness,this.ly-thickness,this.lz-thickness,
  														 this.ux+thickness,this.uy+thickness,this.uz+thickness);
  }
	
	/**
	 * Center.
	 * 
	 * @return the point3d
	 */
	public Point3d center()
  {
    double dx = this.lx+(this.ux-this.lx)/2.0;
    double dy = this.ly+(this.uy-this.ly)/2.0;
    double dz = this.lz+(this.uz-this.lz)/2.0;
    
    return new Point3d(dx,dy,dz);
  }
	
	/**
	 * Distance to point.
	 * 
	 * @param x
	 *          the x
	 * @param y
	 *          the y
	 * @param z
	 *          the z
	 * @return the double
	 */
  public double distanceToPoint(double x, double y, double z)
  {
  	return BasicMath.sqrt(distanceToPointSquared(x,y,z));
  }

	
	/**
	 * Distance to point.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
	public double distanceToPoint(Point3d p)
	{
		return distanceToPoint(p.x, p.y, p.z);
	}

	/**
	 * Distance to point squared.
	 * 
	 * @param x
	 *          the x
	 * @param y
	 *          the y
	 * @param z
	 *          the z
	 * @return the double
	 */
  public double distanceToPointSquared(double x, double y, double z)
  {
  	double minDistance = Double.POSITIVE_INFINITY;
  	
  	//if bound then distance is zero
  	if (isBounding(x,y,z))
  	{
      return 0.0;
  	}

  	//see if it is facing any planes
    if (x>=this.lx && x<this.ux)
    {
    	if (y>=this.ly && y<this.uy)
    	{
       	minDistance = BasicMath.euclideanDistanceSquared(z,this.lz);
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(z,this.uz));
    	}
    	else
      if (z>=this.lz && z<this.uz)
      {
       	minDistance = BasicMath.euclideanDistanceSquared(y,this.ly);
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(y,this.uy));
      }
      else
      {
      	minDistance = BasicMath.euclideanDistanceSquared(y,z,this.ly,this.lz);
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(y,z,this.ly,this.uz));
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(y,z,this.uy,this.lz));
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(y,z,this.uy,this.uz));
      }
    }
    else
    if (y>=this.ly && y<this.uy)
    {
      if (z>=this.lz && z<this.uz)
      {
       	minDistance = BasicMath.euclideanDistanceSquared(x,this.lx);
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,this.ux));
      }
      else
      {
      	minDistance = BasicMath.euclideanDistanceSquared(x,z,this.lx,this.lz);
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,z,this.lx,this.uz));
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,z,this.ux,this.lz));
      	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,z,this.ux,this.uz));
      }
    }
    else
    if (z>=this.lz && z<this.uz)
    {
    	minDistance = BasicMath.euclideanDistanceSquared(x,y,this.lx,this.ly);
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,this.lx,this.uy));
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,this.ux,this.ly));
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,this.ux,this.uy));
    }
    else
    {
    	//no dimension fit, so check distance to all 8 points
    	minDistance = BasicMath.euclideanDistanceSquared(x,y,z,this.lx,this.ly,this.lz);
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,z,this.lx,this.ly,this.uz));
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,z,this.lx,this.uy,this.lz));
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,z,this.lx,this.uy,this.uz));
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,z,this.ux,this.ly,this.lz));
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,z,this.ux,this.ly,this.uz));
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,z,this.ux,this.uy,this.lz));
    	minDistance = Math.min(minDistance, BasicMath.euclideanDistanceSquared(x,y,z,this.ux,this.uy,this.uz));
    }
    
    assert minDistance>=0.0 : "Distance to bounds is negative.";
    
   	return minDistance;
  }

  /**
	 * Distance to point squared.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
	public double distanceToPointSquared(Point3d p)
	{
		return distanceToPointSquared(p.x, p.y, p.z);
	}
  
  /**
	 * Gets the lower bound.
	 * 
	 * @return the lower bound
	 */
	public Point3d getLowerBound()
	{
		return new Point3d(this.lx,this.ly,this.lz);
	}
  
  /**
	 * Gets the upper bound.
	 * 
	 * @return the upper bound
	 */
	public Point3d getUpperBound()
	{
		return new Point3d(this.ux,this.uy,this.uz);
	}
  
  /**
	 * Checks if is bounding.
	 * 
	 * @param x
	 *          the x
	 * @param y
	 *          the y
	 * @param z
	 *          the z
	 * @return true, if is bounding
	 */
  public boolean isBounding(double x, double y, double z)
  {
    if (x>=this.lx && x<this.ux && y>=this.ly && y<this.uy && z>=this.lz && z<this.uz)
      return true;
    else
      return false;
  }
    
  /**
	 * Checks if is bounding.
	 * 
	 * @param p
	 *          the p
	 * @return true, if is bounding
	 */
  public boolean isBounding(Point3d p)
  {
  	return isBounding(p.x,p.y,p.z);
  }

  /**
	 * Checks if is bounding.
	 * 
	 * @param b
	 *          the b
	 * @return true, if is bounding
	 */
  public boolean isBounding(RectangleBounds b)
  {
    if (b.lx>=this.lx && b.ux<=this.ux && b.ly>=this.ly && b.uy<=this.uy && b.lz>=this.lz && b.uz<=this.uz)
      return true;
    else
      return false;
  }

  /**
	 * Max distance.
	 * 
	 * @return the double
	 */
  public double maxDistance()
  {
  	return BasicMath.sqrt(BasicMath.euclideanDistanceSquared(this.lx,this.ly,this.lz,this.ux,this.uy,this.uz));
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
	public String toString()
	{
		return new String("["+this.lx+", "+this.ly+", "+this.lz+", "+this.ux+", "+this.uy+", "+this.uz+"]");
	}

	/**
	 * Union.
	 * 
	 * @param bounds
	 *          the bounds
	 * @return the rectangle bounds
	 */
	public RectangleBounds union(RectangleBounds bounds)
  {
  	//create new bounds
  	return new RectangleBounds(Math.min(this.lx, bounds.lx),
  														 Math.min(this.ly, bounds.ly),
  														 Math.min(this.lz, bounds.lz),
  														 Math.max(this.ux, bounds.ux),
  														 Math.max(this.uy, bounds.uy),
  														 Math.max(this.uz, bounds.uz));

  }
}
