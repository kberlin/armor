/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.util.Iterator;
import java.util.List;

import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Class SolventMolecule.
 */
public final class Solvent extends AbstractMolecule
{	
	private Molecule molecule;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4081733815454778230L;
	
	private Solvent(Molecule mol)
	{
		this.molecule = mol;
	}

	public Solvent(List<? extends AtomImpl> atomList) throws DuplicateAtomException
	{
		this.molecule = new BasicMolecule(atomList);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#center()
	 */
	@Override
	public Point3d center()
	{
		return this.molecule.center();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#deuterate(double)
	 */
	@Override
	public Solvent deuterate(double deuteriumFraction)
	{
    return new Solvent(this.molecule.deuterate(deuteriumFraction));
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#distanceToCore(int)
	 */
	@Override
	public double distanceToCore(int index)
	{
		return Double.POSITIVE_INFINITY;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#distanceToCore(edu.umd.umiacs.armor.math.Point3D)
	 */
	@Override
	public double distanceToCore(Point3d p)
	{
		return Double.POSITIVE_INFINITY;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#exists(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public boolean exists(Atom a)
	{
		return this.molecule.exists(a);
	}

	@Override
	public Atom getAtom(int index)
	{
		return this.molecule.getAtom(index);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(edu.umd.umiacs.armor.molecule.Atom)
	 */
	@Override
	public Point3d getAtomPosition(Atom a) throws AtomNotFoundException
	{
		return this.molecule.getAtomPosition(a);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomPosition(int)
	 */
	@Override
	public Point3d getAtomPosition(int index)
	{
		return this.molecule.getAtomPosition(index);
	}

	@Override
	public List<Atom> getAtoms()
	{
		return this.molecule.getAtoms();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomSurfaceRadius(int)
	 */
	@Override
	public double getAtomSurfaceRadius(int index)
	{
		return this.molecule.getAtomSurfaceRadius(index);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getAtomType(int)
	 */
	@Override
	public AtomType getAtomType(int index)
	{
		return this.molecule.getAtomType(index);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getBounds()
	 */
	@Override
	public RectangleBounds getBounds()
	{
		return this.molecule.getBounds();
	}
	
	@Override
	public List<? extends Atom> getCloseAtoms(Point3d p, double maxDistance)
	{
		return this.molecule.getCloseAtoms(p, maxDistance);
	}

	@Override
	public List<? extends Atom> getClosestAtoms(Point3d p, int n)
	{
		return this.molecule.getClosestAtoms(p, n);
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#getSurfaceBounds()
	 */
	@Override
	public RectangleBounds getSurfaceBounds()
	{
		return this.molecule.getSurfaceBounds();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isCloserThan(edu.umd.umiacs.armor.math.Point3D, double)
	 */
	@Override
	public boolean isCloserThan(Point3d p, double distance)
	{
		return this.molecule.isCloserThan(p, distance);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isContrastAtom(int)
	 */
	@Override
	public boolean isDisplacedAtom(int index)
	{
		return false;
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#isSolventAtom(int)
	 */
	@Override
	public boolean isSolventAtom(int index)
	{
		return true;
	}

	@Override
	public Iterator<Atom> iterator()
	{
		return this.molecule.iterator();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#maxAtomDistance()
	 */
	@Override
	public double maxAtomDistance()
	{
		return this.molecule.maxAtomDistance();
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#size()
	 */
	@Override
	public int size()
	{
		return this.molecule.size();
	}
}
