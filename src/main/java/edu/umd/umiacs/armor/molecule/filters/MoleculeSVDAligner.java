/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule.filters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.linear.SingularValueDecomposition;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.Atom;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.DuplicateAtomException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MoleculeRuntimeException;
import edu.umd.umiacs.armor.molecule.VirtualMoleculeWrapper;
import edu.umd.umiacs.armor.util.Pair;

/**
 * The Class MoleculeAlignment.
 */
public final class MoleculeSVDAligner implements Serializable, MoleculeAligner
{
	private final class AtomPair extends Pair<Atom, Atom>
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 6675201562300914425L;

		public AtomPair(Atom atom1, Atom atom2)
		{
			super(atom1, atom2);
		}
	}

	private final String backboneAtomName;
	
	private final boolean firstChainOnly;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7473553051225646276L;
	
	private static RigidTransform SVDAligner(ArrayList<AtomPair> list)
	{
   	ArrayList<Pair<Point3d, Point3d>> pointList = new ArrayList<Pair<Point3d,Point3d>>(list.size());
  	
  	//get the coordinates
  	for (int iter=0; iter< list.size(); iter++)
  	{
  		AtomPair pair = list.get(iter);
  		
  		Point3d p1 = pair.x.getPosition();
  		Point3d p2 = pair.y.getPosition();
  		
  		pointList.add(new Pair<Point3d,Point3d>(p1,p2));
  	}

  	return SVDAligner(pointList);
	}
	
	public static RigidTransform SVDAligner(List<Pair<Point3d,Point3d>> list)
	{
   	double[][] a1 = new double[list.size()][3];
  	double[][] a2 = new double[list.size()][3];
  	
  	//get the coordinates
  	int counter=0;
  	for (Pair<Point3d,Point3d> pair : list)
  	{
  		Point3d p1 = pair.x;
  		Point3d p2 = pair.y;

  		a1[counter][0] = p1.x;
  		a1[counter][1] = p1.y;
  		a1[counter][2] = p1.z;
  		a2[counter][0] = p2.x;
  		a2[counter][1] = p2.y;
  		a2[counter][2] = p2.z;
  		
  		counter++;
  	}
  	  	
  	double[] c1 = BasicStat.meanColumns(a1);
  	double[] c2 = BasicStat.meanColumns(a2);
  	
  	//center atoms to origin
  	for (int iter=0; iter< a1.length; iter++)
  	{
  		a1[iter][0] -= c1[0];
  		a1[iter][1] -= c1[1];
  		a1[iter][2] -= c1[2];
  		a2[iter][0] -= c2[0];
  		a2[iter][1] -= c2[1];
  		a2[iter][2] -= c2[2];
  	}
 	
  	//compute the covarience matrix
  	double[][] c = new double[3][3];
  	for (int row=0; row<a1[0].length; row++)
  	{
    	for (int col=0; col<a1[0].length; col++)
    	{
    		c[row][col] = 0;
    		for (int iter=0; iter<a1.length; iter++)
    			c[row][col]+= a1[iter][row]*a2[iter][col];
    	}
  	}
  	
  	SingularValueDecomposition svd = new BlockRealMatrix(c).svd();
  	RealMatrix Vt = svd.getVT();
  	RealMatrix U = svd.getU();
  	
  	//zero the matrix
  	double[][] invS = new double[3][3];
  	for (int row=0; row<invS.length; row++)
    	for (int col=0; col<invS[row].length; col++)
    		invS[row][col] = 0.0;
  	
  	//set the diagnal elements
  	invS[0][0] = 1.0;
  	invS[1][1] = 1.0;
  	invS[2][2] = BasicMath.det(c)>0.0 ? 1.0 : -1.0;
  	
  	double[][] W = U.mult(new Array2dRealMatrix(invS, true).mult(Vt)).toArray();
  	if (BasicMath.det(W)<0.0)
  	{
  		invS[2][2] = -invS[2][2];
  		W = U.mult(new Array2dRealMatrix(invS, true).mult(Vt)).toArray();
  	}
  	
  	//compute the shift after rotation
  	Point3d shift = new Point3d(BasicMath.subtract(c1, BasicMath.mult(W, c2)));
  	
  	RigidTransform t = new RigidTransform(new Rotation(W), shift);
  	
  	return t;
	}
	
	public MoleculeSVDAligner()
	{
		this.backboneAtomName = null;
		this.firstChainOnly = false;
	}
	
	public MoleculeSVDAligner(String backboneAtomName)
	{
		this(backboneAtomName, false);
	}
	
	public MoleculeSVDAligner(String backboneAtomName, boolean firstChainOnly)
	{
		this.backboneAtomName = backboneAtomName;
		this.firstChainOnly = firstChainOnly;
	}
	
	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.MoleculeAligner#align(edu.umd.umiacs.armor.molecule.Molecule, edu.umd.umiacs.armor.molecule.Molecule)
	 */
	@Override
	public Molecule align(Molecule toMolecule, Molecule targetMolecule) throws MoleculeAlignmentException
  {
		if (toMolecule==targetMolecule)
			return targetMolecule;
		
  	VirtualMoleculeWrapper targetAligned = new VirtualMoleculeWrapper(targetMolecule);
  	RigidTransform t = alignmentTransform(toMolecule, targetMolecule);
  	targetAligned = targetAligned.createTransformedMolecule(t);
  	
  	return targetAligned;
  }
  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.MoleculeAligner#alignedRmsd(edu.umd.umiacs.armor.molecule.Molecule, edu.umd.umiacs.armor.molecule.Molecule)
	 */
  @Override
	public double alignedRmsd(Molecule mol1, Molecule mol2) throws MoleculeAlignmentException
  {
  	Molecule mol2Adj = align(mol1, mol2);
  	
  	double[] dist = new double[mol1.size()];
  	
  	for (int iter=0; iter<dist.length; iter++)
  		dist[iter] = mol1.getAtomPosition(iter).distance(mol2Adj.getAtomPosition(iter));
  	
  	return BasicStat.rms(dist);
  }
  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.MoleculeAligner#alignmentTransform(edu.umd.umiacs.armor.molecule.Molecule, edu.umd.umiacs.armor.molecule.Molecule)
	 */
  @Override
	public RigidTransform alignmentTransform(Molecule m1, Molecule m2) throws MoleculeAlignmentException
  {
  	ArrayList<AtomPair> matchingList = matchingAtoms(m1, m2);
  	
  	if (matchingList.size()<3)
  		throw new MoleculeAlignmentException("Not enough matching atoms exist between molecules to align.");
  
    return SVDAligner(matchingList);
  }
  
  @Override
	public final Molecule requiredSubObject(Molecule mol)
  {
  	if (!this.firstChainOnly && this.backboneAtomName==null)
  		return mol;
  	
  	String firstChain = null;
  	if (this.firstChainOnly)
  	{
			Atom infoAtom = mol.getAtom(0);
			firstChain = infoAtom.getPrimaryInfo().getChainID();
  	}

  	ArrayList<Atom> atomList = new ArrayList<Atom>();
		for (Atom a : mol)
		{
			if (!this.firstChainOnly || a.getPrimaryInfo().getChainID().equals(firstChain))
			{
				if (this.backboneAtomName==null || a.getPrimaryInfo().getAtomName().equals(this.backboneAtomName))
					atomList.add(a);
			}
		}
			
		if (atomList.isEmpty())
			throw new MoleculeRuntimeException("No atoms found for structure alignment.");
		
		Molecule newMol;
		try
		{
			newMol = new BasicMolecule(atomList);
		}
		catch (DuplicateAtomException e)
		{
			throw new MoleculeRuntimeException("Unexpected duplicate atom found.");
		}

		return newMol;
  }
  
  private final ArrayList<AtomPair> matchingAtoms(Molecule m1, Molecule m2)
	{
  	String firstChain = null;
  	if (this.firstChainOnly)
  	{
			Atom infoAtom = m1.getAtom(0);
			firstChain = infoAtom.getPrimaryInfo().getChainID();
  	}

		ArrayList<AtomPair> matchingList = new ArrayList<AtomPair>(m1.size());
		for (Atom a1 : m1)
		{
			if (!this.firstChainOnly || a1.getPrimaryInfo().getChainID().equals(firstChain))
			{
				if (this.backboneAtomName==null || a1.getPrimaryInfo().getAtomName().equals(this.backboneAtomName))
				{			
					try
					{
						Atom a2 = a1.createWithNewPosition(m2.getAtomPosition(a1));
						matchingList.add(new AtomPair(a1,a2));
						
					}
					catch (AtomNotFoundException e)
					{
					}
				}
			}
		}
		
		if (matchingList.isEmpty())
			throw new MoleculeRuntimeException("No common atoms found for structure alignment.");
		
		return matchingList;
	}
  
  /* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.MoleculeAligner#rmsd(edu.umd.umiacs.armor.molecule.Molecule, edu.umd.umiacs.armor.molecule.Molecule)
	 */
  @Override
	public double rmsd(Molecule mol1, Molecule mol2)
  {
  	ArrayList<AtomPair> matchingList = matchingAtoms(mol1, mol2);
  	
  	if (matchingList.isEmpty())
  		throw new MoleculeRuntimeException("Molecules have no matching atoms.");
  	
  	double[] distSquared = new double[matchingList.size()];
  	int iter=0;
  	for (AtomPair pair : matchingList)
  	{
  		distSquared[iter] = pair.x.distanceToCenterSquared(pair.y);
  		iter++;
  	}
  	
  	return BasicStat.rmsOfSquares(distSquared);
  }
  
	@Override
	public double distance(Molecule a, Molecule b)
	{
		return rmsd(a,b);
	}

	@Override
	public double distanceSquard(Molecule a, Molecule b)
	{
		return BasicMath.square(rmsd(a,b));
	}

}
