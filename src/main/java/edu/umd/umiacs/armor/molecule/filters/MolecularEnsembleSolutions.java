/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule.filters;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.optim.LinearSolution;
import edu.umd.umiacs.armor.math.optim.LinearSolutions;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.refinement.LinearEnsembleInput;
import edu.umd.umiacs.armor.refinement.MultiInputLinearSolution;
import edu.umd.umiacs.armor.refinement.RefinementRuntimeException;
import edu.umd.umiacs.armor.util.ExperimentalData;
import edu.umd.umiacs.armor.util.ExperimentalDatum;
import edu.umd.umiacs.armor.util.FileIO;
import edu.umd.umiacs.armor.util.Pair;

public class MolecularEnsembleSolutions implements Serializable
{
	private LinearEnsembleInput input;
	private ConcurrentHashMap<Pair<Integer,Integer>, Molecule> moleculeHash;
	private final LinearSolutions solutions;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1587584007766344793L;
	
	public static MolecularEnsembleSolutions read(File file) throws IOException, ClassNotFoundException
	{
		MolecularEnsembleSolutions sol = (MolecularEnsembleSolutions) FileIO.readObject(file);
		
		return sol;
	}
	
	public static MolecularEnsembleSolutions read(String file) throws IOException, ClassNotFoundException
	{
		return read(new File(file));
	}
	
	public MolecularEnsembleSolutions(LinearEnsembleInput input, LinearSolutions solutions)
	{
		this.input = input;
		
		//sort the solutions
		this.solutions = solutions;

		//will be used to cache the file, so repeat calls to generate solution will not open file twice
		clearCache();
	}
	
	private void cacheStructures(List<LinearSolution> solutions) throws PdbException
	{
		final HashSet<Integer> totalColumns = new HashSet<Integer>();
		final HashSet<Integer> totalFiles = new HashSet<Integer>();
		for (LinearSolution solution : solutions)
		{
			for (int column : solution.getActiveColumns())
			{
				totalColumns.add(column);
				totalFiles.add(this.input.getModelFileIndex(column).x);
			}
		}
		
  	ExecutorService execSvc = Executors.newSingleThreadExecutor();
		final List<Exception> exceptions = Collections.synchronizedList(new ArrayList<Exception>());
		final List<PdbException> pdbExceptions = Collections.synchronizedList(new ArrayList<PdbException>());
	
		//for now let it be, if the pdb reader improves uncomment this code
    //PrintStream oldErrorStream = System.err;
    //synchronized(System.err)
    //{	    	
	    //ByteArrayOutputStream pipeOut = new ByteArrayOutputStream();
    	//System.setErr(new PrintStream(pipeOut));
    //}
	
		for (int fileIndex : totalFiles)
		{
			boolean needToLoad = false;
			for (int column : totalColumns)
			{
				Pair<Integer, Integer> currColumnInfo = MolecularEnsembleSolutions.this.input.getModelFileIndex(column);
				if (fileIndex==currColumnInfo.x && !this.moleculeHash.containsKey(currColumnInfo))
				{
					needToLoad = true;
					break;
				}
			}
			
			//if the file still needs to be read
			if (needToLoad)
			{
				//two threads will never read from the same file, no need for locking anywhere
				final int currentFileIndex = fileIndex;
	  		Runnable task = new Runnable()
				{					
					@Override
					public void run()
					{
						try
						{
							//don't bother doing work if already failed
							if (!pdbExceptions.isEmpty() || !exceptions.isEmpty())
								return;
							
							//for now let it be, if the pdb reader improves uncomment this code
							//PdbStructure cachedPdb = PdbFileIO.readPDBUnlocked(MolecularEnsembleSolutions.this.input.getPdbFile(currentFileIndex));
							PdbStructure cachedPdb = MoleculeFileIO.readPDB(MolecularEnsembleSolutions.this.input.getPdbFile(currentFileIndex));
							
							//process all the columns that came from the same file
							for (int column : totalColumns)
							{
								Pair<Integer, Integer> currColumnInfo = MolecularEnsembleSolutions.this.input.getModelFileIndex(column);
	
								if (currentFileIndex==currColumnInfo.x && !MolecularEnsembleSolutions.this.moleculeHash.containsKey(currColumnInfo))
								{
									MolecularEnsembleSolutions.this.moleculeHash.put(currColumnInfo, cachedPdb.getModel(currColumnInfo.y).createBasicMolecule());
								}									
							}
						}
						catch(PdbException e1)
						{
							pdbExceptions.add(e1);
						}
						catch (Exception e2)
						{
							exceptions.add(e2);
						}
					}
				};
				
		  	//enqueue the task
				execSvc.execute(task);	

			}
		}
				
		//shutdown the service
    execSvc.shutdown();
    try
		{
			execSvc.awaitTermination(30L, TimeUnit.DAYS);
    	//System.setErr(oldErrorStream);
		} 
    catch (InterruptedException e) 
    {
    	//System.setErr(oldErrorStream);
    	execSvc.shutdownNow();
    	throw new RefinementRuntimeException("Unable to finish all tasks.", e);
    }
    
    //handle exceptions
    if (!pdbExceptions.isEmpty())
    	throw pdbExceptions.get(0);
    
    if (!exceptions.isEmpty())
    	throw new RefinementRuntimeException("Unexpected exception.", exceptions.get(0));
	}
	
	private void clearCache()
	{
		this.moleculeHash = new ConcurrentHashMap<Pair<Integer,Integer>, Molecule>();					
	}
	
	private <T extends ExperimentalDatum> LinearSolutions createNewSolution(ExperimentalData<T> expData, String sourceName)
	{
		ArrayList<MultiInputLinearSolution<T>> list = new ArrayList<MultiInputLinearSolution<T>>();

		for (LinearSolution sol : this.solutions)
		{
			list.add(new MultiInputLinearSolution<T>(sol, expData, sourceName));
		}
		
		LinearSolutions newSolutions = new LinearSolutions(list);
				
		return newSolutions;
	}
	
	public MolecularEnsembleSolutions createWithSourceName(String sourceName)
	{
		LinearSolutions newSolution = createNewSolution(this.input.getExperimentalData(), sourceName);
		
		return new MolecularEnsembleSolutions(this.input, newSolution);
	}

	public MolecularEnsemble generateBestEnsemble(int l0Norm) throws AtomNotFoundException, PdbException
	{
		LinearSolution solution = this.solutions.getBest(l0Norm);
		
		synchronized (this.moleculeHash)
		{
			MolecularEnsemble ensemble = generateEnsemble(solution);
			
			clearCache();

			return ensemble;
		}
	}
	
	private MolecularEnsemble generateEnsemble(LinearSolution solution) throws AtomNotFoundException, PdbException
	{
		//cache the structures
		ArrayList<LinearSolution> structArray = new ArrayList<LinearSolution>(1);
		structArray.add(solution);
		
		ArrayList<Molecule> solutionMoleculeList = new ArrayList<Molecule>();
		cacheStructures(structArray);
		
		//generate the solution
		for (int column : solution.getOrderedActiveColumns())
		{
			Molecule cachedMol = this.moleculeHash.get(this.input.getModelFileIndex(column));
			
			if (cachedMol==null)
				throw new RefinementRuntimeException("Unexpected file caching failure.");
			
			solutionMoleculeList.add(cachedMol);
		}
		
		//Note that cached should be cleared in the calling function
		
		return new MolecularEnsemble(solutionMoleculeList, 
				solution.getOrderedActiveWeights(),
				solution.getOrderedActiveColumns(),
				solution.absoluteError()*solution.absoluteError());
	}
	
	public ArrayList<MolecularEnsemble> generateEnsembleClusters(List<LinearSolution> l0Solutions, MoleculeAligner aligner, double rmsdCutoff) throws AtomNotFoundException, PdbException, MoleculeAlignmentException
	{
		return generateEnsembleClusters(l0Solutions, aligner, rmsdCutoff, null);
	}
	
	public ArrayList<MolecularEnsemble> generateEnsembleClusters(List<LinearSolution> l0Solutions, MoleculeAligner aligner, double rmsdCutoff, MoleculeAligner postAligner) throws AtomNotFoundException, PdbException, MoleculeAlignmentException
	{
		ArrayList<MolecularEnsemble> solList = new ArrayList<MolecularEnsemble>();

		synchronized (this.moleculeHash)
		{
			//cache the list of solution
			cacheStructures(l0Solutions);
			
			//add the structures
			Molecule targetMolecule = null;
			for (LinearSolution sol : l0Solutions)
			{
				MolecularEnsemble ensemble = generateEnsemble(sol);
				
				//align the structures in output, if requested
				if (postAligner!=null)
				{
					if (targetMolecule==null)
						targetMolecule = ensemble.getConformer(0);
					
					ensemble = ensemble.align(postAligner, targetMolecule);
				}
				
				solList.add(ensemble);
			}
			
			clearCache();
		}

		return MolecularEnsemble.clusterEnsembles(solList, aligner, rmsdCutoff);
	}
		
	public PdbStructure generateEnsemblePdb(MolecularEnsemble ensemble) throws AtomNotFoundException, PdbException
	{
		//get example pdb
		PdbStructure pdb = MoleculeFileIO.readPDB(this.input.getPdbFile(0));
		
		//generate the pdb
		PdbStructure pdbOutput = pdb.createFromMoleculeList(ensemble.getConformerList());
		
		StringBuilder str = new StringBuilder();
		str.append("ARMOR ENSEMBLE: ");
		str.append(String.format("CHI^2: %.4f,                 ", ensemble.getChi2()));
		str.append(String.format("RELATIVE ERR.: %.4f,                 ", ensemble.getRelativeError(this.input.yNorm())));
		str.append("   ENSEMBLE_WEIGHTS: [");
		for (double weight : ensemble.getWeights())
			str.append(String.format("%.4e ", weight));
		str.append("]");
		str.append("   ENSEMBLE_NORMALIZED_WEIGHTS: [");
		for (double weight : ensemble.getNormalizedWeights())
			str.append(String.format("%.4f ", weight));
		str.append("]");		
		str.append("   COLUMNS: [");
		//add +1 to reorder columns
		for (int columns : ensemble.getColumns())
			str.append(String.format("%d ", columns+1));
		str.append("]");

		//add cluster information
		str.append("                                                        CLUSTER STAT: ");
		str.append(String.format("#CLUSTERS: %d, ", ensemble.clusterSize()));
		str.append("MEAN NORMAL WEIGHTS: [");
		for (int iter=0; iter<ensemble.size(); iter++)
			str.append(String.format("%.4f ", BasicStat.mean(ensemble.getClusterColumnNormalWeights(iter))));
		str.append("], ");
		str.append("STD OF NORMAL WEIGHTS: [");
		for (int iter=0; iter<ensemble.size(); iter++)
			str.append(String.format("%.4f ", BasicStat.std(ensemble.getClusterColumnNormalWeights(iter))));
		str.append("], ");
		
		pdbOutput.setTitle(str.toString());

		return pdbOutput;
	}

	public LinearEnsembleInput getInput()
	{
		return this.input;
	}
		
	public LinearSolutions getSolutions()
	{
		return this.solutions;
	}
	
	public double[] getSolutionValues(double[] x)
	{
		double[] values = this.input.getA().mult(new BlockRealMatrix(x)).getColumn(0);
		
		values = BasicMath.mult(values, this.input.getExperimentalData().errors());
		
		return values;
	}
	
	public ArrayList<LinearSolution> getTopAbsoluteSolutions(int l0, double absError)
	{
		List<LinearSolution> topSolutions = this.solutions.getSolutions(l0);
		
		if (topSolutions==null || topSolutions.isEmpty())
			return null;

		double err = topSolutions.get(0).absoluteError()+absError;
		
		ArrayList<LinearSolution> solutions = new ArrayList<LinearSolution>();
		for (LinearSolution solution : topSolutions)
		{
			if (solution.absoluteError()<=err)
			{
				solutions.add(solution);
			}
		}
		
		return solutions;
	}	
	
	public ArrayList<LinearSolution> getTopRelativeSolutions(int l0, double relError)
	{
		return getTopAbsoluteSolutions(l0, relError*this.input.yNorm());
	}

	public int maxL0()
	{
		return this.solutions.maxL0();
	}

	public int maxL0(double minTol)
	{
		if (getSolutions().isEmpty())
			return 0;
		
		double bestSol = getSolutions().getBest().absoluteError();
		int maxL0 = this.solutions.maxL0();
		
		for (int l0=1; l0<maxL0; l0++)
		{
			double currValue = this.solutions.getBest(l0).absoluteError();
			if (currValue<bestSol*(1.0+minTol))
				return l0;
		}
		
		return maxL0;
	}			

	public void save(File file) throws FileNotFoundException, IOException
	{
		synchronized (this.moleculeHash)
		{
			//will be used to cache the file, so repeat calls to generate solution will not open it
			clearCache();
			
			FileIO.writeObject(file, this);
		}
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		
		str.append("Best solutions by l0-norm (column index starts at 1):\n");
		int maxL0 = this.solutions.maxL0();
		
		for (int l0=1; l0<=maxL0; l0++)
		{
			List<LinearSolution> solutions = this.solutions.getSolutions(l0);
			
			str.append("l0-norm="+l0+": "+solutions.get(0)+"\n");
		}
		
		return str.toString();
	}
		
	public String toString(double yNorm)
	{
		StringBuilder str = new StringBuilder();
		
		str.append("Best solutions by l0-norm (column index starts at 1):\n");
		int maxL0 = this.solutions.maxL0();
		
		for (int l0=1; l0<=maxL0; l0++)
		{
			List<LinearSolution> solutions = this.solutions.getSolutions(l0);
			
			if (!solutions.isEmpty())
				str.append("l0-norm="+l0+": "+solutions.get(0).toString(yNorm)+"\n");
		}
		
		str.append("\nL-curve: [");
		
		for (int l0=1; l0<=maxL0; l0++)
		{
			List<LinearSolution> solutions = this.solutions.getSolutions(l0);
			
			if (!solutions.isEmpty())
				str.append(solutions.get(0).absoluteError()/yNorm);
			
			if (l0<=maxL0-1)
				str.append(", ");
		}
		str.append("]\n");
		
		return str.toString();
	}
}
