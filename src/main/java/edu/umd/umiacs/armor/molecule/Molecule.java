/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;
import java.util.List;

import edu.umd.umiacs.armor.math.Point3d;

/**
 * The Interface Molecule.
 */
public interface Molecule extends Serializable, Iterable<Atom>
{
	/**
	 * Center.
	 * 
	 * @return the point3d
	 */
	public Point3d center();

	public Molecule combine(List<? extends Atom> atomList) throws DuplicateAtomException;
	
	public Molecule combine(Molecule molecule) throws DuplicateAtomException;

  public Molecule deuterate(double deuteriumFraction);

	/**
	 * Distance to core.
	 * 
	 * @param index
	 *          the index
	 * @return the double
	 */
  public double distanceToCore(int index);

	/**
	 * Distance to core.
	 * 
	 * @param p
	 *          the p
	 * @return the double
	 */
  public double distanceToCore(Point3d p);

	/**
	 * Exists.
	 * 
	 * @param a
	 *          the a
	 * @return true, if successful
	 */
	public boolean exists(Atom a);
		
	public Atom getAtom(int index);
	
	/**
	 * Gets the atom position.
	 * 
	 * @param a
	 *          the a
	 * @return the atom position
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public Point3d getAtomPosition(Atom a) throws AtomNotFoundException;

	/**
	 * Gets the atom position.
	 * 
	 * @param index
	 *          the index
	 * @return the atom position
	 */
	public Point3d getAtomPosition(int index);

	/**
	 * Gets the atom position array.
	 * 
	 * @return the atom position array
	 */
	public double[][] getAtomPositionArray();
  
  /**
	 * Gets the atom position single array.
	 * 
	 * @return the atom position single array
	 */
  public double[] getAtomPositionSingleArray();

  public List<Atom> getAtoms();

	/**
	 * Gets the atom surface radius.
	 * 
	 * @param index
	 *          the index
	 * @return the atom surface radius
	 */
	public double getAtomSurfaceRadius(int index);
	
	//public Atom getAtom(int index);
	/**
	 * Gets the atom type.
	 * 
	 * @param index
	 *          the index
	 * @return the atom type
	 */
	public AtomType getAtomType(int index);
	
  //public boolean isColliding(Atom a);
	
  /**
	 * Gets the bounds.
	 * 
	 * @return the bounds
	 */
  public RectangleBounds getBounds();
  
  public List<? extends Atom> getCloseAtoms(Point3d p, double maxDistance);

  public List<? extends Atom> getClosestAtoms(double x, double y, double z, int n);

  public List<? extends Atom> getClosestAtoms(Point3d p, int n);

  /**
	 * Gets the surface bounds.
	 * 
	 * @return the surface bounds
	 */
	public RectangleBounds getSurfaceBounds();  

	/**
	 * Checks if is closer than.
	 * 
	 * @param p
	 *          the p
	 * @param distance
	 *          the distance
	 * @return true, if is closer than
	 */
	public boolean isCloserThan(Point3d p, double distance);
  
  /**
	 * Checks if is displaced atom.
	 * 
	 * @param index
	 *          the index
	 * @return true, if is displaced atom
	 */
	public boolean isDisplacedAtom(int index);

  /**
	 * Checks if is solvent atom.
	 * 
	 * @param index
	 *          the index
	 * @return true, if is solvent atom
	 */
  public boolean isSolventAtom(int index);

  /**
	 * Max atom distance.
	 * 
	 * @return the double
	 */
  public double maxAtomDistance();
  
  public double maxAtomDistanceExact();

  /**
	 * Size.
	 * 
	 * @return the int
	 */
	public int size();
}