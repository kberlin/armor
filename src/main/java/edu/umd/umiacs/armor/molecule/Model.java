/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2012 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import edu.umd.umiacs.armor.util.Pair;

/**
 * The Class Model.
 */
public final class Model implements Serializable
{
	
	private final ArrayList<Atom> atoms;	
	private final HashMap<Atom,Atom> atomsHashMap;
	private final ArrayList<Atom> hetAtoms;	
	private final HashMap<Atom,Atom> hetAtomsHashMap;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5964964382046494538L;

	/**
	 * Instantiates a new model.
	 * 
	 * @param new ArrayList<Atom> list
	 *          the atoms
	 */
	public Model(List<? extends Atom> list, List<? extends Atom> hetList)
	{
		this.atoms = new ArrayList<Atom>(list.size());
		this.atomsHashMap = new HashMap<Atom,Atom>(list.size());
		
		this.hetAtoms = new ArrayList<Atom>();
		this.hetAtomsHashMap = new HashMap<Atom,Atom>();
		
		//prevent addition of duplicate atoms
		for (Atom a : list)
		{
			if (!this.atomsHashMap.containsKey(a))
			{
				this.atomsHashMap.put(a, a);
				this.atoms.add(a);
			}
		}
		
		//prevent addition of duplicate atoms
		if (hetList != null)
		{
			for (Atom a : hetList)
			{
				if (!this.hetAtomsHashMap.containsKey(a))
				{
					this.hetAtomsHashMap.put(a, a);
					this.hetAtoms.add(a);
				}
			}
		}
	}
	
	public BasicMolecule createBasicHetMolecule() throws PdbException
	{
		try
		{
			if (this.hetAtoms.isEmpty())
				return null;
			
			return new BasicMolecule(this.hetAtoms);
		}
		catch (DuplicateAtomException e)
		{
			throw new PdbException(e);
		}
	}
	
	public BasicMolecule createBasicMolecule() throws PdbException
	{
		try
		{
			return new BasicMolecule(this.atoms);
		}
		catch (DuplicateAtomException e)
		{
			throw new PdbException(e);
		}
	}
		
	public BasicMolecule createBasicMolecule(List<Pair<String,Integer>> excludedList) throws PdbException 
	{
		ArrayList<Atom> atomList = this.atoms;
		if (excludedList!=null && excludedList.size()>0)
		{
			HashSet<Pair<String, Integer>> excludedHash = new HashSet<Pair<String, Integer>>(excludedList);			
			atomList = new ArrayList<Atom>();
			
			for (Atom atom : this.atoms)
			{
				if (!excludedHash.contains(
						new Pair<String,Integer>(atom.getPrimaryInfo().getChainID(),atom.getPrimaryInfo().getResidueNumber())))
				{
					atomList.add(atom);
				}
			}
		}
		
		try
		{
			return new BasicMolecule(atomList);
		}
		catch (DuplicateAtomException e)
		{
			throw new PdbException(e);
		}
	}
	
	/**
	 * Creates the chain.
	 * 
	 * @param index
	 *          the index
	 * @return the model
	 */
	public Model createChain(int index)
	{
		ArrayList<String> chains = getModelChains();

		ArrayList<Atom> chainAtoms = new ArrayList<Atom>();
		ArrayList<Atom> chainHetAtoms = new ArrayList<Atom>();
		
		for (Atom atom : this.atoms)
			if (atom.getPrimaryInfo().getChainID().equals(chains.get(index)))
				chainAtoms.add(atom);
			
		for (Atom atom : this.hetAtoms)
			if (atom.getPrimaryInfo().getChainID().equals(chains.get(index)))
				chainHetAtoms.add(atom);

		return new Model(chainAtoms, chainHetAtoms);
	}
	
	public Model createChain(String name)
	{
		ArrayList<String> chains = getModelChains();

		int index = chains.indexOf(name);
		
		if (index<0)
			throw new MoleculeRuntimeException("Chain name not found: "+name+".");
		
		return createChain(index);
	}
	
	public Atom get(Atom atom)
	{
		Atom a = this.atomsHashMap.get(atom);
		
		if (a==null)
			return this.hetAtomsHashMap.get(atom);
		
		return a;
	}
	
	public Atom get(int index)
	{
		if (index<this.atoms.size())
			return this.atoms.get(index);
		else
			return this.hetAtoms.get(index-this.atoms.size());
	}
	
	public Atom getAtom(int index)
	{
		return this.atoms.get(index);
	}

	public Atom getHetAtom(int index)
	{
		return this.hetAtoms.get(index);
	}
	
	/**
	 * Gets the model bond.
	 * 
	 * @param bond
	 *          the bond
	 * @return the model bond
	 * @throws AtomNotFoundException
	 *           the atom not found exception
	 */
	public Bond getModelBond(Bond bond) throws AtomNotFoundException
	{
		int atom1Index = this.atoms.indexOf(bond.getFromAtom());
		if (atom1Index<0)
			throw new AtomNotFoundException("Could not find in the model atom: "+bond.getFromAtom(),bond.getFromAtom());
		
		int atom2Index = this.atoms.indexOf(bond.getToAtom());
		if (atom2Index<0)
			throw new AtomNotFoundException("Could not find in the model atom: "+bond.getToAtom(),bond.getToAtom());
		
		return new Bond(this.atoms.get(atom1Index),this.atoms.get(atom2Index));
	}
	
	/**
	 * Gets the model chains.
	 * 
	 * @return the model chains
	 */
	public ArrayList<String> getModelChains()
	{
		ArrayList<String> chains = new ArrayList<String>();
		for (Atom atom : this.atoms)
		{
			int index = chains.indexOf(atom.getPrimaryInfo().getChainID());
			if (index<0)
				chains.add(atom.getPrimaryInfo().getChainID());
		}
		
		Collections.sort(chains);
		
		return chains;
	}
	
	protected void set(int index, Atom atom)
	{
		if (index<this.atoms.size())
		{
			this.atoms.set(index, atom);
			this.atomsHashMap.put(atom, atom);
		}
		else
		{
			this.hetAtoms.set(index-this.atoms.size(), atom);
			this.hetAtomsHashMap.put(atom, atom);
		}
	}

	public int size()
	{
		return this.atoms.size()+this.hetAtoms.size();
	}
	
	public int sizeAtoms()
	{
		return this.atoms.size();
	}
	
	public int sizeHetAtoms()
	{
		return this.hetAtoms.size();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		
		for (Atom a: this.atoms)
			str.append(a.toString()+"\n");
		
		return str.toString();
	}
}
