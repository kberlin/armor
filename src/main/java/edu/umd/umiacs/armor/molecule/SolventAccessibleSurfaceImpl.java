/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;
import edu.umd.umiacs.armor.util.BasicUtils;
import edu.umd.umiacs.armor.util.Pair;

public class SolventAccessibleSurfaceImpl implements SolventAccessibleSurface
{
	private class SamplePositions
	{
		public final double[] xpos;
		public final double[] ypos;
		public final double[] zpos;
		
		public SamplePositions(double[] xpos, double[] ypos, double[] zpos)
		{
			this.xpos = xpos;
			this.ypos = ypos;
			this.zpos = zpos;
		}
	}
	
	private final Molecule mol;

	private final double shellRadius;
	
	private HashMap<Integer, SamplePositions> spherePositionHash;

	private final double waterRadius;

	private static final double DEFAULT_ACCESS_SPHERES_DENSITY = 200;
	
	public final static double DEFAULT_SHELL_RADIUS = 0.0;

	private static final double DEFAULT_SURFACE_SPHERES_DENSITY = 35;
	
	public final static double DEFAULT_WATER_BALL_RADIUS = 1.4;
	
	public static boolean isColliding(List<? extends Atom> atoms, Point3d p, double radius)
	{
		for (Atom a : atoms)
		{
			if (a.distanceToSurface(p)<radius)
				return true;
		}
		
		return false;
	}
	
	public SolventAccessibleSurfaceImpl(Molecule mol)
	{
		this(mol, DEFAULT_WATER_BALL_RADIUS, DEFAULT_SHELL_RADIUS);
	}
	
	public SolventAccessibleSurfaceImpl(Molecule mol, double waterRadius)
	{
		this(mol, waterRadius, DEFAULT_SHELL_RADIUS);
	}

	public SolventAccessibleSurfaceImpl(Molecule mol, double waterRadius, double shellRadius)
	{
		this.mol = mol;
		this.waterRadius = waterRadius;
		this.shellRadius = shellRadius;
		this.spherePositionHash = new HashMap<Integer, SolventAccessibleSurfaceImpl.SamplePositions>(20);
	}
	
	public List<Pair<Point3d,Point3d>> atomWaterPoints(Point3d position, double atomRadius, double sphereDensity)
	{
		//generate uniform sampling of a sphere
		int numberOfPoints = (int)Math.floor(BasicMath.square(atomRadius+this.waterRadius)*Math.sqrt(2.0*sphereDensity)/BasicMath.PI);		
		SamplePositions samplePoints = getSamplePositions(numberOfPoints);
		
		//allocate the storage
 		ArrayList<Pair<Point3d,Point3d>> waterCenterList = new ArrayList<Pair<Point3d,Point3d>>(samplePoints.xpos.length);
		double radius = atomRadius+this.waterRadius+0.01;
		
		//for all sample points compute actual position
		for (int angleIter=0; angleIter<samplePoints.xpos.length; angleIter++)
		{
			Point3d waterPoint = new Point3d(position.x+samplePoints.xpos[angleIter]*radius, 
					position.y+samplePoints.ypos[angleIter]*radius,
					position.z+samplePoints.zpos[angleIter]*radius);
			Point3d surfacePoint = new Point3d(position.x+samplePoints.xpos[angleIter]*atomRadius, 
					position.y+samplePoints.ypos[angleIter]*atomRadius,
					position.z+samplePoints.zpos[angleIter]*atomRadius);
			
			waterCenterList.add(new Pair<Point3d,Point3d>(waterPoint, surfacePoint));
		}
		
		return waterCenterList;
	}

	private synchronized SamplePositions getSamplePositions(int numberOfPoints)
	{
		SamplePositions sample = this.spherePositionHash.get(numberOfPoints);
		
		if (sample==null)
		{		
			double[] alphaAngles = BasicUtils.uniformLinePointsExclusive(0.0, BasicMath.TWOPI, numberOfPoints);
			double[] beta = BasicUtils.uniformLinePointsExclusive(-1.0, 1.0, numberOfPoints/2);
			
			double[] xOffset = new double[alphaAngles.length*beta.length];
			double[] yOffset = new double[alphaAngles.length*beta.length];
			double[] zOffset = new double[alphaAngles.length*beta.length];
			
			//convert to cartesian frame
			int count = 0;
			for (double a : alphaAngles)
				for (double b : beta)
				{
					double sb = BasicMath.sqrt(1.0-b*b);
					xOffset[count] = BasicMath.cos(a)*sb;
					yOffset[count] = BasicMath.sin(a)*sb;
					zOffset[count] = b;
					
					count++;
				}
			
			sample = new SamplePositions(xOffset, yOffset, zOffset);
			this.spherePositionHash.put(numberOfPoints, sample);
		}
		
		return sample;
	}
	
	@Override
	public double percentAccessibleSurface(int index)
	{
		Atom atom = this.mol.getAtom(index);
		double surfaceRadius = atom.radius()+this.shellRadius;
		
		//get the potential water points
		List<Pair<Point3d,Point3d>> waterPoints = atomWaterPoints(atom.getPosition(), surfaceRadius, DEFAULT_ACCESS_SPHERES_DENSITY);
		
		//find points that do not collide
		int count = 0;
		for (Pair<Point3d,Point3d> point : waterPoints)
			if (!this.mol.isCloserThan(point.x, this.waterRadius+this.shellRadius))
				count++;
					
		return (double)count/(double)waterPoints.size();
	}
	
	@Override
	public List<Point3d> surfacePoints()
	{
		ArrayList<Point3d> points = new ArrayList<Point3d>();
		for (int index=0; index<this.mol.size(); index++)
			points.addAll(surfacePoints(index));
		
		//filter out the points
		OcTree<Sphere> atomTree = new OcTree<Sphere>(this.mol.getSurfaceBounds().addThickness(this.waterRadius+this.shellRadius+.02));
		ArrayList<Point3d> newPoints = new ArrayList<Point3d>();
		
		for (Point3d p : points)
		{
			//SphereImpl s = new SphereImpl(p, DEFAULT_WATER_BALL_RADIUS);
			SphereImpl s = new SphereImpl(p, 0.5);
			if (!atomTree.isColliding(s))
			{
				atomTree.insert(s);
				newPoints.add(p);
				
				//System.err.println(""+p.x+" "+p.y+" "+p.z);
			}
		}
		
		points = newPoints;
		
		return points;
	}
	
	@Override
	public List<Point3d> surfacePoints(int index) 
	{
		Atom atom = this.mol.getAtom(index);
		double surfaceRadius = atom.radius()+this.shellRadius;
		
		//get the potential water points
		List<Pair<Point3d,Point3d>> waterPoints = atomWaterPoints(atom.getPosition(), surfaceRadius, DEFAULT_SURFACE_SPHERES_DENSITY);
		
		//get the neighbors of the atom
		//List<? extends Atom> atoms = this.mol.getCloseAtoms(atom.getPosition(), surfaceRadius+2.0*this.waterRadius+0.02);
					
		//find points that do not collide
		ArrayList<Point3d> surfacePoints = new ArrayList<Point3d>(waterPoints.size());
		for (Pair<Point3d,Point3d> point : waterPoints)
			//if (!isColliding(atoms, point.x, this.waterRadius+this.shellRadius))
			if (!this.mol.isCloserThan(point.x, this.waterRadius+this.shellRadius))
				surfacePoints.add(point.y);
			
		//make sure not wasting space
		surfacePoints.trimToSize();
		
		return surfacePoints;
	}

}
