/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule.filters;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import edu.umd.umiacs.armor.cluster.Clusterable;
import edu.umd.umiacs.armor.math.Metric;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.util.HashCodeUtil;

public final class ClusterableMolecule implements Clusterable<ClusterableMolecule>
{
	private final Metric<Molecule> aligner;
	private final Map<ClusterableMolecule, Double> distanceCache;
	//private final int index;
	private final Molecule mol;
	
	public ClusterableMolecule(Molecule mol, Metric<Molecule> metric, int index)
	{
		if (mol==null)
			throw new java.lang.NullPointerException("Molecule cannot be null.");
		
		this.mol = mol;
		this.aligner = metric;
		this.distanceCache = Collections.synchronizedMap(new HashMap<ClusterableMolecule, Double>());
		//this.index = index;
	}
	
	@Override
	public ClusterableMolecule centroidOf(Collection<ClusterableMolecule> p)
	{
		double totalDistance = Double.POSITIVE_INFINITY;
		ClusterableMolecule centralMolecule = null;
		
		for (ClusterableMolecule fromMolecule : p)
		{
			double currDistance = 0;
		
			//compute distance to all other objects
			for (ClusterableMolecule toMolecule : p)
			{
				if (fromMolecule!=toMolecule)
					currDistance += toMolecule.distanceFrom(fromMolecule);
			}
			
			if (currDistance<totalDistance)
			{
				totalDistance = currDistance;
				centralMolecule = fromMolecule;
			}
		}
		
		return centralMolecule;
	}
	
	public final void setDistance(ClusterableMolecule p, double dist)
	{
		if (this.hashCode() <= p.hashCode())
			this.distanceCache.put(p, dist);
		else
			p.distanceCache.put(this, dist);		
	}
	
	@Override
	public final double distanceFrom(ClusterableMolecule p)
	{
		Double dist;
		
		if (this.hashCode() <= p.hashCode())
			dist = this.distanceCache.get(p);
		else
			dist = p.distanceCache.get(this);
		
		if (dist==null)
		{
			dist = this.aligner.distance(this.mol, p.mol);			
			setDistance(p, dist);
		}

		return dist;
	}
	
	public Molecule getMolecule()
	{
		return this.mol;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
    int result = HashCodeUtil.SEED;
    
    //collect the contributions of various fields
    result = HashCodeUtil.hash(result, this.mol);

    return result;
	 }

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		ClusterableMolecule other = (ClusterableMolecule) obj;
		
		if (this.mol == null)
		{
			if (other.mol != null)
				return false;
		}
		else 
		if (!this.mol.equals(other.mol))
			return false;
		
		return true;
	}
}