/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

import java.util.ArrayList;
import java.util.List;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.Point3d;

public abstract class AbstractMolecule implements Molecule
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3458937293146500987L;

	@Override
	public BasicMolecule combine(List<? extends Atom> atomList) throws DuplicateAtomException
	{
		List<Atom> newList = getAtoms();
		newList.addAll(atomList);
		
		return new BasicMolecule(newList);
	}
	
  @Override
	public BasicMolecule combine(Molecule molecule) throws DuplicateAtomException
	{
		return combine(molecule.getAtoms());
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#deuterate(double)
	 */
	@Override
	public Molecule deuterate(double deuteriumFraction)
	{
		ArrayList<Atom> atomList = new ArrayList<Atom>(size());
		for (Atom a : this)
		{
			if (Math.random()<=deuteriumFraction)			
				atomList.add(a.deuterate());
			else
				atomList.add(a);
		}
		
		try
		{
			return new BasicMolecule(atomList);
		}
		catch (DuplicateAtomException e)
		{
			throw new MoleculeRuntimeException(e.getMessage(), e);
		}
	}

	@Override
	public double distanceToCore(int index)
	{
		return distanceToCore(getAtomPosition(index));
	}

	@Override
	public double[][] getAtomPositionArray()
	{
		double[][] atomPositions = new double[size()][3];
	
		int atomIter = 0;
		for (Atom a : this)
		{
			atomPositions[atomIter][0] = a.getPosition().x;
			atomPositions[atomIter][1] = a.getPosition().y;
			atomPositions[atomIter][2] = a.getPosition().z;
			atomIter++;
		}
		
		return atomPositions;
	}

	@Override
	public double[] getAtomPositionSingleArray()
	{
		double[] atomPositions = new double[size()*3];
	
		int atomIter = 0;
		for (Atom a : this)
		{
			atomPositions[3*atomIter+0] = a.getPosition().x;
			atomPositions[3*atomIter+1] = a.getPosition().y;
			atomPositions[3*atomIter+2] = a.getPosition().z;
			atomIter++;
		}
	
		return atomPositions;
	}

	@Override
	public List<? extends Atom> getClosestAtoms(double x, double y, double z, int n)
	{
		return getClosestAtoms(new Point3d(x,y,z), n);
	}

	/* (non-Javadoc)
	 * @see edu.umd.umiacs.armor.molecule.Molecule#maxAtomDistanceExact()
	 */
	@Override
	public double maxAtomDistanceExact()
	{
		double max = 0.0;
		for (Atom a1 : this)
			for (Atom a2 : this)
			{
				double dist = a1.distanceToCenterSquared(a2.getPosition());
				if (dist>max)
					max = dist;
			}
				
		return BasicMath.sqrt(max);
	}

	@Override
	public String toString()
	{
		StringBuilder result = new StringBuilder();
	  String NEW_LINE = System.getProperty("line.separator");
	
	  result.append(this.getClass().getName() + " Object {" + NEW_LINE);
	  for (int iter=0; iter<size(); iter++)
	  {
	  	if (isSolventAtom(iter))
	  		result.append("  Solvent Atom ");
	  	else
	  		result.append("  Atom ");
	  	
	  	result.append(iter+": "+getAtom(iter) + NEW_LINE);
	  }
		result.append("}");	
	  
	  return result.toString();
	}
}