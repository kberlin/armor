/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.molecule;

public enum AminoAcidType implements AtomGroupType
{
	ALA("Alanine","Ala","A",1.8),		
	ARG("Arginine",	"Arg",	"R",	-4.5),		
	ASN("Asparagine",	"Asn",	"N",	-3.5),				
	ASP("Aspartic acid","Asp",	"D",-3.5),				
	ASX("Asparagine or aspartic acid",	"Asx","B",0.0),		
	CYS("Cysteine",	"Cys",	"C",2.5),		
	GLN("Glutamine",	"Gln",	"Q", -3.5),
	GLU("Glutamic acid", "Glu",	"E",-3.5),		
	Glx("Glutamine or glutamic acid",	"Glx","Z",0.0),		
	GLY("Glycine",	"Gly",	"G",	-0.4),				
	HIS("Histidine",	"His",	"H", -3.2),				
	HSE("Homoserine",	"Hse",	"S", 0.0),				
	ILE("Isoleucine",	"Ile",	"I", 4.5),				
	LEU("Leucine",	"Leu",	"L",3.8),				
	LYS("Lysine",	"Lys",	"K", -3.9),
	MET("Methionine",	"Met",	"M", 1.9),				
	PHE("Phenylalanine",	"Phe",	"F",2.8),				
	PRO("Proline",	"Pro",	"P", -1.6),				
	SER("Serine",	"Ser",	"S",-0.8),		
	THR("Threonine",	"Thr",	"T", -0.7),
	TRP("Tryptophan",	"Trp",	"W", -0.9),
	TYR("Tyrosine",	"Tyr",	"Y",-1.3),
	VAL("Valine",	"Val",	"V", 4.2),
	Xaa("Unknown amino acid",	"Xaa",	"X",0.0),
	Xle("Leucine or Isoleucine",	"Xle",	"J",0.0);
	
	private final double hydropathy;
	private final String letter;
	private final String name;
	private final String shortName;
	
	private AminoAcidType(String name, String shortName, String letter, double hydropathy)
	{
		this.name = name;
		this.shortName = shortName;
		this.letter = letter;
		this.hydropathy = hydropathy;
	}

	public double getHydropathy()
	{
		return this.hydropathy;
	}

	@Override
	public String getLetter()
	{
		return this.letter;
	}

	@Override
	public String getName()
	{
		return this.name;
	}

	@Override
	public PrimaryStructureType getPrimaryStructureType()
	{
		return PrimaryStructureType.AMINO_ACID;
	}

	@Override
	public String getShortName()
	{
		return this.shortName;
	}
}
