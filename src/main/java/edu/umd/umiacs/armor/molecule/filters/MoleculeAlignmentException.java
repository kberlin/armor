package edu.umd.umiacs.armor.molecule.filters;

import edu.umd.umiacs.armor.molecule.MoleculeException;

public class MoleculeAlignmentException extends MoleculeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2177643805172625552L;

	public MoleculeAlignmentException()
	{
		super();
	}

	public MoleculeAlignmentException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public MoleculeAlignmentException(String message)
	{
		super(message);
	}

	public MoleculeAlignmentException(Throwable cause)
	{
		super(cause);
	}

}
