package edu.umd.umiacs.armor.cluster;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ClusteringSolution<T extends Clusterable<T>> implements Comparable<ClusteringSolution<T>>, Iterable<Cluster<T>>
{
	private final double mergeDistance;
	private final ArrayList<Cluster<T>> clusters;
	private final int cluster1Index;
	private final int cluster2Index;
	
	public ClusteringSolution(List<Cluster<T>> solution, double mergeDistance, int cluster1Index, int cluster2Index)
	{
		this.clusters = new ArrayList<Cluster<T>>(solution);
		this.mergeDistance = mergeDistance;
		this.cluster1Index = cluster1Index;
		this.cluster2Index = cluster2Index;		
	}
	
	public ArrayList<Cluster<T>> getClusters()
	{
		return new ArrayList<Cluster<T>>(this.clusters);
	}
	
	protected ArrayList<Cluster<T>> getSolutionRef()
	{
		return this.clusters;
	}
	
	public double getMergeDistance()
	{
		return this.mergeDistance;
	}

	@Override
	public int compareTo(ClusteringSolution<T> c)
	{
		return Double.compare(this.mergeDistance, c.mergeDistance);
	}

	public int size()
	{
		return this.clusters.size();
	}

	public Cluster<T> get(int index)
	{
		return this.clusters.get(index);
	}
	
	public Cluster<T> getContainingCluster(T point)
	{
		for (Cluster<T> cluster : this.clusters)
		{
			if (cluster.contains(point))
				return cluster;
		}
		
		return null;
	}
	
	public int getContainingClusterIndex(T point)
	{
		int counter = 0;
		for (Cluster<T> cluster : this.clusters)
		{
			if (cluster.contains(point))
				return counter;
			
			counter++;
		}
		
		return -1;
	}
	
	public int getToIndex()
	{
		return this.cluster1Index;
	}
	
	public int getFromIndex()
	{
		return this.cluster2Index;
	}

	@Override
	public Iterator<Cluster<T>> iterator()
	{
		return this.clusters.iterator();
	}

}
