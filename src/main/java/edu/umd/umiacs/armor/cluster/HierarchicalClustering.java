package edu.umd.umiacs.armor.cluster;

import java.util.ArrayList;
import java.util.List;

public final class HierarchicalClustering<T extends Clusterable<T>>
{
	public HierarchicalSolution<T> cluster(List<T> points)
	{
		ArrayList<Cluster<T>> clusterList = new ArrayList<Cluster<T>>(points.size()); 
		for (T point : points)
		{
			clusterList.add(new Cluster<T>(point));
		}
		
		return cluster(clusterList, Double.POSITIVE_INFINITY);
	}
	
	public HierarchicalSolution<T> cluster(List<Cluster<T>> clusters, double maxDistance)
	{
		//create initial clustering
		ClusteringSolution<T> currList = new ClusteringSolution<T>(clusters, 0.0, -1, -1);
		ArrayList<ClusteringSolution<T>> solList = new ArrayList<ClusteringSolution<T>>();
		
		while (currList!=null)
		{
			solList.add(currList);
			currList = next(currList, maxDistance);	
		}
		
		return new HierarchicalSolution<T>(solList);		
	}
	
	private ClusteringSolution<T> next(ClusteringSolution<T> clusters, double maxDistance)
	{
		if (clusters.size()<=1)
			return null;
		
		double lowestWeight = Double.POSITIVE_INFINITY;
		Cluster<T> fromCluster = null;
		int fromIndex = -1;
		Cluster<T> toCluster = null;
		int toIndex = -1;
		for (int iter1=0; iter1<clusters.size(); iter1++)
		{
			Cluster<T> currFromCluster = clusters.get(iter1);
			for (int iter2=iter1+1; iter2<clusters.size(); iter2++)
			{
				Cluster<T> currToCluster = clusters.get(iter2);
				
				//compute the weights
				double weight = currFromCluster.getCenter().distanceFrom(currToCluster.getCenter());
				
				//store better solution
				if (weight<lowestWeight)
				{
					lowestWeight = weight;
					fromCluster = currFromCluster;
					fromIndex = iter1;
					toCluster = currToCluster;
					toIndex = iter2;
				}
			}
		}
		
		//could not find any solution that is close enough
		if (lowestWeight>maxDistance)
			return null;
		
		//merge the two lists
		ArrayList<Cluster<T>> newList = new ArrayList<Cluster<T>>(clusters.size()-1);
		for (int iter=0; iter<clusters.size(); iter++)
		{
			Cluster<T> newCluster = clusters.get(iter);
			
			//do not add if to cluster, since will be added in the from cluster 
			if (newCluster==toCluster)
				continue;
			
		  if (newCluster==fromCluster)
		  {
		  	ArrayList<T> pointList = new ArrayList<T>(newCluster.getPoints());
		  	pointList.addAll(toCluster.getPoints());
		  	
		  	//figure out the new centroid
		  	T newCenter = newCluster.getCenter().centroidOf(pointList);
		  	
		  	if (newCenter==null)
		  		throw new java.lang.NullPointerException("Centroid cannot be null.");
		  	
		  	newCluster = toCluster.merge(newCenter, newCluster);
		  }
			
		  //add the new cluster
			newList.add(newCluster);
		}
		
		return new ClusteringSolution<T>(newList, lowestWeight, fromIndex, toIndex);
	}
}
