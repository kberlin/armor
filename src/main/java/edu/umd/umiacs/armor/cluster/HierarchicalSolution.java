package edu.umd.umiacs.armor.cluster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HierarchicalSolution<T extends Clusterable<T>>
{
	private final ArrayList<ClusteringSolution<T>> solutions;
	
	protected HierarchicalSolution(List<ClusteringSolution<T>> solutions)
	{
		this.solutions = new ArrayList<ClusteringSolution<T>>(solutions);
		Collections.sort(this.solutions);
	}
	
	public ClusteringSolution<T> getTopSolution()
	{
		return this.solutions.get(this.solutions.size()-1);
	}
	
	public ClusteringSolution<T> getSolution(double cutoff)
	{
		ClusteringSolution<T> bestSol = this.solutions.get(0);
		for (ClusteringSolution<T> sol : this.solutions)
		{
			if (sol.getMergeDistance()>cutoff)
				break;
			
			bestSol = sol;
		}
		
		return bestSol;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		
		int counter=0;
		str.append("Clustering into "+getTopSolution().size()+" solutions:\n");
		for (ClusteringSolution<T> sol : this.solutions)
		{
			str.append(String.format("\tLevel %d: %d %d, Weight: %f\n", counter, sol.getToIndex(), sol.getFromIndex(), sol.getMergeDistance()));
			counter++;
		}
		
		return str.toString();
	}
}
