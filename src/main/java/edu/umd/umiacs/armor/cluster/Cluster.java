package edu.umd.umiacs.armor.cluster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public final class Cluster<T extends Clusterable<T>> implements Serializable
{
	private final T center;
	
	private final ArrayList<T> points;
	/**
	 * 
	 */
	private static final long serialVersionUID = -7578109490744646162L;
	
	public Cluster(T center)
	{
		this.points = new ArrayList<T>(1);
		this.points.add(center);
		this.center = center;
	}
	
	public Cluster(T center, ArrayList<T> points)
	{
		this.points = points;
		this.center = center;
	}
	
	public Cluster(T center, T point)
	{
		this(center);
		this.points.add(point);
	}
	
	public boolean contains(T point)
	{
		for (T currPoint : this.points)
			if (currPoint.equals(point))
				return true;
		
		return false;
	}
	
	public T getCenter()
	{
		return this.center;
	}
	
	public List<T> getPoints()
	{
		return this.points;
	}
	
	public Cluster<T> merge(T newCenter, Cluster<T> cluster)
	{
		Cluster<T> newCluster = new Cluster<T>(newCenter, new ArrayList<T>(this.points));
		newCluster.points.addAll(cluster.points);
		
		return newCluster;
	}
	
	public int size()
	{
		return this.points.size();
	}
}
