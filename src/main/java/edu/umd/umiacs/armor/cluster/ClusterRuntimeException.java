package edu.umd.umiacs.armor.cluster;

public class ClusterRuntimeException extends RuntimeException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6216168587670196694L;

	public ClusterRuntimeException()
	{
		super();
	}

	public ClusterRuntimeException(String arg0)
	{
		super(arg0);
	}

	public ClusterRuntimeException(String arg0, Throwable arg1)
	{
		super(arg0, arg1);
	}

	public ClusterRuntimeException(Throwable arg0)
	{
		super(arg0);
	}

}
