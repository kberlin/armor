package edu.umd.umiacs.armor.cluster;

import java.util.Collection;

public interface Clusterable<T extends Clusterable<T>>
{
	T	centroidOf(Collection<T> p);
	
	double distanceFrom(T p);
}
