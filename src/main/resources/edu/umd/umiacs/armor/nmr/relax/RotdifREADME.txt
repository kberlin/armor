ROTDIF v3
 By: Konstantin Berlin (kberlin@umd.edu)
     Fushman Lab
     Department of Chemistry and Biochemistry
     University of Maryland, College Park

 Requirements:
   JVM 6.0+ (Java Virtual Machine)

 Please see http://bitbucket.org/kberlin/armor/wiki/Home for description and documentation.

 To run ROTDIF in non-GUI mode please use the command:
 “java -jar <rotdif.jar> -nogui

 For command-line flags help use the command:
 “java -jar <rotdif.jar> -help”


 Copyright (c) 2014 by Konstantin Berlin 
 University Of Maryland
