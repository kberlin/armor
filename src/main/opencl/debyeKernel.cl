/*
 * ARMOR package
 *
 * This  software is distributed "as is", without any warranty, including
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 *
 * Copyright (c) 2012 by Konstantin Berlin
 * University Of Maryland
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef NUM_LOCAL_Q
#define NUM_LOCAL_Q 2
#endif

/*
 MULTI Q VERSION
 */

__kernel void debyeFunctionMulti(__global const float *atomList, //0: array of atom positions x1,y1,z1,x2,y2,z2,...
    unsigned int n,//1: number of atoms
    __constant const float *qValues,//2: q values
    unsigned int qSize,//3: number of q values
    __global const float *fa,//4: the form factor values
    __global float *Iq,//5: the output variable, length n
    __local float *localAtomList,//6: storage for local atom positions in a group, x1,y1,z1,x2,y2,z2,....
    __local float *localFa,//7: local storage for fa values
    __local float *localIq//8: local storage for Iq values
)
{
  size_t localId = get_local_id(0);
  size_t groupSize = get_local_size(0);
  size_t numGroups = get_num_groups(0);

  size_t blockOffset;
  size_t currGroupId;
  size_t currOffset;

  //this thread's position information
  float myPositionX;
  float myPositionY;
  float myPositionZ;

  //global id
  size_t globalId = get_group_id(0)*groupSize+localId;

  //number of blocks that n elements are divided into
  size_t numBlocks = (n-1)/groupSize+1;

  //the element index of the current worker, could repeat
  size_t myIndex = globalId%(numBlocks*groupSize);

  //the block index of the current worker
  //size_t blockIndex = myIndex/groupSize;

  //element repeat number
  //size_t repeatIndex = globalId/(numBlocks*groupSize);

  //compute number of repeats
  //limit to no more than n
  size_t numIndexRepeats = numGroups/numBlocks;
  if (numIndexRepeats>n)
  numIndexRepeats = n;

  //get current index information
  if (myIndex<n)
  {
    //store local position
    myPositionX = atomList[0*n+myIndex];
    myPositionY = atomList[1*n+myIndex];
    myPositionZ = atomList[2*n+myIndex];
  }

  unsigned int qIterLocal;
  float tempFloat;

  for (unsigned int qIter=0; qIter<qSize; qIter+=NUM_LOCAL_Q)
  {
    //the q value
    __private float privateQ[NUM_LOCAL_Q];
    for (qIterLocal=0; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
    privateQ[qIterLocal] = (qIter+qIterLocal)<qSize ? qValues[qIter+qIterLocal] : 0.0;

    //init the local counter
    __private float privateIq[NUM_LOCAL_Q];
    for (qIterLocal=0; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
    privateIq[qIterLocal] = 0.0;

    //get current q fa information
    __private float myPrivateFa[NUM_LOCAL_Q];
    for (qIterLocal=0; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
    myPrivateFa[qIterLocal] = ((qIter+qIterLocal)*n+myIndex)<qSize*n ? fa[(qIter+qIterLocal)*n+myIndex] : 0.0;

    //must start at 0, since barriers must be hit by all groups
    for (blockOffset=0; blockOffset<numBlocks; blockOffset+=numIndexRepeats)
    {
      //perform initial memory read for first iteration
      //currGroupId = blockIndex+blockOffset+repeatIndex;
      currGroupId = myIndex/groupSize+globalId/(numBlocks*groupSize)+blockOffset;

      //start loading the next block into memory in background
      if (currGroupId<numBlocks)
      {
        currOffset = currGroupId*groupSize;
        localAtomList[0*groupSize+localId] = (currOffset<n) ? atomList[0*n+currOffset+localId] : 0.0;
        localAtomList[1*groupSize+localId] = (currOffset<n) ? atomList[1*n+currOffset+localId] : 0.0;
        localAtomList[2*groupSize+localId] = (currOffset<n) ? atomList[2*n+currOffset+localId] : 0.0;

        for (qIterLocal=0; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
        if ((qIter+qIterLocal)<qSize)
        localFa[qIterLocal*groupSize+localId] = (currOffset+localId<n) ? fa[(qIter+qIterLocal)*n+currOffset+localId] : 0.0;

      }

      //wait until all group finished copying to local memory
      barrier(CLK_LOCAL_MEM_FENCE);

      //execute the summation over local elements
      if (currGroupId<numBlocks)
      {
        float r;
        float qr;
        float rdiff;

        //----cycle through all elements of the current matrix block----//
        for (size_t iter=0; iter<groupSize; iter++)
        {
          currOffset = currGroupId*groupSize+iter;

          //do if not going out of bounds and greater than current element
          if (myIndex<currOffset)
          {
            //compute the difference between atoms
            rdiff = localAtomList[0*groupSize+iter]-myPositionX;
            r = rdiff*rdiff;
            rdiff = localAtomList[1*groupSize+iter]-myPositionY;
            r = mad(rdiff,rdiff,r);
            rdiff = localAtomList[2*groupSize+iter]-myPositionZ;
            r = mad(rdiff,rdiff,r);

            //compute the euclidean distance
            //native sqrt makes 0.2 speedup
            r = native_sqrt(r);

            //compute q*r values
            if (currOffset<n)
            {
              if (0<NUM_LOCAL_Q)
              {
                qr = privateQ[0]*r;
                rdiff = 2.0*(myPrivateFa[0]*localFa[0*groupSize+iter]);
                privateIq[0] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }
              if (1<NUM_LOCAL_Q)
              {
                qr = privateQ[1]*r;
                rdiff = 2.0*(myPrivateFa[1]*localFa[1*groupSize+iter]);
                privateIq[1] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }
              if (2<NUM_LOCAL_Q)
              {
                qr = privateQ[2]*r;
                rdiff = 2.0*(myPrivateFa[2]*localFa[2*groupSize+iter]);
                privateIq[2] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }
              if (3<NUM_LOCAL_Q)
              {
                qr = privateQ[3]*r;
                rdiff = 2.0*(myPrivateFa[3]*localFa[3*groupSize+iter]);
                privateIq[3] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }
              if (4<NUM_LOCAL_Q)
              {
                qr = privateQ[4]*r;
                rdiff = 2.0*(myPrivateFa[4]*localFa[4*groupSize+iter]);
                privateIq[4] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }
              if (5<NUM_LOCAL_Q)
              {
                qr = privateQ[5]*r;
                rdiff = 2.0*(myPrivateFa[5]*localFa[5*groupSize+iter]);
                privateIq[5] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }
              if (6<NUM_LOCAL_Q)
              {
                qr = privateQ[6]*r;
                rdiff = 2.0*(myPrivateFa[6]*localFa[6*groupSize+iter]);
                privateIq[6] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }
              if (7<NUM_LOCAL_Q)
              {
                qr = privateQ[7]*r;
                rdiff = 2.0*(myPrivateFa[7]*localFa[7*groupSize+iter]);
                privateIq[7] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }

              //loop through the rest
              for (qIterLocal=8; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
              {
                qr = privateQ[qIterLocal]*r;
                rdiff = 2.0*(myPrivateFa[qIterLocal]*localFa[qIterLocal*groupSize+iter]);
                privateIq[qIterLocal] += (1.0e-6<qr) ? rdiff*(native_sin(qr)*native_recip(qr)) : rdiff;
              }
            }
          }

          if (myIndex==currOffset)
          {
            if (0<NUM_LOCAL_Q)
            privateIq[0] += myPrivateFa[0]*myPrivateFa[0];
            if (1<NUM_LOCAL_Q)
            privateIq[1] += myPrivateFa[1]*myPrivateFa[1];
            if (2<NUM_LOCAL_Q)
            privateIq[2] += myPrivateFa[2]*myPrivateFa[2];
            if (3<NUM_LOCAL_Q)
            privateIq[3] += myPrivateFa[3]*myPrivateFa[3];
            if (4<NUM_LOCAL_Q)
            privateIq[4] += myPrivateFa[4]*myPrivateFa[4];
            if (5<NUM_LOCAL_Q)
            privateIq[5] += myPrivateFa[5]*myPrivateFa[5];
            if (6<NUM_LOCAL_Q)
            privateIq[6] += myPrivateFa[6]*myPrivateFa[6];
            if (7<NUM_LOCAL_Q)
            privateIq[7] += myPrivateFa[7]*myPrivateFa[7];

            for (qIterLocal=8; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
            privateIq[qIterLocal] += myPrivateFa[qIterLocal]*myPrivateFa[qIterLocal];
          }
        }
      }

      //wait until all group finished using local memory
      barrier(CLK_LOCAL_MEM_FENCE);
    }

    //figure out if current worker is an extra worker
    //must not do anything due to barrier limitations
    //int isActiveWorker = myIndex<n && globalId<(numIndexRepeats*numBlocks*groupSize);
    for (qIterLocal=0; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
    localIq[qIterLocal*groupSize+localId] = (qIter+qIterLocal)<qSize && myIndex<n && globalId<(numIndexRepeats*numBlocks*groupSize) ? privateIq[qIterLocal] : 0.0;

    //wait until all groups finished loading into local memory
    barrier(CLK_LOCAL_MEM_FENCE);

    //unsigned int blockSize = groupSize;
    //blockOffset is actually current block size
    blockOffset = groupSize;

    //unsigned int blockSizePrev;
    //currOffset is actually previous block size

    //perform one reduction
    while(blockOffset>1)
    {
      currOffset = blockOffset;
      blockOffset = blockOffset>>1;

      //if is odd
      if (blockOffset<<1 < currOffset)
      blockOffset++;

      if (localId < blockOffset && (blockOffset+localId)<currOffset)
      {
        for (qIterLocal=0; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
        localIq[qIterLocal*groupSize+localId] += localIq[qIterLocal*groupSize+blockOffset+localId];
      }

      barrier(CLK_LOCAL_MEM_FENCE);
    }

    //store the values into global memory
    if (localId==0)
    {
      for (qIterLocal=0; qIterLocal<NUM_LOCAL_Q; qIterLocal++)
      if ((qIter+qIterLocal)<qSize)
      Iq[numGroups*(qIter+qIterLocal)+get_group_id(0)] = localIq[qIterLocal*groupSize];
    }

    barrier(CLK_GLOBAL_MEM_FENCE);
  }
}

//**********************************REDUCE****************************************//

__kernel void reduceMulti(__global float *g_idata, __global float *g_odata, unsigned int n, __local volatile float* sdata, int numberQ)
{
  size_t tid = get_local_id(0);
  size_t i = get_group_id(0)*get_local_size(0) + tid;

  for (unsigned int qIter=0; qIter<(size_t)numberQ; qIter++)
  {
    unsigned int offset = qIter*n;
    unsigned int blockSize = get_local_size(0);

    if (i<n)
    sdata[tid] = g_idata[i+offset];
    else
    sdata[tid] = 0.0;

    barrier(CLK_LOCAL_MEM_FENCE);

    //perform local reduction in shared memory
    while(blockSize>1)
    {
      blockSize = blockSize>>1;
      if (tid < blockSize)
      sdata[tid] += sdata[tid + blockSize];

      barrier(CLK_LOCAL_MEM_FENCE);
    }

    // write result for this block to global mem
    if (tid == 0)
    g_odata[get_num_groups(0)*qIter+get_group_id(0)] = sdata[0];

    barrier(CLK_GLOBAL_MEM_FENCE);
  }
}

