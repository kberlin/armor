function paticonv(RDC, chainID, atom_1, atom_2, error, reslist)

% 2012-05-06 Carlos Castaneda
% 2013-08-01 Modified by Konstantin Berlin
% assume that RDC variable contains two columns:
% res_num RDC_value
% where res_num is residue number (integer) and RDC_value is in Hz

% reslist is a vector of residue numbers to be analyzed. All other residues will be
% starred ('*'), i.e. commented out in PATI's language.'

% chainID (e.g. 'A' or 'B')
% atom_1 (e.g. 'N')
% atom_2 (e.g. 'H')
% error (value in Hz)

% 2 A N 2 A H 7.8920 1.0

%atom_1 = 'N';
%atom_2 = 'H';

if (nargin<6)
  reslist = [];
end

fprintf('%%<residue1> <chain1> <atom1> <residue2> <chain2> <atom2> <rdc_exp> <rdc_error>\n');

for iter=1:length(RDC(:,1))
  
    residue = RDC(iter,1);
    index_RDC = find(RDC(:,1)==residue);
    
    if (~isempty(reslist))
      index_reslist = find(reslist(:,1)==residue,1);
    else
      index_reslist = 1;
    end
    
    %if not found continue to next residue
    if (isempty(index_RDC))
      continue;
    end;
    if (isnan(RDC(index_RDC,2)))
      continue;
    end
    
    % if residue is not in reslist, then star it out
    if (isempty(index_reslist))
        fprintf('*');
    end;

    fprintf('%3d  %s  %s  %3d  %s  %s  %10.6f  %4.3f', residue, chainID, atom_1, ....
        residue, chainID, atom_2, RDC(iter,2), error);
    
    fprintf('\n');
end;