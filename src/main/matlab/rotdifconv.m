function rotdifconv(R1, R2, NOE, freq, chainID)

for iter=1:length(R1(:,1))
  
    residue = R1(iter,1);
    indexR2 = find(R2(:,1)==residue);
    
    %if not found continue to next residue
    if (isempty(indexR2))
      continue;
    end;

    fprintf('%d %s %s %s %.2f %.6f %.6f %.6f %.6f', residue, chainID,  'N', 'H', freq, ....
      R1(iter,[2,3]), R2(indexR2,[2,3]));

    if (~isempty(NOE))      
      indexNOE = find(NOE(:,1)==residue);  
      
      if (~isempty(indexNOE))
        fprintf(' %.6f %.6f',NOE(indexNOE,[2,3]));
      end;
    end;
    
    fprintf('\n');
end;