function setupjava()

  %start in current directory
  jarpath = '../ARMOR';

  jarfiles = dir(fullfile(jarpath,'lib','*.jar'));
  for x=1:length(jarfiles)
    if (~strncmp(jarfiles(x).name,'gluegen',7))
      javaaddpath(fullfile(jarpath,'lib',jarfiles(x).name))
    end;
  end;
  javaaddpath({fullfile(jarpath,'target','classes')});
