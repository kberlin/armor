/*
    ' error codes (ierr) :'
    '    0       ! no input errors found/normal execution'
    '    1       ! max level of the octree used 1 or less: no input accepted'
    '    2       ! no sources '
    '    3       ! required optional arguments are not provided'
    '    4       ! wavenumber is zero'
    '    5       ! insufficient memory'
    '    6       ! qD exceeds the subroutine limit for the FMM'
    '    9       ! attempt of unauthorized use'

 */
#define REAL double
#define INTEGER long

struct complex {REAL r, i;};

#include <ostream>
#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <unistd.h>
#include <sys/time.h>
#include<omp.h>
#include "sincfmm.h"
using namespace std;

int main( int argc, const char* argv[] )
{
  int numMolecules;
  int numAtoms;
  int numDestAtoms;
  int numQ;
  int iter, qIter;
  int ierr;
  int computeDerivatives;
  double eps;
  double initTime;
  double computeTime;

  timeval startTime;
  timeval endTime;

  //options array
  int optionsInt[5];
  double optionsDouble[5];

  //read in options
  cin >> optionsDouble[0] >> optionsInt[0] >> optionsInt[1] >> optionsInt[2];

  //read in molecule and Q informations
  cin >> numQ >> numMolecules;

  double* q = new double[numQ];
  int* L = new int[numQ];
  double *Iq = new double[numQ];

  //read the q values
  for (iter=0; iter<numQ; iter++)
    cin >> q[iter];

  //read the tree depth
  for (iter=0; iter<numQ; iter++)
    cin >> L[iter];

  //start reading in molecule information
  cin >> numAtoms >> numDestAtoms;

  //allocate storage memory
  double* coordinates = new double[3*numAtoms];
  double *fa = new double[numQ*numAtoms];

  //read the coordinates
  for (iter=0; iter<numAtoms; iter++)
  {
    cin >> coordinates[iter*3+0] >> coordinates[iter*3+1] >> coordinates[iter*3+2];

    //read the form factors
    for (qIter=0; qIter<numQ; qIter++)
    {
      cin >> fa[numQ*iter+qIter];
    }
   }

  //read the destination information
  double* destination = 0;
  double *faTo = 0;
  double *J = 0;
  int falseToValues = 0;
  if (numDestAtoms>0)
  {
    destination = new double[3*numDestAtoms];
    faTo = new double[numQ*numDestAtoms];

    //read the coordinates
    for (iter=0; iter<numDestAtoms; iter++)
    {
      cin >> destination[iter*3+0] >> destination[iter*3+1] >> destination[iter*3+2];

      //read the form factors
      for (qIter=0; qIter<numQ; qIter++)
      {
        cin >> faTo[numQ*iter+qIter];
      }
    }
  }
  else
  if (optionsInt[1]) //if same atoms but jacobian computation
  {
    destination = coordinates;
    faTo = fa;

    numDestAtoms = numAtoms;
    falseToValues = 1;
  }

  //if computing J allocate it
  if (optionsInt[1])
    J = new double[3*numQ*numDestAtoms];

  //setup the problem
  SincFmm predictor = SincFmm(q, numQ, optionsInt[0]);
  predictor.setRelativeError(optionsDouble[0]);
  predictor.setTiming(optionsInt[2]);

  gettimeofday(&startTime, 0);
  if (optionsInt[1] || numDestAtoms>0)
  {
    ierr = predictor.computeProfileJacobian(coordinates, fa, numAtoms, destination, faTo, numDestAtoms, Iq, J);
  }
  else
  {
    ierr = predictor.computeProfile(coordinates, fa, numAtoms, Iq);
  }
  gettimeofday(&endTime, 0);

  //get the computation time
  initTime = predictor.getInitTime();
  computeTime = predictor.getComputationTime();

  //compute the total time
  double totaltime = (((double)(endTime.tv_sec-startTime.tv_sec)) + ((double)(endTime.tv_usec-startTime.tv_usec))*1.0e-6);

  //output the results to screen
  cout.precision(10);
  cout.setf(ios::scientific);
  cout << ierr << " " << totaltime << " " << initTime << " " << computeTime << endl;
  for (iter=0; iter<numQ; iter++)
  {
    cout << Iq[iter] << " ";
  }
  cout << endl << endl;

  //output the jacobian
  if (optionsInt[1])
  {
    for (qIter=0; qIter<numQ; qIter++)
    {
      for (iter=0; iter<3*numDestAtoms; iter++)
        cout << J[qIter*3*numDestAtoms+iter] << " ";
      cout << endl;
    }
  }

  //delete molecule specific information
  delete[] fa;
  delete[] coordinates;
  if (destination!=0 && !falseToValues)
  {
    delete[] destination;
  }
  if (faTo!=0 && !falseToValues)
  {
    delete[] faTo;
  }
  if (J!=0)
    delete[] J;

  //delete intensity information
  delete[] Iq;
  delete[] q;
  delete[] L;
  
  return 0;
}

