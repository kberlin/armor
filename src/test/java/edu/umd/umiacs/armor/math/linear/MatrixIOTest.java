package edu.umd.umiacs.armor.math.linear;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class MatrixIOTest
{

	@Test
	public void MatrixBindaryReadTest() throws IOException
	{
		String matrixFile = MatrixIOTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/math/linear/"+"matrix_binary_test.dat").getFile();
		
		Array2dRealMatrix A_read = LinearMathFileIO.read(new File(matrixFile));
		Array2dRealMatrix A = LinearMathFileIO.readBinary(new File(matrixFile));
		
		Assert.assertTrue((A.subtract(A_read)).normFrobenius()<1.0e-12);

		double[][] A_actual = {{1.0,2.0,3.0},{4.0,5.0,6.0}};
		
		Assert.assertTrue((A.subtract(new Array2dRealMatrix(A_actual))).normFrobenius()<1.0e-12);
	}

	@Test
	public void MatrixStringReadTest() throws IOException
	{
		String matrixFile = MatrixIOTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/math/linear/"+"matrix_string_test.txt").getFile();
		
		Array2dRealMatrix A_read = LinearMathFileIO.read(new File(matrixFile));
		Array2dRealMatrix A = LinearMathFileIO.readText(new File(matrixFile));
		
		Assert.assertTrue((A.subtract(A_read)).normFrobenius()<1.0e-12);
		
		double[][] A_actual = {{1.0,2.0,3.0},{4.0,5.0,6.0}};
		
		Assert.assertTrue((A.subtract(new Array2dRealMatrix(A_actual))).normFrobenius()<1.0e-12);
	}
}
