/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.func.LinearLeastSquaresFunction;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.util.BasicUtils;

public class NonlinearLeastSquaresTest
{
	@Test
	public void NonlinearLeastSquaresConstrainedSmallGeneral()
	{
		//create A
		double[][] A = new double[20][5];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;
		
		RealMatrix M = new Array2dRealMatrix(A);
		
		double[] x1 = BasicUtils.createArray(M.numColumns(), 1.0);
		double[] y1 = M.mult(x1);
		
		double[] x0 = BasicUtils.createArray(M.numColumns(), 0.0);
		
		//compute results
		ActiveSetLeastSquares solver = ActiveSetLeastSquares.getLevenbergMarquardtOptimizer();
		solver.setFunction(new LinearLeastSquaresFunction(M));	
		solver.setObservations(y1);
	
		LeastSquaresOptimizationResult result = solver.optimize(x0);
		double[] x1sol = result.getPoint();
		
	  Assert.assertArrayEquals(x1, x1sol, 1.0e-8);

	  //make function
		solver.setLinearEqualityConstraint(LinearConstraints.createMaxSum(M.numColumns(), 1.0));

		result = solver.optimize(BasicMath.divide(x1, BasicMath.sum(x1)));
		x1sol = result.getPoint();
						
	  Assert.assertEquals(1.0, BasicMath.sum(x1sol), 1.0e-8);
	}
	
	@Test
	public void NonlinearLeastSquaresConstrained()
	{
		//create A
		double[][] A = new double[10][4];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;
		
		RealMatrix M = new Array2dRealMatrix(A);
		
		double[] x1 = BasicUtils.createArray(M.numColumns(), 1.0/M.numColumns());
		x1[0] = -1.0;
		double[] y1 = M.mult(x1);
		
		//create second test
		double[] x2 = BasicUtils.createArray(M.numColumns(), 0.5/M.numColumns());
				
		//compute results
		ActiveSetLeastSquares solver = ActiveSetLeastSquares.getLevenbergMarquardtOptimizer();
		
		//make function
		solver.setLinearInequalityConstraint(LinearConstraints.createMaxSum(M.numColumns(), 1.0));
		solver.setLowerBound(BasicUtils.createArray(M.numColumns(), 0.0));
		solver.setFunction(new LinearLeastSquaresFunction(M));	
		solver.setObservations(y1);
		
		LeastSquaresOptimizationResult result = solver.optimize(x2);
		double[] x1sol = result.getPoint();
		
		double l1 = 0.0;
		boolean positive = true;
		for (double val : x1sol)
		{
			l1+=val;
			
			if (val<-1.0e-10)
				positive = false;
		}
		
	  Assert.assertTrue(l1<=1.0+1.0e-8);
	  Assert.assertTrue(positive);
	}
}
