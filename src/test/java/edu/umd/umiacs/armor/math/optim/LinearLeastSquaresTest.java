/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.linear.RealMatrix;
import edu.umd.umiacs.armor.util.BasicUtils;

public class LinearLeastSquaresTest
{
	@Test
	public void PositiveLinearLeastSquaresTest()
	{
		//create A
		double[][] A = new double[50][200];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;
		
		RealMatrix M = new Array2dRealMatrix(A);
		
		//set one value to negative
		double[] x0 = BasicUtils.createArray(M.numColumns(), 1.0);
		x0[0] = -1.0;
		double[] y = M.mult(x0);
		
		//compute results
		NonNegativeLinearLeastSquares solver = new NonNegativeLinearLeastSquares(1.0e-10, 1000);
		solver.setA(M);
		
		double[] x = solver.solve(y);
		
		boolean pos = true;
		for (double val : x)
			if (val<0)
			{
				pos = false;
				break;
			}
		
	  Assert.assertTrue(pos);
	}
	
	@Test
	public void LinearLinearConst()
	{
		//create A
		double[][] A = new double[100][20];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;
		
		RealMatrix M = new Array2dRealMatrix(A);
		
		double[] x1 = BasicUtils.createArray(M.numColumns(), 1.0);
		double[] y1 = M.mult(x1);
		
		double[] x2 = BasicUtils.createArray(M.numColumns(), 1.0/M.numColumns());
		double[] y2 = M.mult(x2);
		
		//compute results
		LinearConstraintedLinearLeastSquares solver = new LinearConstraintedLinearLeastSquares();
		solver.setA(M);
		solver.setLinearEqualityConstraint(LinearConstraints.createMaxSum(M.numColumns(), 1.0));
		solver.setLowerBound(BasicUtils.createArray(M.numColumns(), 0.0));
		
		double[] x1sol = solver.solve(y1, x2);
		double[] x2sol = solver.solve(y2, x2);
		
		double l1 = 0.0;
		boolean positive = true;
		for (double val : x1sol)
		{
			//System.out.println(val);
			l1+=val;
			
			if (val<-1.0e-10)
				positive = false;
		}
		
	  Assert.assertEquals(1.0, l1, 1.0e-8);
	  Assert.assertTrue(positive);
	  
		l1 = 0.0;
		for (double val : x2sol)
			l1+=val;
		
	  Assert.assertEquals(1.0, l1, 1.0e-8);
	  Assert.assertArrayEquals(y2, M.mult(x2sol), 1.0e-8);
	}
}
