package edu.umd.umiacs.armor.math.linear;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.optim.ConjugantGradientLinearLeastSquares;
import edu.umd.umiacs.armor.math.optim.ConjugateGradientSolver;
import edu.umd.umiacs.armor.util.BasicUtils;

public class ConjugantGradientTest
{
	@Test
	public void ConjugantGradientSolverTest()
	{
		//create A
		double[][] A = new double[200][50];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;
		
		double[] y= new double[A.length];
		for (int row=0; row<y.length; row++)
			y[row] = randomGenerator.nextDouble()-0.5;

		RealMatrix M = new Array2dRealMatrix(A);
		RealMatrix Mt = M.transpose();
		
		double[] xsvd = BasicMath.solveSVD(A, y, 1.0e-12);
		
		ConjugateGradientSolver cgSolver = new ConjugateGradientSolver(1000, 1.0e-12);
		
		double[] xCG =  cgSolver.solve((Mt.mult(M)).toArray(),  Mt.mult(y));
		
		Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xCG, xsvd))<1.0e-12);
	}
	
	@Test
	public void ConjugantGradientLeastSquaresTest()
	{
		//create A
		double[][] A = new double[200][50];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;
		
		double[] y= new double[A.length];
		for (int row=0; row<y.length; row++)
			y[row] = randomGenerator.nextDouble()-0.5;

		RealMatrix M = new Array2dRealMatrix(A);
		RealMatrix Mt = M.transpose();
		
		double[] xsvd = BasicMath.solveSVD(A, y, 1.0e-12);
		
		ConjugantGradientLinearLeastSquares cg = new ConjugantGradientLinearLeastSquares(1000, 1.0e-12);
		cg.setA(A);
		
		double[] xCG =  cg.optimize(y, BasicUtils.createArray(A[0].length, 0.0));
		
		Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xCG, xsvd))<1.0e-12);

		cg.setATranposed(Mt.toArray());
		xCG =  cg.optimize(y, BasicUtils.createArray(A[0].length, 0.0));
		Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xCG, xsvd))<1.0e-12);
	}

}
