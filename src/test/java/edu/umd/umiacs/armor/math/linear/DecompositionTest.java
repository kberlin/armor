package edu.umd.umiacs.armor.math.linear;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

public class DecompositionTest
{
	@Test
	public void LeftSVDTest()
	{
		//create A
		double[][] A = new double[50][200];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;
		
		RealMatrix M = new Array2dRealMatrix(A);
		
		LeftSingularValueDecomposition svdLeft = new LeftSingularValueDecomposition(M);
		SingularValueDecomposition svd = new SingularValueDecomposition(M);
		
		Assert.assertTrue(svdLeft.getU().subtract(svd.getU()).normFrobenius()<1.0e-12);
		Assert.assertTrue(svdLeft.getS().subtract(svd.getS()).normFrobenius()<1.0e-12);
		
		//create A
		A = new double[50][200];
		
		randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;
		
		M = new Array2dRealMatrix(A);

		Assert.assertTrue(svdLeft.getU().subtract(svd.getU()).normFrobenius()<1.0e-12);
		Assert.assertTrue(svdLeft.getS().subtract(svd.getS()).normFrobenius()<1.0e-12);
	}

}
