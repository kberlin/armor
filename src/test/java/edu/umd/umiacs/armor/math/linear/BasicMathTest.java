package edu.umd.umiacs.armor.math.linear;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import edu.umd.umiacs.armor.math.BasicMath;

public class BasicMathTest
{
	@Test
	public void GeneralBasicMathTest()
	{
		//create A
		double[][] A = new double[50][200];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;

		//create A
		double[][] B = new double[200][30];
		
		for (int row=0; row<B.length; row++)
			for (int col=0; col<B[row].length; col++)
				B[row][col] = randomGenerator.nextDouble()-0.5;
		
		//create A
		double[][] C = new double[100][50];
		
		for (int row=0; row<C.length; row++)
			for (int col=0; col<C[row].length; col++)
				C[row][col] = randomGenerator.nextDouble()-0.5;

		
		double[][] At = BasicMath.transpose(A);
		double[][] Bt = BasicMath.transpose(B);

		RealMatrix Am = new Array2dRealMatrix(A, false);
		RealMatrix Amt = new Array2dRealMatrix(At, false);
		RealMatrix Bm = new Array2dRealMatrix(B, false);
		RealMatrix Cm = new Array2dRealMatrix(C, false);
		
	  RealMatrix cValue = Am.mult(Bm);
		RealMatrix aValue =  new Array2dRealMatrix(BasicMath.mult(A, B), false);
		Assert.assertTrue((cValue.subtract(aValue).normFrobenius())<1.0e-12);

		cValue = Cm.mult(Am);
		aValue =  new Array2dRealMatrix(BasicMath.mult(C, A), false);
		Assert.assertTrue((cValue.subtract(aValue).normFrobenius())<1.0e-12);
		
		cValue = Am.mult(Bm);
		aValue =  new Array2dRealMatrix(BasicMath.multTranspose(At, B), false);
		Assert.assertTrue((cValue.subtract(aValue).normFrobenius())<1.0e-12);

		double[] cArray = Amt.transpose().mult(Bm.getColumn(0));
		double[] aArray =  BasicMath.multTranspose(At, Bt[0]);
		Assert.assertTrue(BasicMath.norm(BasicMath.subtract(aArray, cArray))<1.0e-12);
		
		cArray = Amt.multTranspose(Bt[0]);
		aArray =  BasicMath.multTranspose(At, Bt[0]);
		Assert.assertTrue(BasicMath.norm(BasicMath.subtract(aArray, cArray))<1.0e-12);

		cArray = Am.mult(Bm.getColumn(0));
		aArray =  BasicMath.multTranspose(At, Bt[0]);
		Assert.assertTrue(BasicMath.norm(BasicMath.subtract(aArray, cArray))<1.0e-12);

		cArray = Am.getRow(0);
		aArray =  BasicMath.add(cArray, cArray);
		aArray = BasicMath.subtract(aArray, aArray);
		Assert.assertTrue(BasicMath.norm(aArray)<1.0e-12);
	}

}
