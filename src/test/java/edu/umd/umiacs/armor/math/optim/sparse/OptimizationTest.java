/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.math.optim.sparse;

import java.util.Random;

import org.junit.Assert;
import org.junit.Test;

import edu.umd.umiacs.armor.math.BasicMath;
import edu.umd.umiacs.armor.math.linear.Array2dRealMatrix;
import edu.umd.umiacs.armor.math.optim.sparse.SparseLogLikelihoodSolver.Preconditioning;
import edu.umd.umiacs.armor.util.BasicUtils;

public class OptimizationTest
{
	@Test
	public void MultiOrthogonalPersuitSmallTest()
	{
		double[][] A = new double[3][];
		
	  A[0] = new double[]{0.814723686393179,   0.913375856139019,   0.278498218867048,   0.964888535199277,   0.957166948242946,   0.141886338627215,   0.792207329559554,   0.035711678574190};
	  A[1] = new double[]{0.905791937075619,   0.632359246225410,   0.546881519204984,   0.157613081677548,   0.485375648722841,   0.421761282626275,   0.959492426392903,   0.849129305868777};
	  A[2] = new double[]{0.126986816293506,   0.097540404999410,   0.957506835434298,   0.970592781760616,   0.800280468888800,   0.915735525189067,   0.655740699156587,   0.933993247757551};

	  //create x
	  double x[] = BasicUtils.createArray(A[0].length, 0.0);
	  x[2] = 7.32;
	  
	  //create observation
	  double[] y = BasicMath.mult(A, x);

	  double eps = 1.0e-8;
	  
	  MultiOrthogonalMatchingPursuit omp = new MultiOrthogonalMatchingPursuit();
	  omp.setA(new Array2dRealMatrix(A));
	  omp.setRelativeErrorTol(eps);
	  
	  double[] xsol = omp.solve(y);
	  double[] ysol = BasicMath.mult(A, xsol);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(x,xsol))/BasicMath.norm(x)<eps);
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(ysol,y))/BasicMath.norm(y)<eps);
	  
	  //test the two state solution
	  x[4] = 3.21;
	  
	  //create and solve
	  y = BasicMath.mult(A, x);
	  xsol = omp.solve(y);
	  ysol = BasicMath.mult(A, xsol);

	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(x,xsol))/BasicMath.norm(x)<eps);
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(ysol,y))/BasicMath.norm(y)<eps);
	  
	  //test the two state solution, multiple solutions possible
	  x[7] = 1.11;
	  
	  //create and solve
	  y = BasicMath.mult(A, x);
	  xsol = omp.solve(y);
	  ysol = BasicMath.mult(A, xsol);
	  
	  //Assert.assertTrue(BasicMath.norm(BasicMath.subtract(x,xsol))/BasicMath.norm(xsol)<eps);
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(ysol,y))/BasicMath.norm(y)<eps);

	}
	
	@Test
	public void MultiOrthogonalPersuitRandomTest()
	{
		int M = 5;
		
		//create A
		double[][] A = new double[50][200];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;

		double[] x = new double[A[0].length];
		double[] xpos = new double[A[0].length];
		for (int row=0; row<M; row++)
		{
			x[row+2] = randomGenerator.nextDouble()-0.5;
			xpos[row+2] = randomGenerator.nextDouble()+1.5;
		}
		xpos = BasicMath.divide(xpos, BasicMath.sum(xpos));
		
	  double eps = 1.0e-12;
	  
	  MultiOrthogonalMatchingPursuit omp = new MultiOrthogonalMatchingPursuit(10);
	  omp.setA(new Array2dRealMatrix(A));
	  omp.setRelativeErrorTol(eps);
	  
	  double[] y = BasicMath.mult(A, x);
	  double[] xsol = omp.solve(y, M+2);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(x,xsol))/BasicMath.norm(x)<eps);

	  omp = new MultiOrthogonalMatchingPursuit(10, true);
	  omp.setA(new Array2dRealMatrix(A));
	  omp.setRelativeErrorTol(eps);
	
	  y = BasicMath.mult(A, xpos);
	  xsol = omp.solve(y, M+2);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xpos,xsol))/BasicMath.norm(xpos)<eps);
	  
	  omp = new MultiOrthogonalMatchingPursuit(1000, true, 1.0);
	  omp.setA(new Array2dRealMatrix(A));
	  omp.setRelativeErrorTol(eps);
	
	  y = BasicMath.mult(A, xpos);
	  xsol = omp.solve(y, M+2);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xpos,xsol))/BasicMath.norm(xpos)<eps);
	  
	  omp = new MultiOrthogonalMatchingPursuit(true, 1000, true, 1.0);
	  omp.setA(new Array2dRealMatrix(A));
	  omp.setRelativeErrorTol(eps);
	
	  y = BasicMath.mult(A, xpos);
	  xsol = omp.solve(y, M+2);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xpos,xsol))/BasicMath.norm(xpos)<eps);

	}
	
	@Test
	public void MultiOrthogonalPersuitRandomPreconditioningTest()
	{
		int M = 5;
		
		//create A
		double[][] A = new double[50][200];
		
		Random randomGenerator = new Random(123);
		
		for (int row=0; row<A.length; row++)
			for (int col=0; col<A[row].length; col++)
				A[row][col] = randomGenerator.nextDouble()-0.5;

		double[] x = new double[A[0].length];
		double[] xpos = new double[A[0].length];
		for (int row=0; row<M; row++)
		{
			x[row+2] = randomGenerator.nextDouble()-0.5;
			xpos[row+2] = randomGenerator.nextDouble()+1.5;
		}
		xpos = BasicMath.divide(xpos, BasicMath.sum(xpos));
		
	  double eps = 1.0e-12;
	  
	  MultiOrthogonalMatchingPursuit omp = new MultiOrthogonalMatchingPursuit(10);
	  omp.setRelativeErrorTol(eps);
	  SparseLogLikelihoodSolver solver = new SparseLogLikelihoodSolver(omp, Preconditioning.NONE);
	  solver.setA(new Array2dRealMatrix(A));
	  
	  double[] y = BasicMath.mult(A, x);
	  double[] xsol = solver.solve(y, M+2);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(x,xsol))/BasicMath.norm(x)<eps);

	  omp = new MultiOrthogonalMatchingPursuit(10, true);
	  omp.setRelativeErrorTol(eps);
	  solver = new SparseLogLikelihoodSolver(omp, Preconditioning.NONE);
	  solver.setA(new Array2dRealMatrix(A));

	  y = BasicMath.mult(A, xpos);
	  xsol = solver.solve(y, M+2);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xpos,xsol))/BasicMath.norm(xpos)<eps);
	  	  
	  omp = new MultiOrthogonalMatchingPursuit(true, 10000, true, 1.0);
	  omp.setRelativeErrorTol(eps);
	  solver = new SparseLogLikelihoodSolver(omp, Preconditioning.NONE);
	  solver.setA(new Array2dRealMatrix(A));
	
	  y = BasicMath.mult(A, xpos);
	  xsol = solver.solve(y, M+2);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xpos,xsol))/BasicMath.norm(xpos)<eps);
	  
	  solver = new SparseLogLikelihoodSolver(omp, Preconditioning.COMPRESSION, 1.0e-2);
	  solver.setA(new Array2dRealMatrix(A));
		
	  y = BasicMath.mult(A, xpos);
	  xsol = solver.solve(y, M+2);
	  
	  Assert.assertTrue(BasicMath.norm(BasicMath.subtract(xpos,xsol))/BasicMath.norm(xpos)<eps);


	}

}
