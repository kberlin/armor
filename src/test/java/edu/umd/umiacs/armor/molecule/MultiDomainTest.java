package edu.umd.umiacs.armor.molecule;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;

import org.junit.Assert;
import org.junit.Test;

import edu.umd.umiacs.armor.math.RigidTransform;
import edu.umd.umiacs.armor.math.Rotation;

public class MultiDomainTest
{
	@Test
	public void testMultiDomain() throws PdbException
	{
	  String pdbFile = "./edu/umd/umiacs/armor/pdb/1D3Z.pdb";
	  
	  File pdbFullFile = new File(this.getClass().getClassLoader().getResource(pdbFile).getPath());

		PdbStructure pdb = MoleculeFileIO.readPDB(pdbFullFile);
		
		BasicMolecule mol1 = pdb.getModel(0).createBasicMolecule();
		BasicMolecule mol2 = pdb.getModel(0).createBasicMolecule();
		BasicMolecule mol3 = pdb.getModel(0).createBasicMolecule();
		
		//create multi domain
		MultiDomainComplex complex = new MultiDomainComplex(Arrays.asList(mol1,mol2,mol3));
		
		//check if sizes match
		Assert.assertTrue(complex.size()==mol1.size()*3);
		
		//check if atoms are working ok
		Assert.assertTrue(complex.getAtom(2*mol1.size()+20).equals(mol1.getAtom(20)));
		
		Rotation R = Rotation.createZYZ(.2, .2, .2);
		MultiDomainComplex newComplex = complex.createTransformed(new RigidTransform(R, null), new RigidTransform(R, null), new RigidTransform(R, null)).
				createTransformed(new RigidTransform(R.inverse(), null),new RigidTransform(R.inverse(), null),new RigidTransform(R.inverse(), null));
		
		Iterator<Atom> iter1 = complex.iterator();
		Iterator<Atom> iter2 = newComplex.iterator();
		
		//check atoms are correctly adjusted
		while(iter1.hasNext() || iter2.hasNext())
			Assert.assertTrue(iter1.next().getPosition().distance(iter2.next().getPosition())<1.0e-5);
		
		
	}
}
