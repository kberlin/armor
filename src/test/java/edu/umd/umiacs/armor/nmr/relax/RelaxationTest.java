/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.relax;

import java.io.File;
import java.io.FileFilter;
import org.junit.Assert;
import org.junit.Test;

import edu.umd.umiacs.armor.math.linear.LinearMathFileIO;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.Molecule;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;

public class RelaxationTest
{
	private enum TestCases
	{
		CASE1 ("malate synthase G67", "1Y8B.pdb", 55.3, true),
		CASE2 ("human serum albumin", "1AO6h.pdb", 41.0, true),
		CASE3 ("maltose binding protein", "1EZP.pdb", 28.6, true),
		//CASE4 ("beta-lactoglobulin a (dimer)", "1bsy.pdb",  23.2, true),
		CASE5 ("delta-3 ketosteroid isomerase", "1BUQ.pdb", 18.0, false),
		CASE6 ("leukemia inh. factor", "1LKIh.pdb", 14.9, true),
		CASE7 ("trypsin", "2BLVh.pdb", 14.8, true),
		CASE8 ("yellow fluorescent protein", "2YFPh.pdb", 14.8, true),
		CASE9 ("green fluorescent protein", "1W7Sh.pdb", 14.2, true),
		CASE10 ("carbonic anhydrase", "2CABh.pdb", 14.0, true),
		CASE11 ("HIV-1 protease", "1BVG.pdb", 13.0, false),
		CASE12 ("savinase", "1SVNh.pdb", 12.4, true),
		CASE13 ("interleukin-1beta", "6I1B.pdb", 12.4, true),
		CASE15 ("ribonuclease H", "2RN2h.pdb", 11.7, true),
		CASE17 ("cytochrome", "1C2N.pdb", 10.4, true, 2),
		CASE18 ("beta-lactoglobulin A (mono)", "1BSYh.pdb", 9.7, true),
		CASE19 ("apomyoglobin", "1BVC.pdb", 9.5, false),
		CASE20 ("lysozyme", "1E8L.pdb", 8.3, true),
		CASE21 ("barstar C40/83A", "1BTA.pdb", 7.4, true),
		CASE22 ("eglin c", "1EGLh.pdb", 6.2, true),
		//CASE23 ("cytochrome bs", "1hko.pdb", 6.1, true),
		CASE24 ("calbindin-D9k+Ca", "2BCA.pdb", 5.1, true),
		CASE25 ("ubiquitin", "1UBQm.pdb", 5.0, true),
		CASE26 ("calbindin-D9k", "1CLB.pdb", 4.9, true),
		CASE27 ("BPTI", "1PIT.pdb", 4.4, true),
		//CASE28 ("protein G", "1IGDhm.pdb", 3.7, false),
		CASE29 ("Xfin-zinc finger DBD", "1ZNF.pdb", 2.4, true);
		
		@SuppressWarnings("unused")
		public final String name;
		public final String pdb;
		public final double tauC;
		public final boolean firstChain;
		public final int model;
		
		private TestCases(String name, String pdb, double tauC, boolean firstChain)
		{
			this.name = name;
			this.pdb = pdb;
			this.tauC = tauC;
			this.firstChain = firstChain;
			this.model = 1;
		}

		private TestCases(String name, String pdb, double tauC, boolean firstChain, int model)
		{
			this.name = name;
			this.pdb = pdb;
			this.tauC = tauC;
			this.firstChain = firstChain;
			this.model = model;
		}
	}
	
	@Test
	public void tensorTest() throws Exception
	{
		File dir = new File(RelaxationTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/nmr/relax").getFile());
		File[] fileList = dir.listFiles(new FileFilter()
		{			
			@Override
			public boolean accept(File pathname)
			{
				return (pathname.getName().endsWith("rotdif_tensor.txt"));
			}
		});
		
    for (File file : fileList)
    {    
		  String[] parts = file.getName().split("_");
		  
		  System.out.println(file);

		  //get the tensor
		  RotationalDiffusionTensor tensor =  new RotationalDiffusionTensor(LinearMathFileIO.read(file));	    
		  
	    //get the pdb file
	    String pdbName = parts[0]+".pdb";	    
		  PdbStructure pdb = MoleculeFileIO.readPDB(RelaxationTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/pdb/"+pdbName).getFile());
		  Molecule mol = pdb.getModel(0).createBasicMolecule();
		  
		  //load the experimental data
		  String expFile = parts[0]+"_"+parts[1]+"_"+parts[2]+"_relax.txt";
		  System.out.println(expFile);

		  expFile = RelaxationTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/nmr/relax/"+expFile).getFile();
	    ExperimentalRelaxationData relaxData = ExperimentalRelaxationData.readDataFile(expFile);
	    
	    //get tensor type
	    String type = parts[parts.length-3];
			  
		  RotdifResults results = new RotdifResults(relaxData, mol, false, true, false, null);
		  results.toString();
		  
		  System.out.println(results.getAniTensor());
		  
		  if (type.equals("ani"))		  
		  	Assert.assertTrue(results.getAniTensor().subtract(tensor).normFrobenius()<1.0e-1*9);
		  else
		  if (type.equals("axi"))
		  	Assert.assertTrue(results.getAxiTensor().subtract(tensor).normFrobenius()<1.0e-1*9);
		  else
		  if (type.equals("iso"))
		  	Assert.assertTrue(results.getIsoTensor().subtract(tensor).normFrobenius()<1.0e-1*9);
		  else
		  	Assert.assertTrue(false);
    }
	}
	
	@Test
	public void gb3Test() throws Exception
	{
		File file = new File(RelaxationTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/nmr/relax/1P7F_GB35Fields_curated_relax.txt").getFile());
	  ExperimentalRelaxationData relaxData = ExperimentalRelaxationData.readDataFile(file);
			 
    //get the pdb file
	  PdbStructure pdb = MoleculeFileIO.readPDB(RelaxationTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/pdb/1P7F.pdb").getFile());
	  Molecule mol = pdb.getModel(0).createBasicMolecule();

    
	  RotdifResults results = new RotdifResults(relaxData, mol, false, true, false, null);
	  
	  ModelFreePredictor residue39Sol = null;
	  ModelFreePredictor residue41Sol = null;
	  for (ModelFreePredictor predictor : results.getBestDynamicsSolution().getPredictors())
	  {
	  	if (predictor.getBond().getFromAtom().getPrimaryInfo().getResidueNumber()==39)
	  		residue39Sol = predictor;
	  	if (predictor.getBond().getFromAtom().getPrimaryInfo().getResidueNumber()==41)
	  		residue41Sol = predictor;
	  }
	  
	  Assert.assertTrue(residue39Sol.getRex()>.3);
	  Assert.assertTrue(residue41Sol.getS2()<.7);
	}
	
	@Test
	public void elmTest() throws Exception
	{
		double[] percentDiff = new double[TestCases.values().length];
		double[] tauCPred = new double[TestCases.values().length];
		double[] tauCExp = new double[TestCases.values().length];
		
		int counter = 0;
		for (TestCases testCase : TestCases.values())
		{
			//System.out.format("%s (%s): ", testCase.name, testCase.pdb);
			
			PdbStructure pdb;
			try
			{
		    pdb = MoleculeFileIO.readPDB(RelaxationTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/pdb/"+testCase.pdb).getFile());
			}
			catch (Exception e)
			{
				throw new RelaxationDataException("Could not read in data.", e);
			}
		  
		  BasicMolecule mol;
		  if (testCase.firstChain)
		  	mol = pdb.getModel(testCase.model-1).createChain(0).createBasicMolecule();
		  else
		  	mol = pdb.getModel(testCase.model-1).createBasicMolecule();
	
		  RotationalDiffusionTensorPredictor predictor = new ElmPredictor(293.0);
	  	
	    long startTime = System.currentTimeMillis();	  	
	    RotationalDiffusionTensor tensor = predictor.predictTensor(mol);
	    long endTime = System.currentTimeMillis();
	    double lastProfileComputeTime = (endTime-startTime)/1000.0;
		  
		  tauCExp[counter] = testCase.tauC;
		  tauCPred[counter] = tensor.getTauC();
		  percentDiff[counter] = Math.abs(tauCPred[counter]-tauCExp[counter])/tauCExp[counter]*100.0;

		  System.out.format("tauC original= %.2f, tauC predicted= %.2f, Diff = %.0f, Time = %.2f\n", tauCExp[counter], tauCPred[counter], percentDiff[counter], lastProfileComputeTime);
		  System.out.print("");
		  
		  counter++;
		}
		
		System.out.format("Mean Percent Error = %.2f\n", BasicStat.mean(percentDiff));
		Assert.assertTrue(BasicStat.mean(percentDiff)<9.0);
	}
	
	/*
  @Test
  public void testDocking() throws IOException, PdbException, AtomNotFoundException, RelaxationFileException, RelaxationDataException 
  {  
		String relaxFile = "./edu/umd/umiacs/armor/nmr/relax/2JY6_relax.txt";
	  String pdbFile = "./edu/umd/umiacs/armor/pdb/2JY6.pdb";
	  
	  File relaxFullFile = new File(this.getClass().getClassLoader().getResource(relaxFile).getPath());
	  File pdbFullFile = new File(this.getClass().getClassLoader().getResource(pdbFile).getPath());

	  ExperimentalRelaxationData experimentalData = ExperimentalRelaxationData.readDataFile(relaxFullFile);
		PdbStructure pdb = PdbFileIO.readPDB(pdbFullFile);
		
		BasicMolecule mol1 = pdb.getModel(0).createChain("A").createBasicMolecule();
		BasicMolecule mol2 = pdb.getModel(0).createChain("B").createBasicMolecule();
		
		ElmPredictor predictor = new ElmPredictor(296.0);
  	  			
  	TwoDomainRelaxationAligner twoPredictedComputor = new TwoDomainRelaxationAligner(experimentalData, false);
  	MoleculeRelaxationPredictor relaxPredictor = new MoleculeRelaxationPredictor(twoPredictedComputor.getComputor(), predictor);
  	
  	ConvexAlignmentTranslationDocker elmDock = new ConvexAlignmentTranslationDocker(twoPredictedComputor, relaxPredictor, 1.0e-3);

  	ArrayList<RigidTransform> dockResults = elmDock.dock(mol1, mol2);
  	RigidTransform best = null;;
  	for (RigidTransform t : dockResults)
  	{
  		System.out.println(t.getRotation());
  		
  		if (t.getRotation().distance(Rotation.identityRotation())<1.0e-2 && t.getTranslation().length()<1.0e-1)
  		{
  			best = t;
  		}
  	}
		
		Assert.assertNotNull(best);
  }
  */
}
