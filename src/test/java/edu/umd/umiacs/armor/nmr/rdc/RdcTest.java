/* 
 * ARMOR package
 * 
 * This  software is distributed "as is", without any warranty, including 
 * any implied warranty of merchantability or fitness for a particular
 * use. The authors assume no responsibility for, and shall not be liable
 * for, any special, indirect, or consequential damages, or any damages
 * whatsoever, arising out of or in connection with the use of this
 * software.
 * 
 * Copyright (c) 2013 by Konstantin Berlin 
 * University Of Maryland
 * 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
package edu.umd.umiacs.armor.nmr.rdc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import edu.umd.umiacs.armor.math.Rotation;
import edu.umd.umiacs.armor.math.linear.BlockRealMatrix;
import edu.umd.umiacs.armor.math.stat.BasicStat;
import edu.umd.umiacs.armor.molecule.AtomNotFoundException;
import edu.umd.umiacs.armor.molecule.BasicMolecule;
import edu.umd.umiacs.armor.molecule.BondType;
import edu.umd.umiacs.armor.molecule.Model;
import edu.umd.umiacs.armor.molecule.PdbException;
import edu.umd.umiacs.armor.molecule.MoleculeFileIO;
import edu.umd.umiacs.armor.molecule.PdbStructure;
import edu.umd.umiacs.armor.refinement.ConvexAlignmentTranslationDocker;
import edu.umd.umiacs.armor.refinement.DockInfo;

public class RdcTest
{  
	private enum TestCases
	{
		CASE1 ("Cellular factor BAF", "2EZX.pdb", "2ezx_cns.txt", 0.05),
		CASE2 ("B1 domain of protein G", "3GB1.pdb", "3gb1_cns.txt", 0.05),
		CASE3 ("B3 domain of protein G", "2OED.pdb", "2oed_cns.txt", 0.042),
		CASE4 ("Rat apo-S100B", "1B4C.pdb", "1b4c_cns.txt", 0.05),
		CASE5 ("Cyanovirin-N", "2EZM.pdb", "2ezm_cns.txt", 0.04),
		CASE6 ("Ga interacting protein", "1CMZ.pdb", "1cmz_cns.txt", 0.03),
		CASE7 ("Ubiquitin", "1D3Z.pdb", "1d3z_cns.txt", 0.05),
		CASE8 ("Hen lysozyme", "1E8L.pdb", "1e8l_cns.txt", 0.05);
		//CASE9 ("Oxidized putidaredoxin", "1YJJ.pdb", "1yjj_cns.txt", 0.05);
		
		@SuppressWarnings("unused")
		public final String name;
		public final String pdbFile;
		public final String cnsFile;
		public final double volFrac;

		private TestCases(String name, String pdbFile, String cnsFile, double volFrac)
		{
			this.name = name;
			this.pdbFile = pdbFile;
			this.cnsFile = cnsFile;
			this.volFrac = volFrac;
		}
	}
	

	@Test
	public void testPredictor() throws IOException, RdcFileException, PdbException, AtomNotFoundException
	{
		double[] qualityFactors = new double[TestCases.values().length];
		double[] corrSquared = new double[TestCases.values().length];
		double[] orientationalDifference = new double[TestCases.values().length];
		
		int counter = 0;
		for (TestCases testCase : TestCases.values())
		{
			//System.out.format("pdb: %s, Name: %s, ", testCase.pdbFile, testCase.name);
			
			String pdbFile = RdcTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/pdb/"+testCase.pdbFile).getFile();
			String rdcFile = RdcTest.class.getClassLoader().getResource("./edu/umd/umiacs/armor/nmr/rdc/"+testCase.cnsFile).getFile();
		  ExperimentalRdcData experimentalData = ExperimentalRdcData.readCNS(new File(rdcFile));
			PdbStructure pdb = MoleculeFileIO.readPDB(pdbFile);
			
		  Model model = pdb.getModel(0);
		  BasicMolecule mol = model.createBasicMolecule();
			
		  experimentalData = experimentalData.createFromType(BondType.getBondType("N","H")).createSecondaryOnly(model).createRigid();
		  
		  AlignmentComputor computor = new AlignmentComputor(experimentalData);
	  	PatiPredictor predictor = PatiPredictor.createBicellePredictor(testCase.volFrac);

	  	//compute the experimental tensor
	  	RdcSolution solution = computor.solve(mol);
	    AlignmentTensor expTensor = solution.getTensor();
	  	
	  	//get the predicted tensor
	  	AlignmentTensor predictedTensor = predictor.predictTensor(mol);
	  	
	  	orientationalDifference[counter] = solution.getTensor()
	  			.getProperEigenDecomposition().orientationalDistance(predictedTensor.getProperEigenDecomposition());

	  	//back calcualted
	  	double[] backValues = computor.predict(mol, expTensor);
	  	double qBack = BasicStat.qualityFactor(backValues, solution.values());
	  	double rBack = BasicStat.pearsonsCorrelation(solution.predict(), solution.values());

	  	double[] predValues = computor.predict(mol, predictedTensor);
	  	qualityFactors[counter] = BasicStat.scaledQualityFactor(predValues, solution.values());
	  	double rPred = BasicStat.pearsonsCorrelation(predValues, solution.values());
	  	corrSquared[counter] = rPred*rPred;

	  	//System.out.format("Backcalc: Q= %.2f, r^2=%.2f, Pred: Q= %.2f, r^2= %.2f, Tensor: angle = %.1f\n", 
	  	//		qBack, rBack*rBack, qualityFactors[counter], corrSquared[counter], orientationalDifference[counter]);
	  	
			Assert.assertTrue(qBack<0.12 && rBack*rBack>0.95);

			counter++;
		}
		
		//System.out.format("Average: Q= %.2f, r^2= %.2f, Tensor: angle = %.1f\n", 
		//				BasicStat.mean(qualityFactors), BasicStat.mean(corrSquared), BasicStat.mean(orientationalDifference)*180.0/BasicMath.PI);
		
		Assert.assertTrue(BasicStat.mean(qualityFactors)<0.3);
	}
	
	@Test
  public void testBackCalculation() throws RdcFileException, IOException, PdbException, AtomNotFoundException 
  {  
  	double[][] A = new double[3][];
  	A[0] = new double[]{ 4.174*1.0e-4,   0.296*1.0e-4,  -2.176*1.0e-4};
  	A[1] = new double[]{ 0.296*1.0e-4,   2.980*1.0e-4,   6.766*1.0e-4};
  	A[2] = new double[] {-2.176*1.0e-4,   6.766*1.0e-4,  -7.155*1.0e-4};
  	
		String rdcFile = "./edu/umd/umiacs/armor/nmr/rdc/1d3z_cns.txt";
	  String pdbFile = "./edu/umd/umiacs/armor/pdb/1D3Z.pdb";
	  
	  File rdcFullFile = new File(this.getClass().getClassLoader().getResource(rdcFile).getPath());
	  File pdbFullFile = new File(this.getClass().getClassLoader().getResource(pdbFile).getPath());

	  ExperimentalRdcData experimentalData = ExperimentalRdcData.readCNS(rdcFullFile);
		PdbStructure pdb = MoleculeFileIO.readPDB(pdbFullFile);
		
		BasicMolecule mol = pdb.getModel(0).createChain("A").createBasicMolecule();
		
		//PatiResults results = new PatiResults(experimentalData1, mol1, false);
		PatiResults results = new PatiResults(experimentalData, mol, false);
		
		Assert.assertEquals(0.0d, new BlockRealMatrix(A).subtract(results.getSolution().getTensor()).normFrobenius(), 2.0e-3);
  }
  
  @Test
  public void testDocking() throws RdcFileException, IOException, PdbException, AtomNotFoundException, RdcDataException 
  {  
		String rdcFile = "./edu/umd/umiacs/armor/nmr/rdc/K48R_D77_Ub2_RDCs_pH_7p6.txt";
	  String pdbFile = "./edu/umd/umiacs/armor/pdb/K48R_UB2_ensemble.pdb";
	  
	  File rdcFullFile = new File(this.getClass().getClassLoader().getResource(rdcFile).getPath());
	  File pdbFullFile = new File(this.getClass().getClassLoader().getResource(pdbFile).getPath());

	  ExperimentalRdcData experimentalData = ExperimentalRdcData.readPati(rdcFullFile);
		PdbStructure pdb = MoleculeFileIO.readPDB(pdbFullFile);
		
		BasicMolecule mol1 = pdb.getModel(0).createChain("A").createBasicMolecule();
		BasicMolecule mol2 = pdb.getModel(0).createChain("B").createBasicMolecule();
		BasicMolecule mol = pdb.getModel(0).createBasicMolecule();
		
  	TwoDomainRdcAligner twoDomainComputor = new TwoDomainRdcAligner(experimentalData, true);
  	
  	PatiPredictor predictor = PatiPredictor.createBicellePredictor(0.05);
  	double[] predictedRawData = twoDomainComputor.getComputor().predict(mol, predictor.predictTensor(mol));
  	  	
  	ArrayList<RdcDatum> rawDataList = new ArrayList<RdcDatum>();
  	for (int iter=0; iter<experimentalData.size(); iter++)
  	{
  		RdcDatum old = experimentalData.get(iter);
  		rawDataList.add(new RdcDatum(predictedRawData[iter], old.error(), old.getBond(), old.isRigid()));
  	}
  	
	  ExperimentalRdcData predictedData = new ExperimentalRdcData(rawDataList);
  	TwoDomainRdcAligner twoPredictedComputor = new TwoDomainRdcAligner(predictedData, true);
  	RdcDataPredictor rdcPredictor = new RdcDataPredictor(twoPredictedComputor.getComputor(), predictor);
  	
  	ConvexAlignmentTranslationDocker patiDock = new ConvexAlignmentTranslationDocker(twoPredictedComputor, rdcPredictor, 0.5);

  	List<DockInfo> dockResults = patiDock.dock(mol1, mol2);
  	DockInfo best = null;;
  	for (DockInfo t : dockResults)
  	{
  		System.err.println(t.getRigidTransform());
  		if (t.getRigidTransform().getRotation().distance(Rotation.identityRotation())<1.0e-2 && t.getRigidTransform().getTranslation().length()<1.0e-1)
  		{
  			best = t;
  		}
  	}
		
		Assert.assertNotNull(best);
  }
}
