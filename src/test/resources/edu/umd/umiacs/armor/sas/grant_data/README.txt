README for zip file containing data published in: 
"Small Angle X-ray Scattering as a Complementary Tool for High-Throughput Structural Studies"
Grant, et.al. 2011 Biopolymers Volume 95, number 8, pages 517-529

The unzipped folder contains the proof of the paper in pdf form and three directories
named "data", "gnom_output", and "pdb_files".

The folder "data" contains the small angle X-ray scattering data used for analysis of
the particle in solution, comparison against the calculated scattering of the high
resolution structures using CRYSOL, generation of the pair distribution function using 
GNOM, and subsequent generation of ab initio envelopes.

The folder "gnom_output" contains the output files from AutoGNOM.  These files contain
the fit to the experimental scattering as well as the pair distribution function
calculated by AutoGNOM.

The folder "pdb_files" contains both the high resolution structures published in the
Protein Data Bank as well as the ab initio SAXS envelope calculated using the AutoGNOM
output file.  
	
	The high resolution structures are those shown in the figures of the paper
	and are not necessarily the same as the asymmetric unit found in the PDB. 
	In the cases of mixtures, the tetramer is given and the dimer form can be 
	extracted from this.
	
	The ab initio SAXS envelopes have been superpositioned to the corresponding
	high resolution structure as shown in the figures of the paper.
	
All files begin with a number that corresponds to the ID number given in Tables I and II
of the paper.  The rest of the file name indicates whether it is SAXS data, or in the
case of pdb files, indicates the pdb assession code for the high resolution structure as 
well as the oligomer organization chosen by the SAXS data and shown in the figures of 
the paper.

Contact details:

Thomas Grant, tgrant@hwi.buffalo.edu
Edward Snell, esnell@hwi.buffalo.edu